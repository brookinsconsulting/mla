<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
ActiveExtensions[]
ActiveExtensions[]=ezjscore
ActiveExtensions[]=ezmultiupload
ActiveExtensions[]=ez_network
ActiveExtensions[]=ezautosave
ActiveExtensions[]=ezoe
ActiveExtensions[]=ezformtoken
ActiveExtensions[]=ezstarrating
ActiveExtensions[]=ezgmaplocation
ActiveExtensions[]=ezdemo
ActiveExtensions[]=ezwt
ActiveExtensions[]=ezflow
ActiveExtensions[]=ezcomments
ActiveExtensions[]=ezie
ActiveExtensions[]=ezodf
ActiveExtensions[]=ezprestapiprovider
ActiveExtensions[]=ezfind
ActiveExtensions[]=eztags
ActiveExtensions[]=ezcollaborationworkflow
ActiveExtensions[]=admin_styles
ActiveExtensions[]=multimedia
ActiveExtensions[]=list_styles
ActiveExtensions[]=eztika
ActiveExtensions[]=siteanalyzer

[Session]
SessionNameHandler=custom

[SiteSettings]
DefaultAccess=eng
SiteList[]
SiteList[]=site
SiteList[]=eng
SiteList[]=site_admin
RootNodeDepth=1

[UserSettings]
LogoutRedirect=/

[SiteAccessSettings]
CheckValidity=false
AvailableSiteAccessList[]
AvailableSiteAccessList[]=site
AvailableSiteAccessList[]=eng
AvailableSiteAccessList[]=site_admin
MatchOrder=uri
HostMatchMapItems[]

[DesignSettings]
DesignLocationCache=enabled

[DebugSettings]
DebugOutput=enabled
DebugByUser=enabled
DebugUserIDList[]
DebugUserIDList[]=65
DebugUserIDList[]=31337
DebugRedirection=enabled

[RegionalSettings]
TranslationSA[]
TranslationSA[eng]=Eng
TextTranslation=disabled

[FileSettings]
VarDir=var/site

[MailSettings]
Transport=sendmail
AdminEmail=info@thinkcreative.com
EmailSender=

[TemplateSettings]
ShowUsedTemplates=enabled

[EmbedViewModeSettings]
AvailableViewModes[]
AvailableViewModes[]=embed
AvailableViewModes[]=embed-inline
InlineViewModes[]
InlineViewModes[]=embed-inline
*/ ?>
