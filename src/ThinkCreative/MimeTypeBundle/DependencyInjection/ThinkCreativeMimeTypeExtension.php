<?php

namespace ThinkCreative\MimeTypeBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class ThinkCreativeMimeTypeExtension extends Extension
{

    public function load(array $configurations, ContainerBuilder $container)
    {

        $Configuration = new Configuration();

		$YamlFileLoader = new Loader\YamlFileLoader(
			$container, new FileLocator(__DIR__ . '/../Resources/config')
		);
		$YamlFileLoader->load('services.yml');

        $container->setParameter(
            'mimetype.config', $this->processConfiguration($Configuration, $configurations)
        );
	}

	public function getAlias() {
		return 'think_creative_mime_type';
	}

}