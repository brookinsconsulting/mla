<?php

namespace ThinkCreative\MimeTypeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder()
    {
        $TreeBuilder = new TreeBuilder();
        $RootNode = $TreeBuilder->root( 'think_creative_mime_type' );

        $RootNode
            ->children()
                ->arrayNode( 'mimetypes' )
                    ->children()

                        ->arrayNode( 'list' )

                            ->prototype( 'array' )
                                ->children()
                                    ->scalarNode( 'name' )
                                        ->isRequired()
                                    ->end()
                                    ->scalarNode( 'mimeType' )
                                        ->isRequired()
                                    ->end()
                                ->end()
                            ->end()

                        ->end()

                    ->end()
                ->end()
            ->end()
        ;

        return $TreeBuilder;
    }

}