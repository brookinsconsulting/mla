<?php

namespace ThinkCreative\MimeTypeBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ThinkCreativeMimeTypeBundle extends Bundle
{

    protected $name = "ThinkCreativeMimeTypeBundle";

}