<?php
namespace ThinkCreative\MimeTypeBundle\Services;

use eZ\Publish\Core\FieldType\XmlText\Converter;
use ThinkCreative\SiteLinkBundle\Services\SiteLink;

class eZXMLTextLinkPreConverter implements Converter
{

    protected $eZPublishAPIRespository;
    protected $Configuration;

	public function __construct($ezpublish_api_repository) {
        $this->eZPublishAPIRespository = $ezpublish_api_repository;
	}

	public function convert(\DOMDocument $xml) {
        $ContentService = $this->eZPublishAPIRespository->getContentService();
        $LocationService = $this->eZPublishAPIRespository->getLocationService();
		foreach(
			$xml->getElementsByTagName("link") as $Link
		) {
            if(
                $Link->hasAttribute("object_id") || $Link->hasAttribute("node_id")
            ) {
                if(
                    $Link->hasAttribute("node_id")
                ){
                    try {
                        $Location = $LocationService->loadLocation(
                            $Link->getAttribute("node_id")
                        );
                        $Content = $ContentService->loadContentByContentInfo(
                            $Location->getContentInfo()
                        );
                        $File = $Content->getField("file");
                        $fileType = isset($File) ? $File->value->mimeType : false;
                    } catch( \Exception $e ) {
    					continue;
    				}
                }
                else
                {
                    try {
                        $Content = $ContentService->loadContent(
                            $Link->hasAttribute("object_id")
                        );
                        $File = $Content->getField("file");
                        $fileType = isset($File) ? $File->value->mimeType : false;
    				} catch( \Exception $e ) {
    					continue;
    				}
                }
                foreach($this->Configuration['mimetypes']['list'] as $mimetype)
                {
                    if($mimetype['mimeType'] == $fileType)
                    {
                        $Link->setAttribute("class", "file-".$mimetype['name']);
                    }
                }
            }
		}
	}

    public function processConfiguration( array $configuration )
    {
        $this->Configuration = $configuration;
    }

}