<?php

namespace ThinkCreative\ThemesBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ThinkCreativeThemesBundle extends Bundle
{

    protected $name = "ThinkCreativeThemesBundle";

}
