<?php

namespace ThinkCreative\TwitterBootstrapBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ThinkCreativeTwitterBootstrapBundle extends Bundle
{

    protected $name = "ThinkCreativeTwitterBootstrapBundle";

}
