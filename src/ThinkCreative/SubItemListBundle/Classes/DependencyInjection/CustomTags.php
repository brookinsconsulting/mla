<?php

namespace ThinkCreative\SubItemListBundle\Classes\DependencyInjection;

use eZ\Publish\Core\Repository\ContentTypeService;
use ThinkCreative\BridgeBundle\Services\CustomTagsManager;
use ThinkCreative\BridgeBundle\DependencyInjection\CustomTagsHandlerInterface;

/**
 * Adds sub-item-list custom tag
 */ 
class CustomTags implements CustomTagsHandlerInterface
{

    /**
     * Language to use when gathering content types
     * @var string
     */
    const LANGUAGE = "eng-US";

    /**
     * List of content types
     * @var array
     */
    protected $contentTypeList;

    /**
     * List of templates
     * @var array
     */
    protected $templates;


    /**
     * Constructor
     */
    public function __construct(ContentTypeService $contentTypeService, array $templates)
    {
        // generate list of content types
        $this->contentTypeList = array();
        $this->contentTypeList[""] = "(all)";

        foreach ($contentTypeService->loadContentTypeGroups() as $contentGroup) {
            foreach($contentTypeService->loadContentTypes($contentGroup) as $contentType) {
                $this->contentTypeList[$contentType->identifier] = ucwords($contentGroup->identifier) . " | " . $contentType->getName(self::LANGUAGE);
            }
        }
        asort($this->contentTypeList, SORT_STRING);

        // templates
        $this->templates = $templates;

    }

    /**
     * Register custom tag
     */
    public function registerCustomTagList(CustomTagsManager $customtags_manager) {
        $CustomTags = array(
            "sub-item-list" => array(
                "attributes" => array(
                    "parent_location" => array(
                        "name" => "Parent Location",
                        "type" => "link",                        
                    ),
                    "content_type" => array(
                        "name" => "Content Type",
                        "type" => "select",
                        "selection" => $this->contentTypeList
                    ),
                    "limit" => array(
                        "name" => "Limit"
                    ),
                    "offset" => array(
                        "name" => "Offset"
                    ),
                    "template" => array(
                        "name" => "Template",
                        "type" => "select",
                        "selection" => $this->templates
                    ),
                    "view" => array(
                        "name" => "View",
                        "type" => "select",
                        "selection" => array(
                            "line" => "line",
                            "embed" => "embed",
                            "embed-inline" => "embed-inline",
                            "full" => "full"
                        )
                    )
                ),
                "controller" => "ThinkCreativeSubItemListBundle:SubItemList:customTag",
                "template" => "",
            ),
        );

        foreach($CustomTags as $Name => $CustomTagDefinition) {
            $customtags_manager->register(
                $Name, $CustomTagDefinition
            );
        }
    }
}