<?php

namespace ThinkCreative\SubItemListBundle\Services;

use eZ\Publish\Core\Repository\SearchService;
use eZ\Publish\Core\Repository\LocationService;
use eZ\Publish\API\Repository\Values\Content,
    eZ\Publish\API\Repository\Values\Content\LocationQuery,
    eZ\Publish\API\Repository\Values\Content\Query\Criterion,
    eZ\Publish\API\Repository\Values\Content\Search\SearchResult,
    eZ\Publish\API\Repository\Values\Content\Query\SortClause;
use \InvalidArgumentException;

/**
 * Provides method(s) to perform sub item location search
 */
class SubItemListService
{

    protected $searchService;
    protected $locationService;

    /**
     * Constructor.
     * @param SearchService $searchService
     * @param LocationService $locationService
     */
    public function __construct(SearchService $searchService, LocationService $locationService)
    {
        $this->searchService = $searchService;
        $this->locationService = $locationService;
    }

    /**
     * Fetch array of locations for given params
     * @param array $params
     * @return array
     * @throws InvalidArgumentException
     *
     * Query Params:
     * parentLocationId integer - Parent Location id
     * contentType string[] - Content type identifier(s)
     * visible bool - Location visibility
     * priorityRange integer[] - Narrows results to items within given priority range
     * limit integer - Limits results to given value
     * offset integer - Returns results from given point onward
     */
    public function fetch(array $params = array())
    {

        // use criterion to find partner sub items
        $query = new LocationQuery();

        // build query
        $criterion = array();
        $sort = array();

        // parent location id
        if (array_key_exists("parentLocationId", $params) && $params['parentLocationId']) {

            // must be an integer
            if (!is_numeric($params['parentLocationId'])) {
                throw new InvalidArgumentException("Parameter 'parentLocationId' must be an integer.");
            }

            // make criterion
            $criterion[] = new Criterion\ParentLocationId( array($params['parentLocationId']) );

            // respect location sort options
            $location = $this->locationService->loadLocation( $params['parentLocationId'] );
                
            // sort order
            $sort_order = LocationQuery::SORT_DESC;
            if ($location->sortOrder == $location::SORT_ORDER_ASC) {
                $sort_order = LocationQuery::SORT_ASC;
            }

            // sort field
            switch($location->sortField) {
                case $location::SORT_FIELD_PRIORITY:
                    $sort[] = new SortClause\LocationPriority( $sort_order );
                    break;
                case $location::SORT_FIELD_PATH:
                    $sort[] = new SortClause\LocationPathString( $sort_order );
                    break;
                case $location::SORT_FIELD_PUBLISHED:
                    $sort[] = new SortClause\DatePublished( $sort_order );
                    break;
                case $location::SORT_FIELD_MODIFIED:
                    $sort[] = new SortClause\DateModified( $sort_order );
                    break;
                case $location::SORT_FIELD_SECTION:
                    $sort[] = new SortClause\SectionName( $sort_order );
                    break;
                case $location::SORT_FIELD_DEPTH:
                    $sort[] = new SortClause\LocationDepth( $sort_order );
                    break;
                case $location::SORT_FIELD_CLASS_IDENTIFIER:
                    $sort[] = new SortClause\ClassIdentifier( $sort_order );
                    break;
                case $location::SORT_FIELD_CLASS_NAME:
                    break;
                case $location::SORT_FIELD_NAME:
                    $sort[] = new SortClause\ContentName( $sort_order );
                    break;
                case $location::SORT_FIELD_MODIFIED_SUBNODE:
                    break;
                case $location::SORT_FIELD_NODE_ID:
                    break;
                case $location::SORT_FIELD_CONTENTOBJECT_ID:
                    $sort[] = new SortClause\ContentId( $sort_order );
                    break;
            }
        }

        // content type
        if (array_key_exists("contentType", $params) && $params['contentType']) {

            // single string
            if (is_string($params['contentType'])) {
                $params['contentType'] = array($params['contentType']);

            // not string or array
            } else if (!is_array($params['contentType'])) {
                throw new InvalidArgumentException("Parameter 'contentType' must be string or array of strings.");
            }

            // make criterion
            $criterion[] = new Criterion\ContentTypeIdentifier($params['contentType']);
        }

        // visibility
        if (array_key_exists("visibile", $params)) {
            $criterion[] = new Criterion\Visibility( (bool) $params['visibile'] ? Criterion\Visibility::HIDDEN : Criterion\Visibility::HIDDEN );
        }
        
        // priority range
        if (array_key_exists("priorityRange", $params)) {

            // must be array
            if (!is_array($params['priorityRange']) || count($params['priorityRange']) != 2) {
                throw new InvalidArgumentException("Parameter 'priorityRange' must be an array containing two integers.");
            }

            // make criterion
            $criterion[] = new Criterion\LocationPriority(
                Criterion\Operator::BETWEEN,
                $params['priorityRange']
            );
        }
        
        // set query
        $query->criterion = new Criterion\LogicalAnd(
            $criterion
        );

        // sort
        if (!$sort) $sort[] = new SortClause\LocationPriority( Query::SORT_DESC );
        $query->sortClauses = $sort;

        // limit
        if (array_key_exists("limit", $params) && $params['limit']) {

            // must be integer
            if (!is_numeric($params['limit']) && $params['limit'] >= 0) {
                throw new InvalidArgumentException("Parameter 'limit' must be an integer that is greater than or equal to zero.");
            }

            // set limit
            $query->limit = $params['limit'];
        }

        // offset
        if (array_key_exists("offset", $params) && $params['offset']) {

            // must be integer
            if (!is_numeric($params['offset']) && $params['offset'] >= 0) {
                throw new InvalidArgumentException("Parameter 'offset' must be an integer that is greater than or equal to zero.");
            }

            // set offset
            $query->offset = $params['offset'];

        // default offset
        } else {
            $query->offset = 0;
        }

        // get results
        $results = $this->searchService->findLocations($query);

        // get array of locations
        $locations = array();
        foreach ($results->searchHits as $searchItem) {
            $locations[] = $searchItem->valueObject;
        }

        // return locations
        return array(
            "totalCount" => $results->totalCount,
            "locations" => $locations
        );

    }
}