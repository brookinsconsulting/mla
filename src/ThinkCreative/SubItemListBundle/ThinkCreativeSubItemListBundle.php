<?php

namespace ThinkCreative\SubItemListBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ThinkCreativeSubItemListBundle extends Bundle
{

	protected $name = "ThinkCreativeSubItemListBundle";

}
