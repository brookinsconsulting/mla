<?php

namespace ThinkCreative\SubItemListBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for Sub item fetches
 */
class SubItemListController extends Controller
{

    /** 
     * If true will return error response messages
     * otherwise will return blank
     * @var boolean
     */
    const DEBUG_MESSAGES = true;

    /**
     * Perform a sub item fetch and dump results to template
     * @param string $template
     * @param array $fetchParams
     * @param array $templateParams
     */
    public function fetchAction($template = "", array $fetchParams = array(), array $templateParams = array())
    {

        // get sub item list service
        $subItemListService = $this->get("thinkcreative.sub_item_list");

        // perform fetch
        $subItemList = $subItemListService->fetch($fetchParams);

        // return template
        return $this->render(
            $template,
            array_merge(
                $templateParams,
                array(
                    "subItemList" => $subItemList['locations'],
                    "subItemTotalCount" => $subItemList['totalCount']
                )
            )
        );

    }

    /**
     * Perform a sub item fetch via the custom tag
     * @param string $template - ignored
     * @param array $variables
     */
    public function customTagAction($template = "", $variables = array())
    {

        // gereate query parameters
        $queryParams = array();

        // parent location
        if (trim($variables['parent_location'])) {

            // must be eznode
            if (substr(strtolower(trim($variables['parent_location'])), 0, 6) != "eznode") {
                return new Response(
                    self::DEBUG_MESSAGES ?
                    "SubItemList Error: Parent Location must be 'eznode.'" :
                    ""
                );
            }

            // get location id
            $locationId = 
                str_replace(
                    substr(
                        strtolower(
                            trim(
                                $variables['parent_location']
                            )
                        ), 
                        0, 
                        6
                    ), 
                    "", 
                    $variables['parent_location'] 
                )
            ;
            $locationId = str_replace("://", "", $locationId);
            $locationId = str_replace(":\\\\", "", $locationId);

            // set query param
            $queryParams['parentLocationId'] = intval($locationId);
        }

        // content type
        if (trim($variables['content_type'])) {
            $queryParams['contentType'] = trim($variables['content_type']);
        }

        // limit
        if (trim($variables['limit'])) {

            // must be integer
            if (!is_numeric(trim($variables['limit']))) {
                return new Response(
                    self::DEBUG_MESSAGES ?
                    "SubItemList Error: Limit must be a number." :
                    ""
                );
            }

            // set query param
            $queryParams['limit'] = intval(trim($variables['limit']));

        }

        // offset
        if (trim($variables['offset'])) {

            // must be integer
            if (!is_numeric(trim($variables['offset']))) {
                return new Response(
                    self::DEBUG_MESSAGES ?
                    "SubItemList Error: Offset must be a number." :
                    ""
                );
            }

            // set query param
            $queryParams['offset'] = intval(trim($variables['offset']));

        }

        // delegate to fetchAction
        return $this->fetchAction(
            trim($variables['template']),
            $queryParams,
            array(
                "view" => $variables['view']
            )
        );

    }

}