<?php

namespace ThinkCreative\SubItemListBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {

        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('thinkcreative_sub_item_list');

        $rootNode
            ->children()
                ->arrayNode("customtag_templates")
                    ->defaultValue(
                        array(
                            "ThinkCreativeSubItemListBundle:customtag:sub_item_list.html.twig" => "Default"
                        )
                    )
                    ->prototype("scalar")
                ->end()
            ->end()
        ;

        return $treeBuilder;

    }
}