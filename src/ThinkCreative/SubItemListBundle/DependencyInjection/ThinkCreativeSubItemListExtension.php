<?php

namespace ThinkCreative\SubItemListBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class ThinkCreativeSubItemListExtension extends Extension
{

    public function load( array $configurations, ContainerBuilder $container )
    {

        // configurations
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configurations);
        $container->setParameter("think_creative_sub_item_list.customtag_templates", $config['customtag_templates']);

        // services
        $YamlFileLoader = new YamlFileLoader(
            $container, new FileLocator( __DIR__ . '/../Resources/config' )
        );
        $YamlFileLoader->load( 'services.yml' );
    }

    public function getAlias()
    {
        return 'think_creative_sub_item_list';
    }

}
