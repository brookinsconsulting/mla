<?php

namespace ThinkCreative\MenubarBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ThinkCreativeMenubarBundle extends Bundle
{

	protected $name = "ThinkCreativeMenubarBundle";

}
