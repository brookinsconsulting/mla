<?php

namespace ThinkCreative\MenubarBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Yaml\Yaml;

class ThinkCreativeMenubarExtension extends Extension
{

	public function load(array $configurations, ContainerBuilder $container) {
		$YamlFileLoader = new YamlFileLoader(
			$container, new FileLocator(__DIR__ . '/../Resources/config')
		);
		$YamlFileLoader->load('services.yml');

		$configurations = call_user_func_array(
			'array_merge_recursive', array_reverse($configurations)
		);

		$container->getParameterBag()->add(
			array(
				'thinkcreative.menubar.items' => isset($configurations['Menubars']) ? $configurations['Menubars'] : array(),
				'thinkcreative.menubar.options' => Yaml::parse(__DIR__ . '/../Resources/config/options.yml'),
				'thinkcreative.menubar.additional_options' => isset($configurations['Options']) ? $configurations['Options'] : array(),
			)
		);
	}

	public function getAlias() {
		return "think_creative_menubar";
	}

}
