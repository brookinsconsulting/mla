<?php

namespace ThinkCreative\MenubarBundle\Classes;

use ThinkCreative\CoreBundle\Classes\OptionsManager\OptionsHandler;

class MenubarItem extends MenubarItemComponent
{

	protected $Delimiter;
	protected $Menu;

	public function __construct(OptionsHandler $options, $delimiter = false) {
		if($options->get('condition')) {
			$options->add(
				self::executeConditional(
					$options->export('switch', true)
				)
			);
		}

		parent::__construct(
			$options->export('inherit', true)
		);

		if($delimiter) {
			$this->setDelimiter($delimiter);
		}
	}

	public function getDelimiter() {
		return $this->Delimiter ? call_user_func($this->Delimiter) : NULL;
	}

	public function getMenu() {
		return $this->Menu;
	}

	public function removeDelimiter() {
		$this->Delimiter = NULL;
	}

	public function setDelimiter(\Closure $delimiter) {
		$this->Delimiter = $delimiter;
	}

	public function setMenu(Menubar $menubar) {
		$this->Menu = $menubar;
	}

	protected static function executeConditional(OptionsHandler $options) {
		$Result = array();
		$Conditional = $options->get('conditional');
		$Condition = $options->get('condition');
		switch($Condition[0]) {
			case 'result': {
				$Result = $Condition[1];
				break;
			}
		}

		// check $Result type to confirm a boolean value
		if(is_bool($Result)) {
			return $Conditional[(int) $Result];
		}

		return array();
	}

}
