<?php

namespace ThinkCreative\MenubarBundle\Classes;

use Symfony\Component\HttpFoundation\ParameterBag;
use ThinkCreative\CoreBundle\Classes\OptionsManager\OptionsHandler;

class Menubar
{

    protected $Header;
    protected $Orientation;
    protected $Delimiter;
    protected $Items;
    protected $Class;
    protected $DataProperties;

    public function __construct(OptionsHandler $options) {
        $this->Orientation = $options->get('orientation');
        $this->DataProperties = new ParameterBag();

        $Class = $options->get('class');
        $this->Class = $this->Orientation . ($Class ? " $Class" : '');
        if($Delimiter = $options->get('delimiter')) {
            $this->Delimiter = is_string($Delimiter) ? $Delimiter : '&raquo;';
            $this->Class .= ' delimiter';
        }
    }

    public function getClass() {
        return $this->Class;
    }

    public function getDelimiter() {
        return $this->Delimiter;
    }

    public function getDataProperties() {
        return $this->DataProperties;
    }

    public function getHeader() {
        return $this->Header;
    }

    public function getItems() {
        return $this->Items;
    }

    public function getOrientation() {
        return $this->Orientation;
    }

    public function hasHeader($raw = false) {
        return $this->Header ? $this->Header->hasContent($raw) : false;
    }

    public function setHeader(MenubarItemComponent $item) {
        $this->Header = $item;
    }

    public function setItems(array $items) {
        $Count = count($items);
        foreach($items as $Key => $Item) {
            if($Key+1 == $Count) {
                $Item->removeDelimiter();
            }
            $this->add($Item);
        }
    }

    protected function add(MenubarItemComponent $item) {
        $this->Items[] = $item;
    }

}
