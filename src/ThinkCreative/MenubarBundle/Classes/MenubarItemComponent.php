<?php

namespace ThinkCreative\MenubarBundle\Classes;

use Symfony\Component\HttpFoundation\ParameterBag;
use ThinkCreative\CoreBundle\Classes\OptionsManager\OptionsHandler;

class MenubarItemComponent
{

    protected $Content;
    protected $Link;
    protected $Class;
    protected $isExternal;
    protected $hasTarget;
    protected $DataProperties;

    public function __construct(OptionsHandler $options) {
        $this->setContent(
            $options->get('content')
        );
        $this->Link = $options->get('link');
        $this->Class = $options->get('class');
        $this->isExternal = $options->get('is_external');
        $this->hasTarget = $options->get('new_window');
        $this->DataProperties = new ParameterBag();
    }

    public function getClass() {
        return $this->Class;
    }

    public function getContent() {
        return $this->Content;
    }

    public function getDataProperties() {
        return $this->DataProperties;
    }

    public function getLink() {
        return $this->Link;
    }

    public function getTarget() {
        return $this->hasTarget() ? '_blank' : '';
    }

    public function hasContent($raw = false) {
        return $raw || is_string($this->Content) ? (bool) $this->Content : false;
    }

    public function hasTarget() {
        return $this->hasTarget;
    }

    public function isExternal() {
        return $this->isExternal;
    }

    public function setContent($content) {
        if(($isString = is_string($content)) || is_bool($content)) {
            $this->Content = $isString ? htmlentities($content, ENT_COMPAT | 'ENT_HTML5' | 'ENT_HTML401', "UTF-8") : $content;
            return true;
        }
        return false;
    }

    public function setLink($link) {
        if(is_string($link)) {
            $this->Link = $link;
        }
    }

}
