<?php

namespace ThinkCreative\MenubarBundle\Services;

use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ThinkCreative\CoreBundle\Classes\OptionsManager\OptionsHandler;
use ThinkCreative\MenubarBundle\Classes;
use ThinkCreative\MenubarBundle\Events;

class Menubar
{

    protected $EventDispatcher;
    protected $OptionsManager;
    protected $MenubarDefinitions;
    public $MenubarList;

    public function __construct(ContainerInterface $container) {
        $this->EventDispatcher = $container->get('event_dispatcher');
        $this->OptionsManager = $container->get('thinkcreative.optionsmanager');
        $this->MenubarDefinitions = $container->getParameter('thinkcreative.menubar.items');
        $this->MenubarList = new ParameterBag();

        $this->createOptionSchemas(
            $container->getParameter('thinkcreative.menubar.options'),
            $container->getParameter('thinkcreative.menubar.additional_options')
        );
    }

    public function createMenubar(OptionsHandler $options) {
        $Menubar = new Classes\Menubar(
            $options->export()
        );

        if(
            $Header = $options->get('header')
        ) {
            $Menubar->setHeader(
                new Classes\MenubarItemComponent(
                    $this->OptionsManager->createHandler(
                        'MenubarItemComponent', $Header
                    )
                )
            );
        }

        if(
            $Items = $options->get('items')
        ) {
            foreach($Items as $Key => $Item) {
                $Items[$Key] = $this->createMenubarItem(
                    $Item, $Menubar
                );
            }
            $Menubar->setItems($Items);
            return $Menubar;
        }

        $this->EventDispatcher->dispatch(
            Events\MenubarEvents::CREATE_MENUBAR,
            new Events\CreateMenubarEvent(
                $Menubar, $options
            )
        );

        if(
            $options->has('id') && $MenubarID = $options->get('id')
        ) {
            $this->MenubarList->set(
                $MenubarID, $Menubar
            );
        }

        return $Menubar;
    }

    public function createMenubarFromOptions(array $options) {
        return $this->createMenubar(
            $this->OptionsManager->createHandler(
                'Menubar', $options
            )
        );
    }

    public function createMenubarItem(array $configuration, Classes\Menubar $menubar) {
        return new Classes\MenubarItem(
            $this->OptionsManager->createHandler(
                'MenubarItem', $configuration
            ),
            function() use ($menubar) {
                return $menubar->getDelimiter();
            }
        );
    }

    public function getMenubar($id) {
        return (
            $this->MenubarList->has($id) ? $this->MenubarList->get($id) : false
        );
    }

    public function getMenubarDefinition($identifier) {
        if(isset($this->MenubarDefinitions[$identifier])) {
            return $this->MenubarDefinitions[$identifier];
        }
        return false;
    }

    protected function createOptionSchemas(array $parameters = array(), array $additional = array()) {
        foreach($parameters as $Schema => $Configuration) {
            $this->OptionsManager->createSchema($Schema, $Configuration);
            if(isset($additional[$Schema])) {
                $this->OptionsManager->appendSchema($Schema, $additional[$Schema]);
            }
        }
    }

}