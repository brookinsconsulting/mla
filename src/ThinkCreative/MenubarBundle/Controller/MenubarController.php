<?php

namespace ThinkCreative\MenubarBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MenubarController extends Controller
{

    public function indexAction($options_handler_id, $format = 'html', $template = '') {
        $Query = $this->getRequest()->query;
        $format = (
            $Query->has('format') ? $Query->get('format') : $format
        );

        if(!$template) {
            $template = "ThinkCreativeMenubarBundle:menubar:menu.$format.twig";
        }

        $OptionsHandler = $this->get('thinkcreative.optionsmanager')->getHandler($options_handler_id);

        if(
            $Query->has('menu_root')
        ) {
            $OptionsHandler->set(
                'menu_root', (int) $Query->get('menu_root')
            );
        }

        $Response = $this->render(
            $template,
            array(
                'template' => $template,
                'menubar' => $this->get('thinkcreative.menubar')->createMenubar($OptionsHandler),
            )
        );

        if($format != 'html') {
            switch($format) {
                case 'json': {
                    $ContentType = 'application/json';
                }
            }
            $Response->headers->set(
                'Content-Type', $ContentType
            );
        }

        return $Response;
    }

    public function menuAction($identifier, $format = 'html', $template = '') {

        if(
            ($DotPosition = strpos($identifier, '.')
        ) !== false) {
            $format = substr(
                $identifier, $DotPosition + 1
            );
            $identifier = substr(
                $identifier, 0, $DotPosition
            );
        }

        if(
            !$template && $this->container->hasParameter($identifier . '_template')
        ) {
            $template = $this->container->getParameter($identifier . '_template');
        }

        return $this->indexAction(
            $this
                ->container
                ->get('thinkcreative.optionsmanager')
                ->createHandler(
                    'Menubar',
                    $this
                        ->container
                        ->get('thinkcreative.menubar')
                        ->getMenubarDefinition($identifier)
                )
                ->getHandlerID(),
            $format,
            $template
        );
    }

}
