<?php

namespace ThinkCreative\MenubarBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Fragment\FragmentHandler;
use Symfony\Component\HttpKernel\Controller\ControllerReference;

class MenubarExtension extends \Twig_Extension
{

	protected $Container;
	protected $FragmentHandler;
	protected $MenubarService;

	public function __construct(ContainerInterface $container, FragmentHandler $handler) {
		$this->Container = $container;
		$this->FragmentHandler = $handler;
		$this->MenubarService = $container->get('thinkcreative.menubar');
	}

	public function getFunctions() {
		return array(
			'menubar' => new \Twig_Function_Method(
				$this,
				'createMenubar',
				array(
					'is_safe' => array('html'),
				)
		),
		);
	}

	public function createMenubar($options = array(), $strategy = 'inline', $format = 'html', $template = '') {
		if(
			is_string($options) && strpos($options, '@') === 0
		) {
			if(
				!$options = $this
					->MenubarService
					->getMenubarDefinition(
						substr($options, 1)
					)
			) {
				return '';
			}
		}

		$OptionsHandler = $this
			->Container
			->get('thinkcreative.optionsmanager')
			->createHandler(
				'Menubar', $options
			)
		;

		return $this->FragmentHandler->render(
			new ControllerReference(
				'ThinkCreativeMenubarBundle:Menubar:index',
				array(
					'options_handler_id' => $OptionsHandler->getHandlerID(),
					'format' => $format,
					'template' => $template,
				)
			),
			$strategy
		);
	}

	public function getName() {
		return 'menubar';
	}

}
