<?php

namespace ThinkCreative\MenubarBundle\Events;

use Symfony\Component\EventDispatcher\Event;
use ThinkCreative\CoreBundle\Classes\OptionsManager\OptionsHandler;
use ThinkCreative\MenubarBundle\Classes\Menubar;

class CreateMenubarEvent extends Event
{

	protected $Menubar;
	protected $OptionsHandler;

	public function __construct(Menubar $menubar, OptionsHandler $options) {
		$this->Menubar = $menubar;
		$this->OptionsHandler = $options;
	}

	public function getMenubar() {
		return $this->Menubar;
	}

	public function getOptionsHandler() {
		return $this->OptionsHandler;
	}

}
