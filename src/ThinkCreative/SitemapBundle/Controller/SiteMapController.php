<?php

namespace ThinkCreative\SitemapBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class SiteMapController extends Controller
{

    public function indexAction()
    {
        $SiteMapHandler = $this->container->get( 'thinkcreative.sitemap_handler' );

        if(
            !$SiteMapContent = $SiteMapHandler->fetchSiteMap()
        ) {
            $SiteMapContent = $SiteMapHandler->generateSiteMap( true );
        }

        $Response = new Response( $SiteMapContent );

        $Response->setPrivate();
        $Response->setMaxAge( 0 );
        $Response->setSharedMaxAge( 0 );
        $Response->headers->addCacheControlDirective( 'must-revalidate', true );
        $Response->headers->addCacheControlDirective( 'no-store', true );

        return $Response;
    }

}
