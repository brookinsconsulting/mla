<?php

namespace ThinkCreative\SitemapBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;
use ThinkCreative\SitemapBundle\Command\SitemapCommand;

class SiteMapListener implements EventSubscriberInterface
{

    protected $Container;

    public function __construct( Container $container )
    {
        $this->Container = $container;
    }

    public function onConsoleTerminate( ConsoleTerminateEvent $event )
    {
        $SiteMapHandler = $this->Container->get( 'thinkcreative.sitemap_handler' );

        if(
            $SiteMapHandler->isEnabled( true )
        ) {
            $CommandList = $SiteMapHandler->getCommandList();
            $CommandName = $event->getCommand()->getName();

            if(
                in_array( $CommandName, $CommandList )
            ) {
                SitemapCommand::generateSiteMap(
                    $event->getOutput(), $SiteMapHandler, $SiteMapHandler->getSiteAccessList()
                );
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            ConsoleEvents::TERMINATE => array( 'onConsoleTerminate' ),
        );
    }

}
