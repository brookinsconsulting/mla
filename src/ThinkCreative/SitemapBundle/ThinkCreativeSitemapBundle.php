<?php

namespace ThinkCreative\SitemapBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ThinkCreativeSitemapBundle extends Bundle
{

    protected $name = 'ThinkCreativeSitemapBundle';

}
