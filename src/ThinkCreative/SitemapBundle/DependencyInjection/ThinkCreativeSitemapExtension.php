<?php

namespace ThinkCreative\SitemapBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class ThinkCreativeSitemapExtension extends Extension
{

    public function load( array $configurations, ContainerBuilder $container )
    {
        $Configuration = new Configuration();

        $YamlFileLoader = new YamlFileLoader(
            $container, new FileLocator( __DIR__ . '/../Resources/config' )
        );
        $YamlFileLoader->load( 'services.yml' );

        $container->setParameter(
            'sitemap.xml', $this->processConfiguration( $Configuration, $configurations )
        );
    }

    public function getAlias()
    {
        return 'think_creative_sitemap';
    }

}
