<?php

namespace ThinkCreative\SitemapBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder()
    {
        $TreeBuilder = new TreeBuilder();
        $RootNode = $TreeBuilder->root( 'think_creative_sitemap' );

        $RootNode
            ->children()
                ->booleanNode( 'enabled' )
                    ->defaultTrue()
                ->end()

                ->arrayNode( 'listener' )
                    ->children()

                        ->booleanNode( 'enabled' )
                            ->defaultTrue()
                        ->end()

                        ->arrayNode( 'commands' )
                            ->prototype( 'scalar' )->end()
                        ->end()

                    ->end()
                ->end()

                ->arrayNode( 'siteaccess' )
                    ->children()

                        ->arrayNode( 'list' )
                            ->useAttributeAsKey( 'name' )
                            ->normalizeKeys( false )
                            ->prototype( 'scalar' )->end()
                        ->end()

                    ->end()
                ->end()
            ->end()

            ->children()
                ->arrayNode( 'urlset' )
                    ->children()

                        ->booleanNode( 'canonical_only' )
                            ->defaultTrue()
                        ->end()

                        ->arrayNode( 'content_type' )
                            ->children()

                                ->booleanNode( 'enabled' )
                                    ->defaultFalse()
                                ->end()

                                ->enumNode( 'filter_type' )
                                    ->values( array( 'include', 'exclude' ) )
                                    ->defaultValue( 'include' )
                                ->end()

                                ->arrayNode( 'list' )
                                    ->requiresAtLeastOneElement()
                                    ->prototype( 'scalar' )->end()
                                ->end()

                            ->end()
                        ->end()

                        ->arrayNode( 'priority' )
                            ->children()

                                ->booleanNode( 'enabled' )
                                    ->defaultFalse()
                                ->end()

                                ->floatNode( 'increment' )->end()
                                ->floatNode( 'max' )->end()
                                ->floatNode( 'min' )->end()

                            ->end()
                        ->end()

                    ->end()
                ->end()
            ->end()
        ;

        return $TreeBuilder;
    }

}
