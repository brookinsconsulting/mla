<?php

namespace ThinkCreative\SitemapBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use ThinkCreative\SitemapBundle\Services\SiteMapHandler;


class SitemapCommand extends ContainerAwareCommand
{

    public static function generateSiteMap( OutputInterface $output, SiteMapHandler $sitemap_handler, array $siteaccess_list = array() )
    {
        if(
            $sitemap_handler->isEnabled()
        ) {
            foreach( $siteaccess_list as $SiteAccessName )
            {
                if(
                    !$sitemap_handler->isValidSiteAccess( $SiteAccessName )
                ) {
                    $output->writeln( "<error> No matching domain for \"$SiteAccessName\" siteaccess </error>" );
                    continue;
                }

                $output->write( '<info>Generating "' . $SiteAccessName . '" Site Access Sitemap...</info>' );
                if(
                    !$sitemap_handler->setSiteAccess( $SiteAccessName, true )
                ) {
                    $output->writeln( '<error>failed!</error>' );
                    continue;
                }

                $sitemap_handler->generateSiteMap();

                $output->writeln( '<info>generated!</info>' );
            }
        }
    }

    protected function configure()
    {
        $this
            ->setName( 'sitemap:create' )
            ->setDescription( 'Create a XML site map file.' )
            ->addOption( 'default-only', null, InputOption::VALUE_NONE, 'Only generate the sitemap for the default site access.' )
        ;
    }

    protected function execute( InputInterface $input, OutputInterface $output )
    {
        $Container = $this->getContainer();

        $SiteAccessList = (array) $input->getOption( 'siteaccess' );
        $DefaultOnly = (bool) $input->getOption( 'default-only' );

        $SiteMapHandler = $Container->get( 'thinkcreative.sitemap_handler' );

        if( !$SiteAccessList )
        {
            $SiteAccessList = (
                $DefaultOnly ? (array) $SiteMapHandler->getCurrentSiteAccess() : $SiteMapHandler->getSiteAccessList()
            );
        }

        self::generateSiteMap( $output, $SiteMapHandler, $SiteAccessList );
    }

}
