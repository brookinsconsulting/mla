<?php

namespace ThinkCreative\SitemapBundle\Services;

class SiteMapHandler
{

    protected $Configurator;
    protected $Generator;
    protected $CacheHandler;

    public function __construct( SiteMapConfigurator $configurator, SiteMapGenerator $generator, SiteMapCacheHandler $cache_handler )
    {
        $this->Configurator = $configurator;
        $this->Generator = $generator;
        $this->CacheHandler = $cache_handler;
    }

    public function fetchSiteMap()
    {
        $SiteAccessName = $this->Configurator->getSiteAccessName();
        if(
            $this->CacheHandler->exists( $SiteAccessName )
        ) {
            return $this->CacheHandler->retrieve( $SiteAccessName );
        }
        return false;
    }

    public function generateSiteMap( $return = false )
    {
        $SiteMapContent = $this->Generator->generate( $this->Configurator->getParameters() );
        $this->CacheHandler->write(
            $this->Configurator->getSiteAccessName(), $SiteMapContent
        );

        if( $return )
        {
            return $this->fetchSiteMap();
        }
    }

    public function getCommandList()
    {
        return $this->Configurator->getCommandList();
    }

    public function getCurrentSiteAccess()
    {
        return $this->Configurator->getSiteAccessName();
    }

    public function getSiteAccessList()
    {
        return $this->Configurator->getSiteAccessList();
    }

    public function isEnabled( $listener = false )
    {
        return $this->Configurator->isEnabled( $listener );
    }

    public function isValidSiteAccess( $siteaccess )
    {
        return $this->Configurator->isValidSiteAccess( $siteaccess );
    }

    public function setSiteAccess( $siteaccess, $router_context = false )
    {
        if(
            $this->Configurator->setSiteAccess( $siteaccess )
        ) {
            if( $router_context )
            {
                $this->Generator->setRouterHost( $this->Configurator->getSiteAccessHost() );
            }
            return true;
        }
        return false;
    }

}
