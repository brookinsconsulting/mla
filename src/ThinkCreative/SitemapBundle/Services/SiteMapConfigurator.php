<?php

namespace ThinkCreative\SitemapBundle\Services;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use eZ\Publish\Core\MVC\Symfony\SiteAccess;
use eZ\Publish\Core\MVC\Symfony\SiteAccess\Router as SiteAccessRouter;
use eZ\Publish\Core\MVC\Symfony\Routing\SimplifiedRequest;
use eZ\Publish\Core\MVC\Symfony\MVCEvents;
use eZ\Publish\Core\MVC\Symfony\Event\PostSiteAccessMatchEvent;

class SiteMapConfigurator
{

    protected $EventDispatcher;
    protected $eZPublishConfigResolver;
    protected $SiteAccessRouter;
    protected $SiteAccess;
    protected $SiteAccessList;
    protected $Configuration;

    public function __construct(
        EventDispatcherInterface $event_dispatcher, ConfigResolverInterface $ezpublish_config_resolver, SiteAccessRouter $siteaccess_router, SiteAccess $siteaccess
    ) {
        $this->EventDispatcher = $event_dispatcher;
        $this->eZPublishConfigResolver = $ezpublish_config_resolver;
        $this->SiteAccessRouter = $siteaccess_router;
        $this->SiteAccess = $siteaccess;
    }

    public function getCommandList()
    {
        return $this->Configuration["listener"]["commands"];
    }

    public function getParameters()
    {
        $RootLocationID = $this->eZPublishConfigResolver->getParameter(
            'content.tree_root.location_id', null, $this->SiteAccess->name
        );

        $Parameters = new ParameterBag( $this->Configuration["urlset"] );
        $Parameters->set( 'root_location_id', $RootLocationID );

        return $Parameters;
    }

    public function getSiteAccessHost( $siteaccess = false )
    {
        $useSiteAccessName = (
            ( $siteaccess && $this->isValidSiteAccess( $siteaccess ) ) ? $siteaccess : $this->SiteAccess->name
        );
        return $this->SiteAccessList[ $useSiteAccessName ];
    }

    public function getSiteAccessList()
    {
        return array_keys( $this->SiteAccessList );
    }

    public function getSiteAccessName()
    {
        return $this->SiteAccess->name;
    }

    public function isEnabled( $listener = false )
    {
        return (
            $listener ? ( $this->Configuration["enabled"] && $this->Configuration["listener"]["enabled"] ) : $this->Configuration["enabled"]
        );
    }

    public function isValidSiteAccess( $siteaccess )
    {
        return array_key_exists( $siteaccess, $this->SiteAccessList );
    }

    public function processConfiguration( array $configuration, array $siteaccess_list )
    {
        $SiteAccessList = array_intersect(
            $siteaccess_list, array_keys( $configuration["siteaccess"]["list"] )
        );
        foreach( $SiteAccessList as $Key => $SiteAccessName )
        {
            unset( $SiteAccessList[ $Key ] );
            if(
                $SiteAccessHost = $configuration["siteaccess"]["list"][ $SiteAccessName ]
            ) {
                $SiteAccessList[ $SiteAccessName ] = $SiteAccessHost;
            }
        }

        $this->Configuration = $configuration;
        $this->SiteAccessList = $SiteAccessList;
    }

    public function setSiteAccess( $siteaccess )
    {
        $Request = Request::create( 'http://' . $this->getSiteAccessHost( $siteaccess ) . '/' );

        $this->SiteAccessRouter->setSiteAccess( null );

        $this->SiteAccess = $this->SiteAccessRouter->match(
            new SimplifiedRequest(
                array(
                    'scheme'      => $Request->getScheme(),
                    'host'        => $Request->getHost(),
                    'port'        => $Request->getPort(),
                    'pathinfo'    => $Request->getPathInfo(),
                    'queryParams' => $Request->query->all(),
                    'languages'   => $Request->getLanguages(),
                    'headers'     => $Request->headers->all()
                )
            )
        );

        $Request->attributes->set( 'siteaccess', $this->SiteAccess );

        $SiteAccessEvent = new PostSiteAccessMatchEvent(
            $this->SiteAccess, $Request, HttpKernelInterface::MASTER_REQUEST
        );
        $this->EventDispatcher->dispatch( MVCEvents::SITEACCESS, $SiteAccessEvent );

        return true;
    }

}
