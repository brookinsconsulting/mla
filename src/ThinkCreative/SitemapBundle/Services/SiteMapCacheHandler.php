<?php

namespace ThinkCreative\SitemapBundle\Services;

use Symfony\Component\Filesystem\Filesystem;

class SiteMapCacheHandler
{

    protected $FileSystem;
    protected $CacheDirectory;

    public function __construct( Filesystem $filesystem, $cache_directory )
    {
        $this->FileSystem = $filesystem;
        $this->CacheDirectory = $cache_directory;
    }

    public function exists( $siteaccess )
    {
        return $this->FileSystem->exists( $this->getFilePath( $siteaccess ) );
    }

    public function retrieve( $siteaccess )
    {
        return file_get_contents( $this->getFilePath( $siteaccess ) );
    }

    public function write( $siteaccess, $content )
    {
        $this->FileSystem->dumpFile(
            $this->getFilePath( $siteaccess ), $content, 0770
        );
    }

    protected function getFilePath( $siteaccess )
    {
        return "$this->CacheDirectory/sitemap/$siteaccess.xml";
    }

}
