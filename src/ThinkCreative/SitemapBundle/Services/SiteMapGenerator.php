<?php

namespace ThinkCreative\SitemapBundle\Services;

use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use eZ\Publish\API\Repository\Repository;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion\Operator;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;

class SiteMapGenerator
{

    protected $Router;
    protected $Templating;
    protected $eZPublishRepository;
    protected $Container;

    public function __construct( RouterInterface $router, EngineInterface $templating, Repository $ezpublish_api_repository )
    {
        $this->Router = $router;
        $this->Templating = $templating;
        $this->eZPublishRepository = $ezpublish_api_repository;
    }

    public function generate( ParameterBag $parameters )
    {
        $LocationService = $this->eZPublishRepository->getLocationService();

        $RootLocation = $LocationService->loadLocation( $parameters->get( 'root_location_id' ) );

        $Results = $this->eZPublishRepository->getSearchService()->findContent(
            $this->buildContentQuery( $RootLocation, $parameters )
        );

        $SiteMapResults = array();
        if( $Results->searchHits )
        {
            foreach( $Results->searchHits as $SiteMapHit )
            {
                $MainLocation = $LocationService->loadLocation( $SiteMapHit->valueObject->contentInfo->mainLocationId );

                $LocationList = (
                    $parameters->get( 'canonical_only' ) ? array( $MainLocation ) : $LocationService->loadLocations( $SiteMapHit->valueObject->contentInfo )
                );

                foreach( $LocationList as $Location )
                {
                    $ResultItem = array(
                        "content" => $SiteMapHit->valueObject,
                        "location" => $Location,
                    );

                    $Priority = $parameters->get( 'priority' );
                    if( $Priority["enabled"] )
                    {
                        $ResultItem["priority"] = $this->calculatePriority(
                            $Priority, ( $Location->depth - $RootLocation->depth )
                        );
                    }

                    $SiteMapResults[] = $ResultItem;
                }
            }
        }

        return $SiteMapContent = $this->Templating->render(
            'ThinkCreativeSitemapBundle::index.xml.twig', array( "root_location" => $RootLocation, "urlset" => $SiteMapResults )
        );
    }

    public function setRouterHost( $host )
    {
        return $this->Router->getContext()->setHost( $host );
    }

    protected function buildContentQuery( $root_location, $parameters )
    {
        $Criteria = array(
            new Criterion\Subtree( $root_location->pathString ),
            new Criterion\Visibility( Criterion\Visibility::VISIBLE ),
        );

        $ContentTypeParameters = $parameters->get( 'content_type' );
        if( $ContentTypeParameters["enabled"] )
        {
            $ContentTypeCriterion = new Criterion\ContentTypeIdentifier( $ContentTypeParameters["list"] );
            $Criteria[] = (
                $ContentTypeParameters["filter_type"] === 'include' ? $ContentTypeCriterion : new Criterion\LogicalNot( $ContentTypeCriterion )
            );
        }

        return new Query(
            array(
                'criterion' => new Criterion\LogicalAnd( $Criteria ),
            )
        );
    }

    protected function calculatePriority( $parameters, $factor )
    {
        $Value = (
            $parameters["max"] - ( $parameters["increment"] * $factor )
        );
        return (
            $Value >= $parameters["min"] ? $Value : $parameters["min"]
        );
    }

}
