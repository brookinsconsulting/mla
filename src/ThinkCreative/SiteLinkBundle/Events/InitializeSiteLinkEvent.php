<?php

namespace ThinkCreative\SiteLinkBundle\Events;

use Symfony\Component\EventDispatcher\Event;
use ThinkCreative\SiteLinkBundle\Services\SiteLink;

class InitializeSiteLinkEvent extends Event
{

	protected $SiteLink;

	public function __construct(SiteLink $sitelink) {
		$this->SiteLink = $sitelink;
	}

	public function getSiteLink() {
		return $this->SiteLink;
	}

}
