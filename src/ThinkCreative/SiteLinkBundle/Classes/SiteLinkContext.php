<?php

namespace ThinkCreative\SiteLinkBundle\Classes;

use Symfony\Component\Routing\RequestContext;

use eZ\Publish\API\Repository\Values\Content\Content;
use eZ\Publish\API\Repository\Values\Content\Location;

class SiteLinkContext
{

	public $Content;
	public $Location;
	public $URIComponents;
	public $SiteAccessName;
	public $isSiteAccessRootLocation = false;

	public function __construct(RequestContext $request_context, Content $content = null, Location $location = null) {
		$this->Content = $content;
		$this->Location = $location;
		$this->URIComponents = array(
			"scheme" => $request_context->getScheme(),
			"host" => $request_context->getHost(),
			"path" => "",
			"query" => "",
			"fragment" => "",
		);
	}

	public function getFragment($symbol = false) {
		if(
			$Fragment = $this->URIComponents["fragment"]
		) {
			return (
				$symbol ? "#$Fragment" : $Fragment
			);
		}
		return '';
	}

	public function getHost() {
		return $this->URIComponents["host"];
	}

	public function getPath() {
		return $this->URIComponents["path"];
	}

	public function getQueryString($symbol = false) {
		if(
			$QueryString = $this->URIComponents["query"]
		) {
			return $symbol ? "?$QueryString" : $QueryString;
		}
		return '';
	}

	public function getScheme() {
		return $this->URIComponents["scheme"];
	}

	public function setFragment($value) {
		if(
			is_string($value)
		) {
			$this->URIComponents["fragment"] = $value;
			return true;
		}
		return false;
	}

	public function hasLocation() {
		return isset($this->Location) && $this->Location instanceof Location;
	}

	public function setHost($value) {
		if(
			is_string($value)
		) {
			$this->URIComponents["host"] = $value;
			return true;
		}
		return false;
	}

	public function setPath($value) {
		if(
			is_string($value)
		) {
			$this->URIComponents["path"] = $value;
			return true;
		}
		return false;
	}

	public function setQuery($value) {
		if($value) {
			$this->URIComponents["query"] = (
				is_array($value) ? http_build_query($value) : $value
			);
			return true;
		}
		return false;
	}

	public function setValue($value) {
		if($value instanceof SiteLinkContext) {
			$this->URIComponents = $value->URIComponents;
			return;
		}

		foreach(
			parse_url($value) as $Part => $Value
		) {
			$this->URIComponents[$Part] = $Value;
		}
	}

}
