<?php

namespace ThinkCreative\SiteLinkBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use ThinkCreative\SiteLinkBundle\DependencyInjection\Compiler\SiteLinkCompilerPass;

class ThinkCreativeSiteLinkBundle extends Bundle
{

	protected $name = "ThinkCreativeSiteLinkBundle";

	public function build(ContainerBuilder $container) {
		parent::build($container);

		$container->addCompilerPass(
			new SiteLinkCompilerPass()
		);
	}

}
