<?php

namespace ThinkCreative\SiteLinkBundle\Twig;

class SiteLinkExtension extends \Twig_Extension
{

	protected $SiteLink;

	public function __construct($sitelink) {
		$this->SiteLink = $sitelink;
	}

	public function getFunctions() {
		return array(
			'sitelink' => new \Twig_Function_Method(
				$this, 'generateSiteLink', array(
					'is_safe' => array('html'),
				)
			),
		);
	}

	public function generateSiteLink($value, $is_location = true, $absolute = null, $quote = true, $fragment = '', $query = '') {
		$Context = $this->SiteLink->generate(
			$value, $is_location
		);

		$Context->setFragment($fragment);
		$Context->setQuery($query);

		$Hyperlink = $this->SiteLink->buildHyperlink(
			$Context, $absolute
		);

		return (
			$quote ? "\"$Hyperlink\"" : $Hyperlink
		);
	}

	public function getName() {
		return 'sitelink';
	}

}