<?php

namespace ThinkCreative\SiteLinkBundle\Routing;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use eZ\Publish\API\Repository\Repository as eZPublishRepository;
use eZ\Publish\Core\MVC\Symfony\Routing\Generator\UrlAliasGenerator;
use eZ\Publish\API\Repository\Values\Content\Content;
use eZ\Publish\API\Repository\Values\Content\Location;

use ThinkCreative\SiteLinkBundle\DependencyInjection\SiteLinkFieldTypeHandlerInterface;
use ThinkCreative\SiteLinkBundle\Classes\SiteLinkContext;
use ThinkCreative\SiteLinkBundle\Services\SiteLink;

class SiteLinkEngine
{

	protected $CurrentRootLocation;

	public function __construct(eZPublishRepository $ezpublish_api_repository, UrlAliasGenerator $ezpublish_urlalias_generator) {
		$this->Generator = $ezpublish_urlalias_generator;

		$this->LocationService = $ezpublish_api_repository->getLocationService();
		$this->ContentService = $ezpublish_api_repository->getContentService();
		$this->ContentTypeService = $ezpublish_api_repository->getContentTypeService();
		$this->URLAliasService = $ezpublish_api_repository->getURLAliasService();
	}

	public function generate(SiteLink $service, SiteLinkContext $context) {
		// get the content type of the location
		$ContentType = $this->ContentTypeService->loadContentType(
			$context->Content->contentInfo->contentTypeId
		);

		if(
			$ContentTypeFields = $service->getContentTypeFields(
				$ContentType->identifier
			)
		) {
			foreach(
				$ContentTypeFields as $FieldIdentifier
			) {
				if(
					$service->processFieldType(
						$context, $ContentType->getFieldDefinition($FieldIdentifier), $FieldIdentifier
					)
				) {
					return;
				}
			}
		}

		if(
			$service->isMultiSite()
		) {
			$LocationList = (
				$context->hasLocation() ? array($context->Location) : $context->Location
			);

			foreach(
				$LocationList as $Location
			) {
				if(
					$service->processSiteAccessList(
						$context, $Location
					)
				) {
					break;
				}
			}

			// if Location is a siteaccess root location, short circut processing the Location path
			$LocationURLAliasPath =  '/';
			if(
				!$context->isSiteAccessRootLocation
			) {
				$LocationURLAliasPath = $service->removeSiteAccessPathPrefix(
					$context->SiteAccessName, $this->URLAliasService->reverseLookup($context->Location)->path
				);
			}

			return $context->setValue($LocationURLAliasPath);
		}

		return $context->setValue(
			$this->Generator->generate(
				$context->Location, array()
			)
		);
	}

	public function loadContentLocation(SiteLinkContext $context, $value, $is_location = true, $use_main_location = false) {
		$isContentLocationID = is_numeric($value) || is_integer($value);

		if(
			($isContentLocationID && $is_location) || $value instanceof Location
		) {
			$context->Location = (
				$isContentLocationID ? $this->LocationService->loadLocation($value) : $value
			);
			$isContentLocationID = true;
			$value = $context->Location->contentInfo->id;
		}

		$context->Content = (
			$isContentLocationID ? $this->ContentService->loadContent($value) : $value
		);

		if(
			!$context->hasLocation()
		) {
			try {
				$context->Location = $this->loadLocationFromContent(
					$context->Content, $use_main_location
				);
			} catch (\eZ\Publish\Core\Base\Exceptions\BadStateException $e) {
				// @TODO find a way to invalidate the context when an invalid location is found
				$context->Location = $this->LocationService->loadLocation(2);
			} catch (\eZ\Publish\Core\Base\Exceptions\NotFoundException $e) {
				$context->location = null;
			}
		}
	}

	public function loadLocationFromContent(Content $content, $use_main_location = false) {
		if($use_main_location) {
			// use content main location
			return $this->LocationService->loadLocation(
				$content->contentInfo->mainLocationId
			);
		}

		// check current siteaccess for all matching content locations
		if(
			$LocationList = $this->LocationService->loadLocations(
				$content->contentInfo, $this->CurrentRootLocation
			)
		) {
			// loop through all available content locations and return first non-hidden location
			foreach($LocationList as $Location) {
				if(!$Location->hidden) {
					return array($Location);
				}
			}
		}

		return $this->LocationService->loadLocations($content->contentInfo);
	}

	public function setCurrentRootLocation($id) {
		if(
			is_integer($id)
		) {
			$this->CurrentRootLocation = $this->LocationService->loadLocation($id);
			return true;
		}
		return false;
	}

}
