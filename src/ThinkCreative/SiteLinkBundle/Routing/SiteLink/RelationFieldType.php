<?php

namespace ThinkCreative\SiteLinkBundle\Routing\SiteLink;

use eZ\Publish\API\Repository\Values\Content\Field;
use eZ\Publish\API\Repository\Values\ContentType\FieldDefinition;
use ThinkCreative\SiteLinkBundle\DependencyInjection\SiteLinkFieldTypeHandlerInterface;
use ThinkCreative\SiteLinkBundle\Classes\SiteLinkContext;

class RelationFieldType implements SiteLinkFieldTypeHandlerInterface
{

	protected $SiteLinkService;

	public function __construct($sitelink) {
		$this->SiteLinkService = $sitelink;
	}

	public function process(SiteLinkContext $context, FieldDefinition $field_definition, Field $field) {
		if(
			$RelationLink = $this->SiteLinkService->generate(
				(string) $field->value, false
			)
		) {
			$context->setValue($RelationLink);
			return true;
		}
		return false;
	}

	public function supports($identifier) {
		return in_array(
			$identifier, array('ezobjectrelation')
		);
	}

}

