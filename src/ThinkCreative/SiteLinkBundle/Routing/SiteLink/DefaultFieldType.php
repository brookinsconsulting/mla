<?php

namespace ThinkCreative\SiteLinkBundle\Routing\SiteLink;

use eZ\Publish\API\Repository\Values\Content\Field;
use eZ\Publish\API\Repository\Values\ContentType\FieldDefinition;
use ThinkCreative\SiteLinkBundle\DependencyInjection\SiteLinkFieldTypeHandlerInterface;
use ThinkCreative\SiteLinkBundle\Classes\SiteLinkContext;

class DefaultFieldType implements SiteLinkFieldTypeHandlerInterface
{

	public function process(SiteLinkContext $context, FieldDefinition $field_definition, Field $field) {
		if(
			$Value = (string) $field->value
		) {
			$context->setValue($Value);
			return true;
		}
		return false;
	}

	public function supports($identifier) {
		return in_array(
			$identifier, array('ezstring', 'ezurl')
		);
	}

}

