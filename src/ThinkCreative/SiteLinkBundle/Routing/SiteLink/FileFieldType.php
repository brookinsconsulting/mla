<?php

namespace ThinkCreative\SiteLinkBundle\Routing\SiteLink;

use eZ\Publish\API\Repository\Values\Content\Field;
use eZ\Publish\API\Repository\Values\ContentType\FieldDefinition;
use ThinkCreative\SiteLinkBundle\DependencyInjection\SiteLinkFieldTypeHandlerInterface;
use ThinkCreative\SiteLinkBundle\Classes\SiteLinkContext;

class FileFieldType implements SiteLinkFieldTypeHandlerInterface
{

    public function process(SiteLinkContext $context, FieldDefinition $field_definition, Field $field) {
        if($field->value) {
            $context->setValue(
                '/content/download/' . $context->Content->id . "/$field->id/" . $field->value->fileName
            );
            return true;
        }
        return false;
    }

    public function supports($identifier) {
        return in_array(
            $identifier, array('ezbinaryfile')
        );
    }

}

