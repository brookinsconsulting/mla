<?php

namespace ThinkCreative\SiteLinkBundle\DependencyInjection\Builder;

class ContentTypeNode extends BaseNode
{

	protected $Identifier;
	protected $isSelfLinking;
	protected $FieldTypeList;

	public function __construct($identifier, $parent = null) {
		parent::__construct(
			'content_type_list', $parent
		);

		$this->Identifier = $identifier;
		$this->isSelfLinking = true;
		$this->FieldTypeList = array();
	}

	public function addFieldType() {
		foreach(
			func_get_args() as $fieldtype
		) {
			if(
				is_string($fieldtype)
			) {
				$this->FieldTypeList[] = $fieldtype;
			}
		}
		return $this;
	}

	public function getConfiguration() {
		$Configuration = array(
			$this->Identifier => array(
				'self_linking' => $this->isSelfLinking,
			),
		);

		if($this->FieldTypeList) {
			$Configuration[$this->Identifier]["fieldtype_list"] = $this->FieldTypeList;
		}

		return $Configuration;
	}

	public function isSelfLinking($value) {
		if(
			is_bool($value)
		) {
			$this->isSelfLinking = $value;
		}
		return $this;
	}

}
