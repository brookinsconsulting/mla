<?php

namespace ThinkCreative\SiteLinkBundle\DependencyInjection\Builder;

class FieldTypeIdentifierNode extends BaseNode
{

	protected $Value;

	public function __construct($value, $parent = null) {
		parent::__construct(
			'fieldtype_identifier', $parent
		);

		$this->Value = $value;
	}

	public function getConfiguration() {
		return $this->Value;
	}

}
