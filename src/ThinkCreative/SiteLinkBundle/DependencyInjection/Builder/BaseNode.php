<?php

namespace ThinkCreative\SiteLinkBundle\DependencyInjection\Builder;

use ThinkCreative\SiteLinkBundle\DependencyInjection\SiteLinkConfiguration;

abstract class BaseNode
{

	protected $Name;
	protected $Parent;

	public function __construct($name, SiteLinkConfiguration $parent = null) {
		$this->Name = $name;
		$this->Parent = $parent;
	}

	public function end() {
		$this->Parent->apply();
		return $this->Parent;
	}

	public function getName() {
		return $this->Name;
	}

	abstract public function getConfiguration();

}
