<?php

namespace ThinkCreative\SiteLinkBundle\DependencyInjection\Builder;

class HostOverrideNode extends BaseNode
{

	protected $SiteAccess;
	protected $Host;

	public function __construct($siteaccess, $host, $parent = null) {
		parent::__construct(
			'host_override', $parent
		);

		$this->SiteAccess = $siteaccess;
		$this->Host = $host;
	}

	public function getConfiguration() {
		return array(
			$this->SiteAccess => $this->Host
		);
	}

}
