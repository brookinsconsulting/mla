<?php

namespace ThinkCreative\SiteLinkBundle\DependencyInjection\Builder;

class ForceAbsoluteNode extends BaseNode
{

	protected $Value;

	public function __construct($value, $parent = null) {
		parent::__construct(
			'force_absolute', $parent
		);

		$this->Value = $value;
	}

	public function getConfiguration() {
		return $this->Value;
	}

}
