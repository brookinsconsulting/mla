<?php

namespace ThinkCreative\SiteLinkBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class SiteLinkCompilerPass implements CompilerPassInterface
{

	public function process(ContainerBuilder $container) {

		if(
			$container->hasDefinition('thinkcreative.sitelink')
		) {
			$SiteLink = $container->getDefinition('thinkcreative.sitelink');
			$FieldTypeHandlers = $container->findTaggedServiceIds('thinkcreative.sitelink.fieldtype_handler');

			foreach ($FieldTypeHandlers as $ID => $Attributes) {
				$SiteLink->addMethodCall(
					'addFieldTypeHandler', array( new Reference($ID) )
				);
			}
		}

	}

}
