<?php

namespace ThinkCreative\SiteLinkBundle\DependencyInjection;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\Processor;

use ThinkCreative\SiteLinkBundle\DependencyInjection\Builder;

class SiteLinkConfiguration implements ConfigurationInterface
{

	const TYPE_EXTENSION = 0;
	const TYPE_CONFIGURATION = 1;

	private $isResolved = false;
	private $Configuration;
	private $TreeType;

	private $NodeBuilder;

	public function __construct(array $configurations = array(), $tree = self::TYPE_CONFIGURATION) {
		$this->Configuration = $configurations;
		$this->TreeType = $tree;
	}

	public function addContentType($identifier) {
		return $this->NodeBuilder = new Builder\ContentTypeNode(
			$identifier, $this
		);
	}

	public function addHostOverride($siteaccess, $host) {
		return $this->NodeBuilder = new Builder\HostOverrideNode(
			$siteaccess, $host, $this
		);
	}

	public function append(array $configuration) {
		if(!$this->isResolved) {
			$this->Configuration[] = $configuration;
			return true;
		}
		return false;
	}

	public function apply() {
		if(
			$this->NodeBuilder && $this->NodeBuilder instanceof Builder\BaseNode
		) {
			$Builder = $this->NodeBuilder;
			$this->append(
				array(
					$Builder->getName() => $Builder->getConfiguration()
				)
			);
			$this->NodeBuilder = null;
		}
	}

	public function forceAbsolute($value) {
		return $this->NodeBuilder = new Builder\ForceAbsoluteNode(
			$value, $this
		);
	}

	public function getConfigTreeBuilder() {
		$TreeBuilder = new TreeBuilder();
		$RootNode = $TreeBuilder->root(
			$this->TreeType === self::TYPE_EXTENSION ? 'think_creative_site_link' : 'sitelink'
		);

		$RootNode
			->children()
				->booleanNode('force_absolute')
					->defaultFalse()
				->end()

				->arrayNode('host_override')
					->useAttributeAsKey('name')
					->prototype('scalar')->end()
					->beforeNormalization()
						->ifString()->then( function() { return array(); } )
					->end()
				->end()

				->scalarNode('fieldtype_identifier')
					->defaultValue('')
				->end()

				->arrayNode('content_type_list')
					->useAttributeAsKey('name')
					->prototype('array')
						->children()
							->booleanNode('self_linking')
								->defaultTrue()
							->end()

							->arrayNode('fieldtype_list')
								->requiresAtLeastOneElement()
								->prototype('scalar')->end()
							->end()
						->end()
					->end()
				->end()
		;

		if(
			$this->TreeType === self::TYPE_CONFIGURATION
		) {
			$RootNode
				->children()

					->booleanNode('is_multisite')
						->defaultFalse()
					->end()

					->integerNode('current_root_location_id')->end()

					->arrayNode('root_location_id_list')
						->useAttributeAsKey('name')
						->prototype('integer')->end()
					->end()

					->arrayNode('siteaccess_host_mapper')
						->useAttributeAsKey('name')
						->prototype('scalar')->end()
					->end()

					->arrayNode('excluded_uri_prefixes_list')
						->useAttributeAsKey('name')
						->prototype('array')
							->prototype('scalar')->end()
						->end()
					->end()

				->end()
			;
		}

		return $TreeBuilder;
	}

	public function isResolved() {
		return $this->isResolved;
	}

	public function merge(array $configurations) {
		foreach($configurations as $Configuration) {
			if(
				!$this->append($Configuration)
			) {
				return false;
			}
		}
		return true;
	}

	public function resolve() {
		if(!$this->isResolved) {
			$Processor = new Processor();

			$Configuration = $Processor->processConfiguration(
				$this, $this->Configuration
			);
			$this->isResolved = true;

			return $Configuration;
		}
		return false;
	}

	public function setFieldTypeIdentifier($value) {
		return $this->NodeBuilder = new Builder\FieldTypeIdentifierNode(
			$value, $this
		);
	}

}
