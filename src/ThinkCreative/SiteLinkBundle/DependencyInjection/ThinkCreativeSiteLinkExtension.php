<?php

namespace ThinkCreative\SiteLinkBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Yaml\Yaml;

class ThinkCreativeSiteLinkExtension extends Extension
{

	public function load(array $configurations, ContainerBuilder $container) {
		$YamlFileLoader = new YamlFileLoader(
			$container, new FileLocator(__DIR__ . '/../Resources/config')
		);
		$YamlFileLoader->load('services.yml');

		$Configuration = new SiteLinkConfiguration(
			$configurations, SiteLinkConfiguration::TYPE_EXTENSION
		);

		$container->setParameter(
			'thinkcreative.sitelink.configuration', $Configuration->resolve()
		);
	}

	public function getAlias() {
		return 'think_creative_site_link';
	}

}
