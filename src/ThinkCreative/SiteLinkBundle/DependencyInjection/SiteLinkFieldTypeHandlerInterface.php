<?php

namespace ThinkCreative\SiteLinkBundle\DependencyInjection;

use eZ\Publish\API\Repository\Values\Content\Field;
use eZ\Publish\API\Repository\Values\ContentType\FieldDefinition;
use ThinkCreative\SiteLinkBundle\Classes\SiteLinkContext;

interface SiteLinkFieldTypeHandlerInterface
{

	public function process(SiteLinkContext $context, FieldDefinition $field_definition, Field $field);

	public function supports($identifier);

}
