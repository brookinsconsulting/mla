<?php

namespace ThinkCreative\SiteLinkBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\HttpKernelInterface;

use eZ\Publish\Core\MVC\Symfony\MVCEvents as eZPublishEvents;
use eZ\Publish\Core\MVC\Symfony\Event\PostSiteAccessMatchEvent;

use ThinkCreative\SiteLinkBundle\Services\SiteLink;
use ThinkCreative\SiteLinkBundle\Events\SiteLinkEvents;
use ThinkCreative\SiteLinkBundle\Events\InitializeSiteLinkEvent;

class SiteLinkListener implements EventSubscriberInterface
{

	protected $Container;
	protected $EventDispatcher;

	public function __construct(ContainerInterface $container) {
		$this->Container = $container;
		$this->EventDispatcher = $container->get('event_dispatcher');
	}

	public static function getSubscribedEvents() {
		return array(
			eZPublishEvents::SITEACCESS => array('onSiteAccessMatch'),
		);
	}

	public function loadSiteLinkConfiguration($siteaccess) {
		$SiteLink = $this->Container->get('thinkcreative.sitelink');
		$Configuration = $SiteLink->getConfiguration();

		if(
			$this->Container->hasParameter('thinkcreative.sitelink.configuration')
		) {
			$Configuration->append(
				$this->Container->getParameter('thinkcreative.sitelink.configuration')
			);
		}

		$eZPublishConfigResolver = $this->Container->get('ezpublish.config.resolver');
		foreach(
			$this->Container->getParameter('ezpublish.siteaccess.list') as $SiteAccessName
		) {
			$RootLocationIDList[$SiteAccessName] = $eZPublishConfigResolver->getParameter(
				'content.tree_root.location_id', null, $SiteAccessName
			);
			$ExcludedURIPrefixesList[$SiteAccessName] = $eZPublishConfigResolver->getParameter(
				'content.tree_root.excluded_uri_prefixes', null, $SiteAccessName
			);
		}

		$SiteAccessMatchConfiguration = $this->Container->getParameter('ezpublish.siteaccess.match_config');
		$Configuration->append(
			array(
				'is_multisite' => $isMultisite = $siteaccess->matchingType === 'host:map',
				'current_root_location_id' => $eZPublishConfigResolver->getParameter('content.tree_root.location_id'),
				'root_location_id_list' => $RootLocationIDList,
				'siteaccess_host_mapper' => (
					$isMultisite ? $SiteAccessMatchConfiguration['Map\Host'] : array()
				),
				'excluded_uri_prefixes_list' => $ExcludedURIPrefixesList,
			)
		);

		$this->EventDispatcher->dispatch(
			SiteLinkEvents::INITIALIZE_SITELINK, new InitializeSiteLinkEvent($SiteLink)
		);
	}

	public function onSiteAccessMatch(PostSiteAccessMatchEvent $event) {
		$this->loadSiteLinkConfiguration(
			$event->getSiteAccess()
		);

		$this->EventDispatcher->removeListener(
			eZPublishEvents::SITEACCESS, array($this, 'onSiteAccessMatch')
		);

		$this->Container->get('thinkcreative.sitelink')->processConfiguration();
	}

}
