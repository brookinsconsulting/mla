<?php

namespace ThinkCreative\SiteLinkBundle\Services;

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\DependencyInjection\ParameterBag\FrozenParameterBag;

use eZ\Publish\API\Repository\Repository as eZPublishRepository;
use eZ\Publish\Core\Repository\Values\Content\Location;
use eZ\Publish\Core\MVC\Symfony\Routing\Generator\UrlAliasGenerator;

use ThinkCreative\SiteLinkBundle\DependencyInjection\SiteLinkConfiguration;
use ThinkCreative\SiteLinkBundle\DependencyInjection\SiteLinkFieldTypeHandlerInterface;
use ThinkCreative\SiteLinkBundle\Classes\SiteLinkContext;

class SiteLink
{

	protected $GeneratorEngine;
	protected $FieldTypeHandlers;
	protected $Parameters;
	protected $ContentTypeList;

	private $RequestContext;
	private $Configuration;

	private $isMultiSite = false;
	private $useAbsoluteURL = false;

	public function __construct(RequestContext $request_context) {
		$this->RequestContext = $request_context;

		$this->FieldTypeHandlers = array();
		$this->ContentTypeList = array();

		$this->Configuration = new SiteLinkConfiguration();
	}

	public function addFieldTypeHandler(SiteLinkFieldTypeHandlerInterface $handler) {
		$this->FieldTypeHandlers[] = $handler;
		return true;
	}

	public function buildHyperlink(SiteLinkContext $context, $absolute = null) {
		$Hyperlink = $context->getPath();
		$ContextHost = $context->getHost();

		if(
			$absolute || ($absolute === null && $this->useAbsoluteURL) || !$this->isCurrentHost($ContextHost)
		) {
			$Hyperlink = $context->getScheme() . "://$ContextHost$Hyperlink";
		}

		return $Hyperlink . $context->getQueryString(true) . $context->getFragment(true);
	}

	public function generate($value, $is_location = true) {
		if($value) {
			$Context = new SiteLinkContext(
				$this->RequestContext
			);

			if(
				is_string($value) && !is_numeric($value)
			) {
				$Context->setValue($value);
				return $Context;
			}

			$this->GeneratorEngine->loadContentLocation(
				$Context, $value, $is_location, !$this->isMultiSite()
			);

			$this->GeneratorEngine->generate(
				$this, $Context
			);

			return $Context;
		}
		return false;
	}

	public function getConfiguration() {
		return $this->Configuration;
	}

	public function getContentTypeFields($identifier) {
		return (
			isset($this->ContentTypeList[$identifier]) ? $this->ContentTypeList[$identifier]['fieldtype_list'] : false
		);
	}

	public function isCurrentHost($host) {
		return $this->RequestContext->getHost() === $host;
	}

	public function isMultiSite() {
		return $this->isMultiSite;
	}

	public function processConfiguration() {
		$Configuration = $this->Configuration->resolve();

		if(
			$Configuration['host_override'] && is_array($Configuration['host_override'])
		) {
			$HostOverride = $Configuration['host_override'];
			array_walk(
				$Configuration['siteaccess_host_mapper'],
				function($siteaccess, $host) use ($Configuration) {
					if(
						isset($HostOverride[$siteaccess]) && $HostOverride[$siteaccess] !== $host
					) {
						unset($Configuration['siteaccess_host_mapper'][$host]);
					}
				}
			);
		}

		$this->isMultiSite = $Configuration['is_multisite'];
		$this->useAbsoluteURL = $Configuration['force_absolute'];

		$this->GeneratorEngine->setCurrentRootLocation(
			$Configuration['current_root_location_id']
		);

                if (!$Configuration['content_type_list']) { return; }

		foreach(
			$Configuration['content_type_list'] as $Identifier => $Options
		) {
			// sets the default fieldtype identifier to be the first item in the "fieldtype_list"
			if(
				$Configuration['fieldtype_identifier'] &&
				($Position = array_search(
					$Configuration['fieldtype_identifier'], $Options['fieldtype_list']
				)) !== 0
			) {
				if($Position !== false) {
					array_splice($Options['fieldtype_list'], $Position, 1);
				}
				array_unshift(
					$Options['fieldtype_list'], $Configuration['fieldtype_identifier']
				);
			}
			$this->ContentTypeList[$Identifier] = $Configuration['content_type_list'][$Identifier] = $Options;
		}

		$this->Parameters = new FrozenParameterBag(
			array(
				'root_location_id_list' => $Configuration['root_location_id_list'],
				'siteaccess_host_mapper' => $Configuration['siteaccess_host_mapper'],
				'excluded_uri_prefixes_list' => $Configuration['excluded_uri_prefixes_list'],
			)
		);
	}

	public function processFieldType(SiteLinkContext $context, $field_definition, $field_identifier) {
		if($field_definition) {
			foreach(
				$this->FieldTypeHandlers as $FieldTypeHandler
			) {
				if(
					$FieldTypeHandler->supports(
						$field_definition->fieldTypeIdentifier
					)
				) {
					if(
						$FieldTypeHandler->process(
							$context, $field_definition, $context->Content->getField($field_identifier)
						)
					) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public function processSiteAccessList(SiteLinkContext $context, Location $location) {
		$SiteAccessRootLocationIDList = $this->Parameters->get("root_location_id_list");

		$CurrentRequestHost = $this->RequestContext->getHost();
		foreach(
			$SiteAccessRootLocationIDList as $SiteAccessName => $SiteAccessRootLocationID
		) {

			// check if Location is a siteaccess root location, otherwise search Location path for siteaccess root location id
			if(
				($isSiteAccessRootLocation = $SiteAccessRootLocationID === $location->id) || array_search($SiteAccessRootLocationID, $location->path) !== false
			) {
				$context->Location = $location;
				$context->SiteAccessName = $SiteAccessName;
				$context->isSiteAccessRootLocation = $isSiteAccessRootLocation;

				// when a matching siteaccess is found, get the set of avaiable host matches
				$SiteAccessHostList = array_keys(
					$this->Parameters->get("siteaccess_host_mapper"), $SiteAccessName
				);

				$MatchRelevance = 0;
				$SiteAccessHostMatch = '';
				foreach($SiteAccessHostList as $SiteAccessHost) {
					$IterativeRelevance = similar_text(
						$CurrentRequestHost, $SiteAccessHost
					);
					if($IterativeRelevance > $MatchRelevance) {
						$MatchRelevance = $IterativeRelevance;
						$SiteAccessHostMatch = $SiteAccessHost;
					}
				}

				if(
					$SiteAccessHostMatch && $context->getHost() !== $SiteAccessHostMatch
				) {
					$context->setHost($SiteAccessHostMatch);
				}

				return true;
			}
		}

		return false;
	}

	public function removeSiteAccessPathPrefix($siteaccess_name, $path) {
		$ExcludedURIPrefixesList = $this->Parameters->get('excluded_uri_prefixes_list');
		if (is_array($ExcludedURIPrefixesList) && isset($ExcludedURIPrefixesList[$siteaccess_name])) {
		    foreach(
    			$ExcludedURIPrefixesList[$siteaccess_name] as $ExcludedPrefix
    		) {
    			$ExcludedPrefix = '/' . trim($ExcludedPrefix, '/');
    			if(
    				mb_stripos($path, $ExcludedPrefix) === 0
    			) {
    				return str_replace($ExcludedPrefix . '/', '/', $path);
    			}
    		}
		}
		return $path;
	}

	public function setGenerator($engine, eZPublishRepository $ezpublish_api_repository, UrlAliasGenerator $ezpublish_urlalias_generator) {
		if(
			class_exists($engine)
		) {
			$this->GeneratorEngine = new $engine(
				$ezpublish_api_repository, $ezpublish_urlalias_generator
			);
			return true;
		}
		return false;
	}

}
