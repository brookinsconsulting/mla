<?php
namespace ThinkCreative\SiteLinkBundle\Services;

use eZ\Publish\Core\FieldType\XmlText\Converter;
use ThinkCreative\SiteLinkBundle\Services\SiteLink;

class eZXMLTextLinkPreConverter implements Converter
{

	protected $SiteLink;

	public function __construct(SiteLink $sitelink) {
		$this->SiteLink = $sitelink;
	}

	public function convert(\DOMDocument $xml) {
		foreach(
			$xml->getElementsByTagName("link") as $Link
		) {
			if(
				($LinkData = $this->getLinkData($Link)) !== false
			) {

				try {
					$Context = $this->SiteLink->generate(
						$LinkData["id"], $LinkData["is_location"]
					);
				} catch( \Exception $e ) {
					$Link->setAttribute( "url", "/Oops" );
					continue;
				}

				if(
					$Link->hasAttribute("anchor_name")
				) {
					$Context->setFragment(
						$Link->getAttribute("anchor_name")
					);
				}

				$Link->setAttribute(
					"url", $this->SiteLink->buildHyperlink($Context)
				);
			}
		}
	}

	protected function getLinkData(\DOMElement $link) {
		if(
			($isContentID = $link->hasAttribute("object_id")) || $link->hasAttribute("node_id")
		) {
			return array(
				"id" => $link->getAttribute(
					$isContentID ? "object_id" : "node_id"
				),
				"is_location" => !$isContentID,
			);
		}
		return false;
	}

}
