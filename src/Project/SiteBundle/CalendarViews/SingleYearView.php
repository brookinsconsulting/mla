<?php

namespace Project\SiteBundle\CalendarViews;
use ThinkCreative\CalendarBundle\CalendarViews\ListYearView;

/**
 * Displays events of a single year
 */
class SingleYearView extends ListYearView
{

    /**
     * {@inheritdoc}
     **/
    public function startDate()
    {
        if (date("Y", $this->date) == date("Y", time())) {
            return time();
        }
        return mktime(
            0,
            0,
            0,
            1,
            1,
            date("Y", $this->date)
        );
    }

    /**
     * {@inheritdoc}
     **/
    public function nextPage()
    {
        $offset = $this->getOffset();
        if ($this->totalCount == 0 || $offset + $this->limit() >= $this->totalCount ) return array();
        return array(
            "offset" => $offset + $this->limit()
        );
    }

    /**
     * {@inheritdoc}
     **/
    public function prevPage()
    {
        $offset = $this->getOffset();
        if ($offset == 0) return array();

        return array(
            "offset" => ($offset - $this->limit() < 0 ? 0 : $offset - $this->limit())
        );
    }
}
