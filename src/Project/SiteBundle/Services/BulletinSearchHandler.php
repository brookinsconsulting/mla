<?php

namespace Project\SiteBundle\Services;

use Symfony\Component\HttpFoundation\ParameterBag;

class BulletinSearchHandler
{

    protected $SearchService;
    protected $eZPublishAPIRespository;
    protected $TagsService;
    protected $DefaultPaginationParameters;

    public function __construct( $search_service, $ezpublish_api_repository, $tags_service ) {
        $this->SearchService = $search_service;
        $this->eZPublishAPIRespository = $ezpublish_api_repository;
        $this->TagsService = $tags_service;

        $this->DefaultPaginationParameters = new ParameterBag(
            array(
                "limit" => 10,
                "offset" => 0,
            )
        );
    }

    public function getDefaultPaginationParameters() {
        return new ParameterBag(
            $this->DefaultPaginationParameters->all()
        );
    }

    public function search( $parameters, $limit = 10, $offset = 0 ) {
        $SolrQueryString = (
            $this->buildQueryString( $parameters ) . ' AND (meta_class_identifier_ms:bulletin_article)' . $this->buildCategoryString( $parameters )
        );

        if(
            $parameters->has( 'qf' ) && $QueryFilter = $parameters->get( 'qf' )
        ) {
            $SolrQueryString .= " AND ($QueryFilter)";
        }

        return $this->processSearchResults(
            $this->SearchService->search( 'bulletin', $SolrQueryString, $limit, $offset )
        );
    }

    protected function buildQueryString( $parameters ) {
        if(
            $parameters->has( 'query' ) && $QueryParameter = $parameters->get( 'query' )
        ) {
            $TypeParameter = $parameters->get( 'type' ) ?: 'text';
            if(
                ( $isTextType = $TypeParameter === 'text' ) || $TypeParameter === 'author'
            ) {
                return (
                    $isTextType ?"((attr_article_title_t:'$QueryParameter') OR (attr_file_t:'$QueryParameter'))" : "(attr_article_author_t:'$QueryParameter')"
                );
            }
        }
        return '*:*';
    }

    protected function buildCategoryString( $parameters ) {
        $CategoryString = '';
        if(
            $parameters->has( 'category' ) && $CategoryParameter = $parameters->get( 'category' )
        ) {
            $CategoryString .= ' AND (';

            $FinalIndex = count( $CategoryParameter ) - 1;
            foreach( $CategoryParameter as $Key => $Value ) {
                if( is_numeric( $Value ) ) {
                    $Value = $this->TagsService->loadTag( $Value )->keyword;
                }

                $CategoryString .= "attr_tags_lk:'$Value'" ;
                if( $Key !== $FinalIndex ) {
                    $CategoryString .= ' OR ';
                }
            }

            $CategoryString .= ')';
        }
        return $CategoryString;
    }

    protected function processSearchResults( $search_results ) {
        $SearchParameters = $search_results->getSearchParams();

        $SearchResultRows = $search_results->getResults();
        $SearchResults = array(
            "total_count" => $search_results->getNumberFound(),
            "row_count" => count( $SearchResultRows ),
            "limit" => $SearchParameters["limit"],
            "offset" => $SearchParameters["offset"],
            "rows" => $SearchResultRows,
            "pager" => $search_results->getPager(),
            "highlighting" => $search_results->getHighlighting()
        );

        // add content field id list to search result rows ================================
        $ContentService = $this->eZPublishAPIRespository->getContentService();
        foreach( $SearchResults["rows"] as $Key => $Result ) {
            $Content = $ContentService->loadContent( $Result["meta_id_si"] );

            $ContentFieldIDList = array();
            foreach( $Content->getFields() as $ContentField ) {
                $ContentFieldIDList[ $ContentField->fieldDefIdentifier ] = $ContentField->id;
            }

            $SearchResults["rows"][ $Key ]["meta_attributes"] = $ContentFieldIDList;
        }
        // ================================================================================

        return $SearchResults;
    }

}
