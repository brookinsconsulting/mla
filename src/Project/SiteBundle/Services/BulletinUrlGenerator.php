<?php

namespace Project\SiteBundle\Services;

use Symfony\Component\Routing\RouterInterface;
use eZ\Publish\API\Repository\ContentService;
use eZ\Publish\API\Repository\Values\Content\ContentInfo;

/**
 * Handles generating URL for bulletin issues/articles
 */
class BulletinUrlGenerator
{

    /**
     * IDs of bulletin related content classes
     * @var integer
     */
    const BULLETIN_ISSUE_CLASS_ID = 46;
    const BULLETIN_ARTICLE_CLASS_ID = 45;

    /**
     * Route names for bulletin content
     * @var string
     */
    const BULLETIN_ISSUE_ROUTE = "bulletin_issue";
    const BULLETIN_ARTICLE_ROUTE = "bulletin_article";

    /**
     * @var ContentService
     */ 
    protected $contentService;

    /**
     * @var RouterInterface
     */
    protected $defaultRouter;

    /**
     * Constructor
     */ 
    public function __construct(ContentService $contentService, RouterInterface $defaultRouter)
    {
        $this->contentService = $contentService;
        $this->defaultRouter = $defaultRouter;
    }

    /**
     * Check if given ContentInfo represents a bulletin
     * @var ContentInfo $contentInfo
     * @return boolean
     */
    public function check(ContentInfo $contentInfo)
    {
        return in_array(
           $contentInfo->contentTypeId, 
            array(
                self::BULLETIN_ISSUE_CLASS_ID,
                self::BULLETIN_ARTICLE_CLASS_ID
            )
        );
    }

    /**
     * Generate URL string. Returns null if not a bulletin.
     * @param ContentInfo $contentInfo
     * @param array $parameters
     * @return string|null
     */
    public function generate(ContentInfo $contentInfo, array $parameters)
    {

        // check content type id of given location
        switch($contentInfo->contentTypeId)
        {

            // bulletin issue
            case self::BULLETIN_ISSUE_CLASS_ID:

                // get bulletin issue content object
                $content = $this->contentService->loadContent($contentInfo->id);

                return $this->defaultRouter->generate(
                    self::BULLETIN_ISSUE_ROUTE,
                    array( 'id' => $content->getFieldValue("journal")->text . "." . $content->getFieldValue("volume_number")->value . "." . $content->getFieldValue("issue_number")->value, 'siteaccess' => $content->getFieldValue("journal")->text ),
                    true
                );
                break;

            // bulletin article
            case self::BULLETIN_ARTICLE_CLASS_ID:

                // get bulletin article content object
                $content = $this->contentService->loadContent($contentInfo->id);

                return $this->defaultRouter->generate(
                    self::BULLETIN_ARTICLE_ROUTE,
                    array( 'id' => $content->getFieldValue("article_id")->text, 'siteaccess' => $content->getFieldValue("journal")->text ),
                    true
                );
                break;
        }

        // none valid bulletin
        return null;

    }

}