<?php

namespace Project\SiteBundle\Services;

class BulletinSearch
{

    protected $SearchService;

    public function __construct( $search_service ) {
        $this->SearchService = $search_service;
    }

    public function search($searchService, $query, $type, $category = null, $filter = '', $limit = 10, $offset = 0) {
        $QueryString = $this->buildQueryString($query, $type, $filter);
        if(
            $category
            )
        {
            $CategoryString = $this->buildCategoryQuery($category);
            $QueryString .= $CategoryString;
        }

        $SearchResults = $searchService->search(
            'bulletin', $QueryString, $limit, $offset
        );

        return $SearchResults;
    }

    protected function buildQueryString($query, $type, $filter) {
        if(
            $query != '*:*'
        )
        {
            if(
                $type == 'text'
                )
            {
                $QueryString  = "((attr_article_title_t:'".$query."') OR (attr_file_t:'".$query."'))";
            }else{
                $QueryString  = "(attr_article_author_t:'".$query."')";
            }
        }else{
            $QueryString  = $query;
        }
        $QueryString .= " AND (meta_class_identifier_ms:bulletin_article)";
        if($filter){ $QueryString  .= ' AND ('.$filter.')'; }
        return $QueryString;
    }

    protected function buildCategoryQuery($category) {
        $CategoryString = ' AND (';
        $lastKey = count($category) - 1;
        foreach(
            $category as $key => $catItem
            )
        {
            $CategoryString .= " attr_tags_lk:'$catItem'";
            if(
                $key != $lastKey
                )
            {
                $CategoryString .= " OR";
            }
        }
        $CategoryString .= ')';
        return $CategoryString;
    }

}