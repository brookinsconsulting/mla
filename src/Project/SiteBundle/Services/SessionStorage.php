<?php

namespace Project\SiteBundle\Services;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Storage\MetadataBag;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;

class SessionStorage extends NativeSessionStorage
{

    const ADMIN_SITE_ACCESS = "site_admin";

    public function __construct(array $options = array(), $handler = null, MetadataBag $metaBag = null, RequestStack $requestStack)
    {
        if ($requestStack->getMasterRequest()) {
            $host = explode(".", $requestStack->getMasterRequest()->getHost());
            if (count($host) >= 2) {
                $options['cookie_domain'] = "." . $host[count($host) - 2] . "." . $host[count($host) - 1];
            }
        }
        parent::__construct($options, $handler, $metaBag);
    }
}