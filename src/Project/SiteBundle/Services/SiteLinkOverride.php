<?php

namespace Project\SiteBundle\Services;

use Symfony\Component\Routing\RequestContext;
use eZ\Publish\Core\SignalSlot\Repository;
use eZ\Publish\Core\Base\Exceptions\UnauthorizedException;
use ThinkCreative\SiteLinkBundle\Services\SiteLink;
use ThinkCreative\SiteLinkBundle\Classes\SiteLinkContext;
use Project\SiteBundle\Services\BulletinUrlGenerator;

class SiteLinkOverride extends SiteLink
{

    /**
     * Id number of eZ admin user
     * @var integer
     */
    const ADMIN_USER_ID = 14;

    /**
     * @var \Project\SiteBundle\Services\BulletinUrlGenerator
     */
    protected $bulletinUrlGenerator;

    /**
     * @var \eZ\Publish\Core\SignalSlot\Repository
     */
    protected $ezApiRepository;

    /**
     * @var \eZ\Publish\API\Repository\Values\User\User
     */
    protected $adminUser;

    /**
     * Constructor.
     * @param BulletinUrlGenerator $bulletinUrlGenerator
     * @param RequestContext $request_context
     */
    public function __construct(BulletinUrlGenerator $bulletinUrlGenerator, RequestContext $request_context, Repository $ez_api_repository) {
        $this->bulletinUrlGenerator = $bulletinUrlGenerator;
        $this->ezApiRepository = $ez_api_repository;
        $this->adminUser = $this->ezApiRepository->getUserService()->loadUser(self::ADMIN_USER_ID);
        parent::__construct($request_context);
    }

    /**
     * Override buildHyperLink to generate bulletin URLs
     */
    public function buildHyperlink(SiteLinkContext $context, $absolute = null) {

        // get query params
        $queryParams = array();
        parse_str($context->getQueryString(), $queryParams);

        // bulletin custom case
        if (
            $context && $context->Location && isset($context->Location->contentInfo) && $this->bulletinUrlGenerator->check($context->Location->contentInfo)
        ) {

            return $this->bulletinUrlGenerator->generate(
                $context->Location->contentInfo,
                $queryParams
            );

        } elseif (
           $context && $context->Content && isset($context->Content->contentInfo) && $this->bulletinUrlGenerator->check($context->Content->contentInfo)
        ) {

            return $this->bulletinUrlGenerator->generate(
                $context->Content->contentInfo,
                $queryParams
            );

        }

        // fallback to parent
        return parent::buildHyperlink($context, $absolute);
    }

    /**
     * Override generate to handle UnauthorizedExceptions
     */
    public function generate($value, $is_location = true) {
        try {
            return parent::generate($value, $is_location);
        } catch (UnauthorizedException $e) {
            // if unauthorized then temporarly become admin to generate url
            $currentUser = $this->ezApiRepository->getCurrentUser();
            if (!$this->adminUser) {
                throw $e;
            }
            $this->ezApiRepository->setCurrentUser($this->adminUser);
            $returnValue = parent::generate($value, $is_location);
            $this->ezApiRepository->setCurrentUser($currentUser);
            return $returnValue;
        }
    }

}