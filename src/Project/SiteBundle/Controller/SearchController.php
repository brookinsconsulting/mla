<?php

namespace Project\SiteBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use eZ\Publish\API\Repository\Values\Content\Content;
use eZ\Publish\Core\Base\Exceptions\NotFoundException;
use eZ\Publish\Core\Base\Exceptions\UnauthorizedException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use ThinkCreative\SearchBundle\Solr\SolrQuery;
use ThinkCreative\SearchBundle\Exception\SearchTypeNotAvailableException;
use ThinkCreative\SearchBundle\Services\SearchService;
use ThinkCreative\SearchBundle\Services\EzSearchService;
use ThinkCreative\SearchBundle\Classes\SearchResults;
use Project\SiteBundle\Controller\BaseController;

/**
 * Main search controller
 */
class SearchController extends BaseController
{

    /**
     * Main search action
     * @param string $query
     * @param string $search_type
     * @param integer $section_Location_id
     */
    public function indexAction($query = "", $search_type = "", $section_location_id = null, Request $request)
    {

        // get config params
        $searchPageTemplate = $this->container->getParameter("think_creative_search.search_page_template");

        // get params
        $query = $request->query->get("query") ?: "*:*";
        $qf = $request->query->get("qf");
        $limit = $request->query->get("limit") ?: null;
        $offset = $request->query->get("offset") ?: 0;
        $sort = $request->query->get("sort") ?: "";

        // cache
        $response = new Response();
        $response->setPublic();
        $response->setSharedMaxAge(0);
        $response->setETag(
            md5(
                floor(time() / 60) .
                $query .
                $qf .
                $limit .
                $offset .
                $sort
            )
        );
        if ($response->isNotModified($request)) {
            return $response;
        }   

        // get siteaccess name
        $siteaccessName = $this->container->get("ezpublish.siteaccess")->name;

        // get siteaccess root location id
        $rootLocationId = $this->container->hasParameter("ezsettings.{$siteaccessName}.content.tree_root.location_id") ? $this->container->getParameter("ezsettings.{$siteaccessName}.content.tree_root.location_id") : 2;

        // use siteaccess as search type if not defined
        if (!$search_type) {
            $search_type = $siteaccessName;
        }

        // dynamic facet configuration
        $searchService = $this->get("thinkcreative.search.search");
        $searchConfigs = $this->container->getParameter("think_creative_search.search");
        foreach ($searchConfigs as &$searchConfig) {

            if ($search_type == $searchConfig['name'] && $search_type == $siteaccessName) {

                // SECTION FILTER
                $sections = array();
                $sectionSearch = $searchService->search(
                    $search_type, 
                    "meta_main_parent_node_id_si:{$rootLocationId} AND -meta_name_t:*Test*",
                    20,
                    0,
                    "meta_sort_name_ms asc"
                );

                foreach ($sectionSearch->getResults() as $item) {
                    $sections[] = array(
                        "display_name" => $item['meta_name_t'],
                        "identifier" => str_replace(" ", "_", strtolower($item['meta_name_t'])),
                        "query" => "meta_main_path_string_ms:*/{$item['meta_main_node_id_si']}/*"
                    );
                }

                $searchConfig['facet_queries'][] = array(
                    "display_name" => "Sections",
                    "identifier" => "section",
                    "queries" => $sections
                );

                // QF
                if ($qf) {
                    if (!is_array($qf)) {
                        $qf = array($qf);
                    }
                    $searchConfig['query'] = (isset($searchConfig['query']) ? $searchConfig['query'] . " AND " : "") . "(" . implode(") AND (", $qf) . ")";
                    $searchService->setSearchConfig($searchConfig);
                }

                // Section search
                if ($section_location_id) {
                    $searchConfig['query'] = (isset($searchConfig['query']) ? $searchConfig['query'] . " AND " : "") . "(meta_main_path_string_ms:*/{$section_location_id}/*)";
                    $searchService->setSearchConfig($searchConfig);
                }


                break;
            }
        }

        // load ez search service
        $searchService = new EzSearchService(
            new SearchService(
                $this->container->getParameter("think_creative_search.solr"),
                $searchConfigs
            ),
            $this->get("ezpublish.api.service.content"),
            $this->get("ezpublish_legacy.kernel"),
            $this->get("thinkcreative.search.filter.ezpublish")
        );

        // perform search
        $results = $searchService->search(
            $search_type,
            $query,
            $limit,
            $offset,
            $sort,
            $request->query->all(),
            array(
                "hl.useFastVectorHighlighter" => "true",
                "hl.snippets" => 1,
                "hl.fl" => "*_t"
            )
        );

        // get section location
        if ($section_location_id) {
            $sectionLocation = $this->container->get("ezpublish.api.service.location")->loadLocation($section_location_id);
        }

        // render response
        return $this->render(
            $request->isXmlHttpRequest() ? "ProjectSiteBundle:parts:search/search_page.html.twig" : "ProjectSiteBundle:full:search.html.twig",
            array(
                "searchResults" => $results,
                "sortOptions" => $searchService->getSortOptions($search_type),
                "contentObjects" => $searchService->getContentFromSearchResults($results),
                "searchType" => $search_type,
                "sectionLocation" => $section_location_id ? $sectionLocation : null
            ),
            $response
        );
    }
 
    /**
     * Render search result item
     * @param array $search_result
     * @param array $highlighting
     */
    public function searchResultItemAction(array $search_result, array $highlighting = array(), Request $request)
    {

        // cache
        $response = new Response();
        $response->setPublic();
        $response->setMaxAge(60);
        $response->setSharedMaxAge(60);
        $response->setETag(
            md5(
                json_encode($search_result) .
                json_encode($highlighting)
            )
        );
        if ($response->isNotModified($request)) {
            return $response;
        }   

        // get siteaccess name
        $siteaccessName = $this->container->get("ezpublish.siteaccess")->name;

        // get list of available content class templates
        $templatesList = array();
        if ($this->container->hasParameter("ezsettings.{$siteaccessName}.location_view")) {
            $templateList = $this->container->getParameter("ezsettings.{$siteaccessName}.location_view");
        }

        // check if template exists
        if (
            array_key_exists("line", $templateList) &&
            isset($search_result['main_node_meta_node_id_si'])
        ) {

            // try to load location and ensure there are not errors in permission
            try {
                $location = $this->get("ezpublish.api.service.location")->loadLocation($search_result['main_node_meta_node_id_si']);
            } catch (NotFoundException $e) {
            } catch (UnauthorizedException $e) {
            }

            if (isset($location) && !$location->invisible && !$location->hidden) {
                foreach ($templateList['line'] as $template) {

                    if (!isset($template['match']['Identifier\ContentType'])) {
                        continue;
                    }

                    // have template, use ez_content:viewLocation
                    if ($search_result['meta_class_identifier_ms'] == $template['match']['Identifier\ContentType']) {
                        return $this->forward(
                            "ez_content:viewLocation",
                            array(
                                'locationId'  => $search_result['main_node_meta_node_id_si'],
                                'viewType' => 'line',
                                'params' => array(
                                    'search_hightlighting' => $highlighting
                                )
                            )
                        );
                    }
                }
            }
        }

        // use a generic template
        return $this->render(
            "ProjectSiteBundle:parts:search/generic_search_result.html.twig",
            array(
                'search_result' => $search_result,
                'search_highlighting' => $highlighting,
                'link' => (isset($location) && !$location->invisible && !$location->hidden) ? true : false
            ),
            $response
        );
        

    }

}