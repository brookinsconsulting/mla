<?php

namespace Project\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use eZ\Publish\API\Repository\Values\Content,
    eZ\Publish\API\Repository\Values\Content\LocationQuery,
    eZ\Publish\API\Repository\Values\Content\Query\Criterion,
    eZ\Publish\API\Repository\Values\Content\Search\SearchResult,
    eZ\Publish\API\Repository\Values\Content\Query\SortClause;
use eZ\Publish\API\Repository\Values\Content\Location;

class ContentBoxController extends Controller
{

    public function indexAction(Location $location = null, $template = 'ThinkCreativeBridgeBundle:parts/contentbox:list.html.twig') {

        // get content service
        $contentService = $this->get("ezpublish.api.service.content");

        // use criterion to find content box's
        $query = new LocationQuery();
        $query->criterion = new Criterion\LogicalAnd(array(
            new Criterion\Visibility( Criterion\Visibility::VISIBLE ),
            new Criterion\ContentTypeIdentifier(array(
                "content_box",
                "related_links"
            )),
            new Criterion\LogicalOr(array(
                new Criterion\ParentLocationId( array($location ? $location->id : 0) ),
                new Criterion\LogicalAnd(array(
                    new Criterion\Field( 'persistent', Criterion\Operator::EQ, true ),
                    new Criterion\ParentLocationId( $location->path )
                ))
            ))
        ));
        $query->limit = 10;
        $searchResults = $this->get("ezpublish.api.service.search")->findLocations($query);

        // content service
        $contentService = $this->get("ezpublish.api.service.content");

        // make list
        $contentBoxList = array();
        foreach ($searchResults->searchHits as $searchHit) {

            // load content object
            $content = $contentService->loadContent( $searchHit->valueObject->contentInfo->id );

            // related links content box
            $relatedLinks = array();
            if ($content->getFieldValue("related_links")) {
                foreach ($content->getFieldValue("related_links")->destinationContentIds as $contentId) {
                    $relatedLinks[] = $contentService->loadContent($contentId);
                }
            }

            $contentBoxList[] = array(
                "priority" => $searchHit->valueObject->priority,
                "content" => $content,
                "related_links" => $relatedLinks
            );
        }

        // sort by location priority
        usort($contentBoxList, function($a, $b) {
            if ($a['priority'] != $b['priority']) {
                return $b['priority'] - $a['priority']; // descending location prority
            }
            return strcasecmp($a['content']->contentInfo->name, $b['content']->contentInfo->name);
        });

        return $this->render(
            $template, array(
                'content_box_list' => $contentBoxList
            )
        );
    }

}
