<?php

namespace Project\SiteBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use eZ\Bundle\EzPublishCoreBundle\Controller;
use Netgen\TagsBundle\API\Repository\TagsService;
use Netgen\TagsBundle\API\Repository\Values\Tags\Tag;
use Project\SiteBundle\Controller\BaseController;

/**
 * Ez Tag Controller
 */
class EzTagController extends Controller
{

    // copied from NetGenTagBundle

    /**
     * Action for rendering a tag view by using tag ID
     *
     * @param mixed $tagId
     * @param string $template
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewTagByIdAction( $tagId, $template = "" )
    {    
        $tagsService = $this->get("ezpublish.api.service.tags");
        $tag = $tagsService->loadTag( $tagId );
        return $this->renderTag( $tag, $template );
    }

    /**
     * Action for rendering a tag view by using tag URL
     *
     * @param string $tagUrl
     * @param string $template
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewTagByUrlAction( $tagUrl, $template = "" )
    {
        $tag = $this->tagsService->loadTagByUrl( $tagUrl );
        return $this->renderTag( $tag, $template );
    }

    /**
     * Renders the tag
     *
     * @param \Netgen\TagsBundle\API\Repository\Values\Tags\Tag $tag
     * @param string $template
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderTag( Tag $tag, $template = "" )
    {

        // get tag service
        $tagsService = $this->get("ezpublish.api.service.tags");

        // get all child tags
        $children  = $tagsService->loadTagChildren($tag, 0, -1);

        // create response
        $response = new Response();
        $response->headers->set( 'X-Tag-Id', $tag->id );

        return $this->render(
            $template ?: 'ProjectSiteBundle:full:tag.html.twig',
            array(
                'tag' => $tag,
                'children' => $children
            ),
            $response
        );
    }

}