<?php

namespace Project\SiteBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\API\Repository\Values\Content\Content;
//use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Response;
//use Project\SiteBundle\Classes\LocationSearch;
use Project\SiteBundle\Controller\BaseController;

class HomePageLinkController extends Controller
{

	public function indexAction($objectArray = array())
	{
		$Repository = $this->container->get('ezpublish.api.repository');
        $contentService = $Repository->getContentService();
        $SiteLink = $this->get('thinkcreative.sitelink');
		$result = array();
        foreach($objectArray as $key => $object){
            $content = $contentService->loadContent($object);
            $result[$key] = array('link' => $SiteLink->buildHyperlink($SiteLink->generate( $object, false )), 'object' => $content);
            //var_dump($SiteLink->generate( $object, false ));
        }

        return $this->render(
            "ProjectSiteBundle:parts:homepage_link.html.twig",
            array(
                "results" => $result,
            )
        );
	}

}