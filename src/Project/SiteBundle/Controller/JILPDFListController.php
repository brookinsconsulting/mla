<?php

namespace Project\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Project\SiteBundle\Controller\BaseController;

class JILPDFListController extends BaseController
{

    public function indexAction($template, $variables) {
        $eZPublishRepository = $this->container->get('ezpublish.api.repository');
        $LocationService = $eZPublishRepository->getLocationService();

        $SourceLocation = $LocationService->loadLocation(
            $SourceLocationID = str_replace(
                'eznode://', '', $variables['source']
            )
        );
        unset($variables['source']);


        $SearchResults = $this->container->get('aw_ezp_fetch_')->fetch(
            array(
                'filter' => array(
                    'AND' => array(
                        array(
                            'parent_location_id' => array(
                                'EQ' => $SourceLocationID,
                            ),
                        ),
                        array(
                            'visibility' => array(
                                'EQ' => true,
                            ),
                        ),
                        array(
                            'content_type_identifier' => array(
                                'EQ' => 'jil_pdf',
                            ),
                        ),
                    ),
                ),
                'sort' => array(
                    'field.jil_pdf/year/eng-US' => 'ASC',
                    'field.jil_pdf/month/eng-US' => 'ASC',
                ),
            )
        );

        $SiteLink = $this->get('thinkcreative.sitelink');

        $variables['items'] = array();
        foreach($SearchResults->searchHits as $SearchHit) {
            $Content = $SearchHit->valueObject;
            $Year = (string) $Content->getFieldValue('year');
            $Month = (string) $Content->getFieldValue('month');
            $Edition = trim(
                (string) $Content->getFieldValue('edition')
            );

            $DisplayName = (
                $Content->getFieldValue('display') . " $Year " . $Content->getFieldValue('pub') . (
                    $Edition ? ", $Edition" : ''
                )
            );

            $Decade = floor($Year / 10) * 10;
            if($Month < 10) {
                if($Year % 10 == 0) {
                    $Decade -= 10;
                }
                $Year -= 1;
            }

            $variables['items'][$Decade][$Year][] = array(
                'name' => $DisplayName,
                'link' => $SiteLink->buildHyperlink(
                    $SiteLink->generate( $Content, false )
                ),
            );
        }


        ksort( $variables['items'], SORT_NUMERIC );
        foreach(
            $variables['items'] as $DecadeYear => $Years
        ) {
            ksort( $Years, SORT_NUMERIC );
            foreach($Years as $Year => $Items) {
                $Years[$Year] = array_reverse( $Items, true );
            }
            $variables['items'][$DecadeYear] = array_reverse( $Years, true );
        }
        $variables['items'] = array_reverse(
            $variables['items'], true
        );


        return $this->render(
            $template, $variables
        );
    }

}