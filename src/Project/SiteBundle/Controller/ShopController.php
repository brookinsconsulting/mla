<?php

namespace Project\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ThinkCreative\PaymentBundle\Classes\Api\ShopifyApi;
use ThinkCreative\PaymentBundle\Entity\Order;
use Project\SiteBundle\Controller\BaseController;

class ShopController extends BaseController
{

    const ORDER_TYPE = "default";

    protected function getApi()
    {
        $apiConfig = $this->container->getParameter("ThinkCreativePaymentBundle_ShopifyProcessorService_Configuration");
        return new ShopifyApi(
            $apiConfig['api_key'],
            $apiConfig['api_password'],
            $apiConfig['api_host']
        );
    }

    public function indexAction() {

        // init api
        $shopify = $this->getApi();

        // get product list
        $products = $shopify->request("products");
        $products = $products['products'];

        // get order
        $order = Order::getOrderFromSession(
            $this->getRequest(),
            $this->get($this->container->getParameter("ThinkCreativePaymentBundle_ProcessorService")),
            self::ORDER_TYPE
        );

        // save order
        $order->saveOrderToSession($this->getRequest());        

        return $this->render(
            "ProjectSiteBundle:full:shop/main.html.twig",
            array(
                "products" => $products,
                "order" => $order,
                "order_type" => self::ORDER_TYPE
            )
        );
    }

    public function checkoutAction()
    {

        // init api
        $shopify = $this->getApi();

        // get order
        $order = Order::getOrderFromSession(
            $this->getRequest(),
            $this->get($this->container->getParameter("ThinkCreativePaymentBundle_ProcessorService")),
            self::ORDER_TYPE
        );
        // save order
        $order->saveOrderToSession($this->getRequest());

        // use api to get products in this order
        $products = array();
        foreach ($order->getProductList() as $productId => $quantity) {
            list($productId, $variantId) = explode("_", $productId);
            $product = $shopify->request("products/{$productId}");
            $products[] = $product['product'];
        }

        // load payment gateway configuration
        $gatewayConfig = $this->container->getParameter("ThinkCreativePaymentBundle_GatewayConfiguration");        

        return $this->render(
            "ProjectSiteBundle:full:shop/checkout.html.twig",
            array(
                "products" => $products,
                "order" => $order,
                "order_type" => self::ORDER_TYPE,
                "gateway_config" => $gatewayConfig
            )
        );        
    }


}