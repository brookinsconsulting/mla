<?php

namespace Project\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion\Operator;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;
use Project\SiteBundle\Controller\BaseController;

class GeneralController extends BaseController
{

    public function DoubleFormAction() {
        $Variables = array();

        return $this->render(
            "ProjectSiteBundle::double_form.html.twig", $Variables
        );
    }

    public function BannerAction( $template, $location ) {
        $Variables = array(
            "items" => array(),
        );

        $eZPublishRepository = $this->container->get( 'ezpublish.api.repository' );
        $LocationService = $eZPublishRepository->getLocationService();
        $ContentService = $eZPublishRepository->getContentService();

        $Criteria = array(
            new Criterion\ParentLocationId( $location->id ),
            new Criterion\Visibility( Criterion\Visibility::VISIBLE ),
            new Criterion\ContentTypeIdentifier( 'banner' ),
        );

        $Query = new LocationQuery(
            array(
                'criterion' => new Criterion\LogicalAnd( $Criteria ),
                'sortClauses' => array(
                    // new SortClause\Field( 'bulletin_issue', 'issue_year', Query::SORT_DESC, 'eng-US' ),
                    // new SortClause\Field( 'bulletin_issue', 'volume_number', Query::SORT_DESC, 'eng-US' ),
                )
            )
        );

        $Results = $eZPublishRepository->getSearchService()->findLocations( $Query );
        $Variables["items"] = array();
        foreach( $Results->searchHits as $SearchHit ) {

            $content = $ContentService->loadContentByContentInfo( $SearchHit->valueObject->contentInfo );
            $imageContent = $ContentService->loadContent( $content->getFieldValue("ml_image")->destinationContentId );

            // generate link
            $bannerLink = "";
            if ($content->getFieldValue("internal_link") && $content->getFieldValue("internal_link")->destinationContentId) {
                try {
                    $linkContext = $this->get("thinkcreative.sitelink")->generate(
                        $content->getFieldValue("internal_link")->destinationContentId,
                        false
                    );
                    if ($linkContext) {
                        $bannerLink = $this->get("thinkcreative.sitelink")->buildHyperlink($linkContext);
                    }
                } catch(\LogicException $e) {
                }
            }
            if (!$bannerLink && $content->getFieldValue("external_link") && $content->getFieldValue("external_link")->text) {
                $bannerLink = $content->getFieldValue("external_link")->text;
            }

            $Variables["items"][] = array(
                "location" => $SearchHit->valueObject,
                "content" => $content,
                "link" => $bannerLink,
                "image_content" => $ContentService->loadContent( $content->getFieldValue("ml_image")->destinationContentId ),
                "image_alt_text" => ""
            );
        }
        return $this->render( $template, $Variables );
    }

    public function FooterAction( $template, $location) {
        $Variables = array(
            "items" => array(),
        );

        $eZPublishRepository = $this->container->get( 'ezpublish.api.repository' );
        $LocationService = $eZPublishRepository->getLocationService();
        $ContentService = $eZPublishRepository->getContentService();

        $Criteria = array(
            new Criterion\ParentLocationId( $location ),
            new Criterion\Visibility( Criterion\Visibility::VISIBLE ),
            new Criterion\ContentTypeIdentifier( 'footer' ),
        );

        $Query = new LocationQuery(
            array(
                'criterion' => new Criterion\LogicalAnd( $Criteria ),
            )
        );

        $sort = array(
            new SortClause\Location\Priority( Query::SORT_DESC ),
        );

        $Query->sortClauses = $sort;
        $Query->limit = 1;

        $Results = $eZPublishRepository->getSearchService()->findLocations( $Query );

        foreach( $Results->searchHits as $SearchHit ) {
            $Variables["items"][] = array(
                "location" => $SearchHit->valueObject,
                "content" => $ContentService->loadContentByContentInfo( $SearchHit->valueObject->contentInfo ),
            );
        }

        return $this->render( $template, $Variables );
    }

    public function PageSidebarAction( $content, $location ) {
        $PageSidebarTemplate = 'ProjectSiteBundle:menu/microsite:sidemenu.html.twig';

        if(
            in_array( $location->id, array( 5719, 8030 ) )
        ) {
            $PageSidebarTemplate = 'ProjectSiteBundle:menu/microsite:bulletin_sidemenu.html.twig';
        }

        return $this->render(
            $PageSidebarTemplate, array( "content" => $content, "location" => $location )
        );
    }

    public function SearchFormAction( $template, $variables ) {
        return $this->render( $template, $variables );
    }

}