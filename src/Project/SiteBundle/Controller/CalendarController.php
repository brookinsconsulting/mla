<?php

namespace Project\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ThinkCreative\CalendarBundle\Services\EventProcessor;
use ThinkCreative\CalendarBundle\Classes\DateConstraintCalculator;
use Project\SiteBundle\CalendarViews\SingleYearView;

class CalendarController extends Controller
{

    /**
     * Render controls for the calendar
     */
    public function controlsAction($location_id, $embed = false)
    {
        // get request
        $request = Request::createFromGlobals();

        // configuration
        $filterServiceNames = $this->container->getParameter("think_creative_calendar.default_filters");
        $colorFilter = $this->container->getParameter("think_creative_calendar.color_filter");
        $availableLanguages = $this->container->getParameter("think_creative_calendar.languages");
        $repeatRange = array(
            $this->container->getParameter("think_creative_calendar.repeat_start_range"),
            $this->container->getParameter("think_creative_calendar.repeat_end_range")
        );

        // load filter services
        $filterServices = array();
        foreach ($filterServiceNames as $filterServiceName) {
            $filterServices[] = $this->get(trim(trim($filterServiceName), "@"));
            $filterServices[count($filterServices) - 1]->setLocationId($location_id);
        }

        // get request vars
        $timezone = $request->query->get("timezone") ?: date_default_timezone_get();
        $language = $request->query->get("language") ?: $availableLanguages[0];

        // output
        return $this->render(
            "ProjectSiteBundle:parts:event_calendar/controls.html.twig",
            array(
                "location_id" => $location_id,
                "filters" => $filterServices,
                "timezone" => $timezone,
                "language" => $language,
                "embedCalendarView" => $embed
            )
        );        


    }


}