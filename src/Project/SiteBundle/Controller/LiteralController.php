<?php

namespace Project\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Project\SiteBundle\Controller\BaseController;

class LiteralController extends BaseController
{

    public function indexAction($template, $variables) {
        $Output = $this->render(
            $template, $variables
        );

        $Output->setContent(
            html_entity_decode(
                $Output->getContent()
            )
        );

        return $Output;
    }

}