<?php

namespace Project\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion\Operator;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;
use Project\SiteBundle\Controller\BaseController;


class BulletinController extends BaseController
{

    public function ArticleAction( $id = false ) {

        if( $id ) {
            $eZPublishRepository = $this->container->get( 'ezpublish.api.repository' );

            $Criteria = array(
                new Criterion\Field( 'article_id', Criterion\Operator::EQ, $id ),
                new Criterion\Visibility( Criterion\Visibility::VISIBLE ),
                new Criterion\ContentTypeIdentifier( 'bulletin_article' ),
            );

            $Query = new LocationQuery(
                array(
                    'criterion' => new Criterion\LogicalAnd( $Criteria ),
                )
            );
            $Query->limit = 1;

            $SearchResults = $eZPublishRepository->getSearchService()->findLocations( $Query );
            if( $SearchResults->searchHits ) {
                return $this->forward(
                    'ez_content:viewLocation', array( "locationId" => $SearchResults->searchHits[0]->valueObject->id, "viewType" => 'full', )
                );
            }
        }

        // not found
        throw $this->createNotFoundException('Bulletin article not found.');

    }

    public function IssueAction( $id = false ) {

        if( $id ) {
            $eZPublishRepository = $this->container->get( 'ezpublish.api.repository' );

            $IssueReference = explode( '.', $id );
            $Criteria = array(
                new Criterion\Field( 'journal', Criterion\Operator::EQ, $IssueReference[0] ),
                new Criterion\Field( 'volume_number', Criterion\Operator::EQ, $IssueReference[1] ),
                new Criterion\Field( 'issue_number', Criterion\Operator::EQ, $IssueReference[2] ),
                new Criterion\Visibility( Criterion\Visibility::VISIBLE ),
                new Criterion\ContentTypeIdentifier( 'bulletin_issue' ),
            );

            $Query = new LocationQuery(
                array(
                    'criterion' => new Criterion\LogicalAnd( $Criteria ),
                )
            );
            $Query->limit = 1;

            $SearchResults = $eZPublishRepository->getSearchService()->findLocations( $Query );
            if( $SearchResults->searchHits ) {
                return $this->forward(
                    'ez_content:viewLocation', array( "locationId" => $SearchResults->searchHits[0]->valueObject->id, "viewType" => 'full', )
                );
            }
        }

        // not found
        throw $this->createNotFoundException('Bulletin issue not found.');

    }

    public function IssueListAction( $template, $variables ) {
        // "ProjectSiteBundle:bulletin:issue_list.html.twig", $Variables

        $variables["items"] = array();

        $eZPublishRepository = $this->container->get( 'ezpublish.api.repository' );
        $LocationService = $eZPublishRepository->getLocationService();
        $ContentService = $eZPublishRepository->getContentService();

        $SourceLocation = $LocationService->loadLocation(
            $SourceLocationID = str_replace( 'eznode://', '', $variables["source"] )
        );
        unset( $variables["source"] );

        $SearchResults = $this->generateSearchResults( $eZPublishRepository, $SourceLocationID );

        $SiteLink = $this->get( 'thinkcreative.sitelink' );
        foreach( $SearchResults->searchHits as $SearchHit ) {
            $Location = $SearchHit->valueObject;
            $Content = $ContentService->loadContentByContentInfo( $Location->contentInfo );
            $IssueYear = (string) $Content->getFieldValue( 'issue_year' );

            $IssueDecade = floor( $IssueYear / 10 ) * 10;

            $variables["items"][ $IssueDecade ][] = array(
                "name" => $Content->contentInfo->name,
                "location_id" => $Location->id,
                "content" => $Content,
                "link" => $SiteLink->buildHyperlink(
                    $SiteLink->generate( $Content, false )
                ),
            );
        }

        return $this->render( $template, $variables );
    }

    public function CurrentIssueAction( $template, $variables )
    {

        $variables["result"] = array();

        $eZPublishRepository = $this->container->get( 'ezpublish.api.repository' );
        $LocationService = $eZPublishRepository->getLocationService();
        $ContentService = $eZPublishRepository->getContentService();

        $SourceLocation = $LocationService->loadLocation(
            $SourceLocationID = str_replace( 'eznode://', '', $variables["source"] )
        );
        unset( $variables["source"] );

        if (!intval($SourceLocationID)) {
            return $this->render($template, $variables);
        }

        $SearchResults = $this->generateSearchResults( $eZPublishRepository, $SourceLocationID, 1 );

        $SiteLink = $this->get( 'thinkcreative.sitelink' );
        foreach( $SearchResults->searchHits as $SearchHit ) {
            $Location = $SearchHit->valueObject;
            $Content = $ContentService->loadContentByContentInfo( $Location->contentInfo );

            $variables["result"] = array(
                "name" => $Content->contentInfo->name,
                "location_id" => $Location->id,
                "content" => $Content,
                "link" => $SiteLink->buildHyperlink(
                    $SiteLink->generate( $Content, false )
                ),
            );
        }

        return $this->render( $template, $variables );
    }

    public function IssueArticlesAction( $issue_location_id ) {
        $Response = new Response();
        $Response->headers->set( 'Content-Type', 'application/json' );

        $Response->setContent(
            json_encode(
                array(
                    "issue_location_id" => (int) $issue_location_id,
                    "article_list" => $this->getBulletinArticles( $issue_location_id ),
                )
            )
        );

        return $Response;
    }

    protected function generateSearchResults( $ezpublish_api_repository, $source_id, $limit = false ) {

        $Criteria = array(
            new Criterion\ParentLocationId( $source_id ),
            new Criterion\Visibility( Criterion\Visibility::VISIBLE ),
            new Criterion\ContentTypeIdentifier( 'bulletin_issue' ),
        );

        $Query = new LocationQuery(
            array(
                'criterion' => new Criterion\LogicalAnd( $Criteria ),
                'sortClauses' => array(
                    new SortClause\Field('bulletin_issue', 'volume_number', Query::SORT_DESC, "eng-US"),
                    new SortClause\Field('bulletin_issue', 'issue_number', Query::SORT_DESC, "eng-US"),
                    new SortClause\DatePublished()
                )
            )
        );
        if($limit){$Query->limit = $limit;}

        return $Results = $ezpublish_api_repository->getSearchService()->findLocations( $Query );
    }

    protected function getBulletinArticles( $location_id ) {
        $SearchResults = $this->get( 'aw_ezp_fetch' )->fetchContentByLocation(
            $SearchParameters = array(
                "filter" => array(
                    "AND" => array(
                        array(
                            "parent_location_id" => array(
                                "EQ" => $location_id,
                            ),
                        ),
                        array(
                            "visibility" => array(
                                "EQ" => true,
                            ),
                        ),
                        array(
                            "content_type_identifier" => array(
                                "EQ" => 'bulletin_article',
                            ),
                        ),
                    ),
                ),
                "sort" => array(
                    "field.bulletin_article/page_number/eng-US" => 'ASC'
                )
            )
        );

        if( $SearchResults->totalCount > 0 ) {
            $BulletinArticles = array();
            $SiteLink = $this->get( 'thinkcreative.sitelink' );

            foreach( $SearchResults->searchHits as $SearchHit ) {
                $Content = $SearchHit->valueObject;
                $PageNumber = (int) (string) $Content->getFieldValue( 'page_number' );

                // Back Matter
                $ArticleId = (string) $SearchHit->valueObject->getFieldValue("article_id");
                $ArticleId = explode(".", $ArticleId);
                if ( 
                    isset($ArticleId[2]) && (int) $ArticleId[2] == 999 
                ) {
                    $PageNumber = 999;
                }

                if(
                    $ArticleCategory = (string) $Content->getFieldValue( 'article_subhead1' )
                ) {
                    $BulletinArticles[ $PageNumber - 1 ] = array(
                        "article_category" => $ArticleCategory,
                        "level" => 1,
                    );
                }

                if(
                    $ArticleCategory = (string) $Content->getFieldValue( 'article_subhead2' )
                ) {
                    $BulletinArticles[ $PageNumber - 1 ] = array(
                        "article_category" => $ArticleCategory,
                        "level" => 2,
                    );
                }

                $BulletinArticles[ $PageNumber ] = array(
                    "name" => $Content->contentInfo->name,
                    "page_number" => $PageNumber,
                    "url" => $SiteLink->buildHyperlink(
                        $SiteLink->generate( $Content, false )
                    ),
                );
            }
            ksort( $BulletinArticles );
            return $BulletinArticles;
        }

        return false;
    }

}