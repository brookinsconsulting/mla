<?php

namespace Project\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Project\SiteBundle\Controller\BaseController;

class ErrorController extends BaseController
{

    public function NotFoundAction() {
        $RequestViewHandler = $this->container->get("thinkcreative.requestviewhandler");

        $RequestViewHandler->set(
            "name", "Requested Page Not Found"
        );

        return $this->render(
            "ProjectSiteBundle:error:404.html.twig"
        );
    }

}