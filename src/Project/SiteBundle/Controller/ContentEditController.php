<?php

namespace Project\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Temporary controller to redirect /content/edit request
 * to admin.
 */
class ContentEditController extends Controller
{

    /**
     * Name of admin site access
     * @var string
     */
    const ADMIN_SITE_ACCESS = "site_admin";

    /**
     * Redirect to admin
     * @param integer $object_id
     */
    public function redirectAction($object_id)
    {

        $siteaccessRouter = $this->get("ezpublish.siteaccess_router");
        $newRequest = $siteaccessRouter->matchByName(self::ADMIN_SITE_ACCESS)->matcher->getRequest();

        return $this->redirect(
            $newRequest->scheme . "://" . $newRequest->host . $newRequest->pathinfo
        );
    }
}