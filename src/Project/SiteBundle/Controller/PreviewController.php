<?php

namespace Project\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Project\SiteBundle\Controller\BaseController;

class PreviewController extends BaseController
{

	public function indexAction($pageid) {
		return $this->render(
			"ProjectSiteBundle:preview:$pageid.html.twig"
		);
	}

}