<?php

namespace Project\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Project\SiteBundle\Controller\BaseController;
use Pagerfanta\Adapter\FixedAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\View\TwitterBootstrapView;

class BulletinSearchController extends BaseController
{

    protected $RootLocationID;

    public function FormAction() {
        $TagsService = $this->get( 'ezpublish.signalslot.service.tags' );

        $SiteAccessBulletinTag = $TagsService->loadTag(
            $this->getSiteAccessTagID()
        );

        $BulletinTagList = array();
        if(
            $SiteAccessBulletinTag && $BulletinTagCategoryList = $TagsService->loadTagChildren( $SiteAccessBulletinTag )
        ) {
            foreach ( $BulletinTagCategoryList as $BulletinTagCategory ) {
                $BulletinTagList[ $BulletinTagCategory->keyword ] = $TagsService->loadTagChildren( $BulletinTagCategory );
            }
        }

        return $this->render(
            'ProjectSiteBundle:customtags:bulletin_search_form.html.twig', array( "tags" => $BulletinTagList )
        );
    }

    public function filtersAction( array $category) {
        $TagsService = $this->get( 'ezpublish.signalslot.service.tags' );

        $SiteAccessBulletinTag = $TagsService->loadTag(
            $this->getSiteAccessTagID()
        );

        $BulletinTagList = array();
        if(
            $SiteAccessBulletinTag && $BulletinTagCategoryList = $TagsService->loadTagChildren( $SiteAccessBulletinTag )
        ) {
            foreach ( $BulletinTagCategoryList as $BulletinTagCategory ) {
                $BulletinTagList[ $BulletinTagCategory->keyword ] = $TagsService->loadTagChildren( $BulletinTagCategory );
            }
        }
        return $this->render(
            'ProjectSiteBundle:parts:search/bulletin_search_filters.html.twig', array( "tags" => $BulletinTagList, "category" => $category )
        );
    }

    public function SearchAction() {
        $ControllerResponse = null;
        $ControllerResponseTemplate = 'ProjectSiteBundle:full:bulletin_search.html.twig';

        $Request = $this->getRequest();
        $isXmlHttpRequest = $Request->isXmlHttpRequest();
        $isJavaScriptEnabled = $this->isJavaScriptEnabled();

        // non-removal transfer of POST request parameters to GET request parameters
        if(
            $hasSearchQuery = $Request->request->count()
        ) {
            $Request->query->add( $Request->request->all() );
        }

        // search results format user override
        if(
            !$isXmlHttpRequest && $Request->query->has( 'xhr' )
        ) {
            $isXmlHttpRequest = $Request->query->get( 'xhr' ) == 1;
            $Request->query->remove( 'xhr' );
        }

        // has search query parameters check
        if(
            $hasSearchQuery || $Request->query->count()
        ) {
            $TemplateVariables = array(
                "is_xmlhttprequest" => $isXmlHttpRequest,
                "search_results" => false,
                "search_query_string" => $this->makeQueryString( $Request->query->all() ),
                "search_query" => $Request->query->all(),
            );

            // execute search and generate results
            if( $isXmlHttpRequest || !$isJavaScriptEnabled ) {
                $QueryStringParameters = $Request->query->all();
                $TemplateVariables["search_results"] = $SearchResults = $this->generateSearchResults( $Request->query );
                $TemplateVariables["rendered_pagination"] = $this->generateSearchPagination(
                    $TemplateVariables["search_results"], $QueryStringParameters
                );
                $TemplateVariables["search_results"]["viewing"] = array(
                    "start" => (
                        $SearchResults["offset"] !== 0 ? $SearchResults["offset"] : 1
                    ),
                    "end" => (
                        ($Max = $SearchResults["offset"] + $SearchResults["limit"] ) < $SearchResults["total_count"] ? $Max : $SearchResults["total_count"]
                    ),
                );
            }

            $Response = $this->render(
                $ControllerResponseTemplate, $TemplateVariables, $ControllerResponse
            );

            $Response->setPrivate();
            $Response->setMaxAge( 0 );
            $Response->setSharedMaxAge( 0 );
            $Response->headers->addCacheControlDirective( 'must-revalidate', true );
            $Response->headers->addCacheControlDirective( 'no-store', true );

            return $Response;
        }

        return $this->FormAction();
    }

    protected function makeQueryString( array $parameters ) {
        return preg_replace(
            "/\%5B\d+\%5D/s", '%5B%5D', http_build_query( $parameters )
        );
    }

    protected function generateSearchPagination( $search_results, $parameters ) {

        // get pager
        if (!isset($search_results['pager'])) {
            return "";
        }
        $pager = $search_results['pager'];

        // router generator
        $base_url = "";
        $routeGenerator = function($page) use ($pager, $parameters, $base_url) {
            $parameters['offset'] = ($page * $pager->getMaxPerPage()) - $pager->getMaxPerPage();
            return $base_url . "?" . http_build_query($parameters);
        };

        // create view
        $view = new TwitterBootstrapView();
        $options = array(
            'proximity' => 3,
            'prev_message' => "&laquo;",
            'next_message' => "&raquo;"
        );
        $output = $view->render($pager, $routeGenerator, $options);

        // slight hack to get the 'pagination' class on the UL element
        $output = str_replace("<ul", "<ul class='pagination'", $output);
        $output = str_replace('<div class="pagination"', "<div class='pager'", $output);

        return $output;
    }

    protected function generateSearchResults( $parameters ) {
        $SearchHandler = $this->get( 'project.bulletinsearchhandler' );

        // add RootLocationID filter to search query parameters
        $parameters->set(
            'qf', 'meta_main_path_element_2_si:'. $this->getRootLocationID()
        );

        $PaginationParameters = $this->getPaginationParameters( $SearchHandler, $parameters );

        return $SearchHandler->search(
            $parameters, $PaginationParameters->get( 'limit' ), $PaginationParameters->get( 'offset' )
        );
    }

    protected function getPaginationParameters( $search_handler, $parameters ) {
        $PaginationParameters = $search_handler->getDefaultPaginationParameters();

        foreach( $PaginationParameters as $Key => $Value ) {
            if(
                $parameters->has( $Key ) && $ParameterValue = $parameters->get( $Key )
            ) {
                if( $ParameterValue != $Value ) {
                    if( is_numeric( $ParameterValue ) ) {
                        $ParameterValue = (
                            strpos( $ParameterValue, '.' ) ? (float) $ParameterValue : (int) $ParameterValue
                        );
                    }
                    $PaginationParameters->set( $Key, $ParameterValue );
                }
                $parameters->remove( $Key );
            }
        }

        return $PaginationParameters;
    }

    protected function getRootLocationID() {
        if( $this->RootLocationID === null ) {
            $this->RootLocationID = $this->container->get( 'ezpublish.config.resolver' )->getParameter( 'content.tree_root.location_id' );
        }
        return $this->RootLocationID;
    }

    protected function getSiteAccessTagID() {
        $SiteAccessTagIDMap = array(
            "88" => 5, "89" => 5, "90" => 4,
        );
        return $SiteAccessTagIDMap[ $this->getRootLocationID() ];
    }

    protected function isJavaScriptEnabled() {
        $RequestViewHandlerParameters = $this->container->get( 'thinkcreative.requestviewhandler' )->getParameters();
        return (
            isset( $RequestViewHandlerParameters["javascript_enabled"] ) && $RequestViewHandlerParameters["javascript_enabled"]
        );
    }

}