<?php

namespace Project\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SiteMapController extends Controller
{

    public function indexAction($location_id) {
        $eZPublishRepository = $this->container->get('ezpublish.api.repository');
        $LocationService = $eZPublishRepository->getLocationService();

        $Location = $LocationService->loadLocation($location_id);

        return $this->render(
            "ProjectSiteBundle::sitemap.html.twig", array(
                "sitemap_location" => $Location,
            )   
        );  
    }   

}