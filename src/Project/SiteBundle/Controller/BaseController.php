<?php

namespace Project\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller
{
    protected $ttl;

    public function setMaxTTL( $ttl = null )
    {
        $this->ttl = $ttl;
    }

    public function render( $view, array $parameters = array(), Response $response = null )
    {
        $response = parent::render($view, $parameters, $response);
        $resolver = $this->container->get( 'ezpublish.config.resolver' );
        if(
            $resolver->getParameter( 'content.ttl_cache' ) === true && $this->ttl !== null
        )
        {
            $response->setSharedMaxAge( $this->ttl );
        }else{
            $response->setSharedMaxAge(
                $resolver->getParameter( 'content.default_ttl' )
            );
        }

        return $response;
    }
}