<?php

namespace Project\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;


class LegacyRedirectController extends Controller
{

    /**
     * HTTP Status code to use for redirects
     * @var integer
     */
    const REDIRECT_STATUS_CODE = 302;

    /**
     * Domain to redirect to
     * @var string
     */
    const REDIRECT_DOMAIN = "apps.mla.org";

    /**
     * Checks if given location contains redirect
     * information and redirects otherwise
     * returns standard response
     * @param integer $locationId
     */
    public function indexAction($locationId, $viewType, $layout = false, array $params = array())
    {      

        // get location
        $location = $this->get("ezpublish.api.service.location")->loadLocation($locationId);

        // get content
        $content = $this->get("ezpublish.api.service.content")->loadContent($location->contentInfo->id);

        // check if legacy redirect needed
        if ($content->getFieldValue("forward") && $content->getFieldValue("forward")->bool) {

            // get redirect url
            $redirectUrl = trim($content->getFieldValue("forwarding_path")->text);

            // check if full url, if not append 
            if (!parse_url($redirectUrl, PHP_URL_SCHEME)) {
                if (substr($redirectUrl, 0, 1) == "/") {
                    $redirectUrl = substr($redirectUrl, 1);
                }
                $redirectUrl = "http://" . self::REDIRECT_DOMAIN . "/" . $redirectUrl;
            }

            // create a special message if this is a preview
            if (isset($params["isPreview"]) && $params["isPreview"]) {
                return new Response("Redirects to {$redirectUrl}.");
            }

            // create redirect response
            $response = new RedirectResponse($redirectUrl, self::REDIRECT_STATUS_CODE);
            $response->setPublic();
            $response->setMaxAge(60);
            $response->setSharedMaxAge(60);
            $response->setVary( 'X-Location-Id', $locationId );
            return $response;
        }

        // get response
        return $this->get( 'ez_content' )->viewLocation( $locationId, $viewType, $layout, $params);

    }
}
