// set feedback icon as inline form element for Twitter Bootstrap

var FEEDBACK_NONE = 0;
var FEEDBACK_SUCCESS = 1;
var FEEDBACK_ERROR = 2;
var FEEDBACK_THROBER = 3;

function setFeedback(element, type, message)
{
    var element = element;
    $(element)
        .removeClass("has-success")
        .removeClass("has-error")
    ;

    // clear messages
    $(element).find(".alert").slideUp(function() {
        $(this).remove();    
    });

    $(element).find("span.form-control-feedback")
        .removeClass("glyphicon-ok")
        .removeClass("glyphicon-remove")
        .removeClass("glyphicon-refresh")
    ;

    // add feedback glyph
    if (
        $(element).find("input[type=text]").length > 0 ||
        $(element).find("select").length > 0 ||
        $(element).find("textarea").length > 0
    ) {

        if ($(element).find("span.form-control-feedback").size() <= 0) {
            $(element).addClass("has-feedback");
            $(element).append('<span class="form-control-feedback glyphicon"></span>');
        }
        $(element).find("span.form-control-feedback").attr("title", message);

        switch (type) {

            case FEEDBACK_NONE:
            break;

            case FEEDBACK_SUCCESS:
                $(element).addClass("has-success");
                $(element).find("span.form-control-feedback").addClass("glyphicon-ok");
            break;

            case FEEDBACK_ERROR:
                $(element).addClass("has-error");
                $(element).find("span.form-control-feedback").addClass("glyphicon-remove");
            break;

            case FEEDBACK_THROBER:
            default:
                $(element).find("span.form-control-feedback").addClass("glyphicon-refresh");
            break;
        }
    }
}

// set feedback from TC Form validation result
function tcFormSetFeedback(element, code, message)
{

    // normal field
    if (!$(element).attr("data-form-id")) {

        if (
            $(element).is("input[type=radio]") ||
            $(element).is("input[type=checkbox]")
        ) {
            return;
        }

        switch(code) {
            case TC_FORM_VALIDATION_LOADING:
                setFeedback($(element).parent(), FEEDBACK_THROBER, (typeof message == "string") ? message : "Loading...");
            break;

            case TC_FORM_VALIDATION_SUCCESS:
                setFeedback($(element).parent(), FEEDBACK_SUCCESS, (typeof message == "string") ? message : "Valid.");
            break;

            case TC_FORM_VALIDATION_ERROR:
                setFeedback($(element).parent(), FEEDBACK_ERROR, (typeof message == "string") ? message : "Invalid.");

                // message string
                console.log(message);
                if (typeof message == "string") {
                    $(element).before("<div class='alert alert-warning'>" + message + "</div>");
                } else if (typeof message == "object" && message.length == 1) {
                    $(element).before("<div class='alert alert-warning'>" + message[0] + "</div>");
                } else if (typeof message == "object" && message.length > 1) {
                    mhtml = "";
                    for (var i = 0; i < message.length; i++) {
                        mhtml = mhtml + "<li>" + message[i] + "</li>";
                    }
                    $(element).before("<div class='alert alert-warning'>The following errors occured...<ul>" + mhtml + "</ul>");
                }
                $(element).parent().find(".alert").hide();
                $(element).parent().find(".alert").slideDown();

            break;
        }

    // entire form
    } else {
        switch(code) {
            case TC_FORM_VALIDATION_LOADING:
                $(element).find("input, button, select, textarea").attr("disabled", true);
                if ($(element).find(".feedback-area").length > 0) {
                    $(element).find(".feedback-area").html('<span class="form-control-feedback glyphicon glyphicon-refresh" title="Loading..."></span>');
                }
            break;

            case TC_FORM_VALIDATION_SUCCESS:
                $(element).find("input, button, select, textarea").removeAttr("disabled");
                $(element).find("input[type=submit], button[type=submit]").attr("disabled", true);
            break;

            case TC_FORM_VALIDATION_ERROR:
                $(element).find("input, button, select, textarea").removeAttr("disabled");
                if ($(element).find(".feedback-area").length > 0) {
                    $(element).find(".feedback-area").html('<span class="glyphicon glyphicon-exclamation-sign"></span> One or more errors found.');
                }
            break;
        }
    }
}