+function( global, $ ) {

    function processUserLoginForm( form, context ) {
        if( context.name == "login[_username]" || context.name == "login[_password]" ) {
            var LoginFieldName = (
                context.name == "login[_username]" ? "username" : "password"
            );

            form.find( "[name=" + LoginFieldName + "]" ).val( context.value );
        }
    }

    global.MultiLogin = {
        "onSubmitEvent" : function( e ) {
            var _this = $( this );

            if(
                !_this.data( "allow" )
            ) {
                var
                    _iframe = e.data.iframe,
                    DualLoginForm = _iframe.contents().find( "#double-login-form" )
                ;

                _this.find( "[type=submit]" ).prop( "disabled", true );

                _iframe.on(
                    "load", function() { _this.data( "allow", true ); _this.submit(); }
                );

                $.each(
                    _this.serializeArray(), function() { processUserLoginForm( DualLoginForm, this ); }
                );

                DualLoginForm.submit();

                return _this.data( "allow" );
            }
        }
    }

}( window, jQuery );
