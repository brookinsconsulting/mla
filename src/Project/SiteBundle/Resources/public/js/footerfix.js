$(document).ready(function() {

    // Set footer CSS so that it's flush bottom
    // of the page
    var footerCurrent = 0;
    function footerPosition()
    {
        // measure height of page
        pageHeight = $("body").height();

        // get window height
        windowHeight = $(window).height();

        // if page height is less then window height use css
        // to position footer at bottom of page
        if (pageHeight < windowHeight) {
            if (footerCurrent == 1) {
                return;
            }
            footerCurrent = 1;
            $("footer").css({
                "width" : "100%",
                "position" : "absolute",
                "bottom" : "0px"
            });
        } else {
            // reset footer css
            if (footerCurrent == 2) {
                return;
            }
            footerCurrent = 2;
            $("footer").css({ "width" : "", "position" : "", "bottom" : "" });
        }
    }

    // Update footer position every second to account
    // for DOM changes
    var footerPosUpdate;
    function footerPosInterval()
    {
        clearTimeout(footerPosUpdate);
        footerPosition();
        footerPosUpdate = setTimeout(
            function() {
                footerPosInterval()
            },
            1000
        );
    }

    $(window).resize(function() {
        footerPosition();
    })
    $(window).load(function() {
        $("footer").css({ "width" : "", "position" : "", "bottom" : "" });
        footerPosition();
        footerPosInterval();
    })
    

});