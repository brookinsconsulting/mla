+function( global, $ ) {

    var JavaScriptCookie, JavaScriptCookieOptions;
    if(
        $.cookie( "javascript" ) && ( JavaScriptCookie = $.cookie( "javascript", JSON.parse ) )
    ) {

        if( JavaScriptCookie.status !== "enabled" ) {
            JavaScriptCookie.status = "enabled";

            JavaScriptCookieOptions = {
                "path" : JavaScriptCookie.path
            };

            if( JavaScriptCookie.expires ) {
                JavaScriptCookieOptions.expires = JavaScriptCookie.expires;
            }

            $.cookie(
                "javascript", JSON.stringify( JavaScriptCookie ), JavaScriptCookieOptions
            );
        }

    }

}( window, jQuery );
