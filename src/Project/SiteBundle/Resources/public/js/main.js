+function($) {

    // http://openam.github.io/bootstrap-responsive-tabs/
    // responsive tab init
    fakewaffle.responsiveTabs(['xs', 'sm']);

    // http://stackoverflow.com/questions/11167628/trees-in-twitter-bootstrap
    $(
        function() {
            $('.tree li').hide();
            $('.tree > ul > li').show();
            $('.tree li').on(
                'click',
                function(e) {
                    var
                        obj = $(this),
                        children = obj.find('> ul > li')
                    ;
                    if( children.length ) {
                        if(
                            children.is(":visible")
                        ) {
                            obj.removeClass('shown');
                            children.hide('fast');
                        } else {
                            obj.addClass('shown');
                            children.show('fast');
                        }
                    }
                    e.stopPropagation();
                }
            );
        }
    );

    /*-----------------------------------------------------------------------------------*/
    /*    ACCORDION TOGGLES
    /*-----------------------------------------------------------------------------------*/
    $(function(){

        $("#accordion h4").eq(2).addClass("active");
        $("#accordion .accordion_content").eq(2).show();

        $("#accordion h4").click(function(){
            $(this).next(".accordion_content").slideToggle("slow")
            .siblings(".accordion_content:visible").slideUp("slow");
            $(this).toggleClass("active");
            $(this).siblings("h4").removeClass("active");
        });

    });




    $(window).load(function(){
        $(".carousel").carousel(
            {
                interval: 8000
            }
        );
        $('.grey-background').css({'height': $('.homepage #main-content .row.greybg').height()+60+'px'});
    });




    $('.panel .panel-heading span.pull-right span').click(function(){
            $(this).parent().parent().parent().find('.panel-body.collapse').collapse('toggle');
            if($(this).hasClass('glyphicon-minus')){
                $(this).removeClass('glyphicon-minus');
                $(this).addClass('glyphicon-plus');
            }else{
                $(this).removeClass('glyphicon-plus');
                $(this).addClass('glyphicon-minus');
            }
    });




    var
        GlobalNavigation = $('#global-navigation'),
        DropdownMenuCache = {}
    ;

    GlobalNavigation.find('> li').each(
        function() {
            var
                Item = $(this),
                LocationID = Item.data('location-id')
            ;

            if(LocationID) {
                var
                    DropdownMenu = $.ajax(
                        {
                            "url": "/menu/mla_globalsubnav",
                            "data": {
                                "menu_root": LocationID
                            }
                        }
                    )
                ;

                DropdownMenu.then(
                    function(data) {
                        DropdownMenuCache[LocationID] = data;
                        Item.append(data);
                        handleDropdownMenu(Item);
                    }
                );
            }

        }
    );
    var patharray = window.location.pathname.split( '/' ),
        pathitem = '/'+patharray[1];
    $('#global-navigation li > a').each(function(){
        if($(this).attr('href') == pathitem){
            $(this).parent().addClass('current');
        }
    });

    function handleDropdownMenu(item) {
        if(
            $(window).width() < 992
        ) {
            item.addClass('dropdown').find('> a').attr("data-toggle", "dropdown").dropdown();
        } else {
            item.hover(
                function() {
                    item.find('.dropdown-menu').fadeIn(250);
                },
                function() {
                    item.find('.dropdown-menu').stop().fadeOut(250);
                }
            );
        }
    }

    function updateDropdownMenu() {
        GlobalNavigation.find('.dropdown-menu').remove()
        GlobalNavigation.find('> li').off().each(
            function() {
                var
                    Item = $(this),
                    LocationID = Item.data('location-id')
                ;

                Item.append(
                    DropdownMenuCache[LocationID]
                );
                handleDropdownMenu(Item);
            }
        );
    }

    var ResizeTimer;
    $(window).on(
        "resize", function() {
            clearTimeout(ResizeTimer);
            ResizeTimer = setTimeout(
                updateDropdownMenu, 100
            );
            $('.grey-background').css({'height':$('.homepage #main-content .row.greybg').height()+60+'px'});
        }
    );


    // Set footer CSS so that it's flush bottom
    // of the page
    function footerPosition()
    {
        // reset footer css
        $("footer").css({ "width" : "", "position" : "", "bottom" : "" });

        // measure height of page
        pageHeight = $("body").height();

        // get window height
        windowHeight = $(window).height();

        // if page height is less then window height use css
        // to position footer at bottom of page
        if (pageHeight < windowHeight) {
            $("footer").css({
                "width" : "100%",
                "position" : "absolute",
                "bottom" : "0px"
            });
        }
    }
    footerPosition();
    $(window).resize(function() {
        footerPosition();
    })
    $(window).load(function() {
        footerPosition();
    })

    // Update footer position every second to account
    // for DOM changes
    var footerPosUpdate;
    function footerPosInterval()
    {
        clearTimeout(footerPosUpdate);
        footerPosition();
        footerPosUpdate = setTimeout(
            function() {
                footerPosInterval()
            },
            1000
        );
    }
    footerPosInterval();


}(jQuery);


var is_chrome = window.chrome;
var is_safari = (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1);

if (is_chrome) {
    $('html').addClass('chrome');
}
else if (is_safari) {
    $('html').addClass('safari');
}


/*
* FlowType.JS v1.1
* Copyright 2013-2014, Simple Focus http://simplefocus.com/
*
* FlowType.JS by Simple Focus (http://simplefocus.com/)
* is licensed under the MIT License. Read a copy of the
* license in the LICENSE.txt file or at
* http://choosealicense.com/licenses/mit
*
* Thanks to Giovanni Difeterici (http://www.gdifeterici.com/)
*/

(function($) {
   $.fn.flowtype = function(options) {

// Establish default settings/variables
// ====================================
      var settings = $.extend({
         maximum   : 9999,
         minimum   : 1,
         maxFont   : 9999,
         minFont   : 1,
         fontRatio : 35
      }, options),

// Do the magic math
// =================
      changes = function(el) {
         var $el = $(el),
            elw = $el.width(),
            width = elw > settings.maximum ? settings.maximum : elw < settings.minimum ? settings.minimum : elw,
            fontBase = width / settings.fontRatio,
            fontSize = fontBase > settings.maxFont ? settings.maxFont : fontBase < settings.minFont ? settings.minFont : fontBase;
         $el.css('font-size', fontSize + 'px');
      };

// Make the magic visible
// ======================
      return this.each(function() {
      // Context for resize callback
         var that = this;
      // Make changes upon resize
         $(window).resize(function(){changes(that);});
      // Set changes on load
         changes(this);
      });
   };
}(jQuery));

// Custom Settings
// ======================
$('.homepage .greybg .ezxmltext-field h2').flowtype({
    minimum : 360,
    maximum : 1200,
    minFont : 21,
    maxFont : 50,
    fontRatio : 15
});
$('.homepage .greybg .ezxmltext-field p').flowtype({
    minimum : 360,
    maximum : 1200,
    minFont : 16,
    maxFont : 40,
    fontRatio : 20
});

// "/user" Membership History Table
$('#main-content .table.member-history-table').cardtable();

// Object States Customtag Tables
$('#main-content .tc-objectstate-list .table').stacktable();

// Custom Content Tables
$('#main-content .ezxmltext-field .cardtables').cardtable();
$('#main-content .ezxmltext-field .stackedtable').stacktable();
$('#main-content .ezxmltext-field .stackcols').stackcolumns();

// Nav collapse w/ outside click
$(document).ready(function() {

    $(document).click(function(e) {
        if ( $(e.target).parents("nav#primary-navigation").length == 0 ) {
            $("nav#primary-navigation .collapse").collapse("hide");
        }
    });
    $("nav#primary-navigation a[data-toggle='collapse']").click(function(e) {
        var clickTarget = e.target;
        $("nav#primary-navigation a[data-toggle='collapse']").each(function() {
            if (this != clickTarget) {
                $( $(this).attr("data-target") ).collapse("hide");
            }
        })
    });

    $("nav#primary-navigation a[data-toggle='collapse']").each(function() {
        $( $(this).attr("data-target") )
            .css({"display" : "none"})
            .collapse("show")
            .removeClass("collapsing")
            .one("shown.bs.collapse", function() {
                $(this)
                    .removeClass("in")
                    .css({"display" : ""})
                ;
            })
        ;
    });

});

// Banner Image Credit Toggle
$(document).ready(function() {
    $(".carousel .item .image-info").click(function() {
        if ($(this).hasClass("open")) {
            $(this).removeClass("open");
        } else {
            $(this).addClass("open");
        }
    });
});