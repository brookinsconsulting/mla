+function( global, $ ) {

    function processUserLoginForm( form, context ) {
        if( context.name == "login[_username]" || context.name == "login[_password]" ) {
            var LoginFieldName = (
                context.name == "login[_username]" ? "username" : "password"
            );

            form.find( "[name=" + LoginFieldName + "]" ).val( context.value );
        }
    }

    function onLoginFormSubmitEvent( e ) {
        var _this = $( this );

        if(
            !_this.data( "allow" )
        ) {
            var
                _iframe = e.data.iframe,
                DualLoginForm = _iframe.contents().find( "#double-login-form" )
            ;

            _this.find( "[type=submit]" ).prop( "disabled", true );

            _iframe.on(
                "load", function() { _this.data( "allow", true ); _this.submit(); }
            );

            $.each(
                _this.serializeArray(), function() { processUserLoginForm( DualLoginForm, this ); }
            );

            DualLoginForm.submit();

            return _this.data( "allow" );
        }
    }

    $(
        function() {

            var EventData = {
                "iframe" : $( '<iframe src="/double/form" width="0" height="0" style="display:none;"></iframe>' ).appendTo( "body" )
            };

            $( "#nav-login-dropdown form, .mla-form.form-login form, .mla-form.form-login2 form" )
                .data( "allow", false )
                .on(
                    "submit", EventData, onLoginFormSubmitEvent
                )
            ;

        }
    );

}( window, jQuery );
