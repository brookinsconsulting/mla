// Custom Javascript for Event Calendar
function tcCalendarMultiUpdate(form_element)
{
    var form_element = $(form_element);
    $(".tc-calendar[data-instance-id]").each(function() {
        data = {
            "location_id" : $(this).attr("data-location-id"),
            "offset" : $(this).attr("data-offset"),
            "date" : $(this).attr("data-date")
        };
        tcUpdateCalendarView(
            $(this).attr("data-instance-id"),
            form_element.serialize() + "&" + $.param(data)

        );
    })
}

$(document).ready(function() {
    $(".tc-calendar form.tc-calendar-controls input, .tc-calendar form.tc-calendar-controls select").removeAttr("disabled");
    $(".tc-calendar form.tc-calendar-controls .filter-list .checkbox input[type=checkbox]").hide();
    $(".tc-calendar form.tc-calendar-controls .filter-list .checkbox input[type=checkbox]").change(function() {
        if (!$(this).is(":checked")) {
            $(this).parent().find(".colorbox").css({"opacity" : .5});
            $(this).parent().find("span").css({"color" : "gray"});
        } else {
            $(this).parent().find(".colorbox").css({"opacity" : ""});
            $(this).parent().find("span").css({"color" : ""});
        }
        tcCalendarMultiUpdate(
            $(this).parents("form.tc-calendar-controls")
        );
    });
    $(".tc-calendar form.tc-calendar-controls select").change(function() {
        tcCalendarMultiUpdate(
            $(this).parents("form.tc-calendar-controls")
        );
    });
});

