$(document).ready(function() {
    $(".readmore").each(function() {
        var self = this;
        $(self).find(".read-more-link").click(function() {
            if ($(self).hasClass("open")) {
                $(self).removeClass("open");
                $(self).find(".read-more-content").height( 0 );
            } else {
                $(self).addClass("open");
                $(self).find(".read-more-content").height( $(self).find(".read-more-inner-content").height() + 15 );
            }
            return false;
        });
        $(self).find(".read-less-link").click(function() {
            $(self).removeClass("open");
            $(self).find(".read-more-content").height( 0 );
            return false;
        });
    });
});