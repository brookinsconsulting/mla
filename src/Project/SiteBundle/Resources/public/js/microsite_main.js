+function( global, $ ) {

    $(
        function() {

            $( ".bulletin-issue" )
                .data(
                    "bulletin-accordion-template", "articles_list"
                )
            ;

            BulletinManager.initializeBulletinIssueList(
                {
                    "allow_ignore" : true,
                    "selector" : "[data-bulletin-accordion]",
                    "template_list" : {
                        "articles_list" : '<div class="article-list">{{#each article_list}}{{#if article_category }}<div class="row"><div class="col col-xs-12"><span class="category-level-{{level}}">{{article_category}}</span></div></div>{{else}}<div class="row"><div class="page-number col col-xs-1">{{#compare @key "!=" 999 }}{{#compare @key "!=" 0 }}{{@key}}{{/compare}}{{/compare}}</div><div class="col col-xs-11"><a class="bulletin-accordion-ignore" href="{{url}}">{{{name}}}</a></div></div>{{/if}}{{/each}}</div>'
                    }
                }
            );


            function runBulletinSearch( context, url ) {
                context.empty().append(
                    '<div class="loading">Loading...</div>'
                );

                $.ajax( { "url" : url } )
                    .then(
                        function( response ) {
                            context.empty().append( response );
                        }
                    )
                ;
            }

            function runSearchHistoryAction( e ) {
                if( history.state ) {
                    runBulletinSearch(
                        $( history.state.context ), history.state.url
                    );
                }
            }

            function changeQuery ( url, href ) {
                var SearchResultsContext = $( "#bulletin-search-results" );
                history.pushState(
                    { "url" : url, "context" : "#bulletin-search-results" }, document.title, href
                );

                SearchResultsContext.data(
                    "search-query", href.substring(1)
                );

                runBulletinSearch( SearchResultsContext, url );
            }

            +function() {
                var
                    SearchResultsContext = $( "#bulletin-search-results" ),
                    SearchQueryString = SearchResultsContext.data( "search-query" )
                ;

                SearchResultsContext.on(
                    "click", ".pager a", function() {
                        changeQuery( this.href, this.getAttribute( "href" ) );
                        return false;
                    }
                );

                if( SearchQueryString ) {
                    history.pushState(
                        { "url" : "/bulletin/search?" + SearchQueryString, "context" : "#bulletin-search-results" }, document.title
                    );
                    runBulletinSearch(
                        SearchResultsContext, "/bulletin/search?" + SearchQueryString
                    );
                }

                $(window).on( "popstate", runSearchHistoryAction );

                var category = '',
                    query = window.location.href.split('&category[]=')[0].split('/bulletin/search?')[1];
                $('#sidebar .form-group .checkbox input')
                    .on( "click",
                        function() {
                            category = '';
                            $('#sidebar .form-group .checkbox input')
                                .each(
                                    function() {
                                        if($(this).prop("checked")) {
                                            category += '&category[]=' + $(this).attr("value");
                                        }
                                    }
                                )
                            ;
                            category = category.split(' ').join('+');
                            changeQuery( '?' + query + category, "/bulletin/search?" + query + category );
                        }
                    )
                ;

            }();


            var EventData = {
                "iframe" : $( '<iframe src="/double/form" width="0" height="0"></iframe>' ).css( { "display" : "none" } ).appendTo( "body" )
            };

            $( "#nav-login-dropdown form" )
                .data( "allow", false )
                .on(
                    "submit", EventData, MultiLogin.onSubmitEvent
                )
            ;

        }
    );

    $('.panel .panel-heading span.pull-right span').click(function(){
            $(this).parent().parent().parent().find('.panel-body.collapse').collapse('toggle');
            if($(this).hasClass('glyphicon-minus')){
                $(this).removeClass('glyphicon-minus');
                $(this).addClass('glyphicon-plus');
            }else{
                $(this).removeClass('glyphicon-plus');
                $(this).addClass('glyphicon-minus');
            }
    });

}( window, jQuery );