// Search Ajax
function bindSearchInputs()
{

    function ajaxSubmit() {
        applyThrobbers();
        window.history.pushState(null, null, $("#search-page form").attr("action") + "?" + $("#search-page form").serialize() + "&offset=0");
        $.get(
            $("#search-page form").attr("action"),
            $("#search-page form").serialize() + "&offset=0",
            function(data)
            {
                $("#search-page").html(data);
                $("#search-page form").find("input,select").prop("disabled", false);
                bindSearchInputs();
                bindSearchLinks();

            },
            "html"
        );
        $("#search-page form").find("input,select").prop("disabled", true);
        $("ul.facets input[type=checkbox]").prop("disabled", true);
    }

    $("#search-page form").submit(function()
    {
        ajaxSubmit();
        return false;
    });

    $("#filter-options ul.facets input[type=checkbox]").change(function()
    {
        ajaxSubmit();
    });

    $('.search-item .content-view-line .description').each(function() {
        $(this).html(
            $.truncate(
                $(this).html(),
                {
                    length: 256,
                    stripTags: false,
                    words: true,
                    noBreaks: false,
                    ellipsis: "..."
                }
            )
        );
    });
}

function bindSearchLinks()
{
    $("#search-page .current-filters a, #search-page .pagination a, #search-page .search-sort .dropdown a").click(function()
    {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        applyThrobbers();
        window.history.pushState(null, null, $(this).attr("href"));
        $.get(
            $(this).attr("href"),
            "",
            function(data)
            {
                $("#search-page").html(data);
                $("#search-page form").find("input,select").prop("disabled", false);
                bindSearchInputs();
                bindSearchLinks();
            },
            "html"
        );
        $("#search-page form").find("input,select").prop("disabled", true);
        return false;
    });            
}

function applyThrobbers()
{

    $(".search-results").addClass("loading");
    $(".current-filters a, .pagination a, ul.facets")
        .css({"color" : "#e9e9e9"})
    ;
}

$(document).ready(function()
{

    // bind ajax event to inputs
    bindSearchInputs();

    // bind ajax events to search links
    bindSearchLinks();

    // truncate descriptions
    $('.search-item .content-view-line .description').each(function() {
        $(this).html(
            $.truncate(
                $(this).html(),
                {
                    length: 256,
                    stripTags: false,
                    words: true,
                    noBreaks: false,
                    ellipsis: "..."
                }
            )
        );
    });

});