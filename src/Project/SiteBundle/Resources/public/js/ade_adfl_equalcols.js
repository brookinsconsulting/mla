function equalCols()
{
    // other col height
    otherColHeight = $("#sidebar").innerHeight();

    // get window height
    windowHeight = $(window).height();

    // match window height
    if (windowHeight > otherColHeight) {
        $("#main-content-area #right-column, #main-content-area .search-results").css({
            "min-height" : (
                    windowHeight -
                    $("#utility-bar").innerHeight() -
                    $("section header").innerHeight() -
                    $("footer").innerHeight()
                )
            }
        );

    // match other col's height
    } else {
        $("#main-content-area #right-column, #main-content-area .search-results").css({
            "min-height" : (
                    otherColHeight
                )
            }
        );
    }

}

$(document).ready(function() {
    equalCols();
});
$(window).resize(function() {
    equalCols();
})
$(window).load(function() {
    equalCols();
})
