// allows state selection field to update based
// on country field selection
$(document).ready(function() {

    function countryChange(id, options)
    {
        if (
            $("select[data-state-select='" + id + "/country']").length == 0 ||
            $("select[data-state-select='" + id + "/state']").length == 0
        ) {
            return;
        }

        var countrySelection = $("select[data-state-select='" + id + "/country'] option:selected").text();
        var stateSelect = $("select[data-state-select='" + id + "/state']");
        setFeedback($(stateSelect).parent().parent(), FEEDBACK_NONE, "");
        $(stateSelect).find("option[value!='']").remove();

        if (countrySelection in options) {
            var selectValue = $(stateSelect).val();
            for (var key in options[countrySelection]) {
                $(stateSelect).append(
                    '<option value="' + key + '">' + options[countrySelection][key] + '</option>'
                );
            }
            $(stateSelect).slideDown();
            $(stateSelect).append('<option value="_other">Other</option>');
            $(stateSelect).val(selectValue);
            $(stateSelect).trigger("change");

        } else {
            if ($("select[data-state-select='" + id + "/country']").val()) {
                $(stateSelect).append('<option value="_other">Other</option>');
                $(stateSelect).val("_other");
                $(stateSelect).trigger("change");
                $(stateSelect).slideUp();
            } else {
                $(stateSelect).slideDown();
                $(stateSelect).val("");
                $(stateSelect).trigger("change");
            }
        }
        

    }

    $("select[data-state-select$='/state']").each(function() {

        var originalValue = $(this).val();

        // get all optgroups which should represent countries
        // and store then in options array
        var options = {};
        $(this).find("optgroup").each(function() {
            var optgroup = this;
            options[$(optgroup).attr("label")] = {};
            $(this).find("option").each(function() {
                options[$(optgroup).attr("label")][$(this).attr("value")] = $(this).text();
            });
        });

        // delete optgroups
        $(this).find("optgroup").remove();

        // get id
        var id = $(this).attr("data-state-select").split("/")[0];

        // flag this select field
        $(this).addClass("state-select-js");

        // country change updating
        countryChange(id, options);
        $("select[data-state-select='" + id + "/country']").change(function() {
            countryChange(id, options);
        });

        if (!originalValue) {
            $(this).val(
                $(this).parent().parent().find(".choiceother-text input").val()
            );
        } else {
            $(this).val(originalValue);
        }

        $(this).click(function() {
            if ($(this).parent().parent().find(".alert").length == 0) {
                if (!$("select[data-state-select='" + id + "/country']").val()) {
                    setFeedback($(this).parent().parent(), FEEDBACK_ERROR, "You must select a country.");
                    $(this).parent().parent().find(".alert").remove();
                    $(this).parent().parent().find("label").after('<div class="alert alert-warning">You must select a country.</div>');
                }
            }
        });

    })
});