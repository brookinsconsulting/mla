<?php


namespace Project\SiteBundle\Persistence\Legacy\Content\FieldValue\Converter;
use eZ\Publish\Core\FieldType\FieldSettings,
 eZ\Publish\Core\Persistence\Legacy\Content\FieldValue\Converter,
 eZ\Publish\Core\Persistence\Legacy\Content\StorageFieldValue,
 eZ\Publish\Core\Persistence\Legacy\Content\StorageFieldDefinition,
 eZ\Publish\SPI\Persistence\Content\FieldValue,
 eZ\Publish\SPI\Persistence\Content\Type\FieldDefinition,
 DOMDocument;

class eZTags implements Converter
{
    /**
     * Factory for current class
     *
     * @note Class should instead be configured as service if it gains dependencies.
     *
     * @return eZTags
     */
    public static function create()
    {
        return new self;
    }

    /**
     * Converts data from $value to $storageFieldValue
     *
     * @param \eZ\Publish\SPI\Persistence\Content\FieldValue $value
     * @param \eZ\Publish\Core\Persistence\Legacy\Content\StorageFieldValue $storageFieldValue
     */
    public function toStorageValue( FieldValue $value, StorageFieldValue $storageFieldValue )
    {
        $storageFieldValue->dataText = $this->generateXmlString( $value->data );
    }

    /**
     * Converts data from $value to $fieldValue
     *
     * @param \eZ\Publish\Core\Persistence\Legacy\Content\StorageFieldValue $value
     * @param \eZ\Publish\SPI\Persistence\Content\FieldValue $fieldValue
     */
    public function toFieldValue( StorageFieldValue $value, FieldValue $fieldValue )
    {
        $fieldValue->data = $this->restoreValueFromXmlString( $value->dataText );
    }

    /**
     * Converts field definition data in $fieldDef into $storageFieldDef
     *
     * @param \eZ\Publish\SPI\Persistence\Content\Type\FieldDefinition $fieldDef
     * @param \eZ\Publish\Core\Persistence\Legacy\Content\StorageFieldDefinition $storageDef
     */
    public function toStorageFieldDefinition( FieldDefinition $fieldDef, StorageFieldDefinition $storageDef )
    {
        // Nothing to store
    }

    /**
     * Converts field definition data in $storageDef into $fieldDef
     *
     * @param \eZ\Publish\Core\Persistence\Legacy\Content\StorageFieldDefinition $storageDef
     * @param \eZ\Publish\SPI\Persistence\Content\Type\FieldDefinition $fieldDef
     */
    public function toFieldDefinition( StorageFieldDefinition $storageDef, FieldDefinition $fieldDef )
    {
        $fieldDef->defaultValue->data = array();
    }

    /**
     * Returns the name of the index column in the attribute table
     *
     * Returns the name of the index column the datatype uses, which is either
     * "sort_key_int" or "sort_key_string". This column is then used for
     * filtering and sorting for this type.
     *
     * @return string
     */
    public function getIndexColumn()
    {
        return false;
    }

    /**
     * Generates XML string from $eztagsValue to be stored in storage engine
     *
     * @param array $eztagsValue
     *
     * @return string The generated XML string
     */
    private function generateXmlString( array $eztagsValue )
    {
        $doc = new DOMDocument( '1.0', 'utf-8' );

        $root = $doc->createElement( 'ezeztags' );
        $doc->appendChild( $root );

        $eztagss = $doc->createElement( 'eztagss' );
        $root->appendChild( $eztagss );

        foreach ( $eztagsValue as $eztags )
        {
            $eztagsNode = $doc->createElement( 'eztags' );
            $eztagsNode->setAttribute( 'id', $eztags["id"] );
            $eztagsNode->setAttribute( 'name', $eztags["name"] );
            $eztagsNode->setAttribute( 'email', $eztags["email"] );
            $eztagss->appendChild( $eztagsNode );
            unset( $eztagsNode );
        }

        return $doc->saveXML();
    }

    /**
     * Restores an eztags Value object from $xmlString
     *
     * @param string $xmlString XML String stored in storage engine
     *
     * @return \eZ\Publish\Core\FieldType\eZTags\Value
     */
    private function restoreValueFromXmlString( $xmlString )
    {
        $dom = new DOMDocument( '1.0', 'utf-8' );
        $eztagss = array();

        if ( $xmlString && $dom->loadXML( $xmlString ) === true )
        {
            foreach ( $dom->getElementsByTagName( 'eztags' ) as $eztags )
            {
                $eztagss[] = array(
                    'id' => $eztags->getAttribute( 'id' ),
                    'name' => $eztags->getAttribute( 'name' ),
                    'email' => $eztags->getAttribute( 'email' )
                );
            }
        }

        return $eztagss;
    }
}
