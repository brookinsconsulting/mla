<?php

namespace Project\SiteBundle\Routing\Generator;

use eZ\Publish\Core\MVC\Symfony\Routing\Generator\UrlAliasGenerator;
use eZ\Publish\API\Repository\Repository;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use Symfony\Component\Routing\RouterInterface;
use Project\SiteBundle\Services\BulletinUrlGenerator;

class SiteUrlAliasGenerator extends UrlAliasGenerator
{

    /**
     * @var \eZ\Publish\Core\Repository\Repository
     */
    protected $repository;

    /**
     * The default router (that works with declared routes).
     *
     * @var \Symfony\Component\Routing\RouterInterface
     */
    protected $defaultRouter;

    public function __construct( Repository $repository, RouterInterface $defaultRouter, ConfigResolverInterface $configResolver )
    {
        $this->repository = $repository;
        $this->defaultRouter = $defaultRouter;
        parent::__construct($repository, $defaultRouter, $configResolver);
    }

    /**
     * Override eZ's UrlAliasGenerator to check for bullets
     * content objects and build a custom URL for them
     */
    public function doGenerate( $location, array $parameters )
    {

        if (!$location) {
            return "";
        }

        // get the bulletin url generator and check
        $bulletinUrlGenerator = new BulletinUrlGenerator(
            $this->repository->getContentService(),
            $this->defaultRouter
        );
        if ($bulletinUrlGenerator->check($location->contentInfo)) {
            return $bulletinUrlGenerator->generate($location->contentInfo, $parameters);
        }

        // fallback to parent
        return parent::doGenerate($location, $parameters);

    }
}