<?php

namespace Project\SiteBundle\Classes\DependencyInjection;

use eZ\Publish\Core\MVC\Symfony\SiteAccess;
use ThinkCreative\LegacyBundle\DependencyInjection\SettingsInjectionInterface;

class SiteSettingsInjection implements SettingsInjectionInterface
{

    protected $SiteAccess;

    public function __construct(SiteAccess $siteaccess) {
        $this->SiteAccess = $siteaccess;
    }

    public function getSettings() {
        $SiteAccessConfiguration['contentstructuremenu.ini']['TreeMenu']['ShowClasses'] = array(
            'folder', 'user_group', 'wiki_page', 'event_calendar', 'landing_page', 'forums', 'gallery', 'imported_page', 'imported_section', 'section_home', 'product_category', 'link', 'faq_list'
        );
        $SiteAccessConfiguration['content.ini']['link']['AvailableClasses'] = array(
            'donate', 'renew', 'joinnow'
        );
        $SiteAccessConfiguration['site.ini']['ContentSettings']['RedirectAfterPublish'] = 'node';
        return $SiteAccessConfiguration;
    }

}