<?php

namespace Project\SiteBundle\Classes\DependencyInjection;

use ThinkCreative\BridgeBundle\Services\CustomTagsManager;
use ThinkCreative\BridgeBundle\DependencyInjection\CustomTagsHandlerInterface;

class CustomTags implements CustomTagsHandlerInterface
{
    public function registerCustomTagList(CustomTagsManager $customtags_manager) {
        $CustomTags = array(
            "quote" => array(
                "attributes" => array(
                    "author" => ""
                ),  
                "template" => "ProjectSiteBundle:customtags:quote.html.twig",
            ),
            "sup" => array(
                "attributes" => array(),
                "inline" => true,
                "template" => "ProjectSiteBundle:customtags:sup.html.twig",
            ),
            "literal" => array(
                "attributes" => array(
                    "type" => array(
                        "name" => "Type",
                        "type" => "select",
                        "selection" => array(
                            "html" => "HTML",
                        ),
                    ),
                ),
                "controller" => "ProjectSiteBundle:Literal:index",
                "template" => "ProjectSiteBundle:customtags:literal.html.twig",
            ),
            "jilpdf-list" => array(
                "attributes" => array(
                    "source" => array(
                        "name" => "Source",
                        "type" => "link",
                    ),
                ),
                "controller" => "ProjectSiteBundle:JILPDFList:index",
                "template" => "ProjectSiteBundle:customtags:jilpdf_list.html.twig",
            ),
            "bulletin-issue-list" => array(
                "attributes" => array(
                    "source" => array(
                        "name" => "Source",
                        "type" => "link",
                    ),
                ),
                "controller" => "ProjectSiteBundle:Bulletin:IssueList",
                "template" => "ProjectSiteBundle:customtags:bulletin_issue_list.html.twig",
            ),
            "currentbulletinissue" => array(
                "attributes" => array(
                    "source" => array(
                        "name" => "Source",
                        "type" => "link",
                    ),
                ),
                "controller" => "ProjectSiteBundle:Bulletin:CurrentIssue",
                "template" => "ProjectSiteBundle:customtags:current_bulletin_issue.html.twig",
            ),
            "separator" => array(
                "template" => "ProjectSiteBundle:customtags:separator.html.twig",
            ),
            "section-search" => array(
                "attributes" => array(
                    "title" => "",
                    "source" => array(
                        "name" => "Source",
                        "type" => "link",
                    ),
                ),
                "controller" => "ProjectSiteBundle:General:SearchForm",
                "template" => "ProjectSiteBundle:customtags:search_form.html.twig",
            ),
            "read-more" => array(
                "attributes" => array(
                    "style" => array(
                        "name" => "Style",
                        "type" => "select",
                        "selection" => array(
                            "default" => "Default",
                            "faq" => "FAQ"
                        ),
                    ),
                    "read_more_text" => array(
                        "name" => "Read More Text"
                    ),
                    "read_less_text" => array(
                        "name" => "Read Less Text"
                    ),
                ),
                "inline" => false,
                "template" => "ProjectSiteBundle:customtags:read_more.html.twig",
            ),
        );

        foreach($CustomTags as $Name => $CustomTagDefinition) {
            $customtags_manager->register(
                $Name, $CustomTagDefinition
            );
        }
    }
}