<?php

namespace Project\SiteBundle\Classes;

class LocationSearch {

	public static function locationSearch($container = NULL, $parentlocationId = NULL, $contentTypeIdentifier = "", $contentTypeID = "" ,$subtree = "", $language = "", $visibility = "", $status = "", $location_remote_id = "", $remote_id = "", $object_state_id = "", $url_alias = "", $sort = "",  $offset = 0, $limit = 20, $content_id = false, $filter = "")
	{
		if($parentlocationId === NULL){
			return $this->render(
				"ProjectSiteBundle:error:subtree.html.twig",
				array(),
				$response
			);
		}

		$fetcher = $container->get('aw_ezp_fetch');

		$ContentTypeIdentifier = (
			$contentTypeIdentifier ? ", content_type_identifier: {EQ '$contentTypeIdentifier'}" : ""
		);
		$ContentTypeID = (
			$contentTypeID ? ", status: {EQ '$contentTypeID'}" : ""
		);
		$Subtree = (
			$subtree ? ", subtree: {EQ '$subtree'}" : ""
		);
		$Language = (
			$language ? ", language_code: {EQ '$langage'}" : ""
		);
		$Status = (
			$status ? ", status: {EQ '$subtree'}" : ""
		);
		$Visibility = (
			$visibility ? ", visibility: {EQ '$visibility'}" : ""
		);
		$Location_remote_id = (
			$location_remote_id ? ", location_remote_id: {EQ '$location_remote_id'}" : ""
		);
		$Remote_id = (
			$remote_id ? ", remote_id: {EQ '$remote_id'}" : ""
		);
		$Object_state_id = (
			$object_state_id ? ", object_state_id: {EQ '$object_state_id'}" : ""
		);
		$Url_alias = (
			$url_alias ? ", object_state_id: {EQ '$object_state_id'}" : ""
		);
		$Filter = (
			$filter ? ", $filter" : ""
		);
		$query = "{filter: {AND [parent_location_id: {EQ $parentlocationId}$ContentTypeIdentifier$Subtree$Language$Visibility$Location_remote_id$Remote_id$Object_state_id$Url_alias$Filter]}, limit: $limit, offset: $offset";
		$query .= "}";

		$result = $fetcher->fetch($query);

		return $result->searchHits;
	}
}