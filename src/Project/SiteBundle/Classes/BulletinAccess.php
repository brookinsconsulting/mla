<?php

namespace Project\SiteBundle\Classes;

/**
 * Class with methods and variables to help determine
 * if current user should be granted access to
 * bulletin's based on their IP address
 */
class BulletinAccess
{

    /**
     * Id of bulletin_article content class
     * @var integer
     */
    const BULLETIN_ARTICLE_ID = 45;

    /**
     * Id of bulletin_issue content class
     * @var integer
     */
    const BULLETIN_ISSUE_ID = 46;

    /**
     * Field identifiers (seperated by comma) for bulletin PDF
     * @var string
     */
    const BULLETIN_PDF_FIELD_IDENTIFIERS = "file,issue_pdf";

    /**
     * Array of IP addresses that should be granted access
     * @var array
     */
    public static $ipAccessList = array(
        '128.122.',
        '128.8.',
        '129.2.',
        '131.118.',
        '206.196.',
        '136.160.',
        '129.2.',
        '130.85.',
        '131.118.',
        '131.94.',
        '136.165.',
        '130.127.',
        '198.21.',
        '129.174.',
        '198.190.',
        '199.17.',
        '128.174.',
        '130.126.',
        '192.17.',
        '129.137.',
        '148.61.',
        '207.72.',
        '35.40.',
        '138.237.',
        '147.26.',
        '129.72.',
        '128.195.',
        '128.200.',
        '160.87.',
        '169.234.',
        '74.113.',
        '141.161.',
        '158.65.',
        '128.151.',
        '70.32.',
        '165.106.',
        '128.42.',
        '168.7.',
        '132.203.',
        '134.76.',
        '130.160.',
        '192.75.',
        '199.60.',
        '207.23.',
        '142.58.',
        '38.105.236.130',
        //'71.229.20.',
    );

    /**
     * Compare two IP addresses to determine if there
     * is a match. Supports wild cards.
     * @param string $ip1
     * @param string $ip2
     * @return boolean  True if matching
     */
    static function compareIpAddresses($ip1, $ip2)
    {

        // convert to array
        $ips = array(
            explode(".", trim($ip1)),
            explode(".", trim($ip2))
        );

        // validate / fix ips
        foreach ($ips as &$ip) {
            for ($i = 0; $i < 4; $i++) {
                // any empty spots will be replaced with a wildcard
                if (!isset($ip[$i]) || !$ip[$i]) {
                    $ip[$i] = "*";
                    continue;
                }

                // wildcard is ok
                if ($ip[$i] == "*") {
                    continue;
                }

                // only numeric values
                if (!is_numeric($ip[$i])) {
                    return false;
                }
            }
        }

        // convert to ints in ranges
        $ipRanges = array();

        foreach ($ips as &$ip) {

            // make ip string again
            $ipString = implode(".", $ip);

            // convert to ints
            $ipRanges[] = array(
                ip2long(str_replace("*", "0", $ipString)),
                ip2long(str_replace("*", "255", $ipString))
            );
        }

        // ensure ips are in range
        if ($ipRanges[0][0] <= $ipRanges[1][1] && $ipRanges[0][1] >= $ipRanges[1][0]) {
            return true;
        }
        return false;       

    }
}