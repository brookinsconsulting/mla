<?php

namespace Project\SiteBundle\EventListener;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use eZ\Publish\Core\MVC\Legacy\LegacyEvents;
use eZ\Publish\Core\MVC\Legacy\Event\PostBuildKernelEvent;
use eZ\Publish\API\Repository\Repository;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;
use Project\SiteBundle\Classes\BulletinAccess;
use Project\SiteBundle\Exception\Legacy\BulletinPdfAccessDeniedException;
use Project\MemberBundle\Services\Member as MemberService;
use Project\MemberBundle\Classes\Member;
use ezpEvent;
use ezpKernel;

/**
 * Uses SiteAccessMatch event to check if user is trying to
 * access a bulletin PDF and determines access
 */
class BulletinAccessListener implements EventSubscriberInterface
{

    /**
     * @var eZ\Publish\API\Repository\Repository
     */
    private $ezPublishApiRepository;

    /**
     * @var Project\MemberBundle\Services\Member
     */
    private $memberService;

   /**
    * @param Repository $ezPublishApiRepository
    * @param ChainRouter $router
    */
    public function __construct( Repository $ezPublishApiRepository, MemberService $memberService )
    {
        $this->ezPublishApiRepository = $ezPublishApiRepository;
        $this->memberService = $memberService;
    }

    public static function getSubscribedEvents()
    {
        return array(
            LegacyEvents::POST_BUILD_LEGACY_KERNEL => array( 'onPostBuildLegacyKernel', 128 )
        );
    }

    /**
     * Capture content/download events
     */
    public function onPostBuildLegacyKernel(PostBuildKernelEvent $event)
    {
        // attach event to ezpEvent
        $ezpEvent = ezpEvent::getInstance();
        $ezpEvent->attach(
            "content/download",
            array(
                $this,
                "contentDownloadCallback"
            )
        );
    }

    /**
     * Called when user attempts to access "content/download" module.
     * Check if user is accessing bulletin PDF download and ensure
     * they have permission.
     * @param integer $object_id
     * @param integer $object_attribute_id
     * @throws BulletinPdfAccessDeniedException
     */
    public function contentDownloadCallback($object_id = null, $object_attribute_id = null)
    {

        // load content
        $content = $this->ezPublishApiRepository->getContentService()->loadContent($object_id);

        // ensure content is bulletin_article OR bulletin_issue
        if (!in_array($content->contentInfo->contentTypeId, array(BulletinAccess::BULLETIN_ARTICLE_ID, BulletinAccess::BULLETIN_ISSUE_ID))) {
            return;
        }

        // ensure object attribute is for the bulletin PDF
        $hasField = false;
        foreach (explode(",", BulletinAccess::BULLETIN_PDF_FIELD_IDENTIFIERS) as $pdfIdentifier) {
            $field = $content->getField(trim($pdfIdentifier));
            if ($field && $field->id == $object_attribute_id) {
                $hasField = true;
                break;
            }
        }
        if (!$hasField) {
            return;
        }

        // get current member
        $member = $this->memberService->getCurrentMember();

        // if current user is full member then they have access
        if ($member && $member->getMemberStatus() != Member::MEMBER_STATUS_JOIN) {
            return;
        }

        // get request
        $request = Request::createFromGlobals();

        // check if ip address has access
        foreach (BulletinAccess::$ipAccessList as $ip) {
            // match found
            if (BulletinAccess::compareIpAddresses($request->getClientIp(), $ip)) {
                return;
                break;
            }
        }

        // get issue location
        if ($content->contentInfo->contentTypeId == BulletinAccess::BULLETIN_ARTICLE_ID) {
            $articleLocation = $this->ezPublishApiRepository->getLocationService()->loadLocation($content->contentInfo->mainLocationId);
            $issueLocation = $this->ezPublishApiRepository->getLocationService()->loadLocation($articleLocation->parentLocationId);
        } elseif ($content->contentInfo->contentTypeId == BulletinAccess::BULLETIN_ARTICLE_ID) {
            $issueLocation = $this->ezPublishApiRepository->getLocationService()->loadLocation($content->contentInfo->mainLocationId);
        } else {
            return;
        }

        // check if most recent bulletin and allow download if so
        $query = new Query();
        $query->criterion = new Criterion\LogicalAnd(array(
            new Criterion\Visibility( Criterion\Visibility::VISIBLE ),
            new Criterion\ParentLocationId( $issueLocation->parentLocationId ),
            new Criterion\ContentTypeId( BulletinAccess::BULLETIN_ISSUE_ID )
        ));
        $query->sortClauses = array(
            new SortClause\Field('bulletin_issue', 'volume_number', Query::SORT_DESC, "eng-US"),
            new SortClause\Field('bulletin_issue', 'issue_number', Query::SORT_DESC, "eng-US"),
            new SortClause\DatePublished()
        );
        $query->limit = 1;
        $searchResults = $this->ezPublishApiRepository->getSearchService()->findContent($query);

        if (isset($searchResults->searchHits[0]->valueObject)) {
            if ($searchResults->searchHits[0]->valueObject->contentInfo->id == $issueLocation->contentInfo->id) {
                return;
            }
        }

        // get legacy kernel instance
        $kernel = ezpKernel::instance();

        // throw legacy access denied exception
        $kernel->runCallback(
            function() {
                throw new BulletinPdfAccessDeniedException("You do not have permission to download this bulletin PDF file.");
            }
        );        
        return;

    }

}