<?php

namespace Project\SiteBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use ThinkCreative\MenubarBundle\Services\Menubar;
use ThinkCreative\MenubarBundle\Events\MenubarEvents;
use ThinkCreative\MenubarBundle\Events\CreateMenubarEvent;
use ThinkCreative\MenubarBundle\Classes as MenubarClasses;
use ThinkCreative\BridgeBundle\Services\MenubarBridge;

use Symfony\Component\HttpFoundation\ParameterBag;

class MenubarListener implements EventSubscriberInterface
{

    protected $MenubarService;
    protected $MenubarBridgeService;
    protected $MenubarList;

    public function __construct(Menubar $menubar, MenubarBridge $menubar_bridge) {
        $this->MenubarService = $menubar;
        $this->MenubarBridgeService = $menubar_bridge;
        $this->MenubarList = new ParameterBag();
    }

    public function onCreateMenubar(CreateMenubarEvent $event) {
        $Menubar = $event->getMenubar();

        if(
            strpos($Menubar->getClass(), 'global-menubar-count') !== false
        ){
            $this->MenubarList->set(
                $Menubar->getDataProperties()->get('id'), count( $Menubar->getItems() )
            );

            $Menubar->getDataProperties()->set(
                'total_count', count( $Menubar->getItems() )
            );

            $Menubar->getDataProperties()->set(
                'subitem_count_list', array()
            );
        }

        if(
            strpos($Menubar->getClass(), 'global-submenu-count') !== false
        ) {
            $MenubarIDList = explode(
                '|', $Menubar->getDataProperties()->get('id')
            );

            $CurrentCount = $this->MenubarList->get($MenubarIDList[2]);

            $this->MenubarList->set(
                $MenubarIDList[2], $CurrentCount + count( $Menubar->getItems() )
            );

            if(
                $ParentMenubar = $this->MenubarService->getMenubar($MenubarIDList[2])
            ) {
                $ParentMenubar->getDataProperties()->set(
                    'total_count', $CurrentCount + ($SubitemCount = count( $Menubar->getItems() ))
                );

               $SubitemCountList =  $ParentMenubar->getDataProperties()->get('subitem_count_list');

                foreach(
                    $ParentMenubar->getItems() as $Key => $Item
                ) {
                    if(
                        $Item->getDataProperties()->get('location_id') == $MenubarIDList[1]
                    ) {
                        $SubitemCountList[$Key] = $SubitemCount;
                        $ParentMenubar->getDataProperties()->set(
                            'subitem_count_list', $SubitemCountList
                        );
                        break;
                    }
                }
            }

        }

    }

    public static function getSubscribedEvents() {
        return array(
            MenubarEvents::CREATE_MENUBAR => array('onCreateMenubar'),
        );
    }

}