<?php

namespace Project\SiteBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class JavaScriptListener implements EventSubscriberInterface
{

    protected $RequestViewHandler;

    public function __construct( $request_view_handler ) {
        $this->RequestViewHandler = $request_view_handler;
    }

    public function onKernelController( FilterControllerEvent $event ) {

        $Request = $event->getRequest();

        if(
            $Request->cookies->has( 'javascript' )
        ) {
            $JavaScriptCookie = json_decode( $Request->cookies->get( 'javascript' ) );
            $this->RequestViewHandler->set(
                'javascript_enabled', $JavaScriptCookie->status === 'enabled'
            );
        }

    }

    public function onKernelResponse( FilterResponseEvent $event ) {

        $Request = $event->getRequest();
        $Response = $event->getResponse();

        if(
            !$Request->cookies->has( 'javascript' )
        ) {
            $JavaScriptCookie = array(
                "status" => 'disabled',
                "expires" => 0,
                "path" => '/',
                "domain" => null,
                "secure" => false,
            );

            $Response->headers->setCookie(
                new Cookie(
                    'javascript', json_encode( $JavaScriptCookie ), $JavaScriptCookie["expires"], $JavaScriptCookie["path"], $JavaScriptCookie["domain"], $JavaScriptCookie["secure"], false
                )
            );
        }

    }

    public static function getSubscribedEvents() {
        return array(
            KernelEvents::CONTROLLER => array('onKernelController'),
            KernelEvents::RESPONSE => array('onKernelResponse'),
        );
    }

}
