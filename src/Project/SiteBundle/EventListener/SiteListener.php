<?php

namespace Project\SiteBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use eZ\Publish\API\Repository\Repository as eZPublishRepository;
use eZ\Publish\Core\MVC\Symfony\MVCEvents as eZPublishEvents;
use eZ\Publish\Core\MVC\Symfony\Event\PostSiteAccessMatchEvent;
use eZ\Publish\Core\MVC\Symfony\Event\PreContentViewEvent;
use ThinkCreative\SiteLinkBundle\Events\SiteLinkEvents;
use ThinkCreative\SiteLinkBundle\Events\InitializeSiteLinkEvent;

class SiteListener implements EventSubscriberInterface
{

    protected $eZPublishRepository;
    protected $RequestViewHandler;

    public function __construct(eZPublishRepository $ezpublish_repository, $request_view_handler) {
        $this->eZPublishRepository = $ezpublish_repository;
        $this->RequestViewHandler = $request_view_handler;
    }

    public function onPreContentView(PreContentViewEvent $event) {
        $ContentView = $event->getContentView();
        $ContentViewParameters = $ContentView->getParameters();

        if(
            isset( $ContentViewParameters["content"] ) && isset( $ContentViewParameters["location"] )
        ) {
            $Content = $ContentViewParameters["content"];
            $Location = $ContentViewParameters["location"];

            $ContentTypeService = $this->eZPublishRepository->getContentTypeService();
            $ContentTypeIdentifier = $ContentTypeService->loadContentType( $Content->contentInfo->contentTypeId )->identifier;

            if( $ContentTypeIdentifier === 'bulletin_article' ) {
                $LocationService = $this->eZPublishRepository->getLocationService();
                $ContentService = $this->eZPublishRepository->getContentService();

                $ContentView->addParameters(
                    array(
                        "bulletin_issue" =>  array(
                            "location" => $BulletingIssueLocation = $LocationService->loadLocation( $Location->parentLocationId ),
                            "content" => $ContentService->loadContentByContentInfo(
                                $BulletingIssueLocation->getContentInfo()
                            ),
                        ),
                    )
                );
            }

        }

    }

    public function onSiteAccessMatch(PostSiteAccessMatchEvent $event) {
        $SiteAccessName = $event->getSiteAccess()->name;

        $this->RequestViewHandler->set(
            'is_primary_siteaccess', ( $isPrimarySiteAccess = $SiteAccessName === 'site' )
        );

        if(!$isPrimarySiteAccess) {
            $this->RequestViewHandler->set(
                'project_name', strtoupper($SiteAccessName)
            );
            $this->RequestViewHandler->set(
                'layout', 'ProjectSiteBundle::baselayout.html.twig'
            );
            return;
        }
        $this->RequestViewHandler->set(
            'layout', 'ProjectSiteBundle::pagelayout.html.twig'
        );
    }

    public function onSiteLinkInitialize(InitializeSiteLinkEvent $event) {
        $event->getSiteLink()->getConfiguration()
            ->addContentType('jil_pdf')
                ->isSelfLinking(false)
                ->addFieldType(
                    'file'
                )
            ->end()
        ;
    }

    public static function getSubscribedEvents() {
        return array(
            eZPublishEvents::PRE_CONTENT_VIEW => array('onPreContentView'),
            eZPublishEvents::SITEACCESS => array('onSiteAccessMatch'),
            SiteLinkEvents::INITIALIZE_SITELINK => array('onSiteLinkInitialize'),
        );
    }

}
