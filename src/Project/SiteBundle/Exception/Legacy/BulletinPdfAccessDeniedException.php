<?php

namespace Project\SiteBundle\Exception\Legacy;

use ezpAccessDenied;

/**
 * Exception to handle denied access to bulletin
 * PDF download. A legacy exception is used.
 */
class BulletinPdfAccessDeniedException extends ezpAccessDenied
{
}