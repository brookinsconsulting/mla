<?php

namespace Project\SiteBundle\Exception;

class MemberFormException extends \Exception
{

    protected $type = "warning";

    public function __construct($type = "warning", $message)
    {
        $this->message = trim($message);
        $this->type = $type;
    }

    public function getType() {
        return $this->type;
    }

}