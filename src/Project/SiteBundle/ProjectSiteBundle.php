<?php

namespace Project\SiteBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ProjectSiteBundle extends Bundle
{

    protected $name = "ProjectSiteBundle";

    public $extendsBundleViews = "ThinkCreativeTwitterBootstrapBundle";

}