<?php

namespace Project\SiteBundle\CalendarFilters;
use Symfony\Component\HttpFoundation\Request;
use ThinkCreative\CalendarBundle\CalendarFilters\CalendarFilterBase;
use ThinkCreative\CalendarBundle\Classes\EventProcessor;
use eZ\Publish\API\Repository\Values\Content,
    eZ\Publish\API\Repository\Values\Content\Query,
    eZ\Publish\API\Repository\Values\Content\Query\Criterion,
    eZ\Publish\API\Repository\Values\Content\Search\SearchResult,
    eZ\Publish\API\Repository\Values\Content\Query\SortClause;

class Featured extends CalendarFilterBase
{

    /**
     * {@inheritdoc}
     */
    public function availableFilterValues($language = "eng-US")
    {
        return array(
            array(
                "name" => "Show Only Featured",
                "value" => "only_featured",
                "color" => array(
                    "fg" => "#000",
                    "bg" => "#fff"
                )
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function selectedFilterValues(Request $request = null)
    {
        if (!$request) {
            $request = Request::createFromGlobals();
        }
        return $request->request->get("filter_" . $this->filterName) ?: $request->query->get("filter_" . $this->filterName); 
    }


    /**
     * {@inheritdoc}
     */
    public function requiredFieldTypes()
    {
        return array("featured");
    }

    /**
     * {@inheritdoc}
     */
    public function getCriterion()
    {
        return new Criterion\Field("featured", Criterion\Operator::EQ, true);
    }


}