<?php

namespace Project\SiteBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class ProjectSiteExtension extends Extension
{

	public function load(array $configurations, ContainerBuilder $container) {
		$YamlFileLoader = new Loader\YamlFileLoader(
			$container, new FileLocator(__DIR__ . '/../Resources/config')
		);
		$YamlFileLoader->load('parameters.yml');
		$YamlFileLoader->load('services.yml');
	}

	public function getAlias() {
		return 'project_site';
	}

}