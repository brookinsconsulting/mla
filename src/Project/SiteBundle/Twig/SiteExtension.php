<?php

namespace Project\SiteBundle\Twig;

use eZ\Publish\API\Repository\Repository as eZPublishRepository;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion\Operator;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;
use Aw\Ezp\FetchBundle\Fetch\Fetcher;
use ThinkCreative\MenubarBundle\Classes\Menubar;

class SiteExtension extends \Twig_Extension
{

    protected $eZPublishRepository;
    protected $SearchHandler;

    public function __construct(eZPublishRepository $ezpublish_repository, Fetcher $aw_ezp_fetch) {
        $this->eZPublishRepository = $ezpublish_repository;
        $this->SearchHandler = $aw_ezp_fetch;
    }

    public function getFunctions() {
        return array(
            'get_menubar_split_point' => new \Twig_Function_Method(
                $this, 'getMenubarSplitPoint'
            ),
            'get_bulletin_articles' => new \Twig_Function_Method(
                $this, 'getBulletinArticles'
            ),
            'get_current_bulletin_issue' => new \Twig_Function_Method(
                $this, 'getCurrentBulletinIssue'
            ),
            'get_bulletin_issue' => new \Twig_Function_Method(
                $this, 'getBulletinIssue'
            ),
            'get_bulletin_issue_by_volume_number' => new \Twig_Function_Method(
                $this, 'getBulletinIssueByVolumeNumber'
            ),
            'get_faq_list' => new \Twig_Function_Method(
                $this, 'getFAQList'
            )
        );
    }

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter(
                'location_path', array($this, 'getLocationPath',)
            ),
            new \Twig_SimpleFilter(
                'int', array($this, 'getInterval',)
            ),
            new \Twig_SimpleFilter(
                'contains', array($this, 'stringContains',)
            ),
            new \Twig_SimpleFilter(
                'sortfield', array($this, 'sortField',)
            ),
            new \Twig_SimpleFilter(
                'sortorder', array($this, 'sortOrder',)
            ),
            new \Twig_SimpleFilter(
                'nodefetch', array($this, 'nodeFetch',)
            ),
            new \Twig_SimpleFilter(
                'objectfetch', array($this, 'objectFetch',)
            ),
            new \Twig_SimpleFilter(
                'unescape', array($this, 'filterUnescape',)
            ),
            new \Twig_SimpleFilter(
                'bulletin_matched_tags', array($this, 'filterBulletinMatchedTags',)
            ),
            new \Twig_SimpleFilter(
                'bulletin_header',
                array(
                    $this, 'filterBulletinHeader',
                ),
                array(
                    'is_safe' => array('html'),
                )
            ),
            new \Twig_SimpleFilter(
                'wrap_string',
                array(
                    $this, 'filterWrapString',
                ),
                array(
                    'is_safe' => array('html'),
                )
            ),
            new \Twig_SimpleFilter(
                'bulletin_related_articles',
                array(
                    $this, 'filterBulletinRelatedArticles',
                )
            ),
            new \Twig_SimpleFilter(
                'simple_text_format',
                array(
                    $this, 'filterSimpleTextFormat'
                ),
                array(
                    'is_safe' => array('html'),
                )
            ),
        );
    }

    public function filterBulletinHeader($string) {
        if(
            ($Position = stripos($string, 'Bulletin')) !== false
        ) {
            return '<em>' . substr( $string, 0, $Position + 8 ) . '</em>' . substr( $string, $Position + 8 );
        }
        return $string;
    }

    public function filterBulletinMatchedTags( $bulletin, $request ) {
        if(
            $request->query->has( 'category' )
        ) {
            $BulletingTags = array();

            if (isset($bulletin['attr_tags_lk'])) {
                $BulletingTags = explode(
                    ',', preg_replace( '/,\s*/', ',', $bulletin["attr_tags_lk"] )
                );
            }

            return array_intersect(
                $BulletingTags, $request->query->get( 'category' )
            );
        }
        return false;
    }

    public function filterUnescape($string) {
        return html_entity_decode($string);
    }

    public function filterWrapString($string, $text, $pattern) {
        return preg_replace(
            "/($text)/", $pattern, $string
        );
    }

    public function filterBulletinRelatedArticles( array $menubar_items, array $content_id_list ) {
        $ContentService = $this->eZPublishRepository->getContentService();
        foreach( $menubar_items as $Index => $Value ) {
            $VolumeNumber = (string) $ContentService->loadContent( $content_id_list[ $Index ] )->getFieldValue( 'issue_volume' );
            $menubar_items[ $Index ]["content"] .= " (Vol. $VolumeNumber)";
        }
        return $menubar_items;
    }

    public function getCurrentBulletinIssue( $root_location_id ) {
        $LocationService = $this->eZPublishRepository->getLocationService();
        $ContentService = $this->eZPublishRepository->getContentService();

        $Criteria = array(
            new Criterion\ParentLocationId( $root_location_id ),
            new Criterion\Visibility( Criterion\Visibility::VISIBLE ),
            new Criterion\ContentTypeIdentifier( 'bulletin_issue' ),
        );

        $Query = new LocationQuery(
            array(
                'criterion' => new Criterion\LogicalAnd( $Criteria ),
                'sortClauses' => array(
                    new SortClause\Field( 'bulletin_issue', 'issue_year', Query::SORT_DESC, 'eng-US' ),
                    new SortClause\Field( 'bulletin_issue', 'volume_number', Query::SORT_DESC, 'eng-US' ),
                )
            )
        );
        $Query->limit = 1;

        $Results = $this->eZPublishRepository->getSearchService()->findLocations( $Query );

        if( $Results->searchHits ) {
            return $Results->searchHits[0]->valueObject;
        }
        return false;
    }

    public function getBulletinIssue($location_id) {
        $LocationService = $this->eZPublishRepository->getLocationService();
        $ContentService = $this->eZPublishRepository->getContentService();

        return array(
            "location" => $Location = $LocationService->loadLocation($location_id),
            "content" => $ContentService->loadContentByContentInfo(
                $Location->getContentInfo()
            ),
        );
    }

    public function getBulletinIssueByVolumeNumber($volume_number) {
        $SearchResults = $this->SearchHandler->fetchLocations(
            array(
                'filter' => array(
                    'AND' => array(
                        array(
                            'visibility' => array(
                                'EQ' => true,
                            ),
                        ),
                        array(
                            'content_type_identifier' => array(
                                'EQ' => 'bulletin_issue',
                            ),
                        ),
                        array(
                            'field.volume_number' => array(
                                'EQ' => $volume_number,
                            ),
                        ),
                    ),
                )
            )
        );

        if( $SearchResults->searchHits ) {
            return $SearchResults->searchHits[0]->valueObject;
        }
        return false;
    }

    public function getBulletinArticles($location) {
        $SearchResults = $this->SearchHandler->fetchContentByLocation(
            $SearchParameters = array(
                'filter' => array(
                    'AND' => array(
                        array(
                            'parent_location_id' => array(
                                'EQ' => $location->id,
                            ),
                        ),
                        array(
                            'visibility' => array(
                                'EQ' => true,
                            ),
                        ),
                        array(
                            'content_type_identifier' => array(
                                'EQ' => 'bulletin_article',
                            ),
                        ),
                    ),
                ),
                'sort' => array(
                    'field.bulletin_article/page_number/eng-US' => 'ASC'
                )
            )
        );

        if($SearchResults->totalCount > 0) {
            $BulletinArticles = array();
            foreach($SearchResults->searchHits as $SearchHit) {
                $PageNumber = (int) (string) $SearchHit->valueObject->getFieldValue( 'page_number' );

                // Back Matter
                $ArticleId = (string) $SearchHit->valueObject->getFieldValue("article_id");
                $ArticleId = explode(".", $ArticleId);
                if ( 
                    isset($ArticleId[2]) && (int) $ArticleId[2] == 999 
                ) {
                    $PageNumber = 999;
                }

                if(
                    $ArticleCategory = (string) $SearchHit->valueObject->getFieldValue( 'article_subhead1' )
                ) {
                    $BulletinArticles[ $PageNumber - 1 ] = array(
                        "article_category" => $ArticleCategory,
                        "level" => 1,
                    );
                }

                if(
                    $ArticleCategory = (string) $SearchHit->valueObject->getFieldValue( 'article_subhead2' )
                ) {
                    $BulletinArticles[ $PageNumber - 1 ] = array(
                        "article_category" => $ArticleCategory,
                        "level" => 2,
                    );
                }


                $BulletinArticles[ $PageNumber ] = $SearchHit->valueObject;
            }
            ksort( $BulletinArticles );
            return $BulletinArticles;
        }

        return false;
    }


    public function getFAQList($location) {
        $SearchResults = $this->SearchHandler->fetch(
            array(
                'filter' => array(
                    'AND' => array(
                        array(
                            'parent_location_id' => array(
                                'EQ' => $location->id,
                            ),
                        ),
                        array(
                            'visibility' => array(
                                'EQ' => true,
                            ),
                        ),
                        array(
                            'content_type_identifier' => array(
                                'EQ' => 'faq',
                            ),
                        ),
                    ),
                ),
            )
        );

        if($SearchResults->totalCount > 0) {
            $FAQList = array();
            foreach($SearchResults->searchHits as $SearchHit) {
                $FAQList[] = $SearchHit->valueObject;
            }
            return $FAQList;
        }

        return false;
    }

    public function getLocationPath($location, $root_location_id) {
        $LocationPath = array();
        foreach(
            array_reverse($location->path) as $LocationID
        ) {
            $LocationPath[] = $LocationID;
            if($LocationID == $root_location_id) {
                break;
            }
        }
        return array_reverse($LocationPath);
    }

    public function getInterval($string) {
        return intval($string);
    }

    public function stringContains($string, $check) {
        //$result = strpos($string, $check);
        return strpos($string, $check);
    }

    public function sortField($sort) {
        $sortType = array(1 => 'location_path_string', 2 => 'date_published',  3 => 'date_modified', 4 => 'section_identifier', 5 => 'location_depth', 6 => 'class_identifier', 7 => 'class_name', 8 => 'location_priority', 9 => 'content_name');
        return $sortType[$sort];
    }

    public function sortOrder($order){
        $sortOrder = array(0 => 'DESC', 1 => 'ASC');
        return $sortOrder[$order];
    }

    public function nodeFetch($nodeID) {
        $LocationService = $this->eZPublishRepository->getLocationService();
        $NodeResult = $LocationService->loadLocation($nodeID);
        return $NodeResult;
    }

    public function objectFetch($objectID) {
        $ContentService = $this->eZPublishRepository->getContentService();
        $ObjectResult = $ContentService->loadContent($objectID);
        return $ObjectResult;
    }

    public function getMenubarSplitPoint(Menubar $menubar) {
        $MenubarDataProperties = $menubar->getDataProperties();

        $TotalCount = $MenubarDataProperties->get('total_count');
        $IdealSplitPoint = ceil($TotalCount / 2);

        $IterativeCount = 0;
        foreach(
            $MenubarDataProperties->get('subitem_count_list') as $Index => $Count
        ) {
            $IterativeCount += (int) $Count;
            if($IterativeCount >= $IdealSplitPoint) {
                return $Index + 1;
            }
        }
    }

    /**
     * Format a text string
     * @param string $text
     * @return string
     */
    static public function filterSimpleTextFormat($text)
    {

        // Triple astrick to bold+italic
        $text = preg_replace(
            "/(?![^<]*>)\*\*\*([^<>]*?)\*\*\*/s", 
            "<strong><em>$1</em></strong>", 
            $text
        );

        // Double astrick to bold
        $text = preg_replace(
            "/(?![^<]*>)\*\*([^<>]*?)\*\*/s", 
            "<strong>$1</strong>", 
            $text
        );

        // Single asterick to italic
        $text = preg_replace(
            "/(?![^<]*>)\*([^<>]*?)\*/s", 
            "<em>$1</em>", 
            $text
        );

        // [tab] to comma
        $text = str_replace("[tab]", ",", $text);

        return $text;
    }

    public function getName() {
        return 'site';
    }

}