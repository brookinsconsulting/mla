<?php

namespace Project\ThemesBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ProjectThemesBundle extends Bundle
{

    protected $name = "ProjectThemesBundle";

}
