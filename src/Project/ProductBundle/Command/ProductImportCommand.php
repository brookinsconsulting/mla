<?php

namespace Project\ProductBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use eZ\Publish\Core\FieldType\Selection\Value as eZSelectionValue;
use eZ\Publish\API\Repository\Values\Content\Content;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use Netgen\TagsBundle\API\Repository\TagsService;
use Netgen\TagsBundle\Core\FieldType\Tags\Value as TagValue;
use Project\ApiBundle\Endpoints\ProductVariant;
use Project\ProductBundle\Services\Product;
use Project\ProductBundle\FieldType\Matrix\Value as MatrixValue;
use eZOEInputParser;

/**
 * Command line bookstore product import
 */
class ProductImportCommand extends ContainerAwareCommand
{

    /**
     * Location id to create new products
     * in eZPublish
     * @var integer
     */
    const PRODUCT_LOCATION_ID = 14710;

    /**
     * Location id to create product image
     * @var integer
     */
    const PRODUCT_IMAGE_LOCATION_ID = 14709;

    /**
     * Array matching product category ids to
     * period ez tags
     * @var array
     */
    public static $periodMatch = array(
        3   => 115, // Ancient Literature
        16  => 116, // Medieval before 1400
        9   => 119, // Eighteenth Century
        10  => 117, // Fifteenth and Sixteenth Centuries
        26  => 118, // Seventeenth Century
        18  => 120, // Nineteenth Century
        31  => 121, // Twentieth Century
    );

    /**
     * Array matching product category ids to
     * subject ez tags
     * @var array
     */
    public static $subjectMatch = array(
        2   => 130, // American Literature
        4   => 134, // Bibliographical and Textual Studies
        5   => 135, // British Literature
        6   => 138, // Composition Studie
        7   => 139, // Computer-Assisted Instruction and Research
        8   => 141, // Drama
        11  => 143, // French Literature
        12  => 144, // German Literature
        13  => 151, // Language Textbooks
        14  => 153, // Linguistics
        19  => 156, // Nonfictional Prose
        21  => 158, // Poetry
        22  => 160, // Professional Issues
        23  => 161, // Prose Fiction
        24  => 162, // Publishing and Editing
        25  => 133, // Bibliographies and Research Guides
        27  => 164, // Shakespeare
        28  => 165, // Spanish, Portuguese, Latin American
        29  => 167, // The Teaching of Language
        30  => 168, // The Teaching of Literature
        33  => 171, // Writing Guides
        //35  => // Merchandise
        36  => 140, // Disability Studies
        50  => 132, // Asian American Studies
        51  => 136, // Canadian Literature
        52  => 142, // Feminist and Gender Studies
        53  => 145, // Greek Literature
        54  => 146, // Hebrew Literature
        55  => 148, // Interdisciplinary Studies
        56  => 149, // Italian Literature
        57  => 150, // Japanese Literature
        58  => 152, // Latin Literature
        59  => 154, // Literary Theory
        60  => 155, // Native American Studies
        61  => 157, // Norwegian Literature
        62  => 163, // Russian Literature
        63  => 170, // Urdu Literature
        64  => 172, // Yiddish Literature
        65  => 147, // Holocaust Studies
        69  => 169, // Turkish Literature
        74  => 137, // Caribbean Literature
        76  => 129, // African Studies
        77  => 128, // African American Studies
        78  => 131, // Arabic Literature
    );

    /**
     * Array matching product category ids to
     * ez publish content location ids.
     * @var array
     */
    public static $categoryMatch = array(
        0   => 17658, // Non-Series
        38  => 17659, // A New Variorum Edition of Shakespeare
        39  => 17660, // Approaches to Teaching World Literature
        40  => 17661, // Copublished with Houghton Mifflin
        41  => 17662, // Index Society Fund Publications
        42  => 17663, // Introductions to Older Languages
        43  => 17664, // MLA Texts and Translations
        44  => 17665, // Options for Teaching
        45  => 17666, // Research and Scholarship in Composition
        46  => 17667, // Reviews of Research
        47  => 17668, // Selected Bibliographies in Language and Literature
        48  => 17669, // Teaching Languages, Literatures, and Cultures
        67  => 17670, // World Literatures Reimagined
        70  => 17671, // PMLA
        //71  => 15024, // MLA Bibliography
        72  => 17672, // Profession
    );

    /**
     * {@inheritdoc}
     */ 
    protected function configure()
    {
        $this
            ->setName('project_product:import')
            ->setDescription('Import all products from CSV')
            ->addOption(
                'offset',
                null,
                InputOption::VALUE_OPTIONAL,
                'Starts product import from offset'
            )            
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln("<info>Product import start...</info>");

        // define csv files
        $pathes = array(
            "product_csv" => "docstudio_import/store_products.data",
            "variant_csv" => "docstudio_import/store_product_opts2.data"
        );

        // root dir
        $rootDir = $this->getContainer()->get('kernel')->getRootDir();

        // get ezpublish api repository
        $eZPublishApiRepository = $this->getContainer()->get("ezpublish.api.repository");

        // set current user to admin
        $userService = $eZPublishApiRepository->getUserService();
        $adminUser = $userService->loadUserByLogin("admin");
        $eZPublishApiRepository->setCurrentUser($adminUser);

        // csv
        foreach ($pathes as $key=>$path) {
            $csv = file_get_contents(getcwd() . "/" . $path);
            $csvArr[$key] = array();
            foreach (explode("\n", $csv) as $line) {
                $csvArr[$key][] = str_getcsv($line, "|");
            }
        }

        // parse description xml
        // See http://www.php.net/manual/en/class.domelement.php#101243
        $get_inner_html = function( $node ) {
            if (!$node) { return ""; }
            $innerHTML= '';
            $children = $node->childNodes;
            foreach ($children as $child) {
                $innerHTML .= $child->ownerDocument->saveXML( $child );
            }
         
            return $innerHTML;
        };

        // TOC XML
        $tocXML = null;
        if (file_exists($rootDir . "/../docstudio_import/TOCs.xml")) {
            $tocXml = new \DOMDocument();
            $tocXml->load($rootDir . "/../docstudio_import/TOCs.xml");

            // replace "i" with "emphasize"
            while($tocXml->getElementsByTagName("i")->length > 0) {
                $element = $tocXml->getElementsByTagName("i")->item(0);
                $em = $tocXml->createElement("emphasize");
                foreach ($element->childNodes as $child) {
                    $em->appendChild($child);
                }
                $element->parentNode->replaceChild($em, $element);
            }

            // replace "b" with "strong"
            while($tocXml->getElementsByTagName("b")->length > 0) {
                $element = $tocXml->getElementsByTagName("b")->item(0);
                $strong = $tocXml->createElement("strong");
                foreach ($element->childNodes as $child) {
                    $strong->appendChild($child);
                }
                $element->parentNode->replaceChild($strong, $element);
            }

            $tocXpath = new \DomXpath($tocXml);
        }
        
        // process data
        $product_data = array();
        foreach ($csvArr['product_csv'] as $product) {
            if (count($product) < 65) {
                continue;
            }

            // find variants
            $variants = array();

            foreach ($csvArr['variant_csv'] as $variantLine) {
                
                if (trim($variantLine[0]) == trim($product[0])) {

                    $variantRow = explode(";", $variantLine[1]);

                    if (count($variantRow) < 12) {
                        continue;
                    }

                    if (trim($variantRow[2]) && trim($variantRow[5])) {
                        $variants[] = array(
                            "name"      => trim($variantRow[2]),
                            "variant_id"=> trim($variantRow[4]),
                            "isbn"      => trim($variantRow[5])
                        );
                    }

                    if (trim($variantRow[9]) && trim($variantRow[12])) {
                        $variants[] = array(
                            "name"      => trim($variantRow[9]),
                            "variant_id"=> trim($variantRow[11]),
                            "isbn"      => trim($variantRow[12])
                        );
                    }
                }
            }

            // if no variants grab variant from product data dump
            if (!$variants) {
                $variantId = "";
                $isbn = "";
                if (trim($product[26]) && strlen(trim($product[26])) >= 10 && strpos(trim($product[26]), "-") === false ) {
                    $isbn = trim($product[26]);
                } else if (trim($product[27]) && strlen(trim($product[27])) >= 10) {
                    $isbn = trim($product[27]);
                }

                if (trim($product[26]) && strlen(trim($product[26])) < 10) {
                    $variantId = trim($product[26]);
                } else if (trim($product[27]) && strlen(trim($product[27])) < 10) {
                    $variantId = trim($product[27]);
                }

                $variants[] = array(
                    "name"              => trim($product[42]) ?: "Item",
                    "variant_id"        => $variantId,
                    "isbn"              => $isbn

                );
            }

            // REPORTING
            /*foreach ($variants as $variantData) {
                $productVariantObject = trim($variantData['isbn']) ? ProductVariant::getVariantByIsbn($this->getContainer()->get("project.api.api"), trim($variantData['isbn'])) : null;
                if (!$productVariantObject) {
                    $productVariantObject = ProductVariant::getVariantById($this->getContainer()->get("project.api.api"), $variantData['variant_id']);
                }

                if (!$productVariantObject || !$productVariantObject->get("binding")) {
                    // dump info to csv
                    
                    file_put_contents(
                        "/tmp/mla_import",
                        '"' . $shortName . '","'. $variantData['isbn'] . '","' . ( $productVariantObject && $productVariantObject->get("binding") ? "Y - " . $productVariantObject->get("binding") : ( $variantData['name'] && $variantData['name'] != "Item"  ? "Y (From DocStudio) - " .  $variantData['name'] : "N"  ) ) . '","' . ($productVariantObject ? "Y" : "N") . '"' . "\n",
                        FILE_APPEND
                    );

                }

            }*/



            // find image with ISBN as filename
            $image = array();
            foreach ($variants as $variant) {

                // find image with oracle id
                if (file_exists($rootDir . "/../docstudio_import/store_images/{$variant['variant_id']}.jpg")) {
                    $image = array(
                        "file"      => "{$variant['variant_id']}.jpg",
                        "caption"   => preg_replace('/[^(\x20-\x7F)]*/','', trim(html_entity_decode(strip_tags($product[2])))) . " Cover"
                    );
                    break;

                // find image with isbn
                } else if (file_exists($rootDir . "/../docstudio_import/store_images/{$variant['isbn']}.jpg")) {
                    $image = array(
                        "file"      => "{$variant['isbn']}.jpg",
                        "caption"   => preg_replace('/[^(\x20-\x7F)]*/','', trim(html_entity_decode(strip_tags($product[2])))) . " Cover"
                    );
                    break;
                }
            }

            // parse product categories
            $subjects = array();
            $period = array();
            $parent_location_id = self::$categoryMatch[0];
            foreach (explode(",", $product[1]) as $categoryId) {

                // subjects
                if (array_key_exists(trim($categoryId), self::$subjectMatch)) {
                    $subjects[] = self::$subjectMatch[trim($categoryId)];
                }

                // periods
                if (array_key_exists(trim($categoryId), self::$periodMatch)) {
                    $period[] = self::$periodMatch[trim($categoryId)];
                }

                // parent location id
                if (array_key_exists(trim($categoryId), self::$categoryMatch)) {
                    $parent_location_id = self::$categoryMatch[trim($categoryId)];
                }
            }

            $description = "";
            $reviews = "";
            $toc = "";
            $contributors = "";
            $ebooks = array();
            $bio = "";

            if (file_exists($rootDir . "/../docstudio_import/product_descriptions/pd_". intval($product[0]) . ".xml")) {
                $descXml = new \DOMDocument();
                $descXml->load($rootDir . "/../docstudio_import/product_descriptions/pd_{$product[0]}.xml");

                // get description node
                $descriptionList = $descXml->getElementsByTagName("description");

                // message
                $messageId = 0;

                if ($descriptionList->length > 0) {
                    $messageXml = $descriptionList->item(0)->getElementsByTagName("message");
                    if ($messageXml->length > 0) {
                        $messageId = $messageXml->item(0)->getAttribute("id");

                        // remove message from description
                        $descriptionList->item(0)->removeChild($messageXml->item(0));
                    }
                }
                
                // get main description text
                if ($descriptionList->length > 0) {
                    $description = "<p>" . nl2br(trim($get_inner_html($descriptionList->item(0)))) . "</p>"; 
                    $description = str_replace("<br />", "</p><p>", $description);
                }

                // get reviews
                $reviewList = $descXml->getElementsByTagName("reviews");
                if ($reviewList->length > 0) {
                    $reviewsNode = $reviewList->item(0);
                    $blurbList = $reviewsNode->getElementsByTagName("blurb");
                    $sourceList = $reviewsNode->getElementsByTagName("source");
                    for ($i = 0; $i < $blurbList->length; $i++) {
                        $reviews .= '<custom name="quote" align="right" custom:author="'.$get_inner_html($sourceList->item($i)).'">'.$get_inner_html($blurbList->item($i)).'</custom>';
                    }

                    $legacyKernel = $this->getContainer()->get('ezpublish_legacy.kernel');
                    $reviews =  $legacyKernel()->runCallback(
                        function () use ( $reviews ) {
                            $inputParse = new eZOEInputParser();
                            $xml = $inputParse->process( $reviews );
                            return $xml->saveXML();
                        }
                    );

                }

                // get contributors
                $contribList = $descXml->getElementsByTagName("contributors");
                if ($contribList->length > 0) {
                    $contributors = nl2br($get_inner_html($contribList->item(0)));
                }

                // get toc
                $toc = "";
                if ($tocXml) {
                    $tocQuery = $tocXpath->query('//tocs/toc[@id="'. $product[0] .'"]');
                    if ($tocQuery->length > 0) {
                        $tocItem = $tocQuery->item(0);

                        $legacyKernel = $this->getContainer()->get('ezpublish_legacy.kernel');
                        $tocxml =  $legacyKernel()->runCallback(
                            function () {
                                $inputParse = new eZOEInputParser();
                                return $inputParse->process("");
                            }
                        );

                        $toc = str_replace("/>", ">", $tocxml->saveXML());
                        foreach ($tocItem->getElementsByTagName("p") as $element) {
                            $toc .= "<paragraph class=\"". $element->getAttribute("class") ."\">".$get_inner_html($element)."</paragraph>";
                        }
                        $toc .= "</section>";
                    }
                }
                
                // get bio
                $bioList = $descXml->getElementsByTagName("bio");
                if ($bioList->length > 0) {
                    $bio = nl2br($get_inner_html($bioList->item(0)));
                }

                // get ebooks
                $ebookList = $descXml->getElementsByTagName("ebooks");
                if ($ebookList->length > 0) {
                    $ebookNode = $ebookList->item(0);
                    $ebooksList = $ebookNode->getElementsByTagName("a");
                    for ($i = 0; $i< $ebooksList->length; $i++) {
                        $ebooks[] = array(
                            "name" => strip_tags($get_inner_html($ebooksList->item($i))),
                            "url" => $ebooksList->item($i)->getAttribute("href"),
                        );
                    }
                }

            }

            $product_data[] = array(
                "id"                    => (integer) $product[0],
                "categories"            => explode(",", $product[1]),
                "tags"                  => $subjects,
                "period_tags"           => $period,
                "name"                  => trim(preg_replace("/<\/?[iI]>/", "*", $product[2])),
                "short_name"            => trim(preg_replace('/[^(\x20-\x7F)]*/','', html_entity_decode(strip_tags($product[2])))),
                "status"                => (integer) $product[8],
                "keywords"              => explode(",", $product[11]),
                "date_added"            => trim($product[12]) ? strtotime($product[12]) : "",
                "image"                 => $product[19],
                "homepage"              => strtoupper(trim($product[22])) == "Y" ? true : false,
                "related_products"      => explode(",", $product[25]),
                "description"           => $description,
                "bio"                   => $bio,
                "contributors"          => $contributors,
                "authors"               => explode(",", $product[35]),
                "editors"               => explode(",", $product[36]),
                "translators"           => explode(",", $product[37]),
                "compilers"             => explode(",", $product[38]),
                "edition"               => $product[39],
                "page_count"            => $product[40],
                "publication_year"      => $product[41],
                "exc_article_sort"      => $product[63],
                "subtitle"              => $product[64],
                "sort_key"              => $product[65],
                "oracle_id"             => $product[26],
                "variants"              => $variants,
                "message"               => isset($messageId) ? intval($messageId) : 0,
                "_table_of_contents"    => $toc,
                "_reviews"              => $reviews,
                "_image"                => $image,
                "_parent_location_id"   => $parent_location_id,
                "_ebooks"               => $ebooks

            );
        }

        // get product service
        $productService = $this->getContainer()->get("project.product.product");

        // product relation list
        $productRelations = array();

        foreach ($product_data as $key => $product) {

            if (intval($input->getOption("offset")) > $key + 1) {
                continue;
            }

            $output->write("    <info>".trim(strip_tags($product['name']))." ({$product['id']})...</info>");

            try {
                $content = $this->updateProduct($product, $product['_parent_location_id']);
                if ($content) {
                    $productRelations[$product['id']] = $content;
                }
                $output->writeln("<info>Done</info>");
            } catch (\Exception $e) {
                $output->writeln("<error>Error</error>");
                $output->writeln("<info>Use option '--offset ". ($key + 1) . "' to continue.</info>");
                throw $e;
            }

        }

        $output->writeln("<info>Setting product relations...</info>");

        // set related
        // get required ezpublish services
        $contentService = $eZPublishApiRepository->getContentService();
        $languageService =  $eZPublishApiRepository->getContentLanguageService();

        foreach ($product_data as $key => $product) {

            if (!array_key_exists($product['id'], $productRelations)) {
                continue;
            }
            // get content object
            $content = $productRelations[$product['id']];

            // get list of related ids
            $relatedIds = array();
            foreach ($product['related_products'] as $originalProductId) {
                if (array_key_exists($originalProductId, $productRelations)) {
                    $relatedIds[] = $productRelations[$originalProductId]->contentInfo->id;
                }
            }

            // update content
            $contentUpdateStruct = $contentService->newContentUpdateStruct();
            $contentUpdateStruct->initialLanguageCode = $languageService->getDefaultLanguageCode() ?: "eng-US";
            $contentUpdateStruct->setField("related_products", $relatedIds);

            // create draft
            $contentDraft = $contentService->createContentDraft( $content->contentInfo );
            $contentDraft = $contentService->updateContent( $contentDraft->versionInfo, $contentUpdateStruct );

            // publish updated verfsion
            $content = $contentService->publishVersion( $contentDraft->versionInfo );

        }

        $output->writeln("<info>DONE</info>");

        return;
    }

    /**
     * Update or create product based on data from an array.
     * This array is most likely part of the ProductImport
     * Command.
     * @param array $product_data
     * @param integer $parent_location_id
     * @return eZ\Publish\API\Repository\Values\Content\Content
     */
    public function updateProduct(array $product_data, $parent_location_id = null)
    {

        // set parent location
        if (!$parent_location_id) {
            $parent_location_id = self::PRODUCT_LOCATION_ID;
        }

        // get product service
        $productService = $this->getContainer()->get("project.product.product");

        // get ezpublish api repository
        $eZPublishApiRepository = $this->getContainer()->get("ezpublish.api.repository");

        // get required ezpublish services
        $contentService = $eZPublishApiRepository->getContentService();
        $locationService = $eZPublishApiRepository->getLocationService();
        $contentTypeService = $eZPublishApiRepository->getContentTypeService();
        $languageService = $eZPublishApiRepository->getContentLanguageService();
        $searchService = $eZPublishApiRepository->getSearchService();        
        $tagService = $this->getContainer()->get("ezpublish.api.service.tags");

        // attempt to locate existing
        $query = new Query();
        $query->criterion = new Criterion\LogicalAnd(
            array(
                new Criterion\Visibility( Criterion\Visibility::VISIBLE ),
                new Criterion\ContentTypeId( Product::PRODUCT_CLASS_ID ),
                new Criterion\Field( 'short_name', Criterion\Operator::EQ, $product_data['short_name'] )
            )
        );
        $results = $searchService->findContent($query);
        $content = null;
        if ($results->totalCount > 0) {
            foreach ($results->searchHits as $searchItem) {
                $variants = $searchItem->valueObject->getFieldValue("variants")->values;
                $content = $searchItem->valueObject;
                break;
                foreach ($variants as $variant) {
                    foreach ($product_data['variants'] as $importVariant) {
                        if ($importVariant['isbn'] == $variant['isbn']) {
                            $content = $searchItem->valueObject;
                            break;
                        }
                    }
                    if ($content) { break; }
                }
            }
        }

        if ($content) { return $content; }

        // flag to ensure that a change has been made
        $hasUpdate = false;

        // load content type
        $contentType = $contentTypeService->loadContentType(Product::PRODUCT_CLASS_ID);

        // has content create new version
        if ($content) {

            // content update struct
            $contentStruct = $contentService->newContentUpdateStruct();
            $contentStruct->initialLanguageCode = $languageService->getDefaultLanguageCode() ?: "eng-US";

        // create new content
        } else {

            // create new content
            $contentStruct = $contentService->newContentCreateStruct( $contentType, $languageService->getDefaultLanguageCode() ?: "eng-US" );

        }

        // iterate fields
        foreach ($contentType->getFieldDefinitions() as $field) {
            if (!array_key_exists($field->identifier, $product_data)) {
                continue;
            }
            switch ($field->fieldTypeIdentifier) {

                // ezkeyword
                case "ezkeyword":
                    $class = get_class($field->defaultValue);
                    $value = new $class(
                        is_array($product_data[$field->identifier]) ? $product_data[$field->identifier] : explode(",", $product_data[$field->identifier])
                    );
                    $contentStruct->setField($field->identifier, $value);
                    break;

                // ezxmltext
                case "ezxmltext":

                    $legacyKernel = $this->getContainer()->get('ezpublish_legacy.kernel');
                    $xml =  $legacyKernel()->runCallback(
                        function () use ( $product_data, $field ) {
                            $inputParse = new eZOEInputParser();
                            return $inputParse->process( trim($product_data[$field->identifier]) );
                        }
                    );

                    // no change
                    if ($content && $content->getFieldValue($field->identifier)->xml->saveXML() == $xml->saveXML()) {
                        break;
                    }
                    $hasUpdate = true;

                    $contentStruct->setField($field->identifier, $xml->saveXML());
                    break;

                // ezimage
                case "ezimage":
                    break;

                // ezselection
                case "ezselection":
                    $class = get_class($field->defaultValue);
                    $value = new $class(
                        is_array($product_data[$field->identifier]) ? $product_data[$field->identifier] : array($product_data[$field->identifier])
                    );
                    $contentStruct->setField($field->identifier, $value);
                    break;

                case "ezoption":
                case "ezobjectrelationlist":
                    break;

                case "ezmatrix":
                    break;

                // eztags
                case "eztags":
                case "eztag":

                    $tags = new TagValue();
                    foreach ($product_data[$field->identifier] as $tagId) {
                        $tags->tags[] = $tagService->loadTag($tagId);
                    }

                    if ($content && count( array_diff( $tags->tags, $content->getFieldValue($field->identifier)->tags ) ) == 0) {
                        break;
                    }
                    $hasUpdate = true;

                    $contentStruct->setField($field->identifier, $tags);

                    break;

                case "ezdate":
                    $class = get_class($field->defaultValue);
                    $datetime = new \DateTime();
                    $datetime->setTimestamp($product_data[$field->identifier]);
                    $value = new $class(
                        $datetime
                    );

                    // no change
                    if ($content && $datetime->getTimestamp() == $content->getFieldValue($field->identifier)->date->getTimestamp()) {
                        break;
                    }
                    $hasUpdate = true;

                    $contentStruct->setField($field->identifier, $value);
                    break;

                // set raw value
                default:

                    // no change
                    if ($content && (string) $content->getFieldValue($field->identifier) == $product_data[$field->identifier]) {
                        break;
                    }
                    $hasUpdate = true;

                    $contentStruct->setField($field->identifier, $product_data[$field->identifier]);
                    break;

            }
        }


        // image create new object
        if ($product_data['_image'] && !$content) {

            // load content type
            $imageContentType = $contentTypeService->loadContentType(27);

            // create new content
            $imageCreateStruct = $contentService->newContentCreateStruct( $imageContentType, $languageService->getDefaultLanguageCode() ?: "eng-US" );

            $imageCreateStruct->setField("name", $product_data['_image']['caption']);

            $imageValue = new \eZ\Publish\Core\FieldType\Image\Value(
                array(
                    "id" => $this->getContainer()->getParameter("kernel.root_dir") . "/../docstudio_import/store_images/{$product_data['_image']['file']}",
                    "fileSize" => filesize($this->getContainer()->getParameter("kernel.root_dir") . "/../docstudio_import/store_images/{$product_data['_image']['file']}"),
                    "fileName" => $product_data['_image']['file'],
                    "alternativeText" => preg_replace('/[^(\x20-\x7F)]*/','', trim(html_entity_decode(strip_tags($product_data['_image']['caption']))))
                )
            );

            $imageCreateStruct->setField( 'image', $imageValue );

            // create location for object
            $imageLocationCreateStruct = $locationService->newLocationCreateStruct( self::PRODUCT_IMAGE_LOCATION_ID );

            // create draft
            $imageDraft = $contentService->createContent( $imageCreateStruct, array( $imageLocationCreateStruct ) );      

            // publish
            $imageContent = $contentService->publishVersion( $imageDraft->versionInfo );

            // set image
            $contentStruct->setField("cover_image", $imageContent->contentInfo->id);
        }

        // variants
        if (!$content || count($content->getFieldValue("variants")->values) == 0) {

            // create matrix data for product variants
            $matrixValues = array();

            foreach ($product_data['variants'] as $variantData) {
                $productVariantObject = trim($variantData['isbn']) ? ProductVariant::getVariantByIsbn($this->getContainer()->get("project.api.api"), trim($variantData['isbn'])) : null;
                if (!$productVariantObject) {
                    $productVariantObject = ProductVariant::getVariantById($this->getContainer()->get("project.api.api"), $variantData['variant_id']);
                    if (!$productVariantObject) {
                        continue;
                    }
                }

                // dump info to csv
                file_put_contents(
                    "/tmp/mla_import",
                    '"' . str_replace('"', "'", $product_data['short_name']) . '","'. $variantData['isbn'] . '","' . $variantData['variant_id'] . '","' . $productVariantObject->get("id") . '","' . ((bool) $product_data['_image'] ? "Y" : "N") . '"' . "\n",
                    FILE_APPEND
                );

                $matrixValues[] = array(
                    "id"                => $productVariantObject->get("id"),
                    "name"              => $productVariantObject->get("binding") ?: "Item",
                    "isbn"              => $productVariantObject->get("isbn"),
                    "unit_price"        => $productVariantObject->get("unit_price"),
                    "out_of_print"      => $productVariantObject->get("out_of_print"),
                    "availability"      => $productVariantObject->get("availability"),
                    "copyright_year"    => $productVariantObject->get("copyright_year"),
                    "publish_date"      => $productVariantObject->get("publish_date"),
                    "shopify_variant_id"=> "",
                    "taxable"           => ($productVariantObject->get("availability") == "false" || $productVariantObject->get("availability") == "0" || !$productVariantObject->get("availability")) ? "" : "true",
                    "weight"            => $productVariantObject->get("weight")
                );

            }

            // make matrix
            $matrixBuild = MatrixValue::buildFromArray(Product::$variantMatrixColumns, $matrixValues);
            $contentStruct->setField("variants", $matrixBuild);
            $hasUpdate = true;
        }


        // ebooks
        if ($product_data['_ebooks']) {
            $matrixBuild = MatrixValue::buildFromArray(
                array(
                    "name" => "Name",
                    "url" => "URL"
                ),
                $product_data['_ebooks']
            );
            $contentStruct->setField("ebooks", $matrixBuild);
        }

        // review
        if ($product_data['_reviews']) {
            $contentStruct->setField("short_description", $product_data['_reviews']);
            if ($content && $product_data['_reviews'] != $content->getFieldValue("short_description")->xml->saveXML()) {
                $hasUpdate = true;
            }
        }

        // table of contents
        if ($product_data['_table_of_contents']) {
            $contentStruct->setField("table_of_contents", $product_data['_table_of_contents']);   
            if ($content && $product_data['_table_of_contents'] != $content->getFieldValue("table_of_contents")->xml->saveXML()) {
                $hasUpdate = true;
            }
        }

        // update
        if ($content) {

            // no updates
            if (!$hasUpdate) {
                return $content;
            }

            $contentDraft = $contentService->createContentDraft( $content->contentInfo );
            $contentDraft = $contentService->updateContent( $contentDraft->versionInfo, $contentStruct );
            $content = $contentService->publishVersion( $contentDraft->versionInfo );

            // shopify sync
            $productService->syncShopifyProduct($content, false);

        // create
        } else {

            // create location for object
            $locationCreateStruct = $locationService->newLocationCreateStruct( $parent_location_id );

            // create draft
            $draft = $contentService->createContent( $contentStruct, array( $locationCreateStruct ) );

            // publish
            $content = $contentService->publishVersion( $draft->versionInfo );

            // shopify sync
            $productService->syncShopifyProduct($content, true);

        }


        sleep(1);

        return $content;

    }

}