<?php

namespace Project\ProductBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use eZ\Publish\Core\Base\Exceptions\NotFoundException;
use eZ\Publish\Core\Base\Exceptions\UnauthorizedException;
use eZ\Publish\API\Repository\Values\Content,
    eZ\Publish\API\Repository\Values\Content\Query,
    eZ\Publish\API\Repository\Values\Content\Query\Criterion,
    eZ\Publish\API\Repository\Values\Content\Search\SearchResult,
    eZ\Publish\API\Repository\Values\Content\Query\SortClause;
use Project\ProductBundle\Services\Product;
use Project\ProductBundle\Command\ProductImportCommand;

/**
 * Sync all products with shopify
 */
class ProductShopifySyncCommand extends ContainerAwareCommand
{

    /**
     * {@inheritdoc}
     */ 
    protected function configure()
    {
        $this
            ->setName('project_product:shopify_sync')
            ->setDescription('Sync all products with Shopify')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("<info>Start Shopify product sync...</info>");

        // set current user to admin
        $userService = $this->getContainer()->get("ezpublish.api.service.user");
        $adminUser = $userService->loadUserByLogin("admin");
        $this->getContainer()->get("ezpublish.api.repository")->setCurrentUser($adminUser);

        // get product service
        $productService = $this->getContainer()->get("project.product.product");

        // get location service
        $locationService = $this->getContainer()->get("ezpublish.api.service.location");

        // build search query
        $query = new Query();
        $query->criterion = new Criterion\LogicalAnd(array(
            new Criterion\ContentTypeId(Product::PRODUCT_CLASS_ID)
        ));
        $results = $this->getContainer()->get("ezpublish.api.service.search")->findContent($query);

        // iterate results and update
        foreach ($results->searchHits as $item) {
            $output->write("    <info>". $item->valueObject->contentInfo->name . "...</info>");

            // doesn't have a location, skip
            if (!$item->valueObject->contentInfo->mainLocationId) {
                $output->writeln("<comment>Skipped (No locations)</comment>");
                continue;
            }

            // must contain a variant
            if (count($item->valueObject->getFieldValue("variants")->values) == 0) {
                $output->writeln("<error>Skipped (No variants)</error>");
                continue;
            }

            // determine product type from location
            try {
                $location = $locationService->loadLocation($item->valueObject->contentInfo->mainLocationId);
            } catch (NotFoundException $e) {
                $output->writeln("<error>Skipped (Location not found)</error>");
                continue;
            } catch (UnauthorizedException $e) {
                $output->writeln("<error>Skipped (Unauthoried location)</error>");
                continue;
            }

            $productType = "Default";
            $requiresShipping = false;
            foreach ($location->path as $subLocationId) {
                if ($subLocationId == ProductImportCommand::PRODUCT_LOCATION_ID) {
                    $productType = "Bookstore";
                    $requiresShipping = true;
                }
            }

            // sync
            $productService->syncShopifyProduct($item->valueObject, $productType, $requiresShipping);
            sleep(1);
            
            $output->writeln("<info>Done</info>");
        }

        $output->writeln("<info>Completed.</info>");
    }

}