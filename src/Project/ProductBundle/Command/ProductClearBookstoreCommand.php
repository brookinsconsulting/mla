<?php

namespace Project\ProductBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Project\ProductBundle\Services\Product;
use Project\ProductBundle\Command\ProductImportCommand;
use Project\ProductBundle\Api\ShopifyApi;

/**
 * Clear all bookstore products
 */
class ProductClearBookstoreCommand extends ContainerAwareCommand
{

    /**
     * {@inheritdoc}
     */ 
    protected function configure()
    {
        $this
            ->setName('project_product:clear_bookstore')
            ->setDescription('Clear all bookstore products')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        // warn user of immediate product wipe and give them time to cancel script
        for ($i = 5; $i > 0; $i--) {
            $output->writeln("<info>Begin product wipe in {$i} seconds.</info>");
            sleep(1);
        }
        $output->writeln("<info>Start product wipe...</info>");
        

        // set current user to admin
        $eZPublishApiRepository = $this->getContainer()->get("ezpublish.api.repository");
        $userService = $eZPublishApiRepository->getUserService();
        $adminUser = $userService->loadUserByLogin("admin");
        $eZPublishApiRepository->setCurrentUser($adminUser);

        // get location service
        $locationService = $this->getContainer()->get("ezpublish.api.service.location");

        // get content service
        $contentService = $this->getContainer()->get("ezpublish.api.service.content");

        // get solr conf
        $solrConf = $this->getContainer()->getParameter("think_creative_search.solr");

        // create shopify api object
        $shopifyConf = $this->getContainer()->getParameter("ProjectProduct_ShopifyApiConfiguration");
        $shopifyApi = new ShopifyApi(
            isset($shopifyConf['api_key']) ? $shopifyConf['api_key'] : "",
            isset($shopifyConf['api_password']) ? $shopifyConf['api_password'] : "",
            isset($shopifyConf['api_host']) ? $shopifyConf['api_host'] : ""
        );

        // iterate locations
        $count = 0;
        foreach (ProductImportCommand::$categoryMatch as $locationId) {
            $topLevelLocation = $locationService->loadLocation($locationId);
            $locationList = $locationService->loadLocationChildren($topLevelLocation);
            if ($locationList->totalCount > 0) {
                foreach ($locationList->locations as $location) {
                    $output->write("      <info>".$location->contentInfo->name."...</info>");

                    // get content for location
                    $content = $contentService->loadContent($location->contentInfo->id);

                    // remove from shopify
                    $shopifyId = $content->getFieldValue("shopify_id")->text;
                    if ($shopifyId) {
                        $shopifyApi->request(
                            "products/{$shopifyId}",
                            "DELETE"
                        );
                        sleep(1);
                    }

                    // remove cover_image_relation
                    $coverImageRelationId = $content->getFieldValue("cover_image")->destinationContentId;
                    if ($coverImageRelationId) {
                        $coverImageContent = $contentService->loadContent($coverImageRelationId);
                        if ($coverImageContent) {
                            $contentService->deleteContent($coverImageContent->contentInfo);
                        }
                    }

                    // delete from solr
                    $curl = curl_init((isset($solrConf['protocol']) ? $solrConf['protocol'] : 'http') . '://' . (isset($solrConf['host']) ? $solrConf['host'] : 'localhost' ) . ':' . (isset($solrConf['port']) ? $solrConf['port'] : 80) . '/solr/' . $solrConf['core'] . '/update');
                    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                        "Content-Type: text/xml"
                    ));
                    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");  
                    curl_setopt($curl, CURLOPT_POSTFIELDS, "<delete><query>main_node_meta_node_id_si:".$location->id."</query></delete>");
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);                    

                    // remove self
                    $locationService->deleteLocation($location);

                    $output->writeln("<info>Done</info>");
                    $count++;
                }
            }
        }
        $output->writeln("<info>Completed. (".$count." products removed)</info>");
    }

}