<?php

namespace Project\ProductBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use eZ\Publish\Core\FieldType\Selection\Value as eZSelectionValue;
use eZ\Publish\API\Repository\Values\Content\Content;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use Netgen\TagsBundle\API\Repository\TagsService;
use Netgen\TagsBundle\Core\FieldType\Tags\Value as TagValue;
use Project\ApiBundle\Endpoints\ProductVariant;
use Project\ProductBundle\Services\Product;
use Project\ProductBundle\FieldType\Matrix\Value as MatrixValue;
use eZOEInputParser;

/**
 * Command line blurb fix
 */
class BlurbFixCommand extends ContainerAwareCommand
{

    /**
     * Location id where productis live
     * @var integer
     */
    const PRODUCT_LOCATION_ID = 14710;


    /**
     * {@inheritdoc}
     */ 
    protected function configure()
    {
        $this
            ->setName('project_product:blurbfix')
            ->setDescription('Fixes blurbs')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln("<info>Blurb Fix start...</info>");

        // define csv files
        $pathes = array(
            "product_csv" => "docstudio_import/store_products.data",
            "variant_csv" => "docstudio_import/store_product_opts2.data"
        );

        // root dir
        $rootDir = $this->getContainer()->get('kernel')->getRootDir();

        // get ezpublish api repository
        $eZPublishApiRepository = $this->getContainer()->get("ezpublish.api.repository");

        // get required ezpublish services
        $contentService = $eZPublishApiRepository->getContentService();
        $contentTypeService = $eZPublishApiRepository->getContentTypeService();
        $languageService = $eZPublishApiRepository->getContentLanguageService();
        $searchService = $eZPublishApiRepository->getSearchService();        

        // set current user to admin
        $userService = $eZPublishApiRepository->getUserService();
        $adminUser = $userService->loadUserByLogin("admin");
        $eZPublishApiRepository->setCurrentUser($adminUser);

        // csv
        foreach ($pathes as $key=>$path) {
            $csv = file_get_contents(getcwd() . "/" . $path);
            $csvArr[$key] = array();
            foreach (explode("\n", $csv) as $line) {
                $csvArr[$key][] = str_getcsv($line, "|");
            }
        }

        // parse description xml
        // See http://www.php.net/manual/en/class.domelement.php#101243
        $get_inner_html = function( $node ) {
            if (!$node) { return ""; }
            $innerHTML= '';
            $children = $node->childNodes;
            foreach ($children as $child) {
                $innerHTML .= $child->ownerDocument->saveXML( $child );
            }
         
            return $innerHTML;
        };

        // process data
        $product_data = array();
        foreach ($csvArr['product_csv'] as $product) {
            if (count($product) < 65) {
                continue;
            }

            if (file_exists($rootDir . "/../docstudio_import/product_descriptions/pd_". intval($product[0]) . ".xml")) {

                $descXml = new \DOMDocument();
                $descXml->load($rootDir . "/../docstudio_import/product_descriptions/pd_{$product[0]}.xml");

                // get reviews
                $reviews = "";
                $reviewList = $descXml->getElementsByTagName("reviews");
                if ($reviewList->length > 0) {
                    $reviewsNode = $reviewList->item(0);
                    $blurbList = $reviewsNode->getElementsByTagName("blurb");
                    $sourceList = $reviewsNode->getElementsByTagName("source");
                    for ($i = 0; $i < $blurbList->length; $i++) {

                        $author = $get_inner_html($sourceList->item($i));
                        $author = trim(preg_replace("/<\/?[iI]>/", "*", $author));
                        $author = str_replace("—", "", $author);
                        $author = strip_tags($author);


                        $reviews .= '<custom name="quote" align="right" custom:author="'.$author.'">'.$get_inner_html($blurbList->item($i)).'</custom>';
                    }

                    $legacyKernel = $this->getContainer()->get('ezpublish_legacy.kernel');
                    $reviews =  $legacyKernel()->runCallback(
                        function () use ( $reviews ) {
                            $inputParse = new eZOEInputParser();
                            $xml = $inputParse->process( $reviews );
                            return $xml->saveXML();
                        }
                    );

                }

                // find content and update
                $query = new Query();
                $query->criterion = new Criterion\LogicalAnd(
                    array(
                        new Criterion\Visibility( Criterion\Visibility::VISIBLE ),
                        new Criterion\ContentTypeId( Product::PRODUCT_CLASS_ID ),
                        new Criterion\Field(
                            'name', 
                            Criterion\Operator::EQ,
                            trim(preg_replace("/<\/?[iI]>/", "*", $product[2]))
                        ),
                        new Criterion\Field(
                            'edition', 
                            Criterion\Operator::EQ,
                            trim($product[39])
                        ),
                    )
                );
                $results = $searchService->findContent($query);


                //var_dump($reviews);

                $output->write("<info>" . html_entity_decode(trim(preg_replace("/<\/?[iI]>/", "*", $product[2]))) . "...</info>");

                if (count($results->searchHits) == 1) {

                    $contentStruct = $contentService->newContentUpdateStruct();
                    $contentStruct->initialLanguageCode = $languageService->getDefaultLanguageCode() ?: "eng-US";
                    $contentStruct->setField("short_description", $reviews);
                    $contentDraft = $contentService->createContentDraft( $results->searchHits[0]->valueObject->contentInfo );
                    $contentDraft = $contentService->updateContent( $contentDraft->versionInfo, $contentStruct );
                    $contentService->publishVersion( $contentDraft->versionInfo );
                    $output->writeln("<info>OK</info>");

                } if (count($results->searchHits) == 0) {
                    $output->writeln("<info>Not found...</info>");
                    $output->writeln("\n<error>Product not found, manual entry data follows...</error>\n\n" . $reviews . "\n\n");
                }

            }


        }

        $output->writeln("<info>DONE</info>");

        return;
    }

}