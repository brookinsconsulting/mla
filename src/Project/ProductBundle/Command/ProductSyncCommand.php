<?php

namespace Project\ProductBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use Project\ProductBundle\Services\Product;
use Project\ApiBundle\Endpoints\ProductVariant;

/**
 * Command line product sync
 */
class ProductSyncCommand extends ContainerAwareCommand
{

    /**
     * {@inheritdoc}
     */ 
    protected function configure()
    {
        $this
            ->setName('project_product:sync')
            ->setDescription('Sync all products in the API to the eZPublish CMS')
            ->addOption(
                'create-new-shopify',
                null,
                InputOption::VALUE_NONE,
                'If set, will create new Shopify products for each product regardless of whether or not a Shopify product already exists.'
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        // root dir
        $rootDir = $this->getContainer()->get('kernel')->getRootDir();

        // get ezpublish api repository
        $eZPublishApiRepository = $this->getContainer()->get("ezpublish.api.repository");

        // get required ezpublish services
        $contentService = $eZPublishApiRepository->getContentService();
        $locationService = $eZPublishApiRepository->getLocationService();
        $contentTypeService = $eZPublishApiRepository->getContentTypeService();
        $languageService = $eZPublishApiRepository->getContentLanguageService();
        $searchService = $eZPublishApiRepository->getSearchService();        

        // get product service
        $productService = $this->getContainer()->get("project.product.product");

        // get api service
        $apiService = $this->getContainer()->get("project.api.api");

        // set current user to admin
        $userService = $eZPublishApiRepository->getUserService();
        $adminUser = $userService->loadUserByLogin("admin");
        $eZPublishApiRepository->setCurrentUser($adminUser);

        // get verbosity
        $isVerbose = OutputInterface::VERBOSITY_QUIET <= $output->getVerbosity();

        // get all product from API
        if ($isVerbose) {
            $output->write("<info>Fetching products...</info>");
        }
        $apiVariantList = ProductVariant::getAllVariants($apiService);

        // perform a search for existing content object
        $query = new Query();
        $query->criterion = new Criterion\LogicalAnd(
            array(
                new Criterion\Visibility( Criterion\Visibility::VISIBLE ),
                //new Criterion\ParentLocationId( Product::PRODUCT_LOCATION_ID ),
                new Criterion\ContentTypeId( Product::PRODUCT_CLASS_ID )
            )
        );
        $results = $searchService->findContent($query);

        // determine if product list is available
        if ($results->totalCount > 0) {
            if ($isVerbose) {
                $output->write("<info>done.</info>", true);
            }
        } else {
            if ($isVerbose) {
                $output->write("<error>ERROR.</error>", true);
            }
            $output->writeln("<error>No products were found.</error>");
            return;
        }

        if ($isVerbose) {
            $output->writeln("<info>Syncing ".$results->totalCount." products...</info>");
        }

        foreach ($results->searchHits as $searchItem) {
            $valueObject = $searchItem->valueObject;

            if ($isVerbose) {
                $output->write("    <info>".strip_tags($valueObject->contentInfo->name)."...</info>");
            }

            // check if image missing
            if (!$searchItem->valueObject->getFieldValue("cover_image")->destinationContentId) {
                $image = array();
                foreach ($searchItem->valueObject->getFieldValue("variants")->values as $variant) {

                    if (file_exists($rootDir . "/../docstudio_import/store_images/{$variant['id']}.jpg")) {
                        $image = array(
                            "file"      => "{$variant['id']}.jpg",
                            "caption"   => $searchItem->valueObject->contentInfo->name . " Cover"
                        );
                        break;

                    // find image with isbn
                    } else if (file_exists($rootDir . "/../docstudio_import/store_images/{$variant['isbn']}.jpg")) {
                        $image = array(
                            "file"      => "{$variant['isbn']}.jpg",
                            "caption"   => $searchItem->valueObject->contentInfo->name . " Cover"
                        );
                        break;
                    }
                }

                // image found
                if ($image) {

                    $output->write("<info>IMAGE FOUND...</info>");

                    // load content type
                    $imageContentType = $contentTypeService->loadContentType(27);   // 27 == image content type

                    // create new content
                    $imageCreateStruct = $contentService->newContentCreateStruct( $imageContentType, $languageService->getDefaultLanguageCode() ?: "eng-US" );
                    $imageCreateStruct->setField("name", trim(html_entity_decode(strip_tags($image['caption']))));

                    $imageValue = new \eZ\Publish\Core\FieldType\Image\Value(
                        array(
                            "id" => $rootDir . "/../docstudio_import/store_images/{$image['file']}",
                            "fileSize" => filesize( $rootDir . "/../docstudio_import/store_images/{$image['file']}" ),
                            "fileName" => $image['file'],
                            "alternativeText" => preg_replace('/[^(\x20-\x7F)]*/','', trim(html_entity_decode(strip_tags($image['caption']))))
                        )
                    );

                    $imageCreateStruct->setField( 'image', $imageValue );

                    // create location for object
                    $imageLocationCreateStruct = $locationService->newLocationCreateStruct( \Project\ProductBundle\Command\ProductImportCommand::PRODUCT_IMAGE_LOCATION_ID );

                    // create draft
                    $imageDraft = $contentService->createContent( $imageCreateStruct, array( $imageLocationCreateStruct ) );      

                    // publish
                    $imageContent = $contentService->publishVersion( $imageDraft->versionInfo );

                    // update product
                    $contentStruct = $contentService->newContentUpdateStruct();
                    $contentStruct->initialLanguageCode = $languageService->getDefaultLanguageCode() ?: "eng-US";
                    $contentStruct->setField("cover_image", $imageContent->contentInfo->id);
                    $contentDraft = $contentService->createContentDraft( $searchItem->valueObject->contentInfo );
                    $contentDraft = $contentService->updateContent( $contentDraft->versionInfo, $contentStruct );
                    $searchItem->valueObject = $contentService->publishVersion( $contentDraft->versionInfo );
                }
            }

            // content update
            try {

                $valueObject = $productService->syncProductVariants($valueObject, $apiVariantList) ?: $valueObject;
                if ($isVerbose) {
                    $output->write("<info>1</info>");
                }

            } catch (\Exception $e) {
                if ($isVerbose) {
                    $output->writeln("<error>ERROR.</error>");
                    $output->writeln("<error>" . $e->getMessage() . "</error>");
                    return;
                }    
            }

            // shopify update
            sleep(1);
            try {

                $productService->syncShopifyProduct($valueObject, $input->getOption('create-new-shopify'));
                if ($isVerbose) {
                    $output->writeln("<info>...2...ok!</info>");
                }

            } catch (\Exception $e) {
                if ($isVerbose) {
                    $output->writeln("<error>...ERROR.</error>");
                    $output->writeln("<error>" . $e->getMessage() . "</error>");
                    //return;
                }         
            }

        }

        // finished
        if ($isVerbose) {
            $output->writeln("<info>Finished!</info>");
        }


    }
}