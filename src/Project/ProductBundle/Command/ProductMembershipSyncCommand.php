<?php

namespace Project\ProductBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use Project\ProductBundle\Services\Product;

/**
 * Command line 'membership' product sync
 */
class ProductMembershipSyncCommand extends ContainerAwareCommand
{

    /**
     * {@inheritdoc}
     */ 
    protected function configure()
    {
        $this
            ->setName('project_product:membership_sync')
            ->setDescription('Sync membership product')
            ->addOption(
                'create-new-shopify',
                null,
                InputOption::VALUE_NONE,
                'If set, will create new Shopify products for each product regardless of whether or not a Shopify product already exists.'
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        // get verbosity
        $isVerbose = OutputInterface::VERBOSITY_QUIET <= $output->getVerbosity();

        if ($isVerbose) {
            $output->write("<info>Updating 'Membership' product...</info>");
        }

        // get ezpublish api repository
        $eZPublishApiRepository = $this->getContainer()->get("ezpublish.api.repository");

        // set current user to admin
        $userService = $eZPublishApiRepository->getUserService();
        $adminUser = $userService->loadUserByLogin("admin");
        $eZPublishApiRepository->setCurrentUser($adminUser);

        // get product service
        $productService = $this->getContainer()->get("project.product.product");

        try {
            if ($productService->syncMembershipProduct($input->getOption("create-new-shopify"))) {
                if ($isVerbose) {
                    $output->writeln("<info>Done.</info>");
                }
            } else {
                if ($isVerbose) {
                    $output->writeln("<info>No change.</info>");
                }
            }
        } catch (\Exception $e) {
            if ($isVerbose) {
                $output->writeln("<error>ERROR.</error>");
            }
            $output->writeln("<error>" . $e->getMessage() . "</error>");
        }

    }
}