<?php

namespace Project\ProductBundle\Tests\Order;

use Symfony\Component\HttpFoundation\ParameterBag;
use ThinkCreative\PaymentBundle\Entity\Order;
use Project\MemberBundle\Classes\Member;
use Project\ApiBundle\Services\Api;
use Project\ProductBundle\Api\ShopifyApi;
use Project\ProductBundle\Order\OrderProcessorService;

/**
 * OrderProcessorService Tests
 */
class OrderProcessorServiceTest extends \PHPUnit_Framework_TestCase
{

    /**
     * API Vars
     */
    const TEST_MEMBER_ID = 186497;
    const TEST_API_KEY = "thinkcreative";
    const TEST_API_SECRET = "a7ba2096719aece556de08598209d37258e00a61d7619842c0ba9318de498792";
    const TEST_API_URL = "https://apidev.mla.org/1";

    /**
     * Shopify Vars
     */
    const TEST_SHOPIFY_KEY = "897403e416d8addfbe6d3ef0ea504b0d";
    const TEST_SHOPIFY_PASSWORD = "1a9773de218eb75f3176076c4bec8aa6";
    const TEST_SHOPIFY_HOST = "think-creative-test-store-7.myshopify.com";
    //const TEST_SHOPIFY_TRANSACTION_ID = 410745424;

    /**
     * Test OrderProcessorService::orderCapture
     */
    public function testOrderCapture()
    {

        // server vars
        $_SERVER['HTTP_USER_AGENT'] = "Test";
        $_SERVER['REMOTE_ADDR'] = "127.0.0.1";

        // api
        $api = new Api(
            self::TEST_API_KEY,
            self::TEST_API_SECRET,
            self::TEST_API_URL
        );

        // shopify api
        /*$shopifyApi = new ShopifyApi(
            self::TEST_SHOPIFY_KEY,
            self::TEST_SHOPIFY_PASSWORD,
            self::TEST_SHOPIFY_HOST
        );*/

        // get order from shopify
        //$shopifyOrder = $shopifyApi->request("orders/" . self::TEST_SHOPIFY_TRANSACTION_ID, "GET");

        // create mock content
        $content = $this->getMock('eZ\\Publish\\API\\Repository\\Values\\Content\\Content');

        // create mock product service
        $productService = $this->getMockBuilder('\\Project\\ProductBundle\\Services\\Product')
            ->disableOriginalConstructor()
            ->getMock();
        $productService->expects($this->any())
            ->method("getProduct")
            ->will($this->returnValue($content));
        $productService->expects($this->any())
            ->method("getProductVariantById")
            ->will($this->returnValue(
                new ParameterBag(array(
                    "unit_price" => rand(1,100),
                ))
            ));
        $productService->expects($this->any())
            ->method("getProductImage")
            ->will($this->returnValue(null));

        // create mock member
        $member = $this->getMockBuilder('\\Project\MemberBundle\\Classes\\Member')
            ->disableOriginalConstructor()
            ->getMock();
        $member->expects($this->any())
            ->method("getMemberApiId")
            ->will($this->returnValue(self::TEST_MEMBER_ID));

        // create mock member service
        $memberService = $this->getMockBuilder('\\Project\\MemberBundle\\Services\\Member')
            ->disableOriginalConstructor()
            ->getMock();
        $memberService->expects($this->any())
            ->method("getCurrentMember")
            ->will($this->returnValue($member));
        $memberService->expects($this->any())
            ->method("getMemberStatus")
            ->will($this->returnValue(Member::MEMBER_STATUS_JOIN));

        // create mock discount service
        $discountService = $this->getMockBuilder('\\Project\\ProductBundle\\Services\\DiscountService')
            ->disableOriginalConstructor()
            ->getMock();
        $discountService->expects($this->any())
            ->method("getApplicableDiscounts")
            ->will($this->returnValue(array()));

        // init OrderProcessorService
        $orderProcessorService = new OrderProcessorService(
            $productService,
            $api,
            $memberService,
            $discountService,
            array()
        );

        // create order
        $order = new Order(
            $orderProcessorService,
            "join"
        );
        $order->addProduct(
            "TEST_PRODUCT" . OrderProcessorService::PRODUCT_VARIANT_SEPERATOR . "TEST_VARIANT",
            5
        );
        $order->setCustomerDetails(
            "Test",
            "Person",
            "test@person.net",
            array(
                "shopifyOrderId" => rand(1000000000, 9999999999),
                "creditCard" => array(
                    "billingAddress1" => "1234 Test St.",
                    "billingAddress2" => "",
                    "billingCity" => "Test City",
                    "billingCompany" => "Test Company",
                    "billingCountry" => "US",
                    "billingFirstName" => "Test",
                    "billingLastName" => "Person",
                    "phone" => 1234567890,
                    "billingState" => "FL",
                    "billingPostcode" => 32301,
                    "shippingAddress1" => "1234 Test St.",
                    "shippingAddress2" => "",
                    "shippingCity" => "Test City",
                    "shippingCompany" => "Test Company",
                    "shippingCountry" => "US",
                    "shippingFirstName" => "Test",
                    "shippingLastName" => "Person",
                    "shippingState" => "FL",
                    "shippingPostcode" => 32301
                )
            )
        );
        
        $orderProcessorService->orderCaptured(
            $order,
            rand(1000000000, 9999999999)
        );


    }

}