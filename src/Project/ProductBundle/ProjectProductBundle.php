<?php

namespace Project\ProductBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ProjectProductBundle extends Bundle
{

    protected $name = "ProjectProductBundle";

}
