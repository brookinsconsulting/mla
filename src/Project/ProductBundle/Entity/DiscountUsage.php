<?php

namespace Project\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Project\ProductBundle\Classes\Discount;

/**
 * Doctrine Entity to keep track of store discount usage
 * @ORM\Entity
 * @ORM\Table(name="project_product_discountusage")
 */
class DiscountUsage
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Discount content object id
     * @ORM\Column(type="integer")
     */
    protected $discountObjectId;

    /**
     * API member id
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $memberId;

    /**
     * Shopify order id
     * @ORM\Column(type="integer")
     */
    protected $orderId;

    /**
     * Get entity id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set discount object id from a discount object
     * @param Discount $discount
     */
    public function setDiscount(Discount $discount)
    {
        $this->setDiscountObjectId($discount->getContentObjectId());
    }

    /**
     * Set discount object id
     * @param integer $discountObjectId
     */
    public function setDiscountObjectId($discountObjectId)
    {
        $this->discountObjectId = intval($discountObjectId);
    }

    /**
     * Get discount object id
     * @return integer
     */
    public function getDiscountObjectId()
    {
        return $this->discountObjectId;
    }

    /**
     * Set API member id
     * @param integer $memberId
     */
    public function setMemberId($memberId)
    {
        $this->memberId = intval($memberId);
    }

    /**
     * Get API member id
     * @return integer
     */
    public function getMemberId()
    {
        return $this->memberId;
    }

    /**
     * Set Shopify order id
     * @param integer $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = intval($orderId);
    }

    /**
     * Get Shopify order id
     * @param integer 
     */
    public function getOrderId()
    {
        return $this->orderId;   
    }

}
