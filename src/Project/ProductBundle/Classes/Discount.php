<?php

namespace Project\ProductBundle\Classes;
use eZ\Publish\API\Repository\Values\Content\Content;
use eZ\Publish\Core\Repository\LocationService;
use eZ\Publish\Core\Repository\Values\Content\Location;
use Doctrine\ORM\EntityManager;
use ThinkCreative\PaymentBundle\Entity\Order;
use Project\MemberBundle\Classes\Member;
use Project\ProductBundle\Exception\InvalidContentTypeException;
use Project\ProductBundle\Services\Product;

/**
 * Discount class
 */ 
class Discount
{

    /**
     * Content class id for discount objects
     * @var integer
     */
    const DISCOUNT_CLASS_ID = 52;

    /**
     * Functions of a discount code
     */
    const FUNCTION_DISCOUNT_PRICE = 0;
    const FUNCTION_PERCERTAGE = 1;
    const FUNCTION_DAA = 2;

    /**
     * Discount applies to
     */
    const APPLIES_PRODUCT_PRICE = 0;
    const APPLIES_TAX = 1;
    const APPLIES_SHIPPING = 2;
    const APPLIES_TOTAL = 3;
    const APPLIES_TOTAL_SHIPPING = 4;

    /**
     * Binding types
     * @var array
     */
    private static $bindingTypes = array(
        1 => "Paperback",
        2 => "Cloth"
    );

    /**
     * eZPublish Location Service 'ezpublish.api.service.location'
     * @var LocationService
     */
    protected $locationService;

    /**
     * Discount content object
     * @var Content
     */
    protected $discountContent;

    /**
     * Doctrine Entity Manager
     * @var EntityManager
     */
    protected $doctrineEntityManager;

    /**
     * Constructor
     * @param Content $discount_content  Discount content object
     */
    public function __construct(LocationService $location_service, Content $discount_content, EntityManager $doctrine_entity_manager)
    {
        // ensure the content class matches PROMO_CLASS_ID
        if ($discount_content->contentInfo->contentTypeId != self::DISCOUNT_CLASS_ID) {
            throw new InvalidContentTypeException("The given content object is not a discount.");
        }

        $this->locationService = $location_service;
        $this->discountContent = $discount_content;
        $this->doctrineEntityManager = $doctrine_entity_manager;
    }

    /**
     * Get name of the discount
     * @return string
     */
    public function getName()
    {
        $name = trim($this->discountContent->getFieldValue("discount_name")->text);
        if (!$name) {
            $name = "Discount #" . $this->discountContent->getFieldValue("record_identifier")->value;
        }
        return $name;
    }

    /**
     * Get discount's content object id
     * @return integer
     */
    public function getContentObjectId()
    {
        return $this->discountContent->contentInfo->id;
    }

    /**
     * Get discount code
     * @return string
     */
    public function getCode()
    {
        return $this->discountContent->getFieldValue("promotional_code")->text;
    }

    /**
     * Return integer value representing parts of order
     * discount applies to. Uses the constants APPLIES_**
     * to identify value.
     * @return integer
     */
    public function appliesTo()
    {
        return reset($this->discountContent->getFieldValue("applies_to")->selection);
    }

    /**
     * Returns integer value representing function of discount.
     * Uses constants FUNCTION_** to identify value.
     * @return integer
     */
    public function getFunction()
    {
        return reset($this->discountContent->getFieldValue("function")->selection);
    }

    /**
     * Get discount value. Function of value determined
     * by getFunction.
     * @return float
     */
    public function getDiscountValue()
    {
        return $this->discountContent->getFieldValue("discount_amount")->value;
    }

    /**
     * Returns true if discount can be combined with others
     * @return bool
     */
    public function canCombine()
    {
        return $this->discountContent->getFieldValue("combined")->bool;
    }

    /**
     * Returns true if discount is unique (one time use)
     * @return bool
     */
    public function isUnique()
    {
        return $this->discountContent->getFieldValue("unique")->bool;
    }

    /**
     * Return true if this discount is currently active
     * i.e. has not expired, has not been used and is single use
     * @return boolean
     */
    public function isActive()
    {

        // get expiration date
        if (
            $this->discountContent->getFieldValue("expiration_date")->date &&
            time() > $this->discountContent->getFieldValue("expiration_date")->date->getTimestamp() 
        ) {
            return false;
        }

        // ensure code hasn't been used if unique
        if ($this->discountContent->getFieldValue("unique")->bool) {

            // lookup discount usage to determine if this discount has already been used
            $discountRepo = $this->doctrineEntityManager->getRepository('ProjectProductBundle:DiscountUsage');
            $discountUsage = $discountRepo->findOneBy(array(
                "discountObjectId" => $this->getContentObjectId()
            ));
            if ($discountUsage) {
                return false;
            }
        }
        
        return true;

    }

    /**
     * Returns true if this discount applies to given product
     * @param Content $product_content  Product content object
     * @param Location $product_location  Product location object (optional)
     * @return boolean
     */
    public function appliesToProduct(Content $product_content, Location $product_location = null)
    {

        // check product exclusions
        if (in_array($product_content->contentInfo->id, $this->discountContent->getFieldValue("exclusions")->destinationContentIds)) {
            return false;
        }

        // if all product identifying fields are empty assume that discount extends to all products
        if (
            count($this->discountContent->getFieldValue("products_applied_to")->destinationContentIds) <= 0 &&
            count($this->discountContent->getFieldValue("product_tags")->tags) <= 0 &&
            //count($product_content->getFieldValue("period_tags")->tags) <= 0 && ???
            count($this->discountContent->getFieldValue("product_series")->destinationContentIds) <= 0
        ) {
            return true;
        }

        // check if product content object id exist in 'products_applied_to' field
        if (in_array($product_content->contentInfo->id, $this->discountContent->getFieldValue("products_applied_to")->destinationContentIds)) {
            return true;
        }

        // check if product is tagged with 'product_tags'
        foreach ($this->discountContent->getFieldValue("product_tags")->tags as $tag) {
            foreach ($product_content->getFieldValue("tags")->tags as $productTag) {
                if ($tag->id == $productTag->id) {
                    return true;
                }
            }
            foreach ($product_content->getFieldValue("period_tags")->tags as $productTag) {
                if ($tag->id == $productTag->id) {
                    return true;
                }                
            }
        }

        // check if product parent is in 'product_series'
        if (!$product_location) {
            $product_location = $this->locationService->loadLocation(
                $product_content->contentInfo->mainLocationId
            );
        }
        $parentLocation = $this->locationService->loadLocation(
            $product_location->parentLocationId
        );
        if (in_array($parentLocation->contentInfo->id, $this->discountContent->getFieldValue("product_series")->destinationContentIds)) {
            return true;
        }

        return false;

    }

    /**
     * Returns true if given order parameters are valid
     * for this discount (i.e. product quantity, products in cart)
     * @param Order $order
     * @return boolean
     */
    function appliesToOrder(Order $order)
    {

        // count number of products
        $matchingProductCount = 0;
        $totalProductCount = 0;
        foreach ($order->getProductList() as $productId=>$quantity) {
            $productInfo = $order->getProductFromId($productId);
            $productContent = $productInfo->getProductContent();
            $totalProductCount += $quantity;
            if ($this->appliesToProduct($productContent)) {
                $matchingProductCount += $quantity;
            }
        }

        // verifiy at least one product matching discount exists
        if ($matchingProductCount <= 0) {
            return false;
        }

        // verify cart meets min/max quality requirements
        if (
            (
                $this->discountContent->getFieldValue("min_quantity_product")->value > 0 &&
                $matchingProductCount < $this->discountContent->getFieldValue("min_quantity_product")->value
            ) ||
            (   
                $this->discountContent->getFieldValue("max_quantity_product")->value > 0 &&
                $matchingProductCount > $this->discountContent->getFieldValue("max_quantity_product")->value
            )
        ) {
            return false;
        }
        if (
            (
                $this->discountContent->getFieldValue("min_quantity_all")->value > 0 &&
                $totalProductCount < $this->discountContent->getFieldValue("min_quantity_all")->value
            ) ||
            (   
                $this->discountContent->getFieldValue("max_quantity_all")->value > 0 &&
                $totalProductCount > $this->discountContent->getFieldValue("max_quantity_all")->value
            )
        ) {
            return false;
        }


        return true;
    }

    /**
     * Returns true if this discount can be used by given user. If null given
     * check if membership is required to use discount.
     * @param Member $member
     * @return boolean
     */
    public function appliesToUser(Member $member = null)
    {

        // check for member only discount
        if (!$member && reset($this->discountContent->getFieldValue("discount_type")->selection) == 0) {
            return false;
        }

        // ensure user is full member if membership required
        elseif ($member && reset($this->discountContent->getFieldValue("discount_type")->selection) == 0) {
            if ($member->getMemberStatus() != Member::MEMBER_STATUS_FULL) {
                return false;
            }
        }

        return true;
    }


    /**
     * Get the amount that will be deducted from a product variants's price.
     * Returns 0 if discount does not apply to product prices.
     * @param Content $product_content  Product content object
     * @param integer $variant_id  Id of product variant
     * @return float
     */
    public function getProductDiscountPrice(Content $product_content, $variant_id)
    {

        // make sure discount applies to individual product
        if (!in_array(self::APPLIES_PRODUCT_PRICE, $this->discountContent->getFieldValue("applies_to")->selection)) {
            return 0;
        }

        // make sure discount applies to product
        // @TODO does not consider location which could affect product discount status
        if (!$this->appliesToProduct($product_content, null)) {
            return 0;
        }
        
        // make sure discount applies to variant
        $bindingType = reset($this->discountContent->getFieldValue("binding_type")->selection);
        if (array_key_exists($bindingType, self::$bindingTypes)) {
            
            // find variant
            $hasVariant = false;
            foreach ($product_content->getFieldValue("variants")->values as $variant) {
                if ($variant['id'] == $variant_id) {
                    $hasVariant = true;
                    if (strtolower(self::$bindingTypes[$bindingType]) != strtolower(trim( $variant['name'] ))) {
                        return 0;
                    }
                }
            }
            if (!$hasVariant) {
                return 0;
            }

        }

        // make sure discount has a 'function'
        if (count($this->discountContent->getFieldValue("function")->selection) <= 0) {
            return 0;
        }

        // determine price based on function
        switch ($this->getFunction()) {

            // set price to specified amount
            case self::FUNCTION_DISCOUNT_PRICE:
                foreach ($product_content->getFieldValue("variants")->values as $variant) {
                    if ($variant['id'] == $variant_id) {
                        return $variant['unit_price'] - $this->discountContent->getFieldValue("discount_amount")->value;
                    }
                }
            break;

            // takes a percentage amount off the price
            case self::FUNCTION_PERCERTAGE:
                foreach ($product_content->getFieldValue("variants")->values as $variant) {
                    if ($variant['id'] == $variant_id) {
                        return $variant['unit_price'] * ($this->discountContent->getFieldValue("discount_amount")->value / 100);
                    }
                }
                return 0;
            break;

            // takes a specified amount from the price, if amount would cause price
            // to go into negatives then returns price of the product (making it free)
            case self::FUNCTION_DAA:
                foreach ($product_content->getFieldValue("variants")->values as $variant) {
                    if ($variant['id'] == $variant_id) {
                        return $this->discountContent->getFieldValue("discount_amount")->value < $variant['unit_price'] ? (float) $this->discountContent->getFieldValue("discount_amount")->value : (float) $variant['unit_price'];
                    }
                }
                return 0;
            break;
        }

    }

    /**
     * Return total amount discounted from given order.
     * @param Order $order
     * @return float
     */
    public function getTotalDiscountPrice(Order $order)
    {

        // determine what discount applioes to
        switch($this->appliesTo()) {

            // individual product price
            case self::APPLIES_PRODUCT_PRICE:

                // total discount amount
                $discountTotal = 0;

                // iterate each product and get discount price of each individual product
                foreach ($order->getProductList() as $productId=>$quantity) {
                    $productInfo = $order->getProductFromId($productId);
                    $discountTotal += $this->getProductDiscountPrice($productInfo->getProductContent(), $productInfo->getVariantId()) * $quantity;
                }
                return $discountTotal;

            break;

            // order tax
            case self::APPLIES_TAX:
                $total = $order->getExtraFee("tax");
            break;

            // order shipping
            case self::APPLIES_SHIPPING:
                $total = $order->getExtraFee("shipping");
            break;

            // order total w/o shipping
            case self::APPLIES_TOTAL:
            case self::APPLIES_TOTAL_SHIPPING:
                $total = $order->getTotalPrice();
                // no break here on purpose

            // order total w/ shipping
            case self::APPLIES_TOTAL_SHIPPING:
                $total += $order->getExtraFee("shipping");
            break;
        }

        // use total and calculate discount
        if (isset($total)) {

            // get discount value
            $discountValue = $this->discountContent->getFieldValue("discount_amount")->value;

            switch ($this->getFunction()) {

                // set price to value
                case self::FUNCTION_DISCOUNT_PRICE:
                    return $total - $discountValue;
                break;

                // take percentage off price
                case self::FUNCTION_PERCERTAGE:
                    return $total / (100 / $discountValue);
                break;

                // take set amount off price
                case self::FUNCTION_DAA:
                    if ($discountValue < $total) {
                        return $discountValue;
                    }
                    return $total;
                break;
            }
        }

        return 0;

    }

    /**
     * Apply discount to given order. Return false if 
     * discount could not be applied.
     * @param Order $order
     * @param Member $member Optional
     * @return bool
     */ 
    public function setOrderDiscount(Order $order, Member $member = null)
    {
        // discount not active
        if (!$this->isActive()) {
            return false;
        }

        // does not apply to order
        if (!$this->appliesToOrder($order)) {
            return false;
        }

        // does not apply to user
        if (!$this->appliesToUser($member)) {
            return false;
        }

        // apply discount
        $order->setDiscount(
            "discount_" . $this->discountContent->contentInfo->id,
            $this->getTotalDiscountPrice($order),
            $this->getName()
        );
        return true;
    }


}