<?php

namespace Project\ProductBundle\Classes\DependencyInjection;
use eZ\Publish\API\Repository\LocationService;
use Netgen\TagsBundle\API\Repository\TagsService;
use ThinkCreative\BridgeBundle\Services\CustomTagsManager;
use ThinkCreative\BridgeBundle\DependencyInjection\CustomTagsHandlerInterface;
use Project\ProductBundle\Services\Product;

class ProductListCustomTag implements CustomTagsHandlerInterface
{

    /**
     * Location id to look for products in
     * @var integer
     */
    const PRODUCT_LOCATION_ID = 14710;

    /**
     * Netgen Tags Service
     * @var Netgen\TagsBundle\API\Repository\TagsService
     */
    protected $tagsService;

    /**
     * eZ Publish Location Service
     * @var LocationService
     */
    protected $locationService;

    /**
     * Constructor.
     * @param TagsService $tags_service
     */
    public function __construct(TagsService $tags_service, LocationService $location_service) {
        $this->tagsService = $tags_service;
        $this->locationService = $location_service;
    }

    /**
     * Register product related custom tags
     * @param CustomTagsManager $customtags_manager
     */ 
    public function registerCustomTagList(CustomTagsManager $customtags_manager) {       

        // get eztag categories
        $tagList = array();
        foreach(array($this->tagsService->loadTag(113), $this->tagsService->loadTag(112)) as $parent) {
            foreach ($this->tagsService->loadTagChildren($parent) as $tag) {
                $tagList["TAG_" . $tag->id] = $parent->keyword . " | " . $tag->keyword;
            }
        }

        // get sub categories
        $rootLocation = $this->locationService->loadLocation(self::PRODUCT_LOCATION_ID);
        $catList = array();
        foreach ($this->locationService->loadLocationChildren($rootLocation)->locations as $locationChild) {
            $catList["LOC_" . $locationChild->id] = "Category | " . $locationChild->contentInfo->name;
        }

        // category product list config
        $categoryProductListConf = array(
            "attributes" => array(
                "tag" => array(
                    "name" => "Category",
                    "type" => "select",
                    "selection" => array_merge(array("RECENT_0" => "Base | New Titles"), array_merge($catList, $tagList))
                ),
                "limit" => array(
                    "name" => "Limit",
                    "type" => "text",
                    "default" => 8
                ),
                "size" => array(
                    "name" => "Size",
                    "type" => "select",
                    "selection" => array(
                        "bookstore_listitem" => "Small",
                        "featured" => "Large"
                    )
                )
            ),
            "template" => "ProjectProductBundle:customtags:product_list.html.twig",
            "controller" => "ProjectProductBundle:CustomTag:categoryList"
        );

        // register product category customtag
        $customtags_manager->register(
            "product-category-list",
            $categoryProductListConf
        );

    }

}