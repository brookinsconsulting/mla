<?php

namespace Project\ProductBundle\Classes\DependencyInjection;
use eZ\Publish\API\Repository\LocationService;
use ThinkCreative\BridgeBundle\Services\CustomTagsManager;
use ThinkCreative\BridgeBundle\DependencyInjection\CustomTagsHandlerInterface;
use Project\ProductBundle\Services\Product;

class FeaturedProductCustomTag implements CustomTagsHandlerInterface
{
    /**
     * Register product related custom tags
     * @param CustomTagsManager $customtags_manager
     */ 
    public function registerCustomTagList(CustomTagsManager $customtags_manager) {

        $productList = array();
        for ($i = 1; $i <= 8; $i++) {
            $productList["product-{$i}"] = array(
                "name" => "Product #{$i}",
                "type" => "link"
            );
        }

        $customtags_manager->register(
            "product-featured",
            array(
                "attributes" => array_merge(
                    $productList,
                    array(                
                        "size" => array(
                            "name" => "Size",
                            "type" => "select",
                            "selection" => array(
                                "bookstore_listitem" => "Small",
                                "featured" => "Large"
                            )
                        )
                    )
                ),
                "template" => "ProjectProductBundle:customtags:product_list.html.twig",
                "controller" => "ProjectProductBundle:CustomTag:featured"
            )
        );
    }    
}