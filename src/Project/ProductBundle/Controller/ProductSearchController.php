<?php

namespace Project\ProductBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use ThinkCreative\SearchBundle\Services\SearchService;
use ThinkCreative\SearchBundle\Services\EzSearchService;
use Project\ProductBundle\Command\ProductImportCommand;
/**
 * Controller for handling product searches
 */
class ProductSearchController extends Controller
{

    /**
     * Search type to use in ThinkCreativeSearch config
     * @var string
     */
    const SEARCH_TYPE = "bookstore";

    /**
     * Product search
     */
    public function searchAction(Request $request)
    {
        
        // get params
        $query = $request->query->get("query") ?: "*:*";
        $limit = $request->query->get("limit") ?: null;
        $offset = $request->query->get("offset") ?: 0;
        $sort = $request->query->get("sort") ?: "";

        // cache
        $response = new  Response();
        $response->setPublic();
        $response->setSharedMaxAge(60);        
        $response->setETag(
            md5( floor(time() / 60) . $query . $limit . $offset . $sort)   // etag
        );
        if ($response->isNotModified($request)) {
            return $response;
        }   

        // load ez search service
        $searchService = new EzSearchService(
            new SearchService(
                $this->container->getParameter("think_creative_search.solr"),
                $this->container->getParameter("think_creative_search.search")
            ),
            $this->get("ezpublish.api.service.content"),
            $this->get("ezpublish_legacy.kernel"),
            $this->get("thinkcreative.search.filter.ezpublish")
        );

        // perform search
        $results = $searchService->search(
            self::SEARCH_TYPE,
            $query ?: "*:*",
            $limit,
            $offset,
            $sort,
            $request->query->all(),
            array(
                "hl.useFastVectorHighlighter" => "true",
                "hl.fl" => "*_t"
            )
        );

        // category search
        $categorySearchResults = array();
        if ($query) {
            $categorySearchResults = $searchService->search(
                "product_category",
                $query,
                10,
                0,
                "",
                array(
                    "parent" => ProductImportCommand::PRODUCT_LOCATION_ID
                )
            );

        }

        // generate breadcrumbs
        $breadcrumbs = array();
        $locationService = $this->get("ezpublish.api.service.location");
        $location = $locationService->loadLocation(ProductImportCommand::PRODUCT_LOCATION_ID);
        foreach ($location->path as $locationId) {
            if ($locationId <= 2) {
                continue;
            }
            $subLocation = $locationService->loadLocation($locationId);
            $breadcrumbs[] = array(
                "uri" => $this->generateUrl(
                    "ez_urlalias",
                    array(
                        "locationId" => $locationId
                    )
                ),
                "name" => $subLocation->contentInfo->name
            );
        }
        $breadcrumbs[] = array(
            "uri" => "",
            "name" => "Search Results"
        );

        // get response
        $response = $this->render(
            $request->isXmlHttpRequest() ? "ProjectProductBundle:parts:search/search_page.html.twig" : "ProjectProductBundle:full:search.html.twig",
            array(
                "searchResults" => $results,
                "sortOptions" => $searchService->getSortOptions(self::SEARCH_TYPE),
                "categorySearchResults" => $categorySearchResults,
                "breadcrumbs" => $breadcrumbs,
                "searchType" => self::SEARCH_TYPE
            )
        );

        return $response;

    }

    /**
     * Product Category page via location id
     * @param integer $locationId
     * @param string $viewType
     * @param boolean $layout
     * @param array $params
     */
    public function productCategoryLocationAction($locationId, $viewType = "full", $layout = false, array $params = array(), Request $request)
    {

        // get search params
        $limit = $request->query->get("limit") ?: null;
        $offset = $request->query->get("offset") ?: 0;
        $sort = $request->query->get("sort") ?: "";

        // cache
        $response = new Response();
        $response->setPublic();
        $response->setSharedMaxAge(60);
        $response->setVary('X-Location-Id');
        $response->headers->set( 'X-Location-Id', $locationId );
        if ($response->isNotModified($request)) {
            return $response;
        }

        // load ez search service
        $searchService = new EzSearchService(
            new SearchService(
                $this->container->getParameter("think_creative_search.solr"),
                $this->container->getParameter("think_creative_search.search")
            ),
            $this->get("ezpublish.api.service.content"),
            $this->get("ezpublish_legacy.kernel"),
            $this->get("thinkcreative.search.filter.ezpublish")
        );

        // perform search
        $params['searchResults'] = $searchService->search(
            self::SEARCH_TYPE,
            "*:*",
            $limit,
            $offset,
            $sort,
            array(
                "series" => array($locationId)
            )
        );

        // return search results via ajax
        if ($request->isXmlHttpRequest()) {

            $repository = $this->get( 'ezpublish.api.repository' );
            $locationService = $repository->getLocationService();
            $contentService = $repository->getContentService();

            $location = $locationService->loadLocation($locationId);
            $content = $contentService->loadContent($location->contentInfo->id);

            return $this->render(
                "ProjectProductBundle:parts:product_category.html.twig",
                array_merge(
                    $params,
                    array(
                        "content" => $content,
                        "location" => $location
                    )
                ),
                $response
            );
        }

        // pass to ez_content:viewLocation
        return $this->get( 'ez_content' )->viewLocation($locationId, $viewType, $layout, $params);


    }

    /**
     * Product Category page via content object id
     * @param integer $locationId
     * @param string $viewType
     * @param boolean $layout
     * @param array $params
     */
    public function productCategoryContentAction($contentId, $viewType = "full", $layout = false, array $params = array(), Request $request)
    {

        // get repository
        $repository = $this->get( 'ezpublish.api.repository' );

        // get content service
        $contentService = $repository->getContentService();

        // get content
        $content = $contentService->loadContent($contentId);    
        
        // forward to productCategoryLocationAction
        return $this->productCategoryLocationAction(
            $content->contentInfo->mainLocationId,
            $viewType,
            $layout,
            $params
        );
    }

} 