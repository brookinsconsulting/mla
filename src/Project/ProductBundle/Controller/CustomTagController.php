<?php

namespace Project\ProductBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use eZ\Publish\Core\Base\Exceptions\NotFoundException;
use eZ\Publish\Core\Base\Exceptions\UnauthorizedException;
use Project\ProductBundle\Services\Product as ProductService;

/**
 * Product custom tag controller.
 */
class CustomTagController extends Controller
{

    /**
     * List of products in a given category
     * @param string $template
     * @param array $variables
     */
    public function categoryListAction($template, $variables)
    {

        // get category type and id
        list($type, $id) = explode("_", $variables['tag']);

        // get product service
        $productService = $this->get("project.product.product");

        // get content objects in category
        $moreUri = "";
        $contentList = array();
        $count = 0;
        $title = "";

        switch ($type) {

            // recent products
            case "RECENT":

                // invoke search service
                $searchService = $this->get("thinkcreative.search.ezsearch");

                // perform search for recent product
                $results = $searchService->search(
                    "bookstore",
                    "*:*",
                    intval($variables['limit']),
                    0,
                    "attr_date_added_dt desc, meta_sort_name_ms desc"
                );

                $contentList = $searchService->getContentFromSearchResults($results);
                $count = $results->getNumberFound();
                $title = "New Titles";
                $moreUri = $this->generateUrl("ProductSearch", array( "sort" => "attr_date_added_dt desc, meta_sort_name_ms asc" )) ;
                break;

            // ez tag
            case "TAG":
                // get tag service
                $tagService = $this->get("ezpublish.api.service.tags");

                // get tagged content
                $tag = $tagService->loadTag(intval($id));
                $contentList = $tagService->getRelatedContent($tag, 0, intval($variables['limit']));            

                // get parent tag
                $parentTag = $tagService->loadTag($tag->parentTagId);

                // get total count
                $count = $tagService->getRelatedContentCount($tag);

                // get category title
                $title = trim($tag->keyword);

                // set 'more' uri
                $moreUri = $this->generateUrl("ProductSearch", array( strtolower($parentTag->keyword) => strtolower($tag->keyword) )) ;

                // sort contentList alphabetically
                usort($contentList, function($a, $b) {
                    return strcmp(
                        trim(preg_replace("/[^A-Za-z0-9 ]/", '', $a->contentInfo->name)),
                        trim(preg_replace("/[^A-Za-z0-9 ]/", '', $b->contentInfo->name))
                    );
                });

                break;

            // ez publish location
            case "LOC":

                // get location service
                $locationService = $this->get("ezpublish.api.service.location");

                // get content service
                $contentService = $this->get("ezpublish.api.service.content");

                // get parent location
                try {
                    $parentLocation = $locationService->loadLocation(intval($id));
                } catch (NotFoundException $e) {
                    break;
                } catch (UnauthorizedException $e) {
                    break;
                }

                // get content
                $contentList = array();
                $locationChildren = $locationService->loadLocationChildren($parentLocation, 0, intval($variables['limit']));
                foreach ($locationChildren->locations as $location) {
                    try {
                        $contentList[] = $contentService->loadContent($location->contentInfo->id);
                    } catch (NotFoundException $e) {
                        continue;
                    } catch (UnauthorizedException $e) {
                        continue;
                    }
                }

                // get total count
                $count = count($contentList);

                // get category title
                $title = trim($parentLocation->contentInfo->name);

                // set 'more' uri
                $moreUri = $this->generateUrl("ez_urlalias", array("locationId" => intval($id)));

                // sort contentList alphabetically
                usort($contentList, function($a, $b) {
                    return strcmp(
                        trim(preg_replace("/[^A-Za-z0-9 ]/", '', $a->contentInfo->name)),
                        trim(preg_replace("/[^A-Za-z0-9 ]/", '', $b->contentInfo->name))
                    );
                });

                break;
        }

        // get images for all content objects
        $images = array();
        foreach ($contentList as $content) {
            $images[$content->contentInfo->id] = $productService->getProductImage($content);
        }

        return $this->render(
            $template,
            array(
                "category_title" => $title, 
                "content_objects" => $contentList,
                "product_images" => $images,
                "image_variation" => isset($variables['size']) ? $variables['size'] : "small",
                "limit" => intval($variables['limit']),
                "count" => $count,
                "more_uri" => $moreUri
            )
        );
    }

    /**
     * Display featured products
     * @param string $template
     * @param array $variables
     */
    public function featuredAction($template, $variables) {

        // get product service
        $productService = $this->get("project.product.product");

        // get location service
        $locationService = $this->get("ezpublish.api.service.location");

        // get content service
        $contentService = $this->get("ezpublish.api.service.content");

        // make a content list
        $contentList = array();
        for ($i = 1; $i <= 8; $i++) {

            if (!isset($variables["product-{$i}"])) {
                continue;
            }

            $product = explode("://", $variables["product-{$i}"]);

            switch ($product[0]) {
                case "eznode":
                    try {
                        $location = $locationService->loadLocation(intval($product[1]));
                        $content = $contentService->loadContent($location->contentInfo->id);
                    } catch (NotFoundException $e) {
                        break;
                    } catch (UnauthorizedException $e) {
                        break;
                    }
                    if ($content->contentInfo->contentTypeId == ProductService::PRODUCT_CLASS_ID) {
                        $contentList[] = $content;
                    }
                break;

                case "ezobject":
                    try {
                        $content = $contentService->loadContent(intval($product[1]));
                    } catch (NotFoundException $e) {
                        break;
                    } catch (UnauthorizedException $e) {
                        break;
                    }
                    if ($content->contentInfo->contentTypeId == ProductService::PRODUCT_CLASS_ID) {
                        $contentList[] = $content;
                    }                    
                break;
            }

        }

        // get images for all content objects
        $images = array();
        foreach ($contentList as $content) {
            $images[$content->contentInfo->id] = $productService->getProductImage($content);
        }

        return $this->render(
            $template,
            array(
                "category_title" => "Featured Products", 
                "content_objects" => $contentList,
                "product_images" => $images,
                "image_variation" => isset($variables['size']) ? $variables['size'] : "small",
                "limit" => count($contentList),
                "count" => count($contentList),
                "more_uri" => ""
            )
        );

    }

}