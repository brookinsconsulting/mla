<?php

namespace Project\ProductBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use eZ\Bundle\EzPublishCoreBundle\Controller;
use Netgen\TagsBundle\API\Repository\TagsService;
use Netgen\TagsBundle\API\Repository\Values\Tags\Tag;
use Project\ProductBundle\Services\Product as ProductService;

/**
 * Ez Tag Controller for Products
 */
class EzTagController extends Controller
{

    // copied from NetGenTagBundle

    /**
     * Action for rendering a tag view by using tag ID
     *
     * @param mixed $tagId
     * @param string $template
     * @param string $cart_type
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewTagByIdAction( $tag_id, $template = "", $cart_type = "default" )
    {    
        $tagsService = $this->get("ezpublish.api.service.tags");
        $tag = $tagsService->loadTag( $tag_id );
        return $this->renderTag( $tag, $template );
    }

    /**
     * Action for rendering a tag view by using tag URL
     *
     * @param string $tagUrl
     * @param string $template
     * @param string $cart_type
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewTagByUrlAction( $tag_url, $template = "", $cart_type = "default" )
    {
        $tag = $this->tagsService->loadTagByUrl( $tag_url );
        return $this->renderTag( $tag, $template );
    }

    /**
     * Renders the tag
     *
     * @param \Netgen\TagsBundle\API\Repository\Values\Tags\Tag $tag
     * @param string $template
     * @param string $cart_type
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderTag( Tag $tag, $template = "", $cart_type = "default" )
    {

        // load order
        $order = $this->get("thinkcreative.payment.order_service")->getUserOrder(
            $this->get("project.product.order_processor"), 
            $cart_type
        );

        // get tag service
        $tagsService = $this->get("ezpublish.api.service.tags");

        // get all child tags
        $children  = $tagsService->loadTagChildren($tag, 0, -1);

        // create response
        $response = new Response();
        $response->headers->set( 'X-Tag-Id', $tag->id );

        // generate breadcrumbs
        $breadcrumbs = array();
        $locationService = $this->get("ezpublish.api.service.location");
        $location = $locationService->loadLocation(ProductService::BOOKSTORE_LOCATION_ID);
        foreach ($location->path as $locationId) {
            if ($locationId <= 2) {
                continue;
            }
            $subLocation = $locationService->loadLocation($locationId);
            $breadcrumbs[] = array(
                "uri" => $this->generateUrl(
                    "ez_urlalias",
                    array(
                        "locationId" => $locationId
                    )
                ),
                "name" => $subLocation->contentInfo->name
            );
        }
        $breadcrumbs[] = array(
            "uri" => "",
            "name" => $tag->keyword
        );


        return $this->render(
            $template ?: 'ProjectSiteBundle:full:tag.html.twig',
            array(
                'breadcrumbs' => $breadcrumbs,
                'tag' => $tag,
                'children' => $children,
                'order' => $order
            ),
            $response
        );
    }

}