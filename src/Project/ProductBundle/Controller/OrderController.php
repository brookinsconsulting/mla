<?php

namespace Project\ProductBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Response;
use eZ\Publish\API\Repository\Values\Content\Content as EzContent;
use eZ\Publish\Core\Base\Exceptions\NotFoundException;
use Omnipay\Common\CreditCard;
use Omnipay\Common\Exception\InvalidCreditCardException;
use ThinkCreative\PaymentBundle\Entity\Order;
use ThinkCreative\FormBundle\Form\Populator\Countries;
use ThinkCreative\PaymentBundle\Exception\Order\PaymentGatewayErrorException;
use Project\ProductBundle\Order\OrderProcessorService;
use Project\ProductBundle\Command\ProductImportCommand;
use Project\ProductBundle\Api\ShopifyApi;
use Project\ProductBundle\Exception\Api\ApiResponseException;
use Project\MemberBundle\Classes\Member as MemberObject;
use Project\ProductBundle\Classes\Discount;
use Project\ProductBundle\Entity\DiscountUsage;

/**
 * Order Controller
 * Handles rendering of shop related pages. 
 */
class OrderController extends Controller
{

    /**
     * Location id to create new products
     * in eZPublish
     * @var integer
     */
    const PRODUCT_LOCATION_ID = 14710;

    /**
     * Relevant product tags to display on sidebar
     */
    static $tagIds = array(112, 113);

    /**
     * 'Period' tag id
     */
    const PERIOD_TAG_ID = 113;

    /**
     * 'Subject' tag id
     */
    const SUBJECT_TAG_ID = 112;

    /**
     * Retrieve current user's order.
     * @param string $type
     * @return Order
     */
    private function getOrder($type = "bookstore")
    {

        return $this->get("thinkcreative.payment.order_service")->getUserOrder(
            $this->get("project.product.order_processor"), 
            $type
        );

    }

    /**
     * Get array with path to main product page.
     * @return array
     */
    private function getBreadcrumbs()
    {
        $breadcrumbs = array();
        $locationService = $this->get("ezpublish.api.service.location");
        $location = $locationService->loadLocation(self::PRODUCT_LOCATION_ID);
        foreach ($location->path as $locationId) {
            if ($locationId <= 2) {
                continue;
            }
            $subLocation = $locationService->loadLocation($locationId);
            $breadcrumbs[] = array(
                "uri" => $this->generateUrl(
                    "ez_urlalias",
                    array(
                        "locationId" => $locationId
                    )
                ),
                "name" => $subLocation->contentInfo->name
            );
        }
        return $breadcrumbs;
    }

    /**
     * Set fees like shipping, tax, and also discounts
     * @param Order $order
     */
    private function setExtraFees($order)
    {

        // clear extra fees
        $order->removeAllExtraFees();

        // calculate shipping
        // each item = $1.50 shipping (for testing purposes)
        $order->setExtraFee("shipping", 0);
        $itemCount = 0;
        foreach ($order->getProductList() as $product_id => $qty) {
            $itemCount += $qty;
        }
        $order->setExtraFee(
            "shipping",
            $itemCount * 1.50
        );
       
        // get customer details
        $customerDetails = $order->getCustomerDetails();

        // calculate tax (based on billing address)
        $order->setExtraFee("tax", 0);
        if (isset($customerDetails['misc']['creditCard']['billingState']) && isset($customerDetails['misc']['creditCard']['billingCountry'])) {
            $taxProvince = $order->getProcessorService()->getShopifyProvince(
                $customerDetails['misc']['creditCard']['billingCountry'],
                $customerDetails['misc']['creditCard']['billingState']
            );
            if ($taxProvince) {

                $taxTotal = 0;
                foreach ($order->getProductList() as $product_id => $quantity) {
                    if ($order->getProcessorService()->getProduct($product_id)->isTaxable()) {
                        $taxTotal += $order->getProcessorService()->getProduct($product_id)->getPrice();
                    }
                }

                $order->setExtraFee(
                    "tax",
                    $taxTotal * $taxProvince['tax']
                );
            }
        }     
    }

    /**
     * Get 'subject' and 'period' tags to display
     * in sidebar
     */
    private function getRelevantTags()
    {
        // get relevant tags
        $tagService = $this->get("ezpublish.api.service.tags");
        $tags = array();
        foreach (self::$tagIds as $tagId) {
            $tag = $tagService->loadTag($tagId);

            $children = array();
            foreach ($tagService->loadTagChildren($tag, 0, 4) as $childTag) {
                $children[] = $childTag;
            }

            $tags[] = array(
                "parent" => $tag,
                "children" => $children
            );

        }        
        return $tags;
    }

    /**
     * Render sidebar cart counter
     */
    public function cartCountSidebarAction()
    {
        $order = $this->getOrder();
        $response = new Response();
        $response->setPrivate();
        $response->setETag( count($order->getProductList()) );

        return $this->render(
            "ProjectProductBundle:parts:cart_count_sidebar.html.twig",
            array(
                "order" => $this->getOrder()
            ),
            $response
        );
    }

    /**
     * Render sidebar tag search area
     */
    public function searchSidebarAction()
    {
        $order = $this->getOrder();
        $response = new Response();
        $response->setPublic();
        $response->setETag( $order->getOrderType() );
        return $this->render(
            "ProjectProductBundle:parts:search_sidebar.html.twig",
            array(
                "order" => $this->getOrder(),
                "tags" => $this->getRelevantTags()
            ),
            $response
        );        
    }

    /**
     * Render a product page from a location id.
     * @param integer $location_id
     * @param string $viewType
     * @param boolean $layout
     * @param array $params
     */
    public function productLocationAction($locationId, $viewType = "full", $layout = false, array $params = array())
    {

        // get product service
        $productService = $this->get("project.product.product");

        // get repository
        $repository = $this->get("ezpublish.api.repository");

        // get location service
        $locationService = $repository->getLocationService();

        // get content type service
        $contentTypeService = $repository->getContentTypeService();

        // get current location
        $location = $locationService->loadLocation($locationId);

        // get content service
        $contentService = $repository->getContentService();

        // get content
        $content = $contentService->loadContent($location->contentInfo->id);

        // get product image
        $params = array_merge($params, $productService->getProductImage($content));

        // get product series
        if (isset($location->parentLocationId)) {
            $parentLocation = $locationService->loadLocation($location->parentLocationId);
            $params['product_series'] = array(
                "name" => $parentLocation->contentInfo->name,
                "location_id" => $location->parentLocationId
            );
        }

        // get current order
        $params['order'] = $this->getOrder();

        // get product status messages
        $productContentType = $contentTypeService->loadContentType($content->contentInfo->contentTypeId);
        $params['status_messages'] = $productContentType->getFieldDefinition("message")->fieldSettings['options'];

        // get response
        $response = $this->get( 'ez_content' )->viewLocation( $locationId, $viewType, $layout, $params );

        // caching
        $response->setPublic();

        return $response;

    }

    /**
     * Render a product page from a content id.
     * @param integer $location_id
     * @param string $viewType
     * @param boolean $layout
     * @param array $params
     */
    public function productContentAction($contentId, $viewType = "full", $layout = false, array $params = array())
    {

        // get product service
        $productService = $this->get("project.product.product");

        // get repository
        $repository = $this->get( 'ezpublish.api.repository' );

        // get content service
        $contentService = $repository->getContentService();

        // get content
        $content = $contentService->loadContent($contentId);

        // get product image
        $params =  array_merge($params, $productService->getProductImage($content));

        // get current order
        $params['order'] = $this->getOrder();

        // get response
        $response = $this->get( 'ez_content' )->viewContent( $contentId, $viewType, $layout, $params );

        // caching
        $response->setPublic();

        return $response;

    }

    /**
     * Render's shopping cart page.
     * @param string $view
     * @param string $cart_type
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function cartAction($view = "full", $cart_type = "bookstore")
    {

        // get order
        $order = $this->getOrder($cart_type);

        // get customer details
        $customerDetails = $order->getCustomerDetails();

        // get member
        $memberService = $this->get("project.member.member");
        $member = $memberService->getCurrentMember();

        // verify cart items exists
        foreach ($order->getProductList() as $productId=>$quantity) {
            if (!$order->getProductFromId($productId)) {
                // clear item from cart
                $order->removeProduct($productId);
            }
        }

        // set extra fees
        $this->setExtraFees($order);

        // breadcrumbs
        $breadcrumbs = $this->getBreadcrumbs();
        $breadcrumbs[] = array(
            "uri" => "",
            "name" => "Cart"
        );

        // discount service
        $discountService = $this->get("project.product.discount");

        // get promo code
        $promoCode = isset($customerDetails['misc']['promoCode']) ? $customerDetails['misc']['promoCode'] : "";
        $promoCodeForm = $this->get("think_creative_form.form_service")->getForm("promo_code");
        $promoCodeDiscount = null;
        if ($promoCode || ($promoCodeForm->isSubmitted() && $promoCodeForm->isValid())) {

            // get promo code from form if needed
            if (!$promoCode) {
                $promoCode = trim($promoCodeForm->get("promo_code")->getData());
            }

            // get discount
            $promoCodeDiscount = $discountService->getDiscountFromPromoCode($promoCode);

            // Not a valid promo
            if (!$promoCodeDiscount) {
                $promoCodeDiscount = null;
                $promoCodeForm->get("promo_code")->addError(
                    new FormError("Promo code is not valid.")
                );
            // Invalid Promo
            } elseif (!$promoCodeDiscount->isActive()) {
                $promoCodeDiscount = null;
                $promoCodeForm->get("promo_code")->addError(
                    new FormError("Promo code is no longer active.")
                );                
            }
            elseif (!$promoCodeDiscount->appliesToOrder($order)) {
                $promoCodeDiscount = null;
                $promoCodeForm->get("promo_code")->addError(
                    new FormError("Promo code does not apply to items in this order or cart does not meet conditions.")
                );     
            }
            elseif (!$promoCodeDiscount->appliesToUser($member)) {
                $promoCodeDiscount = null;
                $promoCodeForm->get("promo_code")->addError(
                    new FormError("Current user is not eligible to recieve promo code discount.")
                );     
            }

            // set promo to order
            if ($promoCodeForm->isValid()) {
                $customerDetails['misc']['promoCode'] = $promoCode;
                $customerDetails['misc']['discounts'] = array();
                foreach($discountService->getApplicableDiscounts($order, $member, $promoCode) as $discount) {
                    $customerDetails['misc']['discounts'][] = $discount->getContentObjectId();
                }
                $order->setCustomerDetails(
                    isset($customerDetails['firstName']) ? $customerDetails['firstName'] : "",
                    isset($customerDetails['lastName']) ? $customerDetails['lastName'] : "",
                    isset($customerDetails['email']) ? $customerDetails['email'] : "",
                    $customerDetails['misc']            
                );
            }

        }

        // get discounts        
        $discountList = $discountService->getApplicableDiscounts($order, $member, array($promoCode));

        // apply discounts
        $order->removeAllDiscounts();
        $hasPromoCode = false;
        foreach ($discountList as $discount) {
            $discount->setOrderDiscount($order, $member);
            if ($promoCodeDiscount && $discount->getContentObjectId() == $promoCodeDiscount->getContentObjectId()) {
                $hasPromoCode = true;
            }
        }

        // cannot combine
        if ($promoCodeDiscount && !$hasPromoCode) {

            $promoCodeMessage = "Promo code '{$promoCode}' is valid but conflicts with the following discounts and will not be applied... ";
            foreach ($discountList as $discount) {
                $promoCodeMessage .= $discount->getName() . ", ";
            }
            $promoCodeMessage = substr($promoCodeMessage, 0, -2) . ".";
            $promoCodeForm->get("promo_code")->addError(
                new FormError($promoCodeMessage)
            );

        }

        // save order
        $this->get("thinkcreative.payment.order_service")->storeUserOrder($order);

        // create response
        $response = $this->render(
            "ProjectProductBundle:{$view}:cart.html.twig",
            array(
                "member" => $this->get("project.member.member")->getCurrentMember(),
                "breadcrumbs" => $breadcrumbs,
                "order" => $order,
                "product_hash_sep" => OrderProcessorService::PRODUCT_VARIANT_SEPERATOR,
                "product_location_id" => self::PRODUCT_LOCATION_ID,
                "discounts" => $discountList,
                "discount_applies" => array(
                    "product_price" => Discount::APPLIES_PRODUCT_PRICE,
                    "tax" => Discount::APPLIES_TAX,
                    "shipping" => Discount::APPLIES_SHIPPING,
                    "total" => Discount::APPLIES_TOTAL,
                    "total_shipping" => Discount::APPLIES_TOTAL_SHIPPING
                ),
                "discount_types" => array(
                    "price" => Discount::FUNCTION_DISCOUNT_PRICE,
                    "percentage" => Discount::FUNCTION_PERCERTAGE,
                    "daa" => Discount::FUNCTION_DAA
                ),
                "promo" => array(
                    "form" => $promoCodeForm->createView(),
                    "valid" => !$promoCode || ( $promoCodeForm->isValid() && $promoCodeDiscount ? true : false ),
                    "value" => $promoCode,
                    "discount" => $promoCodeDiscount
                )
            )
        );

        // no caching
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('no-store', true);

        return $response;

    }

    /**
     * Add item to cart
     * @param string $product_id_hash
     * @param string $action  (add|remove|set)
     * @param integer $quantity
     * @param string $cart_type
     * @return Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function setCartAction($product_id_hash, $action = "add", $quantity = 1, $cart_type = "bookstore")
    {

        // get order
        $order = $this->getOrder($cart_type);

        // clean up invalid items from cart
        foreach ($order->getProductList() as $productId=>$productQuantity) {
            if (!$order->getProductFromId($productId)) {
                // clear item from cart
                $order->removeProduct($productId);
            }
        }

        switch ($action) {

            // remove product
            case "remove":
                $order->removeProduct($product_id_hash);
                break;

            // set quantity
            case "set":
                if ($this->getRequest()->query->has("quantity")) {
                    $quantity = $this->getRequest()->query->get("quantity");
                }
                $order->setQuantity($product_id_hash, $quantity);
                break;

            // add product to cart
            case "add":
            default:            
                $order->addProduct($product_id_hash, $quantity);
                break;

        }

        // set extra fees
        $this->setExtraFees($order);

        // save order
        $this->get("thinkcreative.payment.order_service")->storeUserOrder($order);

        // redirect to referer
        $referer = $this->get("request")->headers->get('referer');
        if (!$referer) {
            $referer = $this->generateUrl(
                "ProductCart",
                array(
                    "cart_type" => $cart_type
                )
            );
        }

        // create response
        $response = $this->redirect($referer);

        // no caching
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('no-store', true);

        return $response;

    }

    /**
     * Same as 'setCartAction' method except pulls
     * parameters from url query.
     * @param string $cart_type
     * @return Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function setCartQueryAction($cart_type = "bookstore")
    {

        // get request
        $request = $this->getRequest();

        // get product id hash
        $productIdHash = $request->query->get("product_id_hash");

        // get action
        $action = $request->query->get("action") ?: "add";

        // get quantity
        $quantity = $request->query->get("quantity") ?: 1;

        // pass to setCartAction method
        return $this->setCartAction($productIdHash, $action, $quantity, $cart_type);
    }

    /**
     * Empties a cart completely
     * @param string $cart_type
     * @return Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function clearCartAction($cart_type = "bookstore")
    {

        // get order
        $order = $this->getOrder($cart_type);

        // remove products
        $order->removeAllProducts();

        // save order
        $this->get("thinkcreative.payment.order_service")->storeUserOrder($order);

        // redirect to referer
        $referer = $this->get("request")->headers->get('referer');
        if (!$referer) {
            $referer = $this->generateUrl(
                "ProductCart",
                array(
                    "cart_type" => $cart_type
                )

            );
        }

        // create response
        $response = $this->redirect($referer);

        // no caching
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('no-store', true);

        return $response;        

    }

    /**
     * Clear all promo codes from order
     * @param string $cart_type
     * @param string $promo_code
     */
    public function clearPromoAction($cart_type = "bookstore")
    {

        // get order
        $order = $this->getOrder($cart_type);

        // get customer details
        $customerDetails = $order->getCustomerDetails();

        // set promo
        $customerDetails['misc']['promoCode'] = "";
        $order->setCustomerDetails(
            isset($customerDetails['firstName']) ? $customerDetails['firstName'] : "",
            isset($customerDetails['lastName']) ? $customerDetails['lastName'] : "",
            isset($customerDetails['email']) ? $customerDetails['email'] : "",
            $customerDetails['misc']            
        );

        // save order
        $this->get("thinkcreative.payment.order_service")->storeUserOrder($order);

        // redirect to referer
        $referer = $this->get("request")->headers->get('referer');
        if (!$referer) {
            $referer = $this->generateUrl(
                "ProductCart",
                array(
                    "cart_type" => $cart_type
                )

            );
        }

        // create response
        $response = $this->redirect($referer);

        // no caching
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('no-store', true);

        return $response;
    }

    /**
     * Begins checkout process, asks user
     * what payment gateway to use.
     * @param string $cart_type
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function checkoutAction($cart_type = "bookstore")
    {

        // get order
        $order = $this->getOrder($cart_type);

        // clean up invalid items from cart
        foreach ($order->getProductList() as $productId=>$quantity) {
            if (!$order->getProductFromId($productId)) {
                // clear item from cart
                $order->removeProduct($productId);
            }
        }

        // make sure products in order
        if (count($order->getProductList()) <= 0) {
            $response = $this->redirect(
                $this->generateUrl(
                    "ProductCart",
                    array(
                        "cart_type" => $cart_type
                    )
                )
            );

            // no caching
            $response->setPrivate();
            $response->setMaxAge(0);
            $response->setSharedMaxAge(0);
            $response->headers->addCacheControlDirective('must-revalidate', true);
            $response->headers->addCacheControlDirective('no-store', true);

            return $response;
        }

        // set extra fees
        $this->setExtraFees($order);

        // save order
        $this->get("thinkcreative.payment.order_service")->storeUserOrder($order);

        // get checkout form
        $form = $this->get("think_creative_form.form_service")->getForm("checkout");

        // set form data if stored
        $customerDetails = $order->getCustomerDetails();
        if (!$form->isSubmitted() && $customerDetails && array_key_exists("formData", $customerDetails['misc'])) {
            $form->setData($customerDetails['misc']['formData']);

        // pull member data
        } elseif (!$form->isSubmitted()) {

            $memberService = $this->get("project.member.member");
            $member = $memberService->getCurrentMember();

            if ($member && $member->getMemberStatus() == MemberObject::MEMBER_STATUS_FULL) {
                $apiMember = $member ->getApiData();

                foreach ($apiMember->get("addresses") as $address) {
                    if (!$address['send_mail']) {
                        break;
                    }
                }
                if (!$address) {
                    $address = $apiMember->get("addresses");
                    $address = $address[0];
                }


                $form->get("shipping")->get("shippingFirstName")->setData( $apiMember->get("general[first_name]", null, true) );
                $form->get("shipping")->get("shippingLastName")->setData( $apiMember->get("general[last_name]", null, true) );
                $form->get("shipping")->get("shippingCompany")->setData(isset($address['affiliation']) ? $address['affiliation'] : "") ;
                $form->get("shipping")->get("shippingAddress1")->setData($address['line3']);
                $form->get("shipping")->get("shippingAddress2")->setData($address['line1']);
                $form->get("shipping")->get("shippingCity")->setData($address['city']);
                $form->get("shipping")->get("shippingState")->setData($address['state']);
                $form->get("shipping")->get("shippingPostcode")->setData($address['zip']);
                $form->get("shipping")->get("shippingCountry")->setData($address['country_code']);                

                $form->get("contact")->get("phone")->setData( $apiMember->get("general[phone]", null, true) );
                $form->get("contact")->get("email")->setData( $apiMember->get("general[email]", null, true));

            }
        }

        // grab info from form
        if ($form->isSubmitted() && $form->isValid()) {

            // get credit card data
            $ccData = array();

            // billing same as shipping
            if ($form->get("billing")->has("same") && $form->get("billing")->get("same")->getData()) {
                $fromLoc = "shipping";
            }

            // set shipping data
            $ccData = $form->get("shipping")->getData();

            // set billing data
            if ($form->get("billing")->has("same") && $form->get("billing")->get("same")->getData()) {
                $billData = array();
                foreach ($form->get("shipping")->getData() as $key=>$value) {
                    $billData[str_replace("shipping", "billing", $key)] = $value;
                }
                $ccData = array_merge($ccData, $billData);
            } else {
                $ccData = array_merge($ccData, $form->get("billing")->getData());
            }

            // set credit card data
            $ccData = array_merge($ccData, $form->get("payment")->getData());
            $ccData = array_merge($ccData, $form->get("payment")->get("expire")->getData());

            // set contact data
            $ccData = array_merge($ccData, $form->get("contact")->getData());
            $ccData['billingPhone'] = $form->get("contact")->get("phone")->getData();
            $ccData['shippingPhone'] = $form->get("contact")->get("phone")->getData();

            // set name
            $ccData['firstName'] = $ccData['billingFirstName'];
            $ccData['lastName'] = $ccData['billingLastName'];

            // create credit card to insure cc data validates
            try {
                $creditCard = new CreditCard($ccData);
            } catch (InvalidCreditCardException $e) {
                $form->addError( new FormError($e->getMessage() ) );
            }

            // simple shipping address verification with google maps api
            // added validation to avoid having to use shopify to check this
            // could be safely removed
            /*$gmap_geo = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode("{$ccData['shippingAddress1']} {$ccData['shippingCity']} {$ccData['shippingState']} {$ccData['shippingPostcode']} {$ccData['shippingCountry']}") );

            if ($gmap_geo) {
                $gmap_geo = json_decode($gmap_geo, True);

                if (isset($gmap_geo['status']) && $gmap_geo['status'] == "OK") {
                    if (isset($gmap_geo['results'][0]['address_components'])) {
                        foreach ($gmap_geo['results'][0]['address_components'] as $component) {
                            if (in_array("postal_code", $component['types'])) {
                                if (intval($component['short_name']) != intval($ccData['shippingPostcode'])) {
                                    $form->get("shipping")->get("shippingPostcode")->addError( new FormError("Zip code not valid for ". strtoupper("{$ccData['shippingAddress1']} {$ccData['shippingCity']}, {$ccData['shippingState']} {$ccData['shippingCountry']}.")) );
                                }
                            }
                        }
                    } else {
                        $form->get("shipping")->addError(new FormError("Address is invalid."));
                    }
                }
            }*/

            // ensure form still value
            if ($form->isValid()) {

                // store customer data
                $customerDetails = $order->getCustomerDetails();
                $order->setCustomerDetails(
                    $ccData['billingFirstName'],
                    $ccData['billingLastName'],
                    $ccData['email'],
                    array_merge(
                        isset($customerDetails['misc']) ? $customerDetails['misc'] : array(),
                        array(
                            "creditCard" => $ccData,
                            "formData" => $form->getData()
                        )
                    )
                );

                // save order
                $this->get("thinkcreative.payment.order_service")->storeUserOrder($order);

                // redirect to order summary page
                $response = $this->redirect(
                    $this->generateUrl(
                        "ProductOrderReview",
                        array(
                            "cart_type" => $cart_type
                        )
                    )
                );

                // no caching
                $response->setPrivate();
                $response->setMaxAge(0);
                $response->setSharedMaxAge(0);
                $response->headers->addCacheControlDirective('must-revalidate', true);
                $response->headers->addCacheControlDirective('no-store', true);

                return $response;                



            }

        }

        // breadcrumbs
        $breadcrumbs = $this->getBreadcrumbs();
        $breadcrumbs[] = array(
            "uri" => $this->generateUrl(
                "ProductCart",
                array(
                    "cart_type" => $order->getOrderType()
                )
            ),
            "name" => "Cart"
        );
        $breadcrumbs[] = array(
            "uri" => "",
            "name" => "Checkout"
        );

        // create response
        $response = $this->render(
            "ProjectProductBundle:full:checkout.html.twig",
            array(
                "breadcrumbs" => $breadcrumbs,
                "order" => $order,
                "form" => $form->createView(),
                "product_hash_sep" => OrderProcessorService::PRODUCT_VARIANT_SEPERATOR,
                "payment_gateways" => $this->get("thinkcreative.payment.order_service")->getPaymentGateways()
            )
        );

        // no caching
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('no-store', true);

        return $response;

    }

    /**
     * Display an order review page before finalizing order.
     * @param string $cart_type
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function orderReviewAction($cart_type = "bookstore")
    {

        // get order
        $order = $this->getOrder($cart_type);

        // clean up invalid items from cart
        foreach ($order->getProductList() as $productId=>$quantity) {
            if (!$order->getProductFromId($productId)) {
                // clear item from cart
                $order->removeProduct($productId);
            }
        }

        // customer details
        $customerDetails = $order->getCustomerDetails();

        // make sure user has completed checkout
        if (!isset($customerDetails['misc']['creditCard'])) {

            $response = $this->redirect(
                $this->generateUrl(
                    "ProductCheckout",
                    array(
                        "cart_type" => $cart_type
                    )
                )
            );
            // no caching
            $response->setPrivate();
            $response->setMaxAge(0);
            $response->setSharedMaxAge(0);
            $response->headers->addCacheControlDirective('must-revalidate', true);
            $response->headers->addCacheControlDirective('no-store', true);

            return $response;
        }

        // get list of countries
        $countries = new Countries();
        $countries = $countries->execute();

        // make sure products in order
        if (count($order->getProductList()) <= 0) {
            $response = $this->redirect(
                $this->generateUrl(
                    "ProductCart",
                    array(
                        "cart_type" => $cart_type
                    )
                )
            );

            // no caching
            $response->setPrivate();
            $response->setMaxAge(0);
            $response->setSharedMaxAge(0);
            $response->headers->addCacheControlDirective('must-revalidate', true);
            $response->headers->addCacheControlDirective('no-store', true);

            return $response;

        }

        // set extra fees (tax)
        $this->setExtraFees($order);

        // get credit card
        $creditCard = new CreditCard($customerDetails['misc']['creditCard']);

        // breadcrumbs
        $breadcrumbs = $this->getBreadcrumbs();
        $breadcrumbs[] = array(
            "uri" => $this->generateUrl(
                "ProductCart",
                array(
                    "cart_type" => $order->getOrderType()
                )
            ),
            "name" => "Cart"
        );
        $breadcrumbs[] = array(
            "uri" => $this->generateUrl(
                "ProductCheckout",
                array(
                    "cart_type" => $order->getOrderType()
                )
            ),
            "name" => "Checkout"
        );
        $breadcrumbs[] = array(
            "uri" => "",
            "name" => "Review Order"
        );

        // create response
        $response = $this->render(
            "ProjectProductBundle:full:review.html.twig",
            array(
                "breadcrumbs" => $breadcrumbs,
                "order" => $order,
                "creditCard" => $creditCard,
                "countries" => $countries
            )
        );

        // no caching
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('no-store', true);

        return $response;

    }


    /**
     * Create an error response
     * @param Order $order
     * @param string $errorMsg
     * @return Symfony\Component\HttpFoundation\Response
     */
    private function errorResponse(Order $order, $errorMsg)
    {

        // breadcrumbs
        $breadcrumbs = $this->getBreadcrumbs();
        $breadcrumbs[] = array(
            "uri" => $this->generateUrl(
                "ProductCart",
                array(
                    "cart_type" => $order->getOrderType()
                )
            ),
            "name" => "Cart"
        );
        $breadcrumbs[] = array(
            "uri" => $this->generateUrl(
                "ProductCheckout",
                array(
                    "cart_type" => $order->getOrderType()
                )
            ),
            "name" => "Checkout"
        );
        $breadcrumbs[] = array(
            "uri" => "",
            "name" => "Order Error"
        );


        $response = $this->render(
            "ProjectProductBundle:full:pay_error.html.twig",
            array(
                "breadcrumbs" => $breadcrumbs,
                "order" => $order,
                "error_message" => $errorMsg
            )
        );

        // no caching
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('no-store', true);

        return $response;

    }

    /**
     * Finalize order
     * @param string $cart_type
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function placeOrderAction($cart_type = "bookstore")
    {

        // get order
        $order = $this->getOrder($cart_type);

        // make sure products in order
        if (count($order->getProductList()) <= 0) {
            $response = $this->redirect(
                $this->generateUrl(
                    "ProductCart",
                    array(
                        "cart_type" => $cart_type
                    )
                )
            );
            // no caching
            $response->setPrivate();
            $response->setMaxAge(0);
            $response->setSharedMaxAge(0);
            $response->headers->addCacheControlDirective('must-revalidate', true);
            $response->headers->addCacheControlDirective('no-store', true);

            return $response;
        }

        // POST var 'placeOrder' must be set
        if (!$this->getRequest()->request->get("placeOrder")) {
            $response = $this->redirect(
                $this->generateUrl(
                    "ProductOrderReview",
                    array(
                        "cart_type" => $cart_type
                    )
                )
            );

            // no caching
            $response->setPrivate();
            $response->setMaxAge(0);
            $response->setSharedMaxAge(0);
            $response->headers->addCacheControlDirective('must-revalidate', true);
            $response->headers->addCacheControlDirective('no-store', true);

            return $response;
        }

        // get customer details
        $customerDetails = $order->getCustomerDetails();

        // get order service
        $orderService = $this->get("thinkcreative.payment.order_service");

        // get credit card
        $creditCard = new CreditCard($customerDetails['misc']['creditCard']);

        // authorize payment
        $transactionReference = "";
        try {

            $transactionReference = $orderService->authorizePayment(
                $order,
                $this->container->getParameter("ThinkCreativePaymentBundle_DefaultGateway"),
                $creditCard
            );

        // Invalid Credit Card
        } catch (InvalidCreditCardException $e) {
            return $this->errorResponse(
                $order,
                $e->getMessage()
            );
        
        // payment error
        } catch(PaymentGatewayErrorException $e) {
            return $this->errorResponse(
                $order,
                $e->getMessage()
            );
        }

        // create order on Shopify
        try {
            if ($transactionReference) {
                $order->getProcessorService()->createShopifyOrder($order);
            }
        
        // Shopify API Exception
        } catch (ApiResponseException $e) {

            // cancel payment
            $orderService->cancelPayment(
                $order,
                $this->container->getParameter("ThinkCreativePaymentBundle_DefaultGateway"),
                $creditCard,
                $transactionReference
            );

            // get error message from shopify response
            $errorMsg = "An unknown error occured. Please verify that your information is correct and try again.";
            $shopifyResponse = preg_replace("/HTTP Error \d\d\d : /", "", $e->getMessage());
            $shopifyResponse = json_decode($shopifyResponse, true);
            if (array_key_exists("errors", $shopifyResponse)) {
                foreach ($shopifyResponse['errors'] as $key=>$value) {
                    foreach ($value as $error) {
                        $errorMsg = $error;
                        break;
                    }
                    break;
                }
            }

            return $this->errorResponse(
                $order,
                $errorMsg
            );
        }

        // capture payment
        try {
            $orderService->capturePayment(
                $order,
                $this->container->getParameter("ThinkCreativePaymentBundle_DefaultGateway"),
                $creditCard,
                $transactionReference
            );

        // payment error
        } catch(PaymentGatewayErrorException $e) {

            // cancel shopify order
            $order->getProcessorService()->cancelShopifyOrder($order);

            // cancel payment
            $orderService->cancelPayment(
                $order,
                $this->container->getParameter("ThinkCreativePaymentBundle_DefaultGateway"),
                $creditCard,
                $transactionReference
            );

            return $this->errorResponse(
                $order,
                $e->getMessage()
            );

            return $response;

        }

        // get customer details (again)
        $customerDetails = $order->getCustomerDetails();

        // get current member
        $memberService = $this->get("project.member.member");
        $member = $memberService->getCurrentMember();        

        // log unique discounts
        $discountService = $this->get("project.product.discount");
        $discounts = $discountService->getApplicableDiscounts(
            $order,
            $member, 
            array( isset($customerDetails['misc']['promoCode']) ? $customerDetails['misc']['promoCode'] : "" )
        );

        // get doctrine
        $entityManager = $this->get("doctrine.orm.entity_manager");
        $discountRepo = $entityManager->getRepository('ProjectProductBundle:DiscountUsage');

        // iterate and persist unique discounts to DiscountUsage entity
        foreach ($discounts as $discount) {
            if ($discount->isUnique()) {
                $discountUsage = new DiscountUsage();
                $discountUsage->setDiscount($discount);
                $discountUsage->setMemberId($member ? $member->getMemberApiId() : null);
                $discountUsage->setOrderId($customerDetails['misc']['shopifyOrderId']);
                $entityManager->persist($discountUsage);
                $entityManager->flush();
            }
        }

        // create success response
        return $this->redirect(
            $this->generateUrl(
                "ProductOrderConfirmation",
                array(
                    "shopify_order_id" => $customerDetails['misc']['shopifyOrderId']
                )
            )
        );

    }

    /**
     * Retrieve an order from shopify
     * @param integer $shopify_order_id
     * @return array
     */
    private function getShopifyOrder($shopify_order_id)
    {

        // get shopify api
        $shopifyConfig = $this->container->getParameter("ProjectProduct_ShopifyApiConfiguration");
        $this->shopifyApi = new ShopifyApi(
            isset($shopifyConfig['api_key']) ? $shopifyConfig['api_key'] : "",
            isset($shopifyConfig['api_password']) ? $shopifyConfig['api_password'] : "",
            isset($shopifyConfig['api_host']) ? $shopifyConfig['api_host'] : ""
        );

        // request order data
        return $this->shopifyApi->request(
            "orders/{$shopify_order_id}",
            "GET",
            array(
                "fields" => implode(",", array(
                    "id",
                    "name",
                    "email",
                    "created_at",
                    "subtotal_price",
                    "total_price",
                    "total_discounts",
                    "tax_lines",
                    "line_items",
                    "shipping_address",
                    "shipping_lines",
                    "billing_address",
                    "note_attributes"
                ))
            )
        );

    }

    /**
     * Display order confirmation message
     * @param integer $shopify_order_id
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function orderConfirmAction($shopify_order_id)
    {

        // look up order id
        try { 
            $orderData = $this->getShopifyOrder($shopify_order_id);
        } catch(ApiResponseException $e) {
            throw $this->createNotFoundException("Order with ID '{$shopify_order_id}' was not found.");
        }

        // breadcrumbs
        $breadcrumbs = $this->getBreadcrumbs();
        $breadcrumbs[] = array(
            "uri" => "",
            "name" => "Order Success"
        );


        // create response
        return $this->render(
            "ProjectProductBundle:full:pay_success.html.twig",
            array(
                "breadcrumbs" => $breadcrumbs,
                "orderData" => $orderData['order']
            )
        );

    }

    /**
     * Display order reciept
     * @param integer $shopify_order_id
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function orderReceiptAction($shopify_order_id)
    {

        // look up order id
        try { 
            $orderData = $this->getShopifyOrder($shopify_order_id);
        } catch(ApiResponseException $e) {
            throw $this->createNotFoundException("Order with ID '{$shopify_order_id}' was not found.");
        }

        // breadcrumbs
        $breadcrumbs = $this->getBreadcrumbs();
        $breadcrumbs[] = array(
            "uri" => $this->generateUrl(
                "ProductOrderConfirmation",
                array(
                    "shopify_order_id" => $shopify_order_id
                )
            ),
            "name" => "Order Success"
        );
        $breadcrumbs[] = array(
            "uri" => "",
            "name" => "Order Receipt"
        );

        // create response
        return $this->render(
            "ProjectProductBundle:full:receipt.html.twig",
            array(
                "breadcrumbs" => $breadcrumbs,
                "orderData" => $orderData['order']
            )
        );

    }

}