<?php

namespace Project\ProductBundle\FieldType\Matrix;

use eZ\Publish\Core\FieldType\FieldType;
use eZ\Publish\Core\Base\Exceptions\InvalidArgumentType;
use eZ\Publish\Core\FieldType\ValidationError;
use eZ\Publish\Core\FieldType\XmlText\Input;
use eZ\Publish\Core\FieldType\XmlText\Input\EzXml;
use eZ\Publish\SPI\FieldType\Value as SPIValue;
use eZ\Publish\SPI\Persistence\Content\FieldValue;
use eZ\Publish\Core\FieldType\Value as BaseValue;
use eZ\Publish\API\Repository\Values\Content\Relation;
use eZ\Publish\Core\FieldType\XmlText\Type as EzXmlFieldType;
//use eZ\Publish\Core\FieldType\TextLine\Type as EzTextLineFieldType;
use eZ\Publish\Core\Persistence\Legacy\Content\FieldValue\Converter\XmlText;
use Project\ProductBundle\FieldType\Matrix\Value as MatrixValue;
use DOMDocument;
use RuntimeException;

/**
 * Matrix field type.
 */
class Type extends EzXmlFieldType
{

    /**
     * {@inheritdoc}
     */
    public function fromPersistenceValue( FieldValue $fieldValue )
    {

        if ($fieldValue->data) {
            if (is_a($fieldValue->data, "DOMDocument")) {
                return new Value($fieldValue->data);
            
            } elseif (is_string($fieldValue->data)) {
                $xml = new DOMDocument();
                $xml->loadXML($fieldValue->data);
                return new Value( $xml );
            }
        }
        return new Value( null );
    }

    /**
     * {@inheritdoc}
     */
    public function toPersistenceValue( SPIValue $value )
    {

        // get xml
        if (!isset($value->columns)) {
            $value = MatrixValue::buildFromArray(array(), array());
        } elseif (!$value->xml) {
            $value->generateXml();
        }
        
        return new FieldValue(
            array(
                'data'         => $value->xml,
                'externalData' => null,
                'sortKey'      => $this->getSortInfo( $value )
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function fromHash( $hash )
    {
        if ( !isset( $hash["xml"] ) )
        {
            if ( !isset( $hash["matrix"] ) || !isset( $hash["matrixColumns"] ) ) {
                throw new RuntimeException( "'xml' or 'matrix' index is missing in hash." );
            }

            return Value::buildFromArray($hash['columns'], $hash['values']);
        }

        $doc = new DOMDocument;
        $doc->loadXML( $hash['xml'] );
        return new Value( $doc );
    }

    /**
     * {@inheritdoc}
     */
    public function toHash( SPIValue $value ) 
    {
        if (!$value->xml) {
            $value->generateXml();
        }
        return parent::toHash($value);
    }


}