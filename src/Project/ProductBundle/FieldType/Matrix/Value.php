<?php

namespace Project\ProductBundle\FieldType\Matrix;

use eZ\Publish\Core\FieldType\XmlText\Value as EzXmlValue;
//use eZ\Publish\Core\FieldType\TextLine\Value as EzTextLineValue;
use DOMDocument;

/**
 * Value for Matrix field type
 */
class Value extends EzXmlValue
{

    /**
     * Array containing matrix values
     * @var array
     */
    public $values = array();

    /**
     * Array containing matrix column titles
     * @var array
     */
    public $columns = array();

    /**
     * Constructor
     * @param DOMDocument $xmlDoc
     */
    public function __construct( DOMDocument $xmlDoc = null )
    {
        // parent method
        parent::__construct($xmlDoc);

        // parse matrix
        foreach($this->xml->getElementsByTagName('ezmatrix') as $ezmatrix) {

            // get columns
            $columns = array();
            foreach ($ezmatrix->getElementsByTagName('columns') as $xmlColumnList)
            {
                foreach ($xmlColumnList->getElementsByTagName('column') as $xmlColumnItem)
                {
                    $columns[$xmlColumnItem->getAttribute("id")] = $xmlColumnItem->nodeValue;
                }
                break;
            }
            $this->columns = $columns;


            // get rows
            $rows = array();
            $currentRow = array();
            $columnIds = array_keys($columns);
            foreach ($ezmatrix->getElementsByTagName("c") as $xmlColumnValue)
            {
                $currentRow[$columnIds[count($currentRow)]] = $xmlColumnValue->nodeValue;
                if (count($currentRow) > count($columns) - 1) {
                    $rows[] = $currentRow;
                    $currentRow = array();
                }
            }
            
            $this->values = $rows;
            break;
        }

    }

    /**
     * Generate XML from matrix values
     * @return DOMDocument
     */
    public function generateXml()
    {
        $doc = new DOMDocument("1.0", "UTF-8");

        // create ezmatrix element
        $ezMatrixElement = $doc->createElement("ezmatrix");
        $doc->appendChild($ezMatrixElement);

        // create name element
        $nameElement = $doc->createElement("name", "");
        $ezMatrixElement->appendChild($nameElement);

        // create columns element
        $columnsElement = $doc->createElement("columns");
        $columnsElement->setAttribute("number", count($this->columns));
        $ezMatrixElement->appendChild($columnsElement);

        $counter = 0;
        foreach ($this->columns as $id=>$name) {
            $columnElement = $doc->createElement("column", $name);
            $columnElement->setAttribute("num", $counter);
            $columnElement->setAttribute("id", $id);
            $columnsElement->appendChild($columnElement);
            $counter += 1;
        }

        // create rows
        $rowsElement = $doc->createElement("rows");
        $rowsElement->setAttribute("number", count($this->values));
        $ezMatrixElement->appendChild($rowsElement);

        if ($this->values) {

            foreach ($this->values as $row) {
                foreach ($row as $value) {       
                    $rowItemElement = $doc->createElement("c", trim($value));
                    $ezMatrixElement->appendChild($rowItemElement);
                }
            }
        }                 

        return $doc;

    }

    /**
     * Builds Value object from Matrix column and value arrays
     * @param array $columns
     * @param array $values
     * @return Value
     */
    static function buildFromArray(array $columns, array $values = array())
    {
        $value = new self(null);
        $value->columns = $columns;
        $value->values = $values;
        $value->xml = $value->generateXml();
        return $value;
    }
}