<?php

namespace Project\ProductBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {

        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('project_product');

        $rootNode
            ->children()
                ->arrayNode("ShopifyApi")
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode("api_key")
                            ->defaultValue("shopify_store_api_key")
                        ->end()
                        ->scalarNode("api_password")
                            ->defaultValue("shopify_store_api_password")
                        ->end()
                        ->scalarNode("api_host")
                            ->defaultValue("mystore.myshopify.com")
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;

    }
}