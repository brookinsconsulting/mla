<?php

namespace Project\ProductBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Yaml\Yaml;

class ProjectProductExtension extends Extension
{

    public function load(array $configs, ContainerBuilder $container) {

        // process configuration
        $processor = new Processor();
        $configuration = new Configuration();
        $config = $processor->processConfiguration($configuration, $configs);

        // setup shopify api conf
        $container->setParameter("ProjectProduct_ShopifyApiConfiguration", $config['ShopifyApi']);

        // load services
        $YamlFileLoader = new Loader\YamlFileLoader(
            $container, new FileLocator(__DIR__ . '/../Resources/config')
        );
        $YamlFileLoader->load('services.yml');

    }

    public function getAlias() {
        return 'project_product';
    }

}