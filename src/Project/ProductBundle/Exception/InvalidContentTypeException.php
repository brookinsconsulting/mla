<?php

namespace Project\ProductBundle\Exception;

class InvalidContentTypeException extends \Exception
{
}