<?php

namespace Project\ProductBundle\Form\Validator\ValidationGroups;
use Symfony\Component\Form\FormInterface;

class Checkout
{

    /**
     * Determines what fields should be validated
     * for checkout form.
     * @param FormInterface $form
     */
    static function determineValidationGroups(FormInterface $form)
    {
        // if 'same' checkbox is checked under billing then 
        // don't validation billing
        if (!$form->get("billing")->get("same")->getData()) {
            return array("global", "billing");
        }
        return array("global");
    }
}