<?php
namespace Project\ProductBundle\Form\Validator\Constraints;
use Symfony\Component\Validator\Constraint;

/**
 * Promo code constraint for API.
 */
class PromoCode extends Constraint
{
    public $invalidPromoCodeMessage = "Promo code is not valid.";
    public $inactiveDiscountMessage = "Promo code is no longer active.";

    public function validatedBy()
    {
        return "project.product.validator.promo_code";
    }
}