<?php
namespace Project\ProductBundle\Form\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\HttpFoundation\Session\Session;
use Project\ProductBundle\Services\DiscountService;
use Project\MemberBundle\Services\Member as MemberService;
use Project\ApiBundle\Exception\ApiResponseException;
use Project\ApiBundle\Services\Api;

/**
 * Duplicate email constraint.
 */
class PromoCodeValidator extends ConstraintValidator
{

    /**
     * Discount service
     * @var DiscountService
     */
    protected $discountService;

    /**
     * Constructor.
     * @param Api $member
     * @param MemberService $memberService
     */
    public function __construct(DiscountService $discount_service)
    {
        $this->discountService = $discount_service;
    }

    /**
     * Validate Promo Code
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {

        // no value is ok
        if (!$value) {
            return;
        }

        // get discount
        $discount = $this->discountService->getDiscountFromPromoCode(trim($value));

        // invalid
        if (!$discount) {
            $this->context->addViolation(
                $constraint->invalidPromoCodeMessage,
                array()
            );
            return;
        }

        // inactive
        if (!$discount->isActive()) {
            $this->context->addViolation(
                $constraint->inactiveDiscountMessage,
                array()
            );
            return;
        }

    }
}