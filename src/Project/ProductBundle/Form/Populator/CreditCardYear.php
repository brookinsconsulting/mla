<?php

namespace Project\ProductBundle\Form\Populator;
use ThinkCreative\FormBundle\Form\Populator\PopulatorInterface;

/**
 * Form populator for credit card expiration year
 */
class CreditCardYear implements PopulatorInterface
{

    /**
     * Maximum number of years from now a expiration
     * year can be.
     * @var integer
     */
    const MAX_RANGE = 16;

    /**
     * {@inheritdoc}
     */
    public function execute()
    {

        // build list of years
        $yearList = array();
        for ($i = date("Y"); $i < date("Y") + self::MAX_RANGE; $i++) {
            $yearList[$i - (date("Y") - date("y"))] = $i;
        }

        return $yearList;
    }
}
