<?php

namespace Project\ProductBundle\Form\Populator;
use ThinkCreative\FormBundle\Form\Populator\PopulatorInterface;

/**
 * Form populator for months of the year
 */
class Month implements PopulatorInterface
{

    /**
     * {@inheritdoc}
     */
    public function execute()
    {

        // build list of months
        $monthList = array();
        for ($i = 1; $i <= 12; $i++) {
            $time = mktime(0,0,0,$i,1,date("Y"));
            $monthList[$i] = date("F", $time);
        }
        return $monthList;
    }
}
