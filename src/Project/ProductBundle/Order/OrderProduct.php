<?php

namespace Project\ProductBundle\Order;
use eZ\Publish\API\Repository\Values\Content\Content;
use ThinkCreative\PaymentBundle\Product\ProductInterface;
use Project\ApiBundle\Endpoints\ProductVariant;
use Project\ProductBundle\Services\Product as ProductService;

/**
 * Product class for ThinkCreative Payment Bundle
 */
class OrderProduct implements ProductInterface
{

    /**
     * Product content object
     * @var Content
     */
    protected $productContent;

    /**
     * Product data recieved from API
     * @var ProductVariant
     */
    protected $productVariant;

    /**
     * Array containing info to get the product imgae
     * @var array
     */
    protected $productImage;

    /**
     * Constructor.
     * @param ProductService $product_service
     * @param integer $product_id
     * @param string $variant_id
     */
    public function __construct(ProductService $product_service, $product_id, $variant_id)
    {

        $this->productContent = $product_service->getProduct($product_id);
        $this->productVariant = $product_service->getProductVariantById($product_id, $variant_id);
        $this->productImage = $product_service->getProductImage($this->productContent);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->productContent->getFieldValue("name")->text;
    }

    /**
     * Get information about product image
     * @return array
     */
    public function getImage()
    {
        return $this->productImage;
    }

    /**
     * Get product location id
     * @return integer
     */
    public function getLocationId()
    {
        return $this->productContent->contentInfo->mainLocationId;
    }

    /**
     * Return the name of the product variant
     * @return string
     */
    public function getVariantName()
    {
        return $this->productVariant->get("name");
    }

    /**
     * {@inheritdoc}
     */
    public function getPrice()
    {
        return $this->productVariant->get("unit_price");
    }

    /**
     * Get product variant id (MLA oracle id)
     * @return string
     */
    public function getVariantId()
    {
        return $this->productVariant->get("id");
    }

    /**
     * Get product ISBN
     * @return string
     */
    public function getIsbn()
    {
        return $this->productVariant->get("isbn");
    }

    /**
     * Get the Shopify variant ID used for
     * interacting wtih the API
     * @return integer|boolean
     */
    public function getShopifyVariantId()
    {
        if (!$this->productVariant->has("shopify_variant_id")) {
            return false;
        }
        return $this->productVariant->get("shopify_variant_id");
    }

    /**
     * Get product taxability
     * @return boolean
     */
    public function isTaxable()
    {
        return (bool) $this->productVariant->get("taxable");
    }

    /**
     * Get array of related product ids
     * @return array
     */
    public function getRelatedProductIds()
    {
        return $this->productContent->getFieldValue("related_products")->destinationContentIds;
    }

    /**
     * Get product content object
     * @return Content
     */
    public function getProductContent()
    {
        return $this->productContent;
    }

}