<?php
namespace Project\ProductBundle\Order;
use eZ\Publish\Core\Base\Exceptions\NotFoundException;
use ThinkCreative\PaymentBundle\OrderProcessorService\OrderProcessorServiceInterface;
use ThinkCreative\PaymentBundle\Entity\Order;
use Omnipay\Common\CreditCard;
use Omnipay\Common\Exception\InvalidCreditCardException;
use Project\ProductBundle\Api\ShopifyApi;
use Project\ApiBundle\Services\Api as ProjectApi;
use Project\ProductBundle\Services\Product as ProductService;
use Project\MemberBundle\Services\Member as MemberService;
use Project\MemberBundle\Entity\Member as MemberEntity;
use Project\MemberBundle\Classes\Member;
use Project\ProductBundle\Order\OrderProduct;
use Project\ProductBundle\Exception\InvalidOrderException;
use Project\ProductBundle\Services\DiscountService;
use Project\MemberBundle\Classes\FormApiConverter;

/**
 * ThinkCreativePaymentBundle Order Processor Service
 */
class OrderProcessorService implements OrderProcessorServiceInterface
{

    /**
     * String to seperate product id from
     * variant id in product_id_hash
     * @var string
     */
    const PRODUCT_VARIANT_SEPERATOR = "-:-";

    /**
     * Product Service
     * @var ProductService;
     */
    protected $productService;

    /**
     * Shopify API object
     * @var ShopifyApi
     */
    protected $shopifyApi;

    /**
     * Api object
     * @var ProjectApi
     */
    protected $projectApi;

    /**
     * Member service
     * @var MemberService
     */
    protected $projectMember;

    /**
     * Product discount service
     * @var DiscountService
     */
    protected $discountService;

    /**
     * Constructor
     */
    public function __construct(ProductService $product_service, ProjectApi $project_api, MemberService $project_member, DiscountService $discount_service, array $shopify_config)
    {
        $this->productService = $product_service;
        $this->projectApi = $project_api;
        $this->projectMember = $project_member;
        $this->discountService = $discount_service;

        // get shopify api credientials
        $this->shopifyApi = new ShopifyApi(
            isset($shopify_config['api_key']) ? $shopify_config['api_key'] : "",
            isset($shopify_config['api_password']) ? $shopify_config['api_password'] : "",
            isset($shopify_config['api_host']) ? $shopify_config['api_host'] : ""
        );

    }

    /**
     * {@inheritdoc}
     */
    public function getProduct($product_id_hash)
    {

        $product_id_hash = explode(self::PRODUCT_VARIANT_SEPERATOR, $product_id_hash);

        $product_id = $product_id_hash[0];
        $variant_id = $product_id_hash[1];

        try {
            return new OrderProduct(
                $this->productService,
                $product_id,
                $variant_id
            );
        } catch (NotFoundException $e) {
            return null;
        }
    }

    /**
     * Open an order with Shopify for verification purposes.
     */
    public function createShopifyOrder(Order $order) {

        // get customer info
        $customerDetails = $order->getCustomerDetails();

        // get tax info
        $taxProvince = false;
        if ($order->getExtraFee("tax")) {
            $taxProvince = $order->getProcessorService()->getShopifyProvince(
                $customerDetails['misc']['creditCard']['billingCountry'],
                $customerDetails['misc']['creditCard']['billingState']
            );
        }

        // get credit card
        $creditCard = new CreditCard($customerDetails['misc']['creditCard']);

        // build order
        $shopifyOrder = array(
            //"name" => $order->getOrderIdentifier(),
            "email" => $customerDetails['email'],
            "customer" => array(
                "first_name" => $customerDetails['firstName'],
                "last_name" => $customerDetails['lastName'],
                "email" => $customerDetails['email']
            ),
            "send_receipt" => true,
            "line_items" => array(),
            "transactions" => array(
                array(
                    "kind" => "sale",
                    "status" => "success",
                    "amount" => $order->getTotalPrice(true, true),
                    "payment_details" => array(
                        "credit_card_company" => $creditCard->getBrand(),
                        "credit_card_number" => $creditCard->getNumber() ? $creditCard->getNumberMasked() : ""
                    )
                )
            ),
            "financial_status" => "paid",
            "currency" => "USD",
            "billing_address" => array(
                "address1" => $customerDetails['misc']['creditCard']['billingAddress1'],
                "address2" => $customerDetails['misc']['creditCard']['billingAddress2'],
                "city" => $customerDetails['misc']['creditCard']['billingCity'],
                "company" => isset($customerDetails['misc']['creditCard']['billingCompany']) ? $customerDetails['misc']['creditCard']['billingCompany'] : "",
                "country" => $customerDetails['misc']['creditCard']['billingCountry'],
                "first_name" => $customerDetails['misc']['creditCard']['billingFirstName'],
                "last_name" => $customerDetails['misc']['creditCard']['billingLastName'],
                "phone" => $customerDetails['misc']['creditCard']['phone'],
                "province" => $customerDetails['misc']['creditCard']['billingState'],
                "zip" => $customerDetails['misc']['creditCard']['billingPostcode']
            ),
            "shipping_address" => array(
                "address1" => $customerDetails['misc']['creditCard']['shippingAddress1'],
                "address2" => $customerDetails['misc']['creditCard']['shippingAddress2'],
                "city" => $customerDetails['misc']['creditCard']['shippingCity'],
                "company" => isset($customerDetails['misc']['creditCard']['shippingCompany']) ? $customerDetails['misc']['creditCard']['shippingCompany'] : "",
                "country" => $customerDetails['misc']['creditCard']['shippingCountry'],
                "first_name" => $customerDetails['misc']['creditCard']['shippingFirstName'],
                "last_name" => $customerDetails['misc']['creditCard']['shippingLastName'],
                "phone" => $customerDetails['misc']['creditCard']['phone'],
                "province" => $customerDetails['misc']['creditCard']['shippingState'],
                "zip" => $customerDetails['misc']['creditCard']['shippingPostcode']                
            ),
            "shipping_lines" => array(),
            "taxes_included" => false,
            "subtotal_price" => $order->getTotalPrice(false, false),
            "total_discounts" => $order->getDiscountTotal(),
            "total_price" => $order->getTotalPrice(true, true)
        );

        // get current member (if available)
        $member = $this->projectMember->getCurrentMember();

        // note the member id in the order
        $shopifyOrder['note_attributes'] = array(
            "member_id" => 0
        );
        if ($member) {
            $shopifyOrder['note_attributes']['member_id'] = $member->getMemberApiId();
        }

        // note any discounts
        $shopifyOrder['note_attributes']['promo_code'] = isset($customerDetails['misc']['promoCode']) ? $customerDetails['misc']['promoCode'] : "";
        $shopifyOrder['note_attributes']['discounts'] = isset($customerDetails['misc']['discounts']) ? implode(",", $customerDetails['misc']['discounts']) : "";

        // add shipping if provided
        if ($order->getExtraFee("shipping")) {
            $shopifyOrder["shipping_lines"][] = array(
                "code" => "Standard Shipping",
                "title" => "Standard Shipping",
                "source" => "mla",
                "price" => $order->getExtraFee("shipping")
            );
        }

        // add tax if provided
        if ($taxProvince['tax'] && $order->getExtraFee("tax") > 0) {
            $shopifyOrder["total_tax"] = $order->getExtraFee("tax");
            $shopifyOrder["tax_lines"] = array(array(
                "price" => $order->getExtraFee("tax"),
                "rate"  => $taxProvince['tax'],
                "title" => $taxProvince['tax_name']
            ));
        }

        // get items in order
        foreach ($order->getProductList() as $productId=>$quantity) {
            $product = $this->getProduct($productId);
            list($productId, $variantId) = explode(self::PRODUCT_VARIANT_SEPERATOR, $productId);

            if (!$product->getShopifyVariantId()) {
                $shopifyOrder['line_items'][] = array(
                    "title" => $product->getName() . " - " . $product->getVariantName(),
                    "price" => $product->getPrice(),
                    "product_id" => $productId,
                    "variant_id" => $variantId,
                    "quantity" => $quantity,
                    "fulfillment_status" => "null",
                    "requires_shipping" => false,
                    "sku" => $productId . "_" . $variantId
                );
            } else {
                $shopifyOrder['line_items'][] = array(
                    "variant_id" => intval($product->getShopifyVariantId()),
                    "variant_title" => $product->getVariantName(),
                    "sku" => $productId . "_" . $variantId,
                    //"price" => $product->getPrice(),
                    "quantity" => $quantity,
                    "fulfillment_status" => "null",
                    "requires_shipping" => false
                );
            }
        }

        // look for membership product in order and add membership_year note
        foreach ($order->getProductList() as $productId => $quantity) {
            // get product id
            $productId = explode(OrderProcessorService::PRODUCT_VARIANT_SEPERATOR, $productId);

            // has membership item in order
            if ($productId[0] == ProductService::MEMBERSHIP_OBJECT_ID) {
                $shopifyOrder['note_attributes']['membership_year'] = $this->projectMember->getCurrentSignupYear();
                break;
            }
        }

        // add 'joint membership fee'
        if ($member && $order->getExtraFee("joint-membership") > 0) {
            $shopifyOrder['line_items'][] = array(
                "title" => "Joint Membership",
                "price" => $order->getExtraFee("joint-membership"),
                "quantity" => 1,
                "fulfillment_status" => "null",
                "requires_shipping" => false,
                "sku" => "_ADDON_MEMBERSHIP_JOINT"
            );
            $shopifyOrder['note_attributes']['joint_member_id'] = isset($customerDetails['misc']['jointMemberId']) ? $customerDetails['misc']['jointMemberId'] : null;
        }

        // create shopify order
        $response = $this->shopifyApi->request("orders", "POST", array(), array(
            "order" => $shopifyOrder
        ));

        if (array_key_exists("id", $response['order'])) {
            // store shopify order id to order object
            $order->setCustomerDetails(
                $customerDetails['firstName'],
                $customerDetails['lastName'],
                $customerDetails['email'],
                array_merge($customerDetails['misc'], array(
                        "shopifyOrderId" => $response['order']['id']
                    )
                )
            );
            return true;
        }

        throw new InvalidOrderException("Unable to complete order. Please try again.");

    }

    /**
     * Cancel a shopify transaction
     * @param Order $order
     */
    public function cancelShopifyOrder(Order $order)
    {

        // get customer info
        $customerDetails = $order->getCustomerDetails();

        // no shopify order id
        if (!array_key_exists("shopifyOrderId", $customerDetails['misc'])) {
            throw new InvalidOrderException("Unable to complete order.");
            return false;
        }

        // update shopify order
        $response = $this->shopifyApi->request("orders/{$customerDetails['misc']['shopifyOrderId']}/cancel", "POST", array(), array(
            "amount" => false,
            "email" => true
        ));        

    }

    /**
     * Attempt to find tax rate from country code + province code
     * @param string $country_code
     * @param string $province_code
     * @return array  Contains information about province which includes tax info
     */
    public function getShopifyProvince($country_code, $province_code)
    {

        $response = $this->shopifyApi->request(
            "countries",
            "GET",
            array(),
            array()
        );
        if (!array_key_exists("countries", $response)) {
            return false;
        }

        foreach ($response['countries'] as $country) {
            if ($country['code'] == $country_code && array_key_exists("provinces", $country)) {

                foreach ($country['provinces'] as $province) {
                    if ($province['code'] == $province_code) {
                        return $province;
                    }
                }

            }
        }

        return false;
    }

    /**
     * Fires when payment is captured.
     * @param Order $order
     * @param integer $transacation_reference
     */
    public function orderCaptured(Order $order, $transaction_reference)
    {

        // get customer info
        $customerDetails = $order->getCustomerDetails();

        // get current member
        $member = $this->projectMember->getCurrentMember();

        // get order discounts
        $discountList = $this->discountService->getApplicableDiscounts($order, $member, isset($customerDetails['misc']['promoCode']) ? array($customerDetails['misc']['promoCode']) : array());

        // api e-commerce endpoint
        $orderLines = array();
        foreach ($order->getProductList() as $productId=>$quantity) {
            $product = $this->getProduct($productId);
            list($productId, $variantId) = explode(self::PRODUCT_VARIANT_SEPERATOR, $productId);

            // get order discount price
            $discountTotal = $product->getPrice();
            foreach ($discountList as $discount) {
                $discountTotal -= $discount->getProductDiscountPrice(
                    $product->getProductContent(),
                    $product->getVariantId()
                );
            }

            $orderLines[] = array(
                "item" => $variantId,
                "quantity" => $quantity,
                "unit_selling_price" => $discountTotal,
                "unit_list_price" => $product->getPrice(),
                "category" => strtolower($product->getProductContent()->getFieldValue("shopify_category")->text ?: "bookstore")
            );
        }

        // add 'joint membership fee'
        if ($member && $order->getExtraFee("joint-membership") > 0) {
            $orderLines[] = array(
                "item" => "_ADDON_MEMBERSHIP_JOINT",
                "quantity" => 1,
                "unit_selling_price" => $order->getExtraFee("joint-membership"),
                "unit_list_price" => $order->getExtraFee("joint-membership"),
                "category" => "membership"
            );
        }

        $response = $this->projectApi->request(
            "ecommerce",
            "POST",
            array(),
            array(
                "member_id" => $member ? $member->getMemberApiId() : null,
                "order" => array(
                    "shopify_id" => $customerDetails['misc']['shopifyOrderId'],
                    "payment_id" => $transaction_reference,
                    "payment_subtotal" => $order->getTotalPrice(false, true),
                    "tax_total" => $order->getExtraFee("tax") ?: 0,
                    "shipping_total" => $order->getExtraFee("shipping") ?: 0,
                    "first_name" => $customerDetails['firstName'],
                    "last_name" => $customerDetails['lastName'],
                    "email" => $customerDetails['email'],
                    "phone" => $customerDetails['misc']['creditCard']['phone'],
                    "country" => $customerDetails['misc']['creditCard']['shippingCountry'],
                    "company" => isset($customerDetails['misc']['creditCard']['shippingCompany']) ? $customerDetails['misc']['creditCard']['shippingCompany'] : "",
                    "address1" => $customerDetails['misc']['creditCard']['shippingAddress1'],
                    "address2" => $customerDetails['misc']['creditCard']['shippingAddress2'],
                    "city" => $customerDetails['misc']['creditCard']['shippingCity'],
                    "postal_code" => $customerDetails['misc']['creditCard']['shippingPostcode'],
                    "state" => $customerDetails['misc']['creditCard']['shippingState'],
                    "county" => ""
                ),
                "order_lines" => $orderLines
            )
        );

        if ($member) {

            // post member dues to api if applicable
            $this->projectMember->postMemberDues($member, $order);

            // post contributions to api if applicable
            $this->projectMember->postMemberContributions($member, $order, $transaction_reference);

            // post benefits
            $this->projectMember->postMemberBenefits($member, $order);

        }

    }

}