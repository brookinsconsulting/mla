// product line product description truncate
$(function(){
    $('.product-line .product-desc .content').each(function() {
        $(this).html(
            $.truncate(
                $(this).html(),
                {
                    length: 256,
                    stripTags: false,
                    words: true,
                    noBreaks: false,
                    ellipsis: "..."
                }
            )
        );
    });
});

// product embed rollover
$(function() {
    $(".product-embed").hover(function(e) {
        console.log(e.type);
        if (e.type == "mouseenter") {
            topPos = $(this).height() - $(this).find(".product-desc").height();
            $(".product-embed .product-desc").css({"top" : ""});
            $(this).find(".product-desc").css({ "top" : topPos + "px" });
        } else if (e.type == "mouseleave") {
            $(this).find(".product-desc").css({"top" : ""});
        }
    });
});

// product add to cart dropdown
$(document).ready(function() {

    $(".product-addtocart select.dropdown").hide();
    $(".product-addtocart div.dropdown").show();

    $(".product-addtocart .dropdown ul li a").click(function() {
        
        // hide selected choice
        $(this).parent().parent().find("li").show();
        $(this).parent().hide();

        // make choice selected, display it as main dropdown button
        $(this).parent().parent().parent()
            .attr(
                "data-selected", 
                $(this).parent().attr("data-value")
            )
            .find("a.btn .btn-text")
                .html($(this).html())
                .attr("title", $(this).html())
        ;

        // change form 'select' field
        $(this).parent().parent().parent().parent().find("select").val($(this).parent().attr("data-value"));

        return true;
    });
});