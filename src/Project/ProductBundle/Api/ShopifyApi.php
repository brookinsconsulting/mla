<?php

namespace Project\ProductBundle\Api;
use Project\ProductBundle\Exception\Api\ApiCurlException;
use Project\ProductBundle\Exception\Api\ApiResponseException;

/**
 * Class for interfacing with Shopify's API.
 */
class ShopifyApi
{

    /**
     * API Key.
     * @var string
     */
    protected $apiKey;

    /**
     * API Secret.
     * @var string
     */
    protected $apiPassword;

    /**
     * API Host.
     * @var string
     */
    protected $apiHost = "";

    /**
     * Constructor.
     * @param string $api_key
     * @param string $api_secret
     * @param string $api_uri
     */
    public function __construct($api_key, $api_password, $api_host)
    {
        $this->apiKey = $api_key;
        $this->apiPassword = $api_password;
        $this->apiHost = rtrim($api_host, '/');
    }

    /**
     * Make a request to remote server.
     * @param string $endpoint
     * @param string $request_method
     * @param array $url_params
     * @param array $post_params
     */
    public function request($endpoint, $request_method = "GET", $url_params = array(), $post_params = array())
    {

        // generate url
        $uri = "https://" . $this->apiKey . ":" . $this->apiPassword . "@" . $this->apiHost . "/admin/" . $endpoint . ".json" . ($url_params ? ("?" . http_build_query($url_params)) : "");

        // setup curl
        $curl = curl_init();
        $curl_opts = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $uri,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            //CURLOPT_VERBOSE => true
        );

        // set request method
        if (strtoupper($request_method) != "GET") {
            $curl_opts[CURLOPT_CUSTOMREQUEST] = strtoupper($request_method);
            if ($post_params) {
                $curl_opts[CURLOPT_POST] = true;
                $curl_opts[CURLOPT_HTTPHEADER] = array('Content-Type: application/json');
                $curl_opts[CURLOPT_POSTFIELDS] = json_encode($post_params);
            }
        }

        // set curl options
        curl_setopt_array($curl, $curl_opts);

        // perform request
        $result = curl_exec($curl);
        $httpResponseNo = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        // error
        if (curl_errno($curl)) {
            throw new ApiCurlException("Curl Err No " . curl_errno($curl) . ": " . curl_error($curl));
        }

        // close curl
        curl_close($curl);


        // http response not 2xx
        if ($httpResponseNo < 200 || $httpResponseNo > 299) {
            throw new ApiResponseException("HTTP Error {$httpResponseNo} : {$result}");
        }

        // no results
        if (!trim($result)) {
            throw new ApiResponseException("No results were returned.");
        }

        // determine if error was returned
        $json_result = json_decode($result, true);

        // returned error
        if (array_key_exists("status", $json_result) && strtolower($json_result['status']) == "error") {
            throw new ApiResponseException("API returned error: " . $json_result['message']);
        }

        return $json_result;

    }

}

