<?php

namespace Project\ProductBundle\Matcher;

use eZ\Publish\Core\MVC\Symfony\Matcher\ContentBased\MultipleValued;
use eZ\Publish\API\Repository\Values\Content\Location;
use eZ\Publish\API\Repository\Values\Content\ContentInfo;

class PathStringWildcard extends MultipleValued
{

    public function matchLocation( Location $location )
    {
        foreach (array_keys($this->values) as $pathString) {
            if (fnmatch($pathString, $location->pathString)) {
                return true;
            }
        }
        return false;
    }

    public function matchContentInfo( ContentInfo $contentInfo )
    {
        $mainLocation = $this->repository
            ->getLocationService()
            ->loadLocation( $contentInfo->mainLocationId );
        return $this->matchLocation($mainLocation);
    }

}