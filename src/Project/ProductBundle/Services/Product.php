<?php

namespace Project\ProductBundle\Services;

use eZ\Publish\API\Repository\Repository;
use eZ\Publish\Core\Base\Exceptions\BadStateException;
use eZ\Publish\API\Repository\Values\Content\Content;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\Core\Base\Exceptions\InvalidArgumentException;
use eZ\Publish\Core\Base\Exceptions\NotFoundException;
use eZ\Publish\Core\Base\Exceptions\UnauthorizedException;
use Project\ProductBundle\Api\ShopifyApi;
use Project\ApiBundle\Services\Api;
use Project\ApiBundle\Endpoints\ProductVariant;
use Project\ProductBundle\FieldType\Matrix\Value as MatrixValue;
use Project\ProductBundle\Exception\InvalidContentTypeException;
use Project\ProductBundle\Exception\PromoCodeNotFoundException;
use Project\ProductBundle\Exception\Api\ApiCurlException;
use Project\ProductBundle\Exception\Api\ApiResponseException;

/**
 * Product Service
 */
class Product
{

    /**
     * API Service
     * @var Project\ApiBundle\Services\Api
     */
    protected $api;

    /**
     * eZ Publish API Repository
     * @var eZ\Publish\API\Repository\Repository
     */
    protected $ezPublishApiRepository;

    /**
     * Content class id to use when creating
     * products in eZPublish
     * @var integer
     */
    const PRODUCT_CLASS_ID = 21;

    /**
     * Content class id used for promo codes
     * @var integer
     */
    const PROMO_CLASS_ID = 50;

    /**
     * Object id of special 'membership'
     * product.
     * @var integer
     */
    const MEMBERSHIP_OBJECT_ID = 13911;

    /**
     * Object id of special 'donation' product.
     * @var integer
     */
    const DONATION_OBJECT_ID = 24599;

    /**
     * Location id of Bookstore
     * @var integer
     */
    const BOOKSTORE_LOCATION_ID = 14710;

    /**
     * Object id of Bookstore missing cover image
     * @var integer
     */
    const BOOKSTORE_MISSING_COVER_OBJECT_ID = 29415;

    /**
     * Shopify API object
     * @var ShopifyApi
     */
    protected $shopifyApi;

    /**
     * Columns in ezpublish product content type
     * variant matrix
     * @var array
     */
    public static $variantMatrixColumns = array(
        "id" => "Oracle ID",
        "name" => "Variant Name (Binding Type)",
        "isbn" => "ISBN",
        "unit_price" => "Unit Price",
        "out_of_print" => "Out Of Print",
        "availability" => "Availability",
        "copyright_year" => "Copyright Year",
        "publish_date" => "Publish Date",
        "shopify_variant_id" => "Shopify Variant ID",
        "taxable" => "Taxable",
        "weight" => "Weight"
    );

    /**
     * Constructor
     * @param Project\ApiBundle\Services\Api $api
     * @param eZ\Publish\API\Repository\Repository $ezpublish_api_repository
     * @param integer $product_content_class_id
     * @param integer $product_root_content_object_id
     */
    public function __construct(
        Api $api, 
        Repository $ezpublish_api_repository,
        array $shopify_config
    ) {
        $this->api = $api;
        $this->ezPublishApiRepository = $ezpublish_api_repository;

        // get shopify api credientials
        $this->shopifyApi = new ShopifyApi(
            isset($shopify_config['api_key']) ? $shopify_config['api_key'] : "",
            isset($shopify_config['api_password']) ? $shopify_config['api_password'] : "",
            isset($shopify_config['api_host']) ? $shopify_config['api_host'] : ""
        );

    }

    /**
     * Get product variant by product id + variant id
     * @param integer $product_id
     * @param string $variant_id
     * @return ProductVariant|boolean
     */
    public function getProductVariantById($product_id, $variant_id)
    {

        // get required ezpublish services
        $contentService = $this->ezPublishApiRepository->getContentService();

        // load ez content object
        $content = $contentService->loadContent($product_id);

        // make sure content object is of the correct type
        if ($content->contentInfo->contentTypeId != self::PRODUCT_CLASS_ID) {
            throw new InvalidContentTypeException("Unable to retrieve product variant for given content type.");
        }

        // get variants
        $variants = $content->getFieldValue("variants");

        // loop through the variants, try to find given variant id
        foreach ($variants->values as $variant) {
            if ($variant['id'] == $variant_id) {
                return new ProductVariant($variant);
            }
        }

        return false;

    }

    /**
     * Returns an array of all products as ParameterBags
     * @return ProductVariant[]
     */
    public function getAllProductVariants()
    {
        return ProductVariant::getAllVariants($this->api);
    }

    /**
     * Returns all product variants for given ez publish
     * content object
     * @param Content $content
     * @return ProductVariant[]
     */
    public function getProductVariants(Content $content)
    {

        // make sure content object is of the correct type
        if ($content->contentInfo->contentTypeId != self::PRODUCT_CLASS_ID) {
            throw new InvalidContentTypeException("Unable to retrieve product variants for given content type.");
        }

        // get the variant matrix field
        $variants = $content->getFieldValue("variants");

        // create return array
        $productVariants = array();
        foreach ($variants->values as $variant) {
            $productVariants[] = new ProductVariant($variant);
        }
        return $productVariants;
    }

    /**
     * Retrieve an ez publish content object
     * for product of given id.
     * @param integer $product_id
     * @throws InvalidContentTypeException|NotFoundException|UnauthorizedException
     * @return Content
     */
    public function getProduct($product_id)
    {

        // get content service
        $contentService = $this->ezPublishApiRepository->getContentService();

        // load product of given id
        $content = $contentService->loadContent($product_id);

        // make sure content object is of the correct type
        if ($content->contentInfo->contentTypeId != self::PRODUCT_CLASS_ID) {
            throw new InvalidContentTypeException("Invalid content type for product sync.");
        }

        return $content;

    }

    /**
     * Sync variants for special 'Membership'
     * product.
     * @param boolean $create_new_product
     */
    public function syncMembershipProduct($create_new_product = false)
    {

        // get required ezpublish services
        $contentService = $this->ezPublishApiRepository->getContentService();        
        $languageService = $this->ezPublishApiRepository->getContentLanguageService();

        // load content object
        $content = $contentService->loadContent(self::MEMBERSHIP_OBJECT_ID);

        // update
        // get class codes
        $classCodeLookup = $this->api->request(
            "lookups/class_codes",
            "GET",
            array("year" => date("Y"))
        );

        // iterate class codes
        $matrixValues = $content->getFieldValue("variants")->values;
        foreach ($classCodeLookup['data'] as $classCodeItem) {

            // update existing
            $hasExisting = false;
            foreach ($matrixValues as $key=>$values) {
                if ($values['id'] == "_" . $classCodeItem['id']) {
                    $matrixValues[$key]['name'] = "Membership Class " . strtoupper($classCodeItem['id']);
                    $matrixValues[$key]['unit_price'] = $classCodeItem['rate'];
                    $hasExisting = true;
                    break;
                }
            }
            if ($hasExisting) {
                continue;
            }

            // add to matrix array
            $matrixValues[] = array(
                "id" => "_" . $classCodeItem['id'],
                "name" => "Membership Class " . strtoupper($classCodeItem['id']),
                "isbn" => "",
                "unit_price" => $classCodeItem['rate'],
                "out_of_print" => false,
                "availability" => "-1",
                "copyright_year" => "",
                "publish_date" => "",
                "shopify_variant_id" => "",
                "taxable" => "",
                "weight" => "0"
            );                
        }

        // create variant
        $variants = MatrixValue::buildFromArray(self::$variantMatrixColumns, $matrixValues);

        // update content
        $contentUpdateStruct = $contentService->newContentUpdateStruct();
        $contentUpdateStruct->initialLanguageCode = $languageService->getDefaultLanguageCode() ?: "eng-US";
        $contentUpdateStruct->setField("variants", $variants);

        // create draft
        $contentDraft = $contentService->createContentDraft( $content->contentInfo );
        $contentDraft = $contentService->updateContent( $contentDraft->versionInfo, $contentUpdateStruct );

        // publish updated verfsion
        $content = $contentService->publishVersion( $contentDraft->versionInfo );

        // sync with shopify
        $this->syncShopifyProduct($content, $create_new_product);

        return true;

    }

    /**
     * Get image for existing product.
     * @param Content $content
     * @return array
     */
    public function getProductImage(Content $content)
    {

        // make sure content object is of the correct type
        if ($content->contentInfo->contentTypeId != self::PRODUCT_CLASS_ID) {
            throw new InvalidContentTypeException("Invalid content type.");
        }

        // get content service
        $contentService = $this->ezPublishApiRepository->getContentService();

        // store image and alt in array
        $params = array();

        if (!$content) {
            return $params;
        }

        // get image id
        $imageId = 
            isset($content->getFieldValue("cover_image")->destinationContentId) && $content->getFieldValue("cover_image")->destinationContentId ?
            $content->getFieldValue("cover_image")->destinationContentId :
            self::BOOKSTORE_MISSING_COVER_OBJECT_ID     // use placeholder image if cover image not available
        ; 

        // look for media library image       
        try {
            $coverImageContent = $contentService->loadContent( intval($imageId) );
        } catch (NotFoundException $e) {              
            return array();
        } catch (UnauthorizedException $e) {
            return array();
        }

        $params['cover_image_field'] = $coverImageContent->getField("image");
        $params['cover_image_version'] = $coverImageContent->versionInfo;
        $params['cover_image_alt'] = $coverImageContent->getFieldValue("image")->alternativeText;

        return $params;

    }
 
    /**
     * Update a product's variants from the API
     * @param Content $content
     * @param array $api_data (Optional)
     * @return Content|null  Return updated content object if updated, null if no update
     */
    public function syncProductVariants(Content $content, array $api_data = array())
    {

        // make sure content object is of the correct type
        if ($content->contentInfo->contentTypeId != self::PRODUCT_CLASS_ID) {
            throw new InvalidContentTypeException("Invalid content type for product sync.");
        }

        // grab variant data from api if needed
        if (!$api_data) {
            $api_data = ProductVariant::getAllVariants($this->api);
        }

        // get services
        $contentService = $this->ezPublishApiRepository->getContentService();
        $languageService = $this->ezPublishApiRepository->getContentLanguageService();

        // get content variants
        $contentVariants = $content->getFieldValue("variants")->values;

        // updated variants
        $updatedVariants = $contentVariants;

        foreach ($api_data as $apiVariant) {
            foreach ($contentVariants as $contentVariantKey=>$contentVariant) {
                if ($apiVariant->get("id") == $contentVariant['id']) {
                    $updatedVariants[$contentVariantKey] = array_merge($contentVariant, $apiVariant->all());
                    $updatedVariants[$contentVariantKey]['name'] = $apiVariant->get("binding");
                    unset($updatedVariants[$contentVariantKey]['binding']);
                }
            }
        }

        // no change
        if ($updatedVariants == $contentVariants) {
            return false;
        }

        // content update struct
        $contentUpdateStruct = $contentService->newContentUpdateStruct();
        $contentUpdateStruct->initialLanguageCode = $languageService->getDefaultLanguageCode() ?: "eng-US";

        // update variants
        $contentUpdateStruct->setField("variants", MatrixValue::buildFromArray(self::$variantMatrixColumns, $updatedVariants) );

        try {

            // create draft
            $contentDraft = $contentService->createContentDraft( $content->contentInfo );
            $contentDraft = $contentService->updateContent( $contentDraft->versionInfo, $contentUpdateStruct );

            // publish updated version
            return $contentService->publishVersion( $contentDraft->versionInfo );

        } catch (NotFoundException $e) {
            return;
        } catch (UnauthorizedException $e) {
            return;
        }
    }

    /**
     * Add/update a product on Shopify
     * @param Content $content
     * @param boolean $create_new_products
     * @return Content|null
     */
    public function syncShopifyProduct(Content $content, $create_new_product = false)
    {
        // make sure content object is of the correct type
        if ($content->contentInfo->contentTypeId != self::PRODUCT_CLASS_ID) {
            throw new InvalidContentTypeException("Invalid content type for product sync.");
        }

        // get "handle" for product
        $handle = 
            $content->getFieldValue("short_name")->text ? 
            str_replace(" ", "-", preg_replace("/[^A-Za-z0-9 ]/", "", strtolower(strip_tags($content->getFieldValue("short_name")->text)))) :
            str_replace(" ", "-", preg_replace("/[^A-Za-z0-9 ]/", "", strtolower(strip_tags($content->getFieldValue("name")->text))))
        ;

        // find product image
        $image = $this->getProductImage($content);
        $productImage = "";
        if ($image) {
            if (file_exists(getcwd() . $image['cover_image_field']->value->uri)) {
                 $productImage = base64_encode( file_get_contents( getcwd() . $image['cover_image_field']->value->uri ));
            }
        }

        // create/update variant list
        $variants = array();
        foreach ($content->getFieldValue("variants")->values as $variant) {
            if (!$variant['name']) { continue; }
            $variants[] = array(
                "option1" => $variant['name'],
                "title" => $variant['name'],
                "sku" => $variant['id'],
                "price" => $variant['unit_price'],
                "barcode" => $variant['isbn'],
                "requires_shipping" =>  ($content->getFieldValue("shopify_category")->text ?: "Bookstore") == "Bookstore" && !$content->getFieldValue("digital")->bool,
                "inventory_quantity" => intval($variant['availability']),
                "taxable" => true,
                "weight" => $variant['weight']

            );
            if ($variant['shopify_variant_id'] && !$create_new_product) {
                $variants[count($variants) - 1]['id'] = $variant['shopify_variant_id'];
            }
        }

        // must contain a variant
        if (count($variants) == 0) {
            return;
        }

        // create/update shopify product
        $shopifyResponse = $this->shopifyApi->request(
            $content->getFieldValue("shopify_id")->text && !$create_new_product ? "products/".$content->getFieldValue("shopify_id")->text : "products",
            $content->getFieldValue("shopify_id")->text && !$create_new_product ? "PUT" : "POST",
            array(),
            array(
                "product" => array(
                    "handle" => $handle,
                    "title" => $content->getFieldValue("name")->text,
                    "product_type" => $content->getFieldValue("shopify_category")->text ?: "Bookstore",
                    "vendor" => "Modern Language Association",
                    "images" => $productImage ? array( array( "attachment" => $productImage ) ) : array(),
                    "variants" => $variants
                )
            )
        );

        // update shopify id's in content
        if ($shopifyResponse && isset($shopifyResponse['product'])) {

            // create content update struct
            $contentService = $this->ezPublishApiRepository->getContentService();
            $languageService = $this->ezPublishApiRepository->getContentLanguageService();
            $contentUpdateStruct = $contentService->newContentUpdateStruct();
            $contentUpdateStruct->initialLanguageCode = $languageService->getDefaultLanguageCode() ?: "eng-US";

            // update shopify id
            $contentUpdateStruct->setField("shopify_id", (string) $shopifyResponse['product']['id']);

            // update shopify variant ids
            $variants = $content->getFieldValue("variants")->values;
            foreach ($variants as $key=>$variant) {
                foreach ($shopifyResponse['product']['variants'] as $shopifyVariant) {
                    if (
                        ($variant['id'] && $variant['id'] == $shopifyVariant['sku']) || 
                        ($variant['isbn'] && $variant['isbn'] == $shopifyVariant['barcode'])
                    ) {
                        $variants[$key]['shopify_variant_id'] = $shopifyVariant['id'];
                        break;
                    }
                }
            }
            $contentUpdateStruct->setField("variants", MatrixValue::buildFromArray(self::$variantMatrixColumns, $variants) );

            try {

                // create draft
                $contentDraft = $contentService->createContentDraft( $content->contentInfo );
                $contentDraft = $contentService->updateContent( $contentDraft->versionInfo, $contentUpdateStruct );

                // publish updated verfsion
                return $contentService->publishVersion( $contentDraft->versionInfo );

            } catch (NotFoundException $e) {
                return;
            } catch (UnauthorizedException $e) {
                return;
            }
        }

    }

}


