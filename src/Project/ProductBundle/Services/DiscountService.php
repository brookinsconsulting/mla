<?php

namespace Project\ProductBundle\Services;
use eZ\Publish\API\Repository\Values\Content\Content;
use eZ\Publish\Core\Repository\LocationService;
use eZ\Publish\Core\Repository\ContentService;
use eZ\Publish\Core\Repository\SearchService;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use Doctrine\ORM\EntityManager;
use ThinkCreative\PaymentBundle\Entity\Order;
use Project\ProductBundle\Classes\Discount;
use Project\MemberBundle\Classes\Member;

/**
 * Product Discount Service
 */
class DiscountService
{

    /**
     * eZPublish Location Service 'ezpublish.api.service.location'
     * @var LocationService
     */
    protected $locationService;

    /**
     * eZPublish Content Service 'ezpublish.api.service.content'
     * @var ContentService
     */
    protected $contentService;

    /**
     * eZPublish Search Service 'ezpublish.api.service.search'
     * @var SearchService
     */    
    protected $searchService;

    /**
     * Doctrine Entity Manager
     * @var EntityManager
     */
    protected $doctrineEntityManager;

    /**
     * Constructor.
     * @param LocationService $location_service
     */
    public function __construct(LocationService $location_service, ContentService $content_service, SearchService $search_service, EntityManager $doctrine_entity_manager)
    {
        $this->locationService = $location_service;
        $this->contentService = $content_service;
        $this->searchService = $search_service;
        $this->doctrineEntityManager = $doctrine_entity_manager;
    }

    /**
     * Get a discount object from a promo code
     * @param string $promo_code
     * @return Discount
     */
    public function getDiscountFromPromoCode($promo_code)
    {
        // perform criterion search
        $query = new Query();
        $query->criterion = new Criterion\LogicalAnd(
            array(
                new Criterion\ContentTypeId(Discount::DISCOUNT_CLASS_ID),
                new Criterion\Field("promotional_code", Criterion\Operator::EQ, trim($promo_code))
            )
        );
        $searchResults = $this->searchService->findContent($query);
        if ($searchResults->totalCount <= 0) {
            return null;
        }
        return new Discount(
            $this->locationService,
            $searchResults->searchHits[0]->valueObject,
            $this->doctrineEntityManager
        );

    }

    /**
     * Get a discount object from a discount content object
     * @param Content $discount_content
     * @return Discount
     */
    public function getDiscountFromContentObject(Content $discount_content)
    {
        return new Discount(
            $this->locationService,
            $discount_content,
            $this->doctrineEntityManager
        );
    }

    /**
     * Return list of discounts that can be applied to order.
     * These are discounts that don't require a code and apply
     * based on met conditions. The best possible combination is picked
     * based on what discounts can and cannot be combined.
     * @param Order $order
     * @param Member $member Optional.
     * @param array|string $promo_codes Promo codes to apply to order
     * @return Discount[]
     */
    public function getApplicableDiscounts(Order $order, Member $member = null, $promo_codes = array())
    {

        // fix promo_code being a non array
        if (!is_array($promo_codes)) {
            $promo_codes = array($promo_codes);
        }

        // perform criterion search
        $query = new Query();
        $query->criterion = new Criterion\LogicalAnd(
            array(
                new Criterion\ContentTypeId(Discount::DISCOUNT_CLASS_ID),
            )
        );
        $searchResults = $this->searchService->findContent($query);
        if ($searchResults->totalCount <= 0) {
            return null;
        }

        // iterate results and verify discount
        $availableDiscounts = array();
        foreach ($searchResults->searchHits as $hit) {
            $discount = new Discount(
                $this->locationService,
                $hit->valueObject,
                $this->doctrineEntityManager
            );

            // if discount has code make sure it's applied
            if ($discount->getCode() && !in_array($discount->getCode(), $promo_codes)) {
                continue;
            }

            // ensure discount is value
            if (
                $discount->isActive() &&
                $discount->appliesToOrder($order) &&
                $discount->appliesToUser($member)
            ) {
                $availableDiscounts[] = $discount;
            }
        }

        // determine discount amount of all combinables
        $combinableDiscounts = array();
        $combinableTotal = 0;

        // find all combinable results
        foreach ($availableDiscounts as $discount) {
            if ($discount->canCombine()) {
                $combinableDiscounts[] = $discount;
                $combinableTotal += $discount->getTotalDiscountPrice($order);
            }
        }

        // find the best non combinable discount
        $bestDiscountAmount = 0;
        $bestDiscount = null;
        foreach ($availableDiscounts as $discount) {
            if (!$discount->canCombine()) {
                if ($discount->getTotalDiscountPrice($order) > $bestDiscountAmount) {
                    $bestDiscountAmount = $discount->getTotalDiscountPrice($order);
                    $bestDiscount = $discount;
                }
            }
        }

        // if non combined discount is greatest
        if ($bestDiscountAmount > $combinableTotal) {
            return array($bestDiscount);
        }

        // otherwise use combinable discounts
        return $combinableDiscounts;
    }

}