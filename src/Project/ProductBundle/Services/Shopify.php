<?php

namespace Project\ProductBundle\Services;

use Project\ProductBundle\Api\ShopifyApi;

/**
 * Contains methods for interaction with Shopify
 */
class Shopify
{

    /**
     * @var Project\ProductBundle\Api\ShopifyApi
     */
    protected $shopifyApi;

    /**
     * Constructor.
     * @param array $shopifyApiConfig (ProjectProduct_ShopifyApiConfiguration)
     */
    public function __construct(array $shopifyApiConfig)
    {
        $this->shopifyApi = new ShopifyApi(
            isset($shopifyApiConfig['api_key']) ? $shopifyApiConfig['api_key'] : "",
            isset($shopifyApiConfig['api_password']) ? $shopifyApiConfig['api_password'] : "",
            isset($shopifyApiConfig['api_host']) ? $shopifyApiConfig['api_host'] : ""
        );
    }

    /**
     * Retrieve an order from shopify
     * @todo Consider merging this with Project\ProductBundle\Controller\OrderController::getShopifyOrder
     * @param integer $shopify_order_id
     * @return array
     */
    public function getShopifyOrder($shopify_order_id)
    {
        // request order data
        return $this->shopifyApi->request(
            "orders/{$shopify_order_id}",
            "GET",
            array(
                "fields" => implode(",", array(
                    "id",
                    "name",
                    "email",
                    "created_at",
                    "subtotal_price",
                    "total_price",
                    "total_discounts",
                    "tax_lines",
                    "line_items",
                    "shipping_address",
                    "billing_address",
                    "note_attributes",
                ))
            )
        );

    }

    /**
     * Retrieve a list of transaction for given Shopify order
     * @param integer $shopify_order_id
     * @return array
     */
    public function getShopifyTransactions($shopify_order_id)
    {
        // request order data
        return $this->shopifyApi->request(
            "orders/{$shopify_order_id}/transactions",
            "GET",
            array()
        );

    }    
}
