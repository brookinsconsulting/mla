<?php

namespace Project\LegacyBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ProjectLegacyBundle extends Bundle
{

    protected $name = "ProjectLegacyBundle";

}