Overview:
The eZ Collaboration Workflow extension...

Features:
- Leverages existing eZ Publish functionality (workflow applies at the object AND version level) but in a customizable and flexible way, making it easy to install and maintainable
- Unified interface for new content and content updates
- Content to be approved is kept in the content tree with indicators and links for further interaction
- Approval dashboard showing all pending content, their status, and the latest comment
- Commenting interface to have a dialog around content
- Ability to preview content
- Configurable e-mail notifications
- Approval workflows can be linear or parallel
- Ability for a "one-click" super-approver
- Approval rules and flows are configurable in an INI file
- Workflows configurable by section, subtree, content class, users, and more, meaning you can have different sets of workflows on the same site

Requirements:
This extension was developed and tested with eZ Publish 4.5. It is likely to work with older versions of eZ Publish.
This extension does not have any further requirements.

Notes:
In order to show the content flags for "non-public" and "pending" objects, this extension overrides the default node view template in the "admin2" design:
"design/admin2/templates/node/view/full.tpl".

See also:
License -- <extension_name>/LICENSE
Installation instructions -- <extension_name>/doc/INSTALL
Quick Start -- http://youtu.be/gLfG-U2tRlI