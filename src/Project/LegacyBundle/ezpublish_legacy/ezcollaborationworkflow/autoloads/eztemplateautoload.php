<?php
/**
* Activate eZ Collaboration Workflow template operators
*
* @copyright //autogen//
* @license //autogen//
* @version //autogen//
*/

$eZTemplateOperatorArray = array();
$eZTemplateOperatorArray[] = array( 'script' => 'extension/ezcollaborationworkflow/classes/ezcollaborationworkflowtemplateoperators.php',
                                    'class' => 'ezcollaborationworkflowTemplateOperators',
                                    'operator_names' => array( 'get_workflow_versions'
                                                             ) );
?>
