<?php 
/**
* Extends the eZ API class eZCollaborationSimpleMessage to store our own messages
*
* @copyright //autogen//
* @license //autogen//
* @version //autogen//
*/
class ezcollaborationworkflowMessage extends eZCollaborationSimpleMessage
{
	
	public function __construct( $row )
	{
		$this->eZPersistentObject( $row );
	}
		
	static function fetchByObjectId( $id, $asObject = true )
	{
		return eZPersistentObject::fetchObjectList( eZCollaborationSimpleMessage::definition(),
		                                        null,
		                                        array( "data_int1" => $id ), array( 'created' => 'desc' ),
		                                        $asObject );
	}

	static function fetchLatestCommentForObjectId( $id, $asObject = true )
	{
		$return = null;
		
		$limitation = array( 'offset' => 0,
		                     'length' => 1 );
		
		$result = eZPersistentObject::fetchObjectList( eZCollaborationSimpleMessage::definition(),
		                                        null,
		                                        array( "data_int1" => $id ), array( 'created' => 'desc' ), $limitation,
		                                        $asObject );
		
		if( count( $result ) )
		{
			$return = $result[ 0 ];
		}
		
		return $return;
	}
	
}

?>