<?php 

/**
 * Description
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowAction
{
	/**
	 * @var ezcollaborationworkflowDecisionTable
	 */
	protected $decisionTable;
	protected $rules_ini;
	protected $approval_ini;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->rules_ini    = eZINI::instance( 'ezcollaborationworkflow_rules.ini' );
		$this->approval_ini = eZINI::instance( 'ezcollaborationworkflow.ini' );
	}

	/**
	 * @param array $decisionObject
	 * @param string action_value
	 * @return boolean
	 */
	public function execute( $decisionObject, $action_value )
	{
		return $this;
	}
	
	/**
	 * @param ezcollaborationworkflowDecisionTable $dt
	 * @return ezcollaborationworkflowAction
	 */
	public function setDecisionTable( ezcollaborationworkflowDecisionTable $dt )
	{
		$this->decisionTable = $dt;
		return $this;
	}
	
	/**
	 * @return ezcollaborationworkflowDecisionTable
	 */
	public function getDecisionTable()
	{
		return $this->decisionTable;
	}
	
	/**
	 * Reads and returns the _config block for a given action
	 * 
	 * @param string $action_value
	 * @return Ambigous <NULL, array>
	 */
	protected function getActionConfig( $action_value )
	{
		$actionConfigName = $action_value . '_config';
		return $this->rules_ini->group( $actionConfigName );
	}
}

?>