<?php 

/**
 * Description
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowAction_CreateLogEntry extends ezcollaborationworkflowAction
{
	/**
	 * @param array $decisionObject
	 * @param string action_value
	 * @return boolean
	 */
	public function execute( $decisionObject, $action_value )
	{
		$current_ts = time();
		$user = eZUser::currentUser();
		$user_comment = isset( $decisionObject[ 'extra' ][ 'action_comment' ] ) ? $decisionObject[ 'extra' ][ 'action_comment' ] : '';
			
		$comment_row = array(
				'message_type' => 'object_comment',
				'data_int1'    => $decisionObject[ 'object' ]->attribute( 'id' ),
				'data_int2'    => $decisionObject[ 'version' ]->attribute( 'version' ),
				'data_text1'   => $user_comment,
				'data_text2'   => $action_value,
				'creator_id'   => $user->attribute( 'contentobject_id' ),
				'created'      => $current_ts,
				'modified'     => $current_ts
		);
			
		$comment = new ezcollaborationworkflowMessage( $comment_row );
		$comment->store();
		
		return $this;
	}
}

?>