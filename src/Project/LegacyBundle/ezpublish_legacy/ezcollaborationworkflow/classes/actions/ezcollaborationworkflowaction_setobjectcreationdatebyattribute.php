<?php 

/**
 * Sets the system internal creation date of an object to a value from the data_map attribute
 * 
 * Example config:
 * 
 * [approve_1_actions]
 * SetState[]=simple_approval/final
 * SetState[]=approval/approved
 * SetState[]=visibility/public
 * CreateLogEntry=Version was approved.
 * SetObjectCreationDatebyAttribute=publish_date
 * PublishVersion=true
 * 
 * Make sure the content class has an attribute identified by 'publish_date' (should be a date datatype).
 * 
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowAction_SetObjectCreationDateByAttribute extends ezcollaborationworkflowAction
{
	/**
     * $action_value is the attribute identifier.
     * 
	 * @param array $decisionObject
	 * @param string action_value
	 * @return boolean
	 */
	public function execute( $decisionObject, $action_value )
	{
		$data_map = $decisionObject[ 'version' ]->attribute( 'data_map' );

		if( isset( $data_map[ $action_value ] ) && $data_map[ $action_value ]->attribute( 'has_content' ) )
		{
			$publish_time = $data_map[ $action_value ]->attribute( 'data_int' );

			// Making sure not to set the published time to a value in the future
			if( $publish_time < time() )
			{
				$decisionObject[ 'object' ]->setAttribute( 'published', $publish_time );
				$decisionObject[ 'object' ]->store();
			}
		}

		return $this;
	}
}
