<?php 

/**
 * When a pending version is rejected, remove it from the workflow if there are no other pending or queued versions
 * In all cases, set the "Approval" state to "Approved" (that is what removes it from the workflow)
 * If it is a new object (i.e. it's not public), then "Simple Approval" should stay as "Draft"
 * If it is an existing object, then "Simple Approval" should be set to "Final"
 */

class ezcollaborationworkflowAction_RemoveFromWorkflow extends ezcollaborationworkflowAction
{
    /**
     * @param array $decisionObject
     * @param string action_value
     * @return boolean
     */
    public function execute( $decisionObject, $action_value )
    {
        $object = $decisionObject[ 'object' ];
        $version = $decisionObject[ 'version' ];
        $otherWorkflowVersions = ezcollaborationworkflowFunctionCollection::get_approve_versions( $object );
        
        // This version is still a workflow version
        if( count( $otherWorkflowVersions ) <= 1 )
        {
            // If this is an existing object, then "Simple Approval" should be set to "Final"
            $states = $object->attribute( 'state_identifier_array' );
            if( in_array( 'visibility/public', $states ) )
            {
                $simpleApprovalState = eZContentObjectStateMugo::fetchByIdentifier( 'simple_approval/final' );
                if( $simpleApprovalState )
                {
                    $object->assignState( $simpleApprovalState );
                }
            }
            
            
            // Set "Approval" state to "Approved"
            $approvalState = eZContentObjectStateMugo::fetchByIdentifier( 'approval/approved' );
            if( $approvalState )
            {
                $object->assignState( $approvalState );
            }
            eZContentCacheManager::clearContentCacheIfNeeded( $object->attribute( 'id' ) );
        }

        return $this;
    }
}

?>