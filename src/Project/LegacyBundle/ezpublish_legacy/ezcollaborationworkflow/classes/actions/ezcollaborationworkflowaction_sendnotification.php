<?php 

/**
 * Description
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowAction_SendNotification extends ezcollaborationworkflowAction
{
	/**
	 * @var array
	 */
	protected $actionConfig;
	
	/**
	 * Build email and send it.
	 * 
	 * @param array $decisionObject
	 * @param string action_value
	 * @return boolean
	 */
	public function execute( $decisionObject, $action_value )
	{
		$this->actionConfig = $this->getActionConfig( $action_value );
		
		$mail = new ezcMailComposer();
		
		// Email From
		$mail->from = new ezcMailAddress(
				$this->approval_ini->variable( 'Mail', 'SenderEmail' ),
				$this->approval_ini->variable( 'Mail', 'SenderName' )
		);
			
		// Email Receivers
		$receivers = $this->getNotificationReceivers( $decisionObject, $this->actionConfig );
		
		if( !empty( $receivers ) )
		{
			foreach( $receivers as $receiver )
			{
				$mail->addTo( $receiver );
			}
		}
			
		$mail->subject  = $this->getEmailSubject( $decisionObject );
		$mail->htmlText = $this->getEmailBody( $decisionObject );
		$mail->build();
		
		$this->getEmailTransporter()->send( $mail );
		
		return $this;
	}
	
	
	/**
	 * Function to parse the email subject.
	 * 
	 * @param array $decisionObject
	 */
	protected function getEmailSubject( $decisionObject )
	{
		return $this->actionConfig[ 'Subject' ] ? $this->actionConfig[ 'Subject' ] : $this->approval_ini->variable( 'Mail', 'DefaultSubject' );
	}
	
	/**
	 * Parse the email body.
	 * 
	 * @return string
	 */
	protected function getEmailBody( $decisionObject )
	{
		$return = '';
		
		$template = $this->actionConfig[ 'Template' ] ? $this->actionConfig[ 'Template' ] : 'modules/approval/notifications/inform_author.tpl';
		
		$tpl = eZTemplate::factory();
		$tpl->setVariable( 'decisionObject', $decisionObject );

		$return = $tpl->fetch( 'design:' . $template );
		
		//TODO: check $return for FALSE? In case template wasn't found

		return $return;
	}
	
	/**
	 * Not fully implemented - idea is to dynamically get a list of users that can trigger another workflow action
	 *
	 * @param object $object
	 */
	private function get_approvers( $object )
	{
		$roles = eZRole::fetchList();
	
		if( !empty( $roles ) )
		{
			foreach( $roles as $role )
			{
				$policies = $role->attribute( 'policies' );
	
				if( !empty( $policies) )
				{
					foreach( $policies as $policy )
					{
						if( $policy->attribute( 'module_name' ) == 'approval' &&
						    $policy->attribute( 'function_name' ) == 'approve'
						)
						{
							print_r( $policy->attribute( 'limitations' ) );
						}
					}
				}
			}
		}
	
		die('d');
	}

	/**
	 * 
	 * @param array $decisionObject
	 * @param array $config
	 * @return Ambigous <multitype:ezcMailAddress, multitype:ezcMailAddress >
	 */
	private function getNotificationReceivers( $decisionObject, $config )
	{
		$users = array();
	
		//TODO: Use object var for $config.
		if( !empty( $config[ 'Receivers' ] ) )
		{
			foreach( $config[ 'Receivers' ] as $receiver )
			{
				if( substr( $receiver, 0, 5 ) == 'user_' )
				{
					$parts = explode( '_', $receiver );
						
					$id = (int) $parts[1];
						
					if( $id )
					{
						$users[ $id ] = eZContentObject::fetch( $id );
					}
				}
				elseif( substr( $receiver, 0, 6 ) == 'group_' )
				{
					$parts = explode( '_', $receiver );
						
					$id = (int) $parts[1];
						
					if( $id )
					{
						$group = eZContentObject::fetch( $id );
	
						$parameters = array(
								'parent_node_id' => $group->attribute( 'main_node_id' ),
								'class_filter_type' => 'include',
								'limitation' => array(),
								'class_filter_array' => array( 'user' )
						);
	
						$group_users = eZFunctionHandler::execute( 'content', 'tree', $parameters );
	
						if( !empty( $group_users ) )
						{
							foreach( $group_users as $user )
							{
								$users[ $user->attribute( 'contentobject_id' ) ] = $user->attribute( 'object' );
							}
						}
					}
				}
				elseif( $receiver == 'owner' )
				{
					$owner = $decisionObject[ 'object' ]->attribute( 'owner');
					$users[ $owner->attribute( 'id' ) ] = $owner;
				}
			}
		}
	
		return $this->userToEmailAddress( $users );
	}
	
	/**
	 * @param unknown_type $users
	 * @return multitype:ezcMailAddress 
	 */
	private function userToEmailAddress( $users )
	{
		$return = array();
	
		if( !empty( $users ) )
		{
			foreach( $users as $user )
			{
				$data_map = $user->attribute( 'data_map' );
	
				$name = $data_map[ 'first_name' ]->attribute( 'content' ) .
				' ' .
				$data_map[ 'last_name' ]->attribute( 'content' );
	
				$email = $data_map[ 'user_account' ]->attribute( 'content' )->attribute( 'email' );
	
				$return[] = new ezcMailAddress( $email, $name );
			}
		}
	
		return $return;
	}
	
	/**
	 * Reads the email transporter configuration and returns
	 * correct transporter instance.
	 * 
	 * @return ezcMailSmtpTransport|ezcMailMtaTransport
	 */
	private function getEmailTransporter()
	{
		$ini = eZINI::instance();
	
		switch( $ini->variable( 'MailSettings', 'Transport' ) )
		{
			case 'SMTP':
			{
				$server = $ini->variable( 'MailSettings', 'TransportServer' );
				$port   = $ini->variable( 'MailSettings', 'TransportPort' );
				$user   = $ini->variable( 'MailSettings', 'TransportUser' );
				$pass   = $ini->variable( 'MailSettings', 'TransportPassword' );

				return new ezcMailSmtpTransport( $server, $user, $pass, $port );
			}
			break;
					
			default:
			{
				return new ezcMailMtaTransport();
			}
		}
	}
	
}

?>