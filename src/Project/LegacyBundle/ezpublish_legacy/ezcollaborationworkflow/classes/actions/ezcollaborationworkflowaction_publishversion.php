<?php 

/**
 * Description
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowAction_PublishVersion extends ezcollaborationworkflowAction
{
	/**
	 * @param array $decisionObject
	 * @param string action_value
	 * @return boolean
	 */
	public function execute( $decisionObject, $action_value )
	{
		eZOperationHandler::execute(
				'content',
				'publish',
				array(
						'object_id' => $decisionObject[ 'object' ]->attribute( 'id' ),
						'version'   => $decisionObject[ 'version' ]->attribute( 'version' ),
				),
				null,
				false
		);

		return $this;
	}
}

?>