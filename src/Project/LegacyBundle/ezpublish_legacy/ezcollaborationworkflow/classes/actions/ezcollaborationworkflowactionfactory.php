<?php 

/**
 * Description
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowActionFactory
{
	/**
	 * @param string $name
	 * @param ezcollaborationworkflowDecisionTable $dt
	 * @return ezcollaborationworkflowAction
	 */
	public static function factory( $name, ezcollaborationworkflowDecisionTable $dt )
	{
		$class = 'ezcollaborationworkflowAction_' . $name;
		
		if( !class_exists( $class ) )
		{
			eZDebugSetting::writeWarning( 'extension-ezcollaborationworkflow',
					'Unkown action: ' . $name, __METHOD__ );

			$class = 'ezcollaborationworkflowAction';
		}

		eZDebugSetting::writeDebug( 'extension-ezcollaborationworkflow',
				'Loading action: ' . $name, __METHOD__ );
		
		$obj = new $class;
		$obj->setDecisionTable( $dt );
		
		return $obj;
	}
}

?>