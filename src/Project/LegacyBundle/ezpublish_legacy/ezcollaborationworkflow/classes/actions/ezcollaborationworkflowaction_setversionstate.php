<?php 

/**
 * Description
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowAction_SetVersionState extends ezcollaborationworkflowAction
{
	/**
	 * @param array $decisionObject
	 * @param string action_value
	 * @return boolean
	 */
	public function execute( $decisionObject, $action_value )
	{
		$valid_values = array(
				'draft'    => eZContentObjectVersion::STATUS_DRAFT,
				'rejected' => eZContentObjectVersion::STATUS_REJECTED,
				'pending'  => eZContentObjectVersion::STATUS_PENDING,
				'queued'   => eZContentObjectVersion::STATUS_QUEUED );
		
		if( isset( $valid_values[ $action_value ] ) )
		{
			$decisionObject[ 'version' ]->Status = $valid_values[ $action_value ];
			$decisionObject[ 'version' ]->store();
			
			$this->getDecisionTable()->setInterruptPublishingOperation( true );
		}
		
		return $this;
	}
}

?>