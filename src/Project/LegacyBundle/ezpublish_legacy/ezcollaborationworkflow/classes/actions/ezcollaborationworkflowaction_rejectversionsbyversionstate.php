<?php 

/**
 * Description
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowAction_RejectVersionsByVersionState extends ezcollaborationworkflowAction
{
	/**
	 * @param array $decisionObject
	 * @param string action_value
	 * @return boolean
	 */
	public function execute( $decisionObject, $action_value )
	{
		$valid_values = array( 'pending'  => eZContentObjectVersion::STATUS_PENDING );
			
		if( $valid_values[ $action_value ] )
		{
			$parameters = array( 'conditions' => array( 'status' => $valid_values[ $action_value ] ) );
			$versionsToRemove = $decisionObject[ 'object' ]->versions( true, $parameters );
				
			if( !empty( $versionsToRemove ) )
			{
				foreach( $versionsToRemove as $version )
				{
					$version->setAttribute( 'status', eZContentObjectVersion::STATUS_REJECTED );
					$version->store();
				}
			}
		}
		
		return $this;
	}
}

?>