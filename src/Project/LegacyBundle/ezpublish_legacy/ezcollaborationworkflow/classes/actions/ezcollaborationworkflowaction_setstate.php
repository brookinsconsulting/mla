<?php 

/**
 * Description
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowAction_SetState extends ezcollaborationworkflowAction
{
	/**
	 * @param array $decisionObject
	 * @param string action_value
	 * @return boolean
	 */
	public function execute( $decisionObject, $action_value )
	{
		if( (int) $action_value )
		{
			$state = eZContentObjectState::fetchById( $action_value );
		}
		else
		{
			$state = eZContentObjectStateMugo::fetchByIdentifier( $action_value );
		}
			
		if( $state )
		{
			$decisionObject[ 'object' ]->assignState( $state );
			eZContentCacheManager::clearContentCacheIfNeeded( $decisionObject[ 'object' ]->attribute( 'id' ) );
		}
		
		return $this;
	}
}

?>