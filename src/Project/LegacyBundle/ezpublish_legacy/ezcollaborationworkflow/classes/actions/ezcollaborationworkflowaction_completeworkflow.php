<?php 

/**
 * When a pending version is approved, complete the workflow if there are no other pending or queued versions
 * Set the states simple_approval/final and approval/approved
 */

class ezcollaborationworkflowAction_CompleteWorkflow extends ezcollaborationworkflowAction
{
    /**
     * @param array $decisionObject
     * @param string action_value
     * @return boolean
     */
    public function execute( $decisionObject, $action_value )
    {
        $object = $decisionObject['object'];
        $otherWorkflowVersions = ezcollaborationworkflowFunctionCollection::get_approve_versions( $object );

        if( count( $otherWorkflowVersions ) == 0 )
        {
            // Set "Approval" state to "Approved"
            $approvalState = eZContentObjectStateMugo::fetchByIdentifier( 'approval/approved' );
            if( $approvalState )
            {
                $object->assignState( $approvalState );
            }
            // Set "Simple Approval" state to "Final"
            $simpleApprovalState = eZContentObjectStateMugo::fetchByIdentifier( 'simple_approval/final' );
            if( $simpleApprovalState )
            {
                $object->assignState( $simpleApprovalState );
            }
            eZContentCacheManager::clearContentCacheIfNeeded( $object->attribute( 'id' ) );
        }

        return $this;
    }
}

?>