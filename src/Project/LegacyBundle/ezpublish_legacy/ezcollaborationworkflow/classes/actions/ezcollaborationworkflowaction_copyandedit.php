<?php 

/**
 * Creates a draft as a copy of a workflow version and redirects to edit mode for the new version
 * Needs to be the last action in a ruleset since it redirects and needs to come after the action:
 * SetVersionState=rejected
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowAction_CopyAndEdit extends ezcollaborationworkflowAction
{
	/**
	 * @param array $decisionObject
	 * @param string action_value
	 * @return boolean
	 */
	public function execute( $decisionObject, $action_value )
	{
		// createNewVersionIn( $languageCode, $copyFromLanguageCode = false, $copyFromVersion = false, $versionCheck = true, $status = eZContentObjectVersion::STATUS_DRAFT )
		//$decisionObject['object']->copyRevertTo( $decisionObject['version']->attribute( 'version' ), $decisionObject['version']->attribute( 'initial_language_id' ) );

		$object = $decisionObject['object'];
		$sourceVersion = $decisionObject['version'];

		$sourceVersionID = $sourceVersion->attribute( 'version' );
		$sourceVersionLanguage = $sourceVersion->attribute( 'initial_language' )->attribute( 'locale' );
		$sourceVersionLanguageID = $sourceVersion->attribute( 'initial_language_id' );
		$contentObjectID = $object->attribute( 'id' );

        $db = eZDB::instance();
        $db->begin();
        $newVersionID = $object->copyRevertTo( $sourceVersionID, $sourceVersionLanguage );

        // Handle the case where the object translation hasn't been published yet
        // If it hasn't been published yet, it doesn't appear in the object's list of available languages and thus does not get properly copied in the standard copy function
        // See eZContentObject::copyVersion(), which causes this problem
        if( !in_array( $sourceVersionLanguage, $object->availableLanguages() ) )
        {
	        $sourceDataMap = $sourceVersion->attribute( 'data_map' );
	        $newVersion = eZContentObjectVersion::fetchVersion( $newVersionID, $contentObjectID );
	        $newVersionDataMap = $newVersion->attribute( 'data_map' );

	        foreach ( $newVersionDataMap as $attributeIdentifier => $newVersionAttribute )
	        {
	        	if( isset( $sourceDataMap[$attributeIdentifier] ) )
	        	{
		            $newVersionAttribute->fromString( $sourceDataMap[$attributeIdentifier]->toString() );
		            $newVersionAttribute->store();
	            }
	        }
        }

        $db->commit();

    	// Redirect to the new version
        $http = eZHTTPTool::instance();
        $editURL = "content/edit/$contentObjectID/$newVersionID/$sourceVersionLanguage";
        eZURI::transformURI( $editURL );
        $http->redirect( $editURL );
		
		return $this;
	}
}
