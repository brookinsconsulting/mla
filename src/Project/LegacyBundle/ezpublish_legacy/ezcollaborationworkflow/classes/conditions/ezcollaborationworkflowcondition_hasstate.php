<?php 

/**
 * Description
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowCondition_HasState extends ezcollaborationworkflowCondition
{
	/**
	 * @param array $decisionObject
	 * @param array $condition
	 * @return boolean
	 */
	public function checkCondition( $decisionObject, $condition_value )
	{
		//TODO: check if we could use valueMatch
		if( (int) $condition_value )
		{
			$states = $decisionObject[ 'object' ]->attribute( 'state_id_array' );
		}
		else
		{
			$states = $decisionObject[ 'object' ]->attribute( 'state_identifier_array' );
		}
		
		return in_array( $condition_value, $states );
	}
}

?>