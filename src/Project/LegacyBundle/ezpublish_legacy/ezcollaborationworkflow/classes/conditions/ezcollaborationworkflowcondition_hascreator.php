<?php 

/**
 * Checks the version creator ID against a configured value.
 * Can handle multiple IDs in a comma separated list.
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowCondition_HasCreator extends ezcollaborationworkflowCondition
{
	/**
	 * @param array $decisionObject
	 * @param array $condition
	 * @return boolean
	 */
	public function checkCondition( $decisionObject, $condition_value )
	{
		$creator_id = $decisionObject[ 'version' ]->attribute( 'creator_id' );
		return $this->valueMatch( $condition_value, array( $creator_id ) );
	}
}

?>