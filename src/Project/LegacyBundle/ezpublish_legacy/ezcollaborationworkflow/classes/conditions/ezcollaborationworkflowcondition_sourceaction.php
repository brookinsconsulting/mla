<?php 

/**
 * Description
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowCondition_SourceAction extends ezcollaborationworkflowCondition
{
	/**
	 * @param array $decisionObject
	 * @param array $condition
	 * @return boolean
	 */
	public function checkCondition( $decisionObject, $condition_value )
	{
		return $decisionObject[ 'action' ] == $condition_value;
	}
}

?>