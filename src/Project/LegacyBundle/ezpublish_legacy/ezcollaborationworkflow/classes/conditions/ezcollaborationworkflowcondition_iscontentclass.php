<?php 

/**
 * Description
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowCondition_IsContentClass extends ezcollaborationworkflowCondition
{
	/**
	 * @param array $decisionObject
	 * @param array $condition
	 * @return boolean
	 */
	public function checkCondition( $decisionObject, $condition_value )
	{
		return $this->valueMatch(
				$condition_value,
				array(
						$decisionObject[ 'object' ]->attribute( 'contentclass_id' ),
						$decisionObject[ 'object' ]->attribute( 'class_identifier' )
					) );
	}
}

?>