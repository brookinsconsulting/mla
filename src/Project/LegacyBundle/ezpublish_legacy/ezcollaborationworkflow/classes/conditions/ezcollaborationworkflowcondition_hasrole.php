<?php 

/**
 * Description
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowCondition_HasRole extends ezcollaborationworkflowCondition
{
	/**
	 * @param array $decisionObject
	 * @param array $condition
	 * @return boolean
	 */
	public function checkCondition( $decisionObject, $condition_value )
	{
		$creator = eZUser::currentUser();
		$roles = $creator->roleIDList();
		eZDebug::writeDebug($roles);
		eZDebug::writeDebug($condition_value);
		return $this->valueMatch( $condition_value, $roles );
	}
}

?>