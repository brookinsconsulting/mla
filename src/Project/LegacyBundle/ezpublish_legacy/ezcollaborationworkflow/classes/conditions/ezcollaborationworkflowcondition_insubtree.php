<?php 

/**
 * Description
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowCondition_InSubTree extends ezcollaborationworkflowCondition
{
	/**
	 * @param array $decisionObject
	 * @param array $condition
	 * @return boolean
	 */
	public function checkCondition( $decisionObject, $condition_value )
	{
		return $this->valueMatch( $condition_value, $decisionObject[ 'path' ] );
	}
}

?>