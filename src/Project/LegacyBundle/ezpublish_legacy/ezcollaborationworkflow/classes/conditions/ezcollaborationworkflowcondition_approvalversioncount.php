<?php 

/**
 * Description
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowCondition_ApprovalVersionCount extends ezcollaborationworkflowCondition
{
	/**
	 * @param array $decisionObject
	 * @param array $condition
	 * @return boolean
	 */
	public function checkCondition( $decisionObject, $condition_value )
	{
		$remainingVersions = ezcollaborationworkflowFunctionCollection::get_approve_versions( $decisionObject[ 'object' ] );
		return count( $remainingVersions ) == $condition_value;
	}
}

?>