<?php 

/**
 * Checks if the given version state matches the given value in $condition_value
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowCondition_HasVersionState extends ezcollaborationworkflowCondition
{
	/**
	 * @param array $decisionObject
	 * @param array $condition_value
	 * @return boolean
	 */
	public function checkCondition( $decisionObject, $condition_value )
	{
		$valid_values = array(
				'draft'          => eZContentObjectVersion::STATUS_DRAFT,
				'published'      => eZContentObjectVersion::STATUS_PUBLISHED,
				'pending'        => eZContentObjectVersion::STATUS_PENDING,
				'archived'       => eZContentObjectVersion::STATUS_ARCHIVED,
				'rejected'       => eZContentObjectVersion::STATUS_REJECTED,
				'internal_draft' => eZContentObjectVersion::STATUS_INTERNAL_DRAFT,
				'repeat'         => eZContentObjectVersion::STATUS_REPEAT,
				'queued'         => eZContentObjectVersion::STATUS_QUEUED,
		);
	
		if( isset( $valid_values[ $condition_value ] ) )
		{
			return $decisionObject[ 'version' ]->Status == $valid_values[ $condition_value ];
		}
		
		return false;
	}
}
