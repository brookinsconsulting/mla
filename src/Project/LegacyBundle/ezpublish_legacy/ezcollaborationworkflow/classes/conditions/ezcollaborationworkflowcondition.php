<?php 

/**
 * Description
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowCondition
{
	
	/**
	 * @var ezcollaborationworkflowDecisionTable
	 */
	protected $decisionTable;
	
	/**
	 * @param array $decisionObject
	 * @param array $condition
	 * @return boolean
	 */
	public function checkCondition( $decisionObject, $condition_value )
	{
		// default is true to ignore unkown conditions
		return true;
	}
	
	/**
	 * Finds a match against a list of comma separated values or
	 * a single id value or a single identifier.
	 *
	 * @param string $value
	 * @param int $integer
	 * @param string $string
	 */
	protected function valueMatch( $configStr, $matchValues )
	{
		// assuming no ids with value of zero
		// an empty configStr will ignore that condition
		if( !$configStr )
		{
			return true;
		}
	
		// normalize to array
		// assuming no identifier value containing a comma
		if( strpos( $configStr, ',' ) !== false )
		{
			$values = explode( ',', $configStr );
		}
		else
		{
			$values = array( $configStr );
		}
	
		foreach( $values as $value )
		{
			if( in_array( $value, $matchValues ) )
			{
				return true;
			}
		}
	
		return false;
	}
	
	/**
	 * @param ezcollaborationworkflowDecisionTable $dt
	 * @return ezcollaborationworkflowCondition
	 */
	public function setDecisionTable( ezcollaborationworkflowDecisionTable $dt )
	{
		$this->decisionTable = $dt;
		return $this;
	}
	
	/**
	 * @return ezcollaborationworkflowDecisionTable
	 */
	public function getDecisionTable()
	{
		return $this->decisionTable;
	}
	
}

?>