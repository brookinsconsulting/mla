<?php 

/**
 * Checks the version creator groups against a configured value.
 * Can handle multiple IDs in a comma separated list. Use the eZ publish object IDs here.
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowCondition_HasCreatorGroup extends ezcollaborationworkflowCondition
{
	/**
	 * @param array $decisionObject
	 * @param array $condition
	 * @return boolean
	 */
	public function checkCondition( $decisionObject, $condition_value )
	{
		$creator_id = $decisionObject[ 'version' ]->attribute( 'creator_id' );
        $groups = eZUser::fetch( $creator_id )->attribute( 'groups' );
        
		return $this->valueMatch( $condition_value, $groups );
	}
}

?>