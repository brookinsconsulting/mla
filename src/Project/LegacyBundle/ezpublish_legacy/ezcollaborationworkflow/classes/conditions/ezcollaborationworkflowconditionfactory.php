<?php 

/**
 * Description
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowConditionFactory
{
	/**
	 * @param string $name
	 * @param ezcollaborationworkflowDecisionTable $adt
	 * @return ezcollaborationworkflowCondition
	 */
	public static function factory( $name, ezcollaborationworkflowDecisionTable $dt )
	{
		$class = 'ezcollaborationworkflowCondition_' . $name;
		
		if( !class_exists( $class ) )
		{
			eZDebugSetting::writeWarning( 'extension-ezcollaborationworkflow',
					'Unkown condition: ' . $name, __METHOD__ );

			$class = 'ezcollaborationworkflowCondition';
		}

		eZDebugSetting::writeDebug( 'extension-ezcollaborationworkflow',
				'Loading condition: ' . $name, __METHOD__ );
		
		$obj = new $class;
		$obj->setDecisionTable( $dt );
		
		return $obj;
	}
}

?>