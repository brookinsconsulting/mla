<?php 

/**
 * Checks if there is an attribue identifier given with $condition_value. If it exists and has content
 * it compares the attribute value with the current time.
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 * @package extension
 */

class ezcollaborationworkflowCondition_ReachedTimeByAttribute extends ezcollaborationworkflowCondition
{
	/**
	 * @param array $decisionObject
	 * @param array $condition_value
	 * @return boolean
	 */
	public function checkCondition( $decisionObject, $condition_value )
	{
		$data_map = $decisionObject[ 'version' ]->attribute( 'data_map' );

		if( isset( $data_map[ $condition_value ] ) && $data_map[ $condition_value ]->attribute( 'has_content' ) )
		{
			$publish_time = $data_map[ $condition_value ]->attribute( 'data_int' );

			return $publish_time < time();
		}

		return $this;
	}
}
