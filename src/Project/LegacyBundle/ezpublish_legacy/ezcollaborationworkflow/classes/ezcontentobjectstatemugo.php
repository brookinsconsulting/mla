<?php
/**
* Contains some fixes for the eZ API class eZContentObjectState
*
* @copyright //autogen//
* @license //autogen//
* @version //autogen//
*/
class eZContentObjectStateMugo extends eZContentObjectState
{
	static private $object_state_cache;
	
	/*
	* The eZ API function is strange - so let's override it.
	* $identifier takes the format <group_identifier>/<state_identifier>
	* $groupID is unused here and kept to be compatible with eZContentObjectState
	*/
	public static function fetchByIdentifier( $identifier, $groupID = null )
	{
		$return = null;
	
		$db = eZDB::instance();
	
		$parts = explode( '/', $identifier );
	
		if( count( $parts ) == 2 )
		{
			$groupIdent = $db->escapeString( $parts[ 0 ] );
			$stateIdent = $db->escapeString( $parts[ 1 ] );
			$states = self::fetchByConditions2( array( "ezcobj_state.identifier='$stateIdent'", "ezcobj_state_group.identifier='$groupIdent'" ), 1, 0 );
			$return = count( $states ) > 0 ? $states[0] : false;
		}
		return $return;
	}
	
	// TODO: Check if we can user fetchByConditions2
	public static function getStateLabel( $id )
	{
		if( ! self::$object_state_cache )
		{
			$db = eZDB::instance();
				
			$conditions = array(
			            'ezcobj_state.group_id=ezcobj_state_group.id',
			            'ezcobj_state_language.contentobject_state_id=ezcobj_state.id',
			            eZContentLanguage::languagesSQLFilter( 'ezcobj_state' ),
			            eZContentLanguage::sqlFilter( 'ezcobj_state_language', 'ezcobj_state' )
			);
				
			$conditionsSQL = implode( ' AND ', $conditions );
				
			$sql = "SELECT ezcobj_state.*, ezcobj_state_language.* " .
							               "FROM ezcobj_state, ezcobj_state_group, ezcobj_state_language " .
							               "WHERE $conditionsSQL " .
							               "ORDER BY ezcobj_state.priority";
				
			$rows = $db->arrayQuery( $sql );
		
			foreach ( $rows as $row )
			{
				self::$object_state_cache[ $row[ 'contentobject_state_id' ] ] = $row[ 'name' ];
				self::$object_state_cache[ $row[ 'identifier'] ]              = $row[ 'name' ];
			}
		}
	
		return self::$object_state_cache[ $id ];
	}
	
	/*
	 * ezp API method is private - so here we go copy the code
	*/
	private static function fetchByConditions2( $conditions, $limit, $offset )
	{
		$db = eZDB::instance();
	
		$defaultConditions = array(
		            'ezcobj_state.group_id=ezcobj_state_group.id',
		            'ezcobj_state_language.contentobject_state_id=ezcobj_state.id',
		eZContentLanguage::languagesSQLFilter( 'ezcobj_state' ),
		eZContentLanguage::sqlFilter( 'ezcobj_state_language', 'ezcobj_state' )
		);
	
		$conditions = array_merge( $conditions, $defaultConditions );
	
		$conditionsSQL = implode( ' AND ', $conditions );
	
		$sql = "SELECT ezcobj_state.*, ezcobj_state_language.* " .
		               "FROM ezcobj_state, ezcobj_state_group, ezcobj_state_language " .
		               "WHERE $conditionsSQL " .
		               "ORDER BY ezcobj_state.priority";
	
		$rows = $db->arrayQuery( $sql, array( 'limit' => $limit, 'offset' => $offset ) );
	
		$states = array();
		foreach ( $rows as $row )
		{
			$state = new eZContentObjectStateMugo( $row );
			$stateLanguage = new eZContentObjectStateLanguage( $row );
			$state->LanguageObject = $stateLanguage;
				
			$states[] = $state;
		}
	
		return $states;
	}

	public $LanguageObject;
}

?>