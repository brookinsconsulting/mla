<?php
/**
* Class to execute the decision tables against a decision object
*
* @copyright //autogen//
* @license //autogen//
* @version //autogen//
*/
class ezcollaborationworkflowDecisionTable
{
	private $visibilityStateId;
	private $approvalStateId;
	
	private $workflowActions;
	private $rules_ini;
	private $approval_ini;
	
	
	/**
	 * Set at run-time
	 * @var string
	 */
	protected $matchingRule;
	
	/**
	 * Controls if the publishing operation should get interrupted on
	 * content updates. The action "PublishVersion" sets this to false.
	 * 
	 * @var boolean
	 */
	protected $interruptPublishingOperation = false;
	
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->rules_ini    = eZINI::instance( 'ezcollaborationworkflow_rules.ini' );
		$this->approval_ini = eZINI::instance( 'ezcollaborationworkflow.ini' );

		$this->visibilityStateId = $this->approval_ini->variable( 'ObjectStateIds', 'VisibilityStateGroupId' );
		$this->approvalStateId   = $this->approval_ini->variable( 'ObjectStateIds', 'ApprovalStateGroupId' );
		$this->workflowActions   = $this->approval_ini->variable( 'Generic', 'WorkflowActions' );
	}
	
	
	/**
	 * Executes an action
	 * 
	 * @param string $sourceAction
	 * @param integer $id version_id
	 * @param array $extraInfo Any additional info you like to add to the decision object
	 * @return boolean
	 */
	public function execute( $sourceAction, $id, $extraInfo = null )
	{
		$decisionObject = array( 'action' => $sourceAction,
		                         'extra'  => $extraInfo );
		
		$success = $this->buildDecisionObject( $id, $decisionObject );
		
		if( $success )
		{
			$this->matchingRule = $this->findMatchingRule( $decisionObject );
			
			if( $this->matchingRule )
			{
				$this->executeRuleActions( $this->matchingRule, $decisionObject );
			}
		}
		
		return $this;
	}
	
	/**
	 * TODO: won't work in future versions of PHP?: &$decisionObject
	 * 
	 * @param integer $version_id
	 * @param array $decisionObject
	 * @return boolean
	 */
	public function buildDecisionObject( $version_id, &$decisionObject )
	{
		$return = false;
		
		$version = eZContentObjectVersion::fetch( $version_id );
				
		if( $version instanceof eZContentObjectVersion )
		{
			$return = true;
			
			// add version object 
			$decisionObject[ 'version' ] = $version;
			
			// add object
			$decisionObject[ 'object' ] = $version->attribute( 'contentobject' );
			
			// add pending versions
			$decisionObject[ 'pending_versions' ] = array();
			
			$pending_versions = ezcollaborationworkflowFetchFunctionCollection::get_approve_versions( $decisionObject[ 'object' ] );

			$pending_versions = $pending_versions[ 'result' ];
			
			if( count( $pending_versions ) > 1 )
			{
				foreach( $pending_versions as $index => $pending_version )
				{
					if( $pending_version->attribute( 'id' ) == $version->attribute( 'id' ) )
					{
						unset( $pending_versions[ $index ] );
					}
				}
				
				$decisionObject[ 'pending_versions' ] = $pending_versions;
			}
			
			// add path
			$node = $decisionObject[ 'object' ]->attribute( 'main_node' );
			if( $node )
			{
				$decisionObject[ 'path' ] = $node->attribute( 'path_array' );
			}
			// new content object
			else
			{
				$assignments = $version->attribute( 'node_assignments' );
				$assignments = $assignments[ 0 ];
				
				$parent_node_id = $assignments->attribute( 'parent_node' );
				$node = eZContentObjectTreeNode::fetch( $parent_node_id );
				$decisionObject[ 'path' ] = $node->attribute( 'path_array' );
			}
			
			return $return;
		}
				
		eZDebugSetting::writeDebug( 'extension-ezcollaborationworkflow', 'DecisionObject builded: ' . print_r( $decisionObject, true ) , __METHOD__ );
		
		return $return;
	}

	/**
	 * Takes the decision object and checks it against various configured conditions
	 */
	public function findMatchingRule( $decisionObject )
	{
		$return = null;
		
		eZDebugSetting::writeNotice( 'extension-ezcollaborationworkflow',
		                             'Check for matching rule. Object: ' . $decisionObject[ 'object' ]->attribute( 'id' ) . ' Action: ' . $decisionObject[ 'action' ],
		                              __METHOD__ );
		
		$rule_names = $this->rules_ini->variable( 'Generic', 'Rules' );

		if( !empty( $rule_names ) )
		{
			foreach( $rule_names as $rule_name )
			{
				$matches = true;
				
				$conditions = $this->getConditions( $rule_name );
				
				if( !empty( $conditions ) )
				{
					foreach( $conditions as $condition_name => $condition_values )
					{
						// normalize all $condition_values to an array
						if( ! is_array( $condition_values ) )
						{
							$condition_values = array( $condition_values );
						}
						
						if( ! empty( $condition_values ) )
						{
							foreach( $condition_values as $condition_value )
							{
								$conditionChecher = ezcollaborationworkflowConditionFactory::factory( $condition_name, $this );
								
								if( ! $conditionChecher->checkCondition( $decisionObject, $condition_value ) )
								{
									$matches = false;
									break 2;
								}
							}
						}
					}
				}
				else
				{
					// we ignore rules that have no conditions
					$matches = false;
				}
				
				if( $matches )
				{
					$return = $rule_name;
					break;
				}
			}
		}
		if( $return )
		{
			eZDebugSetting::writeNotice( 'extension-ezcollaborationworkflow', 'Found matching rule: ' . $return . '.', __METHOD__ );
		}
		else
		{
			eZDebugSetting::writeNotice( 'extension-ezcollaborationworkflow', 'Could not find a matching rule.', __METHOD__ );
		}
		
		return $return;
	}

	/**
	 * 
	 */
	public function executeRuleActions( $rule_name, $decisionObject )
	{
		$return = false;
		
		if( $rule_name )
		{
			$actions = $this->getActions( $rule_name );
			
			if( !empty( $actions ) )
			{
				foreach( $actions as $action_name => $action_values )
				{
					// normalize all settings to arrays
					if( ! is_array( $action_values ) )
					{
						$action_values = array( $action_values );
					}
					
					if( ! empty( $action_values ) )
					{
						foreach( $action_values as $action_value )
						{
							$actionHandler = ezcollaborationworkflowActionFactory::factory( $action_name, $this );
							$actionHandler->execute( $decisionObject, $action_value );
						}
					}
				}
			}
		}
		return $return;
	}	
	
	public function getWorkflowActions()
	{
		return $this->workflowActions;
	}
	
	public function getWorkflowActionsAsLimitation()
	{
		$return = array( 'name' => 'Workflow_Actions',
		                 'single_select' => false
		);
		
		foreach( $this->workflowActions as $id => $name )
		{
			$return[ 'values' ][] = array( 'Name'  => $name,
			                               'value' => $id
			                             );
		}
		
		return $return;
	}
	
	/*
	 * Get the rule names from the ini files
	 */
	public function getRuleNames( $group = null )
	{
		$return = array();
		
		if( $group )
		{
			$decision_tables = $this->approval_ini->group( 'Workflows' );
			
			if( isset( $decision_tables[ $group ] ) )
			{
				$return = $decision_tables[ $group ];
			}
		}
		else
		{
			$return = $this->rules_ini->variable( 'Generic', 'Rules' );
		}
		
		return $return;
	}
	
	/**
	 * Read the rule condition block
	 * 
	 * @param string $ruleName
	 * @return Ambigous <NULL, unknown>
	 */
	public function getConditions( $ruleName )
	{
		$return = null;
		
		$condition_block = $ruleName . '_conditions';
		
		if( $this->rules_ini->hasGroup( $condition_block ) )
		{
			$return = $this->rules_ini->group( $condition_block );
		}
		else
		{
			eZDebugSetting::writeNotice(
					'extension-ezcollaborationworkflow',
					'Could not find any conditions for this rule: "' . $ruleName . '" - ignoring rule',
					__METHOD__
			);
		}
		
		return $return;
	}
	
	/**
	 * @param string $ruleName
	 * @return Ambigous <NULL, unknown>
	 */
	public function getActions( $ruleName )
	{
		$action_block = $ruleName . '_actions';
		return $this->rules_ini->group( $action_block );
	}
	
	/**
	 * @param string $name
	 * @param unknown_type $value
	 * @return multitype:string 
	 */
	public function getLabels( $name, $value )
	{
		$return = array();
		
		if( is_array( $value ) )
		{
			foreach( $value as $singleValue )
			{
				$return = array_merge( $return, $this->getLabels( $name, $singleValue ) );
			}
		}
		else
		{
			$return[] = $name . ': ' . $value;
		}

		return $return;
	}
	
	/**
	 * Matching rule is set after findMatchingRule() execution
	 * 
	 * @return string
	 */
	public function getMatchingRule()
	{
		return $this->matchingRule;
	}
	
	/**
	 * @return boolean
	 */
	public function getInterruptPublishingOperation()
	{
		return $this->interruptPublishingOperation;
	}
	
	/**
	 * @param boolean $value
	 * @return ezcollaborationworkflowDecisionTable
	 */
	public function setInterruptPublishingOperation( $value )
	{
		$this->interruptPublishingOperation = $value;
		return $this;
	}
	
}

?>