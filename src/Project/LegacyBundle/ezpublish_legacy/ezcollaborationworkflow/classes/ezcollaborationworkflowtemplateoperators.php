<?php
/**
* Collection of eZ Collaboration Workflow template operators
*
* @copyright //autogen//
* @license //autogen//
* @version //autogen//
*/

class ezcollaborationworkflowTemplateOperators
{
    function __construct()
    {
    }

    function operatorList()
    {
        return array(
                'get_workflow_versions'
        );
    }

    function namedParameterPerOperator()
    {
        return true;
    }

    function namedParameterList()
    {
        return array( 
                      'get_workflow_versions' => array(
                                                        'object' => array( 'type' => 'object', 'required' => true )
                                                      )
                    );
    }

    function modify( &$tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters )
    {
        switch ( $operatorName )
        {
            case 'get_workflow_versions':
            {
                $operatorValue = ezcollaborationworkflowFunctionCollection::get_approve_versions( $namedParameters['object'] );
            }
            break;
        }
    }
}

?>
