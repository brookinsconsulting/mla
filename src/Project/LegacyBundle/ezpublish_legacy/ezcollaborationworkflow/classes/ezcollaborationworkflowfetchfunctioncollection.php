<?php
/**
* Template Function Collection
*
* @copyright //autogen//
* @license //autogen//
* @version //autogen//
*/ 
class ezcollaborationworkflowFetchFunctionCollection
{	
	/**
	 * @param eZContentObject $object
	 * @param string $action
	 * @return multitype:boolean 
	 */
	static function can_approve( $object, $version = null, $action = null )
	{
		$return = false;
		
		$user = eZUser::currentUser();
		
		$accessResult = $user->hasAccessTo( 'approval' , 'process' );

		switch( $accessResult[ 'accessWord' ] )
		{
			case 'yes':
			{
				$return = true;
			}
			break;
			
			case 'no':
			{
				// still false
			}
			break;
			
			case 'limited':
			{
				$return = self::matches_limitation( $accessResult[ 'policies' ], $object, $version, $action );
			}
			break;
			
			default:
		}

		return array( 'result' => $return );
	}
	
	static function can_do_action( $object, $version = null, $action = null ) {
	    
	   $adt = new ezcollaborationworkflowDecisionTable();
	    
       $decisionObject = array( 'action' => $action,
		                         'extra'  => null );
		                         
	   $success = $adt->buildDecisionObject( $object->currentVersion()->attribute('id'), $decisionObject );
	   
	   $match = $adt->findMatchingRule( $decisionObject );
	
       return array( 'result' => ($match === null) ? false : true );
	}

	//TODO: don't think we need the language param
	static function get_object_state_label( $id, $language_id = null )
	{		
		return array( 'result' => eZContentObjectStateMugo::getStateLabel( $id ) );
	}

	/**
	 *
	 */
	static function get_approve_versions( $object )
	{
		return array( 'result' => ezcollaborationworkflowFunctionCollection::get_approve_versions( $object ) );
	}
	
	/**
	 *
	 */
	static public function get_latest_comment( $object_id = null )
	{
		return array( 'result' => ezcollaborationworkflowMessage::fetchLatestCommentForObjectId( $object_id ) );
	}

	/**
	 * 
	 * @param unknown_type $id
	 * @return multitype:Ambigous <eZContentObjectStateGroup, boolean> 
	 */
	static public function state_group( $id )
	{
		return array( 'result' => eZContentObjectStateGroup::fetchById( $id ) );
	}
	
	/*
	 * 
	 */
	static public function object_relevant_states( $object = null )
	{
		$return = array();
		
		$ignoreGroupList = self::getGroupIgnoreList( $object );
		
		$groups = eZContentObjectStateGroup::fetchByOffset( 1000, 0 );
		
		foreach( $groups as $group )
		{
			$group_id = $group->attribute( 'id' );

			if( !in_array( $group->attribute( 'identifier' ), $ignoreGroupList ) )
			{
				$states = eZContentObjectState::fetchByGroup( $group_id );
				
				foreach( $states as $index => $state )
				{
					// clean out irrelevant states
					if( $state->attribute( 'identifier' ) == 'irrelevant' )
					{
						unset( $states[ $index ] );
					}
				}
				
				$return[ $group_id ] = array( 'group' => $group,
				                              'states' => $states
				                            );
			}
		}
		
		return array( 'result' => $return );
	}
	
	/**
	 * Here an example for $policies
	 * 
	 *
Array
(
    [p_516] => Array
        (
            [Workflow_Actions] => Array
                (
                    [0] => simple_approve
                    [1] => simple_reject
                )

            [Language] => Array
                (
                    [0] => eng-GB
                    [1] => eng-US
                )

            [StateGroup_content] => Array
                (
                    [0] => 7
                    [1] => 8
                )

        )

)


	 * @param array $policies
	 * @param eZContentObject $object
	 * @param string $action
	 * @return boolean
	 */
	private static function matches_limitation( $policies, $object, $version = null, $action = null )
	{
		$return = false;
	
		if( !empty( $policies ) )
		{
			foreach( $policies as $policy )
			{
				if( !empty( $policy ) )
				{
					// Needs to match all limitations in a policy
					$matchCountRequired = count( $policy );
					$matchCount = 0;
	
					foreach( $policy as $limitation_label => $limitation )
					{
						// Limitation on object states
						if( substr( $limitation_label, 0, 11 ) == 'StateGroup_' )
						{
							if( !empty( $limitation ) )
							{
								$obj_states = $object->attribute( 'state_id_array' );

								foreach( $limitation as $id )
								{
									if( in_array( $id, $obj_states ) )
									{
										$matchCount++;
										break;
									}
								}
							}
						}
	
						// Limitation on workflow action
						if( $limitation_label == 'Workflow_Actions' )
						{
							if( $action )
							{
								if( in_array( $action, $limitation ) )
								{
									$matchCount++;
								}
							}
							else
							{
								//TODO: maybe thats a bit to much
								$matchCount++;
							}
						}
						
						// Limitation on Language
						if( $limitation_label == 'Language' )
						{
							if( !empty( $limitation ) )
							{
								if( $version instanceof eZContentObjectVersion )
								{
									// Assuming version only has one language
									$language     = $version->attribute( 'language_list' );
									$language     = $language[ 0 ];
									$version_code = $language->attribute( 'language_code' );
									
									foreach( $limitation as $lang_code )
									{
										if( $version_code == $lang_code )
										{
											$matchCount++;
											break;
										}
									}
								}
								else
								{
									//TODO: We probably want to check all versions/languages and check if there is a match
									$matchCount++;
									break;
								}
							}
						}
						
						// Limitation on Class
						if( $limitation_label == 'Class' )
						{
							if( !empty( $limitation ) )
							{
								$class_id = $object->attribute( 'contentclass_id' );
									
								foreach( $limitation as $value )
								{
									if( $class_id == $value )
									{
										$matchCount++;
										break;
									}
								}
							}
						}

						// Limitation on Version State
						if( $limitation_label == 'Status' )
						{
							if( !empty( $limitation ) )
							{
								if( $version instanceof eZContentObjectVersion )
								{
									foreach( $limitation as $version_state )
									{
										if(  $version->attribute( 'status' ) == $version_state )
										{
											$matchCount++;
											break;
										}
									}
								}
								else
								{
									//TODO: We probably want to check all versions/languages and check if there is a match
									$matchCount++;
									break;
								}
							}
						}
						
						// Limitation on subtree
						if( $limitation_label == 'User_Subtree' )
						{
							if( !empty( $limitation ) )
							{
								$pathString = $object->attribute( 'main_node' )->attribute( 'path_string' );

								foreach( $limitation as $value )
								{
									if( strpos( $pathString, $value ) !== false )
									{
										$matchCount++;
										break;
									}
								}
							}
						}
						
						
					}
	
					// Check if all limitations match
					if( $matchCount == $matchCountRequired )
					{
						// Only need to match one policy
						$return = true;
						break;
					}
				}
			}
		}
	
		return $return;
	}
	
	protected static function getGroupIgnoreList( $object )
	{
		// Todo: allow to configure it in ini
		$ini = eZINI::instance( 'ezcollaborationworkflow.ini' );
		$irrelevantIdentifier = $ini->variable( 'ObjectStateIds', 'IrrelevantIdentifier' );
		$ignoreList           = $ini->variable( 'ObjectStateIds', 'ListIgnoreGroups' );

		
		if( $object instanceof eZContentObject )
		{
			// Ignore groups that have current state "irrelevant"
			$objectStateIdentifiers = $object->attribute( 'state_identifier_array' );
			foreach( $objectStateIdentifiers as $identifier )
			{
				$parts = explode( '/', $identifier );
		
				if( $parts[ 1 ] == $irrelevantIdentifier )
				{
					$ignoreList[] = $parts[ 0 ];
				}
			}
		}
			
		return $ignoreList;
	}
	
}

?>