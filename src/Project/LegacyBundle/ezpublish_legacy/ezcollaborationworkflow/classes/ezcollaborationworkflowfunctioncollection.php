<?php
/**
* Template Function Collection
*
* @copyright //autogen//
* @license //autogen//
* @version //autogen//
*/ 
class ezcollaborationworkflowFunctionCollection
{	

	
	/**
	 * Gets all relevant versions for the approval process.
	 *
	 * @param eZContentObject $object
	 * @return multitype:eZContentObjectVersion
	 */
	static function get_approve_versions( eZContentObject $object )
	{
		$return = array();
		
		if( $object instanceof eZContentObject )
		{
			$object_states = $object->attribute( 'state_id_array' );
	
			$ini = eZINI::instance( 'ezcollaborationworkflow.ini' );
			$nonPublicId = $ini->variable( 'ObjectStateIds', 'NonPublicId' );
			
			// new content object
			if( in_array( $nonPublicId, $object_states ) )
			{
				// get published version
				$filters = array( 'contentobject_id' => $object->attribute( 'id' ),
				                  'status' => eZContentObjectVersion::STATUS_PUBLISHED
				               );
				
				$return = eZContentObjectVersion::fetchFiltered( $filters, 0, 1 );
			}

			// add any pending versions
			$statuses = array( eZContentObjectVersion::STATUS_DRAFT,
			                   eZContentObjectVersion::STATUS_PENDING,
			                   eZContentObjectVersion::STATUS_QUEUED,
			                   eZContentObjectVersion::STATUS_REJECTED );
			
			$filters = array( 'contentobject_id' => $object->attribute( 'id' ),
			                  'status' => array( $statuses )
			);
			
			$return = array_merge( $return, eZContentObjectVersion::fetchFiltered( $filters, 0, 200 ) );
		}
				
		return $return;
	}	
}

?>