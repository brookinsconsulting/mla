<?php 
/**
* View to execute the collaboration action like "approve" or "reject"
*
* @copyright //autogen//
* @license //autogen//
* @version //autogen//
*/
$module = $Params[ 'Module' ];

if( $Params[ 'version_id' ] && $Params[ 'action' ] )
{
	$version = eZContentObjectVersion::fetch( $Params[ 'version_id' ] );

	$adt = new ezcollaborationworkflowDecisionTable();
	
	//TODO: execute should take the version object
	$success = $adt->execute( $Params[ 'action' ], $Params[ 'version_id' ] );

	return $module->redirectTo( '/approval/view/' . $version->attribute( 'contentobject_id' ) . '/' . $Params[ 'version_id' ] );
}

return $module->redirectTo( '/approval/list' );
?>