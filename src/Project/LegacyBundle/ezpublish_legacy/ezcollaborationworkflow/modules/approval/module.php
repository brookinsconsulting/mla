<?php
/**
*
* @copyright //autogen//
* @license //autogen//
* @version //autogen//
*/

$decisionTable = new ezcollaborationworkflowDecisionTable();

$Module = array( 'name' => 'Mugo Approval Module' );

$ViewList = array();
$ViewList[ 'list' ] = array( 'script' => 'list.php',
                             'default_navigation_part' => 'ezcollaborationworkflownavigationpart',
                             'functions' => array( 'list' ) );

$ViewList[ 'view' ] = array( 'script' => 'view.php',
                             'params' => array( 'object_id', 'version_id' ),
                             'default_navigation_part' => 'ezcollaborationworkflownavigationpart',
                             'functions' => array( 'list' )
);

$ViewList[ 'process' ] = array( 'script' => 'process.php',
                                'functions' => array( 'list' ),
                                'params' => array( 'action', 'version_id' )
);

$ViewList[ 'comment' ] = array( 'script' => 'comment.php',
                                'functions' => array( 'list' ),
                                'params' => array( 'object_id', 'version_id' )
);

$ViewList[ 'decisiontable' ] = array( 'script' => 'decisiontable.php',
                                      'functions' => array( 'list' ),
                                      'default_navigation_part' => 'ezsetupnavigationpart',
                                      'params' => array()
);

// policy limitations
$ClassID = array(
    'name'=> 'Class',
    'values'=> array(),
    'path' => 'classes/',
    'file' => 'ezcontentclass.php',
    'class' => 'eZContentClass',
    'function' => 'fetchList',
    'parameter' => array( 0, false, false, array( 'name' => 'asc' ) )
    );

$ParentClassID = array(
    'name'=> 'ParentClass',
    'values'=> array(),
    'path' => 'classes/',
    'file' => 'ezcontentclass.php',
    'class' => 'eZContentClass',
    'function' => 'fetchList',
    'parameter' => array( 0, false, false, array( 'name' => 'asc' ) )
    );

$SectionID = array(
    'name'=> 'Section',
    'values'=> array(),
    'path' => 'classes/',
    'file' => 'ezsection.php',
    'class' => 'eZSection',
    'function' => 'fetchList',
    'parameter' => array( false )
    );

$VersionStatusRead = array(
    'name'=> 'Status',
    'values'=> array(),
    'path' => 'classes/',
    'file' => 'ezcontentobjectversion.php',
    'class' => 'eZContentObjectVersion',
    'function' => 'statusList',
    'parameter' => array( 'read' )
    );

$VersionStatusRemove = array(
    'name'=> 'Status',
    'values'=> array(),
    'path' => 'classes/',
    'file' => 'ezcontentobjectversion.php',
    'class' => 'eZContentObjectVersion',
    'function' => 'statusList',
    'parameter' => array( 'remove' )
    );

$VersionStatusSelect = array(
    'name'=> 'Status',
    'values'=> array(
		array(
				'Name' => 'Draft',
				'value' => '0'
		),
		array(
				'Name' => 'Published',
				'value' => '1'
		),
		array(
				'Name' => 'Pending',
				'value' => '2'
		),
    		array(
				'Name' => 'Archived',
				'value' => '3'
		),
    		array(
				'Name' => 'Rejected',
				'value' => '4'
		),
    		array(
				'Name' => 'Queued',
				'value' => '7'
		),
));

$Language = array(
    'name'=> 'Language',
    'values'=> array(),
    'path' => 'classes/',
    'file' => 'ezcontentlanguage.php',
    'class' => 'eZContentLanguage',
    'function' => 'fetchLimitationList',
    'parameter' => array( false )
    );

$Assigned = array(
    'name'=> 'Owner',
    'values'=> array(
        array(
            'Name' => 'Self',
            'value' => '1')
        )
    );

$AssignedEdit = array(
    'name'=> 'Owner',
    'single_select' => true,
    'values'=> array(
        array( 'Name' => 'Self',
               'value' => '1'),
        array( 'Name' => 'Self or anonymous users per HTTP session',
               'value' => '2' ) ) );

$ParentAssignedEdit = array(
    'name'=> 'ParentOwner',
    'single_select' => true,
    'values'=> array(
        array( 'Name' => 'Self',
               'value' => '1'),
        array( 'Name' => 'Self or anonymous users per HTTP session',
               'value' => '2' ) ) );


$AssignedGroup = array(
    'name'=> 'Group',
    'single_select' => true,
    'values'=> array(
        array( 'Name' => 'Self',
               'value' => '1') ) );

$ParentAssignedGroup = array(
    'name'=> 'ParentGroup',
    'single_select' => true,
    'values'=> array(
        array( 'Name' => 'Self',
               'value' => '1') ) );

$ParentDepth = array(
    'name' => 'ParentDepth',
    'values' => array(),
    'path' => 'classes/',
    'file' => 'ezcontentobjecttreenode.php',
    'class' => 'eZContentObjectTreeNode',
    'function' => 'parentDepthLimitationList',
    'parameter' => array( false )
    );

$Node = array(
    'name'=> 'Node',
    'values'=> array()
    );

$Subtree = array(
    'name'=> 'Subtree',
    'values'=> array()
    );

$stateLimitations = eZContentObjectStateGroup::limitations();


$FunctionList[ 'list' ] = array();

$FunctionList[ 'process' ] = array(
                                    #'Section' => $SectionID,
                                    #'Owner' => $AssignedEdit,
                                    #'Group' => $AssignedGroup,
                                    #'Node' => $Node,
                                    #'Subtree' => $Subtree,
                                    'Workflow_Actions' => $decisionTable->getWorkflowActionsAsLimitation(),
                                    'Class' => $ClassID,
                                    'Language' => $Language,
                                    'Status' => $VersionStatusSelect,
);

// add object states limits
$FunctionList[ 'process' ] = array_merge( $FunctionList['process'], $stateLimitations );

?>
