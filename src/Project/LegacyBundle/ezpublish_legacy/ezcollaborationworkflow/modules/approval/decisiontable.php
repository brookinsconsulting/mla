<?php 
/**
* View to graphically present the configured decision tables
*
* @copyright //autogen//
* @license //autogen//
* @version //autogen//
*/

$tpl = eZTemplate::factory();
$adt = new ezcollaborationworkflowDecisionTable();

$approval_ini = eZINI::instance( 'ezcollaborationworkflow.ini' );
$workflows = $approval_ini->group( 'Workflows' );

$selected_table = null;
if( isset( $_REQUEST[ 'workflow' ] ) && array_key_exists( $_REQUEST[ 'workflow' ], $workflows ) )
{
	$selected_table = $_REQUEST[ 'workflow' ];
}

$ruleNames = $adt->getRuleNames( $selected_table );

$condition_labels = array();
$action_labels = array();

if( !empty( $ruleNames ) )
{
	foreach( $ruleNames as $ruleName )
	{
		// get a unique lists of conditions
		$conditions = $adt->getConditions( $ruleName );
		
		if( !empty( $conditions ) )
		{
			foreach( $conditions as $condition => $value )
			{
				$condition_labels = array_merge( $condition_labels, $adt->getLabels( $condition, $value ) );
			}
		}

		// get a unique lists of actions
		$actions = $adt->getActions( $ruleName );

		if( !empty( $actions ) )
		{
			foreach( $actions as $action => $value )
			{
				$action_labels = array_merge( $action_labels, $adt->getLabels( $action, $value ) );
			}
		}
	}
}

$condition_labels = array_unique( $condition_labels );
$action_labels    = array_unique( $action_labels );

sort( $condition_labels );
sort( $action_labels );

$conditionMatrix = array();
$actionMatrix = array();

if( !empty( $ruleNames ) )
{
	foreach( $ruleNames as $i => $ruleName )
	{
		// build conditions matrix
		foreach( $condition_labels as $j => $label )
		{
			$has_condition = false;

			$conditions = $adt->getConditions( $ruleName );
		
			if( !empty( $conditions ) )
			{
				foreach( $conditions as $condition => $value )
				{
					$testLabels = $adt->getLabels( $condition, $value );
					
					foreach( $testLabels as $testLabel )
					{
						if( $label == $testLabel )
						{
							$has_condition = true;
							break 2;
						}
					}
				}
			}

			$conditionMatrix[ $i ][ $j ] = $has_condition;
		}

			// build actions matrix
		foreach( $action_labels as $j => $label )
		{
			$has_action = false;

			$actions = $adt->getActions( $ruleName );
		
			if( !empty( $actions ) )
			{
				foreach( $actions as $action => $value )
				{
					$testLabels = $adt->getLabels( $action, $value );
					
					foreach( $testLabels as $testLabel )
					{
						if( $label == $testLabel )
						{
							$has_action = true;
							break 2;
						}
					}
				}
			}

			$actionMatrix[ $i ][ $j ] = $has_action;
		}
	}
}

//print_r( $conditionMatrix );

$data = array( 'name'             => $selected_table ? $selected_table : 'All Rules',
               'rule_labels'      => $ruleNames,
               'condition_labels' => $condition_labels,
               'action_labels'    => $action_labels,
               'conditions'       => $conditionMatrix,
               'actions'          => $actionMatrix
             );

$tpl->setVariable( 'data', $data );
$tpl->setVariable( 'workflows', $workflows );
$tpl->setVariable( 'current_workflow', $selected_table );

$Result = array();
$Result['content'] = $tpl->fetch( 'design:modules/approval/decisiontable.tpl' );
$Result['path'] = array( array( 'url' => 'approval/list',
                                'text' => 'Approval' ),
                         array( 'url' => false,
                                'text' => 'Decision Tables' ) );
                       
?>