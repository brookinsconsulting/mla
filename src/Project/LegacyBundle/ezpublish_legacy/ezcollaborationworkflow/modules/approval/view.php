<?php 
/**
* Detailed view of a collaboration object
*
* @copyright //autogen//
* @license //autogen//
* @version //autogen//
*/
$tpl = eZTemplate::factory();
$module = $Params[ 'Module' ];
$action_name = null;

if( $Params[ 'object_id' ] )
{
	$object = eZContentObject::fetch( $Params[ 'object_id' ] );
	
	if( $object instanceof eZContentObject )
	{
		$pending_versions = ezcollaborationworkflowFunctionCollection::get_approve_versions( $object );

		if( $Params[ 'version_id' ] )
		{
			// Handle POSTs
			if( isset( $_REQUEST[ 'action' ] ) )
			{
				$extra = null;
				if( $_REQUEST[ 'actioncomment' ] )
				{
					$extra = array( 'action_comment' => $_REQUEST[ 'actioncomment' ] );
				}
				
				$action = key( $_REQUEST[ 'action' ] );
				$action_name = current( $_REQUEST[ 'action' ] );
				
				//TODO: execute should take the version object
				$adt = new ezcollaborationworkflowDecisionTable();
				$success = $adt->execute( $action, $Params[ 'version_id' ], $extra );
				
				//Refresh $object and $pending_versions
				$object = eZContentObject::fetch( $Params[ 'object_id' ] );
				$pending_versions = ezcollaborationworkflowFunctionCollection::get_approve_versions( $object );
			}
			
			$version  = eZContentObjectVersion::fetch( $Params[ 'version_id' ] );
			$messages = ezcollaborationworkflowMessage::fetchByObjectId( $Params[ 'object_id' ] );
		}
		else
		{
			if( !empty( $pending_versions ) )
			{
				$version_id = $pending_versions[ 0 ]->attribute( 'id' );
				return $module->redirectTo( '/approval/view/' . $Params[ 'object_id' ] . '/' . $version_id );
			}
			else
			{
				return $module->redirectTo( '/approval/list' );
			}
		}
	}
}
else
{
	return $module->redirectTo( '/approval/list' );
}

$tpl->setVariable( 'object', $object );
$tpl->setVariable( 'pending_versions', $pending_versions );
$tpl->setVariable( 'version', $version );
$tpl->setVariable( 'messages', $messages );
$tpl->setVariable( 'last_action', $action_name );

$Result = array();
$Result['content'] = $tpl->fetch( 'design:modules/approval/view.tpl' );
$Result['path'] = array( array( 'url' => 'approval/list',
                                'text' => 'Approval' ),
                         array( 'url' => false,
                                'text' => 'View' ) );
?>