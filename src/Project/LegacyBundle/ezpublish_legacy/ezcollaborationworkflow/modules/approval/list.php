<?php 
/**
* Template parses the list of objects that are part of the collaboration workflow
*
* @copyright //autogen//
* @license //autogen//
* @version //autogen//
*/
$tpl = eZTemplate::factory();

$Result = array();
$Result['content'] = $tpl->fetch( 'design:modules/approval/list.tpl' );
$Result['path'] = array( array( 'url' => 'approval/list',
                                'text' => 'Approval' ),
                         array( 'url' => false,
                                'text' => 'List' ) );
                       
?>