<?php 
/**
* Creates a new comment and redirects back to approval view
*
* @copyright //autogen//
* @license //autogen//
* @version //autogen//
*/

$module = $Params[ 'Module' ];

if( $Params[ 'object_id' ] && $Params[ 'version_id' ] )
{
	if( $_REQUEST[ 'newcomment' ] )
	{
		$current_ts = time();
		
		$version = eZContentObjectVersion::fetch( $Params[ 'version_id' ] );
		
		$user = eZUser::currentUser();
		$creatorID = $user->attribute( 'contentobject_id' );
            
		$comment_row = array( 'message_type' => 'object_comment',
                              'data_int1'    => $Params[ 'object_id' ],
                              'data_int2'    => $version->attribute( 'version' ),
		                      'data_text1'   => $_REQUEST[ 'newcomment' ],
                              'creator_id'   => $creatorID,
                              'created'      => $current_ts,
                              'modified'     => $current_ts
		);
		
		$comment = new ezcollaborationworkflowMessage( $comment_row );
		$comment->store();
		
		$adt = new ezcollaborationworkflowDecisionTable();
		$success = $adt->execute( 'comment', $Params[ 'version_id' ] );
	}
		
	return $module->redirectTo( '/approval/view/' . $Params[ 'object_id' ] . '/' . $Params[ 'version_id' ] . '/(tab)/discussion' );
}

return $module->redirectTo( '/approval/list' );

?>