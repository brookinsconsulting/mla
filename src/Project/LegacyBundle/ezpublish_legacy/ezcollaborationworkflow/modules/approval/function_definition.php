<?php
/**
*
* @copyright //autogen//
* @license //autogen//
* @version //autogen//
*/

$FunctionList = array();

$FunctionList[ 'can_approve' ] = array(
                           'name' => 'can_approve',
                           'call_method' => array(
                                                  'class' => 'ezcollaborationworkflowFetchFunctionCollection',
                                                  'method' => 'can_approve' ),
                           'parameter_type' => 'standard',
                           'parameters' => array(
                                                 array(  'name'     => 'object',
                                                         'type'     => 'object',
                                                         'required' => true
                                                 ),
                                                 array(  'name'     => 'version',
                                                         'type'     => 'object',
                                                         'required' => false
                                                 ),
                                                 array(  'name'     => 'action',
                                                         'type'     => 'string',
                                                         'required' => false
                                                 )
                                                 )
                                     );
                                     
 $FunctionList[ 'can_do_action' ] = array(
                            'name' => 'can_do_action',
                            'call_method' => array(
                                                   'class' => 'ezcollaborationworkflowFetchFunctionCollection',
                                                   'method' => 'can_do_action' ),
                            'parameter_type' => 'standard',
                            'parameters' => array(
                                                  array(  'name'     => 'object',
                                                          'type'     => 'object',
                                                          'required' => true
                                                  ),
                                                  array(  'name'     => 'version',
                                                          'type'     => 'object',
                                                          'required' => false
                                                  ),
                                                  array(  'name'     => 'action',
                                                          'type'     => 'string',
                                                          'required' => false
                                                  )
                                                  )
                                      );                                     

$FunctionList[ 'object_relevant_states' ] = array(
                           'name' => 'object_relevant_states',
                           'call_method' => array( 
                                                  'class' => 'ezcollaborationworkflowFetchFunctionCollection',
                                                  'method' => 'object_relevant_states'
                                                 ),
                           'parameter_type' => 'standard',
                           'parameters' => array(
                                                 array( 'name'     => 'object',
                                                        'type'     => 'object',
                                                        'required' => false
                                                       )
                                                   ) );

$FunctionList[ 'state_id_to_name' ] = array(
                           'name' => 'state_id_to_name',
                           'call_method' => array( 
                                                  'class' => 'ezcollaborationworkflowFetchFunctionCollection',
                                                  'method' => 'state_id_to_name' ),
                           'parameter_type' => 'standard',
                           'parameters' => array(
                                                 array(  'name'     => 'id',
                                                         'type'     => 'integer',
                                                         'required' => true
                                                 )
                                                 )
                                     );

$FunctionList[ 'get_approve_versions' ] = array(
                           'name' => 'get_approve_versions',
                           'call_method' => array(
                                                  'class' => 'ezcollaborationworkflowFetchFunctionCollection',
                                                  'method' => 'get_approve_versions' ),
                           'parameter_type' => 'standard',
                           'parameters' => array(
                                                 array(  'name'     => 'object',
                                                         'type'     => 'object',
                                                         'required' => true
                                                 )
                                                 )
                                     );

$FunctionList[ 'get_object_state_label' ] = array(
                           'name' => 'get_object_state_label',
                           'call_method' => array(
                                                  'class' => 'ezcollaborationworkflowFetchFunctionCollection',
                                                  'method' => 'get_object_state_label' ),
                           'parameter_type' => 'standard',
                           'parameters' => array(
                                                 array(  'name'     => 'id',
                                                         'type'     => 'integer',
                                                         'required' => true
                                                 ),
                                                 array(  'name'     => 'language_id',
                                                         'type'     => 'integer',
                                                         'required' => false
                                                 )
                                                 )
                                     );

$FunctionList[ 'get_latest_comment' ] = array(
                           'name' => 'get_latest_comment',
                           'call_method' => array(
                                                  'class' => 'ezcollaborationworkflowFetchFunctionCollection',
                                                  'method' => 'get_latest_comment' ),
                           'parameter_type' => 'standard',
                           'parameters' => array(
                                                 array(  'name'     => 'id',
                                                         'type'     => 'integer',
                                                         'required' => false
                                                 )
                                                 )
                           );

$FunctionList[ 'state_group' ] = array(
                           'name' => 'state_group',
                           'call_method' => array(
                                                  'class' => 'ezcollaborationworkflowFetchFunctionCollection',
                                                  'method' => 'state_group' ),
                           'parameter_type' => 'standard',
                           'parameters' => array(
                                                 array(  'name'     => 'id',
                                                         'type'     => 'integer',
                                                         'required' => true
                                                      )
                                                )
);


?>
