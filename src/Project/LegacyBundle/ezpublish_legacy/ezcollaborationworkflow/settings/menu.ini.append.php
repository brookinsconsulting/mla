<?php /* #?ini charset="utf-8"?

[NavigationPart]
Part[ezcollaborationworkflownavigationpart]=ezcollaborationworkflow

[TopAdminMenu]
Tabs[]=ezcollaborationworkflow

[Topmenu_ezcollaborationworkflow]
NavigationPartIdentifier=ezcollaborationworkflownavigationpart
Name=Collaboration
Tooltip=Collaboration Dashboard
URL[]
URL[default]=approval/list
Enabled[]
Enabled[default]=true
Enabled[browse]=false
Enabled[edit]=false
Shown[]
Shown[default]=true
Shown[edit]=true
Shown[navigation]=true
Shown[browse]=false

[Leftmenu_setup]
Links[decision_tables]=approval/decisiontable
LinkNames[decision_tables]=Decision Tables
PolicyList_decision_tables[]=approval/list

*/ ?>