<?php
/**
* eZ Workflow to catch the "Send to publish" action
*
* @copyright //autogen//
* @license //autogen//
* @version //autogen//
*/

/*!

  WorkflowEvent storage fields : data_text1 - selected_sections
                                 data_text2 - selected_usergroups
                                 data_text3 - approve_users
                                 data_text4 - approve_groups
                                 data_int2  - language_list
                                 data_int3  - content object version option
*/
class ezcollaborationworkflowEventType extends eZWorkflowEventType
{
	const WORKFLOW_TYPE_STRING = 'ezcollaborationworkflowevent';

	function __construct()
	{
		$this->eZWorkflowEventType( ezcollaborationworkflowEventType::WORKFLOW_TYPE_STRING, ezpI18n::tr( 'kernel/workflow/event', "ApprovalQueuer" ) );
		$this->setTriggerTypes( array( 'content' => array( 'publish' => array( 'before' ) ) ) );
	}

	function execute( $process, $event )
	{
		$parameters = $process->attribute( 'parameter_list' );
		$versionID = $parameters['version'];
		$objectID = $parameters['object_id'];
		$object = eZContentObject::fetch( $objectID );
		
		if ( !$object )
		{
			eZDebugSetting::writeError( 'kernel-workflow-approvalqueuer', "No object with ID $objectID", 'eZApproveType::execute' );
			return eZWorkflowType::STATUS_WORKFLOW_CANCELLED;
		}

		$version = $object->version( $versionID );

		if ( !$version )
		{
			eZDebugSetting::writeError( 'kernel-workflow-approvalqueuer', "No version $versionID for object with ID $objectID", 'eZApproveType::execute' );
			return eZWorkflowType::STATUS_WORKFLOW_CANCELLED;
		}

		// run the version against the decision table
		$adt = new ezcollaborationworkflowDecisionTable();
		
		// Handle new objects vs objects with new pending version
		// By default we don't interupt the publishing operation - only
		// if the user is doing a content update and only if $adt says so
		// (getInterruptPublishingOperation)
		if( count( $object->attribute( 'versions' ) ) > 1 )
		{
			$adt->execute( 'update', $version->attribute( 'id' ) );

			if( $adt->getInterruptPublishingOperation() )
			{
				$node = $object->attribute( 'main_node' );
				$process->RedirectUrl = $node->attribute( 'url_alias' );
				
				return eZWorkflowType::STATUS_REDIRECT;
			}
		}
		else
		{
			$success = $adt->execute( 'create', $version->attribute( 'id' ) );
		}
		
		return eZWorkflowType::STATUS_ACCEPTED;
	}
}

eZWorkflowEventType::registerEventType( ezcollaborationworkflowEventType::WORKFLOW_TYPE_STRING, "ezcollaborationworkflowEventType" );

?>
