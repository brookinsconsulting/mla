{ezcss_require( 'approval.css' )}
{ezscript_require( 'decisiontable.js' )}

<div id="approval-decisiontable-page">

	<h1>{'Decision Tables'|i18n( 'ezcollaborationworkflow' )}</h1>
	<div>
		{'Select Workflow'|i18n( 'ezcollaborationworkflow' )}:
		<form id="select-workflow" action={'/approval/decisiontable'|ezurl()} method="post">
			<select name="workflow">
				<option value="">{'All Rules'|i18n( 'ezcollaborationworkflow' )}</option>
				{foreach $workflows as $name => $workflow}
					<option value="{$name|wash()}" {if eq( $current_workflow, $name )}selected="selected"{/if}>
						{$name|wash()}
					</option>
				{/foreach}
			</select>
		</form>
	</div>
	
	<div class="approval-decisiontable-wrapper">

		<table>
			<caption>{$data.name|wash()}</caption>

			<tbody>
			<tr>
				<td></td>
				<td style="text-align: right;" colspan="{sum( $data.rule_labels|count(), 1)}">
					{* <span class="rule-default-label">Rule:</span> *}&nbsp;
					{foreach $data.rule_labels as $index => $label}
						<span class="rule-labels col-{$index}">{$label|wash()}</span>
					{/foreach}
				</td>
			</tr>
			
			{foreach $data.condition_labels as $index => $label}
				<tr>
					{if eq( $index, 0 )}
						<td rowspan="{$data.condition_labels|count()}"><strong>{'Conditions'|i18n( 'ezcollaborationworkflow' )}</strong></td>
					{/if}
					<td class="row-condition-{$index}">{$label|wash()}</td>
					
					{foreach $data.conditions as $i => $condition}
						<td class="col-{$i} row-condition-{$index}">{if $condition[ $index ]}X{/if}</td>
					{/foreach}
				</tr>
			{/foreach}
			{* separator *}
			<tr>
				<td colspan="{sum( $data.rule_labels|count(), 2 )}">&nbsp;</td>
			</tr>
			{foreach $data.action_labels as $index => $label}
				<tr>
					{if eq( $index, 0 )}
						<td rowspan="{$data.action_labels|count()}"><strong>{'Actions'|i18n( 'ezcollaborationworkflow' )}</strong></td>
					{/if}
					<td class="row-action-{$index}">{$label|wash()}</td>
					
					{foreach $data.actions as $i => $action}
						<td class="col-{$i} row-action-{$index}">{if $action[ $index ]}X{/if}</td>
					{/foreach}
				</tr>
			{/foreach}

			</tbody>
		</table>
	</div>
</div>
