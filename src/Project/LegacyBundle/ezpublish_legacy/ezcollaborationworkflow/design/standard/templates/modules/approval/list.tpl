{ezcss_require( 'approval.css' )}
{ezscript_require( 'approvallist.js' )}

{def $unapproved_state	= ezini( 'ObjectStateIds', 'UnApprovedId', 'ezcollaborationworkflow.ini' )
	 $non_public_state	= ezini( 'ObjectStateIds', 'NonPublicId', 'ezcollaborationworkflow.ini' )
	 $visibility_group_id = ezini( 'ObjectStateIds', 'VisibilityStateGroupId', 'ezcollaborationworkflow.ini' )
	 $list_columns = ezini( 'Generic', 'ListColumns', 'ezcollaborationworkflow.ini' )
	 $page_limit = 100
	 $offset = first_set( $view_parameters.offset, 0 )
	 $list = fetch( 'content', 'tree', hash( 'parent_node_id', 1,
											 'attribute_filter', array( array( 'state', '=', $unapproved_state ) ),
											 'limit', $page_limit,
											 'offset', $offset,
											 'sort_by', array( 'modified', false())
											 ))}
<div id="approval-list-page">
	<h1>{'Approval List'|i18n( 'ezcollaborationworkflow' )}</h1>
	
	{if $list|count()}
		<div class="yui-dt">
			<table id="approvallist">
				<thead>
					<tr>
						{if $list_columns|contains('actions')}
						<th>
							<div class="yui-dt-liner">
								{'Actions'|i18n( 'ezcollaborationworkflow' )}
							</div>
						</th>
						{/if}
						{if $list_columns|contains('name')}
						<th>
							<div class="yui-dt-liner">
								{'Name'|i18n( 'ezcollaborationworkflow' )}
							</div>
						</th>
						{/if}
						{if $list_columns|contains('author')}
						<th>
							<div class="yui-dt-liner">
								{'Author'|i18n( 'ezcollaborationworkflow' )}
							</div>
						</th>
						{/if}
						<th>
							<div class="yui-dt-liner">
								{'Visibility'|i18n( 'ezcollaborationworkflow' )}
							</div>
						</th>
						{if $list_columns|contains('current_state')}
						<th>
							<div class="yui-dt-liner">
								{'Current State'|i18n( 'ezcollaborationworkflow' )}
							</div>
						</th>
						{/if}
						{if $list_columns|contains('version')}
						<th>
							<div class="yui-dt-liner">
								{'Version'|i18n( 'ezcollaborationworkflow' )}
							</div>
						</th>
						{/if}
						{if $list_columns|contains('last_modified')}
						<th>
							<div class="yui-dt-liner">
								{'Last Modification'|i18n( 'ezcollaborationworkflow' )}
							</div>
						</th>
						{/if}
						{if $list_columns|contains('comments')}
						<th>
							<div class="yui-dt-liner">
								{'Latest Comment'|i18n( 'ezcollaborationworkflow' )}
							</div>
						</th>
						{/if}
					</tr>
				</thead>
	
				<tbody class="yui-dt-data">
					{def $comment = ''
					     $relevant_states = ''
					}
					{foreach $list as $index => $entry}
						
						<tr class="{if eq( $index|mod(2), 0 )}yui-dt-odd{else}yui-dt-even{/if}">
							{if $list_columns|contains('actions')}
							<td class="approve">
								<div class="yui-dt-liner">
									{if fetch( 'approval', 'can_approve', hash( 'object', $entry.object ) )}
										<a class="defaultbutton" href={concat( '/approval/view/', $entry.contentobject_id )|ezurl()}>{'View'|i18n( 'ezcollaborationworkflow' )}</a>
									{else}
										<a class="button" href={concat( '/approval/view/', $entry.contentobject_id )|ezurl()}>{'View'|i18n( 'ezcollaborationworkflow' )}</a>
									{/if}
								</div>
							</td>
							{/if}
							{if $list_columns|contains('name')}
							<td class="name">
								<div class="yui-dt-liner">
									{$entry.class_identifier|class_icon( 'small' )}
									<a href={$entry.url_alias|ezurl()}>{$entry.name|wash()}</a>
								</div>
							</td>
							{/if}
							{if $list_columns|contains('author')}
							<td class="owner">
								<div class="yui-dt-liner">
									{$entry.object.owner.name|wash()}
								</div>
							</td>
							<td class="visibility">
								<div class="yui-dt-liner">
									<span rel="{$entry.object.state_id_array[ $visibility_group_id ]}">[{fetch( 'approval', 'get_object_state_label', hash( 'id', $entry.object.state_id_array.[ $visibility_group_id ] ) )}]</span>
								</div>
							</td>
							<td class="states">
								<div class="yui-dt-liner">
									{set $relevant_states = fetch( 'approval', 'object_relevant_states', hash( 'object', $entry.object ) )}
									
									{foreach $relevant_states as $group_id => $data}
										{$data.group.current_translation.name|wash()}:
										{foreach $data.states as $state}
											{if $entry.object.state_id_array|contains( $state.id )}
												<span class="current" rel="{$state.id}">{$state.current_translation.name|wash()}</span><br />
											{/if}
										{/foreach}
									{/foreach}
								</div>
							</td>
							{/if}
							{if $list_columns|contains('version')}
							<td>
								<div class="yui-dt-liner">
									{$entry.object.current_version}
								</div>
							</td>
							{/if}
							{if $list_columns|contains('last_modified')}
							<td>
								<div class="yui-dt-liner">
									{$entry.object.modified|l10n( 'shortdatetime' )}
								</div>
							</td>
							{/if}
							{if $list_columns|contains('comments')}
							<td>
								<div class="yui-dt-liner">
									{set $comment = fetch( 'approval', 'get_latest_comment', hash( 'id', $entry.contentobject_id ) )}
									
									{$comment.data_text1|wash()}
								</div>
							</td>
							{/if}
						</tr>
					{/foreach}
				<tbody>
			</table>
		</div>
	{else}
		{'Your approval list is currently empty.'|i18n( 'ezcollaborationworkflow' )}
	{/if}
</div>

<script type="text/javascript">
$( '#approvallist' ).approvalList();
</script>
