<li class="ui-message">
	<div class="ui-message-header">
		<div style="float: left">
			{'By'|i18n( 'ezcollaborationworkflow' )} <a href={$author.main_node.url_alias|ezurl()}>{$author.name|wash()}</a> {'on Version'|i18n( 'ezcollaborationworkflow' )} {$message.data_int2}
			
			{if $message.data_text2}
				<p>
					<span class="ui-sprite-arrow-right"></span> {$message.data_text2|wash()}
				</p>
			{/if}
		</div>
		<div style="float: right">
			on {$message.created|l10n( 'datetime' )}
		</div>
		
		<div style="clear: both"></div>
	</div>
	{if $message.data_text1}
		<div class="ui-message-body">
			{$message.data_text1|wash()}
		</div>
	{/if}
</li>
