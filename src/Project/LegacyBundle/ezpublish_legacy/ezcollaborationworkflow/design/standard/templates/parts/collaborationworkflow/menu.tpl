{ezscript_require( array( 'jquery.cookie.min.js',
                          'jquery.json-2.3.min.js',
                          'approvalfilter.js' ) )}

{if eq( $module_result.path.1.text, 'List' )}
	{def $relevant_states     = fetch( 'approval', 'object_relevant_states' )
	     $visibility_group_id = ezini( 'ObjectStateIds', 'VisibilityStateGroupId', 'ezcollaborationworkflow.ini' )
	     $visibility_group    = fetch( 'approval', 'state_group', hash( 'id', $visibility_group_id ) )
	}

	<p id="f_count"></p>
	<div id="approvalfilter">

		<div class="box-header">
			<div class="box-ml">
				<h4>{'State Filter'|i18n( 'ezcollaborationworkflow' )}</h4>
			</div>
		</div>
		
		<div class="box-bc"><div class="box-ml"><div class="box-content">
			<ul id="visibility-filter">
				{* TODO: get name from group object *}
				<li>{'Visibility'|i18n( 'ezcollaborationworkflow' )}:
					<ul>
						{foreach $visibility_group.states as $state}
							<li>
								<label>
									<input type="checkbox" checked="checked" value="{$state.id}" />
									{* TODO: Check how the template prints out the state name (wrong language?) *}
									{$state.translations.0.name}
								<label>
							</li>
						{/foreach}
					</ul>
				</li>
			</ul>
			<ul id="state-filter">
				{foreach $relevant_states as $data}
				<li>{$data.group.current_translation.name|wash()}:
					<ul>
					{foreach $data.states as $state}
					<li><label><input type="checkbox" checked="checked" value="{$state.id}" />
						{$state.current_translation.name|wash()}<br />
					</label></li>
					{/foreach}
					</ul>
				</li>	
				{/foreach}
			</ul>
		</div></div></div>
		
		<div class="box-header">
			<div class="box-ml">
				<h4>{'Text Filter'|i18n( 'ezcollaborationworkflow' )}</h4>
			</div>
		</div>
		
		<div class="box-bc"><div class="box-ml"><div class="box-content">
			<p><label for="owner-filter">{'Author'|i18n( 'ezcollaborationworkflow' )}:<br />
			<input type="text" id="owner-filter" /></label></p>
		
			<p><label for="name-filter">{'Object Name'|i18n( 'ezcollaborationworkflow' )}:<br />
			<input type="text" id="name-filter" /></label></p>
		</div></div></div>
	
		<div class="box-header">
			<div class="box-ml">
				<h4>{'More'|i18n( 'ezcollaborationworkflow' )}</h4>
			</div>
		</div>
		
		<div class="box-bc"><div class="box-ml"><div class="box-content">
			<ul>
				<li>{'Permissions'|i18n( 'ezcollaborationworkflow' )}:
					<ul>
						<li><label><input id="approve" type="checkbox" value="1" />
						Can Edit</label></li>
					</ul>
				</li>	
			</ul>	
		</div></div></div>
	</div>
	
	<script type="text/javascript">
	{literal}
	
		var changeCallback = function( filter )
		{
			// Update Cookie
			$.cookie.json = true;
			$.cookie( 'approvallist', filter );

			$( '#approvallist' ).approvalList( 'filterList', filter );
		};
		
		var readyCallback = function( filter )
		{
			$( '#approvallist' ).approvalList( 'filterList', filter );
		};

		$(function()
		{
			// get cookie values
			$.cookie.json = true;
			var filter = $.cookie( 'approvallist' ) || false;
			
			$( '#approvalfilter' ).approvalFilter({ filter : filter,
			                                        change : changeCallback,
			                                        ready  : readyCallback
			                                      });
		});
				
	{/literal}
	</script>

{else}
	<div class="box-header">
		<div class="box-ml">
			<h4>{'Menu'|i18n( 'ezcollaborationworkflow' )}</h4>
		</div>
	</div>

	<div class="box-bc"><div class="box-ml"><div class="box-content">
		<ul>
			<li><a href={'/approval/list'|ezurl()}>{'Back to List'|i18n( 'ezcollaborationworkflow' )}</a></li>
	</div></div></div>
{/if}
