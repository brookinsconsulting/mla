{*
   INPUT
   
   $object
   $version
   $pending_versions
*}

{def $siteaccess       = ezini( 'Generic', 'PreviewSiteAccess', 'ezcollaborationworkflow.ini' )
     $non_public_state = ezini( 'ObjectStateIds', 'NonPublicId', 'ezcollaborationworkflow.ini' )
     $version_status   = array( 'Draft', 'New Object', 'Pending', 'Archived', 'Rejected', 'Internal Draft', 'Repeat', 'Queued' )
}

{ezscript_require( 'node_tabs.js', 'approval/approve.js' )}
{ezcss_require( 'approval.css' )}

{* get list of collaboration actions *}
{def $collab_actions = array()}
{foreach ezini( 'Generic', 'WorkflowActions', 'ezcollaborationworkflow.ini' ) as $identifier => $name}
	{if fetch( 'approval', 'can_do_action', hash( 'object', $object,
	                                            'version', $version, 
	                                            'action', $identifier ) )}
		{set $collab_actions = $collab_actions|append( array( $identifier, $name ) )}
	{/if}
{/foreach}


{if $last_action}
	<div class="feedback-msg">
		<p>{'You successfully executed'|i18n( 'ezcollaborationworkflow' )} "{$last_action|wash()}" {'on'|i18n( 'ezcollaborationworkflow' )} <a href={$object.main_node.url_alias|ezurl()}>{$object.main_node.url_alias|wash()}</a></p>
	</div>
{/if}

{if $pending_versions|count()}
<div id="controlbar-top" class="controlbar">
	<div class="box-bc">
		<div class="box-ml">
			<div class="button-left">
				{if $collab_actions|count()}
					<form method="post" action={concat( '/approval/view/', $object.id , '/', $version.id )|ezurl()}>
						{'Comment'|i18n( 'ezcollaborationworkflow' )}: <input type="text" class="ui-actioncomment" name="actioncomment" />
						{foreach $collab_actions as $details}
							<input type="submit" class="button" name="action[{$details.0|wash()}]" value="{$details.1|wash()|wrap( 32 )}" />
						{/foreach}
					</form>
				{else}
					{'Currently in progress.'|i18n( 'ezcollaborationworkflow' )}
				{/if}
			</div>
			<div class="float-break"></div>
		</div>	
	</div>	
</div>
{/if}

<h1>
	{$version.name|wash()}
	[{$object.class_name|wash()}]
	{if $object.state_id_array|contains( $non_public_state )}
		[{fetch( 'approval', 'get_object_state_label', hash( 'id', $non_public_state ) )}]
	{/if}
</h1>
<br />
<div id="approval-approve-page">

	<h2>{'Pending Versions'|i18n( 'ezcollaborationworkflow' )}</h2>
		
	<div id="all-buttons">
		<a class="button" href={concat( '/content/history/', $object.id )|ezurl()}>{'All Versions'|i18n( 'ezcollaborationworkflow' )}</a>&nbsp;&nbsp;
		<a class="button" href={$object.main_node.url_alias|ezurl()}>{'View Object'|i18n( 'ezcollaborationworkflow' )}</a>
	</div>

{if $pending_versions|count()}
	<div class="yui-dt">
		<table>
			<thead>
				<tr>
					<th>
						<div class="yui-dt-liner">
							{'Actions'|i18n( 'ezcollaborationworkflow' )}
						</div>
					</th>
					<th>
						<div class="yui-dt-liner">
							{'Version'|i18n( 'ezcollaborationworkflow' )}
						</div>
					</th>
					<th>
						<div class="yui-dt-liner">
							{'Translation'|i18n( 'ezcollaborationworkflow' )}
						</div>
					</th>
					<th>
						<div class="yui-dt-liner">
							{'Status'|i18n( 'ezcollaborationworkflow' )}
						</div>
					</th>
					<th>
						<div class="yui-dt-liner">
							{'Created'|i18n( 'ezcollaborationworkflow' )}
						</div>
					</th>
					<th>
						<div class="yui-dt-liner">
							{'Author'|i18n( 'ezcollaborationworkflow' )}
						</div>
					</th>
				</tr>
			</thead>
			<tbody class="yui-dt-data">
				{foreach $pending_versions as $index => $pending_version}
					<tr class="{if eq( $index|mod(2), 0 )}yui-dt-odd{else}yui-dt-even{/if} {if eq( $pending_version.id, $version.id )}yui-dt-selected{/if}">
						<td>
							<div class="yui-dt-liner">
								<a href={concat( '/approval/view/', $object.id , '/', $pending_version.id )|ezurl()}>{'select'|i18n( 'ezcollaborationworkflow' )}</a>
								{* allow to edit draft - should check for ownership *}
								{if and( $pending_version.status|eq(0), $pending_version.contentobject.can_edit )}
									<a href={concat( '/content/edit/', $object.id , '/', $pending_version.version , '/', $pending_version.initial_language.locale )|ezurl()}>{'edit'|i18n( 'ezcollaborationworkflow' )}</a>
								{/if}
							</div>
						</td>
						<td>
							<div class="yui-dt-liner">
								{$pending_version.version}
							</div>
						</td>
						<td>
							<div class="yui-dt-liner">
								{foreach $pending_version.language_list as $language}
									{if and( $pending_version.status|eq(1), $pending_version.contentobject.can_edit )}
										<a target="_new" href={concat( '/content/view/full/', $object.main_node_id , '/(language)/', $language.language_code )|ezurl()}>
									{/if}
									<img src="{$language.language_code|flag_icon}" width="18" height="12" alt="{$language.language_code}" title="{$language.language_code}"></img>
									{if and( $pending_version.status|eq(1), $pending_version.contentobject.can_edit )}
										</a>
									{/if}
								{/foreach}
							</div>
						</td>
						<td>
							<div class="yui-dt-liner">
								{$version_status[ $pending_version.status ]}
							</div>
						</td>
						<td>
							<div class="yui-dt-liner">
								{$pending_version.created|l10n( 'date' )}
							</div>
						</td>
						<td>
							<div class="yui-dt-liner">
								{$pending_version.creator.name|wash()}
							</div>
						</td>
					</tr>
			{/foreach}
			</tbody>
		</table>
	</div>
	<br />	
	<br />
	<h2>{'Approve Version'|i18n( 'ezcollaborationworkflow' )} {$version.version} &lt;{$version.name|wash()}&gt;</h2>
	<div class="tab-block" id="window-controls">
		<div class="button-left"><a title="{'Disable &quot;Tabs&quot; by default while browsing content.'|i18n( 'ezcollaborationworkflow' )}" href="/amnh/index.php/admin/user/preferences/set/admin_navigation_content/0" class="show-hide-tabs" id="maincontent-hide">-</a></div>
	
	<ul class="tabs tabs-by-cookie">
	    <li class="first selected" id="node-tab-view">
			<a title="{'Show simplified view of content.'|i18n( 'ezcollaborationworkflow' )}" href={concat( '/approval/view', $object.id , '/', $version.id, '/(tab)/view' )|ezurl()}>Content</a>
	    </li>
	    <li class="middle" id="node-tab-preview">
			<a title="{'Show details.'|i18n( 'ezcollaborationworkflow' )}" href={concat( '/approval/view', $object.id , '/', $version.id, '/(tab)/details' )|ezurl()}>Preview</a>
	    </li>
		<li class="middle" id="node-tab-current-state">
			<a title="{'Show details.'|i18n( 'ezcollaborationworkflow' )}" href={concat( '/approval/view', $object.id , '/', $version.id, '/(tab)/current-state' )|ezurl()}>Current State</a>
	    </li>
	    <li class="middle" id="node-tab-discussion">
			<a title="{'Show details.'|i18n( 'ezcollaborationworkflow' )}" href={concat( '/approval/view', $object.id , '/', $version.id, '/(tab)/discussion' )|ezurl()}>Discussion</a>
	    </li>
	</ul>
	
	<div class="float-break"></div>
	
	<div class="tabs-content">
		<div class="tab-content selected" id="node-tab-view-content">
			
			{foreach $version.data_map as $attribute}
				<div>
					<label>{$attribute.contentclass_attribute.name|wash}{if $attribute.is_information_collector} <span class="collector">({'information collector'|i18n( 'design/admin/content/edit_attribute' )})</span>{/if}:</label>
					{attribute_view_gui attribute=$attribute}
				</div>
			{/foreach}
			<div class="break"></div>
		</div>
		
		<div class="tab-content hide" id="node-tab-preview-content">
			<div class="mainobject-window">
				<iframe src={concat("content/versionview/",$version.contentobject_id,"/",$version.version,"/",$version.initial_language.locale, "/site_access/", $siteaccess )|ezurl} width="100%" height="800"></iframe>
			</div>
		</div>
		
		<div class="tab-content hide" id="node-tab-current-state-content">
			<div id="current-state">
				{def $relevant_states = fetch( 'approval', 'object_relevant_states', hash( 'object', $object ) )}
				{foreach $relevant_states as $group_id => $data}
					<label>{$data.group.current_translation.name|wash()}:</label>

					<ul class="states">
					{foreach $data.states as $state}
						<li {if $object.state_id_array|contains( $state.id )}class="current"{/if}>
							{$state.current_translation.name|wash()}
						</li>
					{/foreach}
					</ul>

					<div class="break"></div>
				{/foreach}
				<div class="break"></div>
			</div>
		</div>

		<div class="tab-content hide" id="node-tab-discussion-content">
			<div class="mainobject-window">
				<h2>{'Leave a comment'|i18n( 'ezcollaborationworkflow' )}</h2>
				<form method="post" action={concat( '/approval/comment/', $object.id , '/', $version.id )|ezurl()}>
					<textarea class="ui-newcomment" name="newcomment"></textarea>
					<input class="button" type="submit" name="Submit" value="{'Submit'|i18n( 'ezcollaborationworkflow' )}" />
				</form>
				<br /><br />
				{if $messages|count()}
					<ul class="ui-messages">
					{def $author = ''}
					{foreach $messages as $message}
						{set $author = fetch( 'content', 'object', hash( 'object_id', $message.creator_id ) )}
						
						{include uri='design:parts/message.tpl' message=$message author=$author}
					{/foreach}
					</ul>
				{/if}
			</div>
		</div>
	</div>

	<div id="controlbar-top" class="controlbar">
		<div class="box-bc">
			<div class="box-ml">
				<div class="button-left">
					<form method="post" action={concat( '/approval/view/', $object.id , '/', $version.id )|ezurl()}>
						{if $collab_actions|count()}
							<form method="post" action={concat( '/approval/view/', $object.id , '/', $version.id )|ezurl()}>
								{'Comment'|i18n( 'ezcollaborationworkflow' )}: <input type="text" class="ui-actioncomment" name="actioncomment" />
								{foreach $collab_actions as $details}
									<input type="submit" class="button" name="action[{$details.0|wash()}]" value="{$details.1|wash()|wrap(32)}" />
								{/foreach}
							</form>
						{else}
							{'Currently in progress.'|i18n( 'ezcollaborationworkflow' )}
						{/if}
					</form>
				</div>
				<div class="float-break"></div>
			</div>	
		</div>	
	</div>
	{else}
	<p>
		{'This object currently has no pending versions.'|i18n( 'ezcollaborationworkflow' )}
	</p>
{/if}
</div>