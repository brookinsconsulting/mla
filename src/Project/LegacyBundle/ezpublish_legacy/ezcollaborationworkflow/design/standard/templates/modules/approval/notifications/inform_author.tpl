{def $contentObject = $decisionObject.object}
<html>

<p>Hi there!</p>

<p>The following content is awaiting approval:</p>

<ul>
	<li>Name: <strong><a href="{concat( 'approval/view/', $contentObject.id )|ezroot( 'no', 'full' )}">{$contentObject.name|wash()}</a></strong></li>
	<li>Type: <strong>{$contentObject.class_name|wash()}</strong></li>
	<li>Author: <strong>{$contentObject.owner.name|wash()}</strong></li>
</ul>

Warm regards,<br />
eZCollaborationWorkflow

</html>