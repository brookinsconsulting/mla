(function( $ )
{
	var delay = (function()
	{
		var timer = 0;
		return function(callback, ms)
		{
			clearTimeout (timer);
			timer = setTimeout(callback, ms);
		};
	})();

	var methods = 
	{
		init : function( options )
		{
			var defaults =
			{
				filter : false,
				change : function(){},
				ready  : function(){}
			};
			methods.options = $.extend( defaults, options );
			
			methods._initEvents();
			methods._setFieldStates( methods.options.filter );
			
			methods.options.ready.call( this, methods.options.filter );
		},
		
		_setFieldStates : function()
		{
			// No cookie yet
			if( methods.options.filter == false )
			{
				methods.options.filter = methods._buildFilterObject();
			}
			
			// update states checkboxes
			$( '#state-filter input:checkbox' ).each( function()
			{
				$(this).attr( 'checked', methods._idInFilter( $(this).val(), methods.options.filter.states ) );
			});
			
			// update visibility checkboxes
			$( '#visibility-filter input:checkbox' ).each( function()
			{
				$(this).attr( 'checked', methods._idInFilter( $(this).val(), methods.options.filter.visibility ) );
			});
			
			// update additional form fields
			$( '#owner-filter' ).val( methods.options.filter.owner );
			$( '#name-filter' ).val( methods.options.filter.name );
			$( '#approve' ).attr( 'checked', methods.options.filter.approve );
		},
		
		_initEvents : function()
		{
			$( '#state-filter input:checkbox, #approve, #visibility-filter' ).change( function()
			{
				methods._change( this );
			});
			
			$( '#owner-filter, #name-filter' ).keyup( function()
			{
				delay( 
					function()
					{
						methods._change( this );
					},
					700
				);
			});
		},

		_change : function( context )
		{
			var filter = methods._buildFilterObject();
			
			methods.options.change.call( context, filter );
		},
		
		_buildFilterObject : function()
		{
			var filter =
			{
				states     : new Array(),
				visibility : new Array(),
				owner      : null,
				name       : null,
				approve    : false
			};
			
			// object states
			$( '#state-filter input:checked' ).each( function()
			{
				filter.states.push( $(this).val() );
			});
			// visibility
			$( '#visibility-filter input:checked' ).each( function()
			{
				filter.visibility.push( $(this).val() );
			});
			
			filter.owner   = $( '#owner-filter' ).val();
			filter.name    = $( '#name-filter' ).val();
			filter.approve = $( '#approve:checked' ).length > 0;
			
			return filter;
		},

		_idInFilter : function( id, filterValues )
		{
			var returnVal = false;
			
			for( i = 0; i < filterValues.length; i++ )
			{
				if( filterValues[ i ] == id )
				{
					returnVal = true;
					i = filterValues.length;
				}
			}
			
			return returnVal;
		},
		
		destroy : function()
		{
		}
	};

	$.fn.approvalFilter = function( method )
	{
		// Method calling logic
		if ( methods[method] )
		{
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		}
		else if ( typeof method === 'object' || ! method )
		{
			return methods.init.apply( this, arguments );
		}
		else
		{
			$.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
		}
	};
}( jQuery ) );