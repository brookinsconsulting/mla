(function( $ )
{
	//Not fast but should be a solution for subset
	Array.prototype.subsetOf = function(array)
	{
		returnVal = true;
		
		for( var i = 0; i < array.length; i++ )
		{
			if( this.indexOf( array[ i ] ) == -1 )
			{
				returnVal = false;
			}
		}
		
		return returnVal;
	};
	
	var methods = 
	{
		init : function( options )
		{
			// build data for each <td>
			this.find( 'tbody tr' ).each( function()
			{
				$(this).find( 'td.name' ).each( function()
				{
					var value = $.trim( $(this).find( 'a' ).html() );
					$.data( this, 'content', value );
				});

				$(this).find( 'td.owner' ).each( function()
				{
					var value = $.trim( $(this).find( 'div' ).html() );
					$.data( this, 'content', value );
				});

				$(this).find( 'td.states, td.visibility' ).each( function()
				{
					var value = new Array();
					
					$(this).find( 'span' ).each( function()
					{
						value.push( $(this).attr( 'rel' ) );
					});
					
					$.data( this, 'content', value );
				});

				$(this).find( 'td.approve' ).each( function()
				{
					var value = $(this).find( 'a.defaultbutton' ).length > 0;
					$.data( this, 'content', value );
				});

			});
			
		},
		
		filterList : function( filterOptions )
		{
			this.find( 'tbody tr' ).each( function()
			{
				if( methods._matchesFilter( filterOptions, this ) )
				{	
					$(this).fadeIn(methods._updateAppliedFilterCount);
				}
				else
				{		
					$(this).fadeOut(methods._updateAppliedFilterCount);
				}
			});
		},
		
		_matchesFilter : function( filterOptions, row )
		{
			returnVal = true;
			
			if( filterOptions.states )
			{
				$( row ).find( 'td.states' ).each( function()
				{
					var rowStates = $.data( this , 'content' );
					
					returnVal = filterOptions.states.subsetOf( rowStates );
				});
			}

			if( returnVal && filterOptions.visibility )
			{
				$( row ).find( 'td.visibility' ).each( function()
				{
					var rowVisibility = $.data( this , 'content' );
					
					returnVal = filterOptions.visibility.subsetOf( rowVisibility );
				});
			}

			if( returnVal && filterOptions.owner )
			{
				$( row ).find( 'td.owner' ).each( function()
				{
					var rowOwner = $.data( this , 'content' );
					
					var regEx = new RegExp( filterOptions.owner, 'i' );
					returnVal = rowOwner.match( regEx );
				});
			}

			if( returnVal && filterOptions.name )
			{
				$( row ).find( 'td.name' ).each( function()
				{
					var rowName = $.data( this , 'content' );
					
					var regEx = new RegExp( filterOptions.name, 'i' );
					returnVal = rowName.match( regEx );
				});
			}

			if( returnVal && filterOptions.approve )
			{
				$( row ).find( 'td.approve' ).each( function()
				{
					var data = $.data( this , 'content' );
					
					returnVal = data == filterOptions.approve;
				});
			}

			return returnVal;
		},
		
		_updateAppliedFilterCount: function ()
		{
			$('#f_count').html( 'Showing ' + $('#approvallist tbody tr:visible').length + ' of ' + $('#approvallist tbody tr').length ); 
		},
		
		destroy : function()
		{
		}
	};

	$.fn.approvalList = function( method )
	{
		// Method calling logic
		if ( methods[method] )
		{
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		}
		else if ( typeof method === 'object' || ! method )
		{
			return methods.init.apply( this, arguments );
		}
		else
		{
			$.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
		}
	};
}( jQuery ) );