$(document).ready(function()
{
	var cellClassName = false;
	$('#approval-decisiontable-page td').hover
	(
			function()
			{
					cellClassName = $(this).attr("class");
					
					cellClassNames = cellClassName.split( ' ' );
					
					$(cellClassNames).each( function()
					{
						$("." + this).addClass("hover");
					});
			},
			function()
			{
				cellClassNames = cellClassName.split( ' ' );
				
				$(cellClassNames).each( function()
				{
					$("." + this).removeClass("hover");
				});
			}
	);
	
	$( 'select' ).change( function()
	{
		$( '#select-workflow' ).submit();
	});
});
