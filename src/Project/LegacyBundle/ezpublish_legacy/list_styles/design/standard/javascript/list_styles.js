// update OL tags to use start attribute
function updateEditorOl()
{
    jQuery(".mceIframeContainer iframe").contents().find("ol[customattributes]").each(function() {
        customattributes = $(this).attr("customattributes").split("|");
        for (var i = 0; i < customattributes.length; i++) {
            if (customattributes[i] == "start_number") {
                $(this).attr("start", parseInt(customattributes[i + 1]));
                break;
            }
        }
    });
}

var editorOlUpdateTimer;
function updateEditorOlInterval()
{
    clearTimeout(editorOlUpdateTimer);
    editorOlUpdateTimer = setTimeout(function() {
        updateEditorOlInterval();
    }, 1000);
    updateEditorOl();
}

jQuery(document).ready(function() {
    updateEditorOlInterval();
});