<?php /* #?ini charset="utf-8"?

[ol]
AvailableClasses[]
AvailableClasses[]=upper-roman
AvailableClasses[]=lower-roman
AvailableClasses[]=upper-alpha
AvailableClasses[]=lower-alpha
AvailableClasses[]=numeric
CustomAttributes[]
CustomAttributes[]=start_number
CustomAttributesDefaults[start_number]=1

[ul]
AvailableClasses[]
AvailableClasses[]=disc
AvailableClasses[]=circle
AvailableClasses[]=square
AvailableClasses[]=no-bullet
CustomAttributes[]
