<?php

set_time_limit(0);
ini_set('memory_limit', -1);

date_default_timezone_set('America/New_York');

require 'autoload.php';

$script = eZScript::instance(
	array(
		'description' => ''
	)
);

// set up the environment
$access = array(
	'name' => 'site_admin',
	'type' => eZSiteAccess::TYPE_DEFAULT,
	'uri_part' => array()
);

eZSiteAccess::change($access);
eZSiteAccess::reInitialise();

$cli = eZCLI::instance();

$user = eZUser::fetch(65);
$user->loginCurrent();

$db = eZDB::instance();

$content_root = 8030;

$bulletins = eZContentObjectTreeNode::subTreeByNodeID(
	array(
		'ClassFilterArray' => array('bulletin_article'),
		'ClassFilterType' => 'include'
	),
	$content_root
);


foreach ($bulletins as $bk =>$b)
{
	
	$parent_node_id = $b->attribute('parent_node_id');
	$dm = $b->dataMap();
	$issn = $dm['issn']->content();
	$issue_title = $dm['issue_title']->content();
	
		
	$check = "SELECT * FROM ezcontentobject_attribute WHERE contentclassattribute_id = 432 and data_text = '$issue_title'";
	print_r($check."\r\n");
	$rows = $db->arrayQuery($check);
	
	if (count($rows) > 0) {
	    print_r($rows);
	    continue;
	}
	
	$attributes = array('issn' => $issn, 'issue_title' => $issue_title);

	print_r(array(
		'parent_node_id' => $parent_node_id,
		'class_identifier' => 'bulletin_issue',
		'creator_id' => 65,
		'attributes' => $attributes
	));

	$newObject = eZContentFunctions::createAndPublishObject(
		array(
			'parent_node_id' => $parent_node_id,
			'class_identifier' => 'bulletin_issue',
			'creator_id' => 65,
			'attributes' => $attributes
		)
	);

}

die();

if (!file_exists($storage_dir . $myname))  {
	$getfile = file_get_contents($getme);
	if ($getfile) {
		file_put_contents($storage_dir . $myname, $getfile);
	} else {
		print_r("Could not grab asset!\n");
		continue;
	}
}

$attributesData = array();
$attributesData['name'] = $myname;
$attributesData['original_url'] = $abs_path;
$attributesData['file'] = $myname;

?>