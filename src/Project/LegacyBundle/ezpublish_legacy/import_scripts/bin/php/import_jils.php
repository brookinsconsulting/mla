<?php

set_time_limit(0);
ini_set('memory_limit', -1);

date_default_timezone_set('America/New_York');

require 'autoload.php';

$script = eZScript::instance(
	array(
		'description' => ''
	)
);

// set up the environment
$access = array(
	'name' => 'site_admin',
	'type' => eZSiteAccess::TYPE_DEFAULT,
	'uri_part' => array()
);

eZSiteAccess::change($access);
eZSiteAccess::reInitialise();

$cli = eZCLI::instance();

$user = eZUser::fetch(65);
$user->loginCurrent();

$db = eZDB::instance();

$content_root = 10418;

$storage_dir = "var/storage/original/image/";

$headings = array("PUB","EDITION","YEAR","MONTH","DISPLAY","FILE");

$new_headings = array();
foreach ($headings as $k => $h) {
	$new_h = preg_replace("/[^A-Za-z0-9]/", "_", $h);
	$new_headings[] = strtolower($new_h);
}

$file_array = file('extension/import_scripts/bin/php/JIL_Archive_Metadata_Complete.csv');

foreach ($file_array as $line_number =>$line)
{
	preg_match('/"[^"]*"/', $line, $matches);
	
	foreach ($matches as $m) {
		$line = str_replace($m, str_replace(",", "qqq", $m), $line);
	}
	
	$line = str_replace(",", '[tab]', $line);
	$line = str_replace("qqq", ',', $line);
	$line = str_replace('"', '', $line);

	$data_r = explode("[tab]", trim($line));
	
	$attributes = array();
	foreach($new_headings as $k => $h) {
		if ($h != "_" && array_key_exists($k, $data_r)) {
			$thisdata = $data_r[$k];
			if ($h == 'created' || $h == 'last_edited') {
				$thisdata = strtotime($thisdata);
			}
			$attributes[$h] = $thisdata;
		}
	}
	
	$myname = $attributes['file'];
	$getme = "extension/import_scripts/bin/php/jil/archive/$myname";
	
	if (!file_exists($storage_dir . $myname))  {
    	$getfile = file_get_contents($getme);
    	if ($getfile) {
    	    print_r("putting $getme\r\n");
    		file_put_contents($storage_dir . $myname, $getfile);
    	} else {
    		print_r("Could not grab asset!\n");
    		continue;
    	}
    }
    
    $attributes['pub'] = str_replace("_", '', $attributes['pub']);
	
	print_r(".");

	
	$newObject = eZContentFunctions::createAndPublishObject(
		array(
			'parent_node_id' => $content_root,
			'storage_dir' => $storage_dir,
			'class_identifier' => 'jil_pdf',
			'creator_id' => 65,
			'attributes' => $attributes
		)
	);
	
}

?>