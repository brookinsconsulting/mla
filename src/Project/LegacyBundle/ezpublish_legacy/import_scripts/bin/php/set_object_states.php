<?php

set_time_limit(0);
ini_set('memory_limit', -1);

date_default_timezone_set('America/New_York');

require 'autoload.php';

$script = eZScript::instance(
	array(
		'description' => ''
	)
);

// set up the environment
$access = array(
	'name' => 'site_admin',
	'type' => eZSiteAccess::TYPE_DEFAULT,
	'uri_part' => array()
);

eZSiteAccess::change($access);
eZSiteAccess::reInitialise();

$cli = eZCLI::instance();

$user = eZUser::fetch(65);
$user->loginCurrent();

$db = eZDB::instance();

$content_root = 1;

$q = 'SELECT contentobject_id  FROM ezcontentobject, ezcontentobject_attribute WHERE ezcontentobject.id = ezcontentobject_attribute.contentobject_id AND version = current_version AND contentclassattribute_id =437 AND data_text = "P"';

$rows = $db->arrayQuery($q);

foreach ($rows as $row) {
    
    print_r($row['contentobject_id']);
    
    $db->query("insert into ezcobj_state_link VALUES (".$row['contentobject_id'].", 7)");
    
}


die();

