<?php

set_time_limit(0);
ini_set('memory_limit', -1);

date_default_timezone_set('America/New_York');

require 'autoload.php';

$script = eZScript::instance(
	array(
		'description' => ''
	)
);

// set up the environment
$access = array(
	'name' => 'site_admin',
	'type' => eZSiteAccess::TYPE_DEFAULT,
	'uri_part' => array()
);

eZSiteAccess::change($access);
eZSiteAccess::reInitialise();

$cli = eZCLI::instance();

$user = eZUser::fetch(65);
$user->loginCurrent();

$db = eZDB::instance();

$content_root = 93;

$headings = array("ID","Parent Section","Page Title","Last edited", "new_attribute5", "new_attribute6", "file_name", "new_attribute8", "new_attribute9", "new_attribute10", "new_attribute11", "new_attribute12", "new_attribute13", "new_attribute14", "new_attribute15", "new_attribute16", "new_attribute17", "new_attribute18", "new_attribute19", "new_attribute20", "new_attribute21");

$new_headings = array();
foreach ($headings as $k => $h) {
	$new_h = preg_replace("/[^A-Za-z0-9]/", "_", $h);
	$new_headings[] = strtolower($new_h);
}


$file_array = file('extension/import_scripts/bin/php/sections.data');

foreach ($file_array as $line_number =>$line)
{
	preg_match('/"[^"]*"/', $line, $matches);
	
	foreach ($matches as $m) {
		$line = str_replace($m, str_replace(",", "qqq", $m), $line);
	}
	
	$line = str_replace(",", '[tab]', $line);
	$line = str_replace("qqq", ',', $line);
	$line = str_replace('"', '', $line);
	
	$data_r = explode("|", trim($line));
	
	$attributes = array();
	foreach($new_headings as $k => $h) {
		if ($h != "_" && array_key_exists($k, $data_r)) {
			$thisdata = $data_r[$k];
			if ($h == 'created' || $h == 'last_edit') {
				$thisdata = strtotime($thisdata);
			}
			$attributes[$h] = $thisdata;
		}
	}
	
	$newObject = eZContentFunctions::createAndPublishObject(
		array(
			'parent_node_id' => $content_root,
			'class_identifier' => 'imported_section',
			'creator_id' => 65,
			'attributes' => $attributes
		)
	);
	
	
}

?>