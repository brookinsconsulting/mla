<?php

set_time_limit(0);
ini_set('memory_limit', -1);

date_default_timezone_set('America/New_York');

require 'autoload.php';

$script = eZScript::instance(
	array(
		'description' => ''
	)
);

// set up the environment
$access = array(
	'name' => 'site_admin',
	'type' => eZSiteAccess::TYPE_DEFAULT,
	'uri_part' => array()
);

eZSiteAccess::change($access);
eZSiteAccess::reInitialise();

$cli = eZCLI::instance();

$user = eZUser::fetch(65);
$user->loginCurrent();

$db = eZDB::instance();

$content_root = 2;
$file_root = 43;
$image_root = 43;
$storage_dir = "var/storage/original/image/";
$domain = 'http://www.mla.org';
$done_ids = array();

$imports = eZContentObjectTreeNode::subTreeByNodeID(
	array(
		'ClassFilterArray' => array('imported_page'),
		'ClassFilterType' => 'include'
	),
	$content_root
);

//$imports = array(eZContentObjectTreeNode::fetch(1307));


$count = count($imports);

foreach ($imports as $node_k => $node) {
    
    $this_node_id = $node->attribute('node_id');
    
    print_r("processing $this_node_id - ". ($node_k + 1) ." of $count\r\n");
    
    $dm = $node->dataMap();
    
    $content = $dm['body']->content();

	$xml = $content->XMLData;
	
	//if (strpos($dm['original_html']->content(), "<@") === false) continue;
	
	preg_match_all('/url_id="([0-9]+)">/', $xml, $matches);

	if (count($matches) > 1) {

		foreach ($matches[1] as $url_id) {
		    
		    if (in_array($url_id, $done_ids)) continue;
		    
		    $query = "SELECT * from ezurl where id = $url_id";
			$db_results = $db->arrayQuery($query);

			if (count($db_results) == 0) {
				print_r("url table error for id $url_id on ".$node->attribute('main_node_id')."\n");
				continue;
			}
						 
			if (strpos($db_results[0]['url'], '@') !== false) {
				print_r("Skipping email:\n");
				continue;
			}
			
			if (strpos($db_results[0]['url'], "http") === 0 && strpos($db_results[0]['url'], $domain) === false ) {
				print_r("skipping non-local asset ".$db_results[0]['url']."\n");
				continue;
			}
			
			
			$abs_path = absolutize($db_results[0]['url'], $domain, '/');
			
			$local_filename = str_replace($domain, "", $abs_path);
			
			if (strpos($local_filename, '/store/') !== false) continue;
			
			$local_filename_r = explode("/", $local_filename);
			$local_filename = array_pop($local_filename_r);
			$local_filename = trim($local_filename, '/');
			

			if (strpos($abs_path, 'http') !== 0 || 1 == 1) {
				
				print_r("Checking $local_filename\n");
			
				$check_already_imported = "SELECT main_node_id, path_string from ezcontentobject_attribute, ezcontentobject_tree, ezcontentobject where ezcontentobject.id = ezcontentobject_attribute.contentobject_id and ezcontentobject.current_version = ezcontentobject_attribute.version and ezcontentobject_tree.contentobject_id = ezcontentobject_attribute.contentobject_id AND (path_string like '%/43/%' OR path_string like '%/2/%') and contentclassattribute_id in (325) and data_text = '".addslashes($local_filename)."'";

				$db_results = $db->arrayQuery($check_already_imported);
		
				if (count($db_results) > 1) {
					print_r($db_results);
				} elseif (count($db_results) == 0) {

					$getme = str_replace("./", "/", (strpos($abs_path, 'http') === 0) ? $abs_path: $domain.$abs_path);

					$myname_r = array_reverse(explode("/", $abs_path));
					$myname = $myname_r[0];
					
					if (strpos($myname, '.html') > 0) {
						print_r("Missing HTML page $getme\n");
						continue;
					} 
					
					list($mysize, $mytype) = retrieve_remote_file_size($getme);
					
					$im_test = true;
					
					if ($mysize == -1) {
						print_r("File does not exist $getme, $mysize\n");
						continue;
					}
					
					if (strpos($mytype,"application/") === false && strpos($mytype,"video/") === false && strpos($mytype,"audio/") === false) {
						
						if (strpos($mytype,"image/") !== false) {
							$im_test = false;
							
							$imname = array_shift(explode(".", $myname));
							$attributesData = array();
							$attributesData['name'] = $imname;
							$attributesData['original_url'] = $abs_path;
							$attributesData['image'] = $myname;
                            print_r($attributesData);
							print_r("make new image!\r\n");

							if ($newObject === false) {
								print_r("Image creation failed! $abs_path");
								continue;
							}
							
						} else {
							print_r("Skipping $mytype content $getme\n");
							continue;
						}
						
					}
					
					if ($mysize > 1000000 && $im_test) {
                                                print_r("Import large asset - $getme, $mysize\n");
					}

					if ($im_test) { 
					
						print_r("Import asset - $getme, $mysize\n");
					
					}

				} else {
					print_r("update url $url_id to node ".$db_results[0]['main_node_id']."\n");
					fix_db_url_to_node($url_id, $db_results[0]['main_node_id'], $db);
					$done_ids[] = $url_id;
				}
				
			}
				
		}
		
	}
	
	
}



















function ats_to_params($atstring) {
	preg_match_all('/=(\s*)?"[^"]*"/', $atstring, $morematches);
	foreach($morematches[0] as $mm) {
		$atstring = str_replace($mm, $mm."||", $atstring);
	}
	$params_r = explode("||",$atstring);
	$params = array();
	foreach($params_r as $p) {
		$split = explode("=", $p);
		$key_proc = explode(" ", $split[0]);
		$key = array_pop($key_proc);
		if (count($split) > 1 && $split[0] != '') $params[strtoupper($key)] = trim($split[1], '"/>');
	}
	
	return $params;
}

function absolutize($url, $domain, $cur_path) {
    $url = str_replace('IMAGES/', 'images/docstudio/', $url);
	$url_p = parse_url($url);
	$url = (isset($url_p['path'])) ? $url_p['path'] : "";
	$pre = '';
	if (isset($url_p['host'])) $pre = $url_p['host'];
	if (isset($url_p['scheme'])) $pre = $url_p['scheme']."://".$pre;
	$url = $pre.$url;
	if (strpos($url, 'http') === 0) return $url;
	if (strpos($url, '/') !== 0) {
		$url_r = explode("/", $cur_path."/".$url);
		$fin_r = array();
		foreach ($url_r as $u) {
			if ($u == "..") {
				$trash = array_pop($fin_r);
			} elseif ($u != "." && $u != "") {
				$fin_r[] = $u;
			}
		}
		$url = "/" . implode("/", $fin_r);
	}
	return $url;
}

function pub_remote_asset($myname, $getme, $storage_dir, $storage_node, $attributesData, $class_identifier) {
	if (!file_exists($storage_dir . $myname))  {
		$getfile = file_get_contents($getme);
		if ($getfile) {
			file_put_contents($storage_dir . $myname, $getfile);
		} else {
			print_r("Could not grab asset!\n");
			return false;
		}
	}


	$newObject = eZContentFunctions::createAndPublishObject(
		array(
			'parent_node_id' => $storage_node,
			'class_identifier' => 'image',
			'creator_id' => 65,
			'storage_dir' => $storage_dir,
			'attributes' => $attributesData
		)
	);
	
	return $newObject;
}

function retrieve_remote_file_size($url){ 
     $ch = curl_init($url); 

     curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
     curl_setopt($ch, CURLOPT_HEADER, TRUE); 
     curl_setopt($ch, CURLOPT_NOBODY, TRUE); 

     $data = curl_exec($ch); 
     $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD); 
	 $type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE); 

     curl_close($ch); 
     return array($size, $type); 
}

function fix_db_url_to_node($url_id, $node_id, $db) {
	$likeme = '%url_id="'.$url_id.'">%';
	$findme = 'url_id="'.$url_id.'">';
	$newtext = 'node_id="'.$node_id.'">';
	$fixquery = "UPDATE ezcontentobject_attribute, ezcontentobject  set data_text = replace(data_text, '$findme', '$newtext') WHERE ezcontentobject.id = ezcontentobject_attribute.contentobject_id and contentclassattribute_id = 436 and version = current_version and data_text LIKE '$likeme'";
	$fix = $db->Query($fixquery);
	return true;
}



?>