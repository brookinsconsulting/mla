<?php

set_time_limit(0);
ini_set('memory_limit', -1);

date_default_timezone_set('America/New_York');

require 'autoload.php';

$script = eZScript::instance(
	array(
		'description' => ''
	)
);

// set up the environment
$access = array(
	'name' => 'site_admin',
	'type' => eZSiteAccess::TYPE_DEFAULT,
	'uri_part' => array()
);

eZSiteAccess::change($access);
eZSiteAccess::reInitialise();

$cli = eZCLI::instance();

$user = eZUser::fetch(65);
$user->loginCurrent();

$db = eZDB::instance();

$content_root = 93;

$headings = array("ID","File name","Parent Section","Page title","Description","Keywords","Authors","Created","Last edited","Access level","Dynamic elements","Sort order","Convert blank lines","Allow targeted elements","?","Include in sitemap","?","Template","Display in pop-up","Portal link path","?","last edited?","?","portal link flag","Alt title string","Related links","CSS","File size","?","File type","Right Col HTML","Include share widget","Include comments","Comments by","?","?");

$new_headings = array();
foreach ($headings as $k => $h) {
	$new_h = preg_replace("/[^A-Za-z0-9]/", "_", $h);
	$new_headings[] = strtolower($new_h);
}


$file_array = file('extension/import_scripts/bin/php/pages2.data');

$skip = true;

foreach ($file_array as $line_number =>$line)
{
	preg_match('/"[^"]*"/', $line, $matches);
	
	foreach ($matches as $m) {
		$line = str_replace($m, str_replace(",", "qqq", $m), $line);
	}
	
	$line = str_replace(",", '[tab]', $line);
	$line = str_replace("qqq", ',', $line);
	$line = str_replace('"', '', $line);
	
	$data_r = explode("|", trim($line));
	
	$attributes = array();
	foreach($new_headings as $k => $h) {
		if ($h != "_" && array_key_exists($k, $data_r)) {
			$thisdata = $data_r[$k];
			if ($h == 'created' || $h == 'last_edited') {
				$thisdata = strtotime($thisdata);
			}
			$attributes[$h] = $thisdata;
		}
	}
	
	if ((int)$attributes['id'] == 1809) $skip = false;
	if ($skip) continue;
	
	if (!isset($attributes['file_name'])) {
		print_r("skipping row $line_number, ID = ".$attributes['id']."\n");
		continue;
	}
	
	$db = eZDB::instance();
	$q = "select * from ezcontentobject_attribute where data_text = '".$attributes['id']."' and contentclassattribute_id = 324";
	$rows = $db->arrayQuery($q);
	if (count($rows) == 0) {
	    print_r($attributes['id']." is missing!!!\r\n");
	    continue;
	} else {
	    
	    $ex_ob = eZContentObject::fetch($rows[0]['contentobject_id']);
	    print_r($attributes);
	    
	    eZContentFunctions::updateAndPublishObject(
			$ex_ob,
			array( 'attributes'=> $attributes)
		);
		
	    continue;
	}
	

	
	$newObject = eZContentFunctions::createAndPublishObject(
		array(
			'parent_node_id' => $content_root,
			'class_identifier' => 'imported_page',
			'creator_id' => 65,
			'attributes' => $attributes
		)
	);

	print_r($attributes);
	
}

?>