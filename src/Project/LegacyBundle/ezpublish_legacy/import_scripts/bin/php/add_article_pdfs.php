<?php

set_time_limit(0);
ini_set('memory_limit', -1);

date_default_timezone_set('America/New_York');

require 'autoload.php';

$script = eZScript::instance(
	array(
		'description' => ''
	)
);

// set up the environment
$access = array(
	'name' => 'site_admin',
	'type' => eZSiteAccess::TYPE_DEFAULT,
	'uri_part' => array()
);

eZSiteAccess::change($access);
eZSiteAccess::reInitialise();

$cli = eZCLI::instance();

$user = eZUser::fetch(65);
$user->loginCurrent();

$db = eZDB::instance();

$content_root = 5719;

$bulletins = eZContentObjectTreeNode::subTreeByNodeID(
	array(
		'ClassFilterArray' => array('bulletin_article'),
		'ClassFilterType' => 'include'
	),
	$content_root
);

$storage_dir = "var/storage/original/image/";


foreach ($bulletins as $bk =>$b)
{
    
	$node_id = $b->attribute('node_id');
    
    print_r("processing $node_id \r\n");
    
    if ($node_id==5722) continue;
    
    $ex_ob = $b->object();

	$dm = $b->dataMap();
	
	$myname = $dm['article_link']->content();
	$issue_volume = $dm['issue_volume']->content();
	
	$getme = "extension/import_scripts/bin/php/ade_pdfs/ade/$issue_volume/$myname";
	print_r("getting $getme\r\n");
	
	
	if (!file_exists($storage_dir . $myname))  {
    	$getfile = file_get_contents($getme);
    	if ($getfile) {
    	    print_r("putting $getme\r\n");
    		file_put_contents($storage_dir . $myname, $getfile);
    	} else {
    		print_r("Could not grab asset!\n");
    		continue;
    	}
    }
    
    print_r("updating $node_id \r\n");

    $attributesData = array();
    $attributesData['file'] = $myname;
    
    eZContentFunctions::updateAndPublishObject(
		$ex_ob,
		array( 'attributes'=> $attributesData,
		       'storage_dir' => $storage_dir
		)
	);

}

die();



?>