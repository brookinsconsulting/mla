<?php

set_time_limit(0);
ini_set('memory_limit', -1);

date_default_timezone_set('America/New_York');

require 'autoload.php';

$script = eZScript::instance(
	array(
		'description' => ''
	)
);

// set up the environment
$access = array(
	'name' => 'site_admin',
	'type' => eZSiteAccess::TYPE_DEFAULT,
	'uri_part' => array()
);

eZSiteAccess::change($access);
eZSiteAccess::reInitialise();

$cli = eZCLI::instance();

$user = eZUser::fetch(65);
$user->loginCurrent();

$db = eZDB::instance();

$content_root = 1;

$q = 'SELECT *  FROM ezcontentobject_tree, ezcontentobject, ezcontentobject_attribute WHERE ezcontentobject_tree.contentobject_id = ezcontentobject.id and ezcontentobject.id = ezcontentobject_attribute.contentobject_id AND version = current_version AND contentclassattribute_id =384 AND data_text LIKE "<a href=\'http://mla.ssdstage.thinkcreativeinternal.net/content/download/%"';
$rows = $db->arrayQuery($q);

foreach ($rows as $row) {
    
    $from_node = $row['node_id'];
    preg_match("/content\/download\/([0-9]*)/", $row['data_text'], $matches);
    
    $new_ob_id = (int)$matches[1];
    $ob = eZContentObject::fetch($new_ob_id);
    $to_node = $ob->mainNodeID();
    print_r("$from_node -- $to_node"."\r\n");
    
    $findme = 'node_id="'.$from_node.'">';
    $replaceme = 'node_id="'.$from_node.'">';
    
    $q = 'select * from ezcontentobject, ezcontentobject_attribute where ezcontentobject.id = ezcontentobject_attribute.contentobject_id AND version = current_version and contentclassattribute_id = 436 and data_text like \'%'.$findme.'%\'';
    $morerows = $db->arrayQuery($q);
    if (count($morerows) == 0) {
        print_r("no links to this one!!!"."\r\n");
        continue;
    } else {
        foreach ($morerows as $mr) {
            
            $fn = "select * from ezcontentobject_tree where node_id = $from_node";
            $n_data = $db->arrayQuery($fn);
            $from_object = $mr['contentobject_id'];
            $old_link = $n_data[0]['contentobject_id'];

            $uplink = "update ezcontentobject_link set to_contentobject_id =  $new_ob_id where from_contentobject_id = $from_object and to_contentobject_id = $old_link";
            $db->query($uplink);
            
            $upq = 'update ezcontentobject_attribute set data_text = REPLACE( data_text, \''.$findme.'\', \''.$replaceme.'\' )   where id = '.$mr['id'];
            $db->query($q);
            print_r($uplink."\r\n");
            print_r($upq."\r\n");
            
        }
        
    }
    
}


die();

$count = count($imports);

foreach ($imports as $node_k => $node) {
	
	print_r("processing ".$node->attribute('node_id')." ($node_k of $count)\n");
	
	$pnid = $node->attribute('parent_node_id');
	
	$dm = $node->dataMap();
	
	$parent_section_orig = $dm['parent_section']->content();
	
	if ($parent_section_orig == '100000000000') continue;
	
	if ($parent_section_orig == 0) {
		$parent_section = 2509;
	} else {
		$q = "select main_node_id from ezcontentobject_tree, ezcontentobject, ezcontentobject_attribute where contentclassattribute_id in (324, 385) and version = current_version and ezcontentobject_tree.contentobject_id =  ezcontentobject.id and ezcontentobject.id  = ezcontentobject_attribute.contentobject_id and data_text = '$parent_section_orig'";

		$rows = $db->arrayQuery($q);

		if (count($rows) == 0) {
			print_r($q);
			print_r($rows);
			$parent_section = 2510;
		} else {
			$parent_section=$rows[0]['main_node_id'];
		}

	}
	
	$new_parent_node = eZContentObjectTreeNode::fetch($parent_section);

	
	if (is_object($new_parent_node) && $pnid != $new_parent_node->attribute('node_id')) {
		print_r("Moving...\n");
		if ( eZOperationHandler::operationIsAvailable( 'content_move' ) )
		{
		    $operationResult = eZOperationHandler::execute( 'content',
		                                                    'move', array( 'node_id'            => $node->attribute('node_id'),
		                                                                   'object_id'          => $node->attribute('contentobject_id'),
		                                                                   'new_parent_node_id' => $new_parent_node->attribute('node_id') ),
		                                                    null,
		                                                    true );
		}
		else
		{
		    eZContentOperationCollection::moveNode( $node->attribute('node_id'), $node->attribute('contentobject_id'), $new_parent_node->attribute('node_id') );
		}
	} else {
		if (is_object($new_parent_node)) {
			print_r("Staying...\n");
		} else {
			print_r("OOPS! -- problem with ".$node->attribute('node_id')."...\n");
		}
	}
	
}

?>