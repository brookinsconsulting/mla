#!/usr/bin/env php
<?php

set_time_limit(0);
ini_set('memory_limit', -1);

date_default_timezone_set('America/New_York');

require 'autoload.php';

$script = eZScript::instance(
	array(
		'description' => ''
	)
);

// set up the environment
$access = array(
	'name' => 'site_admin',
	'type' => eZSiteAccess::TYPE_DEFAULT,
	'uri_part' => array()
);
eZSiteAccess::change($access);
eZSiteAccess::reInitialise();

$cli = eZCLI::instance();

$db = eZDB::instance();

$user = eZUser::fetch(65);
$user->loginCurrent();

$file_array = file('extension/import_scripts/bin/php/killdata.csv');

foreach ($file_array as $line_number =>$line)
{
	preg_match('/"[^"]*"/', $line, $matches);
	
	foreach ($matches as $m) {
		$line = str_replace($m, str_replace(",", "qqq", $m), $line);
	}
	
	$line = str_replace(",", '[tab]', $line);
	$line = str_replace("qqq", ',', $line);
	$line = str_replace('"', '', $line);
	
	$data_r = explode("[tab]", trim($line));
	
	
	$q = "select * from ezcontentobject_attribute, ezcontentobject, ezcontentobject_tree where contentclassattribute_id = 324 and version = current_version and ezcontentobject_tree.contentobject_id = ezcontentobject.id and ezcontentobject_attribute.contentobject_id = ezcontentobject.id  and data_text = '".$data_r[1]."'";
	$rows = $db->arrayQuery($q);
	if (count($rows) == 0) continue;
	if ($data_r[0] == "X") {
	    print_r("killing ".$rows[0]['main_node_id']."\r\n");
	    eZContentObjectTreeNode::removeSubtrees( array( $rows[0]['main_node_id'] ), false );
	} elseif ($data_r[0] == "P") {
	    
        $ex_ob = eZContentObject::fetch($rows[0]['contentobject_id']);
        
        $parser = new eZOEInputParser();
    	$document = $parser->process( "*PLACEHOLDER*" );

    	$dataString = eZXMLTextType::domString( $document );
    	
		$attributes = array('status_flag' => $data_r[0], 'body' => $dataString);
    		
	    eZContentFunctions::updateAndPublishObject(
			$ex_ob,
			array( 'attributes'=> $attributes)
		);
		print_r("Processing ".$rows[0]['main_node_id']."\r\n");
	    
	} else {
	    print_r("Skipping ".$rows[0]['main_node_id']."\r\n");
	}
}

die();


foreach ($imports as $node_k => $node) {
	$dm = $node->dataMap();
	
	//if (strpos($node->Name, ".pdf")  !==false) {
		
		$orig_content = $dm['original_content']->content();
		$orig_url = $dm['original_url']->content();
		preg_match("/<title>([^<]*)<\/title>/i",$orig_content, $matches);
		if (count($matches)>1) {

			$title = $matches[1];
			//print_r($title."\n");
			if (strpos($title, "Bad Request") !== false || strpos($title, "Page Not Found") !== false || strpos($title, "Internal server error") !== false || strpos($orig_url, '/tabid/') !== false || strpos($orig_url, '/TabId/') !== false) {
				print_r("$title\n");

				$ob = $node->object();

				$db->begin();
				$ob->removeThis($node->attribute('node_id'));
				$db->commit();

			}

		}
		
	//}

}


$script->shutdown();


?>
