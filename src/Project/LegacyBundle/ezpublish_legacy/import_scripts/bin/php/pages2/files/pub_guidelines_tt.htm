<h1>Guidelines for the Series Texts and Translations</h1>
<h2>Goals of the Series</h2>
The series Texts and Translations was founded in 1991 to provide students and faculty members with important texts and high-quality translations that otherwise are not available at an affordable price. The books in the series are aimed at students in upper-level undergraduate and graduate courses—in national literatures in languages other than English, comparative literature, literature in translation, ethnic studies, area studies, and women's studies. The Publications Committee welcomes submissions that involve texts in any modern language or tradition taught in North America and especially from literatures that remain underrepresented in English translation.

<h2>Series Format</h2>
<ul>
	<li>The original-language text and an English translation are published simultaneously, usually in companion volumes.</li>
	<li>Plays, short novels, and collections of stories or poems are appropriate for the series. Nonfiction works will also be considered. To keep series publications affordable, the Publications Committee limits the length of each text to approximately 150 double-spaced manuscript pages (38,000 words). </li>
	<li>Each translation is based on an authoritative edition of the source text. For works of early periods, the editor of the original-language text is usually expected to modernize spelling.</li>
	<li>The critical apparatus is in English and is included only as appropriate for advanced undergraduates. The following are customarily provided: an introduction of approximately fifteen manuscript pages, which contains biographical information about the author or authors and a discussion of the work's major themes, pertinent literary and social contexts, and pedagogical issues; a short selected bibliography of other works of the author or authors (if relevant) and suggestions for further reading; a note on the text; and footnotes to the text only as helpful for instructional use. The translation volume also contains a translator's note that makes explicit what kind of translation is offered and why.</li></ul>
	
<h2>Prospectus</h2>
If you are interested in proposing a project or have questions about the series, please write the office of scholarly communication (<a href="mailto:scholcomm@mla.org">scholcomm@mla.org</a>) for more information. The review process has two stages.

In the first stage, a prospectus should be sent to the office of scholarly communication (<a href="mailto:scholcomm@mla.org">scholcomm@mla.org</a>). A prospectus includes the following elements: 
<ol>
	<li>A letter briefly describing the proposed text (its length, usefulness in the classroom, and availability in other editions and the status of its reprint and translation rights)</li>
	<li>Sample translations of five to ten pages with corresponding pages of the original text (if the text will be modernized, a five- to ten-page sample of the modernized text should accompany the original)</li>
	<li>A table of contents</li>
	<li>A CV of no more than two or three pages for each contributor</li>
	<li>A five-page, abbreviated version of the introduction that outlines how the project will address upper-level undergraduate and graduate students</li>
	<li>A preview of the translator's note, sketching the interpretation that the translation will inscribe in the foreign-language text and outlining any problems likely to be involved in the translation, as well as potential solutions for these problems</li></ol>
Prospectuses should be sent to
<blockquote>
Texts and Translations 
Office of Scholarly Communication
Modern Language Association
26 Broadway, 3rd floor
New York, NY 10004-1789
<a href="mailto":scholcomm@mla.org">scholcomm@mla.org</a>

<h2>Approval of Prospectus</h2>
If the prospectus indicates that the project meets the series format, it will be reviewed by consultant readers. If the response is generally positive, the staff will present the prospectus to the Publications Committee. If the committee approves the prospectus, the staff will invite the proposer(s) to submit a full manuscript for review.

<h2>Manuscript Preparation</h2>
If you are invited to submit a full manuscript, you should send the following items: 
<ol>
	<li>An introduction of approximately fifteen pages (not including the works-cited list) designed for advanced undergraduates, which should contain a short biography of the author or authors, provide historical and critical context for the material in the volume, and specify issues involved with teaching it</li>
	<li>A works-cited list for the introduction</li>
	<li>A bibliography of other works by the author or authors (if relevant) and of further critical readings</li>
	<li>A short note on the original-language text that explains its provenance and (if relevant) the approach taken toward any issues involved in editing or modernizing the material</li>
	<li>A legible, carefully proofread photocopy or transcribed version of the original work or works (modernized, if appropriate), with accompanying footnotes helpful for classroom use </li>
	<li>A translator's note that demonstrates awareness that a translation does not communicate the source text directly or without mediation but provides only one among a number of justifiable interpretations. The note should include the following:
	
<ul><li>an explanation of the interpretation that the translation is inscribing in the source text(s)</li>
	<li>a detailed account of the special issues and problems involved in translating the source text(s) </li>
	<li>specifics about how these challenges were addressed and why such an approach was taken</li>
	<li>clarification of how the translation differs from any previous English-language translations of the source text(s)</li></ul>
	Above all, the note should be scholarly, avoiding the scanty, impressionistic comments that often preface translations issued by commercial publishers.</li>
	<li>The critical apparatus should be in English. Everything should be double-spaced. At this point, you should demonstrate that the rights to reprint and translate are obtainable; the MLA staff will advise you on these matters as necessary.</li></ol>

<h2>Manuscript Review </h2>
The materials submitted will be evaluated by two or more consultant readers. The project will be judged on the quality of the original text and translation and on potential usefulness for North American classrooms. Issues of appropriateness and balance of the series will be considered as well. If the readers' reports are supportive, the project will be sent to the Publications Committee for review.

The Publications Committee will review the readers' reports and the manuscript. If the committee approves the manuscript, publication is authorized. 

The books follow series design, but editors and translators are usually consulted about ideas for special cover features. Editors and translators receive royalties and a number of complimentary copies of the publication.

<h2>Special Initiative in Arabic Texts and Translations</h2> 
The Publications Committee seeks to develop several new titles in Arabic for the series. To this end, the committee has appointed an editorial board of specialists in Arabic for 2009–14. Board members will identify projects for development and serve as consultant readers for all submissions in Arabic. During this time, the committee will continue to develop and review projects in other languages.
