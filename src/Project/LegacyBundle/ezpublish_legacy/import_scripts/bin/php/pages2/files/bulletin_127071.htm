<b>Number 127, Winter 2001</b>
<h1>The Tower and the Web: Emigr&eacute;s from English Lit Can Find Work in the Field of Online Information Architecture</h1>
<h2 class="byline">LIZ HINES KELLEHER</h2>
<blockquote>
</I>Thesis 89

We have real power and we know it. If you don't quite see the light, some other outfit will come along that's more attentive, more interesting, more fun to play with.

--<I>The Cluetrain Manifesto</I>
</blockquote>

I AM going to explain my presence in the simplest possible way. I am here to advise you that there are opportunities for people with your skills in Web publishing, specifically in areas of this field so new that they are still acquiring labels and job titles. In describing what I do and what I have observed of my peers working in this field, I want to get beyond what can be conveyed by such terms as <I>writer</I>, <I>editor</I>, <I>HTML coder</I>, <I>programmer</I>, <I>Web master</I>, <I>graphic designer</I>, <I>content provider</I>.

For starters, many people working on large Web sites hold more than one of these identities or have held more than one of these roles in their career to date, so it makes little sense to limit one's goals to one of these titles. More important, what you need to grasp about this slippery nomenclature is a broader concept that explains why the Web has an immediate, compelling need for people like you.

I therefore gave a title to this paper in the form of a headline, subject line, or posting. The title leads to two questions: What do I mean by "information architecture"? And what is this work that holds promise for graduate students and recent PhDs in English who choose to work outside academia?

To put it simply, information architecture, as a concept and a need, arises from the creation of large-scale, ongoing Web projects, a phenomenon that is both new and increasingly complex. Louis Rosenfeld and Peter Morville describe the concept in this way: an information architect is a needed component of a large Web team who plays these roles in the Web publication process:
<ul>
<li>Clarifies the mission and vision for the site, balancing the needs of its sponsoring organization and the needs of its audiences.
<li>Determines what content and functionality the site will contain.
<li>Specifies how users will find information in the site by defining its organization, navigation, labeling and searching systems.
<li>Maps out how the site will accommodate change and growth over time. (11)
</ul>

Nathan Shedroff, in his influential manifesto on Web architecture entitled <I>Information Interaction Design</I>, names what can only be called a widespread crisis within the present incarnation of the World Wide Web. He explains this crisis by distinguishing <I>data</I> from <I>information</I> from <I>knowledge</I>, a range of difference he calls "the continuum of understanding." Data

is the raw material we find or create that we use to build our communications. Unfortunately, most of what we experience is <I>merely</I> data. It is fairly easy to distinguish as it is often boring, incomplete, or inconsequential. Data isn't valuable as communication because it isn't a complete message. (4th p.)

Organizing data into actual information takes much more, of course: "Information makes data meaningful for audiences because it requires the creation of relationships and patterns among data," as Shedroff puts it (4th p.). Beyond information lies a higher and rarer skill still: the structuring of information into knowledge, into patterns and meanings that have context and that deliver insights and value to others.

The problem of recognizing and constructing knowledge, instead of streaming disorganized and meaningless data, has become the defining challenge in the enormous transfer of information to the Web. Alan Liu perceptively points out that just when the university has become most self-doubting and tormented about its claims and role in producing knowledge, the world of business and the professions has become intensively eager to understand that process of production.

To give a sense of how this description may match up with your own vision of work, I need to quote another fragment, this time from Richard Saul Wurman's visionary <I>Information Architects</I>:
<blockquote>
There is a tsunami of data that is crashing onto the beaches of the civilized world. This is a tidal wave of unrelated, growing data formed in bits and bytes, coming in an unorganized, uncontrolled, incoherent cacophony of foam. It's filled with flotsam and jetsam. It's filled with the sticks and bones and shells of inanimate life. None of it is easily related, none of it comes with any organizational methodology. [.&nbsp;.&nbsp;.]

As it washed up on our beaches, we see people in suits and ties skipping along the shoreline, men and women in fine shirts and blouses dressed for business. [.&nbsp;.&nbsp;.] All day, from morning at home, to workday lunches, to dinner at night, out loud or to themselves, they "uh-huh, uh-huh, uh-huh," making believe they understand a reference to a name, a reference to a fact, the references to knowledge that are supposed to make the world coherent. [.&nbsp;.&nbsp;.]

They "uh-huh" everybody because they were taught when they were young that it is not good to look stupid, that it is not good to say "I don't know," it is not good to ask questions, not good to focus on failure. Instead the rewards come from acknowledging or answering everything with "I know." You're supposed to look smart in our society. You are supposed to gain expertise and sell it as the means of moving ahead in your career. You are supposed to focus on what you know how to do, and then do it better and better. You are supposed to revel in some niche of ability. That is where the rewards are supposed to come from.

Of course, when you sell your expertise--and what I mean by sell is to move ahead in a corporation, or sell an idea to a publisher, or sell an ability to a client--when you sell your expertise, by definition, you're selling from a limited repertoire. However, when you sell your <I>ignorance</I> to move ahead, when you sell your desire to learn about something, when you sell your desire to create and explore and navigate paths to knowledge, when you sell your <I>curiosity</I>--you sell from a bucket with an infinitely deep bottom. (15-18)
</blockquote>

At the end of 1996, having spent six years struggling to become a professor of English, I left my doctoral program and went to work for a small educational consulting firm in Washington, DC, where to my surprise I soon found myself with the title of business services coordinator, with responsibility for the firm's business computer applications, including their databases, tiny local network, and newly hatched Web site. Eighteen months later, with some significant good fortune and the generous support of my first contacts in the field, I found myself hired by a large nonprofit organization as a contract Web project specialist, at work putting together a very large and ever-growing Web site that now contains more than a dozen major divisions, over a hundred topic areas, and several thousand documents.

My current position, once entitled online writer/editor, now bears the more opaque title interactive services coordinator. Neither reveals much about the nature of the work to anyone outside the field. These are among my functions: I edit several areas of the site, including a health and wellness center and a section that promotes new content; re-edit print pieces for the Web; write leads, headlines, and promotional copy; and do a lot of scheduling. Many of these tasks would be familiar to print editors and journalists.

But another role is less familiar: I also serve as a sort of master cross-referencer or cyber-librarian who tracks all the content on the site, its location and topic, and the relation of documents and topics to one another. In related work, I promote content (part of the job once known as marketing), develop concepts for new content, do a lot of project reporting and tracking (a.k.a.technical writing, or the ability to summarize in succinct, clear prose the contradictory goals and plans of a multidepartment working group), and help evaluate potential outside partners.

If I could describe the skill set that I and my peers use, I would need to speak about functions rather than absolutes. For example, functioning as editor of several areas of the site requires me to master enough coding to convert text into HTML (hypertext markup language), that is, to mark up text with a sense of how it will appear graphically on the screen. HTML itself requires skills very familiar to the teacher and scholar: minute proofreading, nuanced absorption of syntax and practice of style, an understanding of when the rules must be applied and where they are flexible or evolving.

If I pass beyond the day-to-day work to my experience of the construction of the site over a year, yet another area of necessary skills strikes me. This construction requires a grasp of numerous layers of structure: the structure of an individual page, of a feature, of a site; the relation of multiple servers and of drives and directories on the local server; up to the structure, at the highest level, of the Web itself.

An online editor also applies many of the skills used in print media feature writing, while building the new and evolving techniques of a coherent site strategy. Assembling the day's front page includes making choices about the confluence of information and Web page structure, mulling, for example, whether the topic of wellness should be placed above, below, or next to the topic of end-of-life care. Do users look first to the leftmost column or the right? Which links draw heavy traffic and which are ignored? Experience in the print press, in the tradition of lead writing and copyediting, is of great utility here, mixed with a willingness to embrace the new domain of Web usability.

A mastery of metatags and directory structure can be more important to Web publishing than the more glamourous work of creating multimedia. Proofreading, grammar, and punctuation skills, if accompanied by an ability to learn technology rapidly, can help you find a fast footing even without a formal technical background.

In many ways, this work differs wildly from scholarly experience, especially in the omnipresence of collaboration. Creation of Web objects is inherently shared. A typical moment of any day includes a simultaneous phone call with a researcher, instant message from the graphic designer, and e-mail to the Web master. Self-teaching is a constant. You must spend ungrudging hours in the evening and on weekends improving your skills and familiarizing yourself with new developments amid an overwhelming flood of information. The bonus is, as a doctoral student, or scholar, you already know how to do that.

From this brief description, I hope you begin to understand how applicable your skills are to this field of practice. In Borges's "Garden of the Forking Paths," the mystery that haunts Dr. Yu Tsun hinges on a confusion between a labyrinth that is a place and a labyrinth that is a text. Large Web sites and their multiple creators and contributors are persistently haunted by this same false distinction between material structure and information structure.

In our professions and business cultures, there is still a strong either-or legacy. On the one hand, we find traditional print writers, including scholars, editors, researchers, lawyers, administrators, and designers, who tend to understand text as knowledge but lack an understanding of the object structure of the Web. On the other hand, we find technicians who understand texts as objects, as files, but often lack a perceptive grasp of the contents (including meaning and purpose) of those files.

Hands-on Web site work truly is the realm of those who are able to understand and create the relations among objects and spaces, who can comprehend text data in two ways, as information or knowledge and simultaneously as object or space. The crying need for information architects, for potential organizers of heterogeneous data into meaningful information and knowledge, has hugely outpaced the pool of available people with both the technical and the intellectual skills to create Web nodes that are sites of useful meaning. The scholar of literature, as both teacher and writer, serves an audience as a guide and intermediary. The Web editor creates meaningful relations among information spaces on the Web. He or she, too, serves an audience as guide and intermediary.

No discussion of the Web would be constructive without confronting the fact that the Web is a place of risks, risks psychological, intellectual, and ethical. It's a field surrounded by inordinate waves of desire, jubilance, fear, loathing, creativity, destructiveness, greed, generosity, excitement, weariness, cynicism.

A potent object of desire, Web employment necessarily arouses strong anxieties among those working in other fields. Some people, hearing you are looking for or beginning a job in this realm, will instantly attempt to undermine your confidence and sense of possibility. They will offer you good reasons why you live in the wrong city, have the wrong background, or are certain to be swept away by younger competitors. They will tell you that you are already too late, that everybody else is looking in this field or that the Web as a phenomenon of richness is already over and all the spoils are gone. In the face of these anxiety attacks, you have yet another advantage--here, again, as a job candidate in English, you have been through all this already.

Balancing the risks, as of yet, is a tremendous sense of openness and flexibility that permeates most Web jobs. The Web is a field without credentialing. There is still a sort of Samuel Smiles feel to it all, of the office boy miraculously promoted to the big desk. When I moved from my first employer outside academia--a small, all-woman consulting firm--to a large, national, 1,800-person organization, I found myself in a position that would radically increase my responsibility and opportunity and that paid twice my previous salary. I went to work the day I was hired, because there was a desperate need for a writer to document the projects of a Web master who had quit suddenly.

There is, as well, tremendous generosity within the field--the success of my current job hinged on the patience and support of a Web master and technical leader who gave me the opportunity to build my skills in technical areas on the job. What I gave them in return was an unstated promise and practice--to learn fast, get it right, and use my writing, teaching, and communicating skills in ways that create a better Web site and allow the achievement of a new quality of information.

You may be asking yourself, What happens to my present field, my years of commitment to the problems, questions, and skills that have been my life as a scholar of English?

The first point to make about the job possibilities as a Web editor or information architect has to do with market realities. The tsunami of knowledge transfer--the creation of information and its translation to the Web--that Richard Wurman speaks of, is only just beginning. I am not talking for the moment about the get-rich dreams and the lotteries of IPOs. I am talking about the reality of a proliferation of skills-requiring, people-requiring jobs at extremely remunerative salaries and in largely rewarding conditions. You should not underestimate the profound restoration of creativity, confidence, and identity that can come from being treated with the respect, congeniality, and even gratitude of a field that is begging for talent rather than tossing it overboard.

The second point to make about the choice to leave academia for the Web is that your potential pain and loss are both vast and real. You must confront the fact that this regrafting will in all probability hurt a great deal. But your transfer to the new field does not have to be about an absolute loss or severance of your self as a scholar and writer. You will have to come up with your own models of integrating your past as a scholar with your present as a Web professional. Create whatever model you can fashion, preferably one that is amusing, for this rebuilding of meaning: fleeing Huguenot, apostate cleric, all-purpose Professor on a corporate Gilligan's isle. In truth you will be an &eacute;migr&eacute;, who experiences certain irremediable losses but who carries into a new field the learning and the insights gained from years of study and persistent sacrifice in pursuit of a valued art.

The third point to make about this passage from the Ivory Tower to the Web has to do with the undetermined future. Eric Raymond, author of the open-source manifesto <I>The Cathedral and the Bazaar</I>, has argued that the Web is already creating new and unimagined practices of knowledge creation--such as the collaborative authoring, critiquing, and debugging of open-source computer programs by thousands of hackers working to improve the same free program (Raymond, sec. 10). His theory directly challenges the concept of scholarship as best practiced by single, precious, costly experts, protected by quiet walls throughout the long struggle toward original creation, replacing that ancient ideal--the Cathedral, the Ivory Tower--with the Web as bazaar, characterized by noisy, collaborative, simultaneous, and rapidly distributed knowledge work where feedback and constant new releases allow true advances to spread with unprecedented rapidity--and discard mediocrity with equal swiftness.

The possibilities exist that new forms of learning and scholarship, new academic positions or opportunities will be created--that you will help create them--inside or outside traditional universities, possibilities that we cannot imagine right now. They will reflect a revolution that has happened in the last seven years on the Web. They will react against the wasteful system that has damaged so many lives in academia. There will exist new creative projects and contributions to knowledge that will reshape the university of the future. Leaving the university now and acquiring the technical and practical skills to contribute to such projects are highly risky. There's not a lot of probability you'll be welcomed back through the gates. But staying inside the walls is not safe either.

And here's where what Wurman says really comes home. Are you willing to break perhaps the greatest taboo of your struggles in academia? Are you willing to offer your infinite ignorance instead of your limited expertise, your deep curiosity instead of your singular credential, to shed the shames that an embittered profession has laid on you and to say, "I <I>don't</I> know--so let me begin"?


<I>The author is Interactive Services Coordinator at AARP Services, Inc., in Washington, DC. A version of this paper was presented at the 1999 MLA Annual Convention in Chicago.</i>
<a name="citations"> </a>
<h2>Works Cited</h2>
Borges, Jorge. "Garden of the Forking Paths." <cite>Ficciones</cite>. New York: Grove, 1962.

Levine, Rick, Christopher Locke, Doc Searls, and David Weinberger. <cite>The Cluetrain Manifesto</cite>. 1999. 2 Nov. 2000 
http://www.cluetrain.com.

Liu, Alan. "Knowledge in the Age of Knowledge Work." <cite>Profession 1999</cite>. New York: MLA, 1999. 113-24.

Raymond, Eric S. <cite>The Cathedral and the Bazaar</cite>. 1997. 2 Nov. 2000 
http://www.tuxedo.org/~esr/writings/cathedral-bazaar/cathedral-bazaar/x305.html.

Rosenfeld, Louis, and Peter Morville. <cite>Information Architecture for the World Wide Web</cite>. Cambridge: O'Reilly, 1998.

Shedroff, Nathan. <cite>Information Interaction Design: A Unified Field Theory of Design</cite>. 1994. 2 Nov. 2000 
http://www.nathan.com/thoughts/unified.

Wurman, Richard Saul. <cite>Information Architects</cite>. New York: Graphis, 1997.



&#169; 2001 by the Association of Departments of English. All Rights Reserved.
