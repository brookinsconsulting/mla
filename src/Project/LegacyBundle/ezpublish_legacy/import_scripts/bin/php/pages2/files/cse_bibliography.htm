<h1>Annotated Bibliography: Key Works in the Theory of Textual Editing</h1>
<b>Commissioned by the Committee on Scholarly Editions
Compiled by Dirk Van Hulle, University of Antwerp, Belgium</b>

B&eacute;dier, Joseph. "La tradition manuscrite du <i>Lai de l'Ombre</i>: r&eacute;flexions sur l'art d'&eacute;diter les anciens textes." <i>Romania</i> 54 (1928): 161-196, 321-356.
<blockquote>
B&eacute;dier advocates best-text conservatism and rejects the subjectivity of Lachmann's method (see Maas) with its emphasis on the lost authorial text, resulting remarkably often in two-branch stemmata. Instead, B&eacute;dier focuses on manuscripts and scribes, reducing the role of editorial judgment.
</blockquote>

Blecua, Alberto. "Defending Neolachmannianism: On the <i>Palacio</i> Manuscript of <i>La Celestina</i>." <i>Variants</i>. Eds. Peter Robinson and H.T.M. Van Vliet. Turnhout: Brepols, 2002. 113-33.
<blockquote>
A clear position statement by the author of the noteworthy Spanish <i>Manual de Critica Textual</i> (1983) in defence of the neolachmannian method. Blecua argues that stemmatic analysis is superior to the methods based on material bibliography and that only the construction of a stemma can detect the presence of contaminated texts.
</blockquote>

Bornstein, George, and Ralph G. Williams, eds. <i>Palimpsest: Editorial Theory in the Humanities</i>. Ann Arbor: University of Michigan Press, 1993.
<blockquote>
On the assumption that texts are not as stable or fixed as we tend to think they are, these essays examine the palimpsestic quality of texts, emphasizing the contingencies both of their historical circumstances of production, and of their reconstruction in the present. They mark a theoretical period of transition, shifting the focus from product to process in editorial theory and practice.
</blockquote>

Bowers, Fredson. "Some Principles for Scholarly Editions of Nineteenth-Century American Authors." <i>Studies in Bibliography</i> 17 (1964): 223-8.
<blockquote>
Concise and systematic elaboration of W.W. Greg's theories, arguing that "when an author's manuscript is preserved," this document rather than the first edition has paramount authority and should serve as copy-text. Bowers' principles for the application of analytical bibliography in an eclectic method of editing, have been most influential in Anglo-American scholarly editing.
</blockquote>

Cohen, Philip, ed. <i>Devils and Angels: Textual Editing and Literary Theory</i>. Charlottesville: University Press of Virginia, 1991.
<blockquote>
The "increasingly theoretical self-consciousness" characterizing textual criticism and scholarly editing marks an impasse, indicative of a paradigm shift. Assumptions that have been self-evident for several decades are rethought in eight stimulating essays and three responses.
</blockquote>

de Biasi, Pierre-Marc. "What is a Literary Draft? Toward a Functional Typology of Genetic Documentation." <i>Yale French Studies 89: Drafts</i> (1996): 26-58.
<blockquote>
In a continuous effort to present manuscript analysis and <i>critique g&eacute;n&eacute;tique</i> as a scientific approach to literature, de Biasi designs a typology of genetic documentation, starting from the <i>bon tirer</i> moment ("all set for printing") as the dividing line between the <i>texte</i> and what precedes it, the so-called <i>avant-texte</i>.
</blockquote>

Eggert, Paul. "Textual Product or Textual Process: Procedures and Assumptions of Critical Editing." <i>Editing in Australia</i>. Ed. Paul Eggert. Canberra: University College ADFA, 1990. 19-40.
<blockquote>
Starting from a comparison with new techniques of X-raying paintings, Eggert proposes a valuable ideal for a critical edition that allows the reader to study both the writing process and the finished product.
</blockquote>

Ferrer, Daniel. "Production, Invention, and Reproduction: Genetic vs. Textual Criticism." <i>Reimagining Textuality: Textual Studies in the Late Age of Print</i>. Eds. Elizabeth Bergmann Loizeaux and Neil Fraistat. Madison: University of Wisconsin Press, 2002. 48-59.
<blockquote>
Ferrer defines the difference between genetic and textual criticism on the basis of their respective focus on invention and repetition. He pleads for a hypertextual presentation as the best way to do justice to the diverse aspects of the writing process.
</blockquote>

Finneran, Richard J., ed. <i>The Literary Text in the Digital Age</i>. Editorial Theory and Literary Criticism. Ann Arbor: University of Michigan Press, 1996.
<blockquote>
The availability of digital technology coincides with a fundamental paradigm shift in textual theory, away from the idea of a "definitive edition." Fifteen contributions reflect on the shift toward an enhanced attention to nonverbal elements and the integrity of discrete versions.
</blockquote>

Gabler, Hans Walter, George Bornstein, and Gillian Borland Pierce, eds. <i>Contemporary German Editorial Theory</i>. Ann Arbor: The University of Michigan Press, 1995.
<blockquote>
With its representative choice of position statements, this thorough introduction to major trends in German editorial theory in the second half of the twentieth century marks the relatively recent efforts to establish contact between German and Anglo-American editorial traditions.
</blockquote>

Gabler, Hans Walter. "The Synchrony and Diachrony of Texts: Practice and Theory of the Critical Edition of James Joyce's <i>Ulysses</i>." TEXT 1 (1981): 305-26.
<blockquote>
The work's "total text," comprising all its authorial textual states, is conceived as a diachronous structure that correlates different synchronous structures. A published text is only one such synchronous structure and not necessarily a privileged one.
</blockquote>

Gaskell, Philip. <i>From Writer to Reader: Studies in Editorial Method</i>. Oxford: Clarendon Press, 1978.
<blockquote>
In 1972, with <i>A New Introduction to Bibliography</i> replacing McKerrow's, Gaskell had already criticized Greg's copy-text theory, arguing that authors often expect their publishers to correct accidentals. <i>From Writer to Reader</i> zooms in on the act of publication and the supposed acceptance of the textual modifications this may involve.
</blockquote>

Greetham, David C. <i>Textual Scholarship: An Introduction</i>. New York and London: Garland, 1992.
<blockquote>
Impressive survey of various textual approaches: finding, making, describing, evaluating, reading, criticizing, and finally editing the text, i.e. biblio-, paleo- and typography, textual criticism and scholarly editing. The book contains an extensive bibliography, organized per discipline.
</blockquote>

Greetham, David C., ed. <i>Scholarly Editing: A Guide to Research</i>. New York: The Modern Language Association of America, 1995.
<blockquote>
The most comprehensive survey of current scholarly editing of various kinds of literatures, both historically and geographically, with elucidating contributions by textual scholars from different traditions.
</blockquote>

Greg, W. W. "The Rationale of Copy-Text." <i>Studies in Bibliography</i> 3 (1950-1): 19-37.
<blockquote>
This pivotal essay has had an unparallelled influence on Anglo-American scholarly editing in the twentieth century. Greg proposes a distinction between substantive readings (which change the meaning of the text) and accidentals (spelling, punctuation, etc.). He pleads for more editorial judgment and eclectic editing, against "the fallacy of the 'best text'" and "the tyranny of the copy-text," contending that the copy-text should be followed only so far as accidentals are concerned, but that it does not govern in the matter of substantive readings.
</blockquote>

Gr&eacute;sillon, Almuth. <i>El&eacute;ments de critique g&eacute;n&eacute;tique: Lire les manuscrits modernes</i>. Paris: Presses universitaires de France, 1994.
<blockquote>
Introduction to textual genetics or <i>critique g&eacute;n&eacute;tique</i>, which was developed in the 1970s and became a major field of research in France. In spite of correspondences with textual criticism, it sees itself as a form of literary criticism, giving primacy to interpretation over editing.
</blockquote>

Groden, Michael. "Contemporary Textual and Literary Theory." <i>Representing Modernist Texts: Editing as Interpretation</i>. Ed. George Bornstein. Ann Arbor: University of Michigan Press, 1991: 259-86.
<blockquote>
Important plea for more contact between textual and literary theorists by the general editor of The <i>James Joyce Archive</i> facsimile edition of Joyce's works.
</blockquote>

Hay, Louis. "Pass&eacute; et avenir de l'&eacute;dition g&eacute;n&eacute;tique: quelques r&eacute;flexions d'un usager." <i>Cahier de textologie 2</i> (1988): 5-22; trans. "Genetic Editing, Past and Future: A Few Reflections of a User." <i>TEXT</i> 3 (1987): 117-33.
<blockquote>
Genetic editing, presenting the reader with a "work in progress," is a new trend, but it revives an old tradition. The founder of the Paris institute for modern texts and manuscripts (ITEM-CNRS) points out that editing has always reflected the main ideological and cultural concerns of its day.
</blockquote>

Maas, Paul. <i>Einleitung in die Altertumswissenschaft</i> vol. 2: "Textkritik." Leipzig & Berlin: Teubner, 1927; Textual Criticism. Trans. Barbara Flower. Oxford: Clarendon, 1958.
<blockquote>
One of Karl Lachmann's main disciples, Maas systematizes Lachmannian stemmatics, requiring thorough scrutiny of witnesses (<i>recensio</i>) before emending errors and corruptions (<i>emendatio</i>, often involving a third step of divination or <i>divinatio</i>).
</blockquote>

Martens, Gunter and Hans Zeller, eds. <i>Texte und Varianten: Probleme ihrer Edition und Interpretation</i>. Munich: Beck, 1971.
<blockquote>
Epoch-making collection of German essays with important contributions by, amongst others, Hans Zeller (pairing "record" and "interpretation," allowing readers to verify the editor's decisions), Siegfried Scheibe (on fundamental principles for historical-critical editing), and Gunter Martens (on textual dynamics and editing). The collection's central statement is that the apparatus, not the reading text, constitutes the core of scholarly editions.
</blockquote>

McGann, Jerome J. <i>Critique of Modern Textual Criticism</i>. Chicago: University of Chicago Press, 1983. Repr. Charlottesville: University Press of Virginia, 1992.
<blockquote>
Textual criticism does not have to be restricted to authorial changes, but may also include the study of posthumous changes by publishers or other agents. McGann sees the text as a social construct and draws attention to the cooperation involved in the production of literary works.
</blockquote>

McGann, Jerome J. <i>The Textual Condition</i>. Princeton: Princeton University Press, 1991.
<blockquote>
McGann makes several valuable and innovating suggestions, from the idea of a "continuous production text" to a clear distinction between a text's bibliographical and linguistic codes (in the important essay "What Is Critical Editing?").
</blockquote>

McGann, Jerome J. "The Rationale of HyperText." <<a href="http://www2.iath.virginia.edu/public/jjm2f/rationale.html" target="_blank">http://www2.iath.virginia.edu/public/jjm2f/rationale.html</a>>; repr. <i>TEXT</i> 9 (1996): 11-32; repr. <i>Electronic Text: Investigations in Method and Theory</i>. Ed. Kathryn Sutherland. Oxford: Clarendon Press, 1997. 19-46; repr. radiant textuality: literature after the world wide web. New York: Palgrave, 2001. 53-74.
<blockquote>
Conceived in an expressly revisionist relation to Greg's rationale, McGann's ambitious essay presents the book as a machine of knowledge and evaluates the advantages of hyperediting and hypermedia over editions in codex form. As the earliest hypertextual structure the library organization illustrates the theoretical design of a "decentered text".
</blockquote>

McKenzie, D.F. <i>Bibliography and the Sociology of the Text: The Panizzi Lectures 1985</i>. London: The British Library, 1986.
<blockquote>
McKenzie extends the scope of traditional bibliography to a broader sociology of the text, including videogames, movies, and even landscapes. This perspective has been a major stimulus to the advancement of the sociological orientation in scholarly editing.
</blockquote>

McKerrow, R. B. <i>An Introduction to Bibliography for Literary Students</i>. Oxford: Oxford University Press, 1927.
<blockquote>
McKerrow's manual of "new bibliography" reflects the early twentieth-century editorial method which made extensive use of analytical bibliography. The author of <i>Prolegomena for the Oxford Shakespeare</i> was rather averse to the idea of emending the copy-text from other sources.
</blockquote>

Nutt-Kofoth, Rodiger, Bodo Plachta, H.T.M. van Vliet, and Hermann Zwerschina, eds. <i>Text und Edition: Positionen und Perspektiven</i>. Berlin: Erich Schmidt, 2000.
<blockquote>
As a younger generation's counterpart of <i>Texte und Varianten</i> (see Martens), this state of the art of current scholarly editing in Germany also includes interesting survey articles on Anglo-American scholarly editing (Peter Shillingsburg) and "genetic criticism and philology" (Geert Lernout; trans. <i>TEXT</i> 14 [2002]: 53-75).
</blockquote>

Parker, Hershel. <i>Flawed Texts and Verbal Icons: Literary Authority in American Fiction</i>. Evanston: Nortwestern University Press, 1984.
<blockquote>
Starting from analyses of revisions by Melville, Twain, Crane, and Mailer, Parker pleads for more attention to textual composition and the development of (sometimes self-contradictory) authorial intentions, which an institutionalized editorial method is often unable to represent.
</blockquote>

Pasquali, Giorgio. <i>Storia della tradizione e critica del testo</i>. Florence: Le Monnier, 1934.
<blockquote>
Pasquali criticizes some of the basic Lachmannian principles and proposes to take the history of the witnesses and the scribes into account. The current emphasis on textual tradition in Italian philology is to a large extent his legacy.
</blockquote>

Pizer, Donald. "Self-Censorship and Textual Editing." <i>Textual Criticism and Literary Interpretation</i>. Ed. Jerome J. McGann. Chicago: University of Chicago Press, 1985. 144-61.
<blockquote>
Pizer emphasizes the social aspects of texts, arguing that even if an author has personally changed his texts under external pressure, it may be more important to present the reader with the censored versions because of their social resonance.
</blockquote>

Reiman, Donald H. "'Versioning': The Presentation of Multiple Texts." <i>Romantic Texts and Contexts</i>. Columbia: University of Missouri Press, 1987. 167-80.
<blockquote>
Reiman suggests "versioning" (or multiversional representation) as an alternative to ""editing. The main purpose of this textual approach is to offer readers and critics the opportunity to figure out for themselves how the work evolved.
</blockquote>

Robinson, Peter M. W. "The One Text and the Many Texts." <i>Making Texts for the Next Century</i>. Special Issue of <i>Literary & Linguistic Computing</i> 15.1 (2000): 5-14
<blockquote>
In answer to the question "Is There a Text in These Variants?" which Robinson asked in a previous essay, he argues that a scholarly edition is more than merely presenting an archive of variants. The aim of the editor should be to offer a useful tool so as to allow readers to make the connection between variation and meaning. A critically edited text (presented along with "the many texts") is the best means to that end.
</blockquote>

Shillingsburg Peter. <i>Scholarly Editing in the Computer Age: Theory and Practice</i>. 3rd edition. Ann Arbor: University of Michigan Press, 1996.
<blockquote>
Indispensable introduction to practical procedures and controversial issues in editorial theory, offering clear definitions in matters of textual ontology and a survey of different orientations in scholarly editing.
</blockquote>

Shillingsburg, Peter. <i>Resisting Texts: Authority and Submission in Constructions of Meaning</i>. Ann Arbor: University of Michigan Press, 1997.
<blockquote>
The editor's main task, Shillingsburg argues, is to relate the work to the documents and to take responsibility for the integrity of the agency of texts, which is a responsibility to both the author <i>and</i> the social contract. Shillingsburg designs a map with four major forms of textual concern, placing the physical documents at the center of textual and literary theory.
</blockquote>

Stillinger, Jack. <i>Multiple Authorship and the Myth of the Author in Criticism and Textual Theory</i>. New York: Oxford University Press, 1991.
<blockquote>
Stillinger pleads for a broader conception of authorship to include collaboration as an inherent aspect of creation. Case studies include John Stuart Mill and his wife, Keats and his helpers, Wordsworth revising earlier versions of his texts.
</blockquote>

Tanselle, G. Thomas. "The Editorial Problem of Final Authorial Intention." <i>Studies in Bibliography</i> 29 (1976): 167-211.
<blockquote>
An author's revisions do not automatically reflect his final intentions. In the case of <i>Typee</i>, Herman Melville was responsible for the changes in the second edition, but they represent his "acquiescence" rather than his intention, according to Tanselle, who is well aware that a reader does not have access to an author's mind and advises to always take the context into account.
</blockquote>

Tanselle, G. Thomas. <i>A Rationale of Textual Criticism</i>. Philadelphia: University of Pennsylvania Press, 1989.
<blockquote>
In his profound analysis of the ontology of texts Tanselle makes a clear distinction "between work" and "text." A work is an entity that exists in no single historical document. Scholarly editing entails, just like any act of reading, the effort to discover the work that "lies behind" the text(s) one is presented with.
</blockquote>

Thorpe, James. <i>Principles of Textual Criticism</i>. San Marino: Huntington Library, 1972.
<blockquote>
As an early critic of the principles advocated by Greg and Bowers, Thorpe argues that specific compositional peculiarities and contingencies tend to be left out of consideration.
</blockquote>

Timpanaro, Sebastiano. <i>La genesi del metodo del Lachmann</i>. Florence: Le Monnier, 1963. Rev. ed. Padua: Liviana, 1985.
<blockquote>
The genealogical study of manuscript transmission originated in New Testament criticism toward the end of the eighteenth century. By reexamining B&eacute;dier's criticism regarding two-branch stemmata, Timpanaro does not so much aim to correct them but to understand how they came into being.
</blockquote>

Zeller, Hans. "A New Approach to the Critical Constitution of Literary Texts." <i>Studies in Bibliography</i> 28 (1975): 231-63.
<blockquote>
In his evaluation of Anglo-American copy-text theory from a structuralist point of view Zeller contrasts the practice of editing an "eclectic (contaminated) text" with German editorial methods, showing crucial differences with respect to the notions of "authority," "authorial intention," and "version."
</blockquote>
