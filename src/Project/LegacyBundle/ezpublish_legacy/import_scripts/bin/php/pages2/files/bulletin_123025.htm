<b>Number 123, Fall 1999</b>
<h1>Joining the Circus: Leaving a Tenure-Track Position</h1>
<h2 class="byline">ANN C. HALL</h2>
GIVEN the nature of the current job market, leaving a tenure-track position for a job in the arts is probably not a decision many would make and is very similar to joining the circus. Resources for the arts are scarce, programming is always under attack, the hours are long, and someone always has to sweep up afterward. With this said, however, my experiences as a dramaturge at a smaller, professional midwestern theater were invaluable. This job offered me what very few others outside academia could--the opportunity to do research, teach, and even publish.

Why did I decide to join the circus in the first place? I wish I could say that I left academia as the result of a dramatic event, like an offer to produce one of my plays on Broadway or the sudden realization that my creative genius was being stifled by the academic life, but such was not the case. After successfully completing my PhD in English, with a drama specialty, at Ohio State University, I chose to take a position as an assistant professor at Marquette University in Milwaukee, Wisconsin. Here, my classes were relatively small; my course load reflected my area of specialty; I taught graduate-level courses; and my colleagues were personable and intellectually invigorating. In a word, it was one of those jobs we dream about in grad school as we grade the hundredth freshman paper comparing the differences between poodles and Dobermans.

My personal life, however, was on hold. And while being on hold had its benefits--a renewed interest in the lost art of letter writing, for example--I craved the "dull routine of existence" with my Columbus, Ohio, partner. More important, we both wanted to have children, and though I could see myself riding the rails indefinitely, I could not envision negotiating the wilds of the Chicago outerbelt with kids and their attendant paraphernalia in tow.

Fortunately, the theater my husband was working for--the reason he could not leave Columbus--had received a large grant from the Ohio Arts Council to fund educational programming. Since I had helped the theater with educational activities in the past as a volunteer and since I had my PhD in English, the board of the Contemporary American Theatre (CATCO) hired me as the theater's education director and dramaturge.

Part of the reason for the combined title was that very few people knew what a dramaturge was--and many theater audiences still do not know what a dramaturge is. They do not, because it is a relatively new position in American theaters (see Cattaneo [3]; Copelin). New does not mean to imply that the dramaturge is adding something entirely new to the theatrical process. Anne Cattaneo, for example, documents dramaturgical activity in the eighteenth and nineteenth centuries, particularly in Germany (3-6). But the institutionalization of dramaturgical activities is recent.

What exactly are these activities? Simply put, the dramaturge's role is closely linked to education. The theater historian Oscar Brockett writes, "One of dramaturgy's primary goals is to promote integration of the knowledge and perception learned from theatre history, dramatic literature, and theory with the skills and expertise needed to realize the potential of a particular script in a particular production in a particular time and place for a particular audience" (42). So, dramaturges spend their days much as English teachers do, researching a play's history, cultural context, author (with the author's other dramatic works), interpretations, and thematic issues. Once the research is complete, they present this background information through study guides, program notes, production notes, and oral presentations to the cast and crew, which frequently leads to vigorous discussion of the play. These activities shape the performance by influencing the many decisions necessary to move the play from page to stage.

When, for example, I worked on a production of Jeffrey Hatcher's stage version of Henry James's <I>Turn of the Screw</I>, I provided the cast and crew with information on James; information on the various versions and interpretations of <I>Turn of the Screw</I>; a detailed analysis of the changes that Hatcher made to the James story; information on the James family, particularly Alice; information on the period of American and British culture in which the work was written; and explanations for the botanical references in the play.

Once the rehearsal process begins, the dramaturge serves as an in-house theater critic, recommending changes and offering alternative interpretations, definitions of unusual words, and even recipes. During CATCO's production of Truman Capote's <I>A Christmas Memory</I>, I discovered Aunt Sook's all-important fruitcake recipe, which we decided to reprint in the program as well as bake and distribute as part of the production.

In addition to providing information to the cast and crew, dramaturges frequently participate in postshow discussions or talkbacks in order to provide the audience with further information to make their experience of the play richer. At CATCO, I also led theater and playwriting workshops in prisons, recreation centers, nursing homes, churches, and synagogues. Audiences ranged in age from 2 to 92, and the level of interest in the workshops ran from highly enthusiastic to "Could you show me how to make it look like my thumb came off my hand?" Like many other dramaturges, I also worked on script development, read and responded to new plays, and offered play suggestions for the year's season.

What was so exciting and fulfilling about these duties was the collaboration with my colleagues. I did not wait two months for a reader to respond anonymously to my interpretations, frequently with vituperative vigor; my readings were discussed while they were still fresh in my mind, and even though we had disagreements, we had a common goal: a successful production, not an individual's successful career. Working with befuddled audience members--on Harold Pinter's <I>Birthday Party</I>, for example--always gave me a greater appreciation for a play's power and appeal. In a word, the job of dramaturge was intellectually and socially stimulating, akin to those great classes in which all have done their homework.

Many theaters do not offer dramaturges such an active role in the production and decision-making process. Dramaturges there are viewed with the same suspicion with which nurse practitioners are often viewed by doctors. The dramaturge is seen as an interloper, and some theaters even limit the dramaturge's role to that of glorified research assistant or script reader.

Another caution about the profession involves questions of turf. Recently, more colleges and universities have begun to offer training specific to dramaturges, usually through theater departments. According to the Literary Managers and Dramaturgs of America Web page, there are approximately forty-five dramaturgical programs across the country, so some theaters may wish to hire credentialed dramaturges.<A NAME="notefirst"><A HREF="#note1"><SUP>1</SUP></A> Some theaters are openly hostile to candidates with training in literature. Robert Brustein, the theater critic and founding director of the Yale Repertory Theatre, views the relation between English and theater departments as carnivorously antagonistic (33).<A NAME="notesecond"><A HREF="#note2"><SUP>2</SUP></A> While such antagonism probably does not exist in smaller theaters, it is important to remember that "a knowledge of the plays as literature is not sufficient. A knowledge of theater history is equally important. The production dramaturg must also consider how the plays were produced and published in their own time, how the theatre functioned, and who acted in the play" (Catteneo 7).

A final caution concerns small professional theaters. At CATCO no job was too small for anyone in the company. One time I attended a swanky luncheon with the mayor of Columbus, to lobby for his support of our educational activities; later that day, after I had changed my clothes, I was elected to clean the theater's bathrooms in preparation for the opening night festivities. Such is life in the arts.

Today I have returned to academia, lucky enough to have found another dream job, at Ohio Dominican College, this time in the same city as my husband and two children. Here, of course, I can fulfill my first love, teaching, but my experiences as a dramaturge have enhanced my skills as a teacher, colleague, and researcher. I continue to serve as a dramaturge on one play a year at CATCO, savoring the chance to participate in the practical application of close textual analysis and the opportunity to share my enthusiasm for literature and drama with nonstudents and nonacademics.


<I>The author is Associate Professor of English and Chair of the English Division at Ohio Dominican College. This paper was presented at the 1998 MLA Annual Convention in San Francisco.</I>

<h2>Notes</h2>
<A NAME="note1"> </A><A HREF="#notefirst"><SUP>1</SUP></A>See <I>Dramaturgy Programs</I> as well as <I>Literary Managers and Dramaturgs of America: Dramaturgy Northwest</I>. In addition to providing information about programs in dramaturgy, the second Web page provides a great deal of useful information about dramaturgy in general and the opportunity to join the organization.

<A NAME="note2"> </A><A HREF="#notesecond"><SUP>2</SUP></A>Such a response from Brustein may not be surprising, given his dismissal from the Yale Repertory Theatre by then-president A. Bartlett Giamatti, an English professor. Brustein details these events in his autobiography in the chapter entitled "The Year of the Ax: 1977-78" (247-84).
<a name="citations"> </a>
<h2>Works Cited</h2>
Brockett, Oscar. "Dramaturgy in Education." Jonas, Proehl, and Lupu 42-47.

Brustein, Robert. <CITE>Making Scenes: A Personal History of the Turbulent Years at Yale, 1966-1979</CITE>. New York: Random, 1981.

Catteneo, Anne. "Dramaturgy: An Overview." Jonas, Proehl, and Lupu 3-15.

Copelin, David. <CITE>Quotes</CITE>. <CITE>LMDA Review</CITE> (1989): 5. Proehl. <http://www.ups.edu/professionalorgs/dramaturgy/nwquotes.htm>.

<CITE>Dramaturgy Programs</CITE>. Proehl. <http://www.ups.edu/professionalorgs/dramaturgy/programs.htm>.

Jonas, Susan, Geoff Proehl, and Michael Lupu, eds. <CITE>Dramaturgy in American Theatre: A Sourcebook</CITE>. New York: Harcourt, 1997.

Proehl, Geoff, ed. <CITE>Literary Managers and Dramaturgs of America: Dramaturgy Northwest</CITE>. U of Puget Sound. 30 Oct. 1998. <http://www.ups.edu/professionalorgs/dramaturgy/home.htm#dramaturgy>.


&#169; 1999 by the Association of Departments of English. All Rights Reserved.
