<h1>Statement of Professional Ethics</h1>
<H2>Preamble</h2>
As a community of teachers and scholars, the members of the MLA serve the larger society by promoting the study and teaching of the modern languages and literatures.  In order to embrace this enterprise, we require freedom of inquiry.  However, this freedom carries with it the responsibilities of professional conduct.  We intend this statement to embody reasonable norms for ethical conduct in teaching and learning as well as in scholarship.  The statement's governing premises are as follows:

1. The responsibility for protecting free inquiry lies first with tenured faculty members, who may be called on to speak out against the unethical behavior or defend the academic freedom of colleagues at any rank. In addition, faculty members have ethical obligations to students, colleagues, and staff members; to their institutions, their local communities, the profession at large, and society.<sup><a href="#one">1</a></sup> 

2. Our integrity as teachers and scholars requires the responsible use of evidence in developing arguments and fairness in hearing and reading the arguments of both colleagues and students.

3. As a community valuing free inquiry, we must be able to rely on the integrity and the good judgment of our members.  For this reason, we should not
<ul>
	<li>exploit or discriminate against others on grounds such as race, ethnicity, national origin, religious creed, age, gender, sexual preference, or disability</li>
	<li>sexually harass students, colleagues, or staff members </li>
	<li>use language that is prejudicial or gratuitously derogatory</li>
	<li>make capricious or arbitrary decisions affecting working conditions, professional status, or academic freedom</li>
	<li>misuse confidential information</li>
	<li>plagiarize the work of others<sup><a href="#two">2</a></sup> </li>
	<li>practice deceit or fraud on the academic community or the public</li>
</ul>
4. Free inquiry respects variety in the modes and objects of investigation, whether traditional or innovative.  We should defend scholarly practices against unfounded attacks from within or outside our community. 

5. Our teaching and inquiry must respect our own cultures and the cultures we study. 

6. Judgments of whether a line of inquiry is ultimately useful to students, colleagues, or society should not be used to limit the freedom of scholars to pursue their research. 

<h2>Ethical Conduct in Teaching and Learning</h2>
1. Teachers should represent to their students the values of free inquiry.

2. At the outset of each course, teachers should provide students with a statement on approaches to the course materials, on the goals of the course, and on the standards by which students will be evaluated.

3. Teachers should offer constructive and timely evaluation of students' work and specify the times and places when teachers are available to consult with students.

4. Teacher-student collaboration entails the same obligation as other kinds of research.  Teachers and students should acknowledge appropriately any intellectual indebtedness.

5. Teachers whose research in any way includes students as subjects must make clear the obligations, rewards, and consequences of participation.<sup><a href="#three">3</a></sup> 

6. Teachers, in devising requirements for written work and oral discussion, have an ethical responsibility to respect both students' privacy and their emotional and intellectual dignity.

7. Teachers should keep confidential what they know about students' academic standing, personal lives, and political or religious views and should not exploit such personal knowledge.<sup><a href="#four">4</a></sup>  

8. Teachers must provide unbiased, professional evaluation of students seeking admission to graduate study or applying for financial support.

9. Teachers should provide direction to students, especially graduate students; should respect their scholarly interests; and should not exploit them for personal or professional ends.  Teachers should not expect students, graduate or otherwise, to perform unremunerated or uncredited teaching, research, or personal duties.

10. Teachers working with teaching assistants have a special responsibility to provide them with adequate preparation, continuing guidance, and informed evaluation.

11. Teachers must weigh the academic performance of each student on its merits.

12. In overseeing and responding to the work of graduate students, whether they are in courses or at the thesis or dissertation stage, advisers should periodically inform them of their standing in the program.

13. Before graduate students begin searching for jobs, advisers and teachers should provide them with adequate and timely counseling and should be prepared to write honest and constructive letters of recommendation.  Advisers or teachers who doubt their ability to evaluate a student fairly should decline the task of furnishing such a letter.

<h2>Ethical Conduct in Service and Scholarship</h2>
1. Scholars in positions of leadership should assist their institutions in devising and implementing policies and procedures that promote a positive working and learning environment.  

2. A scholar who borrows from the works and ideas of others, including those of students, should acknowledge the debt, whether or not the sources are published. Unpublished scholarly material -- which may be encountered when it is read aloud, circulated in manuscript, or discussed -- is especially vulnerable to unacknowledged appropriation, since the lack of a printed text makes originality hard to establish.

3. Scholars should ensure that their personal activities in politics and in their local communities remain distinct from positions taken by their universities or colleges.  They should avoid appearing to speak for their institutions when acting privately. 

4. As referees for presses, journals, and promotion and tenure committees, scholars should judge the work of others fully, fairly, and in an informed way.  A scholar who has any conflict of interest or is so out of sympathy with the author, topic, or critical stance of a work as to be unable to judge its merits without prejudice must decline to serve as a referee or reviewer.  A scholar with a personal relationship that prevents an unbiased evaluation should turn down an invitation to serve.

5. Referees should discharge their tasks in a timely manner; they should decline invitations whose deadlines they cannot meet. Undue delay in review or publication justifies submission to another outlet, provided the first editor is informed.

6. Members of review committees must keep confidential all information about individuals, departments, or programs under evaluation. 

7. Faculty members planning to resign an appointment should give timely, written notice of this intention in accordance with institutional regulations.  Until the existing appointment ends, they should not accept another appointment involving concurrent obligations without the permission of the appropriate administrator. 

<h2>Conclusion </h2>
This document focuses on the ethical obligations of members of the modern language profession.  A common understanding of such obligations will enable us to exert appropriate restraints in exercising our responsibilities as scholars, teachers, and students and to promote ethical behavior in our departments and institutions.

<h2>History</h2>
"Statement of Professional Ethics" was adopted by the Delegate Assembly in 1991 and published in <i>Profession 92</i>. Earlier drafts were prepared by an ad hoc committee of the MLA appointed by the Executive Council in 1987, following a Delegate Assembly recommendation that such a committee be designated to "study professional ethics and provide MLA-endorsed guidelines" ("Professional Notes" 382).  The committee members who prepared the statement were Barry Gaines, University of New Mexico; Lawrence Poston, University of Illinois, Chicago (chair); Roslyn Abt Schindler, Wayne State University; Mario Valdes, University of Toronto; and Louise Vasvari, State University of New York, Stony Brook.

This original statement was substantially revised in 2004 by the Committee on Academic Freedom and Professional Rights and Responsibilities, whose members were Carla Freccero, University of California, Santa Cruz; Lisa Justine Hern&#225;ndez, Saint Edward's University; Sue Hintz, Northern Virginia Community College; Genaro M. Padilla, University of California, Berkeley; Andrew Parker, Amherst College (chair); and Gema Per&#233;z-S&#225;nchez, University of Miami.

<h2>Notes</h2>
<sup><a name="one">1</a></sup> When a faculty member's fulfillment of ethical obligations is reviewed, care should be taken that it, like other subjects of evaluation, is not arbitrarily or capriciously judged. Any actions that may lead to the nonrenewal of an appointment, to the dismissal of a tenured faculty member, or to other such sanctions should be pursued in accordance with generally accepted procedural standards. See especially the "1940 Statement of Principles on Academic Freedom and Tenure" of the American Association of University Professors, endorsed by the MLA in 1962 and augmented with interpretive comments in 1970, and the related AAUP "Recommended Institutional Regulations on Academic Freedom and Tenure."

<sup><a name="two">2</a></sup> In this statement we adopt the definition of plagiarism given in Joseph Gibaldi's <i>MLA Style Manual</i>: "Using another person's ideas or expressions in your writing without acknowledging the source constitutes plagiarism.... [T]o plagiarize is to give the impression that you wrote or thought something that you in fact borrowed from someone, and to do so is a violation of professional ethics....  Forms of plagiarism include the failure to give appropriate acknowledgment when repeating another's wording or particularly apt phrase, paraphrasing another's argument, and presenting another's line of thinking" (6.1; see also Gibaldi, <i>MLA Handbook</i>, ch. 2). It is important to note that this definition does not distinguish between published and unpublished sources, between ideas derived from colleagues and those offered by students, or between written and oral presentations. 

<sup><a name="three">3</a></sup> Such relationships impose on researchers a special responsibility to guard the students involved from such abuses as breach of confidentiality and research-related harm. Scholars should inform themselves of and observe institutional regulations and guidelines on the use of human subjects in research.

<sup><a name="four">4</a></sup> Teachers should familiarize themselves with the Family Educational Rights and Privacy Act (FERPA, 20 U.S.C. &#167; 1232g; 34 CFR Part 99). As explained in the section of the United States Department of Education's Web site devoted to the act, FERPA is "a Federal law that protects the privacy of student education records. The law applies to all schools that receive funds under an applicable program of the U.S. Department of Education. FERPA gives parents certain rights with respect to their children's education records. These rights transfer to the student when he or she reaches the age of 18 or attends a school beyond the high school level" (<I>Family Educ. Rights</i>).

<h2>Works Cited</h2>
American Association of University Professors. <i>Policy Documents and Reports</i>. Washington: AAUP, 1990.

<I>Family Educational Rights and Privacy Act (FERPA)</i>. Family Policy Complicance Office, U.S. Dept. of Educ. 15 Nov. 2004 < <a href="http://www.ed.gov/print/policy/gen/guid/fpco/ferpa/index.html" target="_blank">http://www.ed.gov/print/policy/gen/guid/fpco/ferpa/index.html</a> >.

Gibaldi, Joseph. <i>MLA Handbook for Writers of Research Papers</i>. 6th ed. New York: MLA, 2003.
---. <i>MLA Style Manual and Guide to Scholarly Publishing</i>. 2nd ed. New York: MLA, 1998.

"1940 Statement of Principles on Academic Freedom and Tenure: With 1970 Interpretive Comments." Amer. Assn. of Univ. Professors 3-7.

"Professional Notes and Comment." <i>PMLA</i> 102 (1987): 374-86.

"Recommended Institutional Regulations on Academic Freedom and Tenure." Amer. Assn. of Univ. Professors 21-30. 

"Statement of Professional Ethics." <i>Profession 92</i>. New York: MLA, 1992. 75-78.

