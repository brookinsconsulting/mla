<h1>Job Offers</h1>
<ol>
<li><a href="#terms">How do I negotiate the terms of a job offer?</a>
<li><a href="#accept">How do I decide whether to accept an offer?</a>
<li><a href="#foreign">How will a job in a foreign country affect my prospects for getting a job at home?</a>
<li><a href="#nooffer">What should I do if I don't get an offer?</a>
</ol>
<A NAME="terms"> </a>
<h2>How do I negotiate the terms of a job offer?</h2>
Candidates may be excited to hear over the telephone that they have won a job, but they should politely insist on receiving the offer in writing before accepting or declining it. In this process, candidates should regard the department chair as an honest broker who nevertheless must negotiate with the institution's interests in mind. Therefore, candidates may wish to discuss the terms of the job offers with graduate school mentors and to speak with recently hired assistant professors at offering schools to confirm that the terms proposed are comparable to theirs. If candidates then negotiate enhancements, they should receive, sign, and return a revised offer letter reflecting any agreed-on changes. During this process, candidates should be patient. Most chairs must discuss with their deans all revisions to offers before writing to candidates. This process can take days, even weeks: for example, a dean may be slow to approve changes or a chair may have to redraft a letter, send it to a dean for approval, and then incorporate the dean's editorial changes. At some schools, the modified offer letter may have to be scrutinized and approved by the legal department.

A candidate who is ready to negotiate should ask the chair about any or all of the terms, working conditions, fringe benefits, and perquisites I list below. At some institutions, many of the items that follow will already be included, though a candidate may wish to improve some terms; at other institutions, a candidate may have to negotiate the inclusion of these items in a contract.

<h3>Compensation</h3>
<ul>
<LI>Salary
<LI>Equity adjustments
<LI>Merit adjustments (Does the institution have a policy? What has its record been in past years?)
<LI>Salary advance (to be paid before appointment begins)
<LI>Course buyouts (trading money for time to work on research)
<LI>Summer salary (after appointment begins)
<LI>Postdoctoral research (What happens if a new hire wins a postdoctoral grant? Does the institution have a postdoctoral grant program?)
</ul>

<h3>Perquisites</h3>
<ul>
<LI>Moving expenses
<LI>Research expenses and leave
	<ul>
	<LI>Computer and software or peripherals
	<LI>Research leave during tenure probation
	<LI>Supplementation for outside grants (How often and for how long can winning a grant stop the tenure clock?)
	<LI>Conference or travel expenses, including expenses for overseas conferences and research
	<LI>Special materials purchase or rental (e.g., video or film, software)
	</ul>
<LI>Teaching support and working conditions
	<ul>
	<LI>Teaching load and class scheduling (Are there choices?)
	<LI>Course reductions (for research, public service, or other reasons)
	<LI>Time off for course development
	<LI>Photocopying and office supplies
	<LI>Graders for large classes
	<LI>Faculty development and training
	</ul>
</ul>

<h3>Fringe Benefits</h3>
(What is the standard package? Can special medical, insurance, or other needs be accommodated?)

<h3>Other conditions</h3>
<ul>
<LI>Spousal or partner hiring or job placement
<LI>Fringe benefits for spouse or partner
<LI>Spousal or partner tuition
<LI>Child care arrangements
<LI>Pregnancy or parental leave
</ul>

Candidates involved in negotiations should bear in mind that they are not only creating favorable terms of employment for themselves but also dealing with immediate supervisors and colleagues whose trust, confidence, and goodwill will be essential during tenure probation. Therefore, candidates should not be shy, should deal firmly but courteously, should ask for what they justifiably need, and should not play games. The best terms are those that both the candidate and his or her new department can agree will enhance the candidate's work and his or her value to the institution as a teacher, researcher, and colleague.

<a href="javascript:popup('/bulletin_118033','docs_popup');">&quot;Negotiating a Job Offer&quot;</A>
<blockquote>
Philip E. Smith
Department of English
University of Pittsburgh, Pittsburgh
</blockquote>
<A NAME="accept"> </a>
<h2>How do I decide whether to accept an offer?</h2>
Should you find yourself in the enviable position of weighing two or more offers, you should seriously consider several factors before you make even a preliminary choice. The prestige of the institution and the level of remuneration will undoubtedly affect your decision. Yet other factors may be more important. Location should be one of your first considerations. If you possibly can, you should be prepared to move&#151;others will be&#151;but not to someplace where you'll be miserable or where there is no adequate affordable housing. The size of the institution is another crucial factor, as are the size and makeup of the department. A large department with strong enrollments will rarely require much versatility. A smaller department may well expect you to show great versatility and, because enrollments are usually low, to attract new students to the program. Since enrollments mean jobs, this responsibility is serious business.

Will you be satisfied teaching undergraduate courses, or will you need the challenge of participating in a graduate program? Do you like teaching both language and literature? What about methodology? Candidly assess your teaching ability and your long-term commitment to research. A small department may need a star in the classroom. It will also make heavier service demands, but if reason prevails, there will be less emphasis on research productivity.

Other important questions revolve around the institution's status as public, private, or religiously affiliated. What is its financial situation? How diverse are its faculty and student body? Is the institution unionized? Do faculty members get raises? Promotions? Tenure? The tenure situation should of course be explored in depth. A lot of issues are involved here. Try to discover when tenure was last granted in the department and what the recipient's credentials were. Ask how much research is expected and how research is supported. What relative weight is given to teaching, research, and service? Inquire about the teaching load, library facilities, and the availability of grants, leaves, and conference money. What kind of contract will you be issued? In a word, try to determine whether junior faculty members are generally nurtured or exploited in the institution you are considering. I certainly do not recommend that you barrage your interviewers with all these questions, some of which you can answer by consulting a catalog, but you should have replies to all of them before you accept a job.

<a href="javascript:popup('/bulletin_241038','docs_popup');">&quot;The MLA Job Interview: What Candidates Should Know&quot;</A>
<blockquote>
Ann Bugliani
Department of Modern Languages and Literatures
Loyola University Chicago
</blockquote>
<A NAME="foreign"> </a>
<h2>How will a job in a foreign country affect my prospects for getting a job at home?</h2>
<blockquote>
If you are in a field where your international experience is directly relevant to the position you want to seek, working abroad will be an advantage. Unfortunately, such experience is relevant only occasionally&#151;in ESL, for instance. For most candidates looking at positions in literature, there is no obvious gain. While living and working overseas is intrinsically enriching and will almost certainly give you intangible benefits valuable to your long-term life and career, at the entry level you may be disadvantaged by not entering the market when your graduate research is freshest. Leaving the United States will also take you away from opportunities to publish, present, and network. Similarly, running a search from abroad is difficult and costly. Different academic calendars, differences in time zones, and sheer distance may all pose barriers to easy participation in the United States hiring cycle. While getting back for the MLA Annual Convention may be feasible, arranging multiple campus interviews is much more problematic. The bottom line: international work experience can be deeply enriching, but, if you are considering it, you need to be aware of the complications it may create, and you must plan a reentry strategy to the United States (the late-year hires into temporary positions may be a good option).
Iain Crawford
Dean, School of Liberal Arts
University of Southern Indiana
</blockquote>
<A NAME="nooffer"> </a>
<h2>What should I do if I don't get an offer?</h2>
Receiving few or no invitations to interview may be no reflection on your accomplishments and qualifications but simply the result of a terrible job market.  "What am I doing wrong" is a question haunting people who have done nothing wrong at all.
<blockquote>
Margaret Schramm
Department of English and Theater Arts
Hartwick College
</blockquote>
