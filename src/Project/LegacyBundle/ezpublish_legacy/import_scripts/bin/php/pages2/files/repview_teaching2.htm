<h1>A Call to Action: The Committee's Recommendations</h1>
We urge a renewed commitment by the MLA to excellence in teaching. It is a commitment worth making, one that will speak for our association to our many academic constituencies and to the larger public, which places a high premium on the instruction of our students.

Because the MLA believes teaching matters, it must expand the ways in which scholars can explore pedagogies, examine classroom practice, and find support for their efforts to develop programs in modern languages that are appropriate to the twenty-first century. By providing occasions for members to discuss their teaching outside their departments, the MLA fosters communication and community building, both inside and beyond the academy.

As the largest national organization devoted to teaching and scholarship in higher education for all the modern languages, the MLA can also foster the type of dialogue so needed today, a dialogue involving faculty members and students at all levels in modern language departments and between those departments and their wider constituencies in their institutions and in their communities. This committee affirms the MLA's mission to support and to influence the ways in which teaching is understood, valued, and rewarded.
<h2>Recommendations to the MLA</h2>
<h3>A Standing Committee on Teaching</h3>
Our strongest recommendation is for the establishment of a standing committee on teaching so that the MLA can better address the issues that we believe to be most critical at this time in our profession, regardless of institutional setting. By establishing a standing committee on teaching, the MLA will demonstrate that teaching as a scholarly and professional endeavor remains a central concern of our organization. Further we believe that the issues identified below constitute a rationale for the establishment of a standing committee and may serve as a guide for the development of an agenda for action. We suggest that this agenda be developed in consultation with the divisional committees on teaching, with the various publication committees, and with the membership as a whole.
<dl>
<dd><i>Accordingly, we recommend</i>
<ul>
<li>that the MLA establish a standing committee on teaching to provide a place for ongoing attention to questions related to teaching
</ul>
</dl>

The issues we identified as important for this standing committee to address follow. We formulate them as guidelines for the committee to conduct its work and as questions that the broader MLA membership may take back to their home institutions for further discussion.

<h3>Publications</h3>
Debates about the state of the canon and definitions of cultural literacy occur in popular media and conference presentations and are addressed in important policy journals such as the <i>ADFL Bulletin</i> and <i>ADE Bulletin</i>. However, no broader-based, prestigious publication exists in the MLA for constructive dialogue about teaching.
<dl>
<dd><i>Accordingly, we recommend</i>
<ul>
<li>that the MLA publication program continue to seek out new and varied examinations of teaching, including new understandings of classroom practice and classroom life
<li>that the MLA publish a second issue annually of <i>Profession</i> that focuses on teaching issues
<li>that at regular intervals <i>PMLA</i> devote a special section to teaching
<li>that the MLA sponsor a member-moderated electronic discussion list in which MLA members explore the values and assumptions they bring to the act of teaching
</ul>
</dl>

<h3>Collaboration between the MLA and Other Organizations</h3>
The committee recognizes the need for information sharing among all members of the profession. We assert the value of collaborative activities devoted to the teaching of and research on culture, language, literature, and writing. To that end, the committee supports the MLA's initiative in sharing sponsorship of and collaborating on proposals for funding to support internships, institutes, publications, and other activities.
<dl>
<dd><i>Accordingly, we recommend</i>
<ul>
<li>that the MLA actively explore means of facilitating collaboration on teaching with other organizations in the field of modern languages
</ul>
</dl>

<h2>Recommendations to the Profession at Large</h2>
<h3>The Reward System</h3>
If teaching matters, it must figure substantively and visibly in our profession's reward system: job security (tenure or long-term contracts), promotion, and other sorts of systemic rewards, such as book prizes, publishing contracts, sabbaticals, and grants.

Aware of teaching's complex implications in local politics, state budget crunches, individual institutions' ambitions, and the politics of the diverse fields the MLA represents, the committee nevertheless tenders the following recommendations as steps both flexible enough to be adapted to local situations and concrete enough to feature teaching more prominently in the reward systems of our profession and of the MLA itself.
<dl>
<dd><i>Accordingly, we recommend</i>
<ul>
<li>that institutions and departments develop clear statements about the place of teaching in tenure and promotion
<li>that institutions and departments design mechanisms to evaluate teaching
<li>that institutions and departments design mechanisms and provide necessary support for continuing education and materials to improve teaching
</ul>
</dl>

<h3>The Scholarship of Teaching</h3>
This committee strongly supports the idea that research on classroom practice is a valid and important aspect of our professional lives. Paying attention to and documenting what happens in college classrooms brings visibility to teaching and learning. It focuses on how students learn and the methods by which we teach them. It asks questions about processes as well as products. And it takes the life of the classroom as its central focus.

A commitment to documenting and studying classroom life will require many changes both inside and outside the academy. In institutions of higher learning, it will mean substantial changes in curricula, graduate programs, teacher training, and tenure decisions. Outside our institutions, such scholarship will enlarge public understanding of what is entailed in educating students.
<dl>
<dd><i>Accordingly, we recommend</i>
<ul>
<li>that institutions and departments value the scholarship of teaching--of the methods, assessment procedures, and ways to improve teaching--as equivalent to traditional forms of scholarship, when it is subjected to equivalent scrutiny by the rest of the profession
<li>that institutions and departments create interdisciplinary seminars and hold colloquia regarding language development and literacy issues
<li>that English and foreign language departments build bridges to scholars in education schools and departments
<li>that English and foreign language departments develop seminars on qualitative and quantitative research methods
<li>that institutions and departments encourage collaboration among and within departments
</ul>
</dl>

<h3>Teacher Education</h3>
The committee believes that the preparation of future teachers is central to the work that we do in our disciplines and of crucial importance for the future of our fields. Research on the contributions of content-area course work to teacher performance and future development has many potential benefits. Further, there is great potential for collaboration in research between collegiate and precollegiate teachers. Solid work on the contributions of our disciplines to teacher preparation could address public misconceptions and doubts about the commitment of higher education to teacher reform. The matter of teacher education speaks profoundly to making the value of our disciplines--of our scholarship and our teaching--known.
<dl>
<dd><i>Accordingly, we recommend</i>
<ul>
<li>that faculty members make the rationales behind their pedagogical choices visible in their classrooms
<li>that faculty members clarify to students why our fields matter in the academy and in society at large
<li>that institutions and departments keep track of majors throughout their careers
<li>that institutions and departments provide formal structures for liaisons between academic disciplines and secondary school teachers
<li>that institutions and departments support and reward those faculty members involved in the training of teaching assistants and teacher education
<li>that institutions and departments encourage research contributing to learning and curriculum
</ul>
</dl>

<h3>Graduate Education</h3>
Graduate education provides the institutional setting for the development of the teacher-scholar. As such, the committee believes that graduate education should demonstrate that teaching matters by offering courses in pedagogy, preparing students for a range of teaching situations, mentoring students, providing models of reflective practice, and helping students with the job-search process.
<dl>
<dd><i>Accordingly, we recommend</i>
<ul>
<li>that graduate programs give higher priority to and strengthen programs in the teaching of language, literature, linguistics, writing, and culture that will orient and train new faculty members in the art and science of teaching and learning
<li>that graduate programs follow the recommendations of the Committee on Professional Employment (<i>Final Report</i>) relating to the expansion of the graduate curriculum to include courses in pedagogy that will prepare students for a range of teaching situations and familiarize them with the complex system of postsecondary education in the United States and Canada
<li>that graduate programs provide students with mentoring and collaborative activities for professional development at every stage of their graduate careers
<li>that graduate programs provide consultation and supervisory support through every stage of the program, from candidacy exam through dissertation
<li>that graduate programs provide early discussion of career options as well as direct assistance with the job-search process
</ul>
</dl>

<h3>Working Conditions</h3>
While recognizing the differences in institutional conditions that exist in higher education, the committee strongly believes that teaching--in its full meaning, going beyond classroom lecturing and discussion--is based on and strengthened by the scholarship of both subject-matter content and pedagogy. Thus, a course load and class sizes that make scholarship possible are as essential to teaching of high quality as fair labor and contract practices.
<dl>
<dd><i>Accordingly, we recommend</i>
<ul>
<li>that institutions and departments, by assigning reasonable teaching loads and limiting class sizes, create conditions conducive to enabling the effective teacher-scholar we envision
<li>that institutions and departments provide professional recognition, appropriate contractual arrangements, and pro rata compensation for part-time faculty members
<li>that institutions and departments provide professional recognition, appropriate contractual arrangements, and appropriate compensation for non-tenure-track faculty members
</ul>
</dl>

Our committee completes its report to the association with a sense of gratitude to the Executive Council for being chosen for such a formidable task and an equally strong sense of satisfaction and pride that we have worked hard in our research, in our many discussions, and in the formulation of our final recommendations.

We are a varied group of MLA colleagues, older and younger, from different kinds of institutions of higher learning, and with both research and teaching interests. We have often disagreed on priorities, strategies, and the most effective ways of articulating our recommendations, but out of strong dialogue a consensus has emerged.

What has made us a confident and collegial committee is our deep commitment to the centrality of excellence in teaching--whether done by a tenure-track, adjunct, or graduate student instructor--in the life of college and university. At a time of uncertainty and challenge in our culture, such teaching, properly attended to and strongly supported, will shape the good student and the good citizen. The Modern Language Association should be in the forefront of this venture, lending its prestige, its strong voice, and its active support.
<blockquote>
<i>Submitted by
Helen R. Houston, language, literature, and philosophy, Tennessee State University
Elizabeth L. Keller, comparative literature, Rutgers University, New Brunswick
Lawrence D. Kritzman, French, Dartmouth College
Frank Madden, English, Westchester Community College, State University of New York, chair
John L. Mahoney, English, Boston College
Scott McGinnis, National Foreign Language Center
Susannah Brietz Monta, English, Louisiana State University, Baton Rouge
Sondra Perl, English, Graduate Center, City University of New York
Janet Swaffar, Germanic Studies, University of Texas, Austin</i>
</blockquote>


<h2>Notes</h2>
<a name="note1" href="/repview_teaching#note1text"><sup>1</sup></a> This comment is not unique to this ad hoc committee. The American Association of State Colleges and Universities recently stated in its report <i>Facing Change: Building the Faculty of the Future</i>, "Higher education has failed to effectively articulate the case for systematic, progressive faculty development" (20). The report goes on to assert, "Higher education faculty are not regularly trained in teaching, learning, advising, or the overall teaching and learning enterprise. New priority must be given to strengthening training programs for the next generation of teachers and to developing programs that will orient and train new faculty in the art and science of teaching and learning" (21).

<a name="note2" href="/repview_teaching#note2text"><sup>2</sup></a> The ADE Ad Hoc Committee on Staffing did a sample survey in 1996-97 of staffing in representative departments. In PhD departments responding, TAs taught 61% of the first-year writing sections, 30% of the lower-division literature courses, and 3% of the upper-division literature sections ("Report").

<a name="note3" href="/repview_teaching#note3text"><sup>3</sup></a> Donald Gray has argued that our profession has tended historically to believe that teacher education is the responsibility of a select few, people in other departments (4). The potential benefits of taking the responsibility of teacher preparation as an integral part of our work are, however, enormous. As Kathryn T. Flannery et al. note, "Perhaps more than any other group of students in English, not excluding PhDs, preservice English teachers are a continuing responsibility, to the university, to their teachers, and to themselves. No other group has a greater impact on the hardest question of all: How will the knowledge, abilities, and canons of judgment that make up what we call English exist and do their work in the culture and politics of our country?" (61).

<a name="note4" href="/repview_teaching#note4text"><sup>4</sup></a> Phyllis Franklin recently argued that teacher preparation is one of the most publicly influential things we do: "For almost a decade MLA members have insisted on the need for the field to reach an audience outside the academy in order to promote a better public understanding of how the humanities and especially the study of language and literature contribute to society, prepare students for careers, and enrich people's lives. I cannot imagine a more effective way of reaching this audience than to participate in the effort to strengthen the quality of schooling in the United States. Each knowledgeable, intellectually lively teacher we educate will affect the lives of thousands of young people and affirm the value of the subjects we teach" (5).


<h2>Works Cited</h2>
"ADE Guidelines for Class Size and Workload for College and University Teachers of English: A Statement of Policy." Mar. 1992. <i>ADE Policy Statements</i>. 28 Oct. 1998. 5 June 2001 <a href="http://www.ade.org/policy/index.htm" target=BLANK><nobr><http://www.ade.org/policy/index.htm></nobr></a>.

"ADE Statement of Good Practice: Teaching, Evaluation, and Scholarship." Mar. 1993. <i>ADE Policy Statements</i>. 28 Oct. 1998. 5 June 2001 <a href="http://www.ade.org/policy/index.htm" target=BLANK><nobr><http://www.ade.org/policy/index.htm></nobr></a>

"ADFL Guidelines on the Administration of Foreign Language Departments." 1993. <i>Projects and Reports</i>. 30 Nov. 2000. 5 June 2001 <a href="http://www.adfl.org/projects/index.htm" target=BLANK><nobr><http://www.adfl.org/projects/index.htm></nobr></a>.

American Association of State Colleges and Universities. <i>Facing Change: Building the Faculty of the Future</i>. Washington: Amer. Assn. of State Colls. and Univs., 1999.

Boggs, George R. "What the Learning Paradigm Means for Faculty." <i>AAHE Bulletin</i> 51.5 (1999): 3-5.

<i>Final Report of the MLA Committee on Professional Employment</i>. New York: MLA, 1997.

Flannery, Kathryn T., et al. "Watch This Space; or, Why We Have Not Revised the Teacher Education Program--Yet." Franklin, Laurence, and Welles 49-64.

Franklin, Phyllis. "An Urgent Request to Rethink Teacher Preparation." <i>MLA Newsletter</i> 31.2 (1999): 4-5.

Franklin, Phyllis, David Laurence, and Elizabeth B. Welles, eds. <i>Preparing a Nation's Teachers: Models for English and Foreign Language Programs</i>. New York: MLA, 1998.

Gray, Donald. "Introduction: What Happens Next? And How? And Why?" Franklin, Laurence, and Welles 1-16.

Marshall, James. "Closely Reading Ourselves: Teaching English and the Education of Teachers." Franklin, Laurence, and Welles 380-89.

"The Politics of Intervention: External Regulation of Academic Activities and Workloads in Public Higher Education." <i>Academe</i> Jan.-Feb. 1996: 46-52.

"Report of the ADE Ad Hoc Committee on Staffing." <i>ADE Bulletin</i> 122 (1999): 7-26.

Schon, Donald. <i>The Reflective Practitioner</i>. New York: Harper, 1983.

"The Work of Faculty: Expectations, Priorities, and Rewards." <i>Academe</i> Jan.-Feb. 1994: 35-48.


<a href="/repview_teaching">< Previous</a>
