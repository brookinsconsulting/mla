<h1>Tool Kit on Intellectual Property</h1>
<h2>What Is Intellectual Property?</h2>
Intellectual property is an expansive legal concept that attaches rights of ownership to intangible commodities such as ideas and processes. This form of property has been expanding rapidly in the last fifty years and is of particular relevance on college and university campuses.  For the purposes of US federal law, three distinct forms of ownership over the “labors of the mind” can be asserted: patent, trademark, and copyright. 

The digital media ecology presents especially significant challenges and opportunities in defining and measuring intellectual property. When content is mashed up digitally, created socially, crowdsourced anonymously, broadcast, shared, and remade across jurisdictions, ownership is in question. These questions are not unprecedented.  However, the contemporary content industry, dominated by large multinational firms with a proprietary interest in information goods of all kinds, has been especially keen to enhance patentability, extend copyright terms, and combat theft or “piracy” of intellectual property.

As scholars we constantly use the intellectual properties of others—when we quote, for example, we are reproducing materials that may belong to someone.   Our right to the “fair use” of such materials, a once-established exception to copyright, is under increasing scrutiny.  We also face a rapidly changing situation as authors of content, not only as scholars but also as teachers.  We create valuable information and resources in the course of our classroom duties, and we need to be clear about who owns that content. 

Universities, like other corporate entities, generally own the work produced by their employees.  Faculty members enjoy a different status, and their intellectual property is not often understood as “work for hire.” Faculty members who create new knowledge in the course of their academic duties normally enjoy a share of any commercial revenues, patent rights, or license fees that stem from their research. Most institutions of higher education do not assert property claims over the professional productions of humanities faculty members—scholarly articles and books, for instance, are understood to be beyond the university’s grasp. Teaching materials, syllabi, assignments, and class lectures are generally understood to be the property of their faculty or student author, provided they are used on campus and for noncommercial purposes. Colleges and universities may, however, choose to assert an intellectual claim over teaching materials, if the faculty author tries to sell or license them commercially.

<h2>Why Does It Matter?</h2>
As MLA members we use information protected by intellectual property rights on a daily basis, and we produce actually or potentially protected work products constantly.

We face a new situation, as some colleges and universities turn to the outsourcing of curricula and instruction. Private organizations have always been present on university campuses, but the role of for-profit vendors of teaching and learning materials and course-management platforms is expanding rapidly. It is essential that MLA members understand how intellectual property considerations are fast becoming a central matter in all areas of academic life. Moreover, MLA members should play an affirmative role in defining their institutions’ policies and practices regarding intellectual property.

<h2>What to Do?</h2>
MLA members should educate themselves about the specific policies of their home institutions.  Each university should have a stated intellectual property policy in faculty and student handbooks, employment contracts, and collective bargaining agreements. At large research universities, members should consult the office of technology transfer or the university’s general counsel. If an institution appears to have no stated policy, members should consult the general counsel’s office and advocate the articulation of a clear policy.  The relationship among faculty members, students, and university administrators on this matter need not be adversarial.

MLA members should consider the following questions:<blockquote>

•	Who owns teaching materials for my courses, such as class notes, assignments, syllabi, and the like? Is the university able to use, record, broadcast, license, sell, or promote my course without my consent or knowledge?

•	Does my institution see student intellectual property rights differently from faculty rights? What rights do students have over any academic content they generate? 

•	If I create a valuable intellectual property interest and the university sells, patents, trademarks, or licenses it, what are my rights and responsibilities?  

•	Does my university policy say expressly that any content I create on campus or in the pursuit of my official duties, whether for research, teaching, or administrative service, is made on a “work for hire” basis? (If so, an author’s rights are very limited.) 

•	Whom do I speak to about the institution’s intellectual property policy, and what are the specific details of the policy as applied to work in the humanities? </blockquote>

<h2>What to Read?</h2>
Johns, Adrian. <i>Piracy: The Intellectual Property Wars from Gutenberg to Gates</i>. Chicago: U of Chicago P, 2010. Print.

Lessig, Lawrence. <i>Remix: Making Art and Commerce Thrive in the Hybrid Economy</i>. New York: Penguin, 2008. Print.
	
Moore, Adam. “Intellectual Property.” <i>The Stanford Encyclopedia of Philosophy</i>. Stanford Encyclopedia of Philosophy, 8 Mar. 2011. Web. 27 Dec. 2012.

Springer, Ann. “Intellectual Property Legal Issues for Faculty and Faculty Unions.” <i>American Association of University Professors</i>. AAUP, 18 Mar. 2005. Web. 27 Dec. 2012.

Vaidhyanathan, Siva. <i>Copyrights and Copywrongs: The Rise of Intellectual Property and How It Threatens Creativity.</i> New York: New York UP, 2003. Print.


<i>This document was created by the Committee on Academic Freedom and Professional Rights and Responsibilities in April 2012.</i>
