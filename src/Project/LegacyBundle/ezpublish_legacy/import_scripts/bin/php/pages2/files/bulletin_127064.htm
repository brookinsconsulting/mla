<b>Number 127, Winter 2001</b>
<h1>The Scholar in Trade Publishing; or, What's a Nice Kid like You Doing in a Place like This?</h1>
<h2 class="byline">MICHAEL KANDEL</h2>
PERHAPS now more than ever, many scholars are considering a career outside the academy (or at least a job that makes some respectable use of their long and expensive education). What sort of fit can they expect for their knowledge and skills in the world of trade publishing?

Trade publishing involves the production and sale of the books found in a general bookstore. Trade books are novels, biographies, mysteries, cookbooks, poetry collections, and so on. They are not textbooks, scientific books, or academic books. The division between trade and academic is often blurred, but the principle is that the trade book is for the layperson, the academic book for the expert or expert-to-be.

I entered trade publishing with no planning or preparation. I had spent what seemed an age in the academy and assumed that I would never leave it. Like many others, I saw myself as a scholar in a reasonably comfortable nook of the ivory tower. I received my PhD in Slavic languages and literatures (Indiana University), taught for several years as an assistant professor (George Washington University), and failed to obtain tenure at about the same time that the job market first soured. I became the parent at home for several years, working as a freelance translator (from Polish, the novels of Stanislaw Lem). As a translator, I made about five thousand dollars a year. I also worked on a novel that I eventually had the sense to throw out. When my children finally went off to college, I rejoined the labor force--in the New York trade department of Harcourt, where I learned how the business ran and how to be an editor.

Today I am editor on the MLA staff but continue to work as a consulting editor for Harcourt, where I acquire science fiction, a title or two a year. A recent moment of excitement for me was acquiring Ursula K. Le Guin.

My professional training at Harcourt was entirely hands-on and practical; I learned book by book and crisis by crisis. (Of the little reading I did on book publishing I would recommend, for those who would like an introduction to the subject, Clarkson N. Potter's <I>Who Does What and Why in Book Publishing</I> and Marshall Lee's <I>Bookmaking</I>). The word <I>practical</I>, an antonym of <I>academic</I>, suggests that scholars hired by a publisher will be starting from square 1 and that there will be a great deal of getting one's hands dirty: filling out copyright and contract request forms, marking chapter openers and extracts for the designer, smoothing an author's ruffled feathers over the phone, obtaining permission to reprint two lines of a song lyric, presenting a new book at a marketing meeting to sales reps, responding to queries from a proofreader, and mailing bound galleys in the hope (slim) of obtaining from a personage a quote that can go on the jacket.

Roughly, trade editors do the following in the soup-to-nuts life of a book. Duties vary, depending on rank and house.
<ol>
<li>Editors read submissions, solicit submissions from authors and literary agents, reject submissions.</li>
<li>For the book they like enough to go to bat for, they prepare a presentation, often both written and oral. It might be a memo distributed, with copies of the manuscript or proposal, to their colleagues in the department: fellow editors, publicists, subsidiary-rights people, marketing people, sales people, executives. The decision typically is reached at an editorial meeting: different voices heard around the conference table.</li>
<li>If the decision is yes, the publisher sets the parameters and editors then negotiate with the author or author's agent on the terms of the contract.</li>
<li>If the deal is closed, editors edit the manuscript, a task that can range from major reshaping to practically no change. Although the editor suggests and the author decides, their collaboration can be ticklish. I sometimes apologize beforehand: "Excuse me for tramping through your garden with my boots." Editors are also responsible for alerting everyone to possible legal problems, like libel or invasion of privacy.</li>
<li>Editors may participate in deciding the physical look of the book. They may help promote the book, talking it up at parties and conventions. Some editors visit independent bookstores and cultivate the acquaintance of the owners.</li>
<li>Good editors keep the author in the picture throughout the process and act as ombudsman, shoulder to cry on, or drill sergeant. When the book gets a <I>New York Times</I> review, they tell the author; when the book is remaindered or pulped, they tell the author.</li>
<li>After the book is published, editors may participate in decisions to advertise or reprint. If the author receives the Nobel Prize in literature, they may have to fly to Stockholm.</li>
</ol>

Scholars entering trade publishing, if their diplomas are of little moment in this particular workplace, do have some advantages over nonscholars. Scholars already know how to focus on a text, on a word, maybe even on a comma; know how to put a detail in a larger context; know how to see the strengths and weaknesses of a work; know how to communicate to others, and infect them with, love for a work and for a writer; and know how to go about digging up information.

Academics and publishing people share certain values. Both respect books. Both play the role, at least sometimes, of cultural custodian. There are personal links between these two worlds; some of our finest professors are also some of our finest authors; some editors also teach courses in universities.

I would say that trade publishing combines three cultures: the academy, show biz, and the corporation. The academy supplies the dignity, show biz the glamour, and the corporation the undignified, unglamorous attention to profit and loss.

The image of big bucks in the book business is popular: million-dollar advances, Stephen King buying a baseball team. But trade publishing--unlike textbook publishing, for example--tends to lose money rather than make it. The publishing of hardcover fiction is especially unwise from a commercial point of view. This very simple fact seems to remain an inside secret.

Nine times out of ten a publisher's excitement takes the form of expectation, not of fulfillment. The cycle of hope and disappointment is repeated season after season. A successful (or at least a sane) publishing person looks to the future and has a short memory. When it's corporate review time, "creative accounting" is used to make the red ink as inconspicuous as possible.

The red-ink norm of this enterprise is the main reason work in trade publishing is so stressful. To keep overhead costs down, publishers keep staff down, workloads up, office areas small. I remember walking into the office of a major house years ago and being shocked. Works of world-famous authors, edited in this illustrious place, were on display in the lobby, and yet behind that lobby, through the door, were people hurrying back and forth in a narrow, paper-cluttered warren of cubicles, people with haggard faces.

The editorial lunch is a famous tradition and can be ego gratifying, but a sixty-hour work week is no less a tradition. Nor is the lugging of heavy manuscripts home on weekends and even to the beach on vacation. The pay is low, and the cost of living, since most trade publishing takes place in New York City, is high. Typically, young people at the entry level live outside Manhattan (e.g., Brooklyn) and in spartan circumstances. The last few years have seen a rise in real estate prices and rentals even in outlying neighborhoods. Young publishing professionals therefore lead an existence not unlike that of young musicians or young actors: they are paying for the privilege of being in the action (see Arnold).

Job security? Here the academy, despite all its budgetary woes and its trend of using adjuncts and graduate students as staff instead of tenure-track people, is more humane than book publishing. An elderly editor assures me that at one time (before my time) a good editor was never fired. The rule nowadays is that heads roll often, good and bad heads both. Go to any house and speak with its editors, and you will find that with few exceptions they have worked at not one or two but several houses.

It's true that this business sees quick rises to rank and power. A receptionist becomes an editorial assistant, then an editor, then a vice president, all in the space of a few years. I have seen that happen. It is part of the employment roller coaster.

Trade publishing, compared with many other businesses, has been good for women. The brass at the very top are almost exclusively men, yes, but on the levels right below them women have considerable responsibility and wield considerable power. And compared with other businesses, there is little homophobia, xenophobia, or racism in trade publishing.

The business is rewarding, most of all, in that it offers a person the opportunity--and the great fun--of playing a role in the creation of the books that make a difference in our lives. I've had the satisfaction of launching the career of an author whom no one would touch, then of seeing him reviewed beautifully in <I>Newsweek</I> and applauded on all sides (the author is Jonathan Lethem; there was a paper on him at the 1999 MLA convention). I've had the enjoyment of saving an author from mortification by pointing to a joke in his book that he didn't intend and would have been the eternal butt of. I've made it possible for a few authors to experience the thrill--and the cash--of a movie option.

Book publishing is undergoing tremendous change, and for most the process has not been pleasant. Publishing people are apprehensive--in private if not in public. The primary reason, discussed in magazines and newspapers every so often, is the increasing domination of the corporate component of publishing (see, e.g., Auletta; Forgey; Miller; Schiffrin). Mergers, streamlining, the bottom line, the assembly line.

In the past, trade-book publishers were patient. As they waited for the winner, they tolerated the many losers. Loyally they waited for their respected author to write that breakthrough novel. When it hit the best-seller list, the author's previous seven or eight books would finally begin selling decently. Once the author was established, these titles would sell themselves, steadily, year after year, in various reprints, having entered the promised land of the backlist. That was the traditional strategy. But waiting for rewards years down the road is unacceptable to a company that must disclose its balance sheet to stockholders semiannually. More and more this business is being run by people who do not understand it and who do not really care to.

Another monster change looms: the electronic revolution. The Internet, young as it is, is already making a difference. And when e-books finally work and are affordable, when a screen is devised that is easy on the eyes and you can download any work you like in a few seconds, then--who knows?--the making of individual, physical books may become a kind of quaint artisan activity, like the making of beeswax candles. What part will editors, book designers, publicists, marketing people, and subsidiary-rights people play in the world of virtual books? It is not known.

Trade publishing presents an exciting arena, but what it offers in adventure and cachet it lacks in security. I recommend it only to those who love books and can afford the luxury of taking a big chance.


<I>The author is Assistant Editor, MLA Publications, and Editor-at-Large, Harcourt, as well as a translator and science fiction writer. A version of this paper was presented at the 1999 MLA Annual Convention in Chicago.</i>
<a name="citations"> </a>
<h2>Works Cited</h2>
Arnold, Martin. "It's the Cachet, Not the Money." <cite>New York Times</cite> 21 May 1998: E3.

Auletta, Ken. "The Impossible Business." <cite>New Yorker Magazine</cite> 7 Oct. 1997: 50-63.

Forgey, Elisa. "Books in Chains: The Corporate Consolidation of the Book Trade." <cite>Black and Blue</cite> 3 (1998). 30 Sept. 1999 http://www.english.upenn.edu/~mruben/forgey_3.html.

Lee, Marshall. <cite>Bookmaking: The Illustrated Guide to Design/Production/Editing</cite>. 2nd ed. New York: Bowker, 1979.

Miller, Mark Crispin. "The Crushing Power of Big Publishing." <cite>Nation</cite> 17 Mar. 1997: 11-17.

Potter, Clarkson N. <cite>Who Does What and Why in Book Publishing: Writers, Editors, and Money Men</cite>. New York: Carol, 1990.

Schiffrin, Andr&eacute;. "The Corporatization of Publishing." <cite>Nation</cite> 3 June 1996: 29-32.</FONT></BODY>


&#169; 2001 by the Association of Departments of English. All Rights Reserved.
