<h1>Guidelines</h1>
<h2>Goals of the Series</h2>
<b>Goals of the Series:</b> The principal objective of the series Approaches to Teaching World Literature is to collect within each volume different points of view on teaching a literary subject such as a work, a tradition (e.g., metaphysical poetry), or a writer widely taught to undergraduates. The series is intended to serve nonspecialists as well as specialists, inexperienced as well as experienced teachers, graduate students as well as senior professors.

The preparation of a series volume begins with a proposal. Once the proposal has been approved by the MLAís Publications Committee, the volume editor (or team of editors) prepares a wide-ranging survey of instructors, which is posted on the MLA Web site. The survey enables the book to include the philosophies and approaches, thoughts and methods of numerous teachers. The result is a sourcebook of material, information, and ideas.

Each volume is divided into two parts: ìMaterialsî and ìApproaches.î In the first part, the editor or editorial team, drawing from personal knowledge and experience as well as from the information and issues that emerge from the survey, presents a guide to the most helpful available materials related to the subject of the volume (e.g., preferred editions and translations; essential reference works, critical studies, and background materials; and useful teaching resources). Part 2 comprises essays written by instructors, describing their approaches to teaching the subject of the book. Volumes are broadly representative in the range of their contributors; in the critical orientations presented; and in the types of schools, students, and courses considered. Editors are responsible for addressing all major issues and approaches relevant to the subject.

Since the chief purpose of volumes in the series is to help instructors in their teaching, essays in part 2, "Approaches," should not simply be critical interpretations of a text but should focus on pedagogical practice in the classroom. Contributors should discuss classroom experiences, including student contributions and reactions, and consider the implications and consequences of teaching a work or author using a particular methodology or theoretical perspective. (Note, though, that quotations from student writing require permission; see the section Manuscript, below.) Where appropriate, editors should call attention to and comment on how essays complement or challenge one another (see suggestions in Appendix B, Preparation of Introductory Materials). 

<b>Proposal:</b> Persons interested in editing a volume in the series should write to Sonia Kane (<a href="skane@mla.org" target="other">skane@mla.org</a>), assistant director of book publications, stating their interest and outlining their qualifications for the task. Letters of inquiry should include a curriculum vitae for each prospective editor.

If a title seems desirable for the series and the prospective editor or editorial team appropriate for the task, a staff member will invite a formal proposal. The proposal should address such questions as the need for the volume and the scholarly and pedagogical issues involved in teaching the work or works covered. Although the final content of a volume depends to some extent on essay proposals received at a later stage, the proposal for the volume should sketch out a tentative table of contents, including possible titles for sections and individual essays, and indicate names of scholars who would be appropriate contributors. In preparing the proposal, the prospective editor or editorial team should consult published series volumes.

The staff normally asks at least two specialists to serve as consultant readers of the proposal. On the basis of their evaluations, the staff either returns the proposal to the editor or editorial team for revision or presents it to the Publications Committee for preliminary action. By approving a proposal, the committee invites submission of a full prospectus for evaluation.

<b>Prospectus:</b> After a proposal has been approved, the next step is for the volume editor or editorial team to prepare, with the advice and assistance of MLA staff members, a survey questionnaire to be posted on the MLA Web site. A notice appears in the <i>MLA Newsletter</i> inviting interested MLA members to participate in the survey; in addition, an e-mail message announcing the survey is sent to members of the MLA division or discussion group most closely associated with the study of the literary work or works involved. Editors are encouraged to publicize the survey in other appropriate venues and to communicate directly with scholars who they think would be valuable contributors. 

The survey questionnaire elicits information on such matters as the following: courses in which the subject is taught, editions or translations used, textbooks selected, central issues and typical problems encountered, background readings assigned or recommended to students, critical and reference works consulted by instructors, and audiovisual and electronic resources used. The final item on the survey provides an opportunity for respondents to submit an essay proposal; thus the survey results assist volume editors in developing a list of contributors for part 2, "Approaches," as well as in determining what information should be provided in part 1, "Materials." Note that essays appearing in Approaches volumes must not have been previously published, although on occasion they may include material adapted from an earlier publication by the contributor.

After deciding which proposed essays should be included in the volume, the volume editor or editorial team submits to the MLA a full prospectus, including an annotated table of contents and a rationale for selections (see Appendix A, Preparation of the Prospectus.) The MLA staff editor shares this material with a number of consultant readers; once the readersí reports have been received, the staff editor discusses them with the volume editor or editorial team to decide on a final table of contents.

The prospectus is an important intermediate stage in the development of books in the series. Advice from consultant readers helps ensure that no essential topics have been omitted from the table of contents and, conversely, that contributions do not overlap. Editors, therefore, are reminded not to invite contributors to submit their essays until the prospectus and annotated table of contents have been approved by the MLA staff.

<b>Manuscript:</b> Once the MLA staff has approved the prospectus, the volume editor or editorial team  may invite contributors to submit their essays. To avoid confusion, editors should send with each invitation to a projected contributor guidelines specifying the nature of and intended audience for the volume, the length of the essay desired (usually not more than 3,500 words), the style and format to be followed, the deadline for submission of essays, and so on. Contributors should be informed that the volume editor or editorial team and the MLA reserve the right to reject or request revision of essays that do not conform to the guidelines or that fall below the quality expected from contributors. 

In preparing manuscripts, editors and contributors should follow MLA style as outlined in the latest edition of the MLA Style Manual and Guide to Scholarly Publishing: parenthetical references in the text refer the reader to a list of works cited at the end of the book, which the editor or editorial team compiles from individual works-cited lists supplied by the contributors. For further specific details about formatting a manuscript for submission, volume editors should consult <a href="http://www.mla.org/publications/publication_program/pub_prog_guidelines/pub_prog_prep" target="other">Directions for Preparing Manuscripts</a>, available on the MLA Web site.

Volume editors should establish and inform contributors about the use of uniform editions of major works cited throughout the volume. Generally, all contributors should use the same edition and, if appropriate, the same translation. If for some reason it is not feasible for individual contributors to use the same edition or translation, the volume editor or editorial team should make sure that the individual editions and translations used are properly identified. If the volume deals with works in translation, contributors should supply quotations in both the original language and in translation, formatting the quotations according to MLA style. Editors should make sure contributors are aware of their responsibility to obtain permission to reproduce material beyond fair use. (See MLA Style Manual and Guide to Scholarly Publishing, 3rd ed., 2.2.13-14.) Contributors must assume the costs for such permissions, if any. In addition, it is the MLAís policy that authors obtain permission from students to quote from their writing. The MLA staff will, on request, supply a form that contributors can use to obtain such permission.

Before submitting the manuscript, the editor or editorial team should compile and insert just before the list of works cited the section "Notes on Contributors," containing brief biographical information on each contributor, followed by a list of survey participants, identified by name and institution. The MLA staff editor will provide the editor or editorial team with a form that can be used to request biographical information from contributors. The complete manuscript, including all material but the index, should not exceed 100,000 words (about 450 pages in Courier 12-point type, double-spaced).

Following submission, the complete manuscript is sent to at least two consultant readers, and their evaluations determine whether the manuscript is returned for revision or presented, along with the readersí reports, to the Publications Committee for a decision on publication.

<b>Production and Publication:</b> After the committee approves a manuscript and the book publications staff has reviewed a final version of it, the manuscript is transmitted to the MLAís editorial department for copyediting, design, and production. At that time, editors and contributors receive contracts. During production of the volume, contributors are asked to review relevant parts of the copyedited manuscript and one stage of proof. When page proof is available, the volume editor or editorial team prepares an index of names for the book (the index contains names only, not subjectsósee Appendix C, Preparation of the Index). The editors of a volume receive royalties, while contributors receive honoraria. All receive complimentary copies of the book.

To ensure that all volumes are consistent with the philosophy and objectives of the series, the MLA staff plays an active advisory role in the preparation of each volume. The staff editor assigned to the volume is available to the volume editor or editorial team for consultation at all significant stages in the planning and writing of the volume.

<h2>Appendix A</h2>

Preparation of the Prospectus

1. The selection of contributors is a crucial stage in the development of each volume. Careful planning at this point will help avoid potential confusion or awkwardness at a later stage.

2. In selecting contributors, volume editors should strive to maintain a balance between innovative and traditional approaches and to include essays broadly representative in the range of contributors chosen, in the critical orientations presented, and in the types of schools (two-year colleges, four-year colleges, universities), students (e.g., nonmajors, majors, traditional, nontraditional), and courses (e.g., required survey courses, specialized upper-division courses) considered.

3. Before inviting contributors, the volume editor or editorial team submits to the MLA staff a full prospectus, which includes the following components: 

<ul>
<li>an introduction presenting a rationale for and discussion of the selections and the structure of the book
<li>an unannotated version of the table of contents (i.e., a simple list of the bookís components, as in a published book)
<li>an annotated table of contents, presenting one- or two-paragraph descriptions of each proposed essay
<li>a list of proposed essays that were submitted but not chosen, along with the reasons for not including them
</ul>

The table of contents should be divided into two parts: "Materials" and "Approaches." The annotated version should supply, for part 1, an overview of the major issues the "Materials" section will cover, and for part 2, a one- or two-paragraph description of each proposed essay, along with the author's name and academic affiliation and the essay's projected length. Editors should strive for a uniform style and rhetorical stance in describing the projected essays; in other words, instead of simply inserting the contributors' abstracts into the annotated table of contents as they were received, editors should paraphrase them to make them relatively consistent in length and tone. 

The projected lengths of individual essays may be uniform or may vary. But in deciding on the number of contributors, the volume editor or editorial team should be careful to keep in mind the maximum length of the complete manuscript (excluding the index but including the list of works cited): 100,000 words, or approximately 450 pages in 12-point Courier type, double-spaced. In addition, essays should be organized in meaningful groups with appropriate headings. (Previous volumes in the series should be consulted for ideas on ways to structure part 2, "Approaches.") 

Essays in part 2 should not simply be critical interpretations of a work or works but should focus on pedagogical practice in the classroom. Contributors should discuss classroom experiences, including student contributions and reactions, and consider the implications and consequences of teaching a work or author using a particular methodology or theoretical perspective. 

4. The staff editor reviews this material and sends it for evaluation to a number of consultant readers. The volume editor or editorial team and the staff editor review all readersí comments and suggestions and jointly decide on any needed revisions. When a final table of contents is achieved, the volume editor or editorial team may invite contributors to submit their essays and begin preparation of the manuscript. 

<h2>Appendix B</h2>

Preparation of Introductory Materials

Volume editors furnish introductory materials to help the reader use the volume as fully and efficiently as possible. Typically, an editor writes a preface to the volume and an introduction to the essays in part 2 (see previously published series volumes).

Topics generally addressed in the introduction to part 2, ìApproaches,î include the following:

<ul>
<li><b>rationale for the book:</b> Discuss, for example, why the book is needed, why it was undertaken, why teachers should read it.

<li><b>purpose of the book:</b> Explain the aim of this particular volume in the series.

<li><b>conceptual framing of the bookís topic:</b> Discuss, for example, reception history, theoretical concerns, or scholarly controversies relating to the body of work treated in the volume.

<li><b>structure of the book:</b> Explain how the book in general and specifically the essays in part 2 are organized and why.

<li><b>contexts for essays:</b> Instead of providing summaries of the individual essays, editors should present a consideration of pedagogical issues that emerge from the essays and a discussion of interpretive and methodological relationships, including tensions, among essays.

<li><b>sequence of reading:</b> Provide guidance to help readers understand how to use the book. (Can the reader consult essays out of sequence? Are there some portions of the book that all readers should read? Which essays should be read with or against one another?)

<li><b>editions:</b> State what uniform edition is being used and why. If contributors are referring to different editions, explain why. 

<li><b>translations and translation issues:</b> Many volumes in the series concern literatures in languages other than English. If contributors discuss teaching translated versions of works, the editors should address issues related to teaching in translation and the selection of appropriate texts.
</ul>
