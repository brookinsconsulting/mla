<b>Number 114, Fall 1996</b>
<h1>The Protocols of the Job Search</h1>
<h2 class="byline">ANNE BRADFORD WARNER </h2>
THOUGH the job search lore adrift in the halls of the MLA convention may point to a process filled with the bizarre and arbitrary, leading to serendipity or to tragedy, these retrospective narratives may reveal more about the genius of the storytellers than the process itself. In fact, as a relative newcomer to MLA interviewing, I have been struck by the participating institutions&#039; consistently fair and productive procedures. It is colorful and entertaining even therapeutic for the teller, to tease out the surreal and absurd in the job interview experience, but it serves the candidates and the search committee best to recall their mutual and ordered desire for a good hire. 

In my experience, the narrative of a well-run search committee&#039;s routines is rarely enthralling; perhaps any tale that satisfies a half-dozen persons&#039; versions of the truth is of necessity dull. Still, the search committee&#039;s tale is worth the telling, in part, to make each step in the process as visible as possible, so that candidates can focus on self-presentation, the most important story to be told. 

Searches are expensive for candidates, and they are an important and expensive investment for institutions. Most search committees spend a great deal of time preparing for the interview&#151;reading applications and dossiers and conferring over selections. Their three days at the MLA convention are often taken up with a tight schedule of interviews. They may spend those three days in close discussion with colleagues chosen for field and availability, not congeniality. This is hard work. No one suggests that members of a search committee experience the degree or kind of pressure that job candidates do, but interviewers too must respond to built-in constraints&#151;of time, law, procedure, and coverage. 

The search process follows a set of established structures&#151;some apparent, some hidden&#151;that respond to a range of considerations, from affirmative action requirements that all candidates be treated alike to interviewers&#039; needs to make brief phone calls or take breaks. The structures reflect a culture you as a candidate have chosen to enter; you have been selected from many others. Consider yourself a citizen of this world, observing its rituals and making your voice heard. 

Familiarity with the process will not change the fundamental issues of professional identity or the values of the interviewing committee, but it can help make the exchange more comfortable, civil, and candid. Most of all, cooperation with the protocols reduces your chances of feeling arbitrarily abused by the system. Understanding constraints on a search committee can prevent the moment of acute misgiving that distracts you from the task of self-presentation and interferes with dialogue. An interview can be, like any narrative, postmodern or providential&#151;and most parties to the occasion hope for the latter, less trendy qualifier. What follows is practical advice for maximizing the operation of the providential. 

Make the timing of the interview work for you. Confirm the hotel and room number of your interview well in advance, using the way the search committee suggests&#151;telephone when it seems unlikely that you will interrupt an interview in progress or get the room number at the appropriate desk in the MLA Job Information Center. 

Allow plenty of time to deal with delays in traffic and at hotel elevators, but do not knock on the door until the time designated, since interviews are closely scheduled. For the same reason, depart on cue. If the interviewers give you a packet and offer you your coat, don&#039;t linger. The interview procedure, rather than the intrinsic interest of your discussion, often determines the pacing. Let the search committee set the pace, and don&#039;t be put off if it seems fast. 

Before the interview, shape responses to predictable good questions, plan for bad ones, and consider questions of your own. Most search committees establish a set of questions to be asked in rotation. A good interview creates tension between committee members&#039; need for responses to set questions and their interest in pursuing further responses. Members may cut off some of your answers because you have done well, the committee is satisfied, and the cycle of questions must be completed. 

An institution&#039;s profile will determine its relative emphasis on scholarship, teaching, and professional service. Research institutions, large comprehensive institutions, and small liberal arts colleges differ in the size of the search committee, in their interviewers&#039; depth in your field, and, most of all, in their priorities. Be sensitive to an institution&#039;s stated or implied goals, and make reference to its stated goals where possible. But be careful about assumptions. Do not assume, for instance, that composition courses will border on the remedial or that you will be able to design a seminar focused on your dissertation topic. Along the same lines, do not hesitate to seek clarification of any question before answering it fully. 

Predictable questions fall into several categories. First, most interviews include a question about course design: How would you design a survey course or special seminar in your area of specialization? What primary and secondary texts would you include? How much theory would you consider appropriate for such-and-such a level? 

Second, almost all search committees wish you to discuss pedagogy, to explain your particular approach to, even your philosophy of teaching. This response must ring true; specifics from your experience will be important here. 

Third, questions on research will vary in depth and extent according to institution and committee makeup. You may be asked about the contribution of your research to the field, your use of theory, future areas of inquiry, the relation between your research and teaching, and the schedule for completion of your dissertation. 

Other questions may be institution-specific. You may be asked what you consider to be the goals of a liberal arts degree or what you have to offer <em>x </em>and its particular constituency?  Institutions that emphasize service may ask about your experience and interests. 

You might also consider responses to questions that should not be asked. Don&#039;t expect the inappropriate, but be prepared to put off a delinquent inquirer with a civil response. Usually such questions are asked by newcomers to the search process and cause distress to participating colleagues, who will not expect you to answer them. Still, if no one leaps in to dismiss the question, you must handle it. It is illegal to ask a job candidate about age, marital status, children, religion, sexual orientation, or national origin. Reveal what you choose but feel under no obligation to answer. Consider possible parrying responses: &#147;I didn&#039;t expect that question.&#148; &#147;I am not prepared to answer that question.&#148; &#147;I would rather address that question at some future time.&#148; &#147;Perhaps it is more important for you to know&#133;&#148; 

Toward the end of an interview you will be asked if you have questions. Your questions may pertain to the time frame for the subsequent search process, tenure expectations, research opportunities, faculty governance structures, student constituencies and performance, and the larger community environment of the institution. Do not ask for information you could find in catalogs and reference books: general student enrollment, department size, institutional philosophy, course offerings. Find out what you need to know without revealing a neglect of widely available material. The search committees may offer a packet that furnishes practical information about salary, benefits, and related employment issues. 

Strive for the most possible exchange. In spite of the tension and your eagerness to discuss your work and aspirations, be sure to listen carefully to the questions. Seek clarification. Don&#039;t be uncomfortable in silences that occur before or after you answer. The dynamics of an interview often induce candidates to overanswer, to speak out of nervous energy instead of thinking through a response. Silence, too, tells a story. 

Last and most important, regard the interview as a mutual inquiry. &#147;Fit&#148; cuts both ways and must involve the integrity of your own vision as it intersects with the goals of the search. Your interviewers may be future peers. Dialogue depends on the absence of condescension and servility. The providential may depend on your willingness to present yourself confidently and to listen attentively. Good luck. 


<em>The author is Associate Professor of English and Chair of the English Department at Spelman College and a member of the ADE Executive Committee. This paper was presented at the 1995 MLA convention in Chicago.</em>

<h2>Selected List of Works Consulted </h2>
Emmerson, Richard K. &#147;&#145;When Do I Knock on the Hotel Room Door?&#146;: The MLA Convention Job Interview.&#148; <cite>ADE Bulletin </cite>111 (1995): 4&#173;6. </a>

Gregory, Marshall. &#147;How to Talk about Teaching in the MLA Interview.&#148; <cite>ADE Bulletin </cite>111 (1995): 7&#173;8.  

Showalter, English. <cite>A Career Guide for PhDs and PhD Candidates in English and Foreign Languages </cite>. New York: MLA, 1994. 

Wilt, Judith. &#147;How to Talk about Scholarship in the MLA Interview.&#148; <cite>ADE Bulletin </cite>111 (1995): 9&#173;10.  


&#169; 1996 by the Association of Departments of English. All Rights Reserved.
