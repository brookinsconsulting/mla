<h1>Tool Kit on Shared Governance</h1>
<h2>What Is Shared Governance?</h2>
A long-standing consensus affirms that institutions of higher education operate on a system of “shared governance.” Whether public or private, colleges and universities are not simply run from the top by governing boards or by administrators—the faculty also makes decisions.  Campuses all across the country, on every scale and organized in widely different ways, do actually work this way, with a structural sharing of power.  Yet there is also a great deal of ambiguity about both principles and processes.  Should trustees, administrators, and faculty members each control specific realms?  When should they collaborate on decisions?  What does <i>collaborate</i> mean in this context?  

Governing boards vary in their level of engagement and the degree to which they affiliate with the administration, faculty, alumni, or student body of their institutions.  Administrators have primary responsibility for an institution’s budget, from fund-raising to salaries to financial aid.  Faculties usually make specifically academic decisions—controlling the curriculum, setting academic standards and graduation requirements, and taking the lead in faculty hiring decisions as well as decisions regarding promotion and tenure.  Most of the everyday activities on any campus depend on economic and scholarly or pedagogical concerns at the same time.  

<h2>Why Does It Matter?</h2>
Although there are many models of shared governance, all bear on decision making (everything from workload and compensation to intellectual property and syllabus contents) that enables institutions of higher learning to operate and defines the quality of professional life at such institutions. 

Put simply: virtually everything in our professional existence depends on the strength of shared governance.   

This is evident in the protocols governing decisions regarding promotion and tenure.  Someone who has a tenure-track job can expect, indeed can demand, that only those professionally competent to evaluate his or her performance be entrusted with the review and decision.  Note, however, that non-tenure-track faculty members, with increasingly precarious job status, now constitute a majority of the faculty members in higher education in the United States and Canada (see the MLA Committee on Contingent Labor in the Profession's <i>Professional Employment Practices for Non-Tenure-Track Faculty Members</i>). MLA members should simultaneously push back against this erosion of the autonomy of the faculty and insist that all colleagues—on and off the tenure track—participate in governance.  When structures and processes developed to negotiate meaningfully between administrative and faculty interests become ineffective, institutions’ capacity for self-determination is severely compromised.  Course requirements for undergraduate majors, admission of graduate students, and hiring of faculty members are activities affected by the robustness of the processes of shared governance. 

In the absence of shared governance, public institutions of postsecondary education in particular lose the means by which to insist that legislative and thus typically economic policy address itself to a fundamental and ever more urgent question: what <i>is</i> a university or college, and what broader historical and cultural purpose ought it serve? 

<h2>What to Do?</h2>
First, we must inform ourselves about how shared governance works at our universities, colleges, and community colleges.  What structures exist?  How well are they working?

Second, we should think of ourselves as citizens, not only of departments but also of our colleges and universities.  The “service” that we do is a vital form of stewardship, important for our workplaces and for higher education in general.  Because the institutions in which we work are subject to the structures and processes of shared governance, we need to take more seriously and value more highly the forms of service essential to maintaining and strengthening it.

Third, get involved.  There are many ways to become organized on these matters, from participating in governing bodies (assemblies, senates, and the like) to forming issue-based task forces or study groups.  Given what can and should be done, no effort is unimportant.  Without this engagement at the day-to-day level, the collaborative work of shared governance is dangerously weakened.   

<h2>What to Read?</h2>
Finkin, Matthew, and Robert Post.  <i>For the Common Good: Principles of American Academic Freedom</i>.  New Haven: Yale UP, 2009. Print.
	
MLA Committee on Contingent Labor in the Profession. <i>Professional Employment Practices for Non-Tenure-Track Faculty Members: Recommendations and Evaluative Questions</i>. <i>Modern Language Association</i>. MLA, June 2011. Web. 20 Dec. 2012.

“1966 Statement on Government of Colleges and Universities.” <i>American Association of University Professors</i>. AAUP, 15 Oct. 1966. Web. 20 Dec. 2012. 

Schrecker, Ellen. <i>The Lost Soul of Higher Education: Corporatization, the Assault on Academic Freedom, and the End of the American University</i>. New York: New, 2010. Print.

Tierney, William G., ed. <i>Competing Conceptions of Academic Governance: Negotiating the Perfect Storm</i>. Baltimore: Johns Hopkins UP, 2004. Print.

<i>This document was created by the Committee on Academic Freedom and Professional Rights and Responsibilities in April 2012.</i>
