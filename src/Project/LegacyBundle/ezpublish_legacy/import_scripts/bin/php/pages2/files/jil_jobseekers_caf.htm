<h1>Guidelines for Search Committees and Job Seekers on Entry-Level Faculty Recruitment and Hiring</h1>
<i>Prepared by the MLA Committee on Academic Freedom and Professional Rights and Responsibilities</i>

<h2>I. General Principles</h2>
<ol>
	<li>Timely, accurate, and open communication between candidates and departments can ensure an atmosphere of collegiality, even when the job market is tight or institutional circumstances are uncertain. Departments help create such an atmosphere when they recognize how vulnerable candidates may feel during a job search; candidates help when they recognize that departments may be affected by institutional policies largely beyond their control. All parties involved in a job search should conduct themselves professionally.</li>

	<li>All job candidates should be treated equitably. Departments should adhere to nondiscrimination and affirmative action guidelines, taking particular care not to discriminate on the basis of race, ethnic or national origin, religion, disability, age, gender, or sexual orientation. All parties need to respect principles of confidentiality.</li>

<li>Candidates should be evaluated on their merits, not on the completion date of their degree.</li>

<li>At every stage of the interview process, advance agreement should be established between the search committee members and the candidate regarding reimbursement, type of interview (screening, preliminary, final), format, and venue.</li>

<li>Search committee members should be aware that it may prove difficult for a candidate to abide by these guidelines if the hiring institution does not follow them.</li>
</ol>

<h2>II. Advertising and Initial Screening</h2>
<ol>
	<li>Advertisements for an opening should be as specific as possible about the availability of the opening (definite, likely, or possible), the type of appointment (tenure-track or non-tenure-track), minimum degree requirements, field(s) of expertise, minimum teaching experience, and any other requirements or criteria.</li>

	<li>Applicants should be allowed ample time to respond to advertisements of openings, and deadlines for applications should be specified whenever possible.</li>

	<li>Applications submitted in response to announcements should be acknowledged promptly and courteously. Following the initial screening, all applicants should be informed of their status and the department's projected timetable for making decisions about interviews at the MLA convention. </li>
</ol>

<h2>III. Preparing Applications</h2>
<ol>
	<li>The candidate should prepare a dossier, including a letter of application, curriculum vitae, transcript(s), and letters of recommendation, by mid-September, when the <i>JIL</i> becomes available for searching online. It is the candidate's responsibility to make sure that all requested materials are supplied. <i>Candidates may wish to designate a faculty adviser to review the completed dossier</i>.</li>

	<li>For the purpose of initial screening, a letter of application and dossier should normally suffice. Candidates should realize that the application process can be costly. To save all parties time and money, the committee recommends that departments request writing samples and other material only after a preliminary list of candidates has been chosen.</li>
</ol>

<h2>IV. Setting Up Screening Interviews</h2>
<ol>
	<li>Many departments invite candidates for screening interviews at the MLA convention or at other, field-specific, meetings (e.g., AWP for creative writing, SCMS for film studies, CCCC for rhetoric and composition). To ensure equal treatment and keep candidates’ expenses down, departments are strongly discouraged from selecting more candidates for screening than they can meet with during a professional conference. Any department that does hold screening interviews at a nonconference venue should cover candidates’ expenses for travel to that venue. Interviews should follow the same protocol whether held locally or at a conference.</li>
</ol>

<h2>V. Holding Screening Interviews at the MLA Convention</h2>
<ol>
<li>Departments advertising in the <i>Job Information List</i> normally expect candidates to attend the MLA convention for screening interviews. Candidates who do not attend the convention may therefore be at a disadvantage. In such cases, an interview by videoconference may be an appropriate alternative (see ADE and ADFL’s “Suggestions for Interviews Using Videoconferencing and the Telephone”). Convention attendance is still generally the most effective way to conduct interviews, and departments should make every effort to be represented at the convention by at least one member of the search committee.</li>

	<li>Departments need to be able to reach candidates quickly to schedule MLA convention interviews. Candidates, especially those who plan to travel during the holidays, should supply departments with contact information. Because of the expenses related to convention attendance, departments should notify all candidates, including those not invited for interviews, of their status as early as possible.</li>

	<li>Candidates applying from outside North America should have a contact in the United States to receive mail and messages. Since few departments have resources to bring candidates to on-campus interviews from outside North America, candidates who reside abroad should determine arrangements for any on-campus interviews during MLA convention interviews.</li>

<li>Departments need to be sure candidates know where the interview is taking place. The Job Information Center is set up to provide this information. If you plan to conduct interviews in the Job Information Center, you must come to the center in person and be assigned a table in the interview area. If you are interviewing in a hotel room, remember that hotel switchboard personnel are not authorized to disclose room numbers. The Job Information Center will provide either a cell phone contact number or hotel room number to candidates with whom your department has set up appointments, as you direct. To take advantage of this service, you need to register information about the location for MLA convention interviews by logging in to your department's <i>JIL</i> listings at the MLA Web site and filling out the online convention interview location form. Information may be entered and updated at any time between 1 November and the dates of the convention. Departments scheduled to begin interviewing on the first day of the convention may need to arrive the day before, to avoid missing appointments because of travel delays or delays in checking in to hotels.</li>

	<li>Departments and candidates should plan realistically and adhere closely to schedules. When arranging interviews, candidates should leave as much time as possible between appointments, keeping in mind that they may have to deal with crowded elevators, slow meal service, or delayed shuttle buses. Departments should remember that interviews that run late may prevent candidates from keeping other appointments and that one instance of lateness can multiply into a whole series of missed or delayed interviews.</li>

	<li>Whether held in person or by videoconference, interviews should be conducted in a professional manner, permitting candidates adequate opportunity to explain and demonstrate their qualifications. Candidates and departments should review <i>Dos and Don'ts for Interviews</i>, revised in 2012 by CAFPRR and available on the MLA Web site.</li>

	<li>Interviewers should make every effort to accommodate candidates with disabilities.</li>
</ol>

<h2>VI. Interviewing on Campus</h2>
<ol>
	<li>Departments inviting candidates for on-campus interviews should pay candidates' expenses, following standard institutional policies for travel reimbursement. Candidates should be told approximately how many others are being invited for on-campus interviews.</li>

	<li>On-campus interviews represent a large investment of time and money for departments; therefore, candidates should not accept on-campus interviews if they are not interested in the position. Before traveling to a campus, candidates should thoroughly research the department's faculty and programs. Candidates should determine whether a salary range and teaching load have been established for the position and should decide in advance what their own minimum requirements are. It is important that candidates also determine in advance whether their decisions may be influenced by special circumstances that should be communicated to the chair.</li>

	<li>A department that invites a candidate to interview on its campus has an obligation to (a) arrange the logistics of the candidate's stay (local transportation, lodging [including disability accommodations], meals); (b) set up interviews with faculty members and administrators; (c) accommodate candidates' special needs; (d) provide a tour of the campus and its facilities; (e) provide adequate information about the department, the university, and the community; (f) plan social activities for the candidate; and (g) inform the candidate of the procedures and timetable for reimbursement.</li>

	<li>Candidates and members of search committees should be aware that visits to a campus, meals and other social activities included, are all inherent parts of the interview and should be conducted accordingly.</li>

	<li>Members of departments and search committees should not discuss other candidates with a visiting candidate.</li>
</ol>

<h2>VII. Negotiating an Offer</h2>
<ol>
	<li>To minimize misunderstanding and anxiety during negotiations about offers, departments should explain the process to candidates before any offers are made. Departments should communicate with candidates regularly and openly about the status of the search process. All parties should be aware that, especially in times of fiscal uncertainty, circumstances beyond the institution's control may delay or disrupt the hiring process.</li>

	<li>No candidate should be required before 31 January to give a final answer to an offer of a position without tenure for the following academic year, however early an offer is tendered. </li>

<li>While informal offers can be communicated by e-mail or other forms of written text, a formal good-faith offer is a written document on official letterhead from an authorized representative of the institution. This document should stipulate the terms of appointment. Candidates should be afforded a minimum of two weeks following receipt to accept or reject a formal offer.</li>

<li>When a search has concluded, all candidates should be appropriately informed.</li>
</ol>

<i>The committee welcomes comments and suggestions from members. Direct correspondence to Staff Liaison, Committee on Academic Freedom and Professional Rights and Responsibilities, MLA, 26 Broadway, 3rd floor, New York, NY 10004-1789, or to <a href="mailto:nfurman@mla.org">nfurman@mla.org</a> or <a href="mailto:dgoldberg@mla.org">dgoldberg@mla.org</a>. Rev. May 2013.</i>
