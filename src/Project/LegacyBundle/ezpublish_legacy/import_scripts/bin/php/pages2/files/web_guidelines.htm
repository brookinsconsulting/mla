<h1>Guidelines for Authors of Digital Resources</h1>
These guidelines recommend best practices for authors and the minimal reference information that should be provided in digital resources intended for use by students, teachers, and scholars in the modern languages. This information will help authors create resources that can be easily discovered and used, fairly evaluated, and adequately cited. 
<h2>Authorship and Credit</h2>
Identify all individuals and groups responsible for the creation and maintenance of the resource. Include individuals' institutional affiliations when relevant. Information to be given might include the following:
<ul>
<li>Authors and researchers</li>
<li>Editors</li>
<li>Designers</li>
<li>Software developers and other collaborators</li>
<li>Institutions or organizations hosting the site</li>
<li>Funders</li>
<li>Contact information</li>
</ul>
<h2>Citations and Reuse</h2>
Offer appropriate citations for content quoted or republished in the resource. Include an explicit statement on further authorized uses of original content by appending a traditional copyright statement or <a href="http://creativecommons.org/" target="_blank">Creative Commons</a> or other license. If an explicit license is not employed, it may be advisable to do the following: 
<ul>
<li>Remind readers of their fair use rights and note whether users may republish media elements, data, or passages of text more substantial than normally permitted under fair use legislation.</li>
<li>Note to whom applications should be made for further use and permissions.</li> 
</ul>
<h2>Accessibility</h2>
Web resources should comply with appropriate institutional and statutory guidelines on accessibility, such as Title II of the Americans with Disabilities Act and Section 508 of the US Rehabilitation Act.
<h2>Metadata</h2>
Where applicable, digital resources should include the following:
<ul>
<li><b>Purpose.</b> Indicate the purpose(s) that informed the design of the resource.</li>
<li><b>Site-information tags.</b> To aid Web searches, include adequate metadata using applicable standards. This information may be provided in machine-readable format in file headers or in the full-text content of the site.</li>
<li><b>Site configuration.</b> Indicate whether a table of contents, site map, index, bibliography, or other apparatus accompanies the site and whether content is available through syndication.</li>
<li><b>Citations and permissions.</b> List all credits, acknowledgments, and permissions.</li>
<li><b>Software and hardware considerations.</b> Note hardware, software, and device requirements or browser features supported by the resource. Indicate any provision for readers with special needs (text-only versions, audio supplements, and so on).</li>
<li><b>Update and revision notices.</b> Consider using content management systems, versioning systems, and other approaches that provide the dates of original postings and their last update, clarify authorship, and make available a revision history. It is particularly important to provide revision histories for scholarly resources after they are mounted on a Web site to alert readers of changes that may have been made to a cited version of the Web site. Publicly available versioning systems for software and scholarly interfaces also help researchers determine which features were visible at the time of citation.</li>
<li><b>Archive notices.</b> If archives of earlier versions of Web texts or relevant software are maintained by the author(s), directions for accessing or requesting those archives should be included in the site information.</li>
</ul>
<h2>Structure and Style</h2>
<ul>
<li>Authors of scholarly writing on the Web should provide anchor tags or permalinks within texts of significant length; they should also consider numbering paragraphs or other sections so that exact locations may be cited.</li>
<li>For this and other matters relating to the citation of information on the Web, consult the latest edition of the <i>MLA Handbook for Writers of Research Papers</i>.</li>
<li>Whenever possible, creators of Web resources should favor content management systems and other approaches that provide for stable, persistent, consistently structured, and human-readable URLs.</li>
</ul>
<h2>Privacy Statement</h2>
If you collect information from users of your resource, beyond normal access and usage logs, you should include a statement of what information is collected and how it will be used. See the <a href="http://www.mla.org/privacy" target="_blank">MLA privacy statement</a>.

<h2>Security Issues</h2>
Web pages and underlying databases containing sensitive or confidential information should be protected against unauthorized access. Authors should adhere to institutional and statutory privacy guidelines with regard to student-produced content and student information.

<i>These guidelines were revised by the Committee on Information Technology in October 2012 and approved by the MLA Executive Council at its 22–23 February 2013 meeting. The committee’s original guidelines were approved by the MLA Executive Council in May 1999.</i>
