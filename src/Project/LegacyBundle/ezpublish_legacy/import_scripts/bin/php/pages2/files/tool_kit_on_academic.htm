<h1>Tool Kit on Academic Freedom</h1>
<h2>What Is Academic Freedom?</h2>
Academic freedom comprises a set of historically evolving ideals that affect institutions of higher education. It ensures professional autonomy, promising members of disciplinary and interdisciplinary communities freedom to determine the parameters of inquiry, research, and teaching. Professional peer-review practices are guaranteed by the principles of academic freedom to be protected from extraneous forms of religious, social, political, economic, or private controls. 

The term <i>academic freedom</i> emerged in nineteenth-century German universities, where the three basic principles were the freedom to teach, the freedom to learn, and the freedom to do research. These principles were adapted to different circumstances in the United States and Canada, but they have also circulated in many countries, affecting to varying degrees higher education in many nations of the world. The American Association of University Professors (AAUP), founded in 1915, has formulated the key statements pertinent to resolving the many legal cases involving academic freedom, most notably through the work of the influential Committee A on Academic Freedom and Tenure. The pivotal 1940 “Statement of Principles on Academic Freedom and Tenure” (“Statement”) significantly clarified the scope of these principles, and in 1962 the MLA endorsed this statement. Many other academic associations have likewise endorsed the 1940 statement, which is also regularly cited in legal cases involving academic freedom.

Academic freedom extends from the core peer-reviewed activities of research and teaching to include what are called intramural and extramural domains of speech. Intramural speech refers to all those arenas within a given institution where faculty members speak on matters of general concern beyond their specific fields of expertise, such as at faculty senate meetings or university-wide committee meetings. Extramural speech occurs when faculty members speak or write on larger political, social, or religious matters outside their local institutions.  

The right of scholars and teachers to be evaluated by their peers on the basis of their expertise in their fields should not be affected by their intramural or extramural speech. Academic freedom therefore carries considerable responsibility for faculty members. As the 1940 “Statement” articulates, faculty members must, when speaking or writing in public, make sure that they are not representing the views of their institution and, because they enjoy privileges to express their opinions freely without fear of institutional reprisal, that they show appropriate professional restraint. The 1940 “Statement” also stipulates that in the classroom faculty members should refrain from introducing controversial materials that have no relation to the assigned subjects. In its 1970 interpretation of this principle, the AAUP specifies that the “intent of this statement is not to discourage” the “controversy” at “the heart of free academic inquiry” but rather to limit any persistent intrusion into the classroom of material irrelevant to the subject being taught. 

The AAUP further specifies that when any specifically focused or religiously affiliated institution of higher education places limitations on the academic freedom of faculty members to teach certain subjects, these restrictions “should be clearly stated in writing at the time of appointment.”

Academic freedom is not, therefore, an individual right to speak as one wishes but derives from the professional collective responsibility of informed educators to determine what counts as knowledge in their disciplinary and interdisciplinary fields and how such knowledge should be disseminated in the classroom and beyond. Academic freedom should not be confused with the First Amendment rights to freedom of speech, although the domains may overlap, especially in cases of extramural utterances. In the United States, the First Amendment grants all citizens the right to speak freely so long as such speech does not violate criminal laws.  Academic freedom affects only those individuals conducting research at or teaching in educational institutions, and these principles ensure that the academic work of faculty members and students can only be assessed according to the standards of the profession. 

According to the 1940 “Statement,” academic freedom is a “common good” necessary for free inquiry and informed debate in a democratic society. The principles of academic freedom define the procedures of shared governance, peer-review professionalism, and tenure.  The AAUP has consistently affirmed that academic freedom pertains to all faculty members, whether adjunct, part-time, temporary, non-tenure-track, or tenure-track members of the profession. The 2009 <i>MLA Statement on Academic Freedom</i> likewise reaffirms that, in conjunction with other allied professional organizations, every institution of higher education should seek to ensure academic freedom for all ranks and categories of faculty members.  

Nevertheless, the practice of tenure remains crucial to the vitality of academic freedom. Tenure results from the set of institutional processes whereby, after a successful probationary period, faculty members are granted the rights of due process: tenured faculty members cannot be dismissed without adequate cause and without a hearing before a faculty committee. Tenure thus serves as the best practical guarantor of academic freedom. 

<h2>Why Does It Matter?</h2>
Loss of academic freedom diminishes the quality of education our students receive and the kind of knowledge produced in higher education. The principles of academic freedom in the twenty-first century must be adaptable to circumstances where boundaries between academic and nonacademic kinds of knowledge are more permeable. Telecommunications networks, multimedia environments, and digital archives also blur the boundaries between public and private domains of knowledge and between pure and applied kinds of research.

Who gets to define academic freedom, when it applies, and when it has been protected or violated are questions that have often been controversial, and there is a long judicial history of legal cases that have been decided on the basis of specific interpretations of academic freedom. Often these disputes have reached the mass media&mdash;for example, when controversial figures have been fired from their academic appointments, when private corporations have engaged in sponsored research projects carried out by university faculty members, or when cost-benefit analyses have focused exclusively on profit rather than on the educational values of faculty members and students.  

The past forty years have witnessed dramatic historical changes in the funding for higher education as well as in the composition of the higher education faculty.  As more universities become dependent on private research grants and corporate funding, universities run the risk of compromising academic freedom. Sponsored research projects can infringe on academic freedom when, for example, publication of unfavorable research results is suppressed by private interests.  When universities own patent rights and copyrights, they can sometimes limit the academic freedom of faculty members to disseminate the results of their research. Rigid standardization and assessment methods that focus only on short-term objectives can diminish academic integrity. Likewise, with the erosion of tenure as an academic value, university and college teachers are more vulnerable to violations of academic freedom. If teachers can be threatened with loss of employment because their research or teaching does not conform to private funding interests or because they publish or teach unpopular ideas, inquiry is not free.  

As a fundamental principle, academic freedom sustains the educational institutions whereby a citizenry is exposed to the risks and benefits of the freedom of thought. Academic freedom therefore matters to educators in all institutions because it models for us, for our society, the way a community of inquirers holds itself responsible for standards. 

<h2>What to Do?</h2>
The task shared by all educators is to draw on the historical archive of principles, policies, and legal cases regarding academic freedom, to redefine for our own age the fundamental grounds of education in a democracy. Faculty members must strive to create and sustain a shared set of principles and procedures. 

A first task is to become familiar with the national policies articulated by the AAUP (see bibliography below). At the same time, faculty members need to familiarize themselves with the legal documents regarding academic freedom at their specific institutions. Bargaining contracts, state and federal laws regarding employment, and intellectual property rights may all come into play.

Bear in mind:<blockquote>

•	The work of a disciplinary or interdisciplinary field should under no circumstances be defined by external considerations.

•	The erosion of tenure needs to be resisted at all levels, as tenure remains the single most tangible way of protecting academic freedom and higher education. 

•	The freedom of inquiry, research, and teaching for all faculty members, regardless of their position within or outside the tenure stream, is paramount.</blockquote>

<h2>What to Read?</h2>
<i>Academe</i> (bimonthly magazine published by the AAUP)

Carvalho, Edward J., and David B. Downing, eds. <i>Academic Freedom in the Post-9/11 Era</i>. New York: Palgrave/Macmillan, 2010. Print.

Finkin, Matthew W., and Robert C. Post. <i>For the Common Good: Principles of American Academic Freedom</i>. New Haven and London: Yale UP, 2009. Print.

Hofstadter, Richard, and Walter P. Metzger. <i>The Development of Academic Freedom in the United States</i>. New York: Columbia UP, 1955. Print.

<i>Journal of Academic Freedom</i> (published online by the AAUP: www.academicfreedomjournal.org/index.html)

Menand, Louis, ed. <i>The Future of Academic Freedom</i>. Chicago and London: U of Chicago P, 1996. Print.

<i>MLA Statement on Academic Freedom (2009</i>). <i>Modern Language Association</i>. MLA, Feb. 2009. Web. 20 Dec. 2012. 

Nelson, Cary.  <i>No University Is an Island: Saving Academic Freedom</i>.  New York: New York UP, 2010. Print.

Schrecker, Ellen. <i>The Lost Soul of Higher Education: Corporatization, the Assault on Academic Freedom, and the End of the American University</i>. New York: New P, 2010. Print.

<i>This document was created by the Committee on Academic Freedom and Professional Rights and Responsibilities in April 2012 and updated in January 2013.</i>
