<?php


set_time_limit(0);
ini_set('memory_limit', -1);

date_default_timezone_set('America/New_York');

require 'autoload.php';

$script = eZScript::instance(
	array(
		'description' => ''
	)
);

// set up the environment
$access = array(
	'name' => 'site_admin',
	'type' => eZSiteAccess::TYPE_DEFAULT,
	'uri_part' => array()
);

eZSiteAccess::change($access);
eZSiteAccess::reInitialise();

$cli = eZCLI::instance();

$user = eZUser::fetch(65);
$user->loginCurrent();

$db = eZDB::instance();

$content_root = 2;
$file_root = 43;
$image_root = 43;
$storage_dir = "var/storage/original/image/";

$domain = 'http://www.mla.org';
$imports = eZContentObjectTreeNode::subTreeByNodeID(
	array(
		'ClassFilterArray' => array('imported_page'),
		'ClassFilterType' => 'include'
	),
	$content_root
);

//$imports = array(eZContentObjectTreeNode::fetch(2518));


$count = count($imports);

foreach ($imports as $node_k => $node) {
    
    $this_node_id = $node->attribute('node_id');
    $dm = $node->dataMap();

	$ex_ob = $node->object();
	
	
	$title = $dm['page_title']->content();
	$newtitle = strip_tags($dm['page_title']->content());
	
	if ($title == $newtitle) {
		print_r(($node_k + 1) . " of " .count($imports). " - no change to ".$node->Name."\n");
		continue;
	}
	
	$attributes = array(
		'page_title' => $newtitle
	);

	eZContentFunctions::updateAndPublishObject(
		$ex_ob,
		array( 'attributes'=> $attributes)
	);
	
	
}




?>