<?php

include( '/usr/share/php/PEAR/phpQuery/phpQuery/phpQuery.php' );
include( '/home/david/sites/freshfromflorida/ezpublish_legacy/extension/indexstaticpages/classes/Encoding.php' );
include( '/home/david/sites/freshfromflorida/ezpublish_legacy/extension/indexstaticpages/classes/htmlfixer.class.php' );

$ArrayBBCode = array(
	'' => array(
		'type' => BBCODE_TYPE_ROOT
	),
	'url' => array(
		'type' => BBCODE_TYPE_OPTARG,
		'open_tag' => '<a href="{PARAM}">',
		'close_tag' => '</a>',
		'default_arg' => '{CONTENT}',
		'childs' => 'b,i'
	),
	'b'=> array(
		'type' => BBCODE_TYPE_NOARG,
		'open_tag' => '<strong>',
		'close_tag' => '</strong>',
		'childs' => 'i,u'
	),
	'i' => array(
		'type' => BBCODE_TYPE_NOARG,
		'open_tag' => '<i>',
		'close_tag' => '</i>',
		'childs' => 'b,u'
	),
	'u'=> array(
		'type' => BBCODE_TYPE_NOARG,
		'open_tag' => '<u>',
		'close_tag' => '</u>',
		'childs' => 'b,i'
	),
	'ul' => array(
		'type' => BBCODE_TYPE_NOARG,
		'open_tag' => '<ul>',
		'close_tag' => '</ul>',
		'childs' => 'li'
	),
	'li' => array(
		'type' => BBCODE_TYPE_NOARG,
		'open_tag' => '<li>',
		'close_tag' => '</li>'
	),
	'quote' => array(
		'type' => BBCODE_TYPE_NOARG,
		'open_tag' => '<blockquote>',
		'close_tag' => '</blockquote>'
	)
);

set_time_limit(0);
ini_set('memory_limit', -1);

date_default_timezone_set('America/New_York');

require 'autoload.php';

$script = eZScript::instance(
	array(
		'description' => ''
	)
);

// set up the environment
$access = array(
	'name' => 'site_admin',
	'type' => eZSiteAccess::TYPE_DEFAULT,
	'uri_part' => array()
);

eZSiteAccess::change($access);
eZSiteAccess::reInitialise();

$cli = eZCLI::instance();

$user = eZUser::fetch(65);
$user->loginCurrent();

$db = eZDB::instance();

$content_root = 2;
$file_root = 43;
$image_root = 43;
$storage_dir = "var/storage/original/image/";

$domain = 'http://www.mla.org';
$imports = eZContentObjectTreeNode::subTreeByNodeID(
	array(
		'ClassFilterArray' => array('imported_page'),
		'ClassFilterType' => 'include'
	),
	8016
);

//$imports = array(eZContentObjectTreeNode::fetch(2517));


$count = count($imports);

foreach ($imports as $node_k => $node) {
    
    $this_node_id = $node->attribute('node_id');
    
    print_r("Processing $this_node_id\r\n");
    
    $dm = $node->dataMap();
    $file = $dm['file_name']->content();
    
    if (!$raw_description = file_get_contents("extension/import_scripts/bin/php/pages2/files/$file.htm")) continue;
    
    $original_html = $raw_description;
    
    //if (strpos($original_html, "<@") !== false) continue;
        
    //$fixer = new HtmlFixer();
	//$content = $fixer->getFixedHtml($raw_description);
	
	$content = $raw_description;
	
	$content = nl2br($content);
	
	$BBCHandler = bbcode_create($ArrayBBCode);
	$content = bbcode_parse($BBCHandler, $content);
	//$content = preg_replace("/(.+)\n/", "<p>$1</p>\n", $content);
	$content = preg_replace("/\<\/ul\>(.+)/", "</ul><p>$1</p>", $content);
	$content = preg_replace("/\<blockquote\>/", "<custom name=\"quote\">", $content);
	$content = preg_replace("/\<\/blockquote\>/", "</custom>", $content);
	$content = preg_replace_callback("/href ?= ?[\"|']([^\"|']*)[\"|']/", function($matches){if (strpos($matches[1], "@") > 0) {return str_replace($matches[1], "#".urlencode($matches[1]), $matches[0]); } else {return $matches[0];}}, $content);

	$content = preg_replace_callback("/\<@[^>]*\>/", function($matches) {return "<custom name=\"logic\">".htmlentities($matches[0])."</custom>";}, $content);
	$content = preg_replace("/(href\s?=\s?['|\"])([a-zA-Z])/", "$1./$2", $content);
	$content = str_replace("./http", "http", $content);
	
	$content = preg_replace("/<strike([^>]*)\/?>/", "", $content);
	$content = str_replace("</strike>", "", $content);
	
		
	$content = preg_replace("#<script(.*?)>(.*?)</script>#is", "", $content);
	$content = preg_replace("#<style(.*?)>(.*?)</style>#is", "", $content);
	
	preg_match_all("/<img([^>]*)\/?>/", $content, $im_matches);

	foreach($im_matches[1] as $im_key => $m) {
	    
	    print_r("Testing images $m\n");
		
		$params = ats_to_params($m);

		if (!isset($params['SRC'])) {
			print_r("Missing image src! $m\n");
			continue;
		}
		
		$abs_path = absolutize($params['SRC'], $domain, '/');
		
		//if (strpos($abs_path, 'http') === 0) {
		//	print_r("Non local asset! $abs_path\n");
		//	die();
		//}
		
		print_r($params);
		
		$getme = str_replace("./", "/", (strpos($abs_path, 'http') === 0) ? $abs_path: $domain.$abs_path);
		$getme = preg_replace("/([^:])\/\/+/","$1/",$getme);

		$myname_r = array_reverse(explode("/", $abs_path));
		$myname = $myname_r[0];
	
		
		$check_already_imported = "SELECT ezcontentobject_attribute.contentobject_id as contentobject_id, path_string from ezcontentobject_attribute, ezcontentobject_tree, ezcontentobject where ezcontentobject.id = ezcontentobject_attribute.contentobject_id and ezcontentobject.current_version = ezcontentobject_attribute.version and ezcontentobject_tree.contentobject_id = ezcontentobject_attribute.contentobject_id AND (path_string like '%/$content_root/%' OR path_string like '%/$image_root/%') and contentclassattribute_id in (251) and data_text like '%original_filename=\"".addslashes($myname)."\"%'";
		
		$db_results = $db->arrayQuery($check_already_imported);
		
		if (count($db_results) == 0) {
		    
		    print_r("$getme MISSING!");
		
			$imname = array_shift(explode(".", $myname));
			$attributesData = array();
			$attributesData['name'] = $imname;
			$attributesData['original_url'] = $abs_path;
			$attributesData['image'] = $myname;
			$attributesData['original_alt'] = $params['ALT'];
		
			$newObject = pub_remote_asset($myname, $getme, $storage_dir, 7949, $attributesData, 27);
		
			if ($newObject === false) {
				print_r("Image creation failed! $abs_path");
				continue;
			}
		
			$new_id = $newObject->ID;
		
		} else {
			
			print_r("Updating image tag!");
			$new_id = $db_results[0]['contentobject_id'];
		
		}
		
		$new_tag = '<embed-inline view="embed-inline" size="original" object_id="'.$new_id.'"/>';
		
		$content = str_replace($im_matches[0][$im_key], $new_tag, $content);
		
	}
		
	$parser = new eZOEInputParser();
	$document = $parser->process( $content );

	$dataString = eZXMLTextType::domString( $document );
	
	$ex_ob = $node->object();
	$attributes = array(
		'body' => $dataString,
		'original_html' => $original_html
	);
	
	$orig_content = $dm['body']->content()->XMLData;
	
	//print_r($attributes);
	
	if ($dataString == $orig_content) {
		print_r(($node_k + 1) . " of " .count($imports). " - no change to ".$node->Name."\n");
		continue;
	}
	
	
	eZContentFunctions::updateAndPublishObject(
		$ex_ob,
		array( 'attributes'=> $attributes)
	);
	
	
}



















function ats_to_params($atstring) {
	preg_match_all('/=(\s*)?"[^"]*"/', $atstring, $morematches);
	foreach($morematches[0] as $mm) {
		$atstring = str_replace($mm, $mm."||", $atstring);
	}
	$params_r = explode("||",$atstring);
	$params = array();
	foreach($params_r as $p) {
		$split = explode("=", $p);
		$key_proc = explode(" ", $split[0]);
		$key = array_pop($key_proc);
		if (count($split) > 1 && $split[0] != '') $params[strtoupper($key)] = trim($split[1], '"/>');
	}
	
	return $params;
}

function absolutize($url, $domain, $cur_path) {
    $url = str_replace('IMAGES/', 'images/docstudio/', $url);
	$url_p = parse_url($url);
	$url = (isset($url_p['path'])) ? $url_p['path'] : "";
	$pre = '';
	if (isset($url_p['host'])) $pre = $url_p['host'];
	if (isset($url_p['scheme'])) $pre = $url_p['scheme']."://".$pre;
	$url = $pre.$url;
	if (strpos($url, 'http') === 0) return $url;
	if (strpos($url, '/') !== 0) {
		$url_r = explode("/", $cur_path."/".$url);
		$fin_r = array();
		foreach ($url_r as $u) {
			if ($u == "..") {
				$trash = array_pop($fin_r);
			} elseif ($u != "." && $u != "") {
				$fin_r[] = $u;
			}
		}
		$url = "/" . implode("/", $fin_r);
	}
	return $url;
}

function pub_remote_asset($myname, $getme, $storage_dir, $storage_node, $attributesData, $class_identifier) {
	if (!file_exists($storage_dir . $myname))  {
		$getfile = file_get_contents($getme);
		if ($getfile) {
			file_put_contents($storage_dir . $myname, $getfile);
		} else {
			print_r("Could not grab asset!\n");
			return false;
		}
	}


	$newObject = eZContentFunctions::createAndPublishObject(
		array(
			'parent_node_id' => $storage_node,
			'class_identifier' => 'image',
			'creator_id' => 65,
			'storage_dir' => $storage_dir,
			'attributes' => $attributesData
		)
	);
	
	return $newObject;
}


?>