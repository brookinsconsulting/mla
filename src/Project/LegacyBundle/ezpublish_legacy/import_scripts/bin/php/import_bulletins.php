<?php

set_time_limit(0);
ini_set('memory_limit', -1);

date_default_timezone_set('America/New_York');

require 'autoload.php';

$script = eZScript::instance(
	array(
		'description' => ''
	)
);

// set up the environment
$access = array(
	'name' => 'site_admin',
	'type' => eZSiteAccess::TYPE_DEFAULT,
	'uri_part' => array()
);

eZSiteAccess::change($access);
eZSiteAccess::reInitialise();

$cli = eZCLI::instance();

$user = eZUser::fetch(65);
$user->loginCurrent();

$db = eZDB::instance();

$content_root = 5719;

$headings = array("JOURNAL","ISSN","ARTICLE ID","ARTICLE DOI","ARTICLE SICI","ISSUE VOLUME","ISSUE NUMBER","ISSUE TITLE","ISSUE SEASON","ISSUE YEAR","COPYRIGHT YR","PAGE FIRST","PAGE LAST","PAGE CNT","ARTICLE AUTHOR","ARTICLE TITLE","ARTICLE SUBHEAD1","ARTICLE SUBHEAD2","ARTICLE SUBHEAD3","ARTICLE CATEGORY","ARTICLE LINK","ACCESS");

$new_headings = array();
foreach ($headings as $k => $h) {
	$new_h = preg_replace("/[^A-Za-z0-9]/", "_", $h);
	$new_headings[] = strtolower($new_h);
}


$file_array = file('extension/import_scripts/bin/php/ade_data.txt');

foreach ($file_array as $line_number =>$line)
{
	preg_match('/"[^"]*"/', $line, $matches);
	
	foreach ($matches as $m) {
		$line = str_replace($m, str_replace(",", "qqq", $m), $line);
	}
	
	$line = str_replace(",", '[tab]', $line);
	$line = str_replace("qqq", ',', $line);
	$line = str_replace('"', '', $line);
	
	$data_r = explode("|", trim($line));
	
	$test = "select * from ezcontentobject, ezcontentobject_attribute where contentclassattribute_id = 412 and ezcontentobject.id = ezcontentobject_attribute.contentobject_id and version=current_version and data_text ='".$data_r[2]."'";
	$rows = $db->arrayQuery($test);
	print_r("testing ".$data_r[2]."\r\n");
	if (count($rows) > 0) continue;
	
	$attributes = array();
	foreach($new_headings as $k => $h) {
		if ($h != "_" && array_key_exists($k, $data_r)) {
			$thisdata = $data_r[$k];
			if ($h == 'created' || $h == 'last_edited') {
				$thisdata = strtotime($thisdata);
			}
			$attributes[$h] = $thisdata;
		}
	}
	

	
	print_r(".");
	die();
	//die();

	
	$newObject = eZContentFunctions::createAndPublishObject(
		array(
			'parent_node_id' => $content_root,
			'class_identifier' => 'bulletin',
			'creator_id' => 65,
			'attributes' => $attributes
		)
	);

	
}

?>