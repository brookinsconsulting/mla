<?php

set_time_limit(0);
ini_set('memory_limit', -1);

date_default_timezone_set('America/New_York');

require 'autoload.php';

$script = eZScript::instance(
	array(
		'description' => ''
	)
);

// set up the environment
$access = array(
	'name' => 'site_admin',
	'type' => eZSiteAccess::TYPE_DEFAULT,
	'uri_part' => array()
);

eZSiteAccess::change($access);
eZSiteAccess::reInitialise();

$cli = eZCLI::instance();

$user = eZUser::fetch(65);
$user->loginCurrent();

$db = eZDB::instance();

$content_root = 1;

$q = 'SELECT *  FROM ezcontentobject_tree, ezcontentobject, ezcontentobject_attribute WHERE ezcontentobject_tree.contentobject_id = ezcontentobject.id and ezcontentobject.id = ezcontentobject_attribute.contentobject_id AND version = current_version AND contentclassattribute_id =384 AND data_text LIKE "<a href=\'http://mla.ssdstage.thinkcreativeinternal.net/content/download/%"';
$rows = $db->arrayQuery($q);

foreach ($rows as $row) {
    
    preg_match("/content\/download\/([0-9]*)/", $row['data_text'], $matches);
    
    $new_ob_id = (int)$matches[1];
    $ob = eZContentObject::fetch($new_ob_id);
    
    $cur_ob = eZContentObject::fetch($row['contentobject_id']);
    
    $old_dm = $cur_ob->dataMap();
    $attributes = array('import_page_id' => $old_dm['id']->content(),
                        'description' => $old_dm['description']->content(),
                        'tags' => $old_dm['keywords']->content(),
                        'authors' => $old_dm['authors']->content(),
                        'created' => $old_dm['created']->content()->timestamp(),
                        'last_edited' => $old_dm['last_edited']->content()->timestamp(),
    
    );
    print_r($attributes);
    eZContentFunctions::updateAndPublishObject(
		$ob,
		array( 'attributes'=> $attributes)
	);
	
	eZContentObjectTreeNode::removeSubtrees( array( $row['main_node_id'] ), false );
    
}


die();

$count = count($imports);

foreach ($imports as $node_k => $node) {
	
	print_r("processing ".$node->attribute('node_id')." ($node_k of $count)\n");
	
	$pnid = $node->attribute('parent_node_id');
	
	$dm = $node->dataMap();
	
	$parent_section_orig = $dm['parent_section']->content();
	
	if ($parent_section_orig == '100000000000') continue;
	
	if ($parent_section_orig == 0) {
		$parent_section = 2509;
	} else {
		$q = "select main_node_id from ezcontentobject_tree, ezcontentobject, ezcontentobject_attribute where contentclassattribute_id in (324, 385) and version = current_version and ezcontentobject_tree.contentobject_id =  ezcontentobject.id and ezcontentobject.id  = ezcontentobject_attribute.contentobject_id and data_text = '$parent_section_orig'";

		$rows = $db->arrayQuery($q);

		if (count($rows) == 0) {
			print_r($q);
			print_r($rows);
			$parent_section = 2510;
		} else {
			$parent_section=$rows[0]['main_node_id'];
		}

	}
	
	$new_parent_node = eZContentObjectTreeNode::fetch($parent_section);

	
	if (is_object($new_parent_node) && $pnid != $new_parent_node->attribute('node_id')) {
		print_r("Moving...\n");
		if ( eZOperationHandler::operationIsAvailable( 'content_move' ) )
		{
		    $operationResult = eZOperationHandler::execute( 'content',
		                                                    'move', array( 'node_id'            => $node->attribute('node_id'),
		                                                                   'object_id'          => $node->attribute('contentobject_id'),
		                                                                   'new_parent_node_id' => $new_parent_node->attribute('node_id') ),
		                                                    null,
		                                                    true );
		}
		else
		{
		    eZContentOperationCollection::moveNode( $node->attribute('node_id'), $node->attribute('contentobject_id'), $new_parent_node->attribute('node_id') );
		}
	} else {
		if (is_object($new_parent_node)) {
			print_r("Staying...\n");
		} else {
			print_r("OOPS! -- problem with ".$node->attribute('node_id')."...\n");
		}
	}
	
}

?>