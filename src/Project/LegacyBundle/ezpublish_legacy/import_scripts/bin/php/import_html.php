<?php

set_time_limit(0);
ini_set('memory_limit', -1);

date_default_timezone_set('America/New_York');

require 'autoload.php';

$script = eZScript::instance(
	array(
		'description' => ''
	)
);

// set up the environment
$access = array(
	'name' => 'site_admin',
	'type' => eZSiteAccess::TYPE_DEFAULT,
	'uri_part' => array()
);

eZSiteAccess::change($access);
eZSiteAccess::reInitialise();

$cli = eZCLI::instance();

$user = eZUser::fetch(65);
$user->loginCurrent();

$db = eZDB::instance();

$content_root = 1;

$imports = eZContentObjectTreeNode::subTreeByNodeID(
	array(
		'ClassFilterArray' => array('imported_section'),
		'ClassFilterType' => 'include'
	),
	$content_root
);

foreach ($imports as $node_k => $node) {
	
	print_r("processing ".$node->attribute('node_id')."\n");
	
	$dm = $node->dataMap();
	
	$name = $dm['page_title']->content();
	
	if (1 == 1) {
		
		$ex_ob = $node->object();
		$attributes = array('page_title' => trim($name));

		eZContentFunctions::updateAndPublishObject(
			$ex_ob,
			array( 'attributes'=> $attributes)
		);
		
	} else {
		print_r("MISSING! extension/import_scripts/bin/php/pages/$name.htm");
	}
	
	
	
}

?>