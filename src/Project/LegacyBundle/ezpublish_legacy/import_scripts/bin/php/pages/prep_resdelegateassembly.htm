<h1>Preparing Resolutions for the Delegate Assembly</h1>
Preparing resolutions for the MLA Delegate Assembly is a complex process.  This guide sets forth the steps of the process to help prevent oversights and omissions that can impede a resolution's progress.

Members should note at the outset, however, that a resolution is not the only—and may not be the best—way to accomplish a desired end.  Motions and requests addressed directly to the Executive Council should be considered.  The members of the <a href="/comm_delegate">Delegate Assembly Organizing Committee</a> welcome the opportunity to advise members on how to go about bringing an issue to the attention of others in the organization.

There are two kinds of resolutions, those received in a timely fashion (or regular resolutions) and emergency resolutions.  They differ significantly in some respects.  This guide was written with regular resolutions in mind, but most of the information applies to emergency resolutions as well.  When emergency resolutions differ from regular resolutions, shaded boxes are used to highlight the differences.

<h2>Calendar of Dates and Deadlines</h2>
<b>Year 1</b>
<table width=100% border="0" cellpadding="6">
<TR><td align="right" valign="top" width="18%">1 October</td><td width="3%"> </td><td width="69%" valign="top">Deadline for receipt of resolutions submitted in a timely fashion (see <a href="#preparing">Preparing the Content and Meeting Other Submission Requirements</a> and <a href="#meeting">Meeting the 1 October Deadline</a>, below, for additional information)
<table width=100% border="0" cellpadding="0"><TR bgcolor="#f4edda"><td valign="top">Emergency resolutions provide the members of the MLA with an opportunity to respond to situations that arise after the 1 October deadline.  The deadline for receipt of emergency resolutions is 1:00 p.m. on the second day of the convention.  This deadline falls during the Open Hearing on Resolutions (see below).</td></tr></table></td></tr>
<TR><td align="right" valign="top" width="18%">
last week of October</td><td width="3%"> </td><td width="69%" valign="top">Delegate Assembly Organizing Committee (DAOC) meets to review resolutions received by 1 October.
<table width=100% border="0" cellpadding="0"><TR bgcolor="#f4edda"><td valign="top">The DAOC will also review emergency resolutions submitted after 1 October but before the committee meets.</td></tr></table></td></tr>
<TR><td align="right" valign="top" width="18%">1–15 November	</td><td width="3%"> </td><td width="69%" valign="top">Period during which proposers can revise resolutions reviewed by the DAOC and modify or supplement the background information (see the subsection <a href="#before">Before the Delegate Assembly Meeting</a> in the section Monitoring the Resolution's Progress)</td></tr>
<TR><td align="right" valign="top" width="18%">early December</td><td width="3%"> </td><td width="69%" valign="top">Resolutions reviewed by the DAOC are sent to Delegate Assembly members along with background information provided by proposer(s).</td></tr></td></table>

<b>Year 2</b>
<table width=100% border="0" cellpadding="6">
<TR><td align="right" valign="top" width="18%">Day 2 of MLA convention, 12:00 noon–1:15 p.m.</td><td width="3%"> </td><td width="69%" valign="top"><ul type="disc"><li>Open Hearing on Resolutions is held during the MLA convention; all MLA members may attend and speak.</li>
	<li>Emergency resolutions must be submitted no later than 1:00 p.m.</li>
	<li>All resolutions received by 1 October and any emergency resolutions received are discussed by attendees.</li></ul></td></tr>
<TR><td align="right" valign="top" width="18%">Day 2 of MLA convention, 1:45&nbsp;p.m.</td><td width="3%"> </td><td width="69%" valign="top">DAOC meets to determine the status of any emergency resolutions submitted after the committee's October meeting and to formulate its recommendations on all resolutions that will be considered by the Delegate Assembly.</td></tr>
<TR><td align="right" valign="top" width="18%">Day 3 of MLA convention, 1:00&nbsp;p.m.</td><td width="3%"> </td><td width="69%" valign="top">Delegate Assembly meeting; each resolution, in turn, is introduced on the assembly floor by a DAOC member for discussion and vote.
<table width=100% border="0" cellpadding="0"><TR bgcolor="#f4edda"><td valign="top">An emergency resolution cannot be discussed and voted on unless the assembly first votes to consider it.</td></tr></table></td></tr>
<TR><td align="right" valign="top" width="18%">last week of February</td><td width="3%"> </td><td width="69%" valign="top">Executive Council meets to conduct the constitutionally required review of all resolutions approved by the Delegate Assembly; the council will either forward a resolution to the membership for ratification or return it to the Delegate Assembly along with a report on the reasons for the council's decision to return the resolution (see MLA constitution, <a href="/mla_constitution#a7B3">art. 7.B.3</a>).</td></tr>
<TR><td align="right" valign="top" width="18%">mid-March or mid-September</td><td width="3%"> </td><td width="69%" valign="top">Resolutions that the membership is asked to ratify are posted at the MLA Web site for comment; members are notified about the comment process by e-mail or regular mail and, if possible, through an article in the <i>MLA Newsletter</i> (see Resolution Comment Process, <a href="#resolcommentproc">below</a>).</td></tr>
<TR><td align="right" valign="top" width="18%">mid-April–1 June or late October–10 December</td><td width="3%"> </td><td width="69%" valign="top">Ratification-vote balloting period</td></tr>
<TR><td align="right" valign="top" width="18%">1 June or 10&nbsp;December</td><td width="3%"> </td><td width="69%" valign="top">Deadline for receipt of ratification ballots</td></tr>
<TR><td align="right" valign="top" width="18%">
postratification</td><td width="3%"> </td><td width="69%" valign="top">Any actions stipulated in ratified resolutions are carried out; the results of the balloting are published in the <i>MLA Newsletter</i>; resolutions ratified by the membership are posted at the MLA Web site (click <a href="/mla_resolutions">here</a> for Resolutions Ratified by the Membership).</td></tr></table>
<a name="preparing"></a>
<h2>Preparing the Content and Meeting Other Submission Requirements</h2>
<ol><li>The content of a resolution must be consistent with the purposes of the MLA, as set forth in <a href="/mla_constitution#a2">article 2</a> of the constitution.</li><BR><BR>
<li>The content of a resolution is limited to "matters of public and institutional policy affecting the study and teaching of the humanities and the status of the language and literature professions represented by the association.  Such matters may include proposed or enacted legislation, regulations, or other governmental and institutional policies, conditions of employment and publication, or additional matters that affect the association, its members in their professional capacities, or the dignity of members' work" (MLA constitution, <a href="/mla_constitution#a9C10">art. 9.C.10</a>).
<table width=100% border="0" cellpadding="0"><TR bgcolor="#f4edda"><td valign="top">Emergency resolutions may not name individuals or institutions in such a way that, in the determination of the DAOC, a response from the named party must be sought.  Since the time available for making this determination is short and since the consequences of misjudgment may be serious, the committee usually applies rigorous standards.</td></tr></table></li>

<li>The resolution may not contain erroneous, tortious, or possibly libelous statements.</li><BR><BR>
<li>The resolution, if adopted, must not pose a threat to the association's operation as a tax-exempt organization.</li><BR><BR>
<li>The entire text of the resolution is limited to 100 words.</li><BR><BR>
<li>The resolution must be accompanied by "material that provides evidence in support of the resolution['s] claims" (MLA constitution, <a href="/mla_constitution#a11C3b">art. 11.C.3.b</a>).  The main criteria here are, of course, the relevance and strength of the support, but it is a good idea to keep in mind both the amount of material that members of the DAOC and members of the assembly may reasonably be expected to read and the association's cost in making the supporting material available to assembly members. In recent years, the DAOC has limited to twenty-four single-sided pages the amount of supporting material that delegates are asked to consider.  Proposers of resolutions who choose to submit more voluminous documentation to the DAOC should therefore be prepared to respond quickly to the DAOC's likely request for a selection of documents that will be transmitted to delegates.</li><BR><BR>
<li>The resolution must be accompanied by the signatures of at least ten current members of the association.
<table width=100% border="0" cellpadding="0"><TR bgcolor="#f4edda"><td valign="top">Emergency resolutions must be accompanied by the signatures of at least twenty-five current members of the association.</td></tr></table></li>

<li>The persons whose signatures are submitted must be members in good standing for the current membership year.  (For example, only those who are 2014 members may provide a signature in support of a resolution submitted for consideration by the Delegate Assembly in January 2015.)  Signatures may be submitted in any of the following ways:</li>
<ul type="disc"><li>as an original signature or a collection of signatures mailed with the resolution;</li>
<li>as an individual signature or a collection of signatures faxed with the resolution text, with the original of the fax forwarded to the MLA office within seven business days; or</li>
<li>as an individual or a collective statement of support e-mailed with the resolution text, with printouts of the original e-mail messages signed by each originator or supporter and forwarded to the MLA office within seven business days.</li></ul>
</li></ol>

<h2>Following Conventional Form</h2>
<ol><li>A resolution consists of a resolved clause or clauses that set forth the resolution's objectives.  It may also contain a preamble that sets forth the reason or reasons for the resolved clause or clauses.  A preamble is, in essence, free debate; that is, it puts before the voting body some of the arguments in support of the resolution in advance of the meeting, during which debate is strictly limited. However, a preamble has some disadvantages.  It may work against adoption if members agree with the resolved clauses but do not agree with the preamble or find the content of the preamble overstated or poorly expressed. Also, of course, a preamble uses some of the 100-word limit imposed on MLA resolutions and may therefore interfere with the careful and complete statement of the resolved clauses.</li><BR><BR>
<li>Writers of resolutions should avoid references to unknowable future events and activities since a resolution should not commit the association to a particular course of action in the absence of clear knowledge of a situation. By the same token, writers of resolutions should state facts, not opinions.</li><BR><BR>
<li>The MLA conventionally uses the following form for a resolution without a preamble.</li><BR>
<blockquote>&nbsp;&nbsp;Be it resolved that ____________________________; and

&nbsp;&nbsp;Be it further resolved that ______________________.</blockquote>

<li>The MLA conventionally uses the following form for a resolution with a preamble.</li><BR>
<blockquote>&nbsp;&nbsp;Whereas ___________________________________;

&nbsp;&nbsp;Whereas ___________________________________; and

&nbsp;&nbsp;Whereas ___________________________________;

&nbsp;&nbsp;Be it resolved that ____________________________; and

&nbsp;&nbsp;Be it further resolved that ______________________.</blockquote>

<li>"Be it resolved that" should be followed by verbs in the subjunctive form.</li></ol><a name="meeting"></a>
<h2>Meeting the 1 October Deadline</h2>
<ol><li>Resolutions plus supporting materials plus original or faxed signatures or e-mailed statements of support must reach the association office by 1 October.</li><BR><BR>
<li>Originals of faxed signatures and signed printouts of e-mailed statements of support must be received by the association office within seven business days after 1 October.</li></ol>
See <a href="submit_checklist">Checklists for Submitting Resolutions</a> for more detailed information.

<a name="monitor"><h2>Monitoring the Resolution's Progress</h2></a> <b><a name="before"></a>Before the Delegate Assembly Meeting</b>
<ol><li>Before it comes to the floor of the Delegate Assembly, a resolution goes to the DAOC, where</li>
<ul type="disc"><li>the coordinator of governance confirms that all submission requirements (those having to do with deadlines, word limit, and supporting signatures) were met;</li>
<li>judgments are made regarding compliance with the content requirements set forth in the MLA constitution (see items 1 and 2 under Preparing the Content, <a href="#preparing">above</a>);</li>
<li>supporting materials are reviewed for sufficiency and relevance; and</li>
<li>a member of the DAOC with special interest or expertise is assigned to present the resolution on the floor of the assembly.  (Note: In cases where it is deemed helpful, a member of the DAOC will contact the proposer of a regular resolution with suggestions for revision that, in the judgment of the committee, increase the measure's chances of adoption.  The DAOC also provides the proposer with a deadline by which the proposed revision must be submitted.  Whether to make the suggested revision is totally at the discretion of the proposer.)</li>
</ul></li>
<li>A hearing exclusively for the consideration of resolutions is held during the MLA convention on the day before the Delegate Assembly meets.  The hearing lasts for an hour and a quarter (12:00 noon to 1:15 p.m.), the time being divided among the proposed resolutions.  It is a good idea for proposers to attend the hearing to briefly set forth the arguments in favor and to hear the responses of other members to the resolution.</li>

<li>The DAOC holds a closed meeting to consider the comments made on each resolution during the open hearing and to determine its recommendation to the Delegate Assembly.  The MLA constitution (<a href="/mla_constitution#a11C5">art. 11.C.5</a>) requires the DAOC to present each resolution to the assembly with a recommendation that the resolution be adopted or that it not be adopted.</li></ol>

<b>During the Delegate Assembly Meeting</b>
<ol><li>Resolutions are taken up under the heading New Business.  Each resolution is presented by a member of the DAOC, who also states the committee's recommendation regarding the adoption of the resolution.  The floor is then opened for debate of the resolution.
<table width=100% border="0" cellpadding="0"><TR bgcolor="#f4edda"><td valign="top">Before an emergency resolution can be debated, the assembly must first vote, by a three-quarters majority, to consider the resolution.</td></tr></table></li>

<li>Both delegates and MLA members who attend the meeting as observers may speak during the debate.  The proposer should be at a microphone ready to speak as soon as the measure is put before the assembly.  Since each member may speak to the measure only twice for three minutes, the proposer's statement should be carefully planned so that the most persuasive points are made first and all points are succinctly expressed.  The proposer's second speech should be reserved for the end of the debate period so that it can be used to respond to opponents' arguments.</li><BR><BR>
<li>The resolution is put to a vote. A majority vote is required for passage.</li></ol>

<b>After the Delegate Assembly Meeting</b>
<ol><li>Resolutions adopted by the Delegate Assembly are sent to the Executive Council.  Because the Executive Council has fiduciary responsibility for the association, its primary concern in considering resolutions is to ensure that the measures do not have the potential to damage the MLA financially or legally.  Thus, the council directs its attention chiefly to the criteria set forth in numbers 1, 2, 3, and 4, under Preparing the Content, <a href="#preparing">above</a>.</li><BR><BR>
<li>Normally the Executive Council approves a resolution and sends it forward to the general membership with no changes or with only nonsubstantive modifications.  More rarely, the Executive Council decides that it cannot send a resolution forward because it fails to meet the criteria listed above.</li><BR><BR>
<li>A ballot on those resolutions sent forward by the Executive Council is made available to the general membership in conjunction with the resolution comment process (see next section). Ratification of a resolution is by a majority vote in which the number of those voting in favor of ratification equals at least ten percent of the association's membership. If the membership ratifies the resolution, the association publicizes that fact and implements any action that may have been called for by the measure.</li></ol>

<b>At the Next Meeting of the Delegate Assembly</b>
<ol><li>The executive director, in his or her report to the Delegate Assembly, announces any action taken as a result of the ratification of a resolution by the membership; or</li><BR>
<li>The Executive Council, in its report to the Delegate Assembly, explains why it was unable to forward a resolution to the membership for ratification and returns the resolution to the assembly for possible revision and resubmission at the following year's assembly meeting.</li></ol>

<h2><a name="resolcommentproc"></a>Resolution Comment Process</h2>
Before the ratification vote is conducted, members have the opportunity to submit comments on the resolutions that are subject to ratification.  The resolution comment process is conducted in the members-only area of the MLA Web site over a one-month period (either in the spring or in the fall). Members are notified by e-mail or by letter of the opening of the comment period.  Resolution texts and the background information considered by the Delegate Assembly are posted at the Web site. Members may enter signed comments throughout the comment period.  These comments are not subject to a word limit, nor is there a limit on the number of comments that an individual may post.  The balloting period begins immediately after the comment period closes.  During the balloting period, additional comments are not accepted but comments already posted remain available for review.
