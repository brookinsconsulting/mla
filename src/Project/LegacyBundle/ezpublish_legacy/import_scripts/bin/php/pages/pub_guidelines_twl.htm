<style>
h3 {text-transform:uppercase;}
</style>
<h1>Guidelines for the Series Approaches to Teaching World Literature</h1>
<h2>Goals of the Series</h2>
<p>
The principal objective of the series Approaches to Teaching World Literature is to collect within each volume different points of view on teaching a literary subject such as a work, a tradition (e.g., metaphysical poetry), or a writer widely taught to undergraduates. The series is intended to serve nonspecialists as well as specialists, inexperienced as well as experienced teachers, graduate students as well as senior professors.
</p>
<p>
The preparation of a series volume begins with a proposal. Once the proposal has been approved by the MLA office of scholarly communication, the volume editor or editorial team prepares a wide-ranging survey of instructors, which is posted online. The survey enables the volume to include the philosophies and approaches, thoughts and methods of numerous teachers. The result is a sourcebook of material, information, and ideas.
</p>
<p>
Each volume is divided into two parts: “Materials” and “Approaches.” (A short preface to the entire volume may appear before part 1.) In the first part, the editor or editorial team, drawing from personal knowledge and experience as well as from the information and issues that emerge from the survey, presents a guide to the most helpful available materials related to the subject of the volume (e.g., preferred editions and translations; essential reference works, critical studies, and background materials; and useful teaching resources). The discussion of editions or translations should state what uniform edition or translation is being used in the volume and why. If contributors are using different editions or translations, the editor or editorial team should explain why. 
</p>
<p>
Part 2 comprises essays written by instructors, describing their approaches to teaching the subject of the volume. Volumes are broadly representative in the range of their contributors; in the critical orientations presented; and in the types of schools, students, and courses considered. Editors are responsible for addressing all major issues and approaches relevant to the subject. The essays in this part are preceded by an introduction by the editor or editorial team that gives a rationale for the volume, a conceptual framing of its topic, and contexts for the essays. The elements that should appear in this introduction are described below in  <a href="#introelements"><i>Elements of the introduction</a></i>.
</p>
<p>
A list of published volumes in the series and their tables of contents can be found at <a href="http://www.mla.org/store/CID39">mla.org/store/CID39</a>.
</p>

<h2>The Development Process</h2>
<p>
There are three stages in the development of a volume in the Approaches series: a proposal, a prospectus, and a manuscript.
</p>
<p>
To ensure that all volumes are consistent with the philosophy and objectives of the series, the MLA staff editors and the Publications Committee play an active advisory role in the preparation of each volume. The staff editor assigned to the volume is available to the volume editor or editorial team for consultation at all stages in the planning and writing of the volume.
</p>

<h3>The Proposal</h3>
<p>
Persons interested in editing a volume in the series should write to the office of scholarly communication (<a href="mailto:scholcomm@mla.org">scholcomm@mla.org</a>) stating their interest and outlining their qualifications for the task. Letters of inquiry should include a curriculum vitae of no more than five pages for each prospective editor.
</p>
<p>
If a title seems desirable for the series and the prospective editor or editorial team appropriate for the task, a staff editor will invite a formal proposal. The proposal should address such questions as the need for the volume, its rationale and goals, and relevant professional, scholarly, and pedagogical issues. Although the final content of a volume depends to some extent on the essay proposals received and on the comments and suggestions from readers of the prospectus, the proposal for the volume should indicate projected essay topics and a tentative organizational plan, including possible titles for sections and individual essays, and indicate names of scholars who would be appropriate contributors. In preparing the proposal, the prospective editor or editorial team should consult published volumes in the series.
</p>
<p>
The staff may decline the proposal, return the proposal for revision, or accept the proposal for development into a full prospectus, including annotated table of contents, for consideration by the Publications Committee.
</p>

<h3>The Prospectus</h3>
<p>
When a prospectus has been invited, the next step is for the volume editor or editorial team to prepare, with the advice and assistance of MLA staff editors, a survey questionnaire to be posted online. A notice appears in the <i>MLA Newsletter</i> inviting interested scholars to participate in the survey; in addition, an e-mail message announcing the survey may be sent to members of the MLA division or discussion group most closely associated with the study of the volume’s topic. Editors are encouraged to publicize the survey in other appropriate venues and to communicate directly with scholars who they think would be valuable contributors. 
</p>
<p>
The survey questionnaire elicits information on such matters as the following: courses in which the subject is taught, editions or translations used, textbooks selected, central issues and typical problems encountered, background readings assigned or recommended to students, critical and reference works used by instructors, and audiovisual and electronic resources used. The final item on the survey provides an opportunity for respondents to submit an essay proposal; thus the survey results assist volume editors in developing a list of contributors for part 2, “Approaches,” as well as in determining what information should be provided in part 1, “Materials.” 
</p>
<p>
In selecting contributors, the volume editor or editorial team should strive to maintain a balance between innovative and traditional approaches and to include essays broadly representative in the range of contributors chosen; in the philosophies, methodologies, and critical orientations presented; and in the types of schools (two-year colleges, four-year colleges, universities), students (e.g., nonmajors, majors, traditional, nontraditional), and courses (e.g., required survey courses, specialized upper-division courses) considered.
</p>
<p>
After deciding which proposed essays should be included in the volume, the volume editor or editorial team submits to the MLA a full prospectus, which includes the following components:
</p>

<ul>
<li>an introduction that is an advanced stage of what would form the introduction to part 2, “Approaches,” of the manuscript (see <a href="#introelements"><i>Elements of the introduction</i></a>);</li>
<li>an unannotated version of the table of contents (i.e., a simple list of the volume’s components, as in a published volume);</li> 
<li>an annotated table of contents, presenting one- or two-paragraph descriptions of each proposed essay, the author’s name and academic affiliation, and the projected length of the essay (number of words);</li>
<li>a list of proposed essays that were submitted but not chosen, along with the reasons for not including them</li>
</ul>

<p>
The lengths of individual essays may be uniform or may vary. But in deciding on the number of contributors, the volume editor or editorial team should be careful to keep in mind the maximum length of the complete manuscript (excluding the index but including the list of works cited): 100,000 words (about 450 pages in Courier 12-point type, double-spaced). In addition, essays should be organized in meaningful groups with appropriate headings. (Previous volumes in the series should be consulted for ideas on ways to structure the volume.) 
</p>

<a name=introelements></a><h4>Elements of the introduction</h4>
<p>
The introduction to the prospectus should be an advanced stage of the full introduction to part 2, “Approaches,” in the manuscript and should reflect the requirements listed below. Introductions that do not substantively address the following will not be sent to the committee (the requirements that rely on having the full essays can be treated more tentatively but should still be observed):
</p>

<ul>
<li><b>Rationale for the volume</b>: Discuss why the volume is needed, why it was undertaken, why teachers should read it.</li>
<li><b>Purpose of the volume</b>: Explain the aim of this particular volume.</li>
<li><b>Conceptual framing of the volume’s topic</b>: Discuss cultural, historical, and theoretical concerns and scholarly controversies relating to the topic of the volume.</li>
<li><b>Structure of the volume</b>: Explain how the volume is organized and why.</li>
<li><b>Contexts for essays</b>: Instead of providing summaries of the individual essays, editors should present pedagogical issues that emerge from the essays and discuss the interpretive and methodological relationships, including tensions, among essays.</li> 
<li><b>Sequence of reading</b>: Provide guidance to help readers understand how to use the volume. (Can the reader consult essays out of sequence? Are there some portions of the volume that all readers should read? Which essays should be read with or against one another?  Which essays complement or challenge each other?)</li>
<li><b>Translation issues</b>: If contributors discuss teaching translated versions of works, the editors should address issues relating to teaching in translation and the selection of appropriate texts.</li>
</ul>

<h4>Emphasis of essay abstracts on pedagogical issues</h4>
<p>
Since volumes in the Approaches series are dedicated to teaching, the abstracts should center on pedagogical issues. Not every essay needs to focus on practical classroom techniques, but whatever subject is covered in an essay, the essay should make explicit how it will apply to the needs of teachers in preparing and teaching classes and of students in learning. Typical pedagogical topics include how to shape a syllabus or unit on the volume’s subject; ways to engage students in difficult or unfamiliar materials; helping students gain necessary background and context for the volume's subject; classroom techniques; addressing the needs of different kinds of students in different kinds of institutions; and (if applicable) teaching the volume’s text(s) in translation. 
</p>

<h4>Evaluation of prospectus by the staff and by the Publications Committee</h4>
<p>
The prospectus will first be read by staff members, who may require revisions. Then, the prospectus is sent to outside readers. Editors will be able to respond to the readers’ comments before the Publications Committee sees the prospectus. 
</p>
<p>
If the staff feels the prospectus is ready, the Publications Committee will evaluate it and review what topics the volume plans to cover and how the editors envision the whole project in the light of the essay abstracts they have included. The committee may request the dropping of essays or the inclusion of new ones as well as revisions to the abstracts or the introductory section. The committee may approve the prospectus, approve it with required changes, ask that it be revised and resubmitted, or reject it and decline to develop the volume any further. 
</p>
<p>
Editors are reminded not to invite contributors to submit their essays until the prospectus and annotated table of contents have been approved by the Publications Committee.
</p>

<h4>Policy on previously published essays</h4>
<p>
The planned volume should not contain previously published material. We also discourage the practice of adapting essays in these volumes from previously published work. If contributors wish to base their essays on work they have previously published, they must make this known in the essay abstract, give full bibliographic citation of the previously published work, and explain why the proposed essay should not be passed over in favor a completely new one. The finished essay must not duplicate any language from the previously published work. Essays that do not conform to these requirements may be dropped from the volume.
</p>
<p>
If an essay based on previously published work is accepted, contributors must show that they have copyright or have permission in writing from the publisher of the previously published work and include a permission statement with the essay. 
</p>

<h4>Permission to quote from students’ writing</h4>
<p>
It is the MLA’s policy that authors obtain permission from students to quote from their writing. The MLA staff will, on request, supply a form that contributors can use to obtain such permission.  Editors should remind their contributors to obtain permission before including quotations from students’ writing in their essays.
</p>

<h4>If the prospectus is approved</h4>
<p>
Once the Publications Committee has voted to approve the prospectus, advance contracts will be issued to the volume editor or editorial team. These advance contracts will stipulate the delivery date and conditions for the full manuscript, including the review processes that will follow delivery of the full manuscript, noting that final acceptance is dependent on the successful peer review and committee approval of the full manuscript. 
</p>

<h3>The Manuscript</h3>
<p>
After approval of the prospectus, the volume editor or editorial team invites contributors to submit their essays. To avoid confusion, editors should send to each invited contributor guidelines specifying the nature of and intended audience for the volume, the length of the essay desired, the style and format to be followed, the deadline for submission of essays, and so on. Contributors should be informed that the volume editor or editorial team and the MLA reserve the right to reject or request revision of essays that do not conform to the guidelines or that fall below the quality expected. 
</p>

<h4>Adherence of essays in the manuscript to the prospectus abstracts</h4>
<p>
The finished essays in the manuscript should correspond to the abstracts submitted. Because the approval of the prospectus depends on the structure of the volume outlined by it, the topics to be covered in the essays, and the approaches to be discussed, if the finished essays differ markedly from the prospectus abstracts, this may be grounds for rejection of the manuscript. 
</p>

<h4>Introduction</h4>
<p>
The introduction to part 2, “Approaches,” in the manuscript should thoroughly address all the elements listed above in <i><a href="#introelements">Elements of the introduction</a></i>. 
</p>

<h4>Preparing the manuscript</h4>
<p>
In preparing manuscripts, editors and contributors should follow MLA style as outlined in the most recent edition of the <i><a href="http://www.mla.org/store/CID24/PID341">MLA Style Manual and Guide to Scholarly Publishing</a></i>: parenthetical references in the text refer the reader to a list of works cited. Each volume in the Approaches to Teaching series contains a comprehensive works-cited list that appears at the end of the volume. It is the responsibility of the editor or editorial team to compile the list from the individual works-cited lists supplied by the contributors. (In some volumes it may be more appropriate for each essay to retain its own works-cited list; this can be determined in consultation with the staff editor.) Content notes should be used sparingly; if they are included, they should appear as endnotes at the end of each essay. The editor or editorial team should ensure that submitted manuscripts follow the guidelines in <a href="http://www.mla.org/pub_prog_prep"><i>Directions for Preparing Manuscripts</i></a>,  available on the MLA Web site. 
</p>
<p>
Editors should make sure contributors are aware of their responsibility to obtain permission to reproduce material beyond fair use (see <a href="http://www.mla.org/store/CID24/PID341"><i>MLA Style Manual and Guide to Scholarly Publishing</i></a>, 2.2.13–14). Contributors must assume the costs for such permissions, if any. Written permission to quote from students’ writing must be provided at the manuscript stage. 
</p>
<p>
Editors of volumes on works in non-Latin scripts, such as Arabic or Chinese, will receive guidelines from the MLA staff on the appropriate use of the original script, transliteration, and translation in quotations, in-text references, and the works-cited list or lists. The editor or editorial team should convey these guidelines to contributors when inviting them to submit their essays.
</p>
<p>
Before submitting the manuscript, the editor or editorial team should compile and insert just before the list of works cited a section, “Notes on Contributors,” that contains brief biographical information on each contributor. The MLA staff editor will provide the editor or editorial team with a form that can be used to request biographical information from contributors. A list of the survey respondents and their affiliations should also be included in the manuscript. The complete manuscript, including all material but the index, should not exceed 100,000 words (about 450 pages in Courier 12-point type, double-spaced).
</p>

<h4>Evaluation of manuscript by the staff and by the Publications Committee</h4>
<p>
The manuscript is reviewed by a staff editor. If the staff sees substantial problems with the manuscript, it may require revisions before sending the manuscript to consultant readers for their evaluation. Editors will be able to respond to the readers’ comments. When the staff feels it is ready, the complete manuscript is presented, along with the readers’ reports and the response from the editor or editorial team, to the Publications Committee for a decision on whether to publish. The committee may approve the manuscript; approve it with required changes; ask that it be revised and resubmitted; or reject it and decline to develop the volume any further.
</p>

<h4>If the manuscript is approved</h4>
<p>
After the committee approves a manuscript and the staff has reviewed the final version of it, the manuscript is transmitted to the MLA’s office of publishing operations for copyediting, design, and production. At that time, contributors receive contracts. 
</p>
<p>
The editors of a volume receive royalties. Editors and contributors receive complimentary copies of the volume and other benefits.
</p>

<h3>Production and Publication</h3>
<p>
During production of the volume, editors and contributors are asked to review the relevant parts of the copyedited manuscript and one stage of proofs. When page proofs are available, the volume editor or editorial team prepares an index of names for the volume (the index contains names only, not subjects—see <i><a href="#Appendix">Appendix: Guidelines for Preparing an Index of Names</a></i>). 
</p>

<a name="Appendix"></a><h2>Appendix: Guidelines for Preparing an Index of Names</h2>
<h3>Scope</h3>

<b><i>Parts of the book to index</b></i>
<ul>
<li>preface to the volume</li>
<li>text proper (including parenthetical references)</li>
<li>endnotes</li>
<li>appendixes</li>
<li>figures, tables</li>
</ul>
<b><i>Parts of the book not to index</b></i>
<ul>
<li>table of contents</li>
<li>acknowledgments, whether made in front matter or in unnumbered endnotes</li>
<li>epigraphs</li>
<li>notes on contributors</li>
<li>list of works cited</li>
<li>bibliographic appendixes</li>
</ul>
<b><i>What to include</b></i>
<ul>
<li>names of persons </li>
<li>titles of important anonymous works (e.g., Bible, <i>Beowulf</i>)</li>
</ul>
<b><i>What not to include</b></i>
<ul>
<li>names of fictional characters</li>
<li>names of persons contained in the titles of works</li>
<li>generic terms like <i>Aristotelian</i>, <i>Lockean</i>, <i>Freudian</i></li>
</ul>

<h3>Format</h3>
<ul>
<li>Double-space the index manuscript. Put a comma after each entry, leave a space, and add the page numbers. Do not put a period at the end.</li>
<li>Alphabetize entries using the letter-by-letter system (see <i>MLA Handbook</i>, 7th ed., 5.3.3).</li>
<li>Make a distinction between continuous discussion of a name (e.g., 34–36) and separate mentions of a name over a sequence of pages (e.g., 34, 35, 36).</li>
<li>When indexing notes, add the lowercase letter <i>n</i> and the note number (218n5); if the page contains more than one note and they are consecutive, specify the page number and the note numbers: 218nn5–6. If the notes are not consecutive, use parentheses: 218 (nn 5,&nbsp;7). </li>
</ul> 
<h3>Forms of a Name</h3>
<p>
Use the name by which a person is widely and professionally known: <i>Eliot, T. S.</i>, not <i>Eliot, Thomas Stearns</i>; <i>Raphael</i>, not <i>Raffaello Sanzio</i>. A person known primarily by a pseudonym (Mark Twain, George Sand) should be listed under the pseudonym. Otherwise give the pseudonym and provide a cross-reference to the real name. An example:
</p>
<blockquote>Ouida. <i>See</i> Ramée, Marie Louise de la <br />Ramée, Marie Louise de la [pseud. Ouida], 555</blockquote>
<p>
For guidelines to indexing in general, consult chapter 16 of <i>The Chicago Manual of Style</i> (16th&nbsp;ed.).
</p>
