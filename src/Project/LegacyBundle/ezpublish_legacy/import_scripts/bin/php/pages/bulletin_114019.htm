<b>Number 114, Fall 1996</b>
<h1>Identity and Economics; or, The Job Placement Procedural</h1>
<h2 class="byline">TERESA MANGUM </h2>
TRUE confession: I am a Victorianist in temperament as well as training. When I was asked to be placement director at Iowa, I shared Samuel Smiles&#039;s optimism that everyone could succeed. I did what Charles Darwin would do: I classified; I organized. I began with a belief in my providential plan but soon started to suspect that the selection system called the job search was to a great degree random. I anguished over the students who had evolved into the seemingly unemployable. 

These changes in my perspective followed my realization that the job hunt requires candidates to pursue careers with determination and optimism when the odds are that a search for a tenure-track position will end in disappointment or failure. Statistics only confirm what we all know: the market cannot absorb the number of people who seek tenure-track professorial positions each year (see Huber, &#147;Survey&#148;; Holub; Langland; Nelson and B&#233;rub&#233;; and Nicklin). This means that our students must repeatedly, wrenchingly renegotiate their great expectations. Finding a position may take three years instead of one. As Lydia Belat&#232;che&#039;s description of her job search suggests, for many students the only serious option will be to accept a short-term teaching position&#151;or even a series of them&#151;with few benefits and little if any support for scholarly work. Often students are understandably naive (as are many faculty members) about the differences among types of schools and among departments&#039; needs and constraints. I fear that faculty members&#039; characterization of &#147;the discipline&#148; leads graduate students not only to expect that they will find positions that replicate the conditions at the research institutions where they are studying but also to feel that these are the positions they should desire. Small wonder if students become confused, even bitter, when they find their training, professional goals, and research projects incommensurate with the positions advertised. 

I begin with this grim introduction because anything we say about the placement process must be contextualized in the knowledge that many of our best and brightest candidates are not securing tenure-track positions. We need to remind students again and again that while careful career planning can improve their chances, nothing they can do (or have neglected to do) can change the job market. 

Many colleagues at the 1995 ADE seminar in Iowa City asked in some way how we have come, again, to such a gap between the numbers of applicants and the number of jobs. We can blame retrenchment and its consequences in part on the public distrust of higher education and the humanities that prompted the slashing of the NEH&#039;s budget in the fall of 1995. John Guillory&#039;s riveting analysis of the economic forces at work in educational institutions suggests this downsizing will continue. However, even as we work vigorously to persuade politicians and other external constituencies to support higher education, we as faculty members also need to assess our responsibilities for the vexed state of the job market. In particular, I am concerned about the consequences of a paradox that we accept too easily. I would argue that placement really begins with the graduate admissions process: departments shape the future pool of job candidates by rewarding particular skills, projects, and values with acceptance. Insofar as our admissions decisions favor early specialization over greater breadth and an individualistic careerism over cooperation or experimentation, I worry that we are selecting students whom we will not warm up to as teachers of colleagues. The way we place our students implicitly and explicitly molds their ideas of how they should place themselves within the profession. To what extent do our professional expectations and fantasies escalate our students&#039; anxieties without offering them solutions? How might we, to paraphrase Trudelle Thomas, demystify the placement process by providing candidates not just with hints for revising their writing samples and improving their letters but also with ways of imagining themselves into and through a job search many find fraught with confusion, despair, self-doubt, and cruel disappointments? 

In the immediate, practical process of applying for positions in the fall&#151;writing letters, composing a vita, polishing writing samples, photocopying syllabi, collecting letters of recommendation, and struggling through mock interviews&#151;students are told it is most crucial to forge an academic identity. Despite years of shifting between the identities of student and teacher (or teaching assistant), the candidates I have worked with generally find this moment of self-construction extremely difficult. Students are unnerved because the candidate identity feels fragile, fluctuating, performative, and as evasive as informative. In effect, they must become people whom they have long perceived as the other&#151;professionals. 

Graduate programs need to give more attention to the difficulties students have learning to integrate and balance the complex, competing responsibilities they will face as professionals. For example, we promote binge-and-purge writing strategies when we place excessive weight on end-of-semester research papers over sequential assignments and varied forms of academic writing. Consider how much of a faculty member&#039;s time is absorbed by kinds of writing that require daily, utilitarian efficiency: course proposals, letters of recommendation, end comments, departmental memos, grant proposals, conference paper abstracts, book reviews and reader reports, to name a few. A second example: despite our convictions that research enhances our teaching, the students I work with show every sign of needing us to guide them more explicitly as they learn to integrate research and pedagogical questions. These students also seem unequipped to perceive differences among the student, faculty, administrative, and public audiences to whom they will present their ideas. Recall the last job candidate whose idea of a talk was a densely written, textually complex blur that might have been a fine article but gave no ground to listeners or the candidate who presented seminar-level material to a class of sophomores. (In fairness to the candidates, departments sometimes ask students to perform unreasonable tasks like pretending to teach a freshman class to an audience of <em>faculty </em>members, an invitation to embarrassment for everyone involved.) 

I am repeatedly reminded that our failures contribute to our students&#039; problems when I advise students laboring to connect their research interests, teaching experience, and service in letters of application. Most initially produce tortuous, jargon-ridden, naively pretentious dissertation descriptions. Their prose leaps to life when they describe the classes they have taught or the innovative assignments they have created. Whatever our intentions, the lesson students often seem to absorb from graduate study is that what they perceive as scholarly (rather than pedagogical) activity is best communicated in prose likely to strike search committee members as elitist and overbearing. Conversely, students who sit tense and silent in my seminars wax eloquent when I ask how they would introduce a theoretical question to their students. When advising students on letters and interviews, I often urge them to be self-conscious about the consequences of accepting a commuting relation between the identities of teacher and scholar. 

Unacknowledged facets of faculty and student relationships can complicate the placement process in other ways. In a job market in which 45.9% of the candidates obtained positions as tenure-track professors (Huber, &#147;Survey&#148; 48), inevitable tensions between graduate students and the faculty are likely to escalate. In my graduate student days, I believed against all evidence that faculty members could do something to help me get a job, but sadistically wouldn&#039;t. One of the great departmental myths among midwestern university students was and still may be that faculty members of Ivy League schools and their California cousins arranged all their heres behind closed doors, whereas state school faculty members, because they were overworked, committed to fairness, or just indifferent, left us to fend for ourselves. Some of our suspicions could have been put to rest had we better understood the roles faculty members play in the search process and how much in the process depends on impersonal factors: a department&#039;s curriculum, the pragmatics of course assignments, affirmative action responsibilities and the influence of deans and other administrators. 

I hear a similar note of anxious interpretation in graduate students&#039; protests that they are now expected to write a publishable paper for every English course they take, as well as fulfill requirements for their degrees. As with the admission process, if departments are making these demands, they may be encouraging intellectual myopia and publication for publication&#039;s sake in graduate students even though we do not desire these characteristics in colleagues. Consider the following examples. On the first day of a seminar, a second-year student express impatience with the course assignments because he wants to spend the semester revising a previously written essay for publication. Another student complains in his evaluation of a seminar entitled Victorian Representations of Africa that he was expected to write about Victorians and Africa, neither of which is his specific area of interest. During a comprehensive exam, a third student is taken aback when asked to extend her answers beyond the context of a particular theorist&#039;s work, which has shaped every course paper she&#039;s written since her arrival. Confusion about the profession, about what professors (and search committees) expect students to know, about what professors actually do aside from the visible activities of publishing and teaching all make students more vulnerable to a narrow, self-serving conception of faculty members&#039;s duties and to a perjudicial hostility toward previous generations of colleagues. These fires can smolder long after candidates become department members. 

As part of our students&#039; fall preparations for placement, I have tried to prompt discussion of these sensed but unspoken gaps between their perceptions and those of faculty members&#151;and thus those of search committees. While respecting the pressures they are under, I have coaxed students to draw on the pleasure and feelings of accomplishment they have gained from a range of activities that correspond with the routine duties of many faculty members: informal professional or political organizing, committee service, and teaching preparation. I encourage them to imagine working in these collaborative contexts as well as doing individual research as they encounter senior faculty members on search committees. 

I also press our students to approach the application process as they would other research projects. I have collected a folder of articles describing steps in the process, such as the letter of application or the interview (Emmerson; Gregory; Musser; Wilt; Gaus; Sledge and Joels), as well as articles addressing issues faculty advisers may overlook, such as partner hiring (Blanshan and Gee; Gee; Preissler; Smart and Smart) or the problems specific to minority hires (Jablin and Tengler). I&#039;ve circulated articles by faculty members for faculty members that discuss interviewing procedures (Bowen; Peirce and Bennett; Tracy; Volkmann), disappointing searches (Malek), and characteristics of various kinds of departments (Carpenter; Dalbey, Hanawalt and Trzyna; Hansen and Hansen-Krenig; Madden; Marshall; Neel; Sledge), so that students can &#147;eavesdrop&#148; on the deliberations of hiring committees. These materials help candidates to approach interviewers as potential colleagues rather than as dissertation-defense committees from hell or as implacable gatekeepers of heaven. 

In addition, I have urged our students to stage peer interviews a week or two before they do mock interviews with faculty members. Students come to the peer-interview session with several copies of their letters of application and vitae along with advertisements for positions at several different types of schools. Three of four students spend ten minutes familiarizing themselves with an advertisement and choosing roles for themselves: the chair who seeks a reliable team player, the traditionalist, the young turk. When candidates articulate their impressions of academics, the exercise pushes them to question those stereotypes. Performing as interviewers dramatically challenges their preconceptions of interviewing. They learn how difficult it can be to entice a tense person into comfortable, informative conversation. They sheepishly acknowledge the mind-numbing effects of a too-long, too-particular dissertation description. They rediscover the importance of audience and the power of intelligent, responsive listening as well as speaking. They realize candidates should ask questions, not just answer them. They recognize the identifying marks of the graduate student identity and begin to imagine inroads to the identity of colleague. They give themselves credit for being what many have been becoming for some time as teaching assistants and members of departmental committees&#151;faculty members in the making rather than subordinates, students, or apprentices. Most important, students realize they can assert far more control over the interview situation than they had anticipated. By repeatedly exchanging roles and performing as search committee members, as candidates, and as observing critics, students whittle away at the distinctions and hierarchies that, more often than we acknowledge, structure relationship in graduate department. 

Having floated in the calmer waters of peer interviews, students are less likely to sink in mock interviews with faculty members. As candidates listen to suggestions and ask questions after each interview, they search for their reflections in the mirror of the profession these interviews provide. The images, of course, are a study in contrarieties. As Gordon Hutner points out, searches whet even considered, abstemious palates with voracious appetites for the most aggressive appointment, the most fashionable theorist, the rising star (76). What is less obvious to job seekers is that, at the same time, faculty members making hiring decisions want evidence that a candidate can publish while teaching and nurturing students, spending hours in committees that make crucial decisions but generate little if any recognition, and taking almost invisible tasks, like writing letters of recommendation, as seriously as producing groundbreaking articles. One of the ironies of our profession is that as students we desperately desired the kindness of faculty members but as overworked faculty members we receive precious few rewards for such time-consuming service. 

Nevertheless, those of us privileged to hold university positions have a moral and professional obligation to participate more actively in our future and present colleagues&#039; transitions from graduate students to job candidates to untenured faculty members to tenured faculty members. These changes are harder than they need to be because too many of us are caught in a trap of either inexperience or denial. On the one hand, new graduate students, as neophytes, cannot fathom that personal and economic insecurities will dog them through the tenure process, then nip at their aging heels in the guise of tense, struggling graduate students and junior faculty members. On the other hand, I am increasingly convinced that placement and tenure must be psychologically analogous to birth and car wrecks, given that so many faculty members I know seem immediately to suppress these experiences, retaining only the perverse tendency to tell horror stories in some gothic emulation of sympathy. 

It would be more helpful for faculty members to assume a formal, thoughtful, systematic mentoring role to graduate students both before and after hiring. The shrinking job market has already led to the restructuring of many graduate programs (Nelson and B&#233;rub&#233;; Langland). Departments admit fewer students, often turning away brilliant applicants. Like many colleagues, I now quietly advise undergraduates to consider other options seriously before applying to graduate school. 

One of the few positive consequences of this retrenchment is that it should now be possible even at large state universities to work more closely with our students. Curiously, most of the English departments I am familiar with have assembled thorough, finely tuned programs to train graduate students to teach, to monitor and address their problems in the classroom, and to evaluate their progress. The quality of students&#039; initiation into nonteaching activities is far less dependable, perhaps because it is largely left up to the individual fairly unstructured relationship between a student and an adviser or dissertation director. Robert Boice found repeated evidence of faculty members who failed to secure tenure because they could not meet the expectations of a discipline that never quite ceased to be a mystery. Inhibited by their hesitation to ask aggressive questions (especially of colleagues who always seemed to be overwhelmed or on the run), these junior faculty members lacked information on how their departments worked, how to determine when an article was ready to send out for review, how to decide where to send it, and so on. My experience leads met to suspect that graduate students face similar problems. 

Instead of absolving ourselves by blaming the victim, faculty members in PhD-granting departments need to ask how we expect students to place themselves in the placement process and to communicate their talents, abilities, and capacity to fulfill departmental needs when we&#039;ve inadvertently withheld essential information about academic life, departmental organization, and our survival strategies. While we can and should provide guidance during the career search through writing and editing workshops and mock interviews, we also need to conceive of our responsibilities as mentors earlier and more broadly. Perversely, many of us sidestep the mentoring role because we fear patronizing students whom we wish to see as our equals or companions&#151;a pleasant but naive belief. This disclaimer again shifts the burden onto students: if I open my office door (as we say in Iowa), they will come. We have positions and our students do not, and they feel this difference acutely. We should use our knowledge and our limited power in their interests instead of denying privileges we feel uncomfortable acknowledging. 

I close with a few suggestions for ways in which we can support job candidates in our programs. First, faculty members need an orientation to the job market as much as students do. Academics are notorious for assuming that it would be presumptuous to advise one another on writing strong letters of recommendation or counseling students about the marketability, to put it bluntly, of a dissertation. In fact, anyone who has not served on a search committee in recent years needs such advice. Many faculty members would be startled by the amount of detail in the best current letters of recommendation. Faculty members also need to be reminded that in today&#039;s competitive market miscasting a student&#151;for example&#151;by overemphasizing a female student&#039;s &#147;nurturing&#148; qualities as a teacher or undermining a student&#039;s dissertation as an &#147;interesting study,&#148; &#147;a fine beginning,&#148; or a &#147;monograph&#148;&#151;can have disastrous consequences. 

Job candidates also have new questions: &#147;When should I ask about spousal or partner hiring?&#148; &#147;Can I mention paternity leave?&#148; &#147;Should I hide my sexual preference even though I&#039;ve been out of the closet for years?&#148; (On issues related to family, see Hamovitch and Morgenstern; Pascoe. Advice to gay candidates is still all too sparse.) To give reliable advice, we must talk to one another about the consequences job candidates face when they ask or answer such questions. Before we advise our students, we should set aside time as faculties to discuss past successes and failures in our departments, the limitations and strengths our students are likely to possess a body (and how to negotiate the former and highlight the latter in letters), and other problems students encounter. 

We also need to provide job seekers with access to those who can answer crucial questions we cannot. We cannot be expected to know a great deal about institutions unlike our own or about alternative careers, but departments can certainly bring in outside experts to educate students and faculty members alike: professors from other types of colleges and universities, editors, and business managers or executives who have a history of hiring liberal arts graduates. 

In addition, we could work harder to help candidates who fall between the cracks. Departments can sponsor grant-writing workshops (drawing on other professionals within the university rather than taking more work upon ourselves). We can assist students in setting up writing groups (while some students set up groups on their own, I suspect that the worse the job market is going, the less likely a person is to ask peers for help). Continuing their research and writing within the supportive structure of writing groups would at least help students pursue tenure-track positions. As Cary Nelson and Michael B&#233;rub&#233; suggest, departments can argue for increased funding and benefits for graduate students and can use departmental funds to hire PhDs while they look for tenure-track positions. We also simply must tackle the timing of the job search. It is unconscionable that a significant number of students end up paying dues, air fare, and hotel deposits to attend MLA convention without interviews in the desperate hope of receiving an interview offer the day before the convention (it happens) or even at the convention. Leaving the convention without an interview should not signal disaster, especially since many candidates are hired for positions that surface afterward. 

Finally, we need to confront our tendency to fetishize our departments&#039; successes, another issue Hutner addresses. Those who do not find academic jobs tend to fade away, and we do not try hard enough to follow them. This goal may be harder to achieve than it sounds because students who are not doing well on the market are understandably evasive, and we hardly want to treat students as statistics. A serious challenge for graduate programs is to maintain contact with those who leave the profession, to learn from their decisions and career changes, to keep revising the structures of departments as well as placement programs in the light of what they can teach us, and to decide just how much responsibility we need to take for those who remain either unemployed or underemployed. 

It sounds like a lot of work, doesn&#039;t it? But I believe the rewards would be significant. For all our sakes we need to take seriously the simmering frustration of job seekers the Erik Curren, a former president of the MLA Graduate Student Caucus, describes: 
<blockquote>In my experience, student evaluations and the assessments of our superiors consistently show that we are some of the best teachers in our programs. We produce dissertations that assimilate and push forward the most advanced work in such important areas as critical theory, cultural and gender studies, and historical research. And, we are more prominent than our predecessors at academic conferences and in refereed publications. In short, despite extreme financial and emotional pressures, we are demonstrating our worthiness to join the profession as has no generation of graduate students before us. (58) </blockquote>

He is right, of course. And when he arrives in your department or mine, he may be angry and impatient with senior scholars who have not published as much or struggled as hard for what they have as he has. He may be unwilling or unable to imagine himself part of a collaborative, cooperative, mutually respectful community when, from his point of view, his new employers (as opposed to colleagues) made getting a job such a brutal, expensive process. 

It is in the best interest of such students and of every department, and it is also common decency, for us to be honest with our students about their prospects from day one. We need to encourage students to prepare themselves for the wide-ranging needs, challenges, and pleasures of the greatly diversified American higher education system. We need to impress upon applicants to graduate school that a significant number may never find lasting academic jobs, that most will have to go into debt to finish graduate school and to support themselves through a year or several years of temporary employment, and that at best a couple of members of each graduating class will be hired by departments like the one in which they study. Having held to the very un-Victorian policy of truth in advertising, we must then find ways to share the responsibility for the future of those students who choose, with our endorsement, to pursue their degrees under these conditions. 


<em>The author is Associate Professor of English at the University of Iowa. This paper was presented at the 1995 ADE Summer Seminar Midwest at the University of Iowa.</em>

<h2>Selected List of Works Consulted </h2>
Belat&#232;che, Lydia. &#147;Temp Prof: Practicing the Profession off the Tenure Track.&#148; <cite>Profession 94 </cite>. New York: MLA, 1994. 64&#173;66. 

Blanshan, Sue A., and E. Gordon Gee. &#147;Working with Partners.&#148; <cite>The Art of Hiring in America&#039;s Colleges and Universities </cite>. Stein and Trachtenberg 101&#173;22. 

Boice, Robert. <cite>The New Faculty Member: Supporting and Fostering Professional Development </cite>. San Francisco: Jossey-Bass, 1992. 

Bowen, Zack. &#147;&#145;Young Blood&#146; and Other Gothic Inspirations: The Hiring Process.&#148; <cite>ADE Bulletin </cite>85 (1986): 46&#173;49. 

Burgan, Mary. &#147;The Rookie-of-the-Year Syndrome.&#148; <cite>ADE Bulletin </cite>89 (1988): 20&#173;23. 

Burgan, Mary, George Butte, Karen Houck, and David Laurence. &#147;Two Careers, One Relationship: An Interim Report on &#145;Spousal&#146; Hiring and Retention in English Departments.&#148; <cite>ADE Bulletin </cite>98 (1991): 40&#173;45. 

Caplan, Paula. <cite>Lifting a Ton Feathers: A Woman&#039;s Guide to Survival in the Academic World </cite>. Toronto: U of Toronto P, 1993. 

Carpenter, Lissette. &#147;Teaching in the Community College: A Possible Road to Be Taken.&#148; <cite>ADE Bulletin </cite>111 (1995): 20&#173;22. 

Colwell, C. Carter. &#147;Videotaping Interviews of Prospective Faculty Members.&#148; <cite>ADE Bulletin </cite>83 (1986): 41&#173;42. 

Curren, Erik D. &#147;No Openings at This Time: Job Market Collapse and Graduate Education.&#148; <cite>Profession 94 </cite>. New York: MLA, 1994. 57&#173;63. 

Dalbey, Marcia A. &#147;What Is a Comprehensive University, and Do I Want to Work There?&#148; <cite>ADE Bulletin </cite>111 (1995): 14&#173;16.

Davis, Diane E., and Helen S. Astin. &#147;Life Cycle. Career Patterns, and Gender Stratification in Academe: Breaking Myths and Exposing truths.&#148; <cite>Storming the Tower: Women in the Acdemic World </cite>. Ed. Suzanne Stiver Lie and Virginia E. O&#039;Leary. New York: Nichols, 1990: 89&#173;107. 

<cite>Directory of Minority PhD and MFA Candidates and Recipients </cite>. University Park: Pennsylvania State UP, 1994. 

Emmerson, Richard K. &#147;Some Thoughts on the Hiring Process in an English Department.&#148; <cite>ADE Bulletin </cite>111 (1995): 22&#173;27. 

&#151;&#151;&#151;. &#147;&#145;When Do I Knock on the Hotel Room Door?&#146;: The MLA Convention Job Interview.&#148; <cite>ADE Bulletin </cite>111 (1995): 4&#173;6. 

Feldesman, Marc R., and Robert T. Trotter. &#147;A Consumer&#039;s View of the &#145;Meat Market.&#146;&#148; <cite>Anthropology Newsletter </cite>Feb. 1989: 1+. 

Gaus, Paula J., Andrea Celine Sledge, and Agnes Rose Joels. &#147;The Academic Interview.&#148; <cite>Journal of College Placement </cite>43&#173;44 (1983): 61&#173;65. 

Gee, E. Gordon. &#147;The Dual Career Couple: A Growing Challenge.&#148; <cite>Educational Record </cite>72.1 (1991): 45&#173;47. 

Green, Eleanor H. &#147;The Job Search: Observations of a Reader of 177 Letters of Application.&#148; <cite>ADE Bulletin </cite>113 (1996): 50&#173;52.

Gregory, Marshall. &#147;From PhD Program to BA College; or, The Sometimes Hard Journey from Life in the Carrel to Life in the World.&#148; <cite>ADE Bulletin </cite>107 (1994): 20&#173;24. 

&#151;&#151;&#151;. &#147;How to Talk about Teaching in the MLA Interview.&#148; <cite>ADE Bulletin </cite>111 (1995): 7&#173;8. 

Gruner, Elizabeth Rose. &#147;Feminists Face the Job Market: Q &#038; A (Questions and Answers).&#148; <cite>Concerns </cite>24.3 (1994): 15&#173;23. 

Guillory, John. &#147;Preprofessionalism: What Graduate Students Want.&#148; <cite>ADE Bulletin </cite>113 (1996): 4&#173;8. 

Hamovitch, William, and Richard D. Morgenstern. &#147;Children and Productivity of Academic Women.&#148; <cite>Journal of Higher Education </cite>48 (1977): 633&#173;45. 

Hanawalt, Jean Allen, and Thomas Trzyna. &#147;Applying to Teach at a Christian College.&#148; <cite>ADE Bulletin </cite>79 (1984): 46&#173;47. 

Hansen, J. T., and Nancy Hansen-Krening. &#147;Difficult Appointments Abroad.&#148; <cite>ADE Bulletin </cite>88 (1987): 71&#173;79. 

Heiberger, Mary Morris, and Julia Miller Vick. <cite>The Academic Job Search Handbook </cite>. Philadelphia: U of Pennsylvania P, 1992. 

Holub, Robert C. &#147;Professional Responsibility: On Graduate Education and Hiring Practices.&#148; <cite>Profession 94 </cite>. New York: MLA, 1994. 79&#173;86. 

Horowitz, Tony. &#147;Class Struggle: Young Professors Find Life in Academia Isn&#039;t What It Used to Be.&#148; <cite>Wall Street Journal </cite>15 Feb. 1994, midwest ed: Al+. 

Huber, Bettina J. &#147;The Changing Job Market.&#148; <cite>Profession 92 </cite>. New York: MLA, 1992. 59&#173;73. 

&#151;&#151;&#151;. &#147;The MLA&#039;s 1993&#173;94 Survey of PhD Placement: The Latest English Findings and Trends through Time.&#148; <cite>ADE Bulletin </cite>112 (1995): 40&#173;51. 

&#151;&#151;&#151;. &#147;Recent Trends in the Modern Language Job Market.&#148; <cite>Profession 94 </cite>. New York: MLA, 1994. 87&#173;105. 

Huber, Bettina J., Denise M. Pinney, David E. Laurence, and Denise Bourassa Knight. &#147;MLA Surveys of PhD Placement: Most Recent Findings and Decade-Long Trends.&#148; <cite>ADE Bulletin </cite>92 (1989): 43&#173;50. 

Hutner, Gordon. &#147;What We Talk about When We Talk about Hiring.&#148; <cite>Profession 94 </cite>. New York: MLA, 1994. 75&#173;78. 

Jablin, Fredric M., and Craig D. Tengler. &#147;Facing Discrimination in On-Campus Interviews.&#148; <cite>Journal of College Placement </cite>41&#173;42 (1982): 57&#173;61. 

Langland, Elizabeth. &#147;The Future of Graduate Education; or, Which Graduate Programs Have a Future?&#148; <cite>ADE Bulletin </cite>111 (1995): 28&#173;32. 

Lemiesz, Linda M. &#147;And the Lost Shall Be Found.&#148; <cite>Profession 94 </cite>. New York: MLA, 1994. 67&#173;69. 

Madden, Frank. &#147;A Job at a &#145;Real&#146; College; or, How I Became a Faculty Members at a Two-Year College.&#148; <cite>ADE Bulletin </cite>111 (1995): 17&#173;19. 

Magner, Denise K. &#147;Job-Market Blues.&#148; <cite>Chronicle of Higher Education </cite>27 Apr. 1994: A17&#173;20. 

Malek, James S. &#147;Caveat Emptor; or, How Not to Get Hired at DePaul.&#148; <cite>ADE Bulletin </cite>92 (1989): 33&#173;36. 

Mangum, Teresa. &#147;The University of Iowa Placement Packet.&#148; Unpublished handout, 1995. 

Mercer, Joye, and Kit Lively. &#147;The Worst May Be Over: Higher Education and the States.&#148; <cite>Chronicle of Higher Education </cite>12 Jan. 1994: A24+. 

Modern Language Association of America. &#147;Statement of Professional Ethics.&#148; <cite>Profession 92 </cite>. New York: MLA, 1992. 75&#173;78. 

Muller, Kurt E., and R. Douglas LeMaster. &#147;Criteria Used in Selecting English Faculty in American Colleges and Universities.&#148; <cite>ADE Bulletin </cite>77 (1984): 51&#173;57. 

Musser, Joseph. &#147;The On-Campus Interview.&#148; <cite>ADE Bulletin </cite>111 (1995): 11&#173;13. 

Neel, Jasper. &#147;On Job Seeking in 1987.&#148; <cite>ADE Bulletin </cite>87 (1987): 36&#173;39. 

Nelson, Cary, and Michael B&#233;rub&#233;. &#147;Graduate Education Is Losing Its Moral Base.&#148; <cite>Chronicle of Higher Education </cite>23 Mar. 1994: B1&#173;3. 

Nicklin, Julie L. &#147;The Layoffs Continue.&#148; <cite>Chronicle of Higher Education </cite>4 May 1994: A37+. 

Pascoe, Judith. &#147;What to Expect When You&#039;re Expecting.&#148; <cite>Profession 94 </cite>. New York: MLA, 1994. 70&#173;74. 

Peirce, Kate, and Roger Bennett. &#147;Interviewing Potential Faculty: Finding the Right Person.&#148; <cite>Journalism Educator </cite>45.3 (1990): 60&#173;66. 

Preissler, Scott M. &#147;Job-Search Help for the &#145;Trailing Spouse.&#146;&#148; <cite>Journal of Career Planning and Employment </cite>50 (1989): 83&#173;84. 

Sedgwick, Eve Kosofsky. &#147;Some Suggestions for Job Candidates.&#148; Unpublished handout, n.d. 

Showalter, English. <cite>A Career Guide for PhDs and PhD Candidates in English and Foreign Languages </cite>. New York: MLA, 1994. 

Sledge, Linda Ching. &#147;The Community College Scholar.&#148; <cite>ADE Bulletin </cite>83 (1986): 9&#173;11. 

Smart, Mollie S., and Russell Smart. &#147;Paired Prospects: Dual-Career Couples on Campus.&#148; <cite>Academe </cite>(1990): 33&#173;37. 

&#147;Statement on the Use of Part-Time Faculty.&#148; <cite>Job Information List </cite>Oct. 1993, foreign lang. ed.: iii. 

Steele, Valerie. &#147;The F-Word.&#148; <cite>Lingua Franca </cite>Apr. 1991: 1+. 

Stein, Ronald H., and Stephen Joel Trachtenberg, eds. <cite>The Art of Hiring in America&#039;s Colleges and Universities </cite>. Buffalo: Prometheus, 1993. 

Storey, Michael L. &#147;Careers in English.&#148; <cite>ADE Bulletin </cite>87 (1987): 34&#173;35. 

Thomas, Trudelle. &#147;Demystifying the Job Search: A Guide for Candidates.&#148; <cite>College Composition and Communication </cite>40 (1989): 312&#173;27. 

Timmerman, John H. &#147;Advice to Candidates.&#148; <cite>College English </cite>50 (1988): 748&#173;51. 

Tokarczyk, Michelle M., and Elizabeth A. Fay. eds. <cite>Working-Class Women in the Academy: Laborers in the Knowledge Factory </cite>. Amherst: U of Massachusetts P, 1993. 

Tracy, Saundra J. &#147;Finding the Right Person&#151;and Collegiality.&#148; <cite>College Teaching </cite>34.2 (1986): 59&#173;62. 

Volkmann, M. Fredric. &#147;What to Look for in a Candidate.&#148; Stein and Trachtenberg 65&#173;84. 

Wilt, Judith. &#147;How to Talk about Scholarship in the MLA Interview.&#148; <cite>ADE Bulletin </cite>111 (1995): 9&#173;10. 

Wynam, Roger E., Nancy A. Risser, et al. <cite>Humanities Ph.D.s and Nonacademic Careers: A Guide for Faculty Advisors </cite>. Evanston: Committee on Institutional Cooperation, 1983. 


&#169; 1996 by the Association of Departments of English. All Rights Reserved.
