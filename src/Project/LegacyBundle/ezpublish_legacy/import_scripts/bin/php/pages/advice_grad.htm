<h1>Advice to Graduate Students: From Application to Career</h1>
This document is addressed to graduate students, both prospective and current. It considers both those studying English and those studying foreign languages and covers issues relevant to candidates for master's degrees and candidates for doctorates.

The choice of a graduate school can determine the direction of your academic career. Beyond the instruction graduate schools provide, they offer essential apprenticeship opportunities for careers in teaching, scholarship, and other fields as well as support toward finding work in those careers after completion of the degree. The strength and quality of an institution's commitment to supporting its students--and often that support is needed far longer than anticipated--can mean the difference between frustration and success. As a result, you should consider more than the contents of a course catalog or student handbook when choosing where to pursue a degree, and you should remain cognizant of departmental and institutional changes throughout your graduate career.

When applying for graduate school, begin with careful research, including talking with faculty members and counselors at your undergraduate institution; then speak to faculty members and students at the schools you are seriously considering. Request contact with current students as well as with recent graduates. If you will be entering graduate school under special circumstances (e.g., you are a single parent, a mature student, interested in interdisciplinary work, continuing a previous career), ask to speak to students who share your circumstance.

Once you have entered a graduate school, meet regularly with your adviser and stay alert to&nbsp;departmental and institutional changes. Ask questions and keep your adviser informed of any changes in your situation that may affect your ability to complete your work and to meet your obligations.

<h2>Academics</h2>
<h3>The Degree Program</h3>
When you select a graduate program, many of your questions can be easily answered by a careful review of the university’s catalog or Web site. Sometimes faculty changes and institutional changes that can directly affect your ability to complete the degree may not appear in published materials, so you should ask about such changes.
<ul>
<li>Does the program have faculty members in your area(s) of interest who are available to work with you?
<li>How many courses are required to complete the degree? What exams are required, and when must they be completed? Is there a thesis option for the MA? What additional qualifications (e.g., languages) are needed? What are the residency requirements?
<li>How often are the courses listed in the catalog offered? How many are typically offered each semester? Is the curriculum likely to change in the next two years? Which faculty members will be on leave in the next two years? Are there faculty members who are regularly gone? How frequently are there visiting scholars in the department?
<li>Are all graduate courses open to all students in the department, or are there restrictions? Can you take graduate courses in other departments, to pursue interdisciplinary interests? Must you take a certain number of courses in your own department each semester?
</ul>

In addition to the university catalog, ask for other graduate school and departmental publications. For example, many departments publish their own handbooks for graduate students; these handbooks can be more easily kept current, and they may also provide useful information on local resources and culture. Ask for the list of courses currently being taught and planned for the coming semester.

Once you have a clear idea of the courses that interest you, make sure you can enroll in&nbsp;them.

Find out what your options are in the event that your plans change as your studies progress.
<ul>
<li>If you enter a master's degree program, can you transfer into the doctoral program? What are the criteria for pursuing the doctorate after successful completion of the MA? For example, how many courses taken to complete the MA will count toward the&nbsp;PhD?
<li>If you start in the doctoral program, what are your options for completing the MA? Is there a degree available (e.g., MPhil) for PhD candidates who do not complete the dissertation?
<li>What are the time limitations for completing the degree? What is the average length of time students take to complete the master's degree? the doctorate?
</ul>

<h3>Expectations</h3>
Once you have entered a program, you should meet with an adviser or faculty member as soon as possible to receive a comprehensive description of your program, including course requirements, exams, deadlines, and expectations for the thesis or dissertation. You should receive a calendar outlining a typical graduate program.

In each course, you can reasonably expect a clear, written description of course objectives, requirements, and criteria for evaluation. You should also be able to expect a knowledgeable, well-prepared, and conscientious professor, committed to teaching, who meets classes as scheduled, devotes adequate time to reading and responding to student papers and exams, returns them within a reasonable time period, and maintains an atmosphere of mutual respect that transcends any consideration of gender, ethnicity, national origin, disability, or sexuality. In return, professors expect students to attend regularly, be prepared, submit all work according to previously established expectations and deadlines, and respect the professor's authority in course design and evaluation.

You should be given the opportunity to evaluate anonymously each course you take, ideally during the term and not just at the end of the course.

The program should have a set of clear guidelines concerning the timelines for completion. The number of years spent on course work, the scheduling of exams and propspectus defenses, and the length of time available to write the dissertation or compile the MA portfolio should be spelled out and available in writing.

You will need support from your professors throughout your academic career, first as mentors and teachers, later as recommenders and colleagues. You should clarify with each of your advisers the expectations for meetings and for turnaround on papers and chapters.  Programs are under increasing pressure to meet strict degree completion deadlines. To finish on time, you need to work out a clear schedule of completion with your advisers. To keep to this schedule, you need to maintain consistent and open communication with your advisers. Expect the institution to have created ways in which your opinion can be expressed and acted on freely, without threats or intimidation. If you feel you have received unfair treatment of any kind, seek appropriate grievance procedures. Information about these procedures should be readily available.

<h2>Employment and Financial&nbsp;Assistance</h2>
Consider with great attention the offer of admission made to you. Does it include both tuition and a livable stipend? Good departments usually offer full financial packages to their top-ranked applicants.

Consider fully the costs of graduate school and plan carefully, beyond the first year. Ask these questions before accepting an offer:
<ul>
<li>What is the estimated cost of living for a graduate student?
<li>What kind of housing assistance is available? what on-campus housing for graduate students? what help locating off-campus housing?
<li>What health coverage is available, and at what cost? Does coverage include summers? Can coverage include family members?
<li>What is the average number of years of support awarded by the department?
</ul>

Graduate student support falls into many categories, from tuition waivers and student employment to dissertation grants. No university is obligated to support graduate students, and admission to a graduate program does not require a university to offer students financial aid or employment or to continue supporting a student beyond a reasonable number of years. The terms of any financial aid, loans, or employment should be clearly and accurately described in official university documents.

The most common source of support is graduate teaching assistantships, which provide not only financial assistance but also essential professional preparation. To be prepared for your career, you need adequate opportunities to assist professors in teaching and research. When possible, you should have primary responsibility for at least one or two courses before completing the degree. Typically, institutions make available a range of employment and teaching experiences. Be careful to balance teaching and other responsibilities with the completion of your degree--they should not impede your progress. Be sure to consider the following questions on being offered a teaching assistantship:
<ul>
<li>What are the responsibilities of a teaching assistant in the&nbsp;department? What is the average load for a teaching assistant?
<li>What is the annual stipend awarded for teaching? Does it&nbsp;include a tuition waiver? Does it include student fees? Do graduate students pay union dues or have other union responsibilities? Does the department award any book allowances?
<li>Is there additional teaching available (summer or night classes or additional sections)? How are the opportunities distributed? What is the salary?
<li>Is there a variety of teaching opportunities available, if not in the department, perhaps in other programs on campus? How does a graduate student take advantage of such opportunities?
<li>What training is available for new teaching assistants? What support is available after that? Does the department offer a course or courses in teaching methods? Is there an institutional center for teaching to help in your professional preparation? Does the department have a formal program of faculty teaching mentorship for graduate students?
<li>What are the working conditions? Do graduate students have access to office space and facilities (computers, copiers, fax machines, and e-mail)?
<li>Other than teaching, what employment opportunities are&nbsp;available, in the department or the university, as a researcher, reader, editorial assistant, or worker in related fields (see <A HREF="/prof_employment">MLA Committee on Professional Employment</A>)? How are such jobs appointed and what is the salary?
</ul>

Graduate schools should offer clear, detailed job descriptions. While the emphasis in evaluation should be on encouraging positive development, departments have a right to determine competence and acceptable performance, and, following fair and equitable evaluation, they may dismiss any student who fails to perform satisfactorily. Policies and procedures for dismissal must be clearly described.

<h2>Career Preparation</h2>
Graduate schools are training grounds for careers both within and beyond academia. Departments and institutions make available and support different kinds of opportunities for practicing the skills necessary for future success.
<ul>
<li>Does the department expect or encourage students to begin sending papers for conferences and publication before they complete their degree?
<li>Are travel funds available for graduate students to present papers at conferences or to be interviewed at conventions?
<li>Does the department or institution sponsor any colloquiums in which graduate students can present papers? Do graduate students help organize these events?
<li>What similar opportunities are available for graduate students to practice skills in preparation for nonacademic&nbsp;jobs?
<li>Does the department offer a course, seminar, or workshop on professional development and the job search process?
<li>What career placement services are available to graduate students? Do the department and career placement center help you pursue teaching positions at different kinds of institutions?
</ul>

Securing a satisfying job after graduate school can be an exciting and rewarding experience; it can also be a long and difficult process. Departments play an essential role in ensuring that you know about employment prospects and options and about procedures to follow when looking for work within and beyond the academy. Being well informed and well prepared can help make a challenging job market less frustrating. (The report of the MLA Committee on Professional Employment [<A HREF="#cpe">Gilbert et al.</A>] also includes information graduate students may find useful.)

After you have made sufficient progress in your course work or dissertation, begin to consider your career after graduate school. The spring or summer before you plan to enter the job market, start preparing materials and researching opportunities.

<h3>Academic Jobs</h3>
<ul>
<li>What percentage of the department's graduate students who were on the job market in the last three to five years&nbsp;obtained academic positions? Where did they find employment?
<li>How many people secured a job within twelve months of receiving the&nbsp;PhD?
<li>What was the employment status of people receiving the&nbsp;MA?
</ul>

The department or career center should maintain both local and national data regarding PhD production and the job market. The <I>MLA Newsletter</I> publishes information about the number of positions departments announce in the MLA's <I>Job Information List</I> (<I>JIL</I>; see <A HREF="/jil">http://www.mla.org/jil</A>). While not a count of all academic jobs available in any given year--most two-year colleges and an unknown number of departments in four-year colleges and universities choose not to use the <I>JIL</I>--the MLA's reports on the <I>JIL</I> do offer a reliable index of the condition of the academic job market. In addition, the MLA conducts periodic surveys of PhD placement, which show the employment situations of PhDs in language and literature within a year of receiving their degrees. The results appear online. The <I>ADE Bulletin</I> and <I>ADFL Bulletin</I> also make available a wealth of material on the job search, ranging from how to craft an effective letter of application to how to succeed in interviews.

Prepare for your job search by acquiring a general knowledge of the system of higher education in the United States and Canada (see <A HREF="#cfat"><i>Carnegie Classification</i></A>). In a period of limited opportunities, casting a wide net to consider working at a variety of institutions can increase your chances of securing a position. For example, two-year colleges represent an increasingly viable employment option for both MAs and PhDs. Develop ways to show evidence of your interest in and preparation for teaching at different institutions by seeking apprenticeship opportunities in a range of settings.

A valuable source of information for graduates&nbsp;seeking employment is <A HREF="#mlag"><I>The MLA Guide to the Job Search</I></A>, which covers both academic and nonacademic opportunities.

When you begin to respond to advertised positions, carefully research the institutions to which you apply; familiarize yourself with their students, programs, and approaches. Be sure that your cover letter and support materials directly address the requirements stated in the&nbsp;job announcement and fit the individual institution's&nbsp;needs. Be sure to alert the professors acting as your referees of your intention to search for employment and to give them plenty of notice so that they can prepare strong letters of recommendation for you.

If you are invited to be interviewed, be prepared to talk not solely about your research; be prepared to translate that research into teaching strategies and lesson plans sensitive to the needs of as wide a range of students as possible (see <A HREF="#dd">&quot;Dos and Don'ts&quot;</A>). Be aware that when you are asked about your research, you need to make your reply lucid and interesting to nonspecialists. Departments also want some picture of what kind of citizen and colleague you will&nbsp;be.

<h3>Nonacademic&nbsp;Jobs</h3>
<ul>
<li>What help is available for seeking a job outside academia? Does the university offer flexible training for the variety of jobs available to graduates?
<li>What kinds of nonacademic jobs have students at the graduate school found? Can you consult with them for advice? Can you tap into networks of alumni or others who can help you prepare for and find jobs outside academia?
</ul>

Before entering the job market, try to develop the ability to translate your academic training into more broadly defined categories of communication, analysis, interpersonal, and organizational skills. Tailor part of your studies and work experience to develop expertise outside conventional academic training. Research the job possibilities and learn and practice the kinds of computer, research, writing, and editing skills these positions often require.

Look for the examples and opportunities available in nonacademic positions within your graduate institution. For example, the university may house an academic press that employs manuscript editors. Departments, particularly in the sciences, may produce journals, newsletters, or reports that require proofreading and editing. Offices of development and of alumni affairs, alumni magazines, university news divisions, and Web pages may be seeking writers and editors. Familiarize yourself with the university and its educational mission when applying for such positions.

Be aware that you will need a different r&eacute;sum&eacute; and different letters of recommendation to apply for jobs outside academia. Check to see if your graduate school's career placement services will help you manage a second placement file; if not, consider using the services at your undergraduate institution.

Try to connect with alumni who have achieved satisfaction and success outside academia: in secondary and elementary school teaching; in publishing, editing, and journalism; in advertising, business, and other enterprises in the for-profit sector; and in not-for-profit organizations, unions, government, and foundations. Imagine yourself seriously and creatively in those fields.
<div ALIGN="RIGHT">Spring 1998
Revised by the Committee on Academic Freedom and Professional Rights and</div> <div align="right">Responsibilities, 2012</div>

<h2>Works&nbsp;Cited</h2>
<A name="cfat"></a><i>The Carnegie Classification of Institutions of Higher Education</i>. Carnegie Foundation for the Advancement of Teaching, n.d. Web. 26 Oct. 2012. <<A HREF="http://classifications.carnegiefoundation.org" target="blank">http://classifications.carnegiefoundation.org</A>>.

<A name="dd"></a>&quot;Dos and Don'ts for MLA Convention Interviews.&quot; <I>Modern Language Association</I>. MLA, 2007. Web. 26 Oct. 2012. <<a href="/jil_jobseekers_dos">http://www.mla.org/jil_jobseekers_dos</a>>.

<A name="cpe"></a>Gilbert, Sandra M., et al. &quot;Final Report of the MLA Committee on Professional Employment.&quot; <I>ADE Bulletin</I> 119 (1998): 27–45. Web. 26 Oct. 2012.

MLA Committee on Professional Employment. &quot;Evaluating the Mission, Size, and Composition of Your Doctoral Programs: A Guide.&quot; <I>ADE Bulletin</I> 119 (1998): 46–50. Web. 26 Oct. 2012.

<A name="mlag"></a><I>The MLA Guide to the Job Search: A Handbook for Departments and for PhDs and PhD Candidates in English and Foreign Languages</I>. New York: MLA, 1997. Print.
