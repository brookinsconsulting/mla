<H2>The Committee and Its Procedures</H2>

<P>
The Ad Hoc Committee on the Future of Scholarly Publishing represented some of the diversity within the MLA as a whole. Its members were drawn from the fields of English, American, German, and Latin American literatures, as well as from comparative literature. The membership included four professors, one associate professor, one assistant professor, a lecturer, and a graduate student. MLA staff members with expertise in problems of publishing, Martha Evans, Joseph Gibaldi, Marcia Henry, Sonia Kane, and David Nicholls, assisted us by participating in our deliberations and locating helpful sources of information. Phyllis Franklin was present at our meetings in New York and provided guidance on numerous aspects of our work throughout the development of our report.
</p>

<P>The committee used a number of different avenues to collect material for this study. We met with representatives of other organizations with a special interest in scholarly publishing, such as Peter Givler, executive director of the Association of American University Presses, and Carol Mandel, dean of the Division of Libraries at New York University. We read essays and articles on such topics as online publishing, the fate of the scholarly monograph, changes in publication standards for tenure, and the economic problems of university libraries and scholarly presses. We viewed a PBS television show on the electronic preservation of knowledge (<i>Into the Future</i>). In addition to this common body of information, individual members of the committee pursued in greater depth specific questions raised by the larger problem of scholarly publishing today.</p>

<P>
Fortunately, a number of other bodies were already looking, from their own particular vantage points, at some of the issues we were confronting. The Association of Research Libraries, the American Association of University Professors, and the Knight Higher Education Collaborative have already conducted intensive inquiries into questions concerning scholarly publishing. We have profited greatly from reports by these organizations, which provided helpful suggestions and some of the statistics from which we have drawn our conclusions.
</p>

<P>
At the MLA convention in December 2000, the committee arranged a panel to inform the membership about some of the information we had gathered and to solicit further ideas about questions we needed to explore. We presented papers on the economic factors at work in scholarly publication today, the problems and the potential of electronic publication, the relation between publication and the award of tenure, the difficulties faced by scholars working in smaller fields, and the opportunities provided by academic publishers in Europe and Latin America.
</p>

<P>A subsequent advertisement in the <i>MLA Newsletter</i> generated a number of responses from members of the association. The advertisement called for comments on the following topics: changes in publishing practices and opportunities, changes in tenure standards and expectations, the potential role and challenges of electronic publishing, library acquisition policies, and publication issues affecting scholars in languages other than English. Among those who responded were department chairs, editors of specialized scholarly journals, younger scholars seeking tenure, and graduate students anxious to establish a publication record. These responses added depth and detail to our understanding of the issues involved, heightening our awareness of the complexity of this situation. It became clear that scholarly publication is enmeshed in a set of complex and often conflicting expectations.</p>

<H2>Rationale for the Committee's Recommendations</h2>
<P>
The committee has come to understand that there is no ready or simple solution to the current crisis. The interlocking network of forces that has contributed to the situation of scholarly publishing today calls for an array of different solutions, each of which can only be partial. The recommendations sketched below are guided by our belief in and commitment to scholarship in our fields.
</p>

<P>
We hope this document will help us move forward to a better-informed understanding of the causes of the problems currently faced by those who wish to publish in the language and literature disciplines. We address our suggestions to members of the Modern Language Association in their various professional roles. We urge members to be actively involved in the struggle for a solution to the challenge of scholarly publishing today, interacting with colleagues in their own and other departments, with the administrations of their institutions, and with the libraries. It is especially important that more experienced members act as mentors for junior colleagues working toward tenure.
</p>

<H3>Recommendations for Departments</h3>
<OL>
<LI>Departments, in formulating their guidelines for tenure and promotion, should bear in mind the dramatic changes that have occurred in scholarly publishing practices and alter their expectations with regard to all levels of scholarly publishing. Departments should engage in dialogue about these standards with other humanities departments at the same institution and other institutions and work energetically to inform their administrations about changes in publication conditions specific to their disciplines.
<LI>Departments should work vigorously against the tendency toward increasing expectations with regard to quantity of publications in tenure and promotion decisions. Recognizing that greater expectations with regard to quantity are likely to prove detrimental to the quality of scholarship, they should work toward developing alternative conceptions about the body of work on which tenure and promotion decisions will be based.
<LI>Departments should develop a broader understanding of what is important for their field and the contribution they make to the educational mission of their institution. They should articulate their position with regard to specific categories of scholarly publications, including editions and translations in addition to more traditional specialized monographs.
<LI>Departments should recognize that we are in a period of transition with regard to electronic publication and Web archives. Working with appropriate committees and administrators, they should develop guidelines about how these will be evaluated.
<LI>Departments should be aware that changing conditions have made subventions an increasingly common factor in scholarly publishing, and they should support their faculty members accordingly.
<LI>Departments should have university administrations make their new guidelines clear to outside evaluators.
</ol>


<H3>Recommendations for University Libraries</h3>
<OL>
<LI>University libraries should continue to build their collections with the clear understanding that print publications, especially books, are still paramount for the humanities. Ready access to books, including those from earlier periods, those addressing topics that may be of relevance for a comparatively small number of faculty members, and both translations and original versions of books in foreign language areas, is of crucial importance to scholars in languages and literatures.
<LI>Libraries should recognize that the mission of universities is endangered if funding goes primarily to the sciences, leaving little for the humanities, which are at the heart of the liberal arts enterprise.
<LI>In the development of electronic resources, libraries should budget sufficient money for humanities databases.
<LI>Libraries should involve faculty members in setting priorities for acquisitions in their fields and in ordering appropriate publications.
</ol>

<H3>Recommendations for Publishers</h3>
<OL>
<LI>We urge university presses to resist pressures to commercialize their operations and to ensure that they maintain their mission to publish scholarly work, including specialized and single-author studies and scholarship, including translations and editions, that are not part of the traditional Anglo-American canon.
<LI>We urge presses and scholarly journals to communicate to their authors realistic time lines with respect to the review, editing, and production processes.
</ol>

<H3>Recommendations for University Administrations</h3>
<OL>
<LI>Administrations should be aware of the radical changes in scholarly publishing that are taking place and the particular pressures that obtain in the languages and literatures. We urge them to meet with departments to review existing criteria for scholarly publishing and decide if they are appropriate to the institution. In particular, the kinds of publications deemed appropriate for tenure should not be restricted to traditional monographic studies.
<LI>In setting library budgets, administrations should be mindful that acquisitions are essential for the health of the humanities. A single intellectual ecology connects the production of scholarship in a field, the library acquisitions in that field, and the field's overall health.
<LI>Administrations should recognize that university presses are essential for the scholarly mission of the university and that this mission is jeopardized to the extent that presses are expected to function as commercial ventures. We urge universities to give their presses more financial support.
<LI>Insofar as institutions require increasingly heavy publication burdens, we urge administrations to support colleagues' scholarship, especially during the probationary period, by means of paid leaves, course relief, and research funding.
<LI>We urge administrations to establish subvention funds to help with publication costs (including permissions fees), with special emphasis on subsidies for faculty members attempting to place their first book.
<LI>We urge administrations to evaluate thoughtfully scholarship that appears in overseas presses, as well as scholarship in languages other than English. They should be mindful of the variety of publication practices and procedures for evaluation of submissions.
</ol>

<P ALIGN=RIGHT>
<I>Judith Ryan, Harvard University, chair<BR>
Idelber Avelar, Tulane University<BR>
Jennifer Fleissner, University of California, Los Angeles<BR>
David E. Lashmet, Independent Scholar<BR>
J. Hillis Miller, University of California, Irvine<BR>
Karen H. Pike, University of Toronto<BR>
John Sitter, Emory University<BR>
Lynne Tatlock, Washington University</i>
<P>
<HR>
<H1>Works Cited and Selected Bibliography</h1>
<P>Association of Research Libraries. "Graph 2: Monograph and Serial Costs in ARL Libraries, 1986-2000." <i>ARL Statistics</i>. 30 July 2002. 1 Aug. 2002 <<a href="http://www.arl.org/stats/arlstat/graphs/2000t2.html" target="_blank">http://www.arl.org/stats/arlstat/graphs/2000t2.html</a>>.<p>Besser, Howard. "Digital Longevity." <i>Handbook for Digital Projects: A Management Tool for Preservation and Access</i>. Ed. Maxine Sitts. Andover: Northeast Document Conservation Center, 2000. 155-66.</p>
<p>Broughton, Walter, and William Conlogue. "What Search Committees Want." <i>Profession 2001</i>. New York: MLA, 2001. 39-51.</p>
<p>Case, Mary M. "Principles for Emerging Systems of Scholarly Publishing." <i>ARL: A Bimonthly Report</i> 210 (2000): 1-4.</p>
<p>Chodorow, Stanley. "The Once and Future Monograph." <i>Specialized Scholarly Monograph</i>. 11 Sept. 1997 <<a href="http://www.arl.org/scomm/epub/papers/chodorow.html" target="_blank">http://www.arl.org/scomm/epub/papers/chodorow.html</a>>.</p>
<p>Ekman, Richard. "Economics of Scholarly Communication." <i>New Challenges for Scholarly Communication in the Digital Era: Changing Roles and Expectations in the Academic Community</i>. ARL Scholarly Communications, 8 Apr. 1999 <<a href="http://www.arl.org/scomm/ncsc/ekman.html" target="_blank">http://www.arl.org/scomm/ncsc/ekman.html</a>>.</p>
<p>Faherty, Robert L. "Response to Richard Ekman." <i>New Challenges for Scholarly Communication in the Digital Era: Changing Roles and Expectations in the Academic Community</i>. ARL Scholarly Communications. 17 May 1999 <<a href="http://www.arl.org/scomm/ncsc/faherty.html" target="_blank">http://www.arl.org/scomm/ncsc/faherty.html</a>>.</p>
<p>Givler, Peter. Letter to Martha Evans for the Ad Hoc Committee on the Future of Scholarly Publishing. 5 Apr. 2000.</p>
<p>---. "Scholarly Books, the Coin of the Realm of Knowledge." <i>Chronicle of Higher Education</i> 12 Nov. 1999: A76.</p>
<p>Greco, Albert N. "The General Reader Market for University Press Books in the United States, 1990-99, with Projections for the Years 2000 through 2004." <i>Journal of Scholarly Publishing</i> 32.2 (2001): 61-86.</p>
<p>"Humanities and Arts on the Information Highways." Project report. Getty Art History Information Program, Coalition for Networked Information, and American Council of Learned Societies. Sept. 1994 <<a href="http://www.cni.org/projects/humartiway/humartiway.html" target="_blank">http://www.cni.org/projects/humartiway/humartiway.html</a>>.</p>
<p>Humphreys, R. Stephen. "Why Do We Write Stuff That Even Our Colleagues Don't Want to Read?" <i>Specialized Scholarly Monograph</i>. 11 Sept. 1997 <<a href="http://www.arl.org/scomm/epub/papers/humphreys.html" target="_blank">http://www.arl.org/scomm/epub/papers/humphreys.html</a>>.</p>
<p><i>Into the Future: On the Preservation of Knowledge in an Electronic Age</i>. By Terry Sanders. PBS. WNET, New York. 13 Jan. 1997.</p>
<p>Knight Higher Education Collaborative. "Op. Cit." <i>Policy Perspectives</i> 10.3 (2001): 1-11.</p>
<p>Lehmann, Chris. "Commercial Presses: Burning Down the House." <i>Lingua Franca</i> Sept. 1997: 48-52.</p>
<p>Lynch, Clifford. "The Battle to Define the Future of the Book in the Digital World." 13 May 2001 <<a href="http://www.firstmonday.dk/issues/issue6_6/lynch/index.html" target="_blank">http://www.firstmonday.dk/issues/issue6_6/lynch/index.html</a>>.</p>
<p>---. "The Scholarly Monograph's Descendants." <i>Specialized Scholarly Monograph</i>. 12 Sept. 1997 <<a href="http://www.arl.org/scomm/epub/papers/cliff.html" target="_blank">http://www.arl.org/scomm/epub/papers/cliff.html</a>>.</p>
<p>Magner, Denise K. "Seeking a Radical Change in the Role of Publishing: Universities Seek to Fix a 'Broken System' and to Change the Way Professors Are Evaluated." <i>Chronicle of Higher Education</i> 16 June 2000: A16.</p>
<p>McGann, Jerome J. "Radiant Textuality." <i>Victorian Studies</i> 39 (1996): 379-90.</p>
<p>---. <i>Radiant Textuality: Literature after the World Wide Web</i>. New York: St. Martin's, 2002.</p>
<p>Modern Language Association of America. "Statement on the Significance of Primary Records." <i>Profession 95</i>. New York: MLA, 1995. 27-28.</p>
<p>Nathan, Peter. "Promotion: A Triple Whammy at Research Universities." <i>Specialized Scholarly Monograph</i>. 11 Sept. 1997 <<a href="http://www.arl.org/scomm/epub/papers/nathan.html" target="_blank">http://www.arl.org/scomm/epub/papers/nathan.html</a>>.</p>
<p>Okerson, Ann. "With Feathers: Effects of Ownership on Scholarly Publishing." Sept. 1991 <<a href="http://www.library.yale.edu/%7eokerson/feathers.html" target="_blank">http://www.library.yale.edu/~okerson/feathers.html</a>>.</p>
<p>Pochoda, Phil. "Universities Press On." <i>Nation</i> 29 Dec. 1997. Dept. of English. Brock U. 1 Aug. 2002 <<a href="http://www.brocku.ca/english/courses/4F70/univpresses.html" target="_blank">http://www.brocku.ca/english/courses/4F70/univpresses.html</a>>.</p>
<p>Ruark, Jennifer K. "University Presses Suffer Bleak Financial Year." <i>Top Education News</i>. U of Houston. 12 July 2001. 1 Aug. 2002 <<a href="http://www.uh.edu/admin/media/topstories/2001/07/chron_712b.html" target="_blank">http://www.uh.edu/admin/media/topstories/2001/07/chron_712b.html</a>>.</p>
<p>Schiffrin, Andr�. "Payback Time: University Presses as Profit Centers." <i>Chronicle of Higher Education</i> 18 June 1999: B4.</p>
<p>"Scholarly Editions in Jeopardy." Editorial. <i>New York Times</i> 21 Oct. 2000: A28.</p>
<p>Shifflett, Crandall. "Scholarly Exchange: Electronic Publication and Scholarship in the Humanities." Address. Virginia Polytechnic Inst. and State Univ. 23 Feb. 1998. </p>
<p>Solow, Robert M., Francis Oakley, Phyllis Franklin, John D'Arms, and Calvin Jones. <i>Making the Humanities Count: The Importance of Data</i>. Cambridge: Amer. Acad. of Arts and Sciences, 2002.</p>
<p><i>The Specialized Scholarly Monograph in Crisis; or, How Can I Get Tenure If You Won't Publish My Book?</i> Conf. sponsored by the Amer. Council of Learned Societies, the Assn. of Amer. Univ. Presses, and the Assn. of Research Libraries. Washington. 11-12 Sept. 1997. 11 Sept. 1997 <<a href="http://www.arl.org/scomm/epub/papers/index.html" target="_blank">http://www.arl.org/scomm/epub/papers/index.html</a>>.</p>
<p>Sullivan, Teresa. "The Future of the Genre." <i>Specialized Scholarly Monograph</i>. 12 Sept. 1997 <<a href="http://www.arl.org/scomm/epub/papers/sullivan.html" target="_blank">http://www.arl.org/scomm/epub/papers/sullivan.html</a>>.</p>
<p>Summerfield, Mary, Carol Mandel, and Paul Kantor. <i>The Potential of Online Books in the Scholarly World. </i>Online Books Project. Columbia U. Dec. 1999. 1 Aug. 2002 <<a href="http://www.columbia.edu/cu/libraries/digital/texts/about.html" target="_blank">http://www.columbia.edu/cu/libraries/digital/texts/about.html</a>>.</p>
<p>Thatcher, Sanford G. "Thinking Systematically about the Crisis in Scholarly Communication." <i>Specialized Scholarly Monograph</i>. 11 Sept. 1997 <<a href="http://www.arl.org/scomm/epub/papers/thatcher.html" target="_blank">http://www.arl.org/scomm/epub/papers/thatcher.html</a>>.</p>
<p>Wasserman, Marlie. "How Much Does It Cost to Publish a Monograph and Why?" <i>Specialized Scholarly Monograph</i>. 11 Sept. 1997 <<a href="http://www.arl.org/scomm/epub/papers/wasserman.html" target="_blank">http://www.arl.org/scomm/epub/papers/wasserman.html</a>>.</p>
<p>Waters, Lindsay. "Are University Presses Producing Too Many Series for Their Own Good?" <i>Chronicle of Higher Education</i> 27 Oct. 2000: B7-9.</p>
<p>---. "A Modest Proposal for Preventing the Books of the Members of the MLA from Being a Burden to Their Authors, Publishers, or Audiences." <i>PMLA</i> 115 (2000): 315-17.</p>
<p>---. "Rescue Tenure from the Tyranny of the Monograph." <i>Chronicle of Higher Education</i> 20 Apr. 2001: B7.</p>
<hr>

<p>This report was originally published in <i>Profession 2002</i> (New York: MLA, 2002) 172-86.
</p>


<a href="/repview_future_pub">< Previous</a>
