<h1>The Committee's Recommendations</h1>
<h2 NAME="grad">Recommendations to Graduate Programs</h2>
<h3>Departmental Self-Study (See <a href="/studyguide">Guide</a>)</h3>
No consideration of the problems of professional employment can avoid addressing directly the fact that the structure of the academic marketplace and the system of graduate education have over the last quarter century led to a significant discrepancy between the number of PhDs granted and the number of tenure-track positions that the economy of higher education has made available. The obvious response, that new admissions to graduate programs should be drastically cut back, is inadequate to the crisis of the 1990s and to the complex system of professional employment that this crisis has exposed. Not only has the MLA no mechanism for mandating the appropriate size of individual programs, but recommendations for system-wide cutbacks are likely to have unintended and chaotic consequences, particularly for small departments and institutions, for equity in access to higher education, and for the future of the profession. The authority for determining size must therefore rest with individual departments, which should be mindful of their ethical and professional obligation to their students, their institutions, and their communities as well as to the discipline as a whole. Our profession must address the current crisis, but global effects can only be achieved through conscientious actions by individual departments.

Accordingly, we recommend
<UL>
<LI>that all departments undertake a self-study following the guidelines provided in the document <i>Evaluating the Mission, Size, and Composition of Your Doctoral Programs</i>
<LI>that where numbers of students are not being placed in the positions for which they are trained, a department should</a>
	<UL>
	<LI>reduce program size or
	<LI>revise its mission and program to accommodate preparation for more-diverse job opportunities and placements
	</UL>
<LI> but that no cutbacks be undertaken that might compromise the diversity of the graduate student population
</UL>

<h3>Distribution of Information for Prospective Graduate Students</h3>
It is important to ensure that candidates for admission to graduate programs, particularly doctoral programs, have the information they need to determine whether they wish to pursue advanced work and whether, if so, they have chosen an appropriate department.

Accordingly, we recommend that graduate programs routinely send applicants the following information:
<UL>
<LI>data on the state of the job market (to be made available by the MLA) 
<LI>specific data on the placement of recipients of PhDs from the department in the last three to five years (names of institutions, numbers of placements, fields of specialization) 
<LI>data on the time recent PhD recipients in the department took to earn the degree 
<LI>an account of the patterns of financial support (including tuition waivers and benefits) available for graduate students in the department 
<LI>a description of the services that the university and the department provide graduate students seeking both academic and nonacademic employment
</UL>

<h3>Funding and Employment of Graduate Students</h3>
Graduate students should be treated as members of the professional community from the outset. All too often institutions use PhD programs in ways incompatible with the best interests of doctoral students, exploiting PhD candidates to provide relatively inexpensive teaching for recurring instructional needs, to support other aspects of the university's work, or to justify allowing more faculty members to teach graduate courses. Institutional needs for graduate student labor should not shape the size or content of PhD programs. Unless appropriate limits are imposed on graduate student teaching loads, we will continue to reinforce the multitier job system described elsewhere in this report. Emphasis on the professional nature of the degree should lead programs to enroll only as many students as the condition of the marketplace might justify, to ensure reasonable and equitable support for all students in a program, and to focus graduate training on areas most appropriate to the real working conditions of the discipline.

Nevertheless, the committee recognizes that the potential objectives of PhD programs are diverse and may include the nonprofessional goal of simply satisfying students' interest in extended study of the humanities. Programs should be sufficiently flexible that the professional model and full-funding principle do not exclude fully prepared students who have other objectives. In reflecting such flexibility in enrollment procedures, each PhD program should carefully determine what constitutes good practice.

Accordingly, we recommend
<UL>
<LI>that all full-time doctoral students have full funding (ideally a combination of fellowships and teaching or research assistantships for at least five years, with fellowship support in the first and last years) 
<LI>that remuneration of graduate student employees should recognize the professional nature of the services they render and should, at a minimum, be sufficient to support study without the need for additional employment or loans 
<LI>that when teaching, graduate students should have primary responsibility for no more than one course each term and that teaching should never be so onerous as to interfere with the timely completion of the dissertation
</UL>

<h3>Job Placement outside the Academy</h3>
For at least the next five years it is virtually certain that a substantial number of new PhDs will be unable to find full-time tenure-track positions. It is therefore incumbent on departments and institutions to make every effort to help these PhDs find employment outside the academy. If this effort is to succeed, all faculty members--especially those in doctoral programs--will have to change the way they view and discuss nonacademic career options and the way they treat graduates who establish careers outside the academy.

Accordingly, we recommend
<UL>
<LI>that departments identify and keep track of graduates employed in a variety of sectors 
<LI>that departments invite these alumni to help other graduates who want employment outside the academy to locate job opportunities (e.g., alumni might network on new graduates' behalf, give formal presentations and informational interviews, and establish other on- or off-campus contacts with doctoral students)
</UL>

<h3>Expansions of the Graduate Curriculum</h3>
As this report has revealed, the job system in our field has complex connections with the nature and mission of contemporary doctoral education. Although a successful career in academia has been increasingly defined as a career in a doctorate-granting university, the vast majority of PhD students today will have careers that do not replicate those of their graduate school professors. Thus, so long as the professoriat in the research university tends to undervalue the teaching of lower-division courses, graduate training will not adequately prepare students for the realities of the academic workplace. Most PhDs in our field will spend much of their time doing the kind of lower-division teaching associated with the great experiment in social access that inspires American education, teaching that offers rich rewards to which graduate students ought to be quickly and imaginatively introduced.

Accordingly, we recommend
<UL>
<LI>that doctoral programs offer students courses in pedagogy that will prepare them for a range of teaching situations 
<LI>that programs offer PhD students experiences designed to familiarize them with the complex system of postsecondary and secondary education in this country (comprising four-year liberal arts colleges, community colleges, universities, and private as well as public high schools) and the full range of job opportunities available in that system 
<LI>that such introductions to pedagogy and to the varieties of academic work involve not only colloquia, seminars, and conferences but also, for instance, mentored internships, residencies, and exchanges among institutions along with experiences outside teaching, such as involvement in institutional, administrative, governance, and editorial tasks
</UL>

<h2 NAME="dept">Recommendations to Departmental and Campus Administrators</h2>
<h3>Career-Placement Centers to Accommodate PhDs Seeking Nonacademic Employment</h3>
As we have already noted, in the near future a substantial number of new PhDs will be unable to find full-time tenure-track positions. Institutions as well as individual departments will need to help many of these PhDs find employment outside the academy.

Accordingly, we recommend
<UL>
<LI>that campus career centers as well as individual departments provide specialized counseling for graduate students interested in learning about employment opportunities outside the academy 
<LI>that campus career centers extend their placement services to PhD candidates interested in nonacademic employment
</UL>

<h3>Equitable Treatment of Part-Time and Full-Time Adjunct Instructors</h3>
Our report has documented the extent and pattern of the increasing reliance on part-time faculty members that characterizes the multitier job system as it has evolved throughout academia in recent years. The proportion of all faculty members who are part-timers grew from 22% in 1970 to 40% in 1993 (<a href="/prof_employment12#cahalan">Cahalan et al. 24-25</a>). Yet although part-time and full-time adjuncts teach a significant number of courses in our fields--especially, as we have shown, introductory courses--they do not usually receive pay and benefits commensurate with the professional services they render. As we have noted in the body of our report, such poor working conditions can have a serious impact not only on the quality of classroom instruction but also on crucial institutional responsibilities like advising and mentoring students, committee work, curriculum development, materials review, and other professional functions that maintain the integrity of a department and its curriculum. Yet despite considerations of quality, so long as an inequitable reward structure persists, many financially pressed institutions are likely to intensify their exploitation of jobless PhDs, thus on the one hand reinforcing the multitier job system and on the other hand eroding the tenure system.

Accordingly, we recommend
<UL>
<LI>that the compensation of part-time faculty members be substantial enough to ensure that they are supported in, and can be held accountable for, course preparation, student involvement, and professional development appropriate to their responsibilities 
<LI>that there be an "equitable provision of salary based on a standardized salary policy that remunerates for commensurate qualifications and is indexed to full-time faculty salaries (aiming for pro rata compensation) rather than per-course-hour rates" (<a href="/prof_employment12#statement"><I>Statement</I> 58</a>) 
<LI>that all part-time faculty members, along with full-time adjuncts, receive essential fringe-benefit opportunities (especially health and life insurance and retirement contributions) on at least a pro rata basis 
<LI>that all part-time faculty members receive sufficient notice of their appointments or reappointments to enable them to prepare their courses adequately
</UL>

<h3>Conversion of Part-Time Positions to Full-Time Positions</h3>
To ensure the educational quality of English and foreign language courses and programs, maintain the integrity of the profession, and improve employment opportunities for new PhDs, administrative reliance on part-timers for course coverage should be drastically reduced, and additional full-time positions should be created to meet the instructional needs currently handled by part-timers. Ideally such positions should be tenure-track, but even full-time non-tenure-track positions would have the advantage of offering regular benefits and would allow those hired to participate fully in the work of the department.

Accordingly, we recommend
<UL>
<LI>that departmental and campus administrators make every effort to convert an optimal number of part-time positions to full-time--preferably tenure-track--positions
</UL>

<h2 NAME="mla">Recommendations to the MLA</h2>
<h3>Distribution of Departmental Self-Study Guidelines</h3>
Many of the committee's recommendations imply intensified or additional MLA activities. To facilitate swift implementation of our recommendation for departmental self-study, for instance, we have prepared a document entitled <I>Evaluating the Mission, Size, and Composition of Your Doctoral Programs</I>. But we consider this document so crucial that we would like it to be regularly available to all MLA members, separately from the report.

Accordingly, we recommend
<UL>
<LI>that the MLA publish and distribute guidelines for departmental self-study as a separate document
</UL>

<h3>Further Collection and Dissemination of Placement Statistics</h3>
Although the MLA already regularly surveys the state of our profession, including the job market and the situation of graduate study, the collection and dissemination of placement statistics might be undertaken more often. In addition, the MLA can assist prospective graduate students by publishing the widely useful aspects of the information gathered through departmental self-studies.

Accordingly, we recommend
<UL>
<LI>that the MLA not only continue to publish and distribute data on the state of the job market but also, wherever it seems appropriate, publish and distribute specific data on patterns of PhD placement that include as much information as possible about placement patterns in particular departments or kinds of departments
</UL>

<h3>Publication of a Guide to Graduate Studies and Careers for PhDs</h3>
Although the committee believes that individual departments must assume primary responsibility for informing prospective doctoral candidates about the purposes of graduate study and the functionings of the academic job system, including the needs and practices associated with various higher-education institutions, here too, as in the dissemination of self-study guidelines and job-market figures, the MLA can offer significant assistance.

Accordingly, we recommend
<UL>
<LI>that the MLA staff prepare a new publication that would function as a guide to graduate studies, on the model of the current <I>MLA Guide to the Job Search</I>
</UL>

<h3>Open Dialogue on Nonacademic Career Services with Council of Graduate Schools</h3>
We have already noted that departmental and campus administrators should open career-placement and counseling services (hitherto reserved for undergraduates) to doctoral candidates to help PhDs entering the current job market find employment outside academia whenever necessary. But we think that in this matter too the MLA can be of considerable assistance.

Accordingly, we recommend
<UL>
<LI>that the MLA seek the support of the Council of Graduate Schools in urging universities to make career services available to graduate students as well as to undergraduates
</UL>

<h3> Renew Commitment to Affirmative Action in Admissions and Hiring</h3>
Both the shrinkage of the job market and the cutbacks in graduate programs that some departments may undertake in response to that shrinkage might well threaten gains in student and faculty diversity that have been made in recent decades. As the committee has already declared, however, we feel strongly that policies of open access to higher education, including graduate school, should be not only continued but expanded and in particular that no cutbacks in doctoral programs should compromise the diversity of the graduate student population.

Accordingly, we recommend
<UL>
<LI>that the MLA reassert its commitment to affirmative action in graduate school admissions as well as in departmental hiring
</UL>

<h3> Collection and Publication of Information about Hiring-Procedure Problems Confronted by Job Seeker</h3>
There is considerable evidence--much of it, to be sure, anecdotal--that the tight job market has sometimes produced situations where job seekers feel abused by hiring committees and departmental administrators. Ranging from tales of unacknowledged application letters to reports of requests that candidates pay their own way to on-campus interviews, these signs of trouble, even of ethical failure, in hiring procedures suggest that the MLA might have a further role to play beyond its current dissemination of good-practice guidelines in the <I>Job Information List</I>.

Accordingly, we recommend
<UL>
<LI>that either through the current standing Committee on Academic Freedom  and Professional Rights and Responsibilities (CAFPRR) or through another body, the MLA act as a clearinghouse for information about problems in hiring procedures confronted by job seekers 
<LI>that job seekers who encounter ethical problems report their difficulties to the CAFPRR or another appropriate MLA committee
</UL>

<h3>Public Representation of the Profession</h3>
The MLA already represents its members on issues crucial to our profession through ongoing communication with other disciplinary organizations, through contacts with the American Association of University Professors, and through such special projects as the September 1997 Conference on the Growing Use of Part-Time and Adjunct Faculty and the recent series of radio programs designed to educate the public about our work. Such activities protect the integrity of professional employment, and in the long term they will help address many of the systemic problems outlined in this report. For this reason, the committee affirms the necessity of maintaining and strengthening these and other MLA projects that promote the common interests of teachers and scholars in the modern languages.

Accordingly, we recommend
<UL>
<LI>that the MLA continue its dialogue with other organizations on issues of academic employment 
<LI>that the MLA regularly publicize this work in print communications with the membership and on the MLA World Wide Web site
</UL>

<h3> Greater Outreach to Community Colleges and Organizations</h3>
As we have noted in our report, since 50% to 60% of first- and second-year college students in the United States attend community colleges and since much of the instruction in college English and foreign languages is offered at the first- and second-year levels, it is important for doctoral programs to engage in meaningful dialogue with community colleges, which offer some of the most significant job opportunities to PhDs in the fields we represent.

Accordingly, we recommend
<UL>
<LI>that the MLA (perhaps making special use of the recently established Committee on Two-Year Colleges) enter into and sustain stronger relations with such community college organizations as the Community College Humanities Association 
<LI>that wherever possible the <I>Job Information List</I>  carry notices of jobs in two-year- and community college English, foreign language, and humanities departments
</UL>

<h3> Uses of the MLA Web Site to Facilitate or Enhance Professional Employment Opportunities</h3>
The committee congratulates the MLA on its inauguration of an organizational World Wide Web site, which will make possible speedier, less cumbersome, and less expensive dissemination of specific as well as general information about professional employment. We note that a number of organizations and publications--from the American Mathematical Society to the <I>Chronicle of Higher Education</I> and the <I>New York Times</I>--are using electronic media to transmit job listings and introduce Web users to a range of career issues and options. As the MLA staff continues to build and diversify links on the new Web site, we believe that increasingly imaginative ways of communicating with job seekers will become possible.

Accordingly, we recommend
<UL>
<LI>that the MLA expand access to online job listings 
<LI>that the MLA staff investigate ways of constructing an online data bank of job candidates 
<LI>that the MLA make creative use of the Web site to distribute specific as well as general information about nonacademic career possibilities 
<LI>that as soon as is feasible the MLA distribute online as well as in print job-market information (including placement statistics in whatever form is appropriate), guides to graduate study, and other information called for in this report 
<LI>that the MLA make this committee's report available online as soon as possible
</UL>


<a href="/prof_employment">< Index</a> | <a href="/prof_employment11">Continue ></a>
