<h1>Developing an Academic Career</h1>
1. What are the research and teaching qualifications for being hired in English for the tenure track in
<ul>
<li><a href="#engAAqualifications">an AA-granting department?</a>
<li><a href="#engBAqualifications">a BA-granting department?</a>
<li><a href="#engMAqualifications">an MA-granting department?</a>
<li><a href="#engPHDqualifications">a doctoral department?</a>
</ul>

2. What are the research and teaching qualifications for being hired in foreign language for the tenure track in
<ul>
<li><a href="#flAAqualifications">an AA-granting department?</a>
<li><a href="#flBAqualifications">a BA-granting department?</a>
<li><a href="#flMAqualifications">an MA-granting department?</a>
<li><a href="#flPHDqualifications">a doctoral department?</a>
</ul>

3. How important
<ul>
<li><a href="#gradimportance">is the graduate school I attend?</a>
<li><a href="#scholarimportance">are the scholars I study with, the classes I take, and the grades I earn?</a>
</ul>

4. How do I build a record of
<ul>
<li><a href="#research">research in graduate school?</a>
<li><a href="#teaching">teaching in graduate school?</a>
<li><a href="#service">service in graduate school?</a>
</ul>

5. How do I
<ul>
<li><a href="#mentors">identify and work effectively with mentors in graduate school?</a>
</ul>

<a name="engAAqualifications"> </a>
<h2>What are the research and teaching qualifications for being hired in English for the tenure track in an AA-granting department?</h2>
You need to look for teaching experience, even if only at the level of aide or tutor, in a two-year-college setting in order to show intent.  Two-year colleges aren't looking for the market's rejects; they want to find people who want to teach two-year college students.  The experience might also yield a letter of recommendation on two-year-college letterhead, which looks good. You need to have CVs and letters of recommendation aimed at two-year-college jobs.  Emphasize the teaching rather than the dissertation. Do some research into the two-year colleges where you apply, to find out something about the system.  Are they independent of the state universities?  Are they primarily transfer institutions or vocational schools?  You will need help finding where this information exists.
<blockquote>
Melissa Kort
Department of English
Santa Rosa Junior College
</blockquote>

Community colleges are looking first and foremost for committed teachers who have demonstrated skills in successful classroom instruction for adults. Typically, this means experience teaching lower-level college or university writing and literature courses, including tutoring in writing centers.  Also of importance are flexibility and sensitivity to the needs of traditional students (recent high school graduates) and nontraditional students (who may be returning to the classroom after years of rich real-world experience but who have many other demands on their time and attention besides studying English). In the community college, research qualifications come a distant second to teaching, although a record of classroom research and publication on pedagogical issues and strategies would likely be viewed very positively.
<blockquote>
Leslie Prast
Department of English
Delta College, Michigan
</blockquote>
<a name="engBAqualifications"> </a>
<h2>What are the research and teaching qualifications necessary for being hired in English for the tenure track in a BA-granting department?</h2>
Departments in BA-granting institutions tend to be small, so we don't have the luxury of hiring people whose research and teaching interests are narrowly defined or limited to one field or subfield.  In my department, we look for candidates who are excited about what they do—about their research, about the possibility of teaching what they know best—and who are willing to teach  courses outside their areas of specialization. We expect our new colleagues to contribute to general education by cheerfully teaching either college writing or one or both semesters of our core course, Culture and Society in the West. We look for applicants who have some teaching experience beyond their work as TAs.  While we don't expect an extensive publication record,  we do look for evidence of potential for excellence in scholarship, whether in the form of conference participation, published articles, or a dissertation abstract that jumps off the page. The single most off-putting question we were ever asked by a candidate at an MLA interview was, &quot;What are the possibilities for research leaves in the first two years of an assistant professor's career at Purchase?&quot;  We were not interested in someone who clearly wanted to take off before she had even arrived.
<blockquote>
Louise Yelin
Literature Board of Study
Purchase College
</blockquote>

We don't speak of the &quot;most impressive mind,&quot; &quot;a first-rate intellect,&quot; we don't talk about &quot;major coups,&quot; &quot;aggressive appointments,&quot; or even about &quot;rising stars.&quot;  We talk about &quot;a match.&quot; We talk about teaching English composition, about general education required courses, about contributing to our curriculum revision process, about adapting pedagogy to different learning styles, about group work, the writing process, contributing to grant proposals, working to train the best secondary school teachers we can.

"<a href="javascript:popup('/bulletin_112011','docs_popup');">The Most of It: Hiring at a Nonelite College</A>"
<blockquote>
Nona Fienberg
Department of English
Keene State College
</blockquote>
<a name="engMAqualifications"> </a>
<h2>What are the research and teaching qualifications necessary for being hired in English for the tenure track in an MA-granting department?</h2>
Many institutions whose research and publication expectations were once relatively minimal are now more stringent, a change made possible in part by the competitiveness of the job market, and often those increased expectations are not matched by corresponding reductions in teaching loads. Still, the pressure to publish is usually not as intense at these institutions as it is at most research institutions. Many comprehensive universities, moreover, have somewhat broader definitions of what constitutes appropriate scholarly activity than major research universities do. We might value pedagogical research, for example, or the publication of an original and innovative textbook, whereas many research institutions rank that kind of work below other forms of scholarship. We might give more credit than some research institutions would for juried conference presentations—such as ones at MLA—that haven't yet seen print. If an excellent teacher can demonstrate that he or she is making steady progress on a book, a comprehensive university might allow a longer gestation period for that project than a research institution would. After all, our faculty members are teaching more students and grading more papers than colleagues at research institutions are, and they probably have fewer opportunities for released time. We might also more readily encourage engaging in nontraditional forms of scholarly activity or writing for popular, rather than strictly academic, audiences.

At job interviews, we might be more interested than research universities in hearing how candidates' scholarly interests inform their teaching of undergraduate courses or how candidates would envision working with undergraduates and MA students on research. Like other kinds of institutions, we'll want to know what balance of teaching and research candidates perceive to be appropriate in their careers. I'm not just talking here about a balance of time, though that's important. I'm thinking as well of questions like the following: How and why does research excite them? What kind of research most engages them? How crucial is it for them to have easy and immediate access to major research libraries? How important is it to them to teach advanced graduate seminars in their current areas of investigation? What kind of support do they need to carry out research programs that would satisfy them?

"<a href="javascript:popup('/bulletin_111014','docs_popup');">What Is a Comprehensive University, and Do I Want to Work There?</A>"
<blockquote>
Marcia Dalbey
Department of English
Eastern Michigan University
</blockquote>
<a name="engPHDqualifications"> </a>
<h2>What are the research and teaching qualifications necessary for being hired in English for the tenure track in a doctoral department?</h2>
Like most PhD departments, my department looks for applicants with the most promising scholarly projects.  We like projects that are ambitious, well-conceived, and likely to have an influence in the field.  If applicants have already published an article or two, it is easier for us to have confidence in their ability to bring their larger, book-length projects to fruition, but we do consider applicants who have not yet published.  In such cases, it is especially important that applicants be able to describe their work in terms that suggest its potential even to people outside their fields.  At the time of tenure, we expect a book or its equivalent.

We also expect all tenure candidates to demonstrate their effectiveness as teachers, and we might put more weight on this expectation than some other institutions do.  Applicants to tenure-track positions must have prior teaching experience and must be able to write and talk about their experience in reflective ways.  They should be able to write and talk about a few other courses in their fields that they would eventually like to teach.  All faculty members at Miami teach regularly at all levels of our curriculum, so we look for potential colleagues who share our belief that teaching lower-level courses to nonmajors can be as rewarding as teaching upper-level courses to majors and graduate students.
<blockquote>
Barry Chabot
Department of English
Miami University, Oxford
</blockquote>
<a name="flAAqualifications"> </a>
<h2>What are the research and teaching qualifications necessary for being hired in foreign language for the tenure track in an AA-granting department?</h2>
A candidate's research will have little influence on the selection committee unless it is applied research directly involving the academic area in which the applicant will be teaching. For example, a candidate applying for an ESL position should include and highlight research done on successful methodologies used in teaching English as a second language to adults.  Or, if you are applying for a position to teach specific foreign language courses to English-speaking adults (business, for example), studies developed in this area would be impressive.

In my state, as in many others, a minimum of an MA degree in the academic subject is required for teaching community college courses. State certification also requires successful completion of a course on community colleges that is taught in all state universities and almost all colleges in Arizona.  An applicant can teach for two years on a temporary certificate but at that point cannot continue teaching without finishing this required course. Find out the requirements for the state and the institution in which you would like to teach.

Teaching experience in the discipline is highly desirable.  This can include experience as a teaching assistant in a university or, ideally, as an adjunct faculty member at a community college.
<blockquote>
F&eacute; Brittain
Department of Languages
Pima Community College, Arizona
</blockquote>

Suggestions for preparing and applying for community college teaching positions:
<ul>
<LI>Visit local community colleges and meet department chairs. Ask for their advice and for introductions to teachers amenable to having observers in their classrooms. Visit their classes.
<LI>Take courses in second language acquisition and in assessment and methodology to prepare yourself for teaching, not just for lecturing.
<LI>Find out what is required to teach at the community college you are applying to. Certification requirements differ.
<LI>Apply as an adjunct faculty member. Experience a few semesters in the actual classroom to see if you like teaching in this setting.
<LI>If you decide to apply for a full-time position, do not do so with the thought that it will do until a &quot;real&quot; job at the university comes along! Such a strategy is not fair to you, to your students, or to the institution. You should make a full commitment to the community college.
</ul>

"<a href="javascript:popup('/bulletin_311013','docs_popup');">Opportunities for PhDs in the Community Colleges</A>"
<blockquote>
F&eacute; Brittain
Department of Languages
Pima Community College, Arizona
</blockquote>
<a name="flBAqualifications"> </a>
<h2>What are the research and teaching qualifications necessary for being hired in foreign language for the tenure track in a BA-granting department?</h2>
Most BA-granting instutitions are concerned about a candidate's ability to teach a wide range of courses in language, literature, and culture. <I>Job Information List</I> postings most often ask for a recommendation letter from someone who can write about a candidate's teaching style, in addition to letters about intellectual and research interests. It is beneficial to candidates to manifest teaching experience and teaching success. I would recommend that you prepare a modest portfolio, consisting of such items as a syllabus, some tests, class materials, a Web project, or other ancillary materials to show the hiring department what you have created in a course.  This portfolio need not be sent with an application but should be available at an interview or on demand from the hiring institution.

While many BA-granting institutions seek dynamic teachers, departments are also interested in people engaged in activities that show a lively and curious intellect. Although scholarly papers and publications serve as the profession's benchmark of this liveliness, candidates can certainly emphasize other accomplishments as well: participation in a department colloquium, creative writings, bibliographic work, and so on. Of keen interest to hiring departments may be the candidates' reflection on the relation of their research or doctoral thesis to potential courses at the institution. Thus it may be beneficial to create one or two courses that you would like to teach and be able to talk about them with the hiring department members.

In teaching and in research qualifications, candidates should try to portray themselves as accurately as they can. Then it is up to the hiring department to decide if those qualifications fit its needs.
<blockquote>
Richard C. Williamson
Department of Classical and Romance Languages and Literatures
Bates College
</blockquote>

What qualifications should an ideal candidate for an undergraduate teaching position have? I believe that the desired qualifications fall into three categories: competence, collaboration, and commitment. The first of these <EM>cs</EM>, competence, includes linguistic, cultural, and pedagogical skills. The second <EM>c</EM>, collaboration, refers to the ability to cooperate effectively with others: students, departmental colleagues, peers in other departments, and people from the local community. The third <EM>c</EM>, commitment, refers to a continuing dedication to serving one's institution in the ways it values most. At Ohio Northern, commitment to creative, effective teaching is the primary responsibility. Second in importance is commitment to serving the department in program-building activities such as recruiting new majors, organizing and leading study-abroad programs, advising majors on graduate study or jobs, publicizing department achievements or events, and generally helping promote the programs. Third is commitment to serving the institution through committee work, grant writing, making conference presentations, and publication of articles or books. Finally, there is commitment to serving the community by attending local events and speaking at community organizations and in area schools to promote language study.

"<a href="javascript:popup('/bulletin_273007','docs_popup');">Preparing for Undergraduate Teaching: Competence, Collaboration, and Commitment</A>"
<blockquote>
Patricia S. Dickson
Department of Modern Languages
Ohio Northern University
</blockquote>
<a name="flMAqualifications"> </a>
<h2>What are the research and teaching qualifications necessary for being hired in foreign language for the tenure track in an MA-granting department?</h2>
We seek to hire the candidate who shows excellent potential as a scholar and has already demonstrated creativity and enthusiasm as a teacher.  We want people who are able to contribute in various ways to our department's teaching mission, who can offer courses in the foreign language and in English, and who will make a mark in the profession.  Everyone we hire is expected to do his or her fair share of the service duties that all our faculty members must shoulder.  For the purpose of evaluating performance, we use the following percentages: 40% of the evaluation is based on research, 40% on teaching, and 20% on service.
<blockquote>
Richard Zipser
Department of Foreign Languages and Literatures
University of Delaware, Newark
</blockquote>
<a name="flPHDqualifications"> </a>
<h2>What are the research and teaching qualifications necessary for being hired in foreign language for the tenure track in a doctoral department?</h2>
When looking at dossiers, I always pay attention to the subject of the doctoral dissertation.  Does it show creativity?  Does it suggest a sophistication in methodology?  Will it lead to a book-length publication?  These sorts of issues reveal whether a candidate at a research university has the intellectual curiosity and creativity-and drive-to be a successful and original scholar.  The doctoral dissertation should be part of a program of intellectual inquiry that will lead the candidate to ongoing creative projects. Candidates are usually swept up in the intensive work relating to their dissertations.  They should be able to show their ability to go beyond the thesis, to talk about intellectual issues in a broad context as well as in the technical, specialized world of their dissertation.

In recent years, the stakes have risen, as graduate students regularly give papers and publish.  Papers and articles at graduate student conferences and in graduate student journals are useful for the experience they give, but more persuasive are papers and essays in major outlets that go beyond the range of the usual seminar paper.

With globalization and technological advances, linguistic competence is now at a very high level among candidates in the foreign languages.  Most of them have experience in teaching language and in the theory of language teaching.  If candidates are in literary or cultural studies, it is important for them to seek experience in teaching the humanities or in a more advanced language setting (such as an introduction to literature course).  They should know how to create a syllabus and should be able to suggest courses that go beyond the often narrow topic of the dissertation.  It is wise to consult the course offerings and requirements of the university where one is a job candidate in advance of the initial interview and to have a sample syllabus or two available.

Obviously, new PhDs can't be expected to have the maturity of a newly tenured associate professor, but search committees are looking for someone who engages in lively intellectual dialogue, who has a spark of creativity.  Make sure your CV portrays your distinctive personal qualities, as well as your scholarly accomplishments and promise.
<blockquote>
Patricia Ward
Department of French and Italian
Comparative Literature Program
Vanderbilt University
</blockquote>
<a name="gradimportance"> </a>
<h2>How important is the graduate school I attend?</h2>At my institution, candidates from prestigious institutions have no advantage over others with stronger records of publication. I must confess to preferring frightened, self-conscious workaholics from lesser schools whose need to prove themselves borders on monomania. We have had a few difficult experiences with graduates of prestigious institutions who believed that their prestigious degrees somehow compensated for their subsequent lackluster efforts in research and publishing.

"<a href="javascript:popup('/bulletin_085046','docs_popup');">'Young Blood' and Other Gothic Inspirations: The Hiring Process</A>"
<blockquote>
Zack Bowen
Department of English
University of Delaware
</blockquote>
<a name="scholarimportance"> </a>
<h2>How important are the scholars I study with, the classes I take, and the grades I earn?</h2>
The classes you've taken establish your preparation in whatever fields you're applying to teach; actual teaching experience in those areas is even more important. Graduate courses are all graded roughly the same; your grades aren't likely to be given much notice, unless they're extremely low.  (You can always use your cover letter to explain your grades, if you feel you need to.)  The scholars you've worked with can be important in two ways: if they speak for the field and even the profession because they are well known or if someone in the department you're applying to knows one of your recommenders.  But applicants can be put at a disadvantage if the well-known scholar uses similar superlatives in recommending multiple graduate students who apply for the same job.
<blockquote>
Cynthia Lewis
Department of English
Davidson College
</blockquote>

Routine checking of the educational background of candidates includes the graduate grades but not specifically the courses. Usually grades are part of the dossier supplied by the career or placement offices. This dossier provides a kind of pedigree and is read with a view toward names one knows. If prominent scholars testify on behalf of the candidate's great performance in class, it can help. But I would not overestimate the importance of scholars the candidate has studied with. The quality and sincerity of their recommendations can vary. The reports about good or bad teaching and other factors often have more weight.
<blockquote>
Frank Trommler
Department of Germanic Languages and Literature
University of Pennsylvania
</blockquote>
<a name="research"> </a>
<h2>How do I build a record of research in graduate school?</h2>
Writing samples drawn from the dissertation best indicate to potential colleagues an applicant’s ability to conceive a research program and carry it to completion. A dissertation should have a definite research component on which any theoretical, explicatory, or interpretive writing rests, and applicants can better spend their time perfecting their style and their ability to articulate effectively the aims, results, and significance of their research and thinking than taking time to present conference papers or small essays in areas outside the topic of the dissertation.

There are two lines of thought on style. The first claims that languages and literatures are as specialized professions as law and economics and that their research writing style should demonstrate a command of the relevant professional jargon and topoi.  The second, as recently demanded by Edward Said in his 1999 presidential address to the MLA, calls for writing in languages and literatures to be lucidly precise, historically informed, and available to specialists, nonspecialists, and intellectually interested readers throughout society.  The contest between these two opinions will not be reconciled soon, and even though orthodoxy tolerates a modicum of essential professional language, ordinary writing seems now to be most desired.

Institutions that do not emphasize research as part of their internal career ladder are more likely than major research universities to welcome a CV displaying a range of competences in a variety of areas valuable to a generalist curriculum.  A set of conference papers or publications in graduate student and other professional journals may well have importance for such institutions. Dissertation chapters rewritten as professional essays and placed in important journals in relevant fields have value for all institutions except those that do not count research in hiring or promotion. But candidates should make the publication of even such essays secondary to the completion of the dissertation requirement.
<blockquote>
Paul Bov&eacute;
Department of English
University of Pittsburgh, Pittsburgh
</blockquote>

In a difficult job market, students need to do whatever they can to impress potential employers. A record of conference papers and articles accepted for publication may help set an applicant apart. Individual departments and professors differ on the advisability of submitting materials to journals and to conferences before the receipt of the degree. Most would agree, however, on the importance of judicious screening of material. Students should realize that very few class papers are ready for submission without revision. Graduate programs give students the chance to seek the counsel of faculty members. Students should consult first with the teachers for whom they wrote the papers—and then with other faculty members—on the advisability of submitting the papers and, once the decision has been made, on how to revise and where to submit.

Making one's self marketable need not be a bad thing. The preparation of a conference paper should go beyond class work, inspire additional dialogue, and give the student experience in introducing and defending ideas, much as classroom teachers and researchers do. Well-written conference papers set forth new concepts or approaches. They give speakers a forum and a foundation for future investigation. Using Patricia Meyer Spacks's terms, one could argue that the questions <EM>What can I say? </EM>and <EM>What do I need to know? </EM>do not have to oppose each other. One can seek the path of least resistance, or one can—quite literally—do one's homework. The difference between the two is the difference between a conference paper and professional effort.

"<a href="javascript:popup('/bulletin_273017','docs_popup');">The Captive Audience; or, Liberating Thoughts on Conference Papers</A>"
<blockquote>
Edward H. Friedman
Department of Spanish and Portuguese
Indiana University, Bloomington
</blockquote>

The professional conference creates a deadline. Once the abstract of a paper yet to be written has been sent in and once the airline ticket is bought, somehow the paper will get done, just as graduate school papers always somehow got done by the due date. But the same paper that costs the presenter $400 and the university another $400 if it is presented at a conference—for which the presenter must miss at least one class day and often two—can be mailed to a journal for $1.35, plus another $1.35 for the return envelope. What a deal: presenters don't miss any teaching days, and they save themselves and the university $797.30. They do miss the collegiality, the contacts, the free soda on the airplane, and the night on the town in Cincinnati or Kansas City, but all the other benefits that I have mentioned still accrue.

The process is fairly simple and well-known. Consult a list of journals. (Libraries have such lists in abundance.) List five addresses. Send the paper to the first one. When it comes back, send it to the next one. Going through the list usually provides the necessary feedback or perspective for rewriting the paper. The next step is to send the rewritten paper to five more markets. The universally agreed-on rule of thumb is not to give up on a piece until it has been rejected by at least ten publications. Most of us have trouble continuing to send out a paper that has been rejected. As with diets, persevering is especially hard if no one knows about the effort.

But where I teach, I have helped establish what I call a blood-oath club. Each member describes his or her current project and a time line for completing it, specifying completion and send-off dates. That information is circulated to other members of the club, who occasionally inquire how the project is going. Such a group can easily expand to offer more services&mdash;editing or even regular meetings&mdash;though such steps should be taken one at a time.

"<a href="javascript:popup('/bulletin_118028','docs_popup');">A Chair's Notes toward a Definition of Scholarship in the Small State University</A>"
<blockquote>
George Slanger
Department of English
Minot State University
</blockquote>
<a name="teaching"> </a>
<h2>How do I build a record of teaching in graduate school?</h2>
Teaching experience <I>and</I> composition knowledge (course work at the graduate level) are required criteria for any tenure-track-job ad developed for my English department. Candidates without these qualifications won't make the cut. Their applications will not even be circulated to the search committee members, because they don't meet minimum qualifications.

My advice to job candidates with PhDs from foreign countries who would like to be hired at institutions like mine would be to get some part-time teaching experience and sign up for TA composition training or graduate training in composition.
<blockquote>
Jane Frick
Department of English, Foreign Languages, and Journalism
Missouri Western State College
</blockquote>
<a name="service"> </a>
<h2>How do I build a record of service in graduate school? </h2>
One very reliable indicator of effective service is membership on departmental  committees.  Candidates who have had experience on, for example, curriculum or textbook committees or who have been members of search committees stand out from others.  Volunteering to mentor an undergraduate language club or helping to organize departmental events (lecture series, tutoring sessions, social hours) can add substantially to a CV.  Being a  member of the graduate student senate (especially as one of its officers) and  its committees, serving as a student member on faculty senate committees, and participating on ad hoc administrative committees all demonstrate a commitment to service.  Membership on one of the MLA's committees is another valid service activity. And going that extra mile to engage in  outreach to the community (e.g., volunteering to tutor in public schools) is a  plus.
<blockquote>
Frank Hugus
Department of Germanic Languages and Literatures
University of  Massachusetts, Amherst
</blockquote>
<a name="mentors"> </a>
<h2>How do I identify and work effectively with mentors in graduate school?</h2>
The first steps are to take advantage of your professors' office hours, involve yourself in departmental activities, and use your intuition.  Get to know the graduate faculty, including people outside your immediate area of study, since your mentors need not actually be your teachers. You are looking for people who will engage you as a person and colleague as well as as a student.  After all, you are looking to develop a working relationship over the course of your career as graduate student and, if you have the good fortune, beyond.  Departmental stars are always good to know, but they do not necessarily make good mentors: they are often too busy, and they usually have an abundance of students.  Unfortunately, these people often provide rather standard and brief letters of recommendation.  Invite them to sit on your dissertation committee so they know your work, but ask someone more available to direct.  You want mentors who will take time to know you fully, who are willing to discuss your teaching and are interested in your overall career.  These people are the ones who write the strongest letters of recommendation, rich in detail, because they know who you are. Good graduate school mentors are accessible, personable, and student-centered.  They are capable of nagging you and expressing disappointment as well as praise:  they will be comfortable enough to be honest with you.  And you will feel comfortable enough to be honest with them.  In working with them, remember that they are people, too: inviting someone out for a cup of coffee makes a great impression and allows for a relationship that has a human dimension.  My graduate mentors made all the difference in my experiences in graduate school and beyond.  I had to take the risk to get to know them, but that risk has been richly repaid.
<blockquote>
Keith Welsh
Department of English
Webster University
</blockquote>
