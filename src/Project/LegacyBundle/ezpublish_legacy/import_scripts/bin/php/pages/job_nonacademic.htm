<h1>Developing a Nonacademic Career</h1>
<ol>
<li><a href="#nonacajob">How do I look for a nonacademic job?</a>
<li><a href="#jobmaterials">How do job materials and the application process differ from those of academic jobs?</a>
<li><a href="#gradwork">Will my graduate work be seen as positive or negative in the nonacademic world?</a>
<li><a href="#move">Can I move into academia after doing nonacademic work?</a>
</ol>
<a name="nonacajob"> </a>
<h2>How do I look for a nonacademic job?</h2>
There is a prevailing myth that good professional-level jobs are not found in the classified ads. This simply is not true: large and small organizations advertise in newspapers, trade journals, and online to create a large enough pool of viable candidates, especially for jobs that require specialized skills. Additional public sources for jobs are institutional, company, or government Web sites. Networking is useful, but that is difficult until one has entered a particular field or professional circle, and even then it can be a hit-and-miss affair. Often it helps, in applying for a job, to be a known quantity, but often it works against one.

Having doctoral experience may allow immediate entry to more responsible and challenging jobs than those available to someone directly out of college. In some areas, those with a doctorate may have to take entry-level jobs but will be able to use their skills, experience, and maturity to rise rapidly. A small number of PhDs moving into business, government, or nonprofits work for an additional qualification (such as a law or management degree), but in an unpublished MLA survey of language and literature PhDs in nonacademic jobs, this course was pursued by less than 5% of those responding, and it does not seem to be necessary in order to find interesting and well-paid work in writing and editing, education, consulting, management, and other fields. Employment of respondents to the survey was divided almost equally among business, government, and nonprofits and included such positions as legislative assistant, personnel officer, international training officer, director of medical education, affirmative action director, planning director, communications manager, systems analyst, newspaper editor, merger brokerage vice president, investment consultancy president, screenwriter, and television producer for employers ranging from the Folger Library to the National Endowment for the Humanities, San Francisco Chronicle, National Geographic, International Monetary Fund, Getty Oil, and Bankers Trust. In other words, having a PhD allows one to look virtually anywhere for employment.

Employers will need to be convinced that an applicant with a PhD has skills beyond the abstruse ones that are valued (or are popularly assumed to be valued) in academia. The ability to do research and analysis; organize large projects; and clearly communicate complex ideas to a mainstream sector of the population are widely applicable skills of the PhD. During graduate school, part-time jobs, internships, and volunteer work outside the university can help demonstrate this applicability and smooth the transition to nonacademic work and may themselves turn into full-time-job opportunities.
<blockquote>James Papp
Modern Language Association
</blockquote>

No job is too small.  I took a summer job as a lowly word processor at a General Electric plant in town many years ago.  I braced myself for humiliation, but I also relished the opportunity to work at a job that would not ooze into all areas of my life.  One day, I overheard some people talking about a speech that they were working on for the president of the company, and I offered my services.  I worked on the team, they nearly doubled my salary, they gave me the floor when we were giving the president advice on the script and his delivery, and they were really sorry when I left.  A firm belief in the English doctorate's transferable nature, an open mind, and a willingness to pay even more dues (this one's particularly difficult after years in grad school) have helped me a great deal.
<blockquote>
Ann Hall
Department of English
Ohio Dominican College
</blockquote>

<a name="jobmaterials"> </a>
<h2>How do job materials and the application process differ from those of academic jobs?</h2>
The salient feature of a nonacademic r&eacute;sum&eacute; is its brevity. Where a CV lists every academic article, paper, teaching experience, and committee and can easily run to twenty pages for those in mid-career and over thirty for senior members of the profession, the initial r&eacute;sum&eacute; stays within one page, although professionals also have a longer version that they can send on request if the initial r&eacute;sum&eacute; inspires interest. The next thing that distinguishes a r&eacute;sum&eacute; from a CV is that it is accomplishment- rather than title-driven. Instead of only listing positions held, it describes the work involved, focusing on responsibilities and innovation. This leaves plenty of room for hyperbole, but since personnel directors are as skilled in deconstruction as literature professors are, describing working the register as &quot;cash-flow resolution&quot; is likely to be counterproductive. For professionals with important responsibilities—a teaching assistant in a doctoral program, for instance—measured and economical language that summarizes duties and skills is appropriate. While employment is described in brief narrative detail, cataloguing is discouraged. An employer will be interested that an applicant can write but not in the title of everything he or she has written. Employers are often interested in the whole person who might be working for them, and it is common to list outside interests such as sports (doing, not watching). These activities also serve as conversation starters in interviews.

There are plenty of books on r&eacute;sum&eacute; writing, and some of them promulgate bizarre practices that an academic newcomer may well imagine are universal (such as stating a career goal at the top). The ideal for a r&eacute;sum&eacute; is to represent yourself honestly in a way that is likely to attract the attention of someone who is quickly scanning dozens of other such documents to fill a job that is appropriate to your skills. Whatever the guidebooks or consultants say, don't put anything on your r&eacute;sum&eacute; that you are uncomfortable with, either in content or style. Trust to the fact that there are people out there like you who are eager to hire someone like you.

Nonacademic application letters are also one page, unless for very advanced positions, and should not be performances of your rhetorical sophistication. The language should be plain, paratactic, and to the point: your appropriateness for the needs advertised and your desirability as an employee. Often an expert (from the department where you want to work) and a nonexpert (from the human resources department) will be involved in the hire, and language that pays more attention to content than style is likely to balance the interests of each.
<blockquote>James Papp
Modern Language Association
</blockquote>
<a name="gradwork"> </a>
<h2>Will my graduate work be seen as positive or negative in the nonacademic world?</h2>
My best advice to job seekers looking outside academia is to believe that you have something valuable to offer. Recently, I participated in a panel discussion at the University of California, Riverside, on the topic of nonacademic employment for people holding advanced degrees. The other panelists included a public historian and an economist. The one common thread that we all touched on was the importance of seeing oneself as an asset to others. Unfortunately, in speaking with my peers, I often sense that seeking nonacademic employment is equated with failure. Job seekers look for nonacademic jobs because they can't find other work. This sort of attitude easily undermines one's performance in a job interview. After all, who would want to hire a failure? Clearly, this problem requires more than a dose of positive self-esteem or a newfound belief in the dignity of all work.
<blockquote>
Cynthia Morrill
Museum Writer, Research and Communications
UCR/California Museum of Photography
</blockquote>

Mark Johnson, an English PhD who works for Intuit, recommends that candidates do summer internships for concrete experience to put on r&eacute;sum&eacute;s as well as freelance writing for a general audience to show potential employers the ability to write for a nonacademic public. While some career centers advise covering up the PhD when applying for nonacademic employment, Johnson argues that, with evidence of clear and accessible writing, a candidate can convince a nonacademic employer that &quot;a PhD is not a detriment&quot; but an advantage. . . . Lisa DeTora, a medical writer, adds, &quot;Don't shun work you think is 'beneath you.' Overeducation should be like good breeding, making you get along with everyone rather than alienating people. This, however, is easier said than done, especially if you use words like 'hegemonic' while describing the candy machine.&quot;

"<a href="javascript:popup('/bulletin_120002','docs_popup');">The Stars and Ourselves: An Ordinary Person's Guide to the English Market</A>"

"<a href="javascript:popup('/bulletin_301044','docs_popup');">The Stars and Ourselves: An Ordinary Person's Guide to the Foreign Language Market</A>"
<blockquote>
James Papp
Modern Language Association
</blockquote>

Prospective employers hire people who are already fully employed. Sounds paradoxical, but the person who is already fully engaged with life, learning, and exploration is the person who is going to be most valuable. When interviewing, don't whine about how you were unemployed or marginally employed for X years after graduating. . . . Even though you might not have had a salaried job for the past four years, you must still impress upon your interviewer the fact that you were, and always will be, fully employed. And that may mean fully employed as an amateur bike racer, world traveler, backyard gardener, connoisseur of Ibsen, or any other passion. The key is to show that you are a person who is 200% occupied with life, and that you will bring that innate passion and curiosity into the workplace.

"Professions beyond the Academy," <I>Profession 1996</I>
<blockquote>
Mark Johnson
Intuit Corporation
</blockquote>
<a name="move"> </a>
<h2>Can I move into academia after doing nonacademic work?</h2>
Many academic departments of foreign languages now value the PhD who can bring practical, discipline-related experience into the classroom. I rather suspect that more pragmatic concerns have come to the fore—namely, what livelihoods can our graduates expect to undertake for professional and economic fulfillment, and who can adequately train and mentor these individuals. Not unlike a number of other institutions that seek academicians with practical experience beyond the ivory tower, my new college was looking for someone who could help to establish a Spanish major with translation and interpreting offerings along with fulfilling other general undergraduate duties.

"<a href="javascript:popup('/bulletin_321050','docs_popup');">A Translation and Interpreting Primer for Foreign Language PhDs</A>"
<blockquote>
Andrew Steven Gordon
Department of Language, Literature, and Communications
Mesa State College
</blockquote>

Since I had helped the Contemporary American Theater with educational activities in the past as a volunteer and since I had my PhD in English, the board hired me as the theater's education director and dramaturge. Today I have returned to academia, lucky enough to have found another dream job, at Ohio Dominican College. Here, of course, I can fulfill my first love, teaching, but my experiences as a dramaturge have enhanced my skills as a teacher, colleague, and researcher. I continue to serve as a dramaturge on one play a year at CATCO, savoring the chance to participate in the practical application of close textual analysis and the opportunity to share my enthusiasm for literature and drama with nonstudents and nonacademics.

"<a href="javascript:popup('/bulletin_123025','docs_popup');">Joining the Circus: Leaving a Tenure-Track Position</A>"
<blockquote>
Ann C. Hall
Division of English
Ohio Dominican College
</blockquote>
