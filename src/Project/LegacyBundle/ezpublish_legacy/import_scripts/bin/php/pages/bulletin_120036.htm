<b>Number 120, Fall 1998</b>
<h1>The Inappropriate Question</h1>
<h2 class="byline">SUSAN KRESS</h2>
THE question is to the academic life what the dollar is to the corporate one: it drives the whole motley enterprise. But, unlike well-paid lawyers, we academics profess to like the kind of question we don&#039;t already know the answer to. As teachers, we spend hours dreaming up the magic questions that will change our students&#039; minds and anticipating, in turn, what questions they might pose that will change ours. As critics and researchers, we want to formulate the questions that will transform a debate or transgress the boundaries of what we think we already know. 

My topic is the subgenre of the inappropriate question in the particular context of the MLA interview. To be sure, as readers and as connoisseurs of character, audience, and convention, we know that subtle codes guide our sense of the appropriate and inappropriate question&#151;and literature furnishes many examples of such questions. Take the first proposal from Mr. Rochester to Jane: wrong; the second: just about right; Mr. Elton&#039;s proposal to Emma: dead wrong; Mr. Knightley&#039;s: right on. I hope you&#039;ll allow that the courtship or proposal scene is an appropriate transition to the occasion of the job interview, although, as in most matters of modern life, the wooing process has been considerably speeded up. Those who secure interviews at the MLA convention can be assured that their interviewers will want to know as much as possible about them&#151;within the confines of approximately three-quarters of an hour. Consequently, each precious question must do marathon work. 

I offered the following advice to job seekers at a preconvention workshop of the 1997 MLA convention: 

As you prepare for your interview, you will doubtless be thinking of all the questions you might be asked. Naturally, you will prepare for questions about your education, your views on teaching, your classroom philosophy, the particular experiments you have tried, and the new courses you might invent. Naturally, too, you will prepare for questions about the particular contributions your research is making, the research methods you have employed, and the progress you have made. Wisely, you will foresee, and be prepared to defend against, possible objections to your methods, your evidence, and your conclusions. Questions might also arise that relate to the character and mission of the college to which you have applied for a position; naturally, you will expect different emphases on the importance of research, different expectations about the amount and kind of teaching you will do, and different concerns about your taste and capacity for the work of department and college service. 

But despite all the dutiful daytime preparation you do, and despite all your wild-eyed middle-of-the-night catalogs of potential questions, you may be blindsided by a question that, for our present purposes, we will call inappropriate. Of course, any question that you don&#039;t expect may well be called inappropriate. 

While inappropriate questions come in various disconcerting shapes, some are actually illegal. In a checklist of dos and don&#039;ts for interviewers, the MLA warns that questions about age, marital status, children, religion (unless the college is church-related), sexual orientation, or national origin are forbidden. But the human animal is insatiably curious, and forbidden questions are always the most tantalizingly interesting ones. Your interviewers are doubtless too savvy to ask questions like the following: 
<blockquote>
&#147;Will your wife be moving to Haven College with you?&#148; 
&#147;With four children, are you concerned about child care?&#148; 
&#147;Are you an out lesbian?&#148; 
&#147;Do you mind that there&#039;s no synagogue in All Saints City?&#148; 
&#147;Will you miss soul food?&#148;
</blockquote>

Nevertheless, those same interviewers might try an indirect sally or two: 
<blockquote>&#147;Paradise College is located in a wonderful town for families,&#148; one interviewer might say, hoping for some information about children, a spouse, or perhaps even sexual orientation. </blockquote>
or 
<blockquote>&#147;Oplocunay. That&#039;s an interesting family name. I&#039;ve done considerable research into names and never come across that one before,&#148; another might remark, hoping to find out about national origin&#151;and perhaps, as an extra bonus, religious background. </blockquote>
or 
<blockquote>&#147;How long did you say you worked as a glassblower before deciding to get your BA?&#148; a third might ask, hoping for some hints about your age. </blockquote>

But before we talk about how to field these kinds of questions if they come your way, let&#039;s look at some other questions&#151;not illegal, but perhaps inappropriate and certainly loaded. These questions can cover a fairly broad range, of course. Let me offer just a few examples: 
<blockquote>
&#147;Are you a deconstructionist?&#148; 
&nbsp;a postcolonialist?&#148; 
&nbsp;a feminist?&#148; 
&nbsp;a multiculturalist?&#148; 
&nbsp;a postmodernist?&#148; 
&nbsp;a Republican?&#148; 
</blockquote>

Depending on the politics (academic or governmental) and persuasions of those in the room, such questions can be loaded indeed. And while we are considering loaded questions, how about this one: 
<blockquote>&#147;Did you read my book <em>Postmodern Meditations on a Postcolonial Multiculturalism? </em>&#148; </blockquote>
or&#151;worse still&#151; 
<blockquote>&#147;You would see just how weak your position is if you had read my book <em>Feminist Fallout. </em>Did you at least see it reviewed in <em>Fausse Femme Fatale? </em>&#148; </blockquote>
or 
<blockquote>An ideological skirmish breaks out among the assembled interviewers, and you are asked, &#147;Well&#151;which of us do you think is right about Jane Gallop [or Edward Said]?&#148; </blockquote>

Another line of questioning might follow not so much academic as domestic politics: 
<blockquote>&#147;At Buddy-Buddy College, we like to think of ourselves as a family. We all pitch in. We have potlucks twice a semester. What&#039;s your dish?&#148; </blockquote>
or (relatedly) 
<blockquote>&#147;The person you&#039;re replacing made the most delectable cookies. Do you bake?&#148; </blockquote>
or 
<blockquote>&#148;Our students are used to lots of attention from us. We frequently invite them home for dinners or barbecues. Will your spouse be comfortable with that?&#148; </blockquote>

Some interviewers, knowing full well it is inappropriate, still can&#039;t resist commenting on appearance: 
<blockquote>&#147;That tie. It&#039;s very elegant. Did you really buy it in Kentucky?&#148; </blockquote>
or 
<blockquote>Do you realize that your skirt is the color of cinnamon on a fresh-baked peach pudding?&#148; </blockquote>

There are many other possible questions, of course; these are just a few examples. How should one respond to these kinds of questions? It bears remembering, of course, that the goal of the MLA interview is not (at least at that moment) boldly to declare the question illegal or inappropriate and whip out a yellow pad to start the documentation needed for a lawsuit; it is to land the campus interview and thereafter get the job offer. You may ultimately decide that you do not want the job, but at least the decision will be yours to make. So it doesn&#039;t help if you respond to an inappropriate question with an inappropriate reply: 
<blockquote>
&#147;I don&#039;t believe I have to answer that!&#148; 
&#147;Cookies are not in the job description, are they?&#148; 
&#147;Isn&#039;t that a sexist question?&#148; 
&#147;You&#039;ve been staring at my skirt for ten minutes.&#148; 
&#147;It&#039;s illegal to ask about my age.&#148;
</blockquote>

Clearly, you don&#039;t want to bring the conversation to a dead stop; you do want to coax it in a more propitious direction. Assume, at least for the duration of the interview, that the interviewers have the best motives for their questions; they have, after all, through a rather arduous process of elimination selected you for an interview. They want to give you every opportunity to shine. 

In this regard, it sometimes helps to play for time, to figure out what is behind the interviewers&#039; questions. A question about your potluck dish may really be a question about your willingness to invest time in a college community. An indirect question about your age may betray an anxiety about your energy level, especially if the department is staffed almost entirely by faculty members of advancing years. A question about theory may mask a fear about what your presence will mean to the uneasy balance of power in the department. A question about the relocation of a spouse may point to concern about whether you&#039;ll be a commuting, and perhaps an absentee, faculty member. 

You might, then, instead of answering an inappropriate question about, for example, the relocation of your spouse, ask directly about what may be driving the question: 
<blockquote>&#147;Are you concerned about the commitment of time I would make to Quicksand U.?&#148; </blockquote>

In response to a question about your theoretical persuasion, you might ask: 
<blockquote>&#147;Do the faculty members at Killjoy State agree about introducing theory into the undergraduate classroom?&#148; </blockquote>

You will be wise not to answer a question with a question of your own too often, but in some circumstances the tactic may get the interviewer back on an appropriate track. 

When possible, take comments about your appearance (tie) at face value; just say thank you. More fanciful remarks (about colors) might best be met with an enigmatic smile. 

Otherwise, try to draw out your interviewers. Get them to give you information about the kind of place they teach at. And try to keep control of the interviewing situation: if discussion seems to be veering off at an irrelevant tangent as two of the interviewers try to remember the name of a lead actor in a long-gone Broadway play, gently guide the conversation back to the play&#039;s relation to your research. 

Being truthful is the best approach, but if you think in advance about the kinds of questions I&#039;ve discussed here, you might prepare answers that supply the most economical version of the truth that you wish to offer. 

While you will want to parry, ward off, deflect, or redirect some questions, you may want to answer others&#151;inappropriate or not. Your responses will, of course, vary according to your personality and your sense of your audience. Some of you will want to respond in kind to in-your-face questions, while others will prefer a more cautious approach. One feisty friend of mine is a lawyer named, we&#039;ll say, Angelopoulos. When she was asked the name question, she shot back, &#147;It&#039;s Greek. And I&#039;m proud of the contributions Greeks have made to civilization.&#148; Those who are not as litigious as she, however, might offer a more temperate response. 

My purpose is to encourage you to expect inappropriate questions so that you will not be disconcerted by them. Plan for them&#151;even though you don&#039;t yet know what they are&#151;as much as you would for questions related to your education, your teaching, or your research. Don&#039;t be shocked or taken aback. Instead, take a breath, count to three, take the measure of the room, and choose the best way to answer back. 


<em> The author is Professor of English at Skidmore College. This paper was presented at the preconvention workshop for job seekers at the 1997 MLA Annual Convention in Toronto. </em> 


&#169; 1998 by the Association of Departments of English. All Rights Reserved.
