<h1>Directions for Preparing Manuscripts</h1>
Please follow these directions in preparing your manuscript for submission to the MLA book publication program. Care taken during the initial stages of manuscript preparation will facilitate evaluation and accelerate production. 

Whether you are an author, a coauthor, a translator, an editor, or a contributor to an edited volume, you will find important guidelines on this page. Contributors to an edited volume will need to consult with the volume editor when preparing materials for submission; volume editors are ultimately responsible for seeing that contributors follow the guidelines.

<H2>Documentation</H2>
1. <b>MLA style</b>
All documentation in the manuscript should follow MLA style as set out in the third edition of the <i>MLA Style Manual</i>.

2. <b>Uniform editions</b>
Citations of major works should be uniform throughout a given volume. For instance, in a book about Germaine de Sta&euml;l, all citations should refer to the same edition of de Sta&euml;l and the same translation of her works. For an edited collection, the volume editor should establish the editions to be used and inform contributors accordingly. If it is not feasible for contributors to use the same edition or translation, the volume editor should make sure that the editions used are properly identified.

3. <b>Uniform references</b>
References to major works should follow a consistent form throughout a given volume. For instance, essays in a book about Thoreau should refer consistently to &quot;the Journal,&quot; &quot;the journals,&quot; or &quot;the <i>Journals</i>.&quot; For an edited volume, the volume editor should establish the convention and inform contributors accordingly.

4. <b>Works-cited list</b>
<ul><li>If you are a volume editor, ask your acquisitions editor whether the works cited will be listed at the end of each essay or in a comprehensive list for the volume. If a comprehensive list is required, you are responsible for consolidating the list. Please also submit individual lists (on paper, not electronically) for each essay to assist the copyeditor. </li>
<li>A full works-cited entry (see <i>MLA Style Manual</i>, 3rd ed., ch. 6) must be provided for every work quoted, discussed, described, or referred to in a substantive way in the running text, the notes, and the appendixes.</li>
<li>Inclusive page numbers should be given for all short works (e.g., articles, essays, poems, short stories) in books and periodicals.</li>
<li>If an essay is dropped in the course of manuscript preparation, the works referred to in that essay should be removed from the works-cited list (unless they appear in other essays). If an essay is added, citations from that essay need to be added to the list. If an essay is revised, the list should be revised accordingly.</li>
<li>When two or more works are cited from one collection, a complete entry should be given for the collection and individual pieces should be cross-referenced to it (see <i>MLA Style Manual</i>, 3rd ed., 6.4.6).
<li>Works-cited entries should be provided for primary as well as secondary works, for classic as well as modern works.</li></ul>
5. <b>Parenthetical references</b>
When the works-cited list shows more than one work for an author, all parenthetical references for that author should include a short title; for example, (Frye, <i>Anatomy</i> 10-11), (Frye, <i>Fables</i> 202).

<H2>Instructions for preparing print and electronic versions of your manuscript</H2>
1. Any manuscript submitted should be formatted as follows:
<ul><li>Use one font for everything, preferably 12-point Courier.</li>
<li>Double-space everything, including notes, the table of contents, block quotations, and works-cited lists.</li>
<li>Use the tabulation key rather than the space bar to indent text.</li>
<li>Set off quoted text of more than four lines as a separate, double-spaced block of text,  with an extra line above and below.</li>
<li>Make the left margin 1'', the right 1.25'', with 1'' at top and bottom. Set text ragged right, not right-justified.</li>
<li>Minimize formatting in the text: do not use all caps, display type, linked text, etc. Do not use bullets or ornaments. Do not use automatic hyphenation.</li>
<li>Use italics to indicate text that will be italicized. </li>
<li>Place heads and subheads flush left. Differentiate head levels by using boldface for level-l heads, normal type for level-2 heads, italics or underlining for level-3 heads. </li>
<li>Supply illustrations, tables, and charts in separate files. Indicate where they go in the text, but do not embed them. </li>
<li>Number pages consecutively.</li></ul>
2. If you are the editor of a volume, make certain that every contribution is formatted as above. Any person responsible for an entire volume should note the following:

<ul><li>Ask each contributor to submit an e-file of his or her essay.</li>
<li>Be sure that the volume is paginated consecutively, from beginning to end, with each file numbered to follow the preceding one.</li>
<li>Whenever you translate from one word-processing program to another, proofread for problems (e.g., loss of italics, faulty character substitutions) and correct.</li>
<li>Save each essay as a separate file. Save front matter (e.g., title page, copyright page, table of contents, acknowledgments) and back matter (e.g., notes on contributors, works-cited list) also as separate files. Use <i>MS Word</i> in a recent edition. Supply software information--for example, &quot;<i>Word 2000</i> for <i>Windows</i>.&quot;</li>
<li>If you compile a works-cited list from the individual contributors' lists, it is helpful if you provide paper copies of the individual lists along with your manuscript and disk.</li></ul>
<H2>Submitting the final manuscript</H2>
When you have completed the manuscript, including all front and back matter, print the entire manuscript and copy all files to the disk you will send the MLA. Your final printout should match the electronic files exactly. Last-minute corrections can be made to your printout in colored pencil or pen. The editorial department checks your printout for corrections but uses its own printout from the disk for copyediting. Questions about the above directions or other matters concerning manuscript preparation should be addressed to your acquisitions editor, who may refer you to the production manager for MLA publications if technical support is requested.

<H2>Permissions</H2>
Authors should be aware that it is their responsibility to obtain permission in using material beyond fair use, including quoted matter, photographs or other illustrations, charts, and student writing (see <i>MLA Style Manual</i>, 3rd ed., 2.2.13-14). The permission should include wording allowing use of the material in electronic format as well as in print. Any costs associated with reproducing such material are also the responsibility of the author.


Questions about the above directions or about manuscript preparation should be addressed to your acquisitions editor.
