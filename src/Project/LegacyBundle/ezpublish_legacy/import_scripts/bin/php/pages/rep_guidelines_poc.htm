<h1>Guidelines for Good Practice by the Committee on the Literatures of People of Color in the US and Canada</h1>
<h2>Introduction</h2>
The Committee on the Literatures of People of Color in the United States and Canada (CLPCUSC) began as the Commission on Minority Groups and the Study of Language and Literature (1973-77). The Commission on the Literatures and Languages of America (CLLA) was established to replace the previous commission in 1978 and became a standing committee in 1988. The committee changed its name to the present form in 2000. The CLPCUSC now consists of ten scholars representing research and teaching in African American, American Indian, Asian and Pacific American, Chicana and Chicano, and Caribbean literatures. The committee takes the term <I>people of color</I> to refer to groups that historically have been considered to be racialized minorities in the United States and Canada.

Throughout its history, the committee has urged that approaches to American literature, culture, and languages be reconceptualized to include and to regard the texts, narrative traditions, and critical practices of so-called minority literatures as essential to the canons of college-level literary studies. The committee has implemented this reconceptualization by fostering curriculum development through NEH seminars in Native American and African American literatures, panels at the MLA Annual Convention, and seven volumes of critical essays and course designs. An eighth volume that had its origin in the work of the committee addresses issues of race, power, and gender in the academic workplace.

The committee seeks to institutionalize its approach to American literature, culture, and languages and its concern for faculty members of color by publishing the following two statements of good practice. The first encourages departments of English and foreign languages to continue to broaden the areas of literature, culture, and language in their curricula. The second describes issues that departments need to address as the number of faculty members of color increases. In the twenty-first century, the recruitment, retention, and promotion of scholars of color is of crucial importance for the North American academy.

The guidelines for good practice grew out of a meeting at the 1999 MLA convention with the members of the CLPCUSC, ethnic studies representatives of the Delegate Assembly, and officers of divisions and discussion groups on literatures by people of color. 

<h2>The Study of Literatures, Cultures, and Languages of People of Color in the United States and Canada</h2>
An important educational goal is to help students develop an understanding of diversity through the study of the literatures, cultures, and languages of people of color in the United States and Canada. As the populations of these nations become increasingly diverse, departments of English and foreign languages should evaluate and revise their curricula to be sure they adequately foster this goal.  Formal study can help all students become more sensitive to cultural differences, assist students of color to develop the sense of self-worth necessary to their success in higher education, and train future teachers to be aware of and appreciate cultural diversity.

Therefore, college and university departments of English and foreign languages that teach the literatures, cultures, and languages of the United States and Canada should offer, as appropriate, courses in literatures and cultures of people of color. In addition, these departments should incorporate appropriate literature by people of color in traditional courses. Foreign language programs can play a special role. Students come to college and university speaking and understanding a variety of heritage languages. Departments that teach some form of these languages should include the study of relevant heritage variants in their curricula.

<h2>Recruitment, Mentoring, and Evaluation of Junior Faculty Members of Color</h2>
CLPCUSC recognizes that many departments already follow the guidelines below, which are designed to assist departments that have not yet adopted or are in the process of developing such procedures.

<B>Recruitment.</B> To attract applications from faculty members of color, English and foreign language departments should reexamine their definitions of positions in traditional fields and consider including diverse approaches to literature, language, and culture, such as analysis based on race, citizenship, class, gender, and sexuality. Departments should also consider creating curricula and positions that either focus entirely on or include literatures written and spoken by people of color.

Departmental and program administrators should use nontraditional places to advertise available positions: e-mail discussion lists, journals, and newsletters for various organizations dealing with literatures of color. Additional advertising sites include the e-mail discussion lists and newsletters for recipients of Ford, Mellon, and Javits dissertation and postdoctoral fellowships. These can sometimes be more effective in recruiting faculty members of color than traditional media.

<B>Mentoring junior faculty members.</B> Mentors who are committed to the mentoring process can play an important role for all junior faculty members, but they are particularly important for junior faculty members of color throughout the probationary period. Whenever possible, such mentors should be senior faculty members of color or senior faculty members familiar with both literatures by people of color and the issues that faculty members of color face in traditional English and foreign language departments. 

<B>Evaluation of faculty members for retention, tenure, and promotion.</B> Departments should disseminate to faculty members written information about evaluations and reviews for retention, tenure, and promotion; the time these occur; specific procedures; and opportunities for faculty members to participate in or appeal the process. Such information should be distributed when new faculty members are hired and at appropriate stages in the review and evaluation process.

When evaluating the dossiers of faculty members of color for renewal or promotion, departments should pay particular attention to the following areas:

<I>Teaching.</I> Departments of English and foreign languages need to be aware of the possible effects of race, gender, and sexuality bias on teaching evaluations. Covering or discussing these areas for literary studies can create tensions in the classroom that can be reflected in students' or senior faculty members' evaluations of a faculty member of color, as well as of other faculty members.

Departments should also develop teaching evaluation procedures that will be sensitive to the impact of these issues on students' or faculty members' evaluations of individual faculty members of color.

<UL>
<LI>Departments should give faculty members of color the opportunity to teach upper-division and, if relevant, graduate courses in their fields, whether these fields are ethnic studies or traditional areas of literature. For example, scholars hired for their expertise in the Harlem Renaissance should be encouraged and allowed to develop courses on American modernism, and those hired to teach Asian American poetry should be encouraged and allowed to teach post-World War II American literature surveys.&nbsp;
<LI>Departments should assign new course preparations to faculty members of color or those teaching ethnic studies in the same proportion as they assign them to other faculty members. Because faculty members teaching literatures by people of color are often called on to create new curricula, they may teach a disproportionate number of new preparations.
</UL>

<I>Scholarship.</I> Departments of English and foreign languages need to be aware that faculty members of color may do their research in fields unfamiliar to departmental colleagues. When evaluating the scholarship of any faculty member of color (and that of any other faculty member), departments should seek external reviewers with expertise in the candidate's area of research.

Some of the important presses publishing research on literatures by people of color may not be the traditional ones familiar to English and foreign language departments, colleges, and universities. Consequently, departments should request that external reviewers include in their evaluations comments about the significance of the press to the scholarship in the candidate's field. Further, the department is obligated to inform itself of the significance of the press.

Statements about the importance of the press should accompany the retention, tenure, and promotion materials of the candidate when they are submitted for approval, both in departmental reviews and in those conducted at other levels of the college or university.

<I>Service.</I> Departments should recognize that because there are so few faculty members of color in most colleges and universities overall, whether they teach ethnic or more traditional literatures, these faculty members are frequently asked or expected to perform more service than are others of their rank. Examples of such service include the following:

<UL>
<LI>Faculty members of color are often asked to serve on departmental, college, and university committees. Many faculty members of color have joint appointments or serve in interdisciplinary programs. These appointments increase their service commitments. In addition, faculty members of color advise and mentor students of color and student organizations.&nbsp;
<LI>Faculty members of color are frequently asked to serve on the committees of students whose theses and dissertations deal with issues of race.&nbsp;
<LI>In addition to their service on campus, faculty members of color are often asked by community organizations to help develop educational and cultural programs.
</UL>

Such heavy service loads decrease the time faculty members of color can devote to their scholarship. Consequently, departments should monitor and limit the number of department- or program-related committees on which junior faculty members of color are asked to serve. Further, departments should limit the service of junior faculty members of color on college and university committees.

When, however, junior faculty members of color have performed substantial and outstanding service to the department, college, university, or professional organizations, departments should recognize this service and consider it carefully in their evaluation of these faculty members' qualifications for retention, tenure, and promotion.

<I>A. LaVonne Brown Ruoff and Margarita Barcel&oacute;, cochairs, 2000-01</I>
<I>Jacqueline Goldsby and Lavina Shankar, cochairs, 2001-02</I>
<I>Committee on the Literatures of People of Color in the United States and Canada

The guidelines were endorsed by the MLA's Executive Council in 2002.</I>
