<b>Number 118, Winter 1997</b>
<h1>Negotiating a Job Offer</h1>
<h2 class="byline">PHILIP E. SMITH </h2>
DURING many of my more than eight years as a department chair, I have offered jobs to newly minted PhDs, several of whom have received competing offers that caused me to negotiate counteroffers. I hope that the following outline of the negotiation process will suggest some operating principles for candidates. I present this account in an abstract and generalized way to avoid references to any actual searches or persons. 

After candidates visit our campus, the department votes to determine which ones we will invite to join our faculty. Let&#039;s say the department votes in favor of three; I&#039;ll refer to them as A. B, and C. I write offer letters and ask the three candidates to keep me advised about their negotiations with other universities. Candidate A accepts the written offer immediately, but B and C both inform me that they have been offered higher salaries elsewhere. I work to persuade my dean that we should raise our opening salary by $2,000 to reflect market conditions. I argue that the raise will be equitable only if A also receives it. In addition, to protect assistant professors hired in the previous two years, I persuade the dean to promise them equivalent equity increases in the following year&#039;s pay increments so that these recently hired professors will not be punished by salary compression and will earn an amount appropriately greater than the increased opening salary now offered to A, B, and C. 

B accepts the job because of the salary increase and because of my promise to pursue similar equity raises in future years if opening salaries continue to inflate. C, however, has been offered salaries as much as $5,000 higher than my increased offer. Since I cannot compete with these salaries, I ask her (let&#039;s say C is a woman) what else might be persuasive and discover that she does not regard the higher salaries as significant; rather, she is interested in substituting community service for one course in her year&#039;s workload. With the dean&#039;s approval, I arrange for C to work in a university program that provides after-school literature classes to minority junior high school students and their parents. She finds this proposal satisfactory and accepts it, even though the salary does not match the higher figures she has been offered elsewhere. 

Though departmental colleagues are generally delighted to welcome new faculty members, two responses to such negotiations can be anticipated. My conversations with other chairs around the country suggest that such responses typically come from star professors who have considerable experience negotiating their own generous salary and benefits packages as they move from campus to campus. Surprised to hear that the chair would argue for equity raises for all new candidates as well as for recent hires, a star will remark, &#147;Equity? I didn&#039;t know we had equity here.&#148; Or another star faculty member will query a raise given to a candidate who, like A, has already accepted a job. &#147;If the candidate didn&#039;t ask for it, why give it?&#148; 

These remarks suggest two principles for negotiating. The first, which might be called comparable equity, has to do with the proposed salary and workload compared with the salaries and workloads of other department members. It is particularly important to learn how the proposed salary and workload compare with the salary and workload offered to last year&#039;s hires. A beginning salary is a benchmark for a career; the monetary value may be compressed quickly if a department hires another beginning assistant professor at a higher salary the following year and gives last year&#039;s new hires (and the rest of the faculty) only a nominal raise. Will compensatory increases be given next year if market conditions raise salaries for beginning assistant professors? What&#039;s happening now, this year, to the salaries of last year&#039;s new assistant professors? Comparable equity may not be an issue at unionized campuses or state systems that publish salaries and salary grades, but jobs in such environments are comparatively few, so the question of comparable equity is worth exploring and, if possible, negotiating. 

The second principle of negotiation is that candidates who don&#039;t ask for special benefits receive them. An important question to answer, though, is, &#147;Why is the benefit justified?&#148; A candidate who requests a benefit, especially something unusual or special, should have a rationale. What can the candidate offer if he or she is granted the benefit? Enhanced research? Improved teaching? Special service to the department or institution? Of course, one very good justification for a higher salary or extra qperquisites is an offer from another university with terms that should be equaled, if not bettered. Given the state of the market, candidates will be lucky to receive one offer; even so, they may wish to negotiate to make sure that it is the best the school can afford. 

Candidates may be excited to hear over the telephone that they have won a job, but they should politely insist on receiving the offer in writing before accepting or declining it. In this process, candidates should regard the department chair as an honest broker who nevertheless must negotiate with the institution&#039;s interests in mind. Therefore, candidates may wish to discuss the terms of the job offers with graduate school mentors and to speak with recently hired assistant professors at offering schools to confirm that the terms proposed are comparable to theirs. If candidates then negotiate enhancements, they should receive, sign, and return a revised offer letter reflecting any agreed-on changes. During this process, candidates should be patient. Most chairs must discuss with their deans all revisions to offers before writing to candidates. This process can take days, even weeks: for example, a dean may be slow to approve changes or a chair may have to redraft a letter, send it to a dean for approval, and then incorporate the dean&#039;s editorial changes. At some schools, the modified offer letter may have to be scrutinized and approved by the legal department. 

A candidate who is ready to negotiate should ask the chair about any or all of the terms, working conditions, fringe benefits, and perquisites I list below. At some institutions, many of the items that follow will already be included, though a candidate may wish to improve some terms; at other institutions, a candidate may have to negotiate the inclusion of these items in a contract. 

<h3>Compensation</h3>
<ul>
<li>Salary 
<li>Equity adjustments 
<li>Merit adjustments (Does the institution have a policy? What has its record been in past years?) 
<li>Salary advance (to be paid before appointment begins) 
<li>Course buyouts (trading money for time to work on research) 
<li>Summer salary (after appointment begins) 
<li>Postdoctoral research (What happens if a new hire wins a postdoctoral grant? Does the institution have a postdoctoral grant program?) 
</ul>

<h3>Perquisites</h3>
<ul>
<li>Moving expenses 
<li>Research expenses and leave
	<ul>
	<li>Computer and software or peripherals 
	<li>Research leave during tenure probation 
	<li>Supplementation for outside grants (How often and for how long can winning a grant stop the tenure clock?) 
	<li>Conference or travel expenses, including expenses for overseas conferences and research 
	<li>Special materials purchase or rental (e.g., video or film, software) 
	</ul>
<li>Teaching support and working conditions
	<ul>
	<li>Teaching load and class scheduling (Are there choices?) 
	<li>Course reductions (for research, public service, or other reasons) 
	<li>Time off for course development 
	<li>Photocopying and office supplies 
	<li>Graders for large classes 
	<li>Faculty development and training 
	</ul>
</ul>

<h3>Fringe Benefits</h3>
(What is the standard package? Can special medical, insurance, or other needs be accommodated?) 

<h3>Other conditions</h3>
<ul>
<li>Spousal or partner hiring or job placement 
<li>Fringe benefits for spouse or partner 
<li>Spousal or partner tuition 
<li>Child care arrangements 
<li>Pregnancy or parental leave 
</ul>

Candidates involved in negotiations should bear in mind that they are not only creating favorable terms of employment for themselves but also dealing with immediate supervisors and colleagues whose trust, confidence, and goodwill will be essential during tenure probation. Therefore, candidates should not be shy, should deal firmly but courteously, should ask for what they justifiably need, and should not play games. The best terms are those that both the candidate and his or her new department can agree will enhance the candidate&#039;s work and his or her value to the institution as a teacher, researcher, and colleague. 


<em> The author is Associate Professor of English at the University of Pittsburgh. A version of this paper was presented at the 1996 MLA convention in Washington, DC </em> 


&#169; 1997 by the Association of Departments of English. All Rights Reserved.

