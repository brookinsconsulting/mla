<b>Vol. 29, No. 3, Spring 1998 </b>
<h1>Our Ethical Commitments to the Graduate Students We Train: A Modest and a Not-So-Modest Proposal</h1>
<h2 class="byline">Pennylyn Dykstra-Pruim </h2>
<h2>A Personal Testimony</h2>
<blockquote>Even the trees remind me that I&#039;m far from home. Their leaves are pale, a heathered mix of subtle greens, dried browns, and old grays. </blockquote>

WHILE sitting alone during one of those social functions at a recent conference, I jotted these simple lines down and realized that one day I would like to write a book&#151;a personal narrative having little to do with academia, nothing to do with second language acquisition, and no chance of securing me tenure. And I have pondered, on more than one of those occasional bad work days, whether the writer&#039;s life might not be less stressful and more lucrative than my own. Nevertheless, I am content for the time being to test the waters of academia, to see if I can stay afloat and perhaps make a few successful laps around the lagoon. 

My fledgling career as an apprentice and member of the academy has been marked by a great deal of good luck. After completing a BA at a small liberal arts college, I was accepted into the German graduate program at a large, prominent state university. My initial and naive interests in twentieth-century poetry were soon supplanted by a temporary enthusiasm for German drama. After completing a master&#039;s degree in modern German literature, I maintained my role as teaching assistant in the German department, but I turned away from the well-worn paths of philology and of literature studies; I pursued instead my interests in applied linguistics. At the time I began my dissertation work, the German department was just beginning to recognize applied linguistics as a viable dissertation field within the department proper. <sup>1 </sup>

Although the path I chose proved a bit rough, I managed to finish the research and writing in two years. In my final year of graduate study, the ratio of the number of job openings with a &#147;specialty in language acquisition&#148; to the number of candidates actually researching in that area was fairly high; considering the relatively poor job market that year, I was in demand. After the job search of 1994&#173;95, I was the only candidate from my department to have secured a tenure-track offer and to have received more than one offer. In February 1995 I had a choice of where to go. I am currently at Brown University, and I love it. 

Looking back, I do not recall any professor, especially early on, <em>truly </em>advising me about what course of study or which graduate opportunities to pursue in order to maximize my marketability; nor was there any discussion of whether I should even go into academia or what options outside the academy might suit me better. In all fairness I ought to add that I was afraid to question publicly whether professorship was a good goal for me; I felt at that time that such questioning might jeopardize my financial support. 

After the challenging years of graduate school, I have indeed arrived in the land of plenty&#151;plenty of work, plenty of demands, and plenty of questions. In my first years as a professional academic, I (like a novice swimmer thrown into the middle of the lake) have felt underprepared for the new tasks and responsibilities. I have sought advice from almost anyone who would give it, regarding everything from the reputation and biases of different conferences and journals to the official and nonofficial criteria for promotion; from new teaching ideas to the process of grant application. With two years of teaching as a faculty member, a few conference presentations, a handful of small grants, and a couple of publications behind her, this novice academic is slowly figuring out how to stay afloat. It is true that my personal story has a relatively happy end, but we should not have to rely on good fortune to guide students through their graduate studies and into the academy, or into another profession where they might use their hard-won skills and finely tuned talents. 

Although some programs, such as applied linguistics at the University of Arizona, do a noble job of training graduate students to be teachers, and although there are certainly excellent advisors at any institution, the way many of our apprentices are currently groomed for their multifaceted job as teacher, researcher, and active university member can best be described as haphazard or non-systematic (see also Gorell and Cubillos). Moreover, while some PhD graduates continue to rely on their advisors for mentoring as they begin professional academic life, other recent graduates have been left to adjust and survive quite on their own. 

I am one of the fortune ones: my area of interest proved to be a good commodity on the job market, I have secured a job that allows me to research and teach in my areas of specialty, and I have a few colleagues who have proved willing to answer my sincere if also naive questions. After hearing personal reports of and from some recently graduated colleagues, I realize that a less-than-ideal preparation for life as a professor, a very frustrating job search, and a good deal of exasperation in the first years on the job are more the rule than the exception. Although most careers involve some element of survival of the fittest, the academic institution, and foreign language departments in particular, must take a closer look at how we train and mentor the future professoriat and our most junior colleagues. Some foreign language departments may be currently flourishing; many others have been feeling the effects of decreased enrollments and financial cutbacks. In either case, we as a body of teachers and scholars should assess carefully where we want to go and how we intend to get there. 

<h2><b>The Need for Change</b></h2>
On today&#039;s college and university campuses, it is increasingly important that our foreign language courses be attractive to a broad range of students&#151;in short, that they be well taught, pedagogically sound, and culturally or personally relevant to a wide audience. To reach this goal, we should train teachers in the way they should go, by giving our graduate students more than the basic understanding of the literature in their field (see Roche; Pons). The graduate students who is to make a successful transition from individual, mentored scholar to dynamic, effective teacher must be educated in theory and methodology in all aspects of the language and the literature and culture. Such pedagogical training may involve the use of amusing anecdotes, trial by fire, or quick-trick techniques, but discussion of the research and learning principles behind those suggestions must also be included. 

As we instill in the new professoriat a solid grounding in theory and pedagogy, we must also develop streetwise scholars who can contribute to their field, in order to promote dialogue in and among the various disciplines. Finally, if we expect the new instructors to be engaged, service-minded brothers and sisters in the university community, we need to give them some idea of how to balance teaching, research, and service. Katherine Arens has called for apprentices of the academy to consider their roles and preferences as scholars, teachers, and administrators (community servants). How successful academics balance these three roles involves &#147;a set of tactics that most of us presumably develop as part of &#145;on-the-job training.&#146; Yet they are so basic for survival in those jobs that leaving them to chance means that we fail to acknowledge the reality that scholarship, teaching and academic service are complexly interrelated in ways that are opaque to the uninitiated&#148; (38). 

In addressing the need to give graduate students a well-rounded training in the profession, we ought not ignore the current job market, with what seems like an increasing pool of qualified candidates competing for fewer and less-desirable jobs (see Holub; Huber). Latent in this type of market are a myriad of potential abuses or misuses of our newest colleagues (see Hutner; Pascoe; Caminero-Santangelo). Our reaction to the condition of the academic job market&#151;a condition characterized by Erik Curren as &#147;a kind of academic Great Depression&#148; (58)&#151;may necessitate a reconfiguring of our expectations. Perhaps we should tell our students honestly that they may likely hold at least one temporary, part-time, or limited-term job before securing a tenure-track position (see Belat&#232;che). Indeed, many graduate students seeking jobs in today&#039;s academic market may feel as Linda Lemiesz did about the job search. She writes of the late 1980s and early 1990s, &#147;Prestigious leaders of higher education bemoaned the loss of a generation of scholars. The problem was, we were not lost: we were left on a hillside and devoured by wolves&#148; (68). I would applaud efforts to make available to our students a palette of options for their life after graduate school, and I agree with Lemiesz when she writes &#147;that graduate departments would be healthier if they recognized the endeavors of those who leave the academy&#148; (69). 

Nevertheless, for those firmly pursuing a professional career in the academy and for those of us already established in that endeavor, I offer two proposals. In a general way, these proposals are a pledge of fairness and kindness to our prot&#233;g&#233;s. Specifically, though, adoption of these proposals will give our students a better picture of what life in the academy is like and provide them with a much-needed helping hand as they begin their careers. 

<h2><b>A Modest Proposal: Career Portfolios</b></h2>
I would first like to offer a modest proposal, one which, in contrast to that of Jonathan Swift, represents an honest recommendation. Graduate departments should adopt a career portfolio development program, which would require that students, throughout their years at the university, actively compile a personal career portfolio. Such a portfolio would have components similar to those of the teaching portfolio as envisioned by Peter Seldin, the most prominent spokesperson for teaching portfolios; however, the career portfolio would go beyond documentation of teaching experience to encompass teaching research, and service. One might raise the point that when students begin the job hunt, they in fact do their professional bookkeeping and manage to put together CVs, transcripts, and letters of recommendation. Area astutely points out, though, that &#147;to wait to instruct TAs about how to develop a professional profile until they are putting together their vitae and dossiers and searching for jobs is simply too late. Instead, &#145;TA training&#146; needs to be redefined as the development of professionals&#148; (46). 

A career portfolio begins with the first year of graduate study and differs significantly from the small mound of paper produced hurriedly by job applicants each year between the second week of October and the first week of November; the career portfolio goes well beyond the three standard parts of the job application (CV, transcript, and letters of recommendation). Following many of Seldin&#039;s ideas, I suggest four main components for the career portfolio. 

<em>Component 1: Introduction. </em>This portion includes reflective statements regarding teaching and research&#151;why the student has chosen to enter graduate school or the academy and what the goals are for that endeavor. This section of the career portfolio would also be home to the standard personal facts, such as a curriculum vitae and a transcript. 

<em>Component 2: Teaching. </em>Here, products of the student&#039;s teaching should be summarized, highlighted, or represented: 
<ol type="1">
<li>Course syllabi that the student has developed or taught through and that describe the target audience, the course goals, and the methodology </li>
<li>Abstracts of relevant seminar papers, articles, or conference presentations </li>
<li>Descriptions, evaluations, letters, or certificate documenting relevant experience outside the standard TA context </li>
<li>Departmental teaching evaluations and perhaps official summaries of student evaluations </li>
<li>Sample of teaching materials developed </li>
</ol>

<em>Component 3: Research. </em>This portion of the career portfolio would represent the breadth and depth of research undertaken. For our graduate students, it would involve summaries of master&#039;s theses, of seminar papers, of conference presentations or articles, and, of course, of the dissertation, along with outlines showing how this research could be developed into further research projects, a graduate course, an undergraduate course, or a colloquium lecture for the nonspecialist. 

<em>Component 4: Service. </em>This component would include letters recognizing service activities as well as summaries of committee work and of other service to the department, university, and profession. 

All too often, the service part of life as an academic is ignored in graduate training. At an institution like Brown, which values teaching and research (supposedly) equally, the published equation for faculty assessment is 45-45-10, that is 45% research, 45% teaching, and 10% service. At institutions that place somewhat less emphasis on research, service can have a much more significant role in reappointments and tenure decisions. In fairness to our prot&#233;g&#233;s, we ought to give them a foretaste of what life in the academy is like, allow them to become involved as integral (not token) members on department committees, and encourage them to seek out other means of service related to their discipline. As part of this endeavor, we should document this service and value it. 

Having a career portfolio that has been developed over the span of graduate study could, of course, prove invaluable in the job market; however, the process of creating this record also has important advantages. First, encouraging graduate students to compile a career portfolio will prompt departments to take seriously the professional development of their students in a more well-rounded and realistic way. Second, putting together a portfolio motivates students to practice, to be prot&#233;g&#233;s with mentors in all three aspects of professional development, thereby gaining the opportunity to learn what the academy is like as a career choice. Third, students and their mentors will be better able to monitor students&#039; progress in all three areas and isolate areas that are weak. 

To students who might hesitate to embrace more responsibilities, I would argue that this is what career academics do. We are constantly balancing teaching, research, and service, and we need to document our accomplishments for purposes of review. When we conduct research, we report on it and then often adapt it for courses at different levels and for purposes other than specialist seminars. And the student would benefit, early on, from writing a reflective statement&#151;why he or she has chosen to enter the academy and what the goals are for that endeavor. If the statement doesn&#039;t offer some focus along the way, it may at least, several years down the road, provide a good laugh. 

To departments who may balk at the extra paperwork or the extra time needed to mentor their students who are developing career portfolios, I would argue that a portion of the task already gets done, just in a less systematic way. Also, the availability of a portfolio on each student would likely be useful in student review and advising. And, finally, the other advantages are worth the cost. We can show graduate students that we care about more than increasing enrollments; we want them to have an apprenticeship experience (to understand what a career in academia is like) and to be able to make an informed decision about which activities may prove most worthwhile and about whether to go into academia at all. 

There are other, more progressive ideas for making department power structures more inclusive of graduate students and more mentoring to our prot&#233;g&#233;s, but the introduction of career portfolios represents a modest, do-able proposal. In fact, most departments could start with next year&#039;s incoming class. 

<h2>A Not-So-Modest Proposal: Project Gen&#233;xus</h2>
The main goal of Project Gen&#233;xus (being both a &#147;beginning&#148; and &#147;a coming together&#148; of many things) is to provide critical information and mentoring to new faculty members. This proposal is based, primarily, on Project NExT, sponsored by the Mathematical Association of America (MAA) and funded in part by the Exxon Education Foundation. <sup>2 </sup>Each year since 1993&#173;94, Project NExT (New Experiences in Teaching) brings together recent PhDs just before or early on their first faculty positions, to enable them to discuss among peers their common interests. James Leitzel, one of the cofounders of the project, desired to me, in personal correspondence, the goals of Project NExT: to help new members of the academy in their transitions from graduate student to professor by conferencing on teaching and other professional interest, by introducing them to experienced professor-mentors, and by building a close-knit support community via three conference meetings and a private listserv on the Net (see also Marshall and Marshall). 

New faculty members interested in participating must be nominated by their employing institutions, which must also be willing to provide matching funds to the program. Over the three-year span of the project, it has reached 214 faculty members just beginning their careers (66 in 1993&#173;94; 78 in 1994&#173;95; 70 in 1995&#173;96), who represent an array of different institutions and geographic regions. The initial meeting takes place the summer before the participants&#039; first (or second) fall semester and is held in conjunction with the annual Mathfest. The Project NExT summer workshop orients incoming faculty members to &#147;new jobs, new responsibilities, new ideas,&#148; including an introduction to innovations in teaching, professional organizations, and journals and publishing. The second conference is concurrent with the joint meetings of the MAA, AMS (American Mathematical Society), and SIAM (Society for Industrial and Applied Mathematics), held shortly after Christmas; this joint meeting in mathematics parallels the MLA conference for language and literary studies. The final meeting for project NExT fellows (as participants are called) takes place the following summer, when they convene the follow up on each other&#039;s year and to provide valuable information for the next, incoming group of Project NExT fellows (see Mathematical Association of America; &#147;Project NExT: The Second Year&#148;; &#147;Project NExT: The Third Year&#148;). 

In addition to fellows, the project involves more-experienced members of the mathematical community who present at the workshops and serve as a general resource for responding to the many questions the fellows pose. According to Leitzel, &#147;These more seasoned members of the community have found the experience to be interesting, exciting, and wish to maintain contact with this energetic group of young professionals.&#148; He further states, &#147;By far, the most positive responses have been from the building of community among these young professionals and that they have reason now to attend and be active at national meetings.&#148; 

The project funds the summer workshop, special programming at the winter national meetings, and a reunion experience in the second summer. In additional, each fellow receives a collection of books from the MAA related to the issues discussed in the program. The estimated cost to Project NExT is $1,100 per participant, including administrative costs. This figure does not include the matching funds provided by each fellow&#039;s department. 

The high positive feedback that Leitzel has received from Project NExT participants underscores the significant impact the project has had. For example, Bernadette writes, &#147;A less lonely professional world! We Fellows are all facing similar concerns not shared by my senior colleagues here. It was great to meet so many people who were <font>WILLING </font>to talk to each other&#148; (Leitzel). Jenny reports, &#147;There was indeed a wonderful array of teaching innovations at our fingertips. It was helpful that several of the presenters encouraged us to &#145;start small&#146; and stay focused on implementing one new idea at a time. Seattle made me feel much better connected to our profession.&#148; Lester comments, &#147;Like many Project NExT Fellows, I had an academic training that consisted of mathematical research in a research institution. Consequently, I was all ears aperk at being exposed to the mathematical life at all kinds of institutions.&#133; My view of our profession has certainly widened.&#148; And Judy points out, &#147;Meeting other mathematicians is a crucial, yet sometimes daunting, part of going to conferences. Project NExT helped to make this much easier&#148; (&#147;Project NExT: The Third Year&#148;). 

I propose that MLA and the ADFL look into developing a Project Gen&#233;xus built on the Project NExT model. The following steps represent a possible course of action for this proposal-in-process. 

<em>Step 1: Form a steering committee </em>. A committee of seasoned professionals, beginning faculty members, and graduate students should be formed, to determine the financial needs of such a project, define the goals, write the funding grants, and choose an administrative committee. 

<em>Step 2: Promote Project Gen&#233;xus and elicit participation </em>. Fellows and mentors would be chosen from a pool of applicants or nominees. Fellows would be invited to two summer workshops and a special miniconference during or overlapping with the MLA Annual Convention. A summer workshop experience could include many components; some particularly helpful topics would be 
<ul type="DISC">
<li>Implicit and explicit criteria for promotion </li>
<li>Conferences in the field: their focus and reputations </li>
<li>Getting published: where and how </li>
<li>Getting grants: where and how </li>
<li>Reform and innovations in teaching </li>
<li>Our professional and personal lives: separation, balance, or integration? </li>
<li>FL teaching in the twenty-first century </li>
<li>From the front line: a passing of the torch </li>
</ul>

<em>Step 3: Establish a private </em>Gen&#233;xus <em>listserv </em>. A private Gen&#233;xus listserv should be established to promote and maintain dialogue and community among each year&#039;s fellows and mentors. 

The key advantages of Project Gen&#233;xus would mirror those of Project NExT. Goals for the project would include the following: 
<ol type="1">
<li>Promoting a community spirit and support among new faculty members in FLs </li>
<li>Providing new faculty members with valuable connections to other institutions, peers, and senior colleagues in FLs </li>
<li>Providing much-needed information about the profession that is difficult to find elsewhere </li>
<li>Better equipping and preparing new faculty members to be valuable, productive members of the academy </li>
</ol>

Adopting by the MLA and the ADFL of a Project Gen&#233;xus and the use of career portfolios in departments nationwide would provide a more effective training and mentoring system for at least some of our apprentices and junior colleagues. But more than what either of these proposals could do for any individual are the effects such action could have for our profession as a whole. These two proposals could become key means to address Robert Holub&#039;s poignant question: &#147;What are our ethical commitments to the graduate students we train?&#148; (82). The ideal, if also simplistic, answer seems not so difficult: fairness and kindness. When students plunge into the waters of academia, rather than leave them to sink or swim, we should show them how to stay afloat, teach them the strokes to negotiate safely around the lagoon, and allow them, if they wish, to return to the beach with honor. 


<em>The author is a lecturer in the Department of German Studies at Brown University. This article is based on her presentation at the 1996 MLA convention in Washington, DC. </em> 

<h2>Notes </h2>
<sup>1 </sup>In the several years before my dissertation defense, only one student directly associated with the German department had completed a dissertation in applied linguistics, and that student was coenrolled in the departments of German and of curriculum and instruction. 

<sup>2 </sup>I first heard of Project NExT through my husband, Randall J. Pruim, who teaches and researches in mathematics. After locating the Project NExT Web site (http://archives.math.utk.edu/projnext/), I initiated contact with contact with one of the cofounders of the project, James R.C. Leitzel. He deserves special thanks for graciously providing invaluable information about Project NExT for this article, including the original grant proposals, annual reports, and participant demographics and comments. 

<h2>Works Consulted </h2>
Arens, Katherine. &#147;Applied Scholarship in Foreign Languages: A program of Study in Professional Development.&#148; Benseler 33&#173;63. 

Belat&#232;che, Lydia. &#147;Temp Prof: Practicing the Profession off the Tenure Track.&#148; <cite>Profession 94. </cite>New York: MLA, 1994. 66&#173;66. 

Benseler, David P., ed. <cite>The Dynamics of Language Program Direction </cite>. Boston: Heinle, 1993. 

Caminero-Santangelo, Marta. &#147;The Ethics of Hiring.&#148; <cite>Profession 94. </cite>New York: MLA, 1994. 62&#173;63. 

Curren, Erik D. &#147;No Openings at This Time: Job Market Collapse and Graduate Education.&#148; <cite>Profession 94. </cite>New York: MLA, 1994. 57&#173;61. 

Gorell, Lynn Carbon, and Jorge Cubillos. &#147;TA Program: The Fit between Foreign Language Teacher Preparation and Institutional Needs.&#148; Benseler 91&#173;109. 

Holub, Robert C. &#147;Professional Responsibility: On Graduate Education and Hiring Practices.&#148; <cite>Profession 94. </cite>New York: MLA, 1994. 79&#173;86. 

Huber, Bettina. &#147;Recent Trends in the Modern Language Job Market.&#148; <cite>Profession 94. </cite>New York: MLA, 1994. 87&#173;105. 

Hutner, Gordon. &#147;What We Talk about When We Talk about Hiring.&#148; <cite>Profession 94. </cite>New York: MLA, 1994. 75&#173;78. 

Leitzel, James R. C. Personal correspondence. 

Lemiesz, Linda M. &#147;And the Lost Shall Be Found.&#148; <cite>Profession 94. </cite>New York: MLA, 1994. 67&#173;69. 

Marshall, James P., and Mary K. Marshall. &#147;The NExT-Lists Creating Math Communities via E-mail.&#148; <cite>MER Newsletter </cite>Spring 1996. 4&#173;5. 

Mathematical Association of America. &#147;Project NExT: New Experiences in Teaching: Second-Year Report to the Exxon Education Foundation.&#148; 1995. 

Pascoe, Judith. &#147;What to Expect When You&#039;re Expecting.&#148; <cite>Profession 94. </cite>New York: MLA, 1994. 70&#173;74. 

Pons, Cathy R. &#147;TA Supervision: Are We Preparing a Future Professoriate?&#148; Benseler 9&#173;32. 

&#147;Project NExT: The Second Year.&#148; <cite>Focus </cite>15.6 (1995): 5+. 

&#147;Project NExT: The Third Year.&#148; <cite>Focus </cite>16.6 (1996): 14&#173;15. 

Roche, J&#246;rg. &#147;Professionalizing Second-Language Teaching.&#148; <cite>ADFL Bulletin </cite>27.1 (1996): 47&#173;53. 

Rodriguez-Farrar, Hannelore. <cite>Teaching Portfolio Handbook </cite>. Brown Univ: Center for the Advancement of Coll. Teaching, 1995. 

Seldin, Peter. <cite>The Teaching Portfolio </cite>. Boston: Anker, 1991. 


&#169; 1998 by the Association of Departments of Foreign Languages. All Rights Reserved.
