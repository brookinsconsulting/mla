<h1>The New Variorum Shakespeare Digital Challenge: The Second Round</h1>

<p>The MLA Committee on the New Variorum Edition of Shakespeare (NVS) is sponsoring its second digital challenge to find the most innovative and compelling uses of the data contained in one of the NVS editions. This year the MLA is making available the XML files and schema for two volumes, <em>The Winter’s Tale</em> and <em>The Comedy of Errors</em>, under a <a href="http://creativecommons.org/licenses/by-nc/3.0/">Creative Commons BY-NC 3.0 license</a>.</p>

<p>Scholars can freely download the XML files and schema from GitHub: <a href="https://github.com/mlaa/nvs-challenge">https://github.com/mlaa/nvs-challenge</a>.

<p>The committee seeks entries featuring new means of displaying, representing, and exploring this data in the most exciting API, interface, visualization, or data-mining project. It is especially interested in entries that combine the NVS data with another Shakespearian project, such as <em>Folger Digital Texts</em>, <em>Internet Shakespeare Editions</em>, or <em>Open Source Shakespeare</em>. The goal is to see the possibilities of the NVS in digital form and, in particular, the innovations in scholarly research that might be enabled by opening up the NVS’s code. Projects will thus be judged both on the quality of the interface they provide for the NVS and on the insights produced by the mash-up.</p>

<p>The deadline for entries is 1 August 2014. The committee will assess the submissions and select the winner no later than 15 September 2014. The prize of $500 and an award certificate will be given at the 2015 MLA convention in Vancouver.</p>

<p>Entries may be sent to <a href="mailto:nvs@mla.org">nvs@mla.org</a>.  Questions about the NVS Digital Challenge should be addressed to Kathleen Fitzpatrick, director of scholarly communication, at <a href="mailto:kfitzpatrick@mla.org">kfitzpatrick@mla.org</a>.</p>

<p>For more information about our partner projects, please contact Michael Best (<a href="mailto:mbest1@uvic.ca">mbest1@uvic.ca</a>), of <em>Internet Shakespeare Editions</em>, or Eric Johnson (<a href="mailto:eric.johnson@folger.edu">eric.johnson@folger.edu</a>), of <em>Open Source Shakespeare</em> and <em>Folger Digital Texts</em>.</p>

<p>Please note that our partners are available to answer questions about the resources, not to provide technical support.</p>

<h2>The First Round</h2>

<p>In the 2012 digital challenge, the MLA released the XML files and schema for <em>The Comedy of Errors</em> under a <a href="http://creativecommons.org/licenses/by-nc/3.0/">Creative Commons BY-NC 3.0 license</a>. The winner of the challenge was Patrick Murray-John’s <i>Bill-Crit-O-Matic</i> (<a href="http://billcritomatic.org" target="_blank">http://billcritomatic.org</a>). The runner-up was Doug Reside and Gregory Lord’s <i>Comedy of Errors</i> (<a href="http://comedyoferrors.zengrove.com/" target="_blank">http://comedyoferrors.zengrove.com</a>).</p>

<h2>A Note on Data Design and TEI Conformance</h2>

<p>The MLA is making a preliminary version of the source XML data for its NVS editions publicly available for research and experimentation. Some explanation of the motives guiding the design of the data and of the data’s status as an intellectual product may be helpful to those who wish to use the data.</p>

<p>First, the version of the data currently being shared is designed primarily for internal use as part of the production process for the NVS print volumes. It is offered as is, for experimentation. It is not intended as a model of good practice in the use of TEI. The current version of the data is not in the TEI namespace but in an NVS namespace. This is a practical measure to simplify certain aspects of the production process. A future release of the data, intended for public use, will use the TEI namespace (with NVS-specific elements in the NVS namespace).</p>

<p>A second point of relevance for potential users is the question of TEI conformance. As those familiar with the TEI will know, TEI conformance is a strictly defined concept; conformant data must be valid against a strict subset of the TEI schema, featuring no new elements or structural changes. The TEI also defines two other categories of data: TEI-conformable data (which can be converted to TEI conformance automatically without information loss) and TEI extensions (which use the TEI but make more substantial structural changes and additions to the tagset). (For full details, please see chapter 23 of the TEI guidelines: <a href="http://www.tei-c.org/release/doc/tei-p5-doc/en/html/USE.html">http://www.tei-c.org/release/doc/tei-p5-doc/en/html/USE.html</a>.) For the NVS, TEI conformance is not possible because of the substantial number of structural adaptations necessary to accommodate NVS-specific features. Some of these may be of value to other edition projects and will be shared in the future with the TEI community as feature requests, but others are simply artifacts of the NVS editions and production process and are not candidates for inclusion in the TEI proper. As a result, the NVS data set taken as a whole can be expressed as a TEI extension but not as TEI conformant or TEI conformable, since it cannot be converted to a conformant format without loss of information.</p>

<p>However, it is important to note that the data can be converted to TEI conformance by eliminating NVS-specific features through a fairly straightforward process using a tool like XSLT. The accompanying documentation describes the function of all NVS-specific elements, which may be helpful in deciding on the closest TEI equivalent (or in determining whether the element and its content could be jettisoned altogether).</p>

<p>Finally, the MLA may in the future create versions of the data that are specifically intended to support an electronic NVS edition, and those versions may better lend themselves to expression as TEI conformant or conformable data (since they will not have to support the print-specific features of the edition).</p>
