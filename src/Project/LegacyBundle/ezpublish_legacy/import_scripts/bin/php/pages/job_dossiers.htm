<h1>CVs, Dossiers, Application Letters, Writing Samples, and Portfolios</h1>
<blockquote><ol>
<li><a href="#when">When and how should I start work on my CV and other job materials?</a>
<li><a href="#cv">What should be in my CV?</a>
<li><a href="#cvorg">How should my CV be organized?</a>
<li><a href="#letters">What should I consider in assembling letters of recommendation?</a>
<li><a href="#application">How do I write an effective application letter?</a>
<li><a href="#sample">How should I choose a writing sample?</a>
<li><a href="#portfolio">What should go into a teaching portfolio, and when should I make it available?</a>
</ol></blockquote><a name="when"> </a>
<h2>When and how should I start work on my CV and other job materials?</h2>
A career portfolio begins with the first year of graduate study and differs significantly from the small mound of paper produced hurriedly by job applicants each year between the second week of October and the first week of November; the career portfolio goes well beyond the three standard parts of the job application (CV, transcript, and letters of recommendation). Following many of Peter Seldin's ideas (<I>The Teaching Portfolio</I>, Anker, 1991), I suggest four main components for the career portfolio.

<em>Component 1: Introduction.</em> This portion includes reflective statements regarding teaching and research-why the student has chosen to enter graduate school or the academy and what the goals are for that endeavor. This section of the career portfolio would also be home to the standard personal facts, such as a curriculum vitae and a transcript.

<em>Component 2: Teaching.</em> Here, products of the student's teaching should be summarized, highlighted, or represented:

1. Course syllabi that the student has developed or taught through and that describe the target audience, the course goals, and the methodology

2. Abstracts of relevant seminar papers, articles, or conference presentations

3. Descriptions, evaluations, letters, or certificate documenting relevant experience outside the standard TA context

4. Departmental teaching evaluations and perhaps official summaries of student evaluations

5. Sample of teaching materials developed

<em>Component 3: Research.</em> This portion of the career portfolio would represent the breadth and depth of research undertaken. For our graduate students, it would involve summaries of master's theses, of seminar papers, of conference presentations or articles, and, of course, of the dissertation, along with outlines showing how this research could be developed into further research projects, a graduate course, an undergraduate course, or a colloquium lecture for the nonspecialist.

<em>Component 4: Service.</em> This component would include letters recognizing service activities as well as summaries of committee work and of other service to the department, university, and profession.

"<a href="javascript:popup('/bulletin_293034','docs_popup');">Our Ethical Commitments to the Graduate Students We Train: A Modest and a Not-So-Modest Proposal</A>"
<blockquote>
Pennylyn Dykstra-Pruim
Department of German Studies
Brown University
</blockquote>

<a name="cv"> </a>
<h2>What should be in my CV?</h2>
In language teaching, be aware that many existing positions are for the teaching of culture and business-related courses. Include on your CV any job-related experience that would attest to your expertise in business-related courses. Don't be reluctant to include experience that is not specifically related to language or literature research and teaching. Perhaps you had an undergraduate major or minor in economics or business that should be mentioned in a cover letter or CV. Highlight any job experience in business that could transfer to the classroom.
<blockquote>
Emily Spinelli
Department of Humanities
University of Michigan, Dearborn
</blockquote>

Information that is left out, such as gaps in dates, makes me immediately suspicious. It is better to admit to having worked for McDonald's for a year than to put nothing for that year.
<blockquote>
Margit Resch
Department of Germanic, Slavic, and East Asian Languages
University of South Carolina, Columbia
</blockquote>

Annoying for an overworked search committee was the failure to provide useful references. Some applicants did not include phone numbers for references in either the letter or the r&eacute;sum&eacute;. Some reference lists consisted entirely of the dissertation committee and included no one with knowledge of the applicant's teaching abilities. Worst of all were statements that the names of references would be furnished on request.

"<a href="javascript:popup('/bulletin_113050','docs_popup');">The Job Search: Observations of a Reader of 177 Letters of Application</A>"
<blockquote>
Eleanor H. Green
Department of English
Ohio Northern University
</blockquote>
<a name="cvorg"> </a>
<h2>How should my CV be organized?</h2>
Do not go to a professional r&eacute;sum&eacute; writer, somebody who advertises to help with r&eacute;sum&eacute;s and who knows all about corporate r&eacute;sum&eacute;s but nothing about the kind that we need in academia. Business-style r&eacute;sum&eacute;s, limited to one or two pages, are not long enough for an academic CV.
<blockquote>
Malcolm Compitello
Department of Spanish and Portuguese
University of Arizona
</blockquote>
<a name="letters"> </a>
<h2>What should I consider in assembling letters of recommendation?</h2>
Most recommendation letters have a conventional beginning (introducing the candidate and the writer) and ending (an affirmation of the candidate's credentials and a promise to offer yet more information about the candidate if its readers so desire). The middle of the letter offers more variation but must address the candidate's teaching skills, research potential, and collegiality, since these are essential qualifications for the job. If it cannot discuss all of these, it acknowledges and explains the lapse of protocol. By adhering to these unwritten rules, the letter writer establishes his or her credentials as a person who understands the needs and expectations of the hiring department (and who can therefore be trusted to judge a candidate's suitability). Yet some originality is necessary to convey what is distinctive about the candidate. A letter that sounds just like every letter in the pile is a certain failure.

The letter writer should begin by requesting that candidates provide him or her with detailed information about their job search, including a draft cover letter, a vita, a teaching portfolio, and a writing sample; then the writer should schedule a time to talk about the jobs they are applying for. This enables the writer to provide some extra feedback and often allows him or her to discover new information that can be useful in the letter. Although it is common for faculty members to complain about inflated recommendations, packed full of empty superlatives and clever phrases, such letters are easy to write, easy to spot, and just as easily discounted. But it is hard to ignore truly good writing, in large part because it signals the writer's sincere commitment to the candidate.

Some circumstances may call for short (one- or two-paragraph) letters. Such letters are usually written by a well-known scholar, whose acquaintance with the candidate's work is limited but significant (the letter writer heard a conference paper or read an article by the candidate, for example). The reason for the letter's brevity should be made clear, so that the length is not taken as an indication of weak support. In most circumstances, a recommendation letter should be one and a half to two pages long: long enough to present the candidate's qualifications in detail, but short enough for the committee to read fairly quickly.

"<a href="javascript:popup('/bulletin_125044','docs_popup');">Writing Letters of Recommendation for Academic Jobs</A>"
<blockquote>
Maura Ives
Department of English
Texas A&M University, College Station
</blockquote>

<a name="application"> </a>
<h2>How do I write an effective application letter?</h2>
Your application letter should make some attempt to show <I>how</I> you meet the <I>particular</I> qualifications of the job posting. Boilerplate letters that launch into a long description of the candidate's dissertation followed by philosophy of teaching are a real turn-off unless the writing is crisp, witty, or shows a dazzling display of authentic voice. My pet peeve is applicants who spell the search committee chair's name incorrectly or don't get the name of the institution right. CVs and generic letters of application with typos or grammatical miscues get thrown out by search committees immediately.
<blockquote>
Jane Frick
Department of English, Foreign Languages, and Journalism
Missouri Western State College
</blockquote>

Avoid "generic" letters.  Tailor each letter to fit the job description and the university and department to which you are applying.   Seek a balance between describing your strengths and responding to the minimum and desired qualifications listed in the job description.  Be judicious in how much you write about your dissertation, particularly if you are not applying to an institution that places great importance upon research.   Tone and form (spelling, punctuation, margins, etc.) are important.  Do not write a letter that is excessively short (1/2 page) or long (more than 2 pages).  Ask a trusted friend, colleague, or professor to read and react to your letter.
<blockquote>
Joseph Chrzanowski
Department of Modern Languages and Literature
California State University, Los Angeles
</blockquote>

One of the persistent problems I've dealt with in job counseling is the one-size-fits-all cover letter where only the name of the university and a few small details are altered for each application. These are often letters wherein the applicant comes across not as an individual with genuine interests and enthusiasms (whether in scholarship or teaching or in both) but rather as the product of an outstanding PhD program and the author of a dissertation. My advice in such cases has been to drastically reduce the attention given to the dissertation in the letter of application and concentrate instead on conveying the candidate's vision of him- or herself as a future citizen of the profession given the intellectual, academic, and personal background he or she can bring to a department and an institution. In this statement of professional outlook and goals, keep in mind the type of institution or department to which you are applying. Only characterize the dissertation and its importance in a succinct paragraph in the letter of application, leaving the highly detailed coverage of the dissertation to an abstract that can be attached to the CV.
<blockquote>
Reed Anderson
Department of Spanish and Portuguese
Miami University
</blockquote>

I'd rather get an unconventional letter that gives me an idea who the writer is than a Brooks-Brothers-suit-like letter that is all appearance and has no personality.
<blockquote>
Margit Resch
Department of Germanic, Slavic, and East Asian Languages
University of South Carolina, Columbia
</blockquote>

Try to sell yourself as an outstanding teacher.  Consider that many colleges now value teaching more than they did in the past and would be interested in candidates who describe their commitment to teaching in letters of application.  Don't be reticent about your innovative and experimental pedagogies.  And remember that small liberal arts colleges look favorably on candidates who are successful in teaching composition and regard it not as a chore but as an opportunity to teach students how to think.

Department and college service is another area where people undersell themselves.  Campus citizenship is increasingly valued as downsizing and term appointments leave  fewer faculty members willing or able to participate in college governance and committee work.
<blockquote>
Margaret Schramm
Department of English and Theater Arts
Hartwick College
</blockquote>

When I advise students laboring to connect their research interests, teaching experience, and service in letters of application, most initially produce tortuous, jargon-ridden, naively pretentious dissertation descriptions. Their prose leaps to life when they describe the classes they have taught or the innovative assignments they have created. Whatever our intentions, the lesson students often seem to absorb from graduate study is that what they perceive as scholarly (rather than pedagogical) activity is best communicated in prose likely to strike search committee members as elitist and overbearing.

"<a href="javascript:popup('/bulletin_114019','docs_popup');">Identity and Economics; or, The Job Placement Procedural</A>"
<blockquote>
Teresa Mangum
Department of English
University of Iowa
</blockquote>

When they are ready to apply for positions in small colleges, candidates should remember that the standard vita is not as important as the cover letter. By the end of December every vita begins to look like the other fifty or one hundred vitae in the files; by that time every form letter of application is also more than familiar to chairs. Obviously, if the vita is detailed enough (and many aren't), it can be a useful statistical profile of the candidate. However, I suspect the cover letter can have greater impact. Consequently, before candidates apply at a given college, they probably should familiarize themselves with that college and its curriculum. The college's catalogue would be the obvious place to begin. And then when they do sent a letter of application, it should reflect their familiarity with the college and perhaps include a brief statement as to why they want to teach there and what contribution they might make. In the light of the candidate's familiarity with the college's curriculum, the letter might emphasize his or her capacities as a generalist, as well as his or her competence as a specialist, with perhaps greater consideration given to the former. And rather than draw attention again to the title of the dissertation, which, nine times out of ten, means nothing to chairs unless it happens to be in their own area of specialization, the letter might also include some statement as to what courses he or she would be interested (and again, not expected) to teach.

"<a href="javascript:popup('/bulletin_027048','docs_popup');">Advice to Candidates for Positions in Small Colleges</A>"
<blockquote>
A. Poulin, Jr.
Department of Humanities
Saint Francis College
</blockquote>

One doesn't gain an interview--or often even a dossier request--without a good letter or r&eacute;sum&eacute;. The application letter is, in a sense, the first in a series of interviews. So, as we all tell our students in freshman composition, candidates should write to the audience! While composing two or three dozen highly individual letters of application is probably unnecessary, the most successful applicants will be able to demonstrate that they best match a particular institution's needs. Sometimes that simply means rearranging the balance of discussion about research and teaching to fit various institutions' priorities. Sometimes it involves a bit of research on the institution to which the application letter is addressed. The rewards will be worth the effort. Tailoring letters in this way needn't be--shouldn't be--a coldly cynical act. Although the job market is undeniably difficult and the prospect of unemployment is terrifying, a far worse fate in the long run, for both institution and employee, is an untenable match. It's far better not to apply if either the position or the institution clearly isn't appropriate to one's needs and qualifications.

"<a href="javascript:popup('/bulletin_111014','docs_popup');">What Is a Comprehensive University, and Do I Want to Work There?</A>"
<blockquote>
Marcia Dalbey
Department of English
Eastern Michigan University
</blockquote>

Before sending out any materials, the candidate should research each position on the target list as fully as possible, focusing on the programs and courses the relevant department offers and on the faculty in that department. Matching the job description and the catalogue may provide information on who's being replaced and on which courses are likely to be the responsibility of the position. The candidate should ask each of his or her professors for additional information about the institution and its personnel. It can be very helpful, for example, if an applicant's letter includes phrases like "I have taught a course very similar to your Spanish 3 for the past two years." The point is to make sure that those doing the hiring at the target institution realize that this is not merely one of dozens of letters sent out willy-nilly, but comes from a candidate who is particularly knowledgeable about and interested in <EM>this </EM>institution.

"<a href="javascript:popup('/bulletin_091022','docs_popup');">Marketing and Matchmaking: How Departments Can Help Students Find Jobs</A>"
<blockquote>
David T. Haberly
Department of Spanish, Italian, and Portuguese
University of Virginia
</blockquote>

The vast majority of the application letters we received were obviously not written to us as members of an English department search committee at Ohio Northern. They were in fact generic form letters written with the apparent aim of securing a position at an institution far different from anything Ohio Northern is or ever will become. Most did not do justice at all to the talents, training, or experiences of the applicants. Most letters emphasized research far more than any other subject. A few candidates even stated their eagerness to teach graduate courses, although a quick look at any college guide would show that Ohio Northern, like most small universities, has no graduate students in English. Many applicants listed all the courses they had ever taught but made no mention of their teaching philosophy or pedagogical techniques.

Of the 58 candidates (approximately one-third of the total) who did have small-school background, few pointed out that they were familiar with an environment like Ohio Northern's, and even fewer expressed a desire to teach in such a setting. Only 34 letters even mentioned the name of our institution. And although 13 candidates did refer to the phrase "student-centered" from our announcement, it was clear that almost none of the candidates had bothered to look up Ohio Northern in a college guide, much less to read our catalog, and those who had taken the trouble to do so were not able to use the information effectively.

Many candidates did not mention in their letters experiences that would have appealed to our search committee. Only 46 pointed out any professional or service work that would have indicated involvement in activities valuable to students or to the institution. At least 58 more of the candidates clearly had this type of experience but tucked it away in their r&eacute;sum&eacute;s while including paragraph after paragraph in their letters about different sections of their dissertations.

Indeed, applicants whose r&eacute;sum&eacute;s suggested diverse interests and backgrounds came across as narrow and dull in their letters.

A somewhat common error was the extreme brevity of many of the letters, apparently the result of a mistaken belief that the letter should be only one page long. While three or four pages is certainly excessive, very few letters of less than one and a half to two pages gave the committee an accurate sense of the applicant. And a small minority of the letters were inadequate even in format and appearance. Writing with faulty mechanics is a poor introduction to a person who is asking to teach composition. However, we received 13 letters with errors such as lack of an inside address, incorrect punctuation of the salutation, double-spaced typing, and omission of the date.

"<a href="javascript:popup('/bulletin_113050','docs_popup');">The Job Search: Observations of a Reader of 177 Letters of Application</A>"
<blockquote>
Eleanor H. Green
Department of English
Ohio Northern University
</blockquote>

Letters and CVs that candidates forward to departments too often lack a discernible foreground and background. They provide, that is, a list, not a story. Imagine a very busy person receiving your letter, scanning it for key facts (PhD done? area of specialty? employment history? publications? recommenders?) and routing it to a committee or putting it to one side.

My father&mdash;who (to his children's endless boredom) loved to recount his success in getting a job during the depression&mdash;always used to say, "A job is something someone else wants done." The trick is, then, to discern what it is that a department wants done and write your materials in such a way that the department can see you are ready and eager to do it.
<blockquote>
Adalaide Morris
Department of English
University of Iowa
</blockquote>
<a name="sample"> </a>
<h2>How should I choose a writing sample?</h2>
If a writing sample is requested, it is not enough to pull the introduction or a chapter from the dissertation; you must reshape the sample to give it its own integrity while it still alludes to the larger arguments outside it. Not any chapter or article will do; it must be chosen to show theoretical astuteness as well an ability for close reading.
<blockquote>
Dianne Sadoff
Department of English
University of Miami
</blockquote>

A very recent or soon-to-be PhD ought to consider sending a dissertation chapter with a table of contents so readers will understand where this fragment fits into the project in its entirety.  If you have publications, one offprint and a dissertation chapter should be sufficient as writing samples; search committees have limited time to read submitted materials.  When choosing a writing sample, make sure it is relevant to the position for which you are applying.  If the department is looking for a medievalist, don't send a piece on contemporary fiction simply because you've written or published one. Departments want to see how you negotiate the texts and critical and theoretical polemics of the particular field for which they are hiring.  This is especially important for ABDs; sending an essay unrelated to your field of specialization may raise suspicions that you have not made significant progress toward completion of the dissertation.
<blockquote>
Hazel Gold
Department of Spanish and Portuguese
Emory University
</blockquote>
<a name="portfolio"> </a>
<h2>What should go into a teaching portfolio, and when should I make it available?</h2>
How can you show your interest, experience, and expertise in teaching? Do your homework thoroughly. For the positions you are extremely interested in, provide sample course syllabi for the types of courses you would be teaching at that institution--the courses listed in the job description and ad, for example. Bring these sample course syllabi to the interview. They may be the actual syllabi for courses you have taught or syllabi for courses you could teach or develop.
<blockquote>
Emily Spinelli
Department of Humanities
University of Michigan, Dearborn
</blockquote>

Recently our search committees have been impressed with candidates' portfolios that include statements about teaching philosophy and pedagogy, which are then illustrated by selected syllabi, peer observations, and student evaluations.  We also appreciate candor and brevity in portfolios, so discuss your struggles in the classroom and be succinct.
<blockquote>
Margaret Schramm
Department of English and Theater Arts
Hartwick College
</blockquote>
