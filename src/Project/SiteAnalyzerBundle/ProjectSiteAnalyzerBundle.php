<?php

namespace Project\SiteAnalyzerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ProjectSiteAnalyzerBundle extends Bundle
{

    protected $name = "ProjectSiteAnalyzerBundle";

}
