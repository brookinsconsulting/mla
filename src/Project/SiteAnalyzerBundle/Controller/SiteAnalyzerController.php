<?php

namespace Project\SiteAnalyzerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class SiteAnalyzerController extends Controller
{

    public function adminAction( $location_id )
    {
        return $this->render(
            'ProjectSiteAnalyzerBundle::admin.html.twig', array( "location_id" => $location_id )
        );
    }

    public function displayAction( $location_id )
    {
        $eZPublishAPIRepository = $this->container->get( 'ezpublish.api.repository' );
        $LocationService = $eZPublishAPIRepository->getLocationService();

        /*
        $Variables = array(
            "location_id" => $location_id,
            "location" => $Location = $LocationService->loadLocation( $location_id ),
            // "children_count" => $LocationChildrenCount = $LocationService->getLocationChildCount( $Location ),
            // "has_children" => (bool) $LocationChildrenCount,
            "children" => $LocationChildren = $LocationService->loadLocationChildren( $Location ),
        );

        foreach( $LocationChildren->locations as $Key => $ChildLocation )
        {
            $Variables["children_data"][ $Key ] = array(
                "children_count" => $LocationChildrenCount = $LocationService->getLocationChildCount( $ChildLocation ),
                "has_children" => (bool) $LocationChildrenCount,
            );
        }
        */

        return $this->render(
            'ProjectSiteAnalyzerBundle::index.html.twig', array( "location" => $LocationService->loadLocation( $location_id ) )
        );
    }

    public function treeAction( $location_id )
    {

        /*
        $Content = array(
            "location_id" => $location_id,
            "location_tree" => $this->buildLocationTree( $location_id ),
        );
        */

        $Content = $this->processLocation(
            $this->container->get( 'ezpublish.api.repository' )->getLocationService()->loadLocation( $location_id )
        );

        $Content["children"] = $this->buildLocationTree( $location_id );

        return new JsonResponse($Content);
        
    }

    protected function buildLocationTree( $location_id )
    {
        $eZPublishAPIRepository = $this->container->get( 'ezpublish.api.repository' );
        $LocationService = $eZPublishAPIRepository->getLocationService();

        $Location = $LocationService->loadLocation( $location_id );
        $LocationChildren = $LocationService->loadLocationChildren( $Location );

        if( $LocationChildren->totalCount )
        {
            $LocationTree = array();

            foreach( $LocationChildren->locations as $Key => $ChildLocation )
            {
                if(
                    $ProcessedChildLocation = $this->processLocation( $ChildLocation )
                ){
                    $LocationTree[] = $ProcessedChildLocation;
                }
            }

            return $LocationTree;
        }
        return false;
    }

    protected function processLocation( $location )
    {
        $Router = $this->container->get( 'router' );
        $eZPublishAPIRepository = $this->container->get( 'ezpublish.api.repository' );
        $LocationService = $eZPublishAPIRepository->getLocationService();
        $ContentTypeService = $eZPublishAPIRepository->getContentTypeService();
        $SiteLink = $this->container->get( 'thinkcreative.sitelink' );

        $SiteLinkContext = $SiteLink->generate( $location );

        $ContentType = $ContentTypeService->loadContentType( $location->contentInfo->contentTypeId );

        if(
            $this->isValidContentType( $ContentType->identifier )
        ) {
            return array(
                "id" => $location->id,
                "name" => $location->contentInfo->name,
                "url" => $Router->generate( $location ),
                "canonical" => $SiteLink->buildHyperlink( $SiteLinkContext, true ),
                "depth" => $location->depth,
                "content" => array(
                    "id" => $location->contentInfo->id,
                    "content_type" => array(
                        "id" => $location->contentInfo->contentTypeId,
                        "identifier" => $ContentType->identifier,
                        "name" => $ContentType->getName( 'eng-US' ),
                    ),
                ),
                "hidden" => $location->hidden,
                "children_count" => $LocationChildrenCount = $LocationService->getLocationChildCount( $location ),
                "has_children" => (bool) $LocationChildrenCount,
            );
        }

        return false;
    }

    protected function isValidContentType( $content_type_identifier )
    {
        $ContentTypeList = array(
            "bulletin_article", "bulletin_issue", "event", "event_calendar", "faq_list", "folder", "imported_page", "imported_section", "landing_page", "link", "article", "product_category", "product", "section_home",
        );
        return in_array( $content_type_identifier, $ContentTypeList );
    }

}
