<?php

namespace Project\MemberBundle\Classes\DependencyInjection;
use ThinkCreative\BridgeBundle\Services\CustomTagsManager;
use ThinkCreative\BridgeBundle\DependencyInjection\CustomTagsHandlerInterface;

class MemberSearchCustomTag implements CustomTagsHandlerInterface
{

    /**
     * Register member custom tag
     * @param CustomTagsManager $customtags_manager
     */ 
    public function registerCustomTagList(CustomTagsManager $customtags_manager) {       

        // members search config
        $memberSearchConf = array(
            "attributes" => array(),
            "template" => "ProjectMemberBundle:customtags:member_search/search_form.html.twig",
            "controller" => "ProjectMemberBundle:MemberSearch:search"
        );

        // register product category customtag
        $customtags_manager->register(
            "member-search",
            $memberSearchConf
        );

    }

}
