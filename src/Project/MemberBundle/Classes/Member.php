<?php

namespace Project\MemberBundle\Classes;
use eZ\Publish\API\Repository\Values\User\User as EzUser;
use Project\ApiBundle\Endpoints\Member as MemberApi;

/**
 * Class that provides relevent membership information
 */
class Member
{

    /**
     * Constants representing various status of a member.
     * @var integer
     */
    const MEMBER_STATUS_JOIN = 1;
    const MEMBER_STATUS_FULL = 2;
    const MEMBER_STATUS_DEPARTMENT = 3;
    const MEMBER_STATUS_UNKNOWN = 98;
    const MEMBER_STATUS_EZUSER = 99;

    /**
     * API Member endpoint
     * @var MemberApi
     */
    protected $apiMember;

    /**
     * eZPublish User
     * @var EzUser
     */
    protected $ezUser;

    /**
     * Member access level (for departments)
     * @var integer
     */
    protected $accessLevel = null;

    public function __construct(
        MemberApi $apiMember = null,
        EzUser $ezUser = null,
        $accessLevel = null
    ) {
        $this->apiMember = $apiMember;
        $this->ezUser = $ezUser;
        $this->accessLevel = $accessLevel;
    }

    /**
     * Get API member id
     * @return integer|null
     */
    public function getMemberApiId()
    {
        if ($this->apiMember) {
            return $this->apiMember->get("id");
        }
        return null;
    }

    /**
     * Get eZ Publish user id
     * @return integer|null
     */
    public function getEzUserId()
    {
        if ($this->ezUser) {
            return $this->ezUser->contentInfo->id;
        }
        return null;
    }

    /**
     * Determine member's current status.
     * This is mostly to determine if member
     * is currently joining.
     * @return integer
     */
    public function getMemberStatus()
    {

        if ($this->apiMember) {
            if ($this->apiMember->getMemberSourceId() == MemberApi::MEMBER_SOURCE_DEPARTMENTS) {
                return self::MEMBER_STATUS_DEPARTMENT;
            } else if ($this->apiMember->getMemberSourceId() == MemberApi::MEMBER_SOURCE_MLA) {
                if ($this->apiMember->get("authentication[membership_status]", null, true) != "active" || !$this->apiMember->get("membership[class_code]", null, true)) {
                    return self::MEMBER_STATUS_JOIN;
                } else {
                    return self::MEMBER_STATUS_FULL;
                }
            }
            return self::MEMBER_STATUS_UNKNOWN;
        }

        // isn't an api member, just an ezuser
        return self::MEMBER_STATUS_EZUSER;
    }

    /**
     * Return eZ User object
     * @return EzUser|null
     */
    public function getEzUser()
    {
        return $this->ezUser;
    }

    /**
     * Returns ApiMember object which
     * can be used to update member API data
     * @return ApiMember|null
     */
    public function getApiData()
    {
        return $this->apiMember;
    }

    /**
     * Get member access level
     * @return integer
     */
    public function getAccessLevel()
    {
        return $this->accessLevel;
    }

}