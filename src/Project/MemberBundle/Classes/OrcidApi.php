<?php

namespace Project\MemberBundle\Classes;
use Project\MemberBundle\Exception\Orcid\OrcidCurlException;
use Project\MemberBundle\Exception\Orcid\OrcidApiResponseException;

/**
 * Class for communicating with the ORCID api
 */
class OrcidApi
{

    /**
     * URL to send user to do authenticate with ORCID
     * @var string
     */
    const AUTH_URL = "https://sandbox.orcid.org";

    /**
     * URL to submit API request to
     * @var string
     */
    const API_URL = "https://api.sandbox.orcid.org";

    /**
     * ORCID API client id
     * @var string
     */
    const CLIENT_ID = "0000-0001-8771-3908";

    /**
     * ORCID API client secret
     * @var string
     */
    const CLIENT_SECRET = "80a39660-c7f5-4a02-9e18-242608361c7b";

    /**
     * Convert an authorization code into an access code
     * @param string
     * @return array Response
     */
    static public function getAccessToken($authorization_code) 
    {

        // build uri
        $uri = self::API_URL . "/oauth/token";

        // setup curl
        $curl = curl_init();
        $curl_opts = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $uri,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            //CURLOPT_VERBOSE => true
        );

        // POST request
        $curl_opts[CURLOPT_POST] = true;
        $curl_opts[CURLOPT_HTTPHEADER] = array(
            'Accept: application/json'
        );
        $curl_opts[CURLOPT_POSTFIELDS] = http_build_query(
            array(
                "client_id" => self::CLIENT_ID,
                "client_secret" => self::CLIENT_SECRET,
                "grant_type" => "authorization_code",
                "code" => $authorization_code
            )
        );

        // set curl options
        curl_setopt_array($curl, $curl_opts);

        // perform request
        $result = curl_exec($curl);

        // error
        if (curl_errno($curl)) {
            throw new OrcidCurlException("Curl Err No " . curl_errno($curl) . ": " . curl_error($curl));
        }

        // get http response no
        $httpResponseNo = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        // close curl
        curl_close($curl);

        // http response not 2xx
        if ($httpResponseNo < 200 || $httpResponseNo > 299) {
            throw new OrcidApiResponseException("API returned {$httpResponseNo} response. [{$result}]");
        }

        // no results
        if (!trim($result)) {
            throw new OrcidApiResponseException("API returned empty response.");
        }

        // determine if error was returned
        $json_result = json_decode($result, true);

        return $json_result;
    }

    /**
     * Get ORCID user profile
     * @param string
     * @param string
     * @return array Response
     */
    static public function getProfile($access_token, $orcid_id) 
    {

        // build uri
        $uri = self::API_URL . "/v1.1/{$orcid_id}/orcid-profile";

        // setup curl
        $curl = curl_init();
        $curl_opts = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $uri,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            //CURLOPT_VERBOSE => true
        );

        $curl_opts[CURLOPT_HTTPHEADER] = array(
            'Authorization: Bearer ' . $access_token,
            'Accept: application/json'
        );

        // set curl options
        curl_setopt_array($curl, $curl_opts);

        // perform request
        $result = curl_exec($curl);

        // error
        if (curl_errno($curl)) {
            throw new OrcidCurlException("Curl Err No " . curl_errno($curl) . ": " . curl_error($curl));
        }

        // get http response no
        $httpResponseNo = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        // close curl
        curl_close($curl);

        // http response not 2xx
        if ($httpResponseNo < 200 || $httpResponseNo > 299) {
            throw new OrcidApiResponseException("API returned {$httpResponseNo} response. [{$result}]");
        }

        // no results
        if (!trim($result)) {
            throw new OrcidApiResponseException("API returned empty response.");
        }

        // determine if error was returned
        $json_result = json_decode($result, true);

        return $json_result;
    }

}