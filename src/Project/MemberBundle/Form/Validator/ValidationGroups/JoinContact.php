<?php

namespace Project\MemberBundle\Form\Validator\ValidationGroups;
use Symfony\Component\Form\FormInterface;

class JoinContact
{

    /**
     * Join/Renew contact and affiliation form
     * @param FormInterface $form
     */
    static function determineValidationGroups(FormInterface $form)
    {

        $groups = array("global");

        // secondary address
        if ($form->get("has_secondary")->getData()) {
            $groups[] = "secondary";
        }

        // authentication
        if (!$form->get("has_login")->getData()) {
            $groups[] = "authentication";
        }

        return $groups;

    }
}