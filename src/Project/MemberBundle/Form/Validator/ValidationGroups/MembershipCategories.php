<?php

namespace Project\MemberBundle\Form\Validator\ValidationGroups;
use Symfony\Component\Form\FormInterface;

class MembershipCategories
{

    /**
     * Determines what fields should be validated
     * for membership categories form.
     * @param FormInterface $form
     */
    static function determineValidationGroups(FormInterface $form)
    {

        // promo apply clicked
        if ($form->get("promo_apply")->isClicked()) {
            return array("promo_apply");
        }


        $membershipCategory = $form->get("membership_category")->getData();
        if ($form->has($membershipCategory)) {
            return array("global", $membershipCategory);
        } else {
            return array("global");
        }
    }
}