<?php

namespace Project\MemberBundle\Form\Validator\ValidationGroups;
use Symfony\Component\Form\FormInterface;
use Project\MemberBundle\Form\Populator\CareerPathSurvey as CareerPathPopulator;

class CareerPath
{
    /**
     * Career Path Survey Form
     * @param FormInterface $form
     */
    static function determineValidationGroups(FormInterface $form)
    {

        $validationGroups = array("global");

        // validation year selection for academic ranks
        foreach ($form->get("rank")->getIterator() as $collection) {
            if ($collection->get("id")->getData() && $collection->get("id")->getData() != CareerPathPopulator::ACADEMIC_RANK_NOT_APPLICABLE) {
                $validationGroups[] = $collection->getName() . "_year";
            }
        }
        return $validationGroups;

    }
}