<?php
namespace Project\MemberBundle\Form\Validator\Constraints;
use Symfony\Component\Validator\Constraint;

/**
 * Member id constraint, checks
 * if field value matches a valid
 * member id.
 */
class MemberId extends Constraint
{
    public $invalidMemberIdMessage = "The member ID you entered was invalid.";
    public $mustBeGreaterThanZeroMessage = "Value must be greater than zero.";
    public $mustBeIntegerMessage = "Value must be an integer.";

    public function validatedBy()
    {
        return "project.member.validator.memberid";
    }
}