<?php
namespace Project\MemberBundle\Form\Validator\Constraints;
use Symfony\Component\Validator\Constraint;

/**
 * Password match constraint, checks
 * if field value matches current member
 * password
 */
class PasswordMatch extends Constraint
{
    public $message = 'The password is incorrect.';

    public function validatedBy()
    {
        return "project.member.validator.passwordmatch";
    }

}