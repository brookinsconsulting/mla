<?php
namespace Project\MemberBundle\Form\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\HttpFoundation\Session\Session;
use Project\MemberBundle\Services\Member;

/**
 * Password match constraint, checks
 * if field value matches current member
 * password
 */
class PasswordMatchValidator extends ConstraintValidator
{

    /**
     * Mla Member API object
     * @var Project\ApiBundle\Classes\Member
     */
    protected $member = null;

    /**
     * Constructor.
     * @param Project\MemberBundle\Services\Member $member
     */
    public function __construct(Member $member_service)
    {
        $this->member = $member_service->getCurrentMember();
    }

    /**
     * Validate password
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {

        if (!$value) {
            return;
        }

        if (!$this->member || !$this->member->getApiData()->verifyPassword($value)) {
            $this->context->addViolation(
                $constraint->message,
                array()
            );
        }
    }
}