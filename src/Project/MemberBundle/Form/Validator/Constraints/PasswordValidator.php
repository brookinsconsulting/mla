<?php
namespace Project\MemberBundle\Form\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\HttpFoundation\Session\Session;
use Project\ApiBundle\Exception\ApiResponseException;
use Project\ApiBundle\Services\Api;

/**
 * Password constraint, checks if field 
 * value is valid password.
 */
class PasswordValidator extends ConstraintValidator
{

    /**
     * Min length a password can be.
     * @var integer
     */
    const MIN_LENGTH = 8;

    /**
     * Validate Username
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {

        // no value is ok
        if (!$value) {
            return;
        }

        // 8+ characters
        if (!self::validateLength($value)) {
            $this->context->addViolation(
                $constraint->notLongEnough,
                array()
            );
        }

        // must contain one uppercase, one lowercase, and one digit
        if (!self::validateCharacters($value)) {
            $this->context->addViolation(
                $constraint->invalidFormat,
                array()
            );
        }


    }

    /**
     * Return true if given value is correct
     * length for a password.
     * @param string $value
     * @return boolean
     */
    static public function validateLength($value)
    {
        if (strlen($value) < self::MIN_LENGTH) {
            return false;
        }
        return true;
    }

    /**
     * Return true if given value contains
     * correct charactesr for a password.
     * @param string $value
     * @return boolean
     */
    static public function validateCharacters($value)
    {
        $hasUcLetter = false;
        $hasLcLetter = false;
        $hasDigit = false;
        for ($i = 0; $i < strlen($value); $i++) {
            if (ctype_upper($value[$i])) {
                $hasUcLetter = true;
            }
            if (ctype_lower($value[$i])) {
                $hasLcLetter = true;
            }
            if (ctype_digit($value[$i])) {
                $hasDigit = true;
            }
        }
        if (!$hasUcLetter || !$hasLcLetter || !$hasDigit) {
            return false;
        }  
        return true;
    }
}