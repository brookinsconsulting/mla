<?php
namespace Project\MemberBundle\Form\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\HttpFoundation\Session\Session;
use Project\MemberBundle\Services\Member as MemberService;
use Project\ApiBundle\Exception\ApiResponseException;
use Project\ApiBundle\Services\Api;

/**
 * Username constraint, checks if field 
 * value is valid username and ensures 
 * username does not already exist.
 */
class UsernameValidator extends ConstraintValidator
{

    /**
     * Api service
     * @var Api
     */
    protected $api;

    /**
     * Member service
     * @var MemberService
     */
    protected $memberService;

    /**
     * Min/Max length a username can
     * be.
     * @var integer
     */
    const MIN_LENGTH = 4;
    const MAX_LENGTH = 20;

    /**
     * Constructor.
     * @param Api $member
     */
    public function __construct(Api $api, MemberService $memberService)
    {
        $this->api = $api;
        $this->memberService = $memberService;
    }

    /**
     * Validate Username
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {

        // no value is ok
        if (!$value) {
            return;
        }

        // validate length
        if (!self::validateLength($value)) {
            $this->context->addViolation(
                $constraint->invalidUsernameMessage,
                array()
            );
            return;            
        }

        // validate format
        if (!self::validateFormat($value)) {
            $this->context->addViolation(
                $constraint->invalidUsernameMessage,
                array()
            );
            return;  
        }

        // validate duplicates
        if (!self::validateDuplicate($this->api, $this->memberService, $value)) {
            $this->context->addViolation(
                $constraint->alreadyExistsMessage,
                array()
            );
            return;       
        }

    }

    /**
     * Return true if given value is valid
     * length of username
     * @param string $value
     * @return boolean
     */
    static public function validateLength($value)
    {
        if (strlen($value) < self::MIN_LENGTH || strlen($value) >self::MAX_LENGTH) {
            return false;
        }
        return true;
    }

    /**
     * Return true if given value is valid
     * format for username
     * @param string $value
     * @return boolean
     */
    static public function validateFormat($value)
    {

        // must contain letters, numbers, and underscores only
        if (preg_match('/[^A-Za-z0-9_]/', $value)) {
            return false; 
        }

        // first character cannot be a number
        if (is_numeric($value[0])) {
            return false;
        }

        // must contain a letter
        $hasLetter = false;
        for ($i = 0; $i < strlen($value); $i++) {
            if (ctype_alpha($value[$i])) {
                $hasLetter = true;
                break;
            }
        }
        if (!$hasLetter) {
            return false;
        }

        return true;
    }

    /**
     * Return true if given value does not already
     * exist as a username
     * @param Api $api
     * @param MemberService $memberService
     * @param string $value
     * @return boolean
     */
    static public function validateDuplicate(Api $api, MemberService $memberService, $value)
    {

        // get current member
        $member = $memberService->getCurrentMember();

        // can use the same username as the currently logged in member
        if ($member) {
            $memberAuth = $member->getApiData()->get("authentication");
            if (isset($memberAuth['username']) && $memberAuth['username'] == $value) {
                return true;
            }
        }

        try {
        
            $request = $api->request(
                "members",
                "GET",
                array(
                    "type" => "duplicate",
                    "username" => $value
                )
            );
           
            if ($request['data'][0]['username']['duplicate']) {
                return false;
            }

        } catch (ApiResponseException $e) {
        }

        return true;
    }


}