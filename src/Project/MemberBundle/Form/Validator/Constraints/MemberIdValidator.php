<?php
namespace Project\MemberBundle\Form\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\HttpFoundation\Session\Session;
use Project\ApiBundle\Exception\ApiResponseException;
use Project\ApiBundle\Services\Api;

/**
 * Member id constraint, checks
 * if field value matches a valid
 * member id.
 */
class MemberIdValidator extends ConstraintValidator
{

    /**
     * Api service
     * @var Api
     */
    protected $api;

    /**
     * Constructor.
     * @param Api $member
     */
    public function __construct(Api $api)
    {
        $this->api = $api;
    }

    /**
     * Validate Member Id
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        // no value is ok
        if (!$value) {
            return;
        }

        // value must be integer
        if (!self::validateInteger($value)) {
            $this->context->addViolation(
                $constraint->mustBeIntegerMessage,
                array()
            );
            return;
        }

        // value must be greater than 0
        if (!self::validateRange($value)) {
            $this->context->addViolation(
                $constraint->mustBeGreaterThanZeroMessage,
                array()
            );
            return;
        }
        

        // make sure we can pull member without getting
        // an ApiResponseException
        if (!self::validateExistance($this->api, $value)) {
            $this->context->addViolation(
                $constraint->invalidMemberIdMessage,
                array()
            );
        }
    }

    /**
     * Ensure value is integer
     * @param mixed $value
     * @return bool
     */
    static public function validateInteger($value)
    {
        return is_numeric($value);
    }

    /**
     * Ensure value is greater than 0
     * @param integer $value
     * @return bool
     */
    static public function validateRange($value)
    {
        return $value > 0;
    }

    /**
     * Validate the existance of a member with given id
     * @param Api $api
     * @param integer $value
     * @return bool
     */
    static public function validateExistance(Api $api, $value)
    {
        try {
            return $api->getMember($value) ? true : false;
        } catch (ApiResponseException $e) {
            return false;
        }
    }
}