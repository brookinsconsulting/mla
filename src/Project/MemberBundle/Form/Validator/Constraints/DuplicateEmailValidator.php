<?php
namespace Project\MemberBundle\Form\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\HttpFoundation\Session\Session;
use Project\MemberBundle\Services\Member as MemberService;
use Project\ApiBundle\Exception\ApiResponseException;
use Project\ApiBundle\Services\Api;

/**
 * Duplicate email constraint.
 */
class DuplicateEmailValidator extends ConstraintValidator
{

    /**
     * Api service
     * @var Api
     */
    protected $api;

    /**
     * Member service
     * var MemberService
     */
    protected $memberService;

    /**
     * Constructor.
     * @param Api $member
     * @param MemberService $memberService
     */
    public function __construct(Api $api, MemberService $memberService)
    {
        $this->api = $api;
        $this->memberService = $memberService;
    }

    /**
     * Validate Username
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {

        // no value is ok
        if (!$value) {
            return;
        }

        // validate duplicates
        if (!self::validateDuplicate($this->api, $this->memberService, $value)) {
            $this->context->addViolation(
                $constraint->alreadyExistsMessage,
                array()
            );
            return;       
        }

    }

    /**
     * Return true if given value does not already
     * exist as a email
     * @param Api $api
     * @param string $value
     * @return boolean
     */
    static public function validateDuplicate(Api $api, MemberService $memberService, $value)
    {

        // get current member
        $member = $memberService->getCurrentMember();

        // accept same email as current member
        if ($member) {
            $memberGeneral = $member->getApiData()->get("general");
            if (isset($memberGeneral['email']) && strtolower($memberGeneral['email']) == strtolower($value)) {
                return true;
            }
        }

        try {
            $request = $api->request(
                "members",
                "GET",
                array(
                    "type" => "duplicate",
                    "email" => $value
                )
            );

            if ($request['data'][0]['email']['duplicate']) {
                return false;
            }

        } catch (ApiResponseException $e) {
        }

        return true;
    }


}