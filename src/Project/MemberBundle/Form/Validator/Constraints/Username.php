<?php
namespace Project\MemberBundle\Form\Validator\Constraints;
use Symfony\Component\Validator\Constraint;

/**
 * Username constraint, checks if field 
 * value is valid username and ensures 
 * username does not already exist.
 */
class Username extends Constraint
{
    public $invalidUsernameMessage = "Invalid username.";
    public $alreadyExistsMessage = "Username already exists.";

    public function validatedBy()
    {
        return "project.member.validator.username";
    }
}