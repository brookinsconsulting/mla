<?php
namespace Project\MemberBundle\Form\Validator\Constraints;
use Symfony\Component\Validator\Constraint;

/**
 * Password constraint, checks if field 
 * value is valid password.
 */
class Password extends Constraint
{
    public $notLongEnough = "Your password must contain at least 8 characters.";
    public $invalidFormat = "Your password must include one uppercase letter, one lowercase letter, and one digit.";

    public function validatedBy()
    {
        return "project.member.validator.password";
    }
}