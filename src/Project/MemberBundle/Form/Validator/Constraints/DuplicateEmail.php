<?php
namespace Project\MemberBundle\Form\Validator\Constraints;
use Symfony\Component\Validator\Constraint;

/**
 * Duplicate email constraint for API.
 */
class DuplicateEmail extends Constraint
{
    public $alreadyExistsMessage = "Email already exists.";

    public function validatedBy()
    {
        return "project.member.validator.duplicate_email";
    }
}