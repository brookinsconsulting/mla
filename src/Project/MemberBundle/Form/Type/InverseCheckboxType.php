<?php

namespace Project\MemberBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\ChoiceList\SimpleChoiceList;
use Project\MemberBundle\Form\DataTransformer\ValueToInverseCheckboxTransformer;

/**
 * Inverse Checkbox Field Type
 * Provided a checkbox that returns
 * a value of false when checked and
 * a value of true when not checked
 */
class InverseCheckboxType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'inversecheckbox';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'checkbox';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        // append 'Other' choice to the choice list
        //$options['choice']['choices'] = $options['choice']['choices'] + array('_other' => 'Other');
        $builder
            //->add('inversecheckbox', 'checkbox', $options)
            ->addModelTransformer(
                new ValueToInverseCheckboxTransformer()
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setOptional(array("inversecheckbox"));
        $resolver->setDefaults(array(
            "error_bubbling" => false,
            "label" => null
        ));
    }
}
