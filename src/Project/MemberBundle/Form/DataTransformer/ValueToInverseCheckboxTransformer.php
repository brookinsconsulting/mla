<?php

namespace Project\MemberBundle\Form\DataTransformer;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Data Transformer for InverseCheckbox field type
 */
class ValueToInverseCheckboxTransformer implements DataTransformerInterface
{

    /**
     * {@inheritdoc}
     */
    public function transform($data)
    {
        return !$data;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($data)
    {
        return !$data;
    }    

}
