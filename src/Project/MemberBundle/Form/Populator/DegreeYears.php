<?php

namespace Project\MemberBundle\Form\Populator;
use Symfony\Component\HttpFoundation\Session\Session;
use eZ\Publish\API\Repository\Repository;
use Project\ApiBundle\Services\Api;

class DegreeYears
{

    /**
     * Returns values to be injected into form.
     */
    public function execute()
    {

        $years = array();
        for ($i = date("Y"); $i > date("Y") - 100; $i--) {
            $years[$i] = $i;
        }
        return $years;
    }
}