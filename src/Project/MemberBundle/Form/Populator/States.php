<?php

namespace Project\MemberBundle\Form\Populator;
use Symfony\Component\HttpFoundation\Session\Session;
use eZ\Publish\API\Repository\Repository;
use Project\ApiBundle\Services\Api;

class States
{


    /**
     * Country to return states for
     * @var string
     */
    protected $filterCountry = "";

    /**
     * State look up
     * @var array
     */
    protected $stateLookup = array();

    /**
     * Constructor.
     * @param Project\ApiBundle\Services\Api $api
     * @param string $item
     */
    public function __construct(Api $api, $filterCountry = "")
    {
        $this->stateLookup = $api->getLookup("states")->all();
        $this->filterCountry = $filterCountry;
    }

    /**
     * Returns values to be injected into form.
     * @return array
     */
    public function execute()
    {
        // filter to one country
        if ($this->filterCountry) {
            $filterVal = '';
            switch ($this->filterCountry) {
                case "U":
                case "US":
                case "USA":
                    $filterVal = 'U';
                    break;

                case "C":
                case "CAN":
                case "CANADA":
                    $filterVal = 'C';
                    break;
            }
            if ($filterVal) {
                $stateList = array();
                foreach ($this->stateLookup as $state) {
                    if ($state['us_canada'] == $filterVal) {
                        $stateList[$state['state_code']] = ucwords(strtolower($state['state']));
                    }
                }
                return $stateList;
            }
        }


        // create list
        $stateList = array();
        foreach ($this->stateLookup as $state) {
            switch (strtoupper(trim($state['us_canada']))) {
                case "U":
                    $stateList["United States"][$state['state_code']] = ucwords(strtolower($state['state']));
                    break;

                case "C":
                    $stateList["Canada"][$state['state_code']] = ucwords(strtolower($state['state']));
                    break;

                default:
                    $stateList["Misc"][$state['state_code']] = ucwords(strtolower($state['state']));
                    break;
            }            
        }
        return $stateList;
    }

}