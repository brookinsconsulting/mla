<?php

namespace Project\MemberBundle\Form\Populator;
use Symfony\Component\HttpFoundation\Session\Session;
use eZ\Publish\API\Repository\Repository;
use Project\ApiBundle\Services\Api;

class Countries
{

    /**
     * Country lookup
     * @var array
     */
    protected $countryLookup = array();

    /**
     * Constructor.
     * @param Project\ApiBundle\Services\Api $api
     */
    public function __construct(Api $api)
    {
        $this->countryLookup = $api->getLookup("countries")->all();
    }

    /**
     * Returns values to be injected into form.
     * @return array
     */
    public function execute()
    {
        $countryList = array();
        foreach ($this->countryLookup as $country) {
            $countryList[$country['country_code']] = $country['country'];
        }
        asort($countryList, SORT_STRING);
        return $countryList;
    }

}