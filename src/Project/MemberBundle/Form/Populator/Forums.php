<?php

namespace Project\MemberBundle\Form\Populator;
use Symfony\Component\HttpFoundation\Session\Session;
use eZ\Publish\API\Repository\Repository;
use Project\ApiBundle\Services\Api;
use Project\ApiBundle\Endpoint\Organizations;

class Forums
{

    /**
     * Mla API service
     * @var Project\ApiBundle\Services\Api
     */
    protected $api = null;

    /**
     * API Organizations Endpoint
     * @var Project\ApiBundle\Endpoint\Organizations
     */
    protected $organizations;

    /**
     * Constructor.
     * @param Project\ApiBundle\Services\Api $api
     */
    public function __construct(Api $api)
    {
        $this->organizations = $api->getOrganizations();
    }

    /**
     * Returns values to be injected into form.
     */
    public function execute()
    {
        $list = $this->organizations->getOrganizations(null, "forum");
        $output = array();
        foreach ($list as $item) {
            $output[$item["id"]] = ($item["category"] ? $item["category"] . " " : "") . $item["name"];
        }
        return $output;
    }
}