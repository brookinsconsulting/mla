<?php

namespace Project\MemberBundle\Form\Populator;
use Symfony\Component\HttpFoundation\Session\Session;
use eZ\Publish\API\Repository\Repository;
use Project\ApiBundle\Services\Api;

class Languages
{

    /**
     * Mla API service
     * @var Project\ApiBundle\Services\Api
     */
    protected $api = null;

    /**
     * Constructor.
     * @param Project\ApiBundle\Services\Api $api
     */
    public function __construct(Api $api)
    {
        $this->api = $api;
    }

    /**
     * Returns values to be injected into form.
     */
    public function execute()
    {
        $languages = $this->api->getLookup("languages");

        $languageList = array();
        foreach ($languages->all() as $language) {
            $languageList[$language['id']] = $language['name'];
        }

        return $languageList;
    }
}