<?php

namespace Project\MemberBundle\Form\Populator;
use Symfony\Component\HttpFoundation\Session\Session;
use eZ\Publish\API\Repository\Repository;
use Project\ApiBundle\Services\Api;

class Departments
{

    /**
     * Mla API service
     * @var Project\ApiBundle\Services\Api
     */
    protected $api = null;

    /**
     * Constructor.
     * @param Project\ApiBundle\Services\Api $api
     */
    public function __construct(Api $api)
    {
        $this->api = $api;
    }

    /**
     * Returns values to be injected into form.
     * @param string $zip_code
     */
    public function execute($zip_code = null)
    {
        $departments = $this->api->getLookup(
            "departments",
            array(
                "zip_code" => $zip_code
            )
        );

        $departmentList = array();
        foreach ($departments->all() as $department) {
            $departmentList[$department['id']] = "{$department['affiliation']} - {$department['name']}";
        }
        return $departmentList;
    }
}