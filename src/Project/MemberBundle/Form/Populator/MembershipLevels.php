<?php

namespace Project\MemberBundle\Form\Populator;
use Symfony\Component\HttpFoundation\Session\Session;
use eZ\Publish\API\Repository\Repository;
use Project\ApiBundle\Services\Api;

class MembershipLevels
{

    /**
     * Mla API service
     * @var Project\ApiBundle\Services\Api
     */
    protected $api = null;

    /**
     * Year
     * @var integer
     */
    protected $year;

    /**
     * Filter out all but the following
     * @var array
     */
    protected $filters = array();

    /**
     * Constructor.
     * @param Project\ApiBundle\Services\Api $api
     * @param integer $year
     * @param array $filters
     */
    public function __construct(Api $api, $year = null, array $filters = array())
    {
        $this->api = $api;
        $this->year = $year ?: date("Y");
        $this->filters = $filters;
    }

    /**
     * Returns values to be injected into form.
     */
    public function execute()
    {

        // get class codes
        $classCodeLookup = $this->api->request(
            "lookups/class_codes",
            "GET",
            array("year" => $this->year)
        );

        // sort results
        usort(
            $classCodeLookup['data'],
            function($a, $b) {
                if ($a['id'] == "F") {
                    return -1;
                } elseif ($b['id'] == "F") {
                    return 1;
                }
                return $a['rate'] - $b['rate'];
            }
        );

        // list of ids to accept
        // populate with list of integers
        // which represent ASCII characters
        $filterIds = array();

        // filters
        foreach ($this->filters as $filter) {

            switch ($filter) {

                // income values are 57-82
                case "income":
                    for ($i = 49; $i <= 57; $i++) {
                        $filterIds[] = $i;
                    }
                    for ($i = 82; $i <= 90; $i++) {
                        $filterIds[] = $i;
                    }

                    break;

                default:

                    // filter is CHR value of id
                    if (is_integer($filter)) {
                        $filterIds[] = $filter;

                    // filter is single character ID
                    } elseif (strlen($filter) == 1) {
                        $filterIds[] = ord($filter);
                    }

                    break;

            }
        }

        $items = array();
        foreach ($classCodeLookup['data'] as $item) {

            // not included in filter
            if ($filterIds && !in_array(ord($item['id']), $filterIds)) {
                continue;
            }

            $items[$item['id']] = ($item['rate'] > 0 ? "\${$item['rate']} - " : "") . str_replace(" - ", "–", $item['income']);
        }

        return $items;
    }
}