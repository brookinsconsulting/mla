<?php

namespace Project\MemberBundle\Form\Populator;
use Symfony\Component\HttpFoundation\Session\Session;
use eZ\Publish\API\Repository\Repository;
use Project\ApiBundle\Services\Api;

class CareerPathSurvey
{

    /**
     * ID of "not applicable" academic rank status
     * @var integer
     */
    const ACADEMIC_RANK_NOT_APPLICABLE = 6;

    /**
     * Item in lookup parameter bag
     * to use.
     * @var string
     */
    protected $item;

    /**
     * Survey Lookup results from api
     * @var array
     */
    protected $surveyLookup = array();

    /**
     * Constructor.
     * @param Project\ApiBundle\Services\Api $api
     * @param string $item
     */
    public function __construct(Api $api, $item)
    {
        $this->surveyLookup = $api->getLookup("surveys/career_path")->all();
        $this->item = $item;
    }

    /**
     * Returns values to be injected into form.
     * @return array
     */
    public function execute()
    {
        if (!isset($this->surveyLookup[0][$this->item])) {
            return array();
        }
        $surveyList = array();
        foreach ($this->surveyLookup[0][$this->item] as $surveyItem) {
            if (strtolower(trim($surveyItem['name'])) == "other") {
                continue;
            }
            $surveyList[$surveyItem['id']] = $surveyItem['name'];
        }
        return $surveyList;
    }

    /**
     * Get 'id' of "other" field
     * @return string|integer|null
     */
    public function getOtherId()
    {
        if (!isset($this->surveyLookup[0][$this->item])) {
            return null;
        }
        $surveyList = array();
        foreach ($this->surveyLookup[0][$this->item] as $surveyItem) {
            if (strtolower(trim($surveyItem['name'])) == "other") {
                return $surveyItem['id'];
            }
        }
        return null;
    }

}