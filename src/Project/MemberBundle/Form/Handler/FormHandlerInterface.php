<?php

namespace Project\MemberBundle\Form\Handler;
use Symfony\Component\Form\Form;

interface FormHandlerInterface
{

    /**
     * Populates a form
     * @param ThinkCreative\FormBundle\Classes\Form $form
     */
    public function populate(Form $form);

    /**
     * Handles processing of a submitted form
     * @param ThinkCreative\FormBundle\Classes\Form $form
     */
    public function submit(Form $form);
}