<?php

namespace Project\MemberBundle\Form\Handler;
use Symfony\Component\Form\Form;
use ThinkCreative\FormBundle\Classes\Form as ThinkCreativeForm;
use Project\ApiBundle\Exception\ApiResponseException;
use Project\MemberBundle\Form\Handler\FormHandlerInterface;
use Project\MemberBundle\Services\Member;
use Project\MemberBundle\Exception\MemberFormException;
use Project\MemberBundle\Form\Populator\CareerPathSurvey as CareerPathSurveyPopulator;

/**
 * Career Path Survey Form Handler
 * Handles retriving and posting of API member data.
 */
class CareerPathSurveyFormHandler implements FormHandlerInterface
{

    /**
     * Member service
     * @var Project\MemberBundle\Services\Member
     */
    protected $memberService;

    /**
     * Constructor.
     * @param Project\MemberBundle\Services\Member $member_service
     */
    public function __construct(Member $member_service)
    {
        $this->memberService = $member_service;
    }

    /**
     * {@inheritdoc}
     */
    public function populate(Form $form)
    {

        // get member 
        $memberApi = $this->memberService->getCurrentMember()->getApiData();

        // get survey
        $survey = $memberApi->getSurvey();

        // iterate fields
        foreach (ThinkCreativeForm::getFieldList($form) as $field) {

            if (!$survey->has($field->getName())) {
                continue;
            }

            // process form fields based on their type
            switch ($field->getConfig()->getType()->getName()) {

                case "choice":

                    $options = $field->getConfig()->getOptions();
                    $values = $survey->get($field->getName());

                    if (is_array($values) && array_key_exists("id", $values)) {
                        if (!is_array($values['id']) && count(explode(",", $values['id']) > 0)) {
                            $values = explode(",", $values['id']);
                        } else {
                            $values = $values['id'];
                        }
                    }
                    if (is_array($values) && !$options['multiple']) {
                        $values = $values[0];
                    }


                    $field->setData(
                        $options['multiple'] && !is_array($values) ? 
                            array($values) :
                            $values
                    );

                    break;

                case "choiceother":

                    $choiceOptions = $field->get("choice")->getConfig()->getOptions();
                    $values = $survey->get($field->getName());

                    if (!is_array($values['id']) && count(explode(",", $values['id']) > 0)) {
                        $values['id'] = explode(",", $values['id']);
                    }

                    if ($values['id'] == "O") {
                        $values['id'] = "_other";
                    } else if (is_array($values['id']) && array_search("O", $values['id']) !== false) {
                        $values['id'][array_search("O", $values['id'])] = "_other";
                    }
                    
                    if (
                        (
                            !array_key_exists("multiple", $choiceOptions) || 
                            !$choiceOptions['multiple']
                        ) &&
                        is_array($values['id'])
                    ) {
                        $values['id'] = $values['id'][0];
                    }
                    $field->get("choice")->setData($values['id']);

                    if (array_key_exists("other", $values)) {
                        $field->get("text")->setData($values['other']);
                    }

                    break;

                case "form":

                    $values = $survey->get($field->getName());
                    $ct = 0;
                    foreach ($field->all() as $child) {
                        if ($ct >= count($values)) {
                            break;
                        }
                        $child->setData($values[$ct]);
                        $ct++;
                    }

                    break;

                default:
                    $value = $survey->get($field->getName());
                    if (is_array($value) && array_key_exists("id", $value)) {
                        $value = $value['id'];
                    }

                    $field->setData($value);
                    break;

            }
            
        }

    }

    /**
     * {@inheritdoc}
     */
    public function submit(Form $form)
    {

        // get member 
        $memberApi = $this->memberService->getCurrentMember()->getApiData();

        // get survey
        $survey = $memberApi->getSurvey();

        // iterate fields
        foreach (ThinkCreativeForm::getFieldList($form) as $field) {

            if (!in_array($field->getName(), array_keys($form->getData()))) {
                continue;
            }

            // process form fields based on their type
            switch ($field->getConfig()->getType()->getName()) {

                case "choiceother":

                    $values = array();
                    $choiceValue = $field->get("choice")->getData();

                    // set 'other' value
                    $values['other'] = "";
                    if ($choiceValue == "_other" || (is_array($choiceValue) && in_array("_other", $choiceValue))) {
                        $values['other'] = $field->get("text")->getData();
                    }

                    // replace _other with correct value
                    if ($choiceValue == "_other") {
                        $populator = new CareerPathSurveyPopulator($this->memberService->getApiService(), $field->getName());
                        $choiceValue = $populator->getOtherId() ?: "O";
                    } else if (is_array($choiceValue) && array_search("_other", $choiceValue) !== false) {
                        $populator = new CareerPathSurveyPopulator($this->memberService->getApiService(), $field->getName());
                        $choiceValue[array_search("_other", $choiceValue)] = $populator->getOtherId() ?: "O";
                    }

                    // set 'id' value
                    $values['id'] = $choiceValue;

                    $survey->set($field->getName(), $values);
                    break;

                case "form":
                    $survey->set($field->getName(), array_values($field->getData()));
                    break;

                case "collection":
                    $values = array();
                    foreach ($field->all() as $subField) {
                        $values[] = $subField->getData();
                    }
                    $survey->set($field->getName(), $values);
                    break;

                case "choice":

                    $choiceOptions = $field->getConfig()->getOptions();
                    if ($choiceOptions['multiple']) {
                        $fieldValue = array("id" => $field->getData());
                    } else {
                        $survey->set($field->getName(), $field->getData());
                    }

                default:

                    $apiValue = $survey->get($field->getName());
                    $fieldValue = $field->getData();
                    if (is_array($apiValue) && array_key_exists("id", $apiValue)) {
                        $fieldValue = array("id" => $fieldValue);
                    }

                    $survey->set($field->getName(), $fieldValue);
                    break;

            }

        }

        $survey->update(false);

    }

}