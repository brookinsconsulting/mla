<?php

namespace Project\MemberBundle\Form\Handler;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\ParameterBag;
use ThinkCreative\FormBundle\Classes\Form as ThinkCreativeForm;
use Project\ApiBundle\Exception\ApiResponseException;
use Project\ApiBundle\Services\Api as ProjectApi;
use Project\ApiBundle\Endpoints\Member as MemberApiEndpoint;
use Project\MemberBundle\Form\Handler\FormHandlerInterface;
use Project\MemberBundle\Services\Member;
use Project\MemberBundle\Exception\MemberFormException;

/**
 * Member Form Handler
 * Handles retriving and posting of API member data.
 */
class MemberFormHandler implements FormHandlerInterface
{

    /**
     * Member service
     * @var Project\MemberBundle\Services\Member
     */
    protected $memberService;

    /**
     * Fields to ignore if blank
     * @var array
     */
    protected $ignoreBlankFields = array(
        "authentication/membership_status",
        "authentication/username",   // special case where username will be submitted to general/username
        "authentication/password"
    );

    /**
     * Constructor.
     * @param Project\MemberBundle\Services\Member $member_service
     */
    public function __construct(Member $member_service)
    {
        $this->memberService = $member_service;
    }

    /**
     * {@inheritdoc}
     */
    public function populate(Form $form)
    {

        // get member 
        $memberApi = $this->memberService->getCurrentMember()->getApiData();

        // iterate fields
        foreach (ThinkCreativeForm::getFieldList($form) as $field) {

            // get api endpoint
            $apiEndpoint = $this->getEndpointField($field, $form);
            if (!$apiEndpoint) {
                continue;
            }

            // address "special case" endpoint defined by "address_{type}"
            if (substr($apiEndpoint[0], 0, 8) == "address_") {
                $addressType = substr($apiEndpoint[0], 8);
                $apiEndpoint[0] = "addresses";
                $hasAddress = false;
                foreach ($memberApi->get("addresses") as $key=>$address) {
                    if ($address['type'] == $addressType) {
                        $apiEndpoint[1] = $key;
                        $hasAddress = true;
                        break;
                    }
                }
                if (!$hasAddress) {
                    continue;
                }
            }

            // get data from endpoint, drilldown to needed value
            $drilldown = $memberApi->get(trim($apiEndpoint[0]));
            if (count($apiEndpoint) > 1) {
                for ($i = 1; $i <= count($apiEndpoint) - 1; $i++) {
                    if (!array_key_exists($apiEndpoint[$i], $drilldown)) {
                        continue;
                    }
                    $drilldown = $drilldown[$apiEndpoint[$i]];
                }
            }

            // username override
            if ($apiEndpoint[0] == "general" && $field->getName() == "username") {
                $fieldValue = $memberApi->get("authentication[username]", null, true);

            // drill down does not contain field value
            } else if ($drilldown && !array_key_exists($field->getName(), $drilldown)) {
                continue;

            // get field value from drill down
            } else {
                $fieldValue = $drilldown[$field->getName()];
            }

            // addresses override
            if ($apiEndpoint[0] == "addresses" && $field->getName() == "department") {

                $address = $memberApi->get($apiEndpoint[0]);
                $address = $address[$apiEndpoint[1]];

                if (isset($address['department']) && $address['department']) {
                    $fieldValue = "_" . $address['department'];
                } elseif (isset($address['department_other'])) {
                    $fieldValue = $address['department_other'];
                }

            }

            // set field values, force to specific type if needed
            switch(gettype($field->getData())) {
                case "boolean":

                    switch(strtoupper($fieldValue)) {
                        case "N":
                        case "0":
                        case "NO":
                            $field->setData(false);
                            break;

                        default:
                            $field->setData((bool) $fieldValue);
                            break;        
                    }                   
                    break;

                default:

                    // boolean string parse
                    if ($field->getData() == "Y") {
                        $field->setData(true);
                    } elseif ($field->getData() == "N") {
                        $field->setData(false);
                    } else {
                        $field->setData($fieldValue);
                    }
                    break;
            }
            
        }
    }

    /**
     * {@inheritdoc}
     * @param boolean $new_member  Will create a new member if true
     * @return integer Member ID number
     */
    public function submit(Form $form, $new_member = false)
    {

        // get member
        if (!$new_member) {
            $memberApi = $this->memberService->getCurrentMember()->getApiData();
        } else {
            $memberApi = new ParameterBag();
        }

        // endpoints to update
        $updateEndpoints = array();

        // iterate fields
        foreach (ThinkCreativeForm::getFieldList($form) as $field) {

            // get endpoint
            $apiEndpoint = $this->getEndpointField($field, $form);
            if (!$apiEndpoint) {
                continue;
            }

            // ignore children of "repeated" field type
            if ($field->getParent()->getConfig()->getType()->getName() == "repeated") {
                continue;
            }

            // address "special case" endpoint defined by "address_{type}"
            if (substr($apiEndpoint[0], 0, 8) == "address_") {
                $addressType = substr($apiEndpoint[0], 8);
                $apiEndpoint[0] = "addresses";
                $hasAddress = false;

                if ($memberApi->get("addresses")) {
                    foreach ($memberApi->get("addresses") as $key=>$address) {
                        if ($address['type'] == $addressType) {
                            $apiEndpoint[1] = $key;
                            $hasAddress = true;
                            break;
                        }
                    }
                }
                if (!$hasAddress) {
                    $apiEndpoint[1] = count( $memberApi->get("addresses") );
                }
            }

            // ignore fields that contain array values
            if (is_array($field->getData())) {
                continue;
            }

            // ignore fields
            $hasIgnoreField = false;
            foreach ($this->ignoreBlankFields as $ignoreField) {
                $ignoreField = explode("/", $ignoreField);

                if (count($ignoreField) - 1 != count($apiEndpoint)) {
                    continue;
                }

                $isMatching = true;
                for ($i = 0; $i < count($apiEndpoint); $i++) {
                    if ($ignoreField[$i] != $apiEndpoint[$i]) {
                        $isMatching = false;
                        break;
                    }
                }
                if ($isMatching) {
                    if ($field->getName() == $ignoreField[count($ignoreField) - 1]) {
                        $hasIgnoreField = true;
                        break;
                    }
                }
            }

            if ($hasIgnoreField && !$field->getData()) {
                continue;
            }         

            // set 'username' value in general instead of authentication
            if ($apiEndpoint[0] == "authentication" && $field->getName() == "username" && !$new_member) {
                // username can no longer be updated
                continue;
                /*if (!in_array("general", $updateEndpoints)) {
                    $updateEndpoints[] = "general";
                }*/

            // set 'username' value in authentication for new member
            } elseif ($apiEndpoint[0] == "general" && $field->getName() == "username" && $new_member) {
                if (!in_array("authentication", $updateEndpoints)) {
                    $updateEndpoints[] = "authentication";
                }

            // set 'password' value
            } elseif ($apiEndpoint[0] == "authentication" && $field->getName() == "password") {

                // if not new member then old password is required
                if ($field->getData() && !$new_member && !$form->get("authentication")->get("update_password")->get("old_password")->getData()) {
                    $form->get("authentication")->get("update_password")->get("old_password")->addError(
                        new FormError("Old Password is required to update password.")
                    );
                }

                //if ($form->get("update_password")->get("old_password"))

            // create list of updated endpoints                
            } elseif (
                !in_array($apiEndpoint[0], $updateEndpoints) && 
                !is_array($field->getData()) && 
                !($apiEndpoint[0] == "authentication" && !$field->getData())
            ) {
                $updateEndpoints[] = $apiEndpoint[0];
            }

            // get data from endpoint, drilldown to needed value
            // and set the values
            if (in_array($apiEndpoint[0], MemberApiEndpoint::$apiEndpoints)) {
                $memberData = $memberApi->get(trim($apiEndpoint[0]));
                $drilldown =& $memberData;

                // set field values based on datatype
                switch(gettype($field->getData())) {
                    case "boolean":
                        $fieldData = $field->getData() ? "Y" : "";
                        break;

                    default:
                        $fieldData = $field->getData();
                        break;
                }

                if (count($apiEndpoint) > 1) {
                    for ($i = 1; $i <= count($apiEndpoint) - 1; $i++) {
                        $drilldown =& $drilldown[$apiEndpoint[$i]];
                    }
                    if (!is_array($field->getData()))  {
                        $drilldown[$field->getName()] = $fieldData;
                        if ($apiEndpoint[0] == "addresses" && $field->getName() == "department") {
                            if ($fieldData[0] == "_") {
                                $drilldown[$field->getName()] = intval(substr($fieldData, 1));
                                $drilldown[$field->getName() . "_other"] = "";
                            } else {
                                $drilldown[$field->getName()] = null;
                                $drilldown[$field->getName() . "_other"] = $fieldData;
                            }
                        }
                    }
                } else {
                    if (!is_array($field->getData()))  {
                        $memberData[$field->getName()] = $fieldData;
                    }
                }

                $memberApi->set(trim($apiEndpoint[0]), $memberData);
            }

        }

        // username belongs in general if not a new member
        $username = $memberApi->get("authentication[username]", null, true);
        if ($username && !$new_member) {
            $general = $memberApi->get("general");
            $general['username'] = $username;
            $memberApi->set("general", $general);
            $authentication = $memberApi->get("authentication");
            unset($authentication['username']);
            $memberApi->set("authentication", $authentication);

        } elseif (!$username && $new_member) {
            if ($memberApi->get("general[username]", null, true)) {
                $authentication = $memberApi->get("authentication");
                $authentication['username'] = $memberApi->get("general[username]", null, true);
                $memberApi->set("authentication", $authentication);
            }
        }

        // if unsubscribing from all emails uncheck other email prefs
        if ($memberApi->get("general[email_unsubscribe]", null, true) == "Y") {
            $general = $memberApi->get("general");
            $general['email_convention'] = "";
            $general['email_digest'] = "";
            $memberApi->set("general", $general);
        }

        // validation for addresses send mail
        $sendMailFields = array(
            "address_primary",
            "address_secondary",
            "address_home"
        );
        $sendMailCount = 0;
        foreach ($memberApi->get("addresses") as $address) {
            if (isset($address['send_mail']) && $address['send_mail'] != "N" && $address['send_mail'] != "") {

                // address must exist in order to send mail to it
                if (
                    !$address['line3'] ||
                    !$address['state'] || 
                    !$address['city'] || 
                    !$address['zip']
                    
                ) {
                    if ($form->has("address_" . $address['type'])) {
                        $form->get("address_" . $address['type'])->get("send_mail")->addError(
                            new FormError("Invalid or incomplete addresses cannot be used as a preferred mailing address.")
                        );
                        return false;
                    }
                }
                $sendMailCount++;
            }
        }

        // cannot have 0 send mail addresses
        if ($sendMailCount <= 0) {
            foreach ($sendMailFields as $fieldName) {
                if ($form->has($fieldName)) {
                    $form->get($fieldName)->get("send_mail")->addError(
                        new FormError("You must select one address as your preferred mailing address.")
                    );
                }
            }
            return false;
        }

        // cannot have more then 1 send mail address
        if ($sendMailCount > 1) {

            // unselect addresses not submitted in form
            foreach ($sendMailFields as $fieldName) {
                if (!$form->has($fieldName) || !$form->get($fieldName)->get("send_mail")->getData()) {
                    $addresses = $memberApi->get("addresses");
                    foreach ($addresses as $key=>$address) {
                        if ("address_" . $address['type'] == $fieldName) {
                            $addresses[$key]['action'] = 'U';
                            $addresses[$key]['send_mail'] = "";
                            break;
                        }
                    }

                    $memberApi->set("addresses", $addresses);
                }
            }

            // retry send mail count
            $sendMailCount = 0;
            foreach ($memberApi->get("addresses") as $address) {
                if (isset($address['send_mail']) && $address['send_mail']) {
                    $sendMailCount++;
                }
            }

            // if still more then one report an error
            if ($sendMailCount > 1) {
                foreach ($sendMailFields as $fieldName) {
                    if ($form->has($fieldName) && $form->get($fieldName)->get("send_mail")->getData()) {
                        $form->get($fieldName)->get("send_mail")->addError(
                            new FormError("You cannot select more the one preferred mailing address.")
                        );
                    }
                }
                return false;
            }
        }

        // determine address actions
        if (!$new_member && $this->memberService->getCurrentMember()) {
            $priorApiData = $this->memberService->getCurrentMember()->getApiData();
        } else {
            $priorApiData = new ParameterBag();
        }
        $addresses = $memberApi->get("addresses");
        foreach ($addresses as $key=>$address) {
            $priorAddresses = $priorApiData->get("addresses");

            // assume adding/updating address if nothing else
            $addressUpdateAction = 'U';

            // address is not in use
            if (
                ($address['type'] == "home" && !array_key_exists('line1', $address)) ||
                ($address['type'] != "home" && !array_key_exists('line3', $address)) ||
                !array_key_exists('state', $address) || 
                !array_key_exists('city', $address) || 
                !array_key_exists('zip', $address)
            ) {
                $addressUpdateAction = "";

            // check if important values have been removed, assume deletion
            } else if (
                ($address['type'] == "home" && !$address['line1']) ||
                ($address['type'] != "home" && !$address['line3']) ||
                !$address['state'] ||
                !$address['city'] || 
                !$address['zip']
            ) {
                $addressUpdateAction = 'D';
            }

            // delete if new member
            if ($new_member && ($addressUpdateAction == "D" || $addressUpdateAction == "")) {
                unset($addresses[$key]);
                continue;
            }

            $addresses[$key]['action'] = $addressUpdateAction;
        }
        $memberApi->set("addresses", array_values($addresses));

        // attempt to update API
        if (!$new_member) {
            try {
                
                foreach ($updateEndpoints as $endpoint) {
                    $memberApi->update($endpoint, false);
                }

            } catch (ApiResponseException $e) {
                if (ProjectApi::DEBUG) {
                    throw new MemberFormException(
                        "danger",
                        get_class($e) . ": " . $e->getMessage()
                    );
                } else {
                    throw new MemberFormException(
                        "danger",
                        "Unable to update your data."
                    );
                }
            }

            return $memberApi->get("id");

        // new member
        } else {

            // clear action from addresses
            $addresses = $memberApi->get("addresses");
            foreach ($addresses as $key=>$address) {
                if (array_key_exists("action", $address)) {
                    unset($addresses[$key]['action']);
                }
            }

            // push to api
            $memberResults = MemberApiEndpoint::create(
                $this->memberService->getApiService(),
                $memberApi->all()
            );
            if ($memberResults) {
                return $memberResults->get("id");
            }
        }

        return null;

    }

    /**
     * Get API endpoint for given field
     */
    private function getEndpointField($field, $form)
    {

        // get field config
        $fieldConfig = array();
        if (method_exists($form, "getCustomFieldAttributes")) {
            $fieldConfig = $form->getCustomFieldAttributes($field);
        }

        $apiEndpoint = array();
        if ($fieldConfig && $fieldConfig->has("mlaapi")) {
            $apiEndpoint = explode("/", $fieldConfig->get("mlaapi"));
            if (count($apiEndpoint) < 1) {
                return false;
            }
        } elseif ($field->getParent()) {

            $fieldConfig = array();
            if (method_exists($form, "getCustomFieldAttributes")) {
                $fieldConfig = $form->getCustomFieldAttributes($field->getParent());
            }            

            if (!$fieldConfig) {
                return false;
            }
            $apiEndpoint = explode("/", $fieldConfig->get("mlaapi"));

            if (count($apiEndpoint) < 1) {
                return false;
            }     
        }

        return $apiEndpoint;
    }

}