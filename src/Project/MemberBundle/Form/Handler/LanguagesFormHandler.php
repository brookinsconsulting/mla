<?php

namespace Project\MemberBundle\Form\Handler;
use Symfony\Component\Form\Form;
use ThinkCreative\FormBundle\Classes\Form as ThinkCreativeForm;
use Project\ApiBundle\Exception\ApiResponseException;
use Project\MemberBundle\Form\Handler\FormHandlerInterface;
use Project\MemberBundle\Services\Member;
use Project\MemberBundle\Exception\MemberFormException;

/**
 * Languages Form Handler
 * Handles retriving and posting of API member language data.
 */
class LanguagesFormHandler implements FormHandlerInterface
{

    /**
     * Member service
     * @var Project\MemberBundle\Services\Member
     */
    protected $memberService;

    /**
     * Constructor.
     * @param Project\MemberBundle\Services\Member $member_service
     */
    public function __construct(Member $member_service)
    {
        $this->memberService = $member_service;
    }

    /**
     * {@inheritdoc}
     */
    public function populate(Form $form)
    {

        // get member 
        $memberApi = $this->memberService->getCurrentMember()->getApiData();

        // iterate fields
        foreach (ThinkCreativeForm::getFieldList($form) as $field) {

            switch ($field->getName()) {
                case "other":
                    $languages = $memberApi->get("languages");
                    $languageList = array();
                    foreach ($languages as $language) {
                        if ($language['primary']) {
                            continue;
                        }
                        if (array_key_exists('lang_code', $language)) {
                            $languageList[] = $language['lang_code'];    
                        } else {
                            $languageList[] = $language['code'];
                        }
                    }
                    $field->setData($languageList);
                    break;
                case "primary":

                    $languages = $memberApi->get("languages");
                    foreach ($languages as $language) {
                        if (!$language['primary']) {
                            continue;
                        }

                        if (array_key_exists('lang_code', $language)) {
                            $field->setData($language['lang_code']);
                        } else {
                            $field->setData($language['code']);
                        }
                        
                    }
                    break;
            }
        }

    }

    /**
     * {@inheritdoc}
     */
    public function submit(Form $form)
    {

        // get member 
        $memberApi = $this->memberService->getCurrentMember()->getApiData();

        // array of languages
        $languages = array();

        // iterate fields
        foreach (ThinkCreativeForm::getFieldList($form) as $field) {

            // primary language
            if ($field->getName() == "primary" && $field->getData()) {
                $languages[$field->getData()] = array("lang_code" => (string) $field->getData(), "primary" => "Y");

            // selected language
            } else if ($field->getName() == "other") {

                $fieldData = $field->getData();
                if (gettype($fieldData) != "array") {
                    $fieldData = array($fieldData);
                }

                foreach ($fieldData as $item) {
                    if (!isset($languages[$item])) {
                        $languages[$item] = array("lang_code" => (string) $item, "primary" => "");
                    }
                }
            }

        }

        $memberApi->set("languages", $languages ? array_values($languages) : "null");
        $memberApi->update("languages");

    }

}