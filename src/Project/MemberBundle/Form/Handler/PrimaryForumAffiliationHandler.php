<?php

namespace Project\MemberBundle\Form\Handler;
use Symfony\Component\Form\Form;
use ThinkCreative\FormBundle\Classes\Form as ThinkCreativeForm;
use Project\ApiBundle\Exception\ApiResponseException;
use Project\MemberBundle\Form\Handler\FormHandlerInterface;
use Project\MemberBundle\Services\Member;
use Project\ApiBundle\Services\Api as ApiService;
use Project\MemberBundle\Exception\MemberFormException;

/**
 * Primary Forum Affiliation Form Handler
 */
class PrimaryForumAffiliationHandler implements FormHandlerInterface
{

    /**
     * Api service
     * @var Project\ApiBundle\Services\Api
     */
    protected $apiService;

    /**
     * Member service
     * @var Project\MemberBundle\Services\Member
     */
    protected $memberService;

    /**
     * Constructor.
     */
    public function __construct(ApiService $apiService, Member $memberService)
    {
        $this->apiService = $apiService;
        $this->memberService = $memberService;
    }

    /**
     * {@inheritdoc}
     */
    public function populate(Form $form)
    {

        // get member 
        $memberApi = $this->memberService->getCurrentMember()->getApiData();

        // set organizations
        $currentOrganizations = $memberApi->get("organizations");
        $currentOrganizationIds = array();
        foreach ($currentOrganizations as $organization) {
            $currentOrganizationIds[] = $organization["id"];
        }
        $form->get("primary_forum_selection")->get("forums")->setData($currentOrganizationIds);
    }

    /**
     * {@inheritdoc}
     */
    public function submit(Form $form)
    {

        // get member 
        $memberApi = $this->memberService->getCurrentMember()->getApiData();

        // get list of convention codes (as opposed to list of ids)
        $organizationList = null;
        $forumConventionCodes = array();

        foreach ($form->get("primary_forum_selection")->get("forums")->getData() as $item) {

            // check if itme is a convention or id
            // convention codes should not be numeric
            if (!is_numeric(trim($item))) {
                $forumConventionCodes[] = trim($item);
                continue;
            }

            // get organization list
            if (!$organizationList) {
                $organizationList = $this->apiService->getOrganizations()->getOrganizations(null, "forum");
            }

            // iterate and match id with convention code
            foreach ($organizationList as $organization) {
                if (trim($organization["id"]) == trim($item)) {
                    $forumConventionCodes[] = trim($organization["convention_code"]);
                    break;
                }
            }
        }

        // get current organizations
        $currentOrganizations = $memberApi->get("organizations");
        $currentPrimaryList = false;
        foreach ($currentOrganizations as $organization) {
            if (trim(strtolower($organization["primary"])) == "n" || trim($organization["primary"]) == "") {
                continue;
            }
            $currentPrimaryList[] = $organization["convention_code"];
        }
        if ($currentPrimaryList) {
            $this->memberService->deleteMemberOrganizations(
                $this->memberService->getCurrentMember(),
                $currentPrimaryList,
                true
            );
        }

        // add new organizations
        if (!$forumConventionCodes) {
            return;
        }
        $this->memberService->postMemberOrganizations(
            $this->memberService->getCurrentMember(),
            $forumConventionCodes,
            true
        );


    }

}
