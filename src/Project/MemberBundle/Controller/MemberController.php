<?php

namespace Project\MemberBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;
use ThinkCreative\FormBundle\Classes\Form as ThinkCreativeForm;
use ThinkCreative\FormBundle\Exception\FormNotFoundException;
use Project\ApiBundle\Endpoints\Member as MemberEndpoint;
use Project\ApiBundle\Exception\ApiResponseException;
use Project\MemberBundle\Exception\MemberFormException;
use Project\MemberBundle\Classes\Member as MemberObject;
use Project\MemberBundle\Classes\OrcidApi;

/**
 * Member Controller
 * Contains actions for MLA Members
 */
class MemberController extends Controller
{

    /**
     * Siteaccess or member section
     * @var string
     */
    const MEMBER_SITE_ACCESS = "site";

    /**
     * Min time in seconds user has to wait 
     * before resending email verification
     * @var interger
     */
    const EMAIL_RESEND_TIME = 30;

    /**
     * Secret salt for verification hash
     * @var string
     */
    const SECRET = "tVPa=d?V;t4h&h2C%fdB19J/aftV@qm+re;S#?F4";


    /**
     * Action for displaying main member page (My MLA)
     */
    public function indexAction(Request $request)
    {

        // create response
        $response = new Response();

        // get request and check siteaccess
        $request = $this->get("request");
        if ($request->attributes->get("siteaccess") && $request->attributes->get("siteaccess")->name != self::MEMBER_SITE_ACCESS) {
            return $this->redirect(
                $this->get("ezpublish.chain_router")->generate(
                    "UserMain",
                    array(
                        "siteaccess" => self::MEMBER_SITE_ACCESS
                    ),
                    true
                ),
                $response
            );
        }

        // get member service
        $memberService = $this->get("project.member.member");

        // get member
        $member = $memberService->getCurrentMember();

        // make sure user isn't department member
        if ($member && $member->getMemberStatus() == MemberObject::MEMBER_STATUS_DEPARTMENT)  {
            throw new AccessDeniedException("Must be a MLA member to access this section.");
        }

        // make sure user isn't a 'joining' member
        if ($member && $member->getMemberStatus() == MemberObject::MEMBER_STATUS_JOIN) {
            return $this->redirect(
                $this->generateUrl("JoinIndex")
            );            
        }

        // check current user type
        if (!$member || $member->getMemberStatus() == MemberObject::MEMBER_STATUS_EZUSER) {
            throw new AccessDeniedException("Must be a member to access this section.");
        }

        // cache
        $memberData = $member->getApiData()->all();
        unset($memberData['authentication']['password']);
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->setVary( 'X-User-Hash' );
        $response->setETag(
            !$member ?
                hash(
                    "sha256",
                    $request->getClientIp() . $request->getSession()->getId()
                ) :
                hash(
                    "sha256",
                    $request->getClientIp() .
                    $request->getSession()->getId() .
                    $member->getMemberApiId() .
                    $member->getEzUserId() .
                    json_encode($memberData)
                )
        );
        if ($response->isNotModified($request)) {
            return $response;
        }

        // get member receipts
        $lookupYears = array();
        if (is_array($member->getApiData()->get("dues_history"))) {
            foreach ($member->getApiData()->get("dues_history") as $dues) {
                if ($dues['membership_year'] && !in_array(intval($dues['membership_year']), $lookupYears)) {
                    $lookupYears[] = intval($dues['membership_year']);
                }
            }
        }

        if (is_array($member->getApiData()->get("contributions_history"))) {
            foreach ($member->getApiData()->get("contributions_history") as $contribution) {
                if ($contribution['contribution_year'] && !in_array(intval($contribution['contribution_year']), $lookupYears)) {
                    $lookupYears[] = intval($contribution['contribution_year']);
                }
            }
        }

        $receipts = array();
        sort($lookupYears, SORT_NUMERIC);
        foreach ($lookupYears as $year) {
            $receipts[] = $memberService->getMemberReceipt($member, $year);
        }

        return $this->render(
            "ProjectMemberBundle:full:mymla/main.html.twig",
            array(
                "member" => $member,
                "receipts" => $receipts
            ),
            $response
        );
    }

    /**
     * Display a member form
     */
    public function formAction(Request $request)
    {

        // create response
        $response = new Response();

        // get member service
        $memberService = $this->get("project.member.member");

        // get member
        $member = $memberService->getCurrentMember();

        // check current user type
        if (!$member || $member->getMemberStatus() == MemberObject::MEMBER_STATUS_EZUSER) {
            throw new AccessDeniedException("Must be a member to access this section.");
        }

        // cache
        $memberData = $member->getApiData()->all();
        unset($memberData['authentication']['password']);
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->setVary( 'X-User-Hash' );
        $response->setETag(
            !$member ?
                hash(
                    "sha256",
                    $request->getClientIp() . $request->getSession()->getId()
                ) :
                hash(
                    "sha256",
                    $request->getClientIp() .
                    $request->getSession()->getId() .
                    $member->getMemberApiId() .
                    $member->getEzUserId() .
                    json_encode($memberData)
                )
        );
        if ($response->isNotModified($request)) {
            return $response;
        }   

        // get form name
        $form_name = $this->get("request")->query->get("form");

        // get form
        $form = $this->get("think_creative_form.form_service")->getForm("member/{$form_name}");

        // get handler
        $config = $form->getCustomAttributes();
        if ($config->get("handler")) {
            $handler = $this->get($config->get("handler"));
        } else {
            $handler = $this->get("project.member.form_handler.member");
        }

        // populate form
        if (!$form->isSubmitted()) {
            $handler->populate($form);

        // submit form
        } else if (
            $form->isSubmitted() && 
            (
                $form->isValid() ||
                (
                    $form->has($form_name) &&
                    $form->get($form_name)->isValid()
                )
            )
        ) {

            // do submit
            $handler->submit($form);

            // return success template
            if ($form->has($form_name) && $form->get($form_name)->isValid()) {

                return $this->render(
                    "ProjectMemberBundle:full:mymla/formsuccess.html.twig",
                    array(
                        "form_name" => $form_name
                    ),
                    $response
                );
            }
        }

        // get template
        return $this->render(
            "ProjectMemberBundle:full:mymla/form.html.twig",
            array(
                "member" => $member->getApiData(),
                "form_name" => $form_name,
                "form" => $form->createView(),
                "has_login" => true
            ),
            $response
        );

    }

    /**
     * Presents a member form in a read only state
     */
    public function formDataReadAction(Request $request)
    {

        // create response
        $response = new Response();

        // get form name
        $form_name = $request->query->get("form");

        // get member service
        $memberService = $this->get("project.member.member");

        // get member
        $member = $memberService->getCurrentMember();

        // check current user type
        if (!$member || $member->getMemberStatus() == MemberObject::MEMBER_STATUS_EZUSER) {
            throw new AccessDeniedException("Must be a member to access this section.");
        }

        // cache
        $memberData = $member->getApiData()->all();
        unset($memberData['authentication']['password']);
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->setVary( 'X-User-Hash' );
        $response->setETag(
            !$member ?
                hash(
                    "sha256",
                    $request->getClientIp() . $request->getSession()->getId()
                ) :
                hash(
                    "sha256",
                    $request->getClientIp() .
                    $request->getSession()->getId() .
                    $member->getMemberApiId() .
                    $member->getEzUserId() .
                    json_encode($memberData)
                )
        );
        if ($response->isNotModified($request)) {
            return $response;
        }
        
        // build response with api data if template is available
        if ($this->get("templating")->exists("ProjectMemberBundle:parts:member/read/{$form_name}.html.twig")) {
            return $this->render(
                "ProjectMemberBundle:parts:member/read/{$form_name}.html.twig",
                array(
                    "form_name" => $form_name,
                    "member" => $member->getApiData(),
                    "ranks" => $this->get("project.member.form_populator.career_path_survey.rank")->execute(),
                    "languages" => $this->get("project.member.form_populator.languages")->execute(),
                    "forum_organizations" => $this->get("project.api.api")->getOrganizations()->getOrganizations(null, "forum")
                ),
                $response
            );
        }

        // get form
        $form = $this->get("think_creative_form.form_service")->getForm("member/{$form_name}");

        // get handler
        $config = $form->getCustomAttributes();
        if ($config->get("handler")) {
            $handler = $this->get($config->get("handler"));
        } else {
            $handler = $this->get("project.member.form_handler.member");
        }

        // populate
        $handler->populate($form);

        // iterate fields create output
        $outputVars = array(
            "prefs" => array(),
            "attrs" => array()
        );
        foreach (ThinkCreativeForm::getFieldList($form) as $field) {
            if (!in_array(
                $field->getConfig()->getType()->getName(),
                array(
                    "text",
                    "choice",
                    "checkbox",
                    "radio"
                )
            )) {
                continue;
            }
            if (is_bool($field->getData())) {
                $outputVars['prefs'][] = array(
                    "name" => $field->getName(),
                    "label" => $field->getConfig()->getOption("label", $field->getName()),
                    "value" => $field->getData()
                );
            } elseif (!is_array($field->getData())) {
                $outputVars['attrs'][] = array(
                    "name" => $field->getName(),
                    "label" => $field->getConfig()->getOption("label", $field->getName()),
                    "value" => $field->getData()
                );
            }
        }

        // get template
        return $this->render(
            "ProjectMemberBundle:parts:member/read/default.html.twig",
            array(
                "form_name" => $form_name,
                "values" => $outputVars
            ),
            $response
        );

    }

    /**
     * Action for displaying a receipt
     * @param integer $year
     */
    public function receiptAction($year)
    {

        // create response
        $response = new Response();

        // get member service
        $memberService = $this->get("project.member.member");

        // get current logged in member
        $member = $memberService->getCurrentMember();

        // no current member
        if (!$member) {
            throw new AccessDeniedException("Must be logged in to access this data.");
        }

        // cache
        $response->setPrivate();
        $response->setVary( 'X-User-Hash' );
        $response->setMaxAge(28800); // 8 hours -- content should not change
        $response->setSharedMaxAge(28800);
        if ($response->isNotModified($request)) {
            return $response;
        }

        // get api data
        $memberApiData = $member->getApiData();

        // render receipt
        return $this->render(
            "ProjectMemberBundle:full:mymla/receipt.html.twig",
            array(
                "receipt" => $memberService->getMemberReceipt($member, $year),
                "member" => $memberApiData,
            )
        );

    }

    /**
     * Renders login form that appears in top navigation
     */
    public function loginEmbedAction(Request $request, $lastUsername = "", $targetPath = "")
    {

        // create response
        $response = new Response();

        // get member service
        $memberService = $this->get("project.member.member");

        // get current member
        $member = $memberService->getCurrentMember();

        // caching
        $response->setPrivate();
        $response->setVary( 'X-User-Hash' );
        $response->setSharedMaxAge(0);
        $response->setETag(
            md5(
                Request::createFromGlobals()->getPathInfo() . 
                $lastUsername . $targetPath . ($member ? $member->getMemberApiId() : 0)
            )
        );
        if ($response->isNotModified($request)) {
            return $response;
        }

        if (
            $member && 
            !(
                $request->get("siteaccess")->name == self::MEMBER_SITE_ACCESS &&
                $member->getMemberStatus() == MemberObject::MEMBER_STATUS_DEPARTMENT
            )
        ) {  
            return $this->render(
                "ProjectMemberBundle:embed:misc/login.html.twig",
                array(
                    "member" => $member,
                    "member_access_level" => $member->getAccessLevel(),
                    "MEMBER_STATUS_JOIN" => MemberObject::MEMBER_STATUS_JOIN,
                    "MEMBER_STATUS_EZUSER" => MemberObject::MEMBER_STATUS_EZUSER,
                    "MEMBER_STATUS_DEPARTMENT" => MemberObject::MEMBER_STATUS_DEPARTMENT
                ),
                $response
            );
        }

        // return login form
        return $this->render(
            "ProjectMemberBundle:embed:misc/login.html.twig",
            array(
                "last_username" => $lastUsername,
                "current_uri" => $targetPath ?: Request::createFromGlobals()->getUri(),
                "csrf_token" => $this->get("form.csrf_provider")->generateCsrfToken( 'authenticate' ),
                "orcid_auth_url" => OrcidApi::AUTH_URL,
                "orcid_client_id" => OrcidApi::CLIENT_ID,
                "view" => "embed"
            ),
            $response
        );

    }

    /**
     * Username retrival / password reset page.
     */
    public function accountRetrivalAction()
    {

        // create response
        $response = new Response();

        // cache -- none
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);

        // get email confirmation form
        $form = $this->get("think_creative_form.form_service")->getForm("account_retrival");

        // get account retrival data from session
        $accountRetrivalStatus = $this->get("session")->get("accountRetrival");

        // disable resending if last email was sent less then EMAIL_RESEND_TIME seconds ago
        if ($form->isSubmitted() && $form->isValid()) {

            // check last email send time
            if (
                isset($accountRetrivalStatus['time']) && 
                time() - $accountRetrivalStatus['time'] < self::EMAIL_RESEND_TIME
            ) {
                $form->addError( new FormError( "You must wait at least ". self::EMAIL_RESEND_TIME ." seconds before retrying." ) );
            }

            // lookup member data
            if ($form->isValid()) {

                // look up member data
                try {

                    $member = MemberEndpoint::find(
                        $this->get("project.api.api"),
                        array(
                            "email" => $form->get("email")->getData(),
                            "membership_status" => "all"
                        )
                    );

                    if ($member['data'][0]['total_num_results'] == 0) {
                        $form->addError( new FormError( "The provided email address was not found in our system." ) );
                    } else {

                        // get member service
                        $memberService = $this->get("project.member.member");

                        // get member object
                        $member = $memberService->getMemberFromApiId(abs($member['data'][0]['search_results'][0]['id']));

                        // determine what action to perform
                        if ($form->get("buttons")->get("password_reset")->isClicked()) {
                            $this->sendPasswordResetEmail($member);

                        } elseif ($form->get("buttons")->get("username_retrival")->isClicked()) {
                            $this->sendUsernameEmail($member);
                        }

                    }

                // email not found
                } catch (ApiResponseException $e) {
                    if ($e->getStatusCode() == "API-2102") {
                        $form->addError( new FormError( "The provided email address was not found in our system." ) );
                    } else {
                        $form->addError( new FormError( "Unable to process request. Please try again later." ) );
                    }
                }

            }

        }

        return $this->render(
            "ProjectMemberBundle:full:misc/account_retrival.html.twig",
            array(
                "form" => $form->createView(),
                "password_email_sent" => $form->isSubmitted() && $form->isValid() && $form->get("buttons")->get("password_reset")->isClicked(),
                "username_email_sent" => $form->isSubmitted() && $form->isValid() && $form->get("buttons")->get("username_retrival")->isClicked()
            ),
            $response
        );

    }

    /**
     * Send out email with username in it
     * @param MemberObject $member
     */
    private function sendUsernameEmail(MemberObject $member)
    {

        // get member api data
        $memberApiData = $member->getApiData();

        // store email send time to session
        $accountRetrivalStatus = $this->get("session")->get("accountRetrival");
        $accountRetrivalStatus['time'] = time();
        $this->get("session")->set("accountRetrival", $accountRetrivalStatus);

        // send email
        $message = \Swift_Message::newInstance()
            ->setSubject("MLA Username Retrival")
            ->setFrom("membership@mla.org")
            ->setTo($memberApiData->get("general[email]", null, true))
            ->setBody(
                $this->renderView(
                    "ProjectMemberBundle:email:misc/username_retrival.txt.twig",
                    array(
                        "member" => $memberApiData
                    )
                )
            )
        ;
        $this->get("mailer")->send($message);
    }


    /**
     * Send out password reset email. Generated code is stored to the
     * session.
     * @param MemberObject $member
     */
    private function sendPasswordResetEmail(MemberObject $member)
    {

        // get member api data
        $memberApiData = $member->getApiData();

        // generate reset code
        $hash = hash("sha256", $memberApiData->get("general[last_name]", null, true) . $memberApiData->get("id") . $memberApiData->get("general[email]", null, true) . self::SECRET . time());

        // store hash to session
        $accountRetrivalStatus = $this->get("session")->get("accountRetrival");
        $accountRetrivalStatus['hash'] = $hash;
        $accountRetrivalStatus['member_id'] = $memberApiData->get("id");
        $accountRetrivalStatus['time'] = time();
        $this->get("session")->set("accountRetrival", $accountRetrivalStatus);

        // send email
        $message = \Swift_Message::newInstance()
            ->setSubject("MLA Password Reset")
            ->setFrom("membership@mla.org")
            ->setTo($memberApiData->get("general[email]", null, true))
            ->setBody(
                $this->renderView(
                    "ProjectMemberBundle:email:misc/password_reset.txt.twig",
                    array(
                        "memberName" => ($memberApiData->get("general[title]", null, true) ? $memberApiData->get("general[title]", null, true) . " " : "") . $memberApiData->get("general[last_name]", null, true),
                        "hash" => $hash
                    )
                )
            )
        ;
        $this->get("mailer")->send($message);

    }

    /**
     * Password reset page
     */
    public function passwordResetAction($code = "")
    {

        // create response
        $response = new Response();

        // get code value from query param
        if ($this->getRequest()->query->has("code")) {
            $code = strtolower(trim($this->getRequest()->query->get("code")));
        }

        // get hash from session
        $accountRetrivalStatus = $this->get("session")->get("accountRetrival");

        // cache
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->setETag($code);
        if ($response->isNotModified($request)) {
            return $response;
        }
        
        // verify code
        if ($code == $accountRetrivalStatus['hash']) {

            // get password reset form
            $form = $this->get("think_creative_form.form_service")->getForm("password_reset");

            // update password
            if ($form->isSubmitted() && $form->isValid()) {

                try {

                    // update password via api
                    $this->get("project.api.api")->request(
                        "members/".abs($accountRetrivalStatus['member_id'])."/authentication",
                        "PUT",
                        array(),
                        array(
                            "password" => $form->get("password")->getData()
                        )
                    );

                    // remove code from session
                    $accountRetrivalStatus['hash'] = "";
                    $accountRetrivalStatus['member_id'] = null;
                    $this->get("session")->set("accountRetrival", $accountRetrivalStatus);

                } catch (ApiResponseException $e) {
                    $form->addError( new FormError( "Unable to process request. Please try again later." ) );
                }

            }

        }

        return $this->render(
            "ProjectMemberBundle:full:misc/password_reset.html.twig",
            array(
                "form" => isset($form) ? $form->createView() : null,
                "valid_code" => $code == $accountRetrivalStatus['hash'],
                "reset_success" => isset($form) ? ($form->isValid() && $form->isSubmitted()) : false
            ),
            $response
        );

    }

    /**
     * Member form custom tag
     */
    public function memberFormCustomTagAction($template, $variables)
    {

        // create response
        $response = new Response();

        // get member service
        $memberService = $this->get("project.member.member");

        // get current logged in member
        $member = $memberService->getCurrentMember();

        // send blank response if not a member
        if (!$member || $member->getMemberStatus() != MemberObject::MEMBER_STATUS_FULL) {
            return new Response("");
        }

        // cache
        $memberData = $member->getApiData()->all();
        unset($memberData['authentication']['password']);
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->setVary( 'X-User-Hash' );
        $response->setETag(
            !$member ?
                hash(
                    "sha256",
                    $request->getClientIp() . $request->getSession()->getId()
                ) :
                hash(
                    "sha256",
                    $request->getClientIp() .
                    $request->getSession()->getId() .
                    $member->getMemberApiId() .
                    $member->getEzUserId() .
                    json_encode($memberData)
                )
        );
        if ($response->isNotModified($request)) {
            return $response;
        }

        // get form
        $form = $this->get("think_creative_form.form_service")->getFormFromPath($variables['form']);

        // get handler
        if ($form->getCustomAttributes()->get("handler")) {
            $formHandler = $this->get($form->getCustomAttributes()->get("handler"));

            // submitted and valid
            if ($form->isSubmitted() && $form->isValid()) {
                $formHandler->submit($form);

            // not submitted, populate
            } else {
                $formHandler->populate($form);
            }

        }

        return $this->render(
            $template,
            array(
                "content" => $variables['content'],
                "form" => $form->createView()
            ),
            $response
        );
    }

}