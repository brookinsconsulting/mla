<?php

namespace Project\MemberBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Project\MemberBundle\Form\Validator\Constraints\UsernameValidator;
use Project\MemberBundle\Form\Validator\Constraints\PasswordValidator;
use Project\MemberBundle\Form\Validator\Constraints\DuplicateEmailValidator;
use Project\MemberBundle\Form\Validator\Constraints\MemberIdValidator;
use Project\MemberBundle\Controller\MemberJoinController;
use Project\ApiBundle\Exception\ApiResponseException;

/**
 * Ajax Request Controller
 * Contains actions for validating form elements with ajax
 */
class AjaxRequestController extends Controller
{

    /**
     * Check membership category promo code
     */
    public function validateMemberCategoryPromoCodeAction()
    {
        $request = $this->get("request");
        $promoCode = urldecode($request->request->get("value"));
        if (!$promoCode) {
            return new Response(json_encode(array("message" => "", "valid" => true)));
        }

        $joinRenewConfiguration = $this->container->getParameter("ProjectMember_JoinRenew");

        $promoValues = array();
        foreach ($joinRenewConfiguration['promo_codes'] as $promo) {
            if ($promo['code'] == $promoCode) {
                $promoValues[] = $promo['value'];
            }
        }
        if ($promoValues) {
            return new Response(json_encode(array("message" => "Valid promo code.", "valid" => true, "values" => $promoValues)));
        }
        return new Response(json_encode(array("message" => "Invalid promo code.", "valid" => false)));

    }

    /**
     * Member id lookup -- Part of Step #2
     * Returns JSON response
     * @param integer $member_id
     * @return Response
     */
    public function memberLookupAction($member_id = null)
    {
        // get order
        $order = $this->get("thinkcreative.payment.order_service")->getUserOrder(
            $this->get("project.product.order_processor"), 
            "membership"
        );
        $memberData = $order->getCustomerDetails();

        // must be past join/renew step 1
        if (
            !isset($memberData['firstName']) ||
            !$memberData['firstName'] ||
            !isset($memberData['lastName']) ||
            !$memberData['lastName'] ||
            !isset($memberData['email']) ||
            !$memberData['email']
        ) 
        {
            return new Response("");
        }

        // get api
        $api = $this->get("project.api.api");

        // perform validation
        $memberResponse = null;
        if (!MemberIdValidator::validateInteger($member_id)) {
            $memberResponse = array(
                "status" => "error",
                "message" => "Member ID must be a numeric value."
            );
        }

        elseif (!MemberIdValidator::validateRange($member_id)) {
            $memberResponse = array(
                "status" => "error",
                "message" => "Member ID must be greater than 0."
            );
        } else {
            // lookup member
            try {
                $member = $api->getMember($member_id);
                $memberGeneral = $member->get("general");
                $memberResponse = array(
                    "status" => "success",
                    "message" => "Valid.",
                    "data" => array(
                        "first_name" => $memberGeneral['first_name'],
                        "last_name" => $memberGeneral['last_name']
                    )
                );
            } catch (ApiResponseException $e) {
                $memberResponse = array(
                    "status" => "error",
                    "message" => "The member ID you entered was invalid."
                );
            }
        }

        return new Response(json_encode($memberResponse));
    }

    /**
     * Department lookup -- Used in Step #4
     * Returns JSON response
     * @param string $zip_code
     * @return Response
     */
    public function departmentLookupAction()
    {
        $zip_code = $this->getRequest()->query->get("zip_code");
        try {
            return new Response(json_encode($this->get("project.member.form_populator.departments")->execute($zip_code)));
        } catch (ApiResponseException $e) {
            return new Response(json_encode(array()));
        }
    }

}