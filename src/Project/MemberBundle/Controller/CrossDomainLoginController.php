<?php

namespace Project\MemberBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use eZ\Publish\Core\MVC\Symfony\Security\User;
use Project\MemberBundle\Services\CrossDomainToken;

/**
 * Handles cross domain login request
 */
class CrossDomainLoginController extends Controller
{

    /**
     * Return list of all domains used by this site.
     * Uses siteaccess host map config.
     * @return array
     */
    private function getCrossDomainList(Request $request = null)
    {

        // get site access match config
        $siteAccessMatchConfig = $this->container->getParameter("ezpublish.siteaccess.match_config");
        if (!isset($siteAccessMatchConfig['Map\\Host'])) {
            return array();
        }

        // generate a list of cross domain site accesses from host map
        $hostList = array();
        foreach ($siteAccessMatchConfig['Map\\Host'] as $host => $siteaccess) {
            if (!in_array($siteaccess, CrossDomainToken::$validSiteAccesses)) {
                continue;
            }

            if ($request && $request->getHost() == $host) {
                continue;
            }

            $domain = explode(".", trim($host));
            if (count($domain) < 2) {
                continue;
            }
            $domain = $domain[count($domain) - 2] . "." . $domain[count($domain) - 1];
            if (in_array($domain, CrossDomainToken::$validDomains)) {
                $hostList[] = $host;
            }
        }

        return $hostList;
    }


    /**
     * Process cross domain request token, login if valid, return
     * a valid response based on given format type
     */
    public function indexAction($format = "js")
    {

        // get request
        $request = $this->get("request");

        // get token
        $token = $request->query->get("token");

        // no token provided
        if (!$token) {
            throw new AccessDeniedHttpException("Cross domain login token not provided.");
        }

        // get cross domain authentication provider
        $crossDomainTokenService = $this->get("project.member.cross_domain_authentication");

        // ensure token is valid
        if (!$crossDomainTokenService->isTokenValid($token)) {
            throw new AccessDeniedHttpException("Cross domain login token is not valid.");
        }

        // load ezuser for member
        $ezUser = $crossDomainTokenService->getMemberFromToken($token)->getEzUser();

        // must have valid ezUser
        if (!$ezUser) {
            throw new AccessDeniedHttpException("Invalid member type.");
        }

        // user roles
        $roles = array('ROLE_USER');

        // create user token
        $user = new User($ezUser, $roles);
        $user->setAPIUser($ezUser);
        $authToken = new UsernamePasswordToken(
            $user,
            null,
            "ezpublish_front",
            $roles
        );
        $this->get("security.context")->setToken($authToken);

        $event = new InteractiveLoginEvent($request, $authToken);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        // format to return a response in
        // request will be sent from remote site via image/script/iframe/etc
        switch($format)
        {
            case "png":
                $response = new Response(base64_decode("iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNg+P+/HgAFfwJ+BSYS9wAAAABJRU5ErkJggg=="));
                $response->headers->set("Cache-Control", "max-age=0");
                $response->headers->set("Content-Type", "image/png");
                $response->sendHeaders();
                break;

            case "js":
            default:
                $response = new Response("var crossDomainLoginSuccess_" . $this->get("request")->get("siteaccess")->name . " = true;" );
                $response->headers->set("Cache-Control", "max-age=0");
                $response->headers->set("Content-Type", "text/plain");
                $response->sendHeaders();
                break;
        }

        // add cookie to response to signify that cross domain login was sent
        $response->headers->setCookie(
            new Cookie(
                CrossDomainToken::CROSS_DOMAIN_COOKIE_NAME,
                true,
                time() + CrossDomainToken::CROSS_DOMAIN_COOKIE_EXPIRE
            )
        );
        $response->sendHeaders();

        return $response;
    }

    /**
     * Generate tags to embed on a page that will log the user
     * in on all domains
     */
    public function loginEmbedAction()
    {

        // get member
        $member = $this->get("project.member.member")->getCurrentMember();

        // member not found, empty response
        if (!$member) {
            return new Response("");
        }

        // get token
        $token = $this->get("project.member.cross_domain_authentication")->generateTokenFromMember($member);

        // token not provided, empty response
        if (!$token) {
            return new Response("");
        }

        // get global request
        $request = Request::createFromGlobals();

        // request contains cross domain cookie
        if ($request->cookies->get(CrossDomainToken::CROSS_DOMAIN_COOKIE_NAME)) {
            return new Response("");
        }

        // get host map list
        $hostList = $this->getCrossDomainList($this->get("request"));

        if (!$hostList) {
            return new Response("");
        }

        // generate embed code
        $embedCode = "<div class='crossDomainEmbed'>";
        foreach ($hostList as $host) {
            $embedCode .= "<img src='https://{$host}/cross_domain_login.png?token={$token}' width='1' height='1' />";
        }
        $embedCode .= "</div>";

        // create response
        $response = new Response($embedCode);

        // add cookie to response to signify that cross domain login was sent
        $response->headers->setCookie(
            new Cookie(
                CrossDomainToken::CROSS_DOMAIN_COOKIE_NAME,
                true,
                time() + CrossDomainToken::CROSS_DOMAIN_COOKIE_EXPIRE
            )
        );
        $response->sendHeaders();

        return $response;

    }

    /**
     * Generate tags to logout a user on all domains
     */
    public function logoutEmbedAction(Request $request)
    {

        // create response
        $response = new Response();

        // caching
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);

        // get global request
        $globalRequest = Request::createFromGlobals();

        // request must contain cross domain cookie
        if (!$globalRequest->cookies->get(CrossDomainToken::CROSS_DOMAIN_COOKIE_NAME)) {
            return new Response("");
        }

        // get member
        $member = $this->get("project.member.member")->getCurrentMember();

        // member found, empty response
        if ($member) {
            return new Response("");
        }

        // get host map list
        $hostList = $this->getCrossDomainList($this->get("request"));

        if (!$hostList) {
            return new Response("");
        }

        // generate embed code
        $embedCode = "<div class='crossDomainEmbed'>";
        foreach ($hostList as $host) {
            $embedCode .= "<iframe src='https://{$host}/logout' width='1' height='1'></iframe>";
        }
        $embedCode .= "</div>";

        // create response
        $response->setContent($embedCode);

        // clear cookie
        $response->headers->clearCookie(CrossDomainToken::CROSS_DOMAIN_COOKIE_NAME);
        $response->sendHeaders();

        return $response;

    }

}