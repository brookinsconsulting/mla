<?php

namespace Project\MemberBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use eZ\Publish\API\Repository\Exceptions\NotFoundException;
use Project\ProductBundle\Order\OrderProcessorService;
use Project\ProductBundle\Services\Product as ProductService;
use Project\MemberBundle\Classes\Member as MemberObject;
use Project\MemberBundle\Classes\OrcidApi;
use Project\MemberBundle\Exception\Orcid\OrcidApiResponseException;
use Project\ApiBundle\Exception\ApiResponseException;

/**
 * ORCID Controller
 */
class OrcidController extends Controller
{

    /**
     * Handles ORCID authorization.
     */
    public function authorizeAction()
    {

        // get member service
        $memberService = $this->get("project.member.member");

        // get member
        $member = $memberService->getCurrentMember();

        // if not logged in as a member assume in join/renew
        if (!$member || $member->getMemberStatus() == MemberObject::MEMBER_STATUS_JOIN) {

            // join renew service
            $joinRenewService = $this->get("project.member.join_renew");

            // verify order has membership product
            if (!$joinRenewService->getMemberClassCode()) {
                return $this->render(
                  "ProjectMemberBundle:full:orcid/error.html.twig",
                    array(
                        "error_message" => "Unable to validate user. [Session expiration?]"
                    )
                );
            }

        }

        // get request
        $request = $this->get("request");

        // get authorization code
        $authCode = $request->query->get("code");

        // no auth code, was probably denied, close popup
        if (!$authCode) {
            return $this->render(
                "ProjectMemberBundle:full:orcid/close.html.twig"
            );
        }

        // get results
        try {
            $results = OrcidApi::getAccessToken($authCode);
        } catch (OrcidApiResponseException $e) {
            return $this->render(
                "ProjectMemberBundle:full:orcid/error.html.twig",
                array(
                    "error_message" => $e->getMessage()
                )
            );
        }
        
        // no orcid id returned
        if (!isset($results['orcid'])) {
            return $this->render(
                "ProjectMemberBundle:full:orcid/error.html.twig",
                array(
                    "error_message" => "Unable to process request"
                )
            );
        }

        // ensure orcid id isn't already in system
        $doctrineManager = $this->get("doctrine.orm.entity_manager");
        $memberRepo = $doctrineManager->getRepository('ProjectMemberBundle:Member');
        $memberFind = $memberRepo->findOneBy(
            array("orcidUserId" => $results['orcid'])
        );
        if ($memberFind) {
            return $this->render(
                "ProjectMemberBundle:full:orcid/error.html.twig",
                array(
                    "error_message" => "This ORCID account is already linked to another member."
                )
            );
        }

        // store orcid data to order
        if ((!$member || $member->getMemberStatus() == MemberObject::MEMBER_STATUS_JOIN) && isset($orderData)) {
            $joinRenewService->setOrcid($results['orcid']['orcid']);

        // store orcid id to member entity
        } elseif ($member && $member->getMemberStatus() == MemberObject::MEMBER_STATUS_FULL) {
            
            if (!$memberService->setMemberOrcid($member, $results['orcid']['orcid'])) {
                return $this->render(
                    "ProjectMemberBundle:full:orcid/error.html.twig",
                    array(
                        "error_message" => "Unable to store ORCID authorization data."
                    )
                );
            }

        }

        // render
        return $this->render(
            "ProjectMemberBundle:full:orcid/authorized.html.twig"
        );

    }

    /**
     * Handle login via ORCID
     */
    /*public function loginAction()
    {

        // get member service
        $memberService = $this->get("project.member.member");

        // get request
        $request = $this->get("request");

        // get authorization code
        $authCode = $request->query->get("code");

        // no auth code, was probably denied, close popup
        if (!$authCode) {
            return $this->render(
                "ProjectMemberBundle:full:orcid/close.html.twig"
            );
        }

        // get results
        try {
            $results = OrcidApi::getAccessToken($authCode);
        } catch (OrcidApiResponseException $e) {
            return $this->render(
                "ProjectMemberBundle:full:orcid/error.html.twig",
                array(
                    "error_message" => $e->getMessage()
                )
            );
        }
        
        // no orcid id returned
        if (!isset($results['orcid'])) {
            return $this->render(
                "ProjectMemberBundle:full:orcid/error.html.twig",
                array(
                    "error_message" => "Unable to process request"
                )
            );
        }

        // look for orcid id linked with member
        $doctrineManager = $this->get("doctrine.orm.entity_manager");
        $memberRepo = $doctrineManager->getRepository('ProjectMemberBundle:Member');
        $memberFind = $memberRepo->findOneBy(
            array("orcidUserId" => $results['orcid'])
        );
        if (!$memberFind) {
            return $this->render(
                "ProjectMemberBundle:full:orcid/login_not_linked.html.twig"
            );
        }

        // perform login
        try {
            $memberService->loginWithMemberApi(
                $memberFind->getMemberApiId(),
                $request
            );
        } catch (NotFoundException $e) {
            return $this->render(
                "ProjectMemberBundle:full:orcid/error.html.twig",
                array(
                    "error_message" => "Unable to complete the login."
                )
            );
        } catch (ApiResponseException $e) {
            return $this->render(
                "ProjectMemberBundle:full:orcid/error.html.twig",
                array(
                    "error_message" => "Unable to obtain login data."
                )
            );
        }

        // render
        return $this->render(
            "ProjectMemberBundle:full:orcid/authorized.html.twig"
        );

    }*/

}