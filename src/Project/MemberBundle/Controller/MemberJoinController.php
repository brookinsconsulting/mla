<?php

namespace Project\MemberBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use eZ\Publish\Core\MVC\Symfony\Security\User;
use eZ\Publish\Core\Base\Exceptions\NotFoundException;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion\Operator;
use Omnipay\Common\Exception\InvalidCreditCardException;
use ThinkCreative\PaymentBundle\Exception\Order\PaymentGatewayErrorException;
use ThinkCreative\PaymentBundle\Entity\Order;
use Project\MemberBundle\Services\JoinRenew;
use Project\MemberBundle\Classes\OrcidApi;
use Project\ApiBundle\Exception\ApiResponseException;
use Project\ProductBundle\Order\OrderProcessorService;
use Project\ProductBundle\Services\Product as ProductService;
use Project\ProductBundle\Exception\Api\ApiResponseException as ShopifyApiResponseException;
use Project\MemberBundle\Services\Member as MemberService;
use Project\MemberBundle\Security\Authentication\Provider\ApiMemberProvider;
use Project\MemberBundle\Exception\JoinRenew\JoinStepNotAvailableException;

/**
 * Member Join Controller
 * Contains actions for MLA member sign up
 */
class MemberJoinController extends Controller
{

    /**
     * Siteaccess or member section
     * @var string
     */
    const MEMBER_SITE_ACCESS = "site";

    /**
     * Identifier of join/renew content class
     * @var string
     */
    const JOIN_RENEW_CONTENT_CLASS = "join_renew";

    /**
     * Mapping of join step to action
     * @var array
     */
    static protected $joinStepActionMap = array(
        JoinRenew::JOIN_INTRO => "introAction",
        JoinRenew::JOIN_MEMBERSHIP_CATEGORY => "membershipCategoryAction",
        JoinRenew::JOIN_IDENTIFICATION => "identificationAction",
        JoinRenew::JOIN_CONTACT => "contactAction",
        JoinRenew::JOIN_FORUM => "forumAffiliationAction",
        JoinRenew::JOIN_CAREER_PATH => "careerPathAction",
        JoinRenew::JOIN_ADDITIONAL_OPTIONS => "additionalOptionsAction",
        JoinRenew::JOIN_MEMBERSHIP_REVIEW => "reviewAction"
    );

    /**
     * Find a join/renew content object that refers to given
     * join step and return path
     * @throws Project\MemberBundle\Exception\JoinRenew\JoinStepNotAvailableException
     * @return string
     */
    private function getPathToStep($joinStep)
    {

        if (!is_integer($joinStep)) {
            throw new \InvalidArgumentException("joinStep must be an integer.");
        }

        $joinRenewService = $this->get("project.member.join_renew");

        $criteria = array(
            new Criterion\Visibility( Criterion\Visibility::VISIBLE ),
            new Criterion\ContentTypeIdentifier( self::JOIN_RENEW_CONTENT_CLASS ),
        );
        $query = new Query(
            array(
                'criterion' => new Criterion\LogicalAnd( $criteria ),
            )
        );
        $result = $this->get("ezpublish.api.service.search")->findContent($query);

        foreach ($result->searchHits as $item) {

            if (
                !defined(
                    get_class($joinRenewService) . "::" . 
                    strtoupper(trim($item->valueObject->getFieldValue("step_id")->text))
                )
            ) {
                continue;
            }
            if (
                constant(
                    get_class($joinRenewService) . "::" . 
                    strtoupper(trim($item->valueObject->getFieldValue("step_id")->text))
                ) != $joinStep
            ) {
                continue;
            }

            return $this->get("ezpublish.chain_router")->generate(
                "ez_urlalias",
                array(
                    "locationId" => $item->valueObject->contentInfo->mainLocationId
                    //"siteaccess" => self::MEMBER_SITE_ACCESS
                ),
                true
            );

        }

        throw new JoinStepNotAvailableException("Join step #{$joinStep} is not available.");

    }

    /** 
     * Ensure user is at right step and redirects if not
     * @param integer $joinStep
     */
    private function verifyStep($joinStep)
    {

        // verify joinStep
        if (!is_integer($joinStep)) {
            throw new \InvalidArgumentException("joinStep must be an integer.");
        }

        // send to correct site access
        $request = $this->get("request");
        if ($request->attributes->get("siteaccess") && $request->attributes->get("siteaccess")->name != self::MEMBER_SITE_ACCESS) {

            $redirect = new RedirectResponse(
                $this->get("ezpublish.chain_router")->generate(
                    $this->getPathToStep($joinStep),
                    array(
                        "siteaccess" => self::MEMBER_SITE_ACCESS
                    ),
                    true
                )
            );
            $redirect->send();
            return;
        }

        // get join/renew service
        $joinRenewService = $this->get("project.member.join_renew");

        // no redirect needed from JOIN_INTRO
        if ($joinStep == JoinRenew::JOIN_INTRO) {
            return;
        }

        // full member or ezuser
        if (!$joinRenewService->isValidUser()) {
            if (
                $joinRenewService->isFullMember() &&
                in_array(
                    $joinRenewService->getJoinStep(),
                    array(
                        JoinRenew::JOIN_INTRO,
                        JoinRenew::JOIN_MEMBERSHIP_CATEGORY
                    )
                )
            ) {
                return;
            }

            $redirect = new RedirectResponse(
                $this->getPathToStep(
                    JoinRenew::JOIN_INTRO
                )
            );
            $redirect->send();
            return;
        }

        // verify that membership product is in cart if step is beyond JOIN_MEMBERSHIP_CATEGORY
        if (
            $joinRenewService->getJoinStep() > JoinRenew::JOIN_MEMBERSHIP_CATEGORY &&
            !$joinRenewService->getMemberClassCode()
        ) {
            $joinRenewService->setJoinStep(JoinRenew::JOIN_MEMBERSHIP_CATEGORY);
        }

        if (
            !$joinRenewService->isMember() &&
            $joinRenewService->getJoinStep() > JoinRenew::JOIN_CONTACT
        ) {
            $joinRenewService->setJoinStep(JoinRenew::JOIN_INTRO);
        }

        // check if joint member is valid, remove if not
        if (!$joinRenewService->hasValidJointMember()) {
            $joinRenewService->clearJointMember();
            $this->get("session")->getFlashBag()->add("join-renew-notices", "Invalid joint member");
        }

        // determine is member is at a valid step
        if ($joinRenewService->getJoinStep() >= $joinStep) {
            return;
        }

        // redirect to last valid step
        $redirect = new RedirectResponse(
            $this->getPathToStep(
                $joinRenewService->getJoinStep()
            )
        );
        $redirect->send();

    }

    /**
     * Handler for /join/ route, redirects to proper page
     */
    public function indexAction(Request $request)
    {
        $this->verifyStep(JoinRenew::JOIN_INTRO);
        return $this->redirect(
            $this->getPathToStep(
                JoinRenew::JOIN_INTRO
            )
        );
    }

    /**
     * Render join/renew location
     */
    public function joinRenewLocationAction(Request $request, $locationId, $viewType = "full", $layout = false, array $params = array())
    {

        // load content
        $location = $this->get("ezpublish.api.service.location")->loadLocation($locationId);
        $content = $this->get("ezpublish.api.service.content")->loadContent($location->contentInfo->id);

        // get join renew service
        $joinRenewService = $this->get("project.member.join_renew");

        // get join step
        $joinStep = null;
        if (defined(get_class($joinRenewService) . "::" . strtoupper(trim($content->getFieldValue("step_id")->text)))) {
            $joinStep = constant(get_class($joinRenewService) . "::" . strtoupper(trim($content->getFieldValue("step_id")->text)) );    
        }        
        $params['joinStep'] = $joinStep;
 
        // verify
        $this->verifyStep($joinStep);

        // get action for step
        $params['action'] = null;
        if (array_key_exists($joinStep, self::$joinStepActionMap)) {
            $params['action'] = preg_replace(
                '/Action$/',
                '',
                self::$joinStepActionMap[$joinStep]
            );
        }

        return $this->get( 'ez_content' )->viewLocation( $locationId, $viewType, $layout, $params );
    }

    /**
     * Member join form (first page) Step #1
     * @see https://docs.google.com/a/thinkcreative.com/document/d/1K5_Ny6TxuO9tmmLvdKSGgqEsBxFl3h9MvuSrqTz68ss/edit#heading=h.y2decanzbnz2 
     * @return Response
     */
    public function introAction(Request $request, $view = "embed")
    {

        // get join renew service
        $joinRenewService = $this->get("project.member.join_renew");

        // redirect to last join step
        if ($joinRenewService->getJoinStep() > JoinRenew::JOIN_INTRO) {
            $redirect = new RedirectResponse(
                $this->getPathToStep($joinRenewService->getJoinStep())
            );
            $redirect->send();
            return new Response("");
        }

        // get form
        $form = $this->get("think_creative_form.form_service")->getForm("join/basic_info");

        // check submission status
        if ($form->isSubmitted() && $form->isValid()) {

            // find existing members
            $memberSearch = $joinRenewService->findExistingMembers($form);

            // if no member data found go to next step
            if (!$memberSearch) {
                $joinRenewService->storeBasicInformation(
                    $form->get("first_name")->getData(),
                    $form->get("last_name")->getData(),
                    $form->get("email")->getData()
                );

                $redirect = new RedirectResponse(
                    $this->getPathToStep(JoinRenew::JOIN_MEMBERSHIP_CATEGORY)
                );
                $redirect->send();
                return new Response("");
            }
        }

        // cache
        $response = new Response();
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);

        // if member exists in system give the
        // user the option to retrieve their account
        return $this->render(
            "ProjectMemberBundle:{$view}:join/introduction.html.twig",
            array(
                "form" => $form->createView(),
                "form_submitted" => $form->isSubmitted() && $form->isValid(),
                "member_search" => isset($memberSearch) ? $memberSearch : null,
                "joinRenew" => $joinRenewService,
                "nextStepPath" => $this->getPathToStep(JoinRenew::JOIN_MEMBERSHIP_CATEGORY),
                "returnStepPath" => $joinRenewService->getJoinStep() != JoinRenew::JOIN_INTRO ? $this->getPathToStep( $joinRenewService->getJoinStep() ) : null
            ),
            $response
        );

    }

    /**
     * Membership category selection page -- Step #2
     * @see https://docs.google.com/a/thinkcreative.com/document/d/1K5_Ny6TxuO9tmmLvdKSGgqEsBxFl3h9MvuSrqTz68ss/edit#heading=h.cutquv6fm8bk
     * @return Response
     */
    public function membershipCategoryAction(Request $request, $view = "embed")
    {

        // get join renew service
        $joinRenewService = $this->get("project.member.join_renew");

        // get form
        $form = $this->get("think_creative_form.form_service")->getForm("join/membership_category");

        // get join/renew conf
        $joinRenewConfiguration = $this->container->getParameter("ProjectMember_JoinRenew");

        // if form is submitted determine what
        // class code user will have and proceed
        if ($form->isSubmitted() && $form->isValid()) {

            // set promo code
            $joinRenewService->setPromoCode($form->get("promo")->getData());

            // form submission
            if ($form->isValid() && !$form->get("promo_apply")->isClicked()) {

                $classCode = "";
                $memberShipCat = $form->get("membership_category")->getData();

                // category with sub form
                if ($form->has($memberShipCat)) {

                    $subForm = $form->get($memberShipCat);
                    if ($subForm->has("member_rate")) {
                        $classCode = $subForm->get("member_rate")->getData();
                    }

                // single item category
                } else {
                    $classCode = $memberShipCat;
                }

                // add appropiate product to cart and redirect to checkout
                if ($classCode) {

                    // ensure selected product isn't hidden with promo
                    $hasValidCode = true;
                    foreach ($joinRenewConfiguration['promo_codes'] as $promo) {
                        if ($promo['value'] == $classCode) {
                            if ($form->get("promo")->getData() != $promo['code']) {
                                $hasValidCode = false;
                            } else {
                                $hasValidCode = true;
                                break;
                            }
                        }
                    }
                    if (!$hasValidCode) {
                        $form->get("membership_category")->addError(new FormError("Invalid membership category selected. Valid promo code required."));
                    }

                    if ($form->isValid()) {

                        $joinRenewService->addMemberClassToOrder(
                            $classCode,
                            $memberShipCat,
                            $memberShipCat == "joint" ? $form->get("joint")->get("joint_member_number")->getData() : null
                        );

                        if ($joinRenewService->getJoinStep() < JoinRenew::JOIN_IDENTIFICATION) {
                            $joinRenewService->setJoinStep(JoinRenew::JOIN_IDENTIFICATION);

                            // has orcid
                            if ($joinRenewService->getOrcid()) {
                                // skip next step
                                $joinRenewService->setJoinStep(JoinRenew::JOIN_CONTACT);                
                            }

                        }

                        // redirect to next step
                        $redirect = new RedirectResponse(
                            $this->getPathToStep(JoinRenew::JOIN_IDENTIFICATION)
                        );
                        $redirect->send();
                        return new Response("");
                    }
                }
            }

        // populate form with current product if selected
        } elseif (!$form->isSubmitted()) {

            if ($joinRenewService->getMemberClassCode()) {
                $form->get("membership_category")->setData($joinRenewService->getMembershipCategory());
                $form->get("joint")->get("joint_member_number")->setData($joinRenewService->getJointMemberId());
            }

            // populate with promo code if previously entered
            if ($joinRenewService->getPromoCode()) {
                $form->get("promo")->setData($joinRenewService->getPromoCode());
            }
        }

        // cache
        $response = new Response();
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);

        return $this->render(
            "ProjectMemberBundle:{$view}:join/membership_category.html.twig",
            array(
                "form" => $form->createView(),
                "joinRenew" => $joinRenewService,
                "previousStepPath" => $this->getPathToStep(JoinRenew::JOIN_INTRO)
            ),
            $response
        );
    }

    /**
     * Identification and Disambiguation -- Step #3
     * @see https://docs.google.com/a/thinkcreative.com/document/d/1K5_Ny6TxuO9tmmLvdKSGgqEsBxFl3h9MvuSrqTz68ss/edit#heading=h.s06mayyoxbji
     */
    public function identificationAction(Request $request, $view = "embed")
    {

        // get join renew service
        $joinRenewService = $this->get("project.member.join_renew");

        // unregister if requested
        if ($this->get("request")->query->get("unlink_orcid")) {
            $joinRenewService->setOrcid(null);
            // redirect
            $redirect = new RedirectResponse(
                $this->getPathToStep(JoinRenew::JOIN_IDENTIFICATION)
            );
            $redirect->send();
            return new Response("");
        }

        // set current step
        if ($joinRenewService->getJoinStep() < JoinRenew::JOIN_CONTACT) {
            $joinRenewService->setJoinStep(JoinRenew::JOIN_CONTACT);
        }

        // cache
        $response = new Response();
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);

        return $this->render(
            "ProjectMemberBundle:{$view}:join/identification.html.twig",
            array(
                "orcid_auth_url" => OrcidApi::AUTH_URL,
                "orcid_client_id" => OrcidApi::CLIENT_ID,
                "orcid_id" => $joinRenewService->getOrcid(),
                "previousStepPath" => $this->getPathToStep(JoinRenew::JOIN_MEMBERSHIP_CATEGORY),
                "thisStepPath" => $request->getUri(),
                "nextStepPath" => $this->getPathToStep(JoinRenew::JOIN_CONTACT)
            ),
            $response
        );

    }

    /**
     * Contact and Affiliation Information -- Step #4
     * @see https://docs.google.com/a/thinkcreative.com/document/d/1K5_Ny6TxuO9tmmLvdKSGgqEsBxFl3h9MvuSrqTz68ss/edit#heading=h.suc5z08c3u0w
     */
    public function contactAction(Request $request, $view = "embed")
    {
        // get join renew service
        $joinRenewService = $this->get("project.member.join_renew");

        // get form
        $form = $this->get("think_creative_form.form_service")->getForm("join/contact");

        // extra validation
        if ($form->isSubmitted()) {

            // require at least one send mail address
            if (
                !$form->get("address_home")->get("send_mail")->getData() &&
                !$form->get("address_primary")->get("send_mail")->getData() &&
                !(
                    $form->get("has_secondary")->getData() &&
                    $form->get("address_secondary")->get("send_mail")->getData()
                )
            ) {
                $formError = new FormError("You must select one address to send mail to.");
                $form->get("address_home")->get("send_mail")->addError($formError);
                $form->get("address_primary")->get("send_mail")->addError($formError);
                $form->get("address_secondary")->get("send_mail")->addError($formError);
            }    

            // home address validation
            if (
                !$form->get("address_home")->get("line3")->getData() ||
                !$form->get("address_home")->get("city")->getData() ||
                !$form->get("address_home")->get("state")->getData() ||
                !$form->get("address_home")->get("zip")->getData()
            ) {

                // home address cannot be mailing address if not filled out
                if ($form->get("address_home")->get("send_mail")->getData()) {
                    $form->get("address_home")->get("send_mail")->addError(new FormError("Must provide home address fields to select home address as mail recipent."));
                }

                // cannot select home address under privacy settings
                if (!$form->get("privacy")->get("address_home")->get("hidden")->getData()) {
                    $form->get("privacy")->get("address_home")->get("hidden")->addError(new FormError("Must provide home address to include home address in member listings."));
                }
            }

            // secondary address privacy
            if (
                !$form->get("has_secondary")->getData() &&
                !$form->get("privacy")->get("address_secondary")->get("hidden")->getData()
            ) {
               $form->get("privacy")->get("address_secondary")->get("hidden")->addError(new FormError("Must provide secondary address to include secondary address in member listings."));
            }

            // ensure only one of the following three fields are checked
            elseif (
                !(
                    (
                        $form->get("address_home")->get("send_mail")->getData() ^
                        $form->get("address_primary")->get("send_mail")->getData() ^
                        (
                            $form->get("has_secondary")->getData() &&
                            $form->get("address_secondary")->get("send_mail")->getData()
                        )
                    ) &&
                    !(
                        $form->get("address_home")->get("send_mail")->getData() &&
                        $form->get("address_primary")->get("send_mail")->getData() &&
                        (
                            $form->get("has_secondary")->getData() &&
                            $form->get("address_secondary")->get("send_mail")->getData()
                        )
                    )
                )
            ) {
                $formError = new FormError("Only one address may have mail sent to it.");
                $form->get("address_home")->get("send_mail")->addError($formError);
                $form->get("address_primary")->get("send_mail")->addError($formError);
                $form->get("address_secondary")->get("send_mail")->addError($formError);
            }
 
            // primary instituation name is required for student members
            if ($joinRenewService->getMemberClassCode() == "E" && !$form->get("address_primary")->get("affiliation")->getData()) {
                $form->get("address_primary")->get("affiliation")->addError(
                    new FormError("This field is required for student memberships.")
                );
            }

        }

        // submit form
        if ($form->isSubmitted() && $form->isValid()) {
            
            // submit member application to api and get member id
            if (!$joinRenewService->isMember()) {

                // set current step
                if ($joinRenewService->getJoinStep() < JoinRenew::JOIN_FORUM) {
                    $joinRenewService->setJoinStep(JoinRenew::JOIN_FORUM);
                }

                // member form handler
                $formHandler = $this->get("project.member.form_handler.member");
                $memberId = $formHandler->submit($form, true);

                // no member id set
                if (!$memberId && $form->isValid()) {
                    $form->addError( new FormError("An unknown error has occured. Please try again later.") );

                // create ezuser
                // @TODO delegate some of this to join renew service??
                } elseif ($form->isValid()) {

                    // get member service
                    $memberService = $this->get("project.member.member");

                    // get ez api repository
                    $ezApiRepository = $this->get("ezpublish.api.repository");

                    // get current user
                    $currentEzUser = $ezApiRepository->getCurrentUser();

                    // temporarly elevate permissions
                    $ezApiRepository->setCurrentUser( $ezApiRepository->getUserService()->loadUser( ApiMemberProvider::ADMIN_USER ) );

                    // get member
                    $member = $memberService->getMemberFromApiId($memberId);

                    // get member api
                    $memberApi = $member->getApiData();

                    // create ezuser and member entity
                    $memberService->createNewMember($memberApi);

                    // get member, with new ezuser
                    $member = $memberService->getMemberFromApiId($memberId);

                    // restore permissions
                    $ezApiRepository->setCurrentUser($currentEzUser);

                    // set orcid
                    if ($joinRenewService->getOrcid()) {
                        $memberService->setMemberOrcid($member, $joinRenewService->getOrcid());
                    }

                    // login
                    $user = new User(
                        $member->getEzUser(),
                        array("ROLE_USER")
                    );
                    $user->setAPIUser($member->getEzUser());
                    $token = new UsernamePasswordToken(
                        $member->getEzUser()->login,
                        MemberService::generateMemberPassword($memberApi),
                        "ezpublish_front",
                        array("ROLE_USER")
                    );
                    $this->get("security.context")->setToken($token);
                    $event = new InteractiveLoginEvent($this->get("request"), $token);
                    $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
                    $ezApiRepository->setCurrentUser($member->getEzUser());

                    // set languages
                    $languageFormHandler = $this->get("project.member.form_handler.languages");
                    $languageFormHandler->submit($form->get("languages"));                    

                }

            // signed in
            } elseif ($joinRenewService->isMember()) {

                // member form handler
                $formHandler = $this->get("project.member.form_handler.member");
                $formHandler->submit($form);

                // language form handler
                $languageFormHandler = $this->get("project.member.form_handler.languages");
                $languageFormHandler->submit($form->get("languages"));
                
            }

            // if form is valid after processing then move to next step
            if ($form->isValid()) {

                // set current step
                if ($joinRenewService->getJoinStep() < JoinRenew::JOIN_FORUM) {
                    $joinRenewService->setJoinStep(JoinRenew::JOIN_FORUM);
                }

                // redirect to next step
                $redirect = new RedirectResponse(
                    $this->getPathToStep(JoinRenew::JOIN_FORUM)
                );
                $redirect->send();
                return new Response("");
            }
            
        // set form data from stored member data
        } elseif (!$form->isSubmitted() && $joinRenewService->isMember()) {

            // has login
            $form->get("has_login")->setData(true);

            // member handler
            $formHandler = $this->get("project.member.form_handler.member");
            $formHandler->populate($form);

            // language handler
            $languageFormHandler = $this->get("project.member.form_handler.languages");
            $languageFormHandler->populate($form->get("languages"));

            // check 'add a second instition' if data provided
            if (
                $form->get("address_secondary")->get("line3")->getData() ||
                $form->get("address_secondary")->get("city")->getData() ||
                $form->get("address_secondary")->get("state")->getData() ||
                $form->get("address_secondary")->get("zip")->getData()
            ) {
                $form->get("has_secondary")->setData(true);
            }


        // prepopulate name/email from basic information in step 1
        } elseif (!$form->isSubmitted()) {

            if (!$form->get("general")->get("first_name")->getData()) {
                $form->get("general")->get("first_name")->setData(
                    $joinRenewService->getOrderDetail("firstName")
                );
            }
            if (!$form->get("general")->get("last_name")->getData()) {
                $form->get("general")->get("last_name")->setData(
                    $joinRenewService->getOrderDetail("lastName")
                );
            }
            if (!$form->get("general")->get("email")->getData()) {
                $form->get("general")->get("email")->setData(
                    $joinRenewService->getOrderDetail("email")
                );
            }

        }

        // cache
        $response = new Response();
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);

        return $this->render(
            "ProjectMemberBundle:{$view}:join/contact.html.twig",
            array(
                "form" => $form->createView(),
                "previousStepPath" => $this->getPathToStep(JoinRenew::JOIN_IDENTIFICATION)
            ),
            $response
        );

    }

    /**
     * Primary Forum Affiliation Selection -- Step #5
     * @see https://docs.google.com/document/d/15_ZZw5g5C2D_UGDAssyKEtJPCTsYHnTnUFh0SQt-xkQ/edit#heading=h.umrwmsw755l2
     */
    public function forumAffiliationAction(Request $request, $view = "embed")
    {

        // get join renew service
        $joinRenewService = $this->get("project.member.join_renew");

        // get form
        $form = $this->get("think_creative_form.form_service")->getForm("member/primary_forum_selection");

        // get organizations
        $organizations = $this->get("project.api.api")->getOrganizations();
        $organizationCategories = $organizations->getCategories("forum");
        $organizationList = $organizations->getOrganizations(null, "forum");

        if (!$form->isSubmitted()) {

            // use form handler to populate
            $formHandler = $this->get("project.member.form_handler.primary_forum_affiliation");
            $formHandler->populate($form);


        // submission
        } elseif ($form->isSubmitted() && $form->isValid()) {

            if (count($form->get("primary_forum_selection")->get("forums")->getData()) > 5) {
                $form->addError(
                    new FormError("You may only select five forums.")
                );
            }

            if ($form->isValid()) {

                // use form handler to submit to api
                $formHandler = $this->get("project.member.form_handler.primary_forum_affiliation");
                $formHandler->submit($form);

                if ($form->isValid()) {

                    // set current step
                    if ($joinRenewService->getJoinStep() < JoinRenew::JOIN_CAREER_PATH) {
                        $joinRenewService->setJoinStep(JoinRenew::JOIN_CAREER_PATH);
                    }

                    // redirect to next step
                    $redirect = new RedirectResponse(
                        $this->getPathToStep(JoinRenew::JOIN_CAREER_PATH)
                    );
                    $redirect->send();
                    return new Response("");

                }
            }
        }

        // cache
        $response = new Response();
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);

        return $this->render(
            "ProjectMemberBundle:{$view}:join/primary_forum_selection.html.twig",
            array(
                "form" => $form->createView(),
                "categories" => $organizationCategories,
                "organizations" => $organizationList,
                "previousStepPath" => $this->getPathToStep(JoinRenew::JOIN_CONTACT),
                "nextStepPath" => $this->getPathToStep(JoinRenew::JOIN_CAREER_PATH)
            ),
            $response
        );
    }

    /**
     * Career Path Survey -- Step #6
     * @see https://docs.google.com/a/thinkcreative.com/document/d/1K5_Ny6TxuO9tmmLvdKSGgqEsBxFl3h9MvuSrqTz68ss/edit#heading=h.jjlqsyg1kg0e
     */
    public function careerPathAction(Request $request, $view = "embed")
    {

        // get join renew service
        $joinRenewService = $this->get("project.member.join_renew");

        // get form
        $form = $this->get("think_creative_form.form_service")->getForm("member/career_path");

        // submit
        if ($form->isSubmitted() && $form->isValid()) {

            // use form handler to submit to api
            $formHandler = $this->get("project.member.form_handler.career_path_survey");
            $formHandler->submit($form);

            // set current step
            if ($joinRenewService->getJoinStep() < JoinRenew::JOIN_ADDITIONAL_OPTIONS) {
                $joinRenewService->setJoinStep(JoinRenew::JOIN_ADDITIONAL_OPTIONS);
            }

            // redirect to next step
            $redirect = new RedirectResponse(
                $this->getPathToStep(JoinRenew::JOIN_ADDITIONAL_OPTIONS)
            );
            $redirect->send();
            return new Response("");

        // restore from api
        } elseif (!$form->isSubmitted())  {
            $formHandler = $this->get("project.member.form_handler.career_path_survey");
            $formHandler->populate($form);
        }

        // cache
        $response = new Response();
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);

        return $this->render(
            "ProjectMemberBundle:{$view}:join/career_path.html.twig",
            array(
                "form" => $form->createView(),
                "previousStepPath" => $this->getPathToStep(JoinRenew::JOIN_FORUM)
            ),
            $response
        );

    }

    /**
     * Additonal Options -- Step #7
     * @see https://docs.google.com/a/thinkcreative.com/document/d/1K5_Ny6TxuO9tmmLvdKSGgqEsBxFl3h9MvuSrqTz68ss/edit#heading=h.uqcuoddabfw1
     */
    public function additionalOptionsAction(Request $request, $view = "embed")
    {

        // get join renew service
        $joinRenewService = $this->get("project.member.join_renew");

        // ez content service
        $contentService = $this->get("ezpublish.api.service.content");

        // get form
        $form = $this->get("think_creative_form.form_service")->getForm("join/additional_options");

        // get join/renew configuration
        $joinRenewConfiguration = $this->container->getParameter("ProjectMember_JoinRenew");

        // form submission actions
        if ($form->isSubmitted() && $form->isValid()) {

            // ensure validity of donations
            foreach ($form->get("donate")->getIterator() as $field) {
                if ($field->get("donate")->getData() && (!$field->get("amount")->getData() || !ctype_digit($field->get("amount")->getData()))) {
                    $field->get("amount")->addError(new FormError("Invalid dollar amount entered."));
                }
            }

            // use form handler to submit to api
            $formHandler = $this->get("project.member.form_handler.member");
            $formHandler->submit($form);

            // add/remove products
            foreach ($form->get("products")->getIterator() as $field) {
                
                // remove
                $joinRenewService->removeBenefit(
                    intval($field->getConfig()->getOption("value"))
                );

                // add
                if ($field->getData()) {
                    $joinRenewService->addBenefit(
                        intval($field->getConfig()->getOption("value"))
                    );
                }

            }

            // remove previous donation products
            $joinRenewService->removeDonations();

            // add donations from form
            foreach ($form->get("donate")->getIterator() as $field)
            {
                if ($field->get("donate")->getData() && intval($field->get("amount")->getData()) > 0) {
                    $joinRenewService->addDonation(
                        "_" . strtoupper(trim($field->getName())),
                        $field->get("amount")->getData()
                    );
                }
            }

            // set current step
            if ($joinRenewService->getJoinStep() < JoinRenew::JOIN_MEMBERSHIP_REVIEW) {
                $joinRenewService->setJoinStep(JoinRenew::JOIN_MEMBERSHIP_REVIEW);
            }

            // redirect to next step
            $redirect = new RedirectResponse(
                $this->getPathToStep(JoinRenew::JOIN_MEMBERSHIP_REVIEW)
            );
            $redirect->send();
            return new Response("");

        // populate
        } else {

            // populate api
            $formHandler = $this->get("project.member.form_handler.member");
            $formHandler->populate($form);

            // populate donations
            foreach ($form->get("donate")->getIterator() as $field) {
                $donationAmount = $joinRenewService->getDonationAmount(
                    "_" . strtoupper(trim($field->getName()))
                );
                $field->get("amount")->setData($donationAmount);
                if ($donationAmount) {
                    $field->get("donate")->setData(true);
                }
            }

            // populate products
            foreach ($form->get("products")->getIterator() as $field) {
                if (
                    $joinRenewService->hasProductInOrder(
                        intval($field->getConfig()->getOption("value"))
                    )
                ) {
                    $field->setData(true);
                }
            }
        }

        // response
        $response = new Response();
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);

        return $this->render(
            "ProjectMemberBundle:{$view}:join/additional_options.html.twig",
            array(
                "form" => $form->createView(),
                "joinRenew" => $joinRenewService,
                "previousStepPath" => $this->getPathToStep(JoinRenew::JOIN_CAREER_PATH)
            ),
            $response
        );

    }

    /**
     * Review -- Step #8
     * @see https://docs.google.com/a/thinkcreative.com/document/d/1K5_Ny6TxuO9tmmLvdKSGgqEsBxFl3h9MvuSrqTz68ss/edit?pli=1#heading=h.rs8y62yg32yg
     */
    public function reviewAction(Request $request, $view = "embed")
    {

        // get join renew service
        $joinRenewService = $this->get("project.member.join_renew");

        // get member service
        $memberService = $this->get("project.member.member");

        // get current logged in member api data
        $memberApi = $memberService->getCurrentMember()->getApiData();

        // cache
        $memberVary = $memberApi->all();
        unset($memberVary['authentication']['password']);
        $response = new Response();
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->setVary( 'X-User-Hash' );
        $response->setETag(
            md5(
                json_encode(
                    $memberVary
                )
            )
        );
        if ($response->isNotModified($request)) {
            return $response;
        }

        // get department lists
        $departments = array();
        foreach ($memberApi->get("addresses") as $address) {
            if ($address['type'] == "home") {
                continue;
            }
            if (!isset($address['zip']) || !$address['zip']) {
                continue;
            }
            $departments += $this->get("project.member.form_populator.departments")->execute($address['zip']);
        }        

        return $this->render(
            "ProjectMemberBundle:{$view}:join/review.html.twig",
            array(
                "member" => $memberApi,
                "order" => $joinRenewService->getOrder(),
                "ranks" => $this->get("project.member.form_populator.career_path_survey.rank")->execute(),
                "languages" => $this->get("project.member.form_populator.languages")->execute(),
                "departments" => $departments,
                "product_variant_seperator" => OrderProcessorService::PRODUCT_VARIANT_SEPERATOR,
                "membership_product_object_id" => ProductService::MEMBERSHIP_OBJECT_ID,
                "previousStepPath" => $this->getPathToStep(JoinRenew::JOIN_ADDITIONAL_OPTIONS),
            )
        );

    }

    /**
     * Pay by check Step #9A
     * @see https://docs.google.com/a/thinkcreative.com/document/d/1K5_Ny6TxuO9tmmLvdKSGgqEsBxFl3h9MvuSrqTz68ss/edit#heading=h.ijjr5iqpdyzh
     */
    public function payCheckAction(Request $request)
    {

        // verify step
        $this->verifyStep(JoinRenew::JOIN_MEMBERSHIP_REVIEW);

        // get join renew service
        $joinRenewService = $this->get("project.member.join_renew"); 

        // get current logged in member api data
        $memberApi = $joinRenewService->getMember()->getApiData();

        // get department lists
        $departments = array();
        foreach ($memberApi->get("addresses") as $address) {
            if ($address['type'] == "home") {
                continue;
            }
            if (!isset($address['zip']) || !$address['zip']) {
                continue;
            }
            $departments += $this->get("project.member.form_populator.departments")->execute($address['zip']);
        }  

        // cache
        $response = new Response();
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);

        return $this->render(
            "ProjectMemberBundle:full:join/pay_check.html.twig",
            array(
                "member" => $joinRenewService->getMember() ? $joinRenewService->getMember()->getApiData() : null,
                "order" => $joinRenewService->getOrder(),
                "ranks" => $this->get("project.member.form_populator.career_path_survey.rank")->execute(),
                "languages" => $this->get("project.member.form_populator.languages")->execute(),
                "departments" => $departments,
                "product_variant_seperator" => OrderProcessorService::PRODUCT_VARIANT_SEPERATOR,
                "membership_product_object_id" => ProductService::MEMBERSHIP_OBJECT_ID,
                "signup_year" => $joinRenewService->getCurrentSignupYear(),
                "previousStepPath" => $this->getPathToStep(JoinRenew::JOIN_MEMBERSHIP_REVIEW),
                "progressPayment" => JoinRenew::JOIN_PAYMENT
            )
        );
    }

    /**
     * Pay with credit card / Paypal -- Step #9B/C
     * @see https://docs.google.com/a/thinkcreative.com/document/d/1K5_Ny6TxuO9tmmLvdKSGgqEsBxFl3h9MvuSrqTz68ss/edit?pli=1#heading=h.rgmmvw52an9t
     */
    public function paymentAction($payment_type = "credit-card", Request $request)
    {
        // get gateway
        switch ($payment_type) {
            case "paypal":
                $gateway = JoinRenew::PAYPAL_GATEWAY;
                break;

            case "credit-card":
            default:
                $gateway = JoinRenew::CREDIT_CARD_GATEWAY;
                break;
        }

        // verify step
        $this->verifyStep(JoinRenew::JOIN_MEMBERSHIP_REVIEW);

        // get join renew service
        $joinRenewService = $this->get("project.member.join_renew");

        // get form
        switch($gateway) {
            case JoinRenew::CREDIT_CARD_GATEWAY:
                $form = $this->get("think_creative_form.form_service")->getForm("join/pay_credit");
                break;

            case JoinRenew::PAYPAL_GATEWAY:
                $form = $this->get("think_creative_form.form_service")->getForm("join/pay_paypal");
                break;
        }

        // proceed to checkout
        if ($form->isSubmitted() && $form->isValid()) {

            // create credit card
            // find send_mail address to use as shipping address
            $sendMailAddress = null;
            foreach ($joinRenewService->getMember()->getApiData()->get("addresses") as $address) {
                if ($address['send_mail'] == "N" || !$address['send_mail']) {
                    continue;
                }
                $sendMailAddress = $address;
                break;
            }
            if (!$sendMailAddress) {
                throw new \InvalidArguementException("A mailing address is required to proceed.");
            }

            $ccData = array(
                'shippingAddress1' => $sendMailAddress['line3'],
                'shippingAddress2' => $sendMailAddress['line1'],
                'shippingCity' => $sendMailAddress['city'],
                'shippingCountry' => $sendMailAddress['country_code'],
                'shippingFirstName' => $joinRenewService->getMember()->getApiData()->get("general[first_name]", null, true),
                'shippingLastName' => $joinRenewService->getMember()->getApiData()->get("general[last_name]", null, true),
                'shippingState' => $sendMailAddress['state'],
                'shippingPostcode' => $sendMailAddress['zip']
            );

            if (
                ($gateway == JoinRenew::CREDIT_CARD_GATEWAY && $form->get("billing")->get("same")->getData()) ||
                $gateway == JoinRenew::PAYPAL_GATEWAY
            ) {
                $ccData = array_merge(
                    $ccData,
                    array(
                        'billingAddress1' => $sendMailAddress['line3'],
                        'billingAddress2' => $sendMailAddress['line1'],
                        'billingCity' => $sendMailAddress['city'],
                        'billingCountry' => $sendMailAddress['country_code'],
                        'billingFirstName' => $joinRenewService->getMember()->getApiData()->get("general[first_name]", null, true),
                        'billingLastName' => $joinRenewService->getMember()->getApiData()->get("general[last_name]", null, true),
                        'billingState' => $sendMailAddress['state'],
                        'billingPostcode' => $sendMailAddress['zip'],
                        'billingPhone' => $joinRenewService->getMember()->getApiData()->get("general[phone]", null, true),
                        'phone' => $joinRenewService->getMember()->getApiData()->get("general[phone]", null, true),
                        'email' => $joinRenewService->getMember()->getApiData()->get("general[email]", null, true)
                    )
                );
            }

            if ($gateway == JoinRenew::CREDIT_CARD_GATEWAY && !$form->get("billing")->get("same")->getData()) {
                $ccData = array_merge($ccData, $form->get("billing")->getData());
            }

            if ($gateway == JoinRenew::CREDIT_CARD_GATEWAY) {
                $ccData = array_merge($ccData, $form->get("payment")->getData());
                $ccData = array_merge($ccData, $form->get("payment")->get("expire")->getData());
                unset($ccData['expiration']);
            }

            // store payment data, ensure validity
            try {
                $joinRenewService->setPaymentData($gateway, $ccData);
            } catch (InvalidCreditCardException $e) {
                if ($gateway == JoinRenew::CREDIT_CARD_GATEWAY) {
                    $form->get("payment")->get("number")->addError( new FormError($e->getMessage() ) );
                } else {
                    $form->addError( new FormError($e->getMessage()) );
                }
            }

            // ensure form is still valid
            if ($form->isValid()) {

                // authorize payment
                try {

                    $joinRenewService->authorizePayment(
                        $gateway == JoinRenew::PAYPAL_GATEWAY ? $this->generateUrl("JoinMembershipPayment", array("payment_type" => "paypal"), true) : "",
                        $gateway == JoinRenew::PAYPAL_GATEWAY ? $this->generateUrl("JoinMembershipPayCancel", array(), true) : ""
                    );

                    $orderDetails = $joinRenewService->getOrder()->getCustomerDetails();
                    if (isset($orderDetails['misc']['shopifyOrderId'])) {
                        $redirect = new RedirectResponse(
                            $this->generateUrl(
                                "JoinMembershipPayConfirm", 
                                array("order_id" => $orderDetails['misc']['shopifyOrderId'])
                            )
                        );
                        $redirect->send();
                        return new Response("");
                    }

                // invalid credit card
                } catch (InvalidCreditCardException $e) {

                    if ($gateway == JoinRenew::CREDIT_CARD_GATEWAY) {
                        $form->get("payment")->get("number")->addError( new FormError($e->getMessage() ) );
                    } else {
                        $form->addError( new FormError($e->getMessage()) );
                    }

                // payment error
                } catch(PaymentGatewayErrorException $e) {
                    if ($gateway == JoinRenew::CREDIT_CARD_GATEWAY) {
                        $form->get("payment")->get("number")->addError( new FormError($e->getMessage() ) );
                    } else {
                        $form->addError( new FormError($e->getMessage()) );
                    }
                
                // shopify error
                } catch(ShopifyApiResponseException $e) {
                    $form->addError( new FormError($e->getMessage()) );   
                }

            }

        }

        // paypal return
        if ($gateway == JoinRenew::PAYPAL_GATEWAY && $request->query->get("token")) {

            try {
                $joinRenewService->completeRedirectPayment(
                    $request->query->get("token")
                );
            } catch (\Exception $e) {
                $form->addError(new FormError($e->getMessage()));
            }

            $orderDetails = $joinRenewService->getOrder()->getCustomerDetails();
            if (isset($orderDetails['misc']['shopifyOrderId'])) {
                $redirect = new RedirectResponse(
                    $this->generateUrl(
                        "JoinMembershipPayConfirm", 
                        array("order_id" => $orderDetails['misc']['shopifyOrderId'])
                    )
                );
                $redirect->send();
                return new Response("");
            }
        }

        // get template to render
        switch ($gateway) {
            case JoinRenew::CREDIT_CARD_GATEWAY:
                $template = "ProjectMemberBundle:full:join/pay_credit.html.twig";
                break;
            case JoinRenew::PAYPAL_GATEWAY:
                $template = "ProjectMemberBundle:full:join/pay_paypal.html.twig";
                break;
        }

        // cache
        $response = new Response();
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);

        // render template
        return $this->render(
            $template,
            array(
                "form" => $form->createView(),
                "member" => $joinRenewService->getMember() ? $joinRenewService->getMember()->getApiData() : array(),
                "order" => $joinRenewService->getOrder(),
                "previousStepPath" => $this->getPathToStep(JoinRenew::JOIN_MEMBERSHIP_REVIEW),
                "progressPayment" => JoinRenew::JOIN_PAYMENT
            )
        );

    }

    /**
     * Pay Confirmation -- Step #10
     * @see https://docs.google.com/a/thinkcreative.com/document/d/1K5_Ny6TxuO9tmmLvdKSGgqEsBxFl3h9MvuSrqTz68ss/edit?pli=1#heading=h.qroue4xntaj
     */
    public function payConfirmAction(Request $request, $order_id)
    {

        // cache
        $response = new Response();
        $response->setPrivate();
        $response->setMaxAge(600);
        $response->setSharedMaxAge(600);
        $response->setVary( 'X-User-Hash' );
        $response->setETag($order_id);
        if ($response->isNotModified($request)) {
            return $response;
        }   

        // get join renew service
        $joinRenewService = $this->get("project.member.join_renew");

        // look up order id
        try { 
            $orderData = $joinRenewService->getShopifyOrder($order_id);
        } catch(ApiResponseException $e) {
            throw $this->createNotFoundException("Order with ID '{$shopify_order_id}' was not found.");
        }

        // get join year from shopify order
        $joinYear = $joinRenewService->getCurrentSignupYear();
        foreach ($orderData['order']['note_attributes'] as $note) {
            if ($note['name'] == "membership_year") {
                $joinYear = intval($note['value']);
                break;
            }
        }

        return $this->render(
            "ProjectMemberBundle:full:join/pay_confirm.html.twig",
            array(
                "order_id" => $order_id,
                "member" => $joinRenewService->getMember()->getApiData(),
                "join_year" => $joinYear,
                "progressPayment" => JoinRenew::JOIN_PAYMENT
            )
        );

    }

    /**
     * Page to display when payment canceled from Paypal or any
     * other payment service that requires a redirect.
     */
    public function payCancelAction(Request $request)
    {

        // cache
        $response = new Response();
        $response->setPublic();
        $response->setMaxAge(3600);
        $response->setSharedMaxAge(3600);
        if ($response->isNotModified($request)) {
            return $response;
        }   

        return $this->render(
            "ProjectMemberBundle:full:join/pay_cancel.html.twig",
            array(
                "previousStepPath" => $this->getPathToStep(JoinRenew::JOIN_MEMBERSHIP_REVIEW),
                "progressPayment" => JoinRenew::JOIN_PAYMENT
            )
        );
    }

    /**
     * Order Receipt -- Step #11
     * @see https://docs.google.com/a/thinkcreative.com/document/d/1K5_Ny6TxuO9tmmLvdKSGgqEsBxFl3h9MvuSrqTz68ss/edit#heading=h.102sqnfpjuml 
     */
    public function payReceiptAction($order_id, Request $request)
    {

        // cache
        $response = new Response();
        $response->setPrivate();
        $response->setMaxAge(600);
        $response->setSharedMaxAge(600);
        $response->setVary( 'X-User-Hash' );
        $response->setETag($order_id);
        if ($response->isNotModified($request)) {
            return $response;
        }   

        // get join renew service
        $joinRenewService = $this->get("project.member.join_renew");

        // look up order id
        try { 
            $orderData = $joinRenewService->getShopifyOrder($order_id);
        } catch(ApiResponseException $e) {
            throw $this->createNotFoundException("Order with ID '{$shopify_order_id}' was not found.");
        }

        return $this->render(
            "ProjectMemberBundle:full:join/pay_receipt.html.twig",
            array(
                "order_id" => $order_id,
                "order" => $orderData['order'],
                "transactions" => $orderData['transactions'],
                "member" => $joinRenewService->getMember()->getApiData(),
                "join_year" => $joinRenewService->getCurrentSignupYear(),
                "donation_content_id" => ProductService::DONATION_OBJECT_ID
            )
        );

    }

}