<?php

namespace Project\MemberBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\View\TwitterBootstrapView;
use Project\ApiBundle\Exception\ApiResponseException;
use Project\MemberBundle\Entity\MemberSearchLookups;
use Project\MemberBundle\Classes\Member as MemberObject;
use Project\MemberBundle\Exception\MemberSearchLimitException;

/**
 * Controller for handling member searches
 */
class MemberSearchController extends Controller
{

    /**
     * Main search action
     */
    public function searchAction()
    {

        // get member service
        $memberService = $this->get("project.member.member");

        // get member
        $member = $memberService->getCurrentMember();

        // make sure user isn't a 'joining' member
        if ($member && $member->getMemberStatus() == MemberObject::MEMBER_STATUS_JOIN) {
            throw new AccessDeniedException("Must be a full member to access this section.");
        }

        // check current user type
        if (!$member) {
            throw new AccessDeniedException("Must be a full member to access this section.");
        }

        // get search form
        $form = $this->get("think_creative_form.form_service")->getForm("member_search");

        // get global request
        $request = Request::createFromGlobals();

        // form submitted
        if ($form->isSubmitted() && $form->isValid()) {
            if (!$form->get("first_name")->getData() && !$form->get("last_name")->getData() && !$form->get("affiliation")->getData()) {
                $form->addError(new FormError("Search fields are empty."));
            }
            if ($form->isValid()) {

                // get member search configuration
                $memberSearchConf = $this->container->getParameter("ProjectMember_MemberSearch");

                if ($member->getMemberStatus() != MemberObject::MEMBER_STATUS_EZUSER) {

                    // get member search usage entity
                    $doctrineManager = $this->get("doctrine.orm.entity_manager");
                    $memberRepo = $doctrineManager->getRepository('ProjectMemberBundle:MemberSearchLookups');
                    $memberSearchUsage = $memberRepo->findOneBy(
                        array("memberId" => $member->getMemberApiId())
                    );

                    // create new entity if one doesn't exist
                    if (!$memberSearchUsage) {
                        $memberSearchUsage = new MemberSearchLookups();
                        $memberSearchUsage->setMemberId($member->getMemberApiId());
                        $memberSearchUsage->updateFirstRequestTime();
                        $memberSearchUsage->resetSentEmailNotification();
                    }

                    // set search conf
                    $memberSearchUsage->setMemberSearchConfiguration($memberSearchConf);

                    // reset usage if able
                    if ($memberSearchUsage->canReset()) {
                        $memberSearchUsage->resetUsageCount();
                        $memberSearchUsage->updateFirstRequestTime();
                        $memberSearchUsage->resetSentEmailNotification();
                    }

                    // ensure member can use
                    if (!$memberSearchUsage->canUse()) {
                       
                        // send email
                        if (!$memberSearchUsage->hasSentEmailNotification()) {
                            $message = \Swift_Message::newInstance()
                                ->setSubject("Member search lockout")
                                ->setFrom("membership@mla.org")
                                ->setTo($memberSearchConf['limit_reached_notification_emails'])
                                ->setBody(
                                    $this->renderView(
                                        "ProjectMemberBundle:email:misc/member_search_limit_notification.txt.twig",
                                        array(
                                            "member" => $member->getApiData(),
                                            "search_limit" => $memberSearchConf['limit'],
                                            "first_request_time" => $memberSearchUsage->getFirstRequestTime()
                                        )
                                    )
                                )
                            ;
                            $this->get("mailer")->send($message);
                        }

                        // flag email as sent
                        $memberSearchUsage->setSentEmailNotification();

                        // store entity
                        $doctrineManager->persist($memberSearchUsage);
                        $doctrineManager->flush();

                        // display error message
                        return $this->render(
                            "ProjectMemberBundle:customtags:member_search/limit_reached_error.html.twig"
                        );

                        // throw exception
                        //throw new MemberSearchLimitException("Maximum number of allowed search queries has been reached.");

                    }

                    // increment usage
                    $memberSearchUsage->incrementUsageCount();
                    $memberSearchUsage->updateLastRequestTime();
                    $memberSearchUsage->resetSentEmailNotification();

                    // store entity
                    $doctrineManager->persist($memberSearchUsage);
                    $doctrineManager->flush();

                }

                // redirect to search results
                $redirect = new RedirectResponse($request->getBaseUrl() . "?" . http_build_query(array(
                    "first_name" => $form->get("first_name")->getData(),
                    "last_name" => $form->get("last_name")->getData(),
                    "affiliation" => $form->get("affiliation")->getData()
                )));
                $redirect->send();
            }

        // get search values from request
        } elseif($request->query->has("first_name") || $request->query->has("last_name") || $request->query->has("affiliation")) {

            // set form fields
            $form->get("first_name")->setData($request->query->get("first_name"));
            $form->get("last_name")->setData($request->query->get("last_name"));
            $form->get("affiliation")->setData($request->query->get("affiliation"));

            // get search results
            $searchResults = $this->performSearch(
                $request->query->get("first_name"),
                $request->query->get("last_name"),
                $request->query->get("affiliation")
            );
            
            // build pagerfanta search object
            $searchAdapter = new ArrayAdapter(
                $searchResults['search_results']
            );
            $pagerfanta = new Pagerfanta($searchAdapter);
            $pagerfanta->setMaxPerPage($request->query->get("limit") ?: 20);
            $pagerfanta->setCurrentPage($request->query->get("page") ?: 1);

            // render pagination for twitter bootstrap
            if ($pagerfanta->haveToPaginate()) {
                // router generator
                $routeGenerator = function($page) use ($request) {
                    $urlParams = $request->query->all();
                    $urlParams['page'] = $page;
                    return $request->getBaseUrl() . "?" . http_build_query($urlParams);
                };

                // create view
                $view = new TwitterBootstrapView();
                $options = array(
                    'proximity' => 3,
                    'prev_message' => "&laquo;",
                    'next_message' => "&raquo;"
                );
                $pagination = $view->render($pagerfanta, $routeGenerator, $options);

                // slight hack to get the 'pagination' class on the UL element
                $pagination = str_replace("<ul", "<ul class='pagination'", $pagination);
            }

        }

        return $this->render(
            "ProjectMemberBundle:customtags:member_search/main.html.twig",
            array(
                "uri" => $request->getBaseUrl(),
                "query" => $request->query->all(),
                "form" => $form->createView(),
                "results" => isset($pagerfanta) ? $pagerfanta : null,
                "total_number_results" => isset($searchResults['total_num_results']) ? $searchResults['total_num_results'] : 0,
                "pagination" => isset($pagination) ? $pagination : null,
                "return_path" => $request->getPathInfo(),
                "display_search_form" => $request->query->get("display_search_form")
            )
        );
    }

    /**
     * Search by name results
     * @param string $first_name
     * @param string $last_name
     * @param string $affiliation
     * @return array
     */
    private function performSearch($first_name = "", $last_name = "", $affiliation = "")
    {

        // no results
        if (!$first_name && !$last_name && !$affiliation) {
            return array();
        }

        // perform api query
        $api = $this->get("project.api.api");

        try {
            $apiResults = $api->request(
                "members",
                "GET",
                array(
                    "first_name" => $first_name,
                    "last_name" => $last_name,
                    "affiliation" => $affiliation
                )
            );
        } catch (ApiResponseException $e) {
            if ($e->getStatusCode() != "API-2102") {
                throw $e;
            }
            $apiResults = array();
        }

        // sort member results by last name
        $memberList = isset($apiResults['data'][0]['search_results']) ? $apiResults['data'][0]['search_results'] : array();
        usort($memberList, function($a, $b) {
            return strcasecmp(
                $a['general']['last_name'] . ", " . $a['general']['first_name'],
                $b['general']['last_name'] . ", " . $b['general']['first_name']
            );
        });

        return array(
            "total_num_results" => isset($apiResults['data'][0]['total_num_results']) ? $apiResults['data'][0]['total_num_results'] : 0,
            "search_results" => $memberList
        );
    }

    /**
     * Display a member profile
     * @param integer $meme
     */
    public function memberProfileAction($member_id)
    {

        // get member service
        $memberService = $this->get("project.member.member");

        // get member
        $member = $memberService->getCurrentMember();

        // make sure user isn't a 'joining' member
        if ($member && $member->getMemberStatus() == MemberObject::MEMBER_STATUS_JOIN) {
            throw new AccessDeniedException("Must be a full member to access this section.");
        }

        // check current user type
        if (!$member || $member->getMemberStatus() == MemberObject::MEMBER_STATUS_EZUSER) {
            throw new AccessDeniedException("Must be a full member to access this section.");
        }

        // get looked up member
        $lookupMember = $memberService->getMemberFromApiId($member_id);

        // get api service
        $api = $this->get("project.api.api");

        // get member search results
        try {

            $searchParams = array(
                "first_name" => $lookupMember->getApiData()->get("general[first_name]", null, true),
                "last_name" => $lookupMember->getApiData()->get("general[last_name]", null, true), 
            );
            if ($lookupMember->getApiData()->get("general[email]", null, true)) {
                $searchParams["email"] = $lookupMember->getApiData()->get("general[email]", null, true);
            }

            $apiResults = $api->request(
                "members",
                "GET",
                $searchParams
            );
        } catch (ApiResponseException $e) {
            if ($e->getStatusCode() != "API-2102") {
                throw $e;
            }
            $apiResults = array();
        }

        // no results
        if (!$apiResults || !isset($apiResults['data'][0]['total_num_results']) || $apiResults['data'][0]['total_num_results'] == 0) {
            throw new NotFoundHttpException("Member not found.");            
        }

        return $this->render(
            "ProjectMemberBundle:full:misc/member_profile.html.twig",
            array(
                "member" => $lookupMember->getApiData(),
                "search_result" => $apiResults['data'][0]['search_results'][0],
                "query" => $this->get("request")->query->get("query"),
                "return_path" => $this->get("request")->query->get("return_path"),
            )
        );

    }

}