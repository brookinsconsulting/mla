<?php

namespace Project\MemberBundle\Security\Authentication;

use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\Request;
use Project\MemberBundle\Services\LoginEntryPoint;

class ApiMemberLoginFailure implements AuthenticationFailureHandlerInterface
{

    protected $loginEntryPoint;

    public function __construct(LoginEntryPoint $loginEntryPoint)
    {
        $this->loginEntryPoint = $loginEntryPoint;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return $this->loginEntryPoint->start(
            $request,
            $exception,
            true
        );
    }

}
