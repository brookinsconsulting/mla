<?php

namespace Project\MemberBundle\Security\Authentication;

use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Cmf\Component\Routing\ChainRouter;

class ApiMemberLogoutSuccess implements LogoutSuccessHandlerInterface
{
    public function __construct(ChainRouter $router)
    {
        $this->router = $router;
    }

    public function onLogoutSuccess(Request $request)
    {

        // redirect to referer
        $redirect = $request->headers->get('referer');

        // redirect to root if no referer
        if (!$redirect) {
            $redirect = "/";
        }

        // create response
        $response = new RedirectResponse($redirect);
        $response->headers->clearCookie('REMEMBERME');      

        // redirect
        return $response ;

    }

}