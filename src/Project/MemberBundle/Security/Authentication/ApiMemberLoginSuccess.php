<?php

namespace Project\MemberBundle\Security\Authentication;

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Cmf\Component\Routing\ChainRouter;
use eZ\Bundle\EzPublishCoreBundle\DependencyInjection\Configuration\ChainConfigResolver;
use Project\ApiBundle\Exception\ApiResponseException;
use Project\MemberBundle\Services\Member as MemberService;
use Project\MemberBundle\Classes\Member as MemberObject;

class ApiMemberLoginSuccess implements AuthenticationSuccessHandlerInterface
{

    const MEMBER_SITE_ACCESS = "site";
    protected $router;
    protected $memberService;
    protected $configResolver;
    
    public function __construct(ChainRouter $router, MemberService $memberService, ChainConfigResolver $configResolver)
    {
        $this->router = $router;
        $this->memberService = $memberService;
        $this->configResolver = $configResolver;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {

        // get current member
        $member = $this->memberService->getCurrentMember();

        // default redirect
        $redirect = $this->router->generate(
            "ez_urlalias",
            array(
                "locationId" => $this->configResolver->getParameter("content.tree_root.location_id")
            )
        );

        // redirect to join page if member is joining
        if ($member && $member->getMemberStatus() == MemberObject::MEMBER_STATUS_JOIN) {
            $redirect = $this->router->generate(
                "JoinIndex",
                array(
                    "siteaccess" => self::MEMBER_SITE_ACCESS
                )
            );

        // redirect to _target_path if set
        } elseif ($request->request->get("_target_path")) {
            $redirect = $request->request->get("_target_path");

        // redirect to user page if current site access is "MEMBER_SITE_ACCESS"
        } elseif($request->attributes->get("siteaccess")->name == self::MEMBER_SITE_ACCESS) {
            $redirect = $this->router->generate(
                "UserMain"
            );
        }

        // create redirect response
        $response = new RedirectResponse($redirect);

        // redirect
        return $response;
    }

}
