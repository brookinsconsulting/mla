<?php

namespace Project\MemberBundle\Security\Authentication\Provider;

use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use eZ\Publish\Core\MVC\Symfony\Security\User;
use eZ\Publish\API\Repository\Repository;
use eZ\Publish\Core\Base\Exceptions\NotFoundException;
use Project\ApiBundle\Services\Api as ProjectApi;
use Project\ApiBundle\Endpoints\Member as MemberApi;
use Project\ApiBundle\Exception\ApiResponseException;
use project\MemberBundle\Services\Member as ProjectMember;
use project\MemberBundle\Classes\Member as MemberObject;

class ApiMemberProvider implements AuthenticationProviderInterface 
{

    /**
     * eZPublish admin user content id. Used
     * to gain permission to create new users.
     * @var integer
     */
    const ADMIN_USER = 14;

    /**
     * @var UserProviderInterface
     */
    private $userProvider;

    /**
     * @var Member
     */
    protected $memberService;

    /**
     * @var Repository
     */
    protected $eZPublishApiRepository;

    /**
     * Member Service
     * @var ProjectMember
     */
    protected $projectMember;

    /**
     * Project API Service
     * @var ProjectApi
     */
    protected $projectApi;

    /**
     * Event dispatcher
     * @var ContainerAwareEventDispatcher
     */
    protected $eventDispatcher;

    /**
     * Constructor.
     */
    public function __construct(
        UserProviderInterface $userProvider,
        Repository $eZPublishApiRepository,
        ProjectMember $projectMember,
        ProjectApi $projectApi,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->userProvider = $userProvider;
        $this->eZPublishApiRepository = $eZPublishApiRepository;
        $this->projectMember = $projectMember;
        $this->projectApi = $projectApi;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Authenticate API member
     * @var TokenInterface $token
     * @return TokenInterface
     */
    public function authenticate(TokenInterface $token)
    {

        // get member source
        $memberSource = $this->projectMember->getCurrentMemberSource();

        // attempt to get API member
        try {
            $memberApi = $this->projectApi->getMember($token->getUsername(), $memberSource);
        } catch (ApiResponseException $e) {
            throw new AuthenticationException();
        }

        // verify password and get member level
        $accessLevel = $memberApi->verifyPassword($token->getCredentials());
        if (!$accessLevel) {
            throw new BadCredentialsException();
        }

        // find member
        $memberObject = $this->projectMember->getMemberFromApiId($memberApi->get("id"), $memberSource, $accessLevel);

        // create new member object if one doesn't exist
        if (!$memberObject || !$memberObject->getEzUser()) {

            // get current ez user
            $currentEzUser = $this->eZPublishApiRepository->getCurrentUser();

            // temporarly elevate permissions
            $this->eZPublishApiRepository->setCurrentUser( $this->eZPublishApiRepository->getUserService()->loadUser(self::ADMIN_USER) );

            // create new ezuser and member entity
            $this->projectMember->createNewMember($memberApi, $accessLevel);

            // restore original user level
            $this->eZPublishApiRepository->setCurrentUser($currentEzUser);

            // reload member object
            $memberObject = $this->projectMember->getMemberFromApiId($memberApi->get("id"), $memberSource, $accessLevel);

        }

        // load ez user and login
        if ($memberObject) {

            // get current ez user
            $currentEzUser = $this->eZPublishApiRepository->getCurrentUser();

            // temporarly elevate permissions
            $this->eZPublishApiRepository->setCurrentUser( $this->eZPublishApiRepository->getUserService()->loadUser( ApiMemberProvider::ADMIN_USER ) );

            // set member groups as needed
            $this->projectMember->setMemberUserGroup($memberObject, $accessLevel);

            // refresh member object
            $memberObject = $this->projectMember->getMemberFromApiId($memberObject->getApiData()->get("id"), $memberSource, $accessLevel);

            // restore original user level
            $this->eZPublishApiRepository->setCurrentUser($currentEzUser);

            // load ezuser for member
            $ezUser = $memberObject->getEzUser();

            // user roles
            $roles = array('ROLE_USER');
                
            // create user token
            $user = new User($ezUser, $roles);
            $user->setAPIUser($ezUser);
            $authToken = new UsernamePasswordToken(
                $user,
                null,
                "ezpublish_front",
                $roles
            );

            // department account expiration
            if (
                $memberObject->getMemberStatus() == MemberObject::MEMBER_STATUS_DEPARTMENT &&
                (
                    trim($memberObject->getApiData()->get("authentication[member_type]", null, true)) == "none" ||
                    trim($memberObject->getApiData()->get("authentication[member_type]", null, true)) == ""
                )
            ) {
                $e = new AccountExpiredException();
                $e->setToken($token);
                $e->setUser($user);
                throw $e;
            }

            return $authToken;


        } else {
            throw new AuthenticationException();
        }

    }

    /**
     * Determine if provider supports auth token
     * @param TokenInterface $token
     */
    public function supports(TokenInterface $token)
    {
        return $token instanceof UsernamePasswordToken;
    }

}
