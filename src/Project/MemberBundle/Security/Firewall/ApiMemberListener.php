<?php

namespace Project\MemberBundle\Security\Firewall;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Project\MemberBundle\Services\Member as MemberService;
use Project\MemberBundle\Classes\Member;

class ApiMemberListener implements ListenerInterface
{
    const MAIN_SITE_ACCESS = "site";
    protected $securityContext;
    protected $authenticationManager;
    protected $session;

    public function __construct(SecurityContextInterface $securityContext, AuthenticationManagerInterface $authenticationManager, Session $session)
    {
        $this->securityContext = $securityContext;
        $this->authenticationManager = $authenticationManager;
        $this->session = $session;
    }

    public function handle(GetResponseEvent $event)
    {

        // get request
        $request = $event->getRequest();

        // login attempt
        if ($request->request->get("_username") && $request->request->get("_password")) {

            // set member source
            if ($request->request->has("source")) {
                $this->session->set(
                    "member_source_id",
                    intval($request->request->get("source"))
                );
            }

            // create token
            $token = new UsernamePasswordToken(
                $request->request->get("_username"),
                $request->request->get("_password"),
                "ezpublish_front",
                array('ROLE_USER')
            );

            // authenticate
            $authToken = $this->authenticationManager->authenticate($token);
            $this->securityContext->setToken($authToken);
        }
    }

}