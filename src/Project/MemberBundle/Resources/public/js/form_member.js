// department lookup
function getDepartments(element, zip_code)
{
    var element = element;
    setFeedback($(element).parent(), FEEDBACK_THROBER, "");
    $.get(
        DEPARTMENT_LOOKUP_URL,
        "zip_code=" + zip_code,
        function(data) {
            setFeedback($(element).parent(), FEEDBACK_NONE, "");
            $(element).find("option:not([value='']):not([value='_other'])").remove();
            for (key in data) {
                $("<option></option>")
                    .attr("value", key)
                    .text(data[key])
                    .insertBefore($(element).find("option[value='_other']"))
                ;
            }
            $(element).removeAttr("disabled");

            var fieldValue = $(element).parent().parent().find("input.department").val();
            if (fieldValue && !$(element).hasClass("has-init")) {

                if (fieldValue[0] == "_") {
                    $(element).val( fieldValue.substring(1) );
                } else {
                    $(element).val("_other");
                    $(element).trigger("change");
                    $(element).parent().parent().find(".choiceother-text input").val(fieldValue);
                }
                $(element).addClass("has-init");
            }
        },
        "json"
    );
}

$(document).ready(function() {

    // show/hide secondary address
    $("#contact_address_secondary").hide();
    $("#contact_has_secondary").change(function() {
        if ($(this).prop("checked")) {
            $("#contact_address_secondary").slideDown();        
        } else {
            $("#contact_address_secondary").slideUp();
        }
    });
    if ($("#contact_has_secondary").prop("checked")) {
        $("#contact_address_secondary").show();
    }
    $("#contact_has_secondary").change(function() {
        if ($(this).val()) {
            $("#contact_address_secondary_action").val("D");
        } else {
            $("#contact_address_secondary_action").val("U");
        }
    });

    // ensure only one primary address checkbox is checked
    $(".form-row-send_mail input[type=checkbox]").click(function() {
        var t = this;
        $(".form-row-send_mail input[type=checkbox]").each(function() {
            if ($(this).attr("id") != $(t).attr("id")) {
                $(this).prop("checked", false);
            }
        });
    });

    // populate department lookup
    $("input.department").each(function() {

        // inject dropdown
        $(this).attr("type", "hidden");
        $(this).parent().append(
            "<div class='choiceother-choice has-feedback'><select id='"+ $(this).attr("id") +"_choice' name='"+$(this).attr("id")+"_choice' class='form-control' disabled='disabled'><option value=''>Select A Department...</option><option value='_other'>Other</option></div>"
        );
        $(this).parent().append(
            "<div class='choiceother-text' style='display:none;'><input type='text' id='"+ $(this).attr("id") +"_other' name='"+$(this).attr("id")+"_other' class='form-control' placeholder='Other' /></div>"
        );

        // add 'choice-other' functionality
        hookChoiceother($(this).parent());

        // get department list
        var zipCode = $(this).parent().parent().find(".form-row-zip input").val().trim();
        if (zipCode) {
            getDepartments(
                $(this).parent().find(".choiceother-choice select"),
                zipCode
            );
        }

        // get new list of departments of zip code changes
        var t = this;
        $(this).parent().parent().find(".form-row-zip input").change(function() {
            getDepartments(
                $(t).parent().find(".choiceother-choice select"),
                $(this).parent().parent().find(".form-row-zip input").val().trim()
            );                    
        })

        $(this).parent().find(".choiceother-choice select").change(function() {
            $(t).val("_" + $(this).val());
        });
        $(this).parent().find(".choiceother-text input").change(function() {
            $(t).val($(this).val()); 
        });

    });

    // disable address privacy settings if address not provided
    function privacyHomeAddress()
    {
        $("#contact_privacy_address_home_hidden").prop("disabled", false);
        if (
            !$("#contact_address_home_line3").val() &&
            !$("#contact_address_home_city").val() &&
            !$("#contact_address_home_state").val() &&
            !$("#contact_address_home_zip").val()
        ) {
            $("#contact_privacy_address_home_hidden")
                .prop("disabled", true)
                .prop("checked", false)
            ;
        }
    }
    privacyHomeAddress();
    $("#contact_address_home input").change(function() {
        privacyHomeAddress();
    })

    function checkSecondaryAddress()
    {
        $("#contact_privacy_address_secondary_hidden").prop("disabled", false);
        if (!$("#contact_has_secondary").prop("checked")) {
            $("#contact_privacy_address_secondary_hidden")
                .prop("disabled", true)
                .prop("checked", false)
            ;
        }
    }
    checkSecondaryAddress()
    $("#contact_has_secondary").change(function() {
        checkSecondaryAddress()
    })

    // uncheck other email prefs if unsubscribe to all is checked
    $(".form-row-email_unsubscribe.checkbox input").change(function() {
        if ($(this).prop("checked")) {
            $(".form-row-email_digest.checkbox input").prop("checked", false);
            $(".form-row-email_convention.checkbox input").prop("checked", false);
        }
    });
    $(".form-row-email_digest.checkbox input, .form-row-email_convention.checkbox input").change(function() {
        if ($(this).prop("checked")) {
            $(".form-row-email_unsubscribe.checkbox input").prop("checked", false);
        }
    });

    var sendMailFields = ["#contact_address_home_send_mail", "#contact_address_primary_send_mail", "#contact_address_secondary_send_mail"];
    function checkSendMail()
    {
        var selectionCount = 0;
        $(sendMailFields).each(function(key, value) {
            setFeedback($(value).parent(), FEEDBACK_NONE, "");
            if ($(value + ":checked").length > 0) {
                selectionCount++;
            }
        });

        if (selectionCount == 0) {
            $(sendMailFields).each(function(key, value) {
                setFeedback($(value).parent(), FEEDBACK_ERROR, "You must select one address as your preferred mailing address.");
                $('<div class="alert alert-warning">You must select one address as your preferred mailing address.</div>').insertBefore($(value));
                $(value).parent().find("div.alert").slideDown();
            });
        }

    }
    $(sendMailFields).each(function(key, value) {
        $(value).change(function() {
            checkSendMail();
        });
    });
    $(sendMailFields[0]).closest("form").submit(function() {

        var selectionCount = 0;
        $(sendMailFields).each(function(key, value) {
            setFeedback($(value).parent(), FEEDBACK_NONE, "");
            if ($(value + ":checked").length > 0) {
                selectionCount++;
            }
        });
        if (selectionCount != 1) {
            return false;
        }
        return true;

    });

});
