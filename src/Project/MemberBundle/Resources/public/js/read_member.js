// MODALS
function iframeResize()
{
    iframeHeight = $("#member-modal iframe").contents().height();
    if (iframeHeight < $(window).height() - 300) {
        if ($("#member-modal iframe").height() != iframeHeight) {
            $("#member-modal iframe").css({"height" : (iframeHeight) + "px"});
        }
    } else {
        if ($("#member-modal iframe").height() != ($(window).height() - 300)) {
            $("#member-modal iframe").css({"height" : ($(window).height() - 300) + "px" });
        }
    }
}

// attempt to resize iframe at regular intervals
var resizeTimer;
function iframeResizeInterval()
{
    iframeResize();
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function() {
        iframeResizeInterval();
    }, 1000);
}

function loadModal()
{
    $("#member-modal .btn-submit")
        .html('<span class="glyphicon spinning glyphicon-refresh"></span>')
        .prop("disabled", true)
    ;
    $("#member-modal h4.modal-title").html("Loading...");
    $("#member-modal iframe")
        .fadeOut(function() {
            $("#member-modal .loading-throbber")
                .height( $("#member-modal iframe").height() )
                .fadeIn()
            ;
            $("#member-modal .modal-body").css({"min-height" : ($("#member-modal iframe").height() + 40) + "px" });
        })
        .on("load", function() {
            $("#member-modal .loading-throbber").fadeOut(function() {

                var title = $("#member-modal iframe").contents().find("h2").html();
                $("#member-modal h4.modal-title").fadeOut(function() {
                    $("#member-modal iframe").contents().find("h2").remove();
                    $("#member-modal h4.modal-title").html(title);
                    
                    $("#member-modal iframe").show();
                    iframeResizeInterval();
                    $("#member-modal iframe").hide();
                    $("#member-modal iframe").slideDown();
                    $(this).fadeIn();
                });

                $("#member-modal .btn-submit")
                    .html("Save changes")
                    .prop("disabled", false)
                ;

                $("#member-modal .btn-submit").unbind("click");
                $("#member-modal .btn-submit").click(function() {
                    $("#member-modal iframe").contents().find("form").submit();
                    loadModal();
                });

            });
        })
    ;
}

function launchModal(formName)
{
    $("#member-modal .loading-throbber").css({"height" : "128px"});
    $("#member-modal .modal-body").css({"min-height" : "128px"});
    $("#member-modal iframe")
        .height(128)
        .attr("src", "/user/edit?form=" + formName)
        .hide()
    ;
    $("#member-modal").modal();
    loadModal();
}

$(document).ready(function() {
    iframeResizeInterval();
});

// RELOAD DATA
function reloadData(element)
{
    var element = element;
    if (!$(element).attr("data-member-data")) {
        return;
    }
    $(element).addClass("loading");
    $.get(
        "/user/read",
        "form=" + $(element).attr("data-member-data"),
        function(data) {
            $(element).removeClass("loading");
            $(element).html(data);
        },
        "html"
    );

}