// FORUM SELECTION
$("#primary_forum_selection_primary_forum_selection_primary_forum_selection_forums").hide();
$(document).ready(function() {

    $("#primary_forum_selection_primary_forum_selection_forums").hide();
    if ($(".mla-form .forum-selection .js-forum-selection input.forum-typeahead").length > 0) {

        var forums = $("#primary_forum_selection_primary_forum_selection_forums option").map(function() {return $(this).text();}).get();
        var forumsBh = new Bloodhound({
              datumTokenizer: Bloodhound.tokenizers.whitespace,
              queryTokenizer: Bloodhound.tokenizers.whitespace,
              local: forums
        });

        function addForum(name)
        {
            var name = name;
            var values = $("#primary_forum_selection_primary_forum_selection_forums").val()
            if (!values) { values = []; }

            if (values.length >= 5) {
                if ($(".js-forum-selection .alert").length > 0) {
                    $(".js-forum-selection .alert").html("You may only select five forums.");
                    return;
                }
                $(".js-forum-selection").prepend('<div class="alert alert-warning" role="alert">You may only select five forums.</div>')
                $(".js-forum-selection .alert").slideDown();
                return;
            }

            $(".js-forum-selection .alert").slideUp(function() {
                $(this).remove();
            });

            $("#primary_forum_selection_primary_forum_selection_forums option").each(function() {
                if ($(this).text() == name) {
                    values.push($(this).attr("value"));
                }
            });
            $("#primary_forum_selection_primary_forum_selection_forums").val(values);            
        }

        function renderSelections()
        {
            $(".js-forum-selection .selection-list .btn-remove").unbind("click");
            $(".js-forum-selection .selection-list").html("");
            var values = $("#primary_forum_selection_primary_forum_selection_forums").val()
            $(values).each(function(key) {
                if ($("#primary_forum_selection_primary_forum_selection_forums option[value=" + values[key] + "]").length > 0) {
                    name = $("#primary_forum_selection_primary_forum_selection_forums option[value=" + values[key] + "]").text();
                    firstWord = name.split(" ")[0];
                    if (firstWord.toUpperCase() == firstWord) {
                        name = name.replace(firstWord, '<span class="text-primary">' + firstWord + '</span> ');
                    }

                    $(".js-forum-selection .selection-list").append('<div class="forum-item" id="forum-item-' + values[key] + '"><span class="btn-remove glyphicon glyphicon-remove-sign"></span> ' + name + '</div>');
                    var key = key;

                    if ($(".js-forum-selection .selection-list .forum-item").length == 1) {
                        $(".js-forum-selection .selection-list").slideDown();
                    }

                    $("#forum-item-" + values[key] + " .btn-remove").click(function() {
                        $(this).unbind("click");
                        values[key] = null;
                        $("#primary_forum_selection_primary_forum_selection_forums").val(values);
                        renderSelections();
                        $(".js-forum-selection .alert").slideUp(function() {
                            $(this).remove();
                        });
                        if ($(".js-forum-selection .selection-list .forum-item").length == 0) {
                            $(".js-forum-selection .selection-list").slideUp();
                        }
                    });
                }
            });
        }

        $(".mla-form .forum-selection .js-forum-selection input.forum-typeahead").typeahead({
            highlight: true,
            minLength: 1
        },
        {
            name: "forums",
            source: forumsBh
        })
            .bind("typeahead:select", function(evt) {
                addForum($(this).typeahead('val'));
                $(this).typeahead('val', "");
                renderSelections();
            })
            .keyup(function(evt) {
                if(evt.keyCode == 13){
                    if (forums.indexOf($(this).typeahead('val')) != -1) {
                        addForum($(this).typeahead('val'));
                        $(this).typeahead('val', "");
                    }
                    renderSelections
                }
            })
        ;

        renderSelections();

    }
});