function launchOrcidAuth(auth_url, client_id, redirect_uri)
{

    // calculate browser window position
    var browserPosX = window.screenLeft != null ? window.screenLeft : window.screenX;
    var browserPosY = window.screenTop != null ? window.screenTop : window.screenY;

    window.open(
        auth_url + "/oauth/authorize?client_id=" + client_id + "&response_type=code&scope=/orcid-profile/read-limited&redirect_uri=" + redirect_uri,
        "_blank",
        "scrollbars=yes, width=500, height=600, top="+ ( browserPosY + (window.outerHeight / 2) - 300)  +", left=" + ( browserPosX + (window.outerWidth / 2) - 250 )
    );
}

$(document).ready(function() {

    $("#orcidModal").on("show.bs.modal", function(e) {
        $(this).find("iframe").attr("src", $(this).attr("data-orcid-url"));
    });

});