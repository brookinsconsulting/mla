// MEMBERSHIP CATERGORY
function checkPromo()
{
    if (!$("#membership_category_promo").val()) {
        setFeedback($("#membership_category_promo").parent(), FEEDBACK_NONE, "");
        if (typeof JOIN_AVAILABLE_PROMO_VALUES !== "undefined") {
            for (var i = 0; i < JOIN_AVAILABLE_PROMO_VALUES.length; i++) {
                $("input[value=" + JOIN_AVAILABLE_PROMO_VALUES[i] + "]").parent().slideUp();
            }
        }
        return;
    }

    $.post(
        JOIN_PROMO_VALIDATE_PATH,
        "value=" + $("#membership_category_promo").val(),
        function(data) {
            if (data.valid)  {
                setFeedback($("#membership_category_promo").parent(), FEEDBACK_SUCCESS, data.message);
                if (typeof JOIN_AVAILABLE_PROMO_VALUES !== "undefined") {
                    for (var i = 0; i < JOIN_AVAILABLE_PROMO_VALUES.length; i++) {
                        console.log(JOIN_AVAILABLE_PROMO_VALUES[i]);
                        if (data.values.indexOf(JOIN_AVAILABLE_PROMO_VALUES[i]) > -1) {
                            $("input[value=" + JOIN_AVAILABLE_PROMO_VALUES[i] + "]").parent().slideDown();
                            $("input[value=" + JOIN_AVAILABLE_PROMO_VALUES[i] + "]").parent().removeClass("hidden");
                        } else {
                            $("input[value=" + JOIN_AVAILABLE_PROMO_VALUES[i] + "]").parent().slideUp();
                        }
                    }
                }
            } else {
                setFeedback($("#membership_category_promo").parent(), FEEDBACK_ERROR, data.message);
                if (typeof JOIN_AVAILABLE_PROMO_VALUES !== "undefined") {
                    for (var i = 0; i < JOIN_AVAILABLE_PROMO_VALUES.length; i++) {
                        $("input[value=" + JOIN_AVAILABLE_PROMO_VALUES[i] + "]").parent().slideUp();
                    }
                }
            }
        },
        "json"
    )
}
var checkPromoTimeout;
$(document).ready(function() {
    $("#membership_category_promo").on("input", function() {
        setFeedback($(this).parent(), FEEDBACK_THROBER, "");
        clearTimeout(checkPromoTimeout);
        checkPromoTimeout = setTimeout(
            function() { checkPromo(); },
            2000
        )
    });
    checkPromo();

    // hide all sub forms
    $(".mla-form .membership-category .form-sub-content").hide();

    // reveil sub form if membership category changes
    $(".membership-category.radio input").change(function() {
        $(".form-sub-content.form-row").slideUp();
        $(".form-sub-content.form-row-" + $(this).val()).slideDown();
    });

    // if membership category has value then reveil sub form at page load
    if ($(".membership-category.radio input:checked").val()) {
        $(".form-sub-content.form-row-" + $(".membership-category.radio input:checked").val()).show();
    }

    // check first category radio if none selected
    if ($(".mla-form .membership-category.radio input:checked").length <= 0) {
        $(".mla-form .membership-category.radio input:first").prop("checked", true);
        $(".form-sub-content.form-row-" + $(".mla-form .membership-category.radio input:first").val()).slideDown();
    }

    // use ajax to retrieve member information
    function loadMember(member_id) {
        $(".form-row-joint_member_name div").html("<span class='glyphicon glyphicon-refresh spinning'></span>");
        setFeedback($("#membership_category_joint_joint_member_number").parent(), FEEDBACK_THROBER, "");

        $.ajax({
            type: "GET",
            url: "/join/membership-category/member-lookup/" + member_id,
            dataType: "json",
            success: function(data) {
                if (data && data['status'] == "success") {
                    $(".form-row-joint_member_name div").html(data['data']['last_name'] + ", " + data['data']['first_name']);
                    setFeedback($("#membership_category_joint_joint_member_number").parent(), FEEDBACK_SUCCESS, data['message']);
                } else if (data && data['status'] == "error") {
                    $(".form-row-joint_member_name div").html("<em>" + data['message'] + "</em>");
                    setFeedback($("#membership_category_joint_joint_member_number").parent(), FEEDBACK_ERROR, data['message']);
                }
            },
            error: function(data) {
                console.log(data);
                $(".form-row-joint_member_name div").html("<em>An error occured</em>");
                setFeedback($("#membership_category_joint_joint_member_number").parent(), FEEDBACK_ERROR, "An error occured");
            }
        });        
    }
    $(".form-row-joint_member_name div").html("<em>Not yet selected</em>");
    $(".mla-form .membership-category .form-row-joint_member_number input").change(function() {
        loadMember($(this).val());
    });
    if ($(".mla-form .membership-category .form-row-joint_member_number input").val()) {
        loadMember(
            $(".mla-form .membership-category .form-row-joint_member_number input").val()
        );
    }

});

// ADDITIONAL OPTIONS
function disableDonationFields()
{
    $(".form-row-donate").each(function() {
        if (!$(this).find("input[type=checkbox]").prop("checked")) {
            $(this).parent().find("input[type=text]").attr("disabled", "disabled");
        } else {
            $(this).parent().find("input[type=text]").removeAttr("disabled");
        }
    });
}
$(document).ready(function() {
    disableDonationFields();
    $(".form-row-donate").find("input[type=checkbox]").change(function() {
        disableDonationFields();
    });
});