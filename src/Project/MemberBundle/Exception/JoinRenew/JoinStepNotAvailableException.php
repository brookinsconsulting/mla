<?php

namespace Project\MemberBundle\Exception\JoinRenew;

/**
 * Should be fired when a step in the join/renew process
 * is not found or not available.
 */
class JoinStepNotAvailableException extends \Exception
{
}