<?php

namespace Project\MemberBundle\Exception\Orcid;

/**
 * Exception to fire if there is a
 * curl error when making an ORCID
 * API call.
 */
class OrcidCurlException extends \Exception
{
}