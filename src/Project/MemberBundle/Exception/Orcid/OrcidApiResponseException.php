<?php

namespace Project\MemberBundle\Exception\Orcid;
use Project\ApiBundle\Exception\ApiResponseException;

/**
 * Exception to fire if there is a
 * error responsed from the ORCID
 * API.
 */
class OrcidApiResponseException extends \Exception
{
}