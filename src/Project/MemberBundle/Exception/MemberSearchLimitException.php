<?php

namespace Project\MemberBundle\Exception;

/**
 * Exception to fire if current member
 * has reached the member search query
 * limit.
 */
class MemberSearchLimitException extends \Exception
{
}