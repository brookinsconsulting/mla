<?php

namespace Project\MemberBundle\Exception;

/**
 * Exception to fire if there
 * is an error with a member form.
 */
class MemberFormException extends \Exception
{

    protected $type = "warning";

    public function __construct($type = "warning", $message)
    {
        $this->message = trim($message);
        $this->type = $type;
    }

    public function getType() {
        return $this->type;
    }

}