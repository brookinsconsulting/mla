<?php

namespace Project\MemberBundle\Exception;

/**
 * Exception to fire if there is a
 * configuration error with a member
 * form.
 */
class MemberFormConfigurationException extends \Exception
{
}