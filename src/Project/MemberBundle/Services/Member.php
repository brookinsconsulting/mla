<?php

namespace Project\MemberBundle\Services;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\SecurityContext;

use Doctrine\ORM\EntityManager;

use eZ\Publish\Core\Repository\UserService as EzUserService;
use eZ\Publish\API\Repository\Values\User\User as EzUser;

use ThinkCreative\PaymentBundle\Entity\Order;
use Project\ProductBundle\Order\OrderProcessorService;
use Project\ProductBundle\Services\Product as ProductService;

use Project\ApiBundle\Services\Api as ProjectApi;
use Project\ApiBundle\Endpoints\Member as MemberApi;
use Project\MemberBundle\Entity\Member as MemberEntity;
use Project\MemberBundle\Classes\Member as MemberObject;

use Project\ApiBundle\Exception\ApiResponseException;
use Project\ApiBundle\Exception\ApiCurlException;
use eZ\Publish\API\Repository\Exceptions\NotFoundException;
use eZ\Publish\Core\Base\Exceptions\UnauthorizedException;

/**
 * MLA member handling service
 */
class Member
{

    /**
     * Secret key used to hash passwords
     * @var string
     */
    const HASH_SECRET = "randomLettersNumb3rs&Symbols!@@#434342EResresfeDFxzxjykTJTfhj$#54s";

    /**
     * eZPublish content object id of desired 
     * join in progress user group.
     * @var integer
     */
    const JOIN_PROGRESS_USER_GROUP = 26406;

    /**
     * eZPublish content object id of desired 
     * member user group.
     * @var integer
     */
    const MEMBER_USER_GROUP = 11;

    /**
     * eZPublish content object id of anonymous
     * user group.
     * @var integer
     */
    const ANONYMOUS_USER_GROUP = 42;

    /**
     * eZPublish admin user content id. Used
     * to gain permission to create new users.
     * @var integer
     */
    const ADMIN_USER = 14;

    /**
     * eZPublish content object ids of ade/adfl
     * department members
     * @var integer
     */
    const DEPARTMENT_ADE_LEVEL1_USER_GROUP = 29946;
    const DEPARTMENT_ADE_LEVEL2_USER_GROUP = 29957;
    const DEPARTMENT_ADFL_LEVEL1_USER_GROUP = 31165;
    const DEPARTMENT_ADFL_LEVEL2_USER_GROUP = 31166;

    /**
     * Api Service
     * @var Project\ApiBundle\Services\Api
     */
    protected $api = null;

    /**
     * Session
     * @var Symfony\Component\HttpFoundation\Session\Session
     */
    protected $session = null;

    /**
     * Symfony security context service
     * @var Symfony\Component\Security\Core\SecurityContext
     */
    protected $securityContext;

    /**
     * eZPublish User Service
     * @var eZ\Publish\Core\Repository\UserService
     */
    protected $ezUserService;

    /**
     * Doctrine entity manager
     * @var Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * Constructor.
     * @param Project\ApiBundle\Services\Api $api
     * @param Symfony\Component\HttpFoundation\Session\Session $session
     * @param eZ\Publish\API\Repository\Repository $ezpublish_api_repository
     */
    public function __construct(
        ProjectApi $api, 
        Session $session, 
        EzUserService $ezUserService,
        EntityManager $entityManager,
        SecurityContext $securityContext = null
    ) {
        $this->api = $api;
        $this->session = $session;
        $this->securityContext = $securityContext;
        $this->ezUserService = $ezUserService;
        $this->entityManager = $entityManager;
    }

    /**
     * Return Api Service
     * @return Project\ApiBundle\Services\Api
     */
    public function getApiService()
    {
        return $this->api;
    }

    /**
     * Get a member relation entity from MemberObject
     * @param MemberObject $member
     * @return MemberEntity|null
     */
    private function getMemberEntity(MemberObject $member, $accessLevel = null)
    {

        // get entity repo
        $memberRepo = $this->entityManager->getRepository('ProjectMemberBundle:Member');

        // get member relation entity
        if ($member->getMemberApiId() && $accessLevel > 0) {
            return $memberRepo->findOneBy(
                array(
                    "memberApiId" => $member->getMemberApiId(),
                    "memberAccessLevel" => $accessLevel
                )
            );
        } else {
            return $memberRepo->findOneBy(
                array("ezUserId" => $member->getEzUserId())
            );            
        }     

        return null;

    }

    /**
     * Retrieve currently logged in member or null
     * if no member is logged in
     * @return MemberObject|null
     */
    public function getCurrentMember()
    {

        // need security context
        if (!$this->securityContext) {
            return null;
        }

        // get security context token
        $token = $this->securityContext->getToken();

        // try to get ezuser from token
        if ($token && $token->getRoles() && $token->getProviderKey() == "ezpublish_front") {
            $ezUser = $this->ezUserService->loadUserByLogin($token->getUsername());
        } else {
            return null;
        }
        return $this->getMemberFromEzUser($ezUser);
    }

    /**
     * Attempt to get current member source id from session
     * @return integer|null
     */
    public function getCurrentMemberSource()
    {
        if ($this->session) {
            return $this->session->get("member_source_id") ?: null;
        }
        return null;
    }

    /**
     * Retrieve member from api member id. Returns
     * null if no member was found.
     * @param integer $value
     * @param integer $memberSourceId
     * @throws Project\ApiBundle\Exception\ApiResponseException  If member api returned error response
     * @throws Project\ApiBundle\Exception\ApiCurlException  If curl returned error when connecting to member api
     * @return MemberObject
     */
    public function getMemberFromApiId($value, $memberSourceId = null, $accessLevel = null)
    {

        // get member source id
        if (!$memberSourceId) {
            if ($this->session) {
                $memberSourceId = $this->session->get("member_source_id") ?: MemberApi::MEMBER_SOURCE_MLA;
            } else {
                $memberSourceId = MemberApi::MEMBER_SOURCE_MLA;
            }
        }

        // get member api
        $memberApi = $this->api->getMember($value, $memberSourceId);

        // get member relation entity
        $memberRepo = $this->entityManager->getRepository('ProjectMemberBundle:Member');

        switch ($memberSourceId) {
            case MemberApi::MEMBER_SOURCE_DEPARTMENTS:
                $memberRelation = $memberRepo->findOneBy(
                    array(
                        "memberSourceId" => $memberSourceId,
                        "memberApiId" => $value,
                        "memberAccessLevel" => $accessLevel
                    )
                );            
                break;
            default:
                $memberRelation = $memberRepo->findOneBy(
                    array(
                        "memberSourceId" => $memberSourceId,
                        "memberApiId" => $value,
                    )
                );
                break;
        }

        // get ezuser if member relation is available
        $ezUser = null;
        if ($memberRelation && $memberRelation->getEzUserId()) {
            try {
                $ezUser = $this->ezUserService->loadUser($memberRelation->getEzUserId());
            } catch (NotFoundException $e) {
            }
        }

        return new MemberObject(
            $memberApi,
            $ezUser,
            $accessLevel
        );

    }

    /**
     * Retrieve member from ezuser id or ezuser
     * object.
     * @param integer|EzUser $value
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException  If ezpublish user was not found
     * @return MemberObject
     */
    public function getMemberFromEzUser($value)
    {

        // get ez user
        if ($value instanceof EzUser) {
            $ezUser = $value;
        } elseif (in_array(gettype($value), array("integer", "string"))) {
            $ezUser = $this->ezUserService->loadUser( intval($value) );
        } else {
            throw new NotFoundException("Given value does not correspond to an eZUser.");
        }

        // get member relation entity
        $memberRepo = $this->entityManager->getRepository('ProjectMemberBundle:Member');
        $memberRelation = $memberRepo->findOneBy(
            array("ezUserId" => $ezUser->contentInfo->id)
        );

        // get member api
        $memberApi =  null;
        if ($memberRelation && $memberRelation->getMemberApiId()) {
            try {
                $memberApi = $this->api->getMember(
                    $memberRelation->getMemberApiId(),
                    $memberRelation->getMemberSourceId()
                );
            } catch (ApiResponseException $e) {
            } catch (ApiCurlException $e) {
            }
        }

        return new MemberObject(
            $memberApi,
            $ezUser,
            $memberRelation ? $memberRelation->getMemberAccessLevel() : null
        );

    }

    /**
     * Retrieve a stored value for a member
     * @param MemberObject $member
     * @param string $key
     * @return mixed
     */
    public function getDataFromMember(MemberObject $member, $key)
    {

        // get member entity
        $memberRelation = $this->getMemberEntity($member);
        if (!$memberRelation) {
            return null;
        }

        // get member data
        $memberData = $memberRelation->getMemberData();
        return $memberData->get($key, null, true);

    }

    /**
     * Persist a key/value to a member
     * @param MemberObject $member
     * @param string $key
     * @param mixed $value
     * @param bool  Return false if unable to store key/value
     */
    public function persistDataToMember(MemberObject $member, $key, $value)
    {

        // get member entity
        $memberRelation = $this->getMemberEntity($member);
        if (!$memberRelation) {
            return false;
        }

        // set member data
        $memberData = $memberRelation->getMemberData();
        $memberData->set($key, $value);
        $memberRelation->setMemberData($memberData);

        // persist
        $this->entityManager->persist($memberRelation);
        $this->entityManager->flush();

    }

    /**
     * Get id of member entity
     * @param MemberObject $member
     * @return integer
     */
    public function getMemberEntityId(MemberObject $member) {
        // get member entity
        $memberRelation = $this->getMemberEntity($member);
        if (!$memberRelation) {
            return null;
        }
        return $memberRelation->getId();
    }

    /**
     * Get ORCID for given member object
     * @param MemberObject $member
     * @return string|null
     */
    public function getMemberOrcid(MemberObject $member)
    {
        // get member entity
        $memberRelation = $this->getMemberEntity($member);
        if (!$memberRelation) {
            return null;
        }
        return $memberRelation->getOrcidUserId();
    }

    /**
     * Set ORCID for given member object
     * @param MemberObject $member
     * @param string $value
     */
    public function setMemberOrcid(MemberObject $member, $value = null)
    {
        // get member entity
        $memberRelation = $this->getMemberEntity($member);
        if (!$memberRelation) {
            return null;
        }

        // set value
        $memberRelation->setOrcidUserId($value);

        // persist
        $this->entityManager->persist($memberRelation);
        $this->entityManager->flush();

    }

    /**
     * Place given member in the correct eZPublish user
     * group based on their current status
     * @param MemberObject $member
     * @param integer $memberAccessLevel  Access level for department members
     */
    public function setMemberUserGroup(MemberObject $member, $memberAccessLevel = null)
    {

        // ezuser
        if ($member->getMemberStatus() == MemberObject::MEMBER_STATUS_EZUSER) {
            return;
        }

        // get current user group
        $currentUserGroups = $this->ezUserService->loadUserGroupsOfUser( $member->getEzUser() );

        // join a group as needed
        switch ($member->getMemberStatus())
        {
            case MemberObject::MEMBER_STATUS_JOIN:
            {

                $userGroup = $this->ezUserService->loadUserGroup(self::JOIN_PROGRESS_USER_GROUP);
                $hasUserGroup = false;
                foreach ($currentUserGroups as $currentUserGroup) {
                    if ($currentUserGroup->contentInfo->id == $userGroup->contentInfo->id) {
                        $hasUserGroup = true;
                        break;
                    }
                }
                if (!$hasUserGroup) {
                    $this->ezUserService->assignUserToUserGroup($member->getEzUser(), $userGroup); 
                }

                break;
            }

            case MemberObject::MEMBER_STATUS_FULL:
            {

                $userGroup = $this->ezUserService->loadUserGroup(self::MEMBER_USER_GROUP);
                $hasUserGroup = false;
                foreach ($currentUserGroups as $currentUserGroup) {
                    if ($currentUserGroup->contentInfo->id == $userGroup->contentInfo->id) {
                        $hasUserGroup = true;
                        break;
                    }
                }
                if (!$hasUserGroup) {
                    $this->ezUserService->assignUserToUserGroup($member->getEzUser(), $userGroup);             
                }
                
                break;
            }

            case MemberObject::MEMBER_STATUS_DEPARTMENT:
            {

                $memberApiData = $member->getApiData();
                switch (
                    trim(strtolower($memberApiData->get("authentication[member_type]", null, true)))
                ) {

                    case "ade":
                    case "both":
                    case "all":
                    {
                        $userGroup = null;
                        switch($memberAccessLevel)
                        {
                            case 1:
                            {
                                $userGroup = $this->ezUserService->loadUserGroup(self::DEPARTMENT_ADE_LEVEL1_USER_GROUP);
                                break;
                            }
                            case 2:
                            {
                                $userGroup = $this->ezUserService->loadUserGroup(self::DEPARTMENT_ADE_LEVEL2_USER_GROUP);
                                break;
                            }               
                        }
                        $hasUserGroup = false;
                        foreach ($currentUserGroups as $currentUserGroup) {
                            if ($currentUserGroup->contentInfo->id == $userGroup->contentInfo->id) {
                                $hasUserGroup = true;
                                break;
                            }
                        }
                        if (!$hasUserGroup) {
                            $this->ezUserService->assignUserToUserGroup($member->getEzUser(), $userGroup);             
                        }

                    }
                }

                switch (
                    trim(strtolower($memberApiData->get("authentication[member_type]", null, true)))
                ) {

                    case "adfl":
                    case "both":
                    case "all":
                    {
                        $userGroup = null;
                        switch($memberAccessLevel) 
                        {
                            case 1:
                            {
                                $userGroup = $this->ezUserService->loadUserGroup(self::DEPARTMENT_ADFL_LEVEL1_USER_GROUP);
                                break;
                            }
                            case 2:
                            {
                                $userGroup = $this->ezUserService->loadUserGroup(self::DEPARTMENT_ADFL_LEVEL2_USER_GROUP);
                                break;
                            }               
                        }
                        $hasUserGroup = false;
                        foreach ($currentUserGroups as $currentUserGroup) {
                            if ($currentUserGroup->contentInfo->id == $userGroup->contentInfo->id) {
                                $hasUserGroup = true;
                                break;
                            }
                        }
                        if (!$hasUserGroup) {
                            $this->ezUserService->assignUserToUserGroup($member->getEzUser(), $userGroup);             
                        }
                    }

                }
                break;
            }
        }

        // delete user from groups they are no longer apart of
        foreach ($currentUserGroups as $userGroup) {

            switch ($userGroup->contentInfo->id)
            {
                case self::MEMBER_USER_GROUP:
                {
                    if ($member->getMemberStatus() != MemberObject::MEMBER_STATUS_FULL) {
                        $this->ezUserService->unAssignUserFromUserGroup($member->getEzUser(), $userGroup);
                    }
                    break;
                }
                case self::JOIN_PROGRESS_USER_GROUP:
                {
                    if ($member->getMemberStatus() != MemberObject::MEMBER_STATUS_JOIN) {
                        $this->ezUserService->unAssignUserFromUserGroup($member->getEzUser(), $userGroup);  
                    }
                    break;
                }
                case self::DEPARTMENT_ADE_LEVEL1_USER_GROUP:
                case self::DEPARTMENT_ADE_LEVEL2_USER_GROUP:
                case self::DEPARTMENT_ADFL_LEVEL1_USER_GROUP:
                case self::DEPARTMENT_ADFL_LEVEL2_USER_GROUP:
                {
                    if ($member->getMemberStatus() != MemberObject::MEMBER_STATUS_DEPARTMENT) {
                        $this->ezUserService->unAssignUserFromUserGroup($member->getEzUser(), $userGroup);
                    } else {

                        $memberApiData = $member->getApiData();
                        switch ($userGroup->contentInfo->id)
                        {
                            case self::DEPARTMENT_ADE_LEVEL1_USER_GROUP:
                            case self::DEPARTMENT_ADE_LEVEL2_USER_GROUP:
                            {
                                if (
                                    !in_array(
                                        trim(strtolower($memberApiData->get("authentication[member_type]", null, true))),
                                        array(
                                            "ade", "all", "both"
                                        )
                                    )
                                ) {
                                    $this->ezUserService->unAssignUserFromUserGroup($member->getEzUser(), $userGroup);
                                }
                                break;
                            }
                            case self::DEPARTMENT_ADFL_LEVEL1_USER_GROUP:
                            case self::DEPARTMENT_ADFL_LEVEL2_USER_GROUP:
                            {
                                if (
                                    !in_array(
                                        trim(strtolower($memberApiData->get("authentication[member_type]", null, true))),
                                        array(
                                            "adfl", "all", "both"
                                        )
                                    )
                                ) {
                                    $this->ezUserService->unAssignUserFromUserGroup($member->getEzUser(), $userGroup);
                                }
                                break;
                            }
                        }
                    }
                    break;
                }
            }

        }

    }

    /**
     * Create a new member from member API data
     * @param MemberApi $memberApiData
     * @param integer $accessLevel  Used for department members
     */
    public function createNewMember(MemberApi $memberApiData, $accessLevel = 1)
    {
        $userGroups = array();
        switch($memberApiData->getMemberSourceId())
        {
            case MemberApi::MEMBER_SOURCE_MLA:

                // get user group
                if ($memberApiData->get("authentication[membership_status]", null, true) != "active" || !$memberApiData->get("membership[class_code]", null, true)) {
                    $userGroups[] = $this->ezUserService->loadUserGroup(self::JOIN_PROGRESS_USER_GROUP);
                } else {
                    $userGroups[] = $this->ezUserService->loadUserGroup(self::MEMBER_USER_GROUP);
                }        

                // create new user
                $userCreate = $this->ezUserService->newUserCreateStruct(
                    $memberApiData->get("id"),
                    $memberApiData->get("general[email]", null, true) ?: "no-email@mla.org",
                    self::generateMemberPassword($memberApiData),
                    "eng-US"
                );

                $userCreate->setField("first_name", $memberApiData->get("general[first_name]", null, true));
                $userCreate->setField("last_name", $memberApiData->get("general[last_name]", null, true));


                break;

            case MemberApi::MEMBER_SOURCE_DEPARTMENTS:

                switch(
                    trim(strtolower($memberApiData->get("authentication[member_type]", null, true)))
                ) {
                    case "ade":
                    case "both":
                    case "all":
                    {
                        switch ($accessLevel) 
                        {
                            case 1:
                            {
                                $userGroups[] = $this->ezUserService->loadUserGroup(self::DEPARTMENT_ADE_LEVEL1_USER_GROUP);
                                break;
                            }
                            case 2:
                            {
                                $userGroups[] = $this->ezUserService->loadUserGroup(self::DEPARTMENT_ADE_LEVEL2_USER_GROUP);
                                break;
                            }
                        }   
                    }
                }

                switch(
                    trim(strtolower($memberApiData->get("authentication[member_type]", null, true)))
                ) {
                    case "adfl":
                    case "both":
                    case "all":
                    {
                        switch ($accessLevel) 
                        {
                            case 1:
                            {
                                $userGroups[] = $this->ezUserService->loadUserGroup(self::DEPARTMENT_ADFL_LEVEL1_USER_GROUP);
                                break;
                            }
                            case 2:
                            {
                                $userGroups[] = $this->ezUserService->loadUserGroup(self::DEPARTMENT_ADFL_LEVEL2_USER_GROUP);
                                break;
                            }
                        }   
                    }
                }

                // create new user
                $userCreate = $this->ezUserService->newUserCreateStruct(
                    $accessLevel . "_" . $memberApiData->get("id"),
                    "departments@mla.org",
                    self::generateMemberPassword($memberApiData),
                    "eng-US"
                );

                $userCreate->setField("first_name", $memberApiData->get("general[name]", null, true));
                $userCreate->setField("last_name", $memberApiData->get("general[affiliation]", null, true));
                break;
        }

        // create user
        $ezUser = $this->ezUserService->createUser(
            $userCreate,
            $userGroups
        );

        // create relation
        $memberEntity = new MemberEntity();
        $memberEntity->setEzUserId($ezUser->contentInfo->id);
        $memberEntity->setMemberApiId($memberApiData->get("id"));
        $memberEntity->setMemberSourceId($memberApiData->getMemberSourceId());
        $memberEntity->setMemberAccessLevel($accessLevel);
        //$memberEntity->setMemberData($memberApiData);
            
        // persist relation
        $this->entityManager->persist($memberEntity);
        $this->entityManager->flush();

    }

    /**
     * Generate password to use internally
     * @param MemberApi $memberApiData
     * @return string
     */
    static public function generateMemberPassword(MemberApi $memberApiData) {
        return hash(
            "sha256",
            $memberApiData->get("id") . $memberApiData->get("membership[year_joined]", null, true) . self::HASH_SECRET
        );
    }

    /**
     * Generate a membership receipt for given member
     * for given year
     * @param MemberObject $member
     * @param integer $year
     * @throws InvalidArgumentException
     * @return array
     */
    public function getMemberReceipt(MemberObject $member, $year)
    {

        // must be MLA member
        if (
            !in_array($member->getMemberStatus(), array(MemberObject::MEMBER_STATUS_JOIN, MemberObject::MEMBER_STATUS_FULL)
            )
        ) {
            throw new \InvalidArgumentException("Invalid member type. Must be MLA member.");
        }

        // year must be defined and be a number
        if (!$year || !is_numeric($year)) {
            throw new \InvalidArgumentException("Year must be a numeric value.");
        }

        // get api data
        $memberApiData = $member->getApiData();

        // find dues for given year
        $dues = null;
        foreach ($memberApiData->get("dues_history") as $dues) {
            if (intval($dues['membership_year']) == intval($year)) {
                break;
            }
        }

        // lookup class codes to get dues amount
        $duesAmount = 0;
        if ($dues['date_paid'] && $dues['class_code']) {
            $classCodeLookup = $this->api->getLookup(
                "class_codes",
                array(
                    "year" => $year,
                    "member_id" => $member->getMemberApiId()
                )
            )->all();

            if (isset($classCodeLookup['data'])) {
                foreach ($classCodeLookup['data'] as $class) {
                    if ($class['id'] == $dues['class_code']) {
                        $duesAmount = intval($class['rate']);
                        break;
                    }
                }
            }
        }

        // get contributions
        $contributions = array();
        $contributionTotal = 0;

        if (is_array($memberApiData->get("contributions_history"))) {
            foreach ($memberApiData->get("contributions_history") as $contribution) {
                if (intval($contribution["contribution_year"]) != intval($year)) {
                    continue;
                }
                $contributions[] = array(
                    "name" => $contribution['name'],
                    "type" => $contribution['type'],
                    "fund_code" => $contribution['fund_code'],
                    "date_paid" => strtotime($contribution['date_paid']),
                    "amount" => $contribution['amount']
                );
                $contributionTotal += intval($contribution['amount']);
            }
        }

        // get publications
        $publications = array();
        $publicationTotal = 0;

        if (is_array($memberApiData->get("publications_history"))) {
            foreach ($memberApiData->get("publications_history") as $publication) {
                if (intval($publication['year']) != intval($year)) {
                    continue;
                }
                $publications[] = array(
                    "name" => $publication['year'],
                    "price" => $publication['price'],
                    "pub_code" => $publication['pub_code']
                );
                $publicationTotal += intval($publication['price']);
            }
        }


        return array(
            "year" => intval($year),
            "dues" => array(
                "class_code" => $dues['class_code'],
                "amount" => $duesAmount,
                "date_paid" => strtotime($dues['date_paid'])
            ),
            "contributions" => $contributions,
            "contribution_total" => $contributionTotal,
            "publications" => $publications,
            "publication_total" => $publicationTotal
        );

    }

    /**
     * Return current year that a user can join/renew.
     * @return integer
     */
    public function getCurrentSignupYear()
    {

        // lookup current membership year        
        $membershipYear = $this->api->getLookup(
            "membership_year"
        )->all();

        // api results
        if (isset($membershipYear[0]['membership_year'])) {
            return $membershipYear[0]['membership_year'];
        } elseif (isset($membershipYear['membership_year'])) {
            return $membershipYear['membership_year'];
        }

        // otherwise guess year if nothing from api
        // beginning half of the year        
        if ( intval(date("n")) < 7 ) {
            return intval(date("Y"));

        // last half of year (year + 1)
        } else {
            return intval(date("Y")) + 1;
        }

    }

    /**
     * Post member dues from a join/renew order to API
     * @param MemberObject $member
     * @param Order $order
     */
    public function postMemberDues(MemberObject $member, Order $order)
    {

        // must be MLA member
        if (
            !in_array($member->getMemberStatus(), array(MemberObject::MEMBER_STATUS_JOIN, MemberObject::MEMBER_STATUS_FULL)
            )
        ) {
            throw new \InvalidArgumentException("Invalid member type. Must be MLA member.");
        }

        // get customer details from order
        $customerDetails = $order->getCustomerDetails();

        // look for membership product in order
        foreach ($order->getProductList() as $productId => $quantity) {

            // get product id
            $productId = explode(OrderProcessorService::PRODUCT_VARIANT_SEPERATOR, $productId);

            // has membership item in order
            if ($productId[0] == ProductService::MEMBERSHIP_OBJECT_ID) {

                // send membership to API
                $memberId = $member->getMemberApiId();

                // post member dues
                $this->api->request(
                    "members/{$memberId}/dues",
                    "POST",
                    array(),
                    array(
                        "class_code" => substr($productId[1], 1),
                        "membership_year" => $this->getCurrentSignupYear(),
                        "amount" => $order->getProductFromId($productId[0] . OrderProcessorService::PRODUCT_VARIANT_SEPERATOR . $productId[1])->getPrice(),
                        "joint_member_id" => isset($customerDetails['misc']['jointMemberId']) ? $customerDetails['misc']['jointMemberId'] : null,
                        "renew" => $member->getMemberStatus() == MemberObject::MEMBER_STATUS_JOIN ? "" : "Y",
                        "promotion_code" => isset($customerDetails['misc']['promoCode']) ? $customerDetails['misc']['promoCode'] : ""
                    )
                );

                // post benefits
                $this->postMemberBenefits($member, $order);                

                break;
            }

        }

    }

    /**
     * Post member contributions from an order to API
     * @param MemberObject $member
     * @param Order $order
     * @param string $transaction_reference
     */
    public function postMemberContributions(MemberObject $member, Order $order, $transaction_reference = "")
    {

        // must be MLA member
        if (
            !in_array($member->getMemberStatus(), array(MemberObject::MEMBER_STATUS_JOIN, MemberObject::MEMBER_STATUS_FULL)
            )
        ) {
            throw new \InvalidArgumentException("Invalid member type. Must be MLA member.");
        }

        // get customer details from order
        $customerDetails = $order->getCustomerDetails();

        // look for membership product in order
        $hasMembershipProduct = false;
        foreach ($order->getProductList() as $productId => $quantity) {

            // get product id
            $productId = explode(OrderProcessorService::PRODUCT_VARIANT_SEPERATOR, $productId);

            if ($productId[0] == ProductService::MEMBERSHIP_OBJECT_ID) {
                $hasMembershipProduct = true;
                break;
            }

        }

        // look for contribution product in order
        foreach ($order->getProductList() as $productId => $quantity) {

            // get product id
            $productId = explode(OrderProcessorService::PRODUCT_VARIANT_SEPERATOR, $productId);

            // has donation/contribution product in order
            if ($productId[0] == ProductService::DONATION_OBJECT_ID && $member) {

                // send membership to API
                $memberId = $member->getMemberApiId();

                $this->api->request(
                    "members/{$memberId}/contributions",
                    "POST",
                    array(),
                    array(
                        "contributor_type" => "M",
                        "fund_code" => substr($productId[1], 1),
                        "amount" => $order->getProductFromId($productId[0] . OrderProcessorService::PRODUCT_VARIANT_SEPERATOR . $productId[1])->getPrice() * $quantity,
                        "transaction_id" => $transaction_reference,
                        "total_amount" => $order->getTotalPrice(true, true),
                        "join_renew" => $hasMembershipProduct ? "Y" : ""
                    )
                );


            }

        }

    }

    /**
     * Search for products in order that are considered member benefits
     * and report to API
     * @param MemberObject $member
     * @param Order $order
     */
    public function postMemberBenefits(MemberObject $member, Order $order)
    {

        // must be MLA member
        if (
            !in_array($member->getMemberStatus(), array(MemberObject::MEMBER_STATUS_JOIN, MemberObject::MEMBER_STATUS_FULL)
            )
        ) {
            throw new \InvalidArgumentException("Invalid member type. Must be MLA member.");
        }

        $availableBenefitVariants = $this->api->request(
            "members/" . $member->getMemberApiId() . "/benefits"
        );
        if (!isset($availableBenefitVariants['data'][0]['publication_codes'])) {
            return;
        }
        $availableBenefitVariants = $availableBenefitVariants['data'][0]['publication_codes'];

        // look for benefit products
        $benefitVariantsInOrder = array();
        foreach ($order->getProductList() as $productId => $quantity) {

            // get product id
            $productId = explode(OrderProcessorService::PRODUCT_VARIANT_SEPERATOR, $productId);

            if (in_array($productId[1], $availableBenefitVariants)) {
                $benefitVariantsInOrder[] = $productId[1];
            }

        }

        // post to api
        if ($benefitVariantsInOrder) {
            $this->api->request(
                "members/" . $member->getMemberApiId() . "/benefits",
                "POST",
                array(),
                array(
                    "publication_codes" => $benefitVariantsInOrder
                )
            );
        }

    }

    /**
     * Add member to given organizations
     * @param MemberObject $member
     * @param array $items
     * @param boolean $primary
     */
    public function postMemberOrganizations(MemberObject $member, $items = array(), $primary = false)
    {

        // must be MLA member
        if (
            !in_array($member->getMemberStatus(), array(MemberObject::MEMBER_STATUS_JOIN, MemberObject::MEMBER_STATUS_FULL)
            )
        ) {
            throw new \InvalidArgumentException("Invalid member type. Must be MLA member.");
        }

        $this->api->request(
            "members/" . $member->getMemberApiId() . "/organizations",
            "POST",
            array(
                "items" => implode(",", $items),
                "primary" => $primary ? "Y" : null
            ),
            array(
                "pointless" => "Y"
            )
        );

    }

    /**
     * Remove member from given organizations
     * @param MemberObject $member
     * @param array $items
     * @param boolean $primary
     */
    public function deleteMemberOrganizations(MemberObject $member, $items = array(), $primary = false)
    {

        // must be MLA member
        if (
            !in_array($member->getMemberStatus(), array(MemberObject::MEMBER_STATUS_JOIN, MemberObject::MEMBER_STATUS_FULL)
            )
        ) {
            throw new \InvalidArgumentException("Invalid member type. Must be MLA member.");
        }

        $this->api->request(
            "members/" . $member->getMemberApiId() . "/organizations",
            "DELETE",
            array(
                "items" => implode(",", $items),
                "primary" => $primary ? "Y" : null
            ),
            array(
                "pointless" => "Y"
            )
        );

    }

}
