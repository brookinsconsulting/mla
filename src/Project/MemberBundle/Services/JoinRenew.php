<?php

namespace Project\MemberBundle\Services;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use eZ\Publish\Core\Repository\ContentService;
use Omnipay\Common\CreditCard;
use Omnipay\Common\Exception\InvalidCreditCardException;
use ThinkCreative\PaymentBundle\Entity\Order;
use ThinkCreative\PaymentBundle\Services\OrderService;
use ThinkCreative\PaymentBundle\Exception\Order\PaymentGatewayErrorException;
use Project\ApiBundle\Services\Api as ApiService;
use Project\ApiBundle\Exception\ApiResponseException;
use Project\MemberBundle\Services\Member as MemberService;
use Project\MemberBundle\Classes\Member as MemberObject;
use Project\ProductBundle\Services\Shopify;
use Project\ProductBundle\Services\Product as ProductService;
use Project\ProductBundle\Order\OrderProcessorService;
use Project\ProductBundle\Exception\Api\ApiResponseException as ShopifyApiResponseException;

class JoinRenew
{

    /**
     * Unique identifer for join/renew order in
     * shopping cart system as to not conflict
     * with other shopping carts (such as bookstore)
     * @var string
     */
    const ORDER_TYPE = "membership";

    /**
     * Define gateways to use for payment
     * @var string
     */
    const CREDIT_CARD_GATEWAY = "AuthorizeNetSandbox";
    const PAYPAL_GATEWAY = "PaypalSandbox";

    /**
     * Steps in join/renew process
     * @var integer
     */
    const JOIN_INTRO = 0;
    const JOIN_MEMBERSHIP_CATEGORY = 1;
    const JOIN_IDENTIFICATION = 2;
    const JOIN_CONTACT = 3;
    const JOIN_FORUM = 4;
    const JOIN_CAREER_PATH = 5;
    const JOIN_ADDITIONAL_OPTIONS = 6;
    const JOIN_MEMBERSHIP_REVIEW = 7;
    const JOIN_PAYMENT = 8;
    const JOIN_COMPLETE = 9;

    /**
     * @var Project\ApiBundle\Service\Api
     */
    protected $apiService;

    /**
     * @var Project\MemberBundle\Services\Member
     */
    protected $memberService;

    /**
     * @var Project\MemberBundle\Classes\Member|null
     */
    protected $member;

    /**
     * @var ThinkCreative\PaymentBundle\Services\OrderService
     */
    protected $orderService;

    /**
     * @var Project\ProductBundle\Services\Shopify
     */
    protected $shopifyService;

    /**
     * @var ThinkCreative\PaymentBundle\Entity\Order
     */
    protected $order;

    /**
     * @var Symfony\Component\HttpFoundation\Session\Session
     */
    protected $session;

    /**
     * @var eZ\Publish\Core\Repository\ContentService
     */
    protected $contentService;

    /**
     * @var array
     */
    protected $configuration;

    /**
     * Constructor.
     */
    public function __construct(
        ApiService $apiService,
        MemberService $memberService,
        OrderService $orderService,
        Shopify $shopifyService,
        OrderProcessorService $orderProcessorService,
        Session $session,
        ContentService $contentService,
        array $configuration
    ) {

        $this->apiService = $apiService;
        $this->memberService = $memberService;
        $this->member = $this->memberService->getCurrentMember();
        $this->orderService = $orderService;
        $this->shopifyService = $shopifyService;
        $this->session = $session;
        $this->contentService =$contentService;
        $this->configuration = $configuration;

        // restore order from member profile
        if ($this->member) {
            $sessionOrders = $this->session->get("thinkcreative_payment_order");

            // kill order in session if not eligible for join/renew
            if (!$this->isValidUser() && isset($sessionOrders[self::ORDER_TYPE])) {
                $this->session->set("thinkcreative_payment_order", null);

            // store order to session
            } else if (!isset($sessionOrders[self::ORDER_TYPE])) {
                $sessionOrders[self::ORDER_TYPE] = $this->memberService->getDataFromMember($this->member, "order");
                $this->session->set("thinkcreative_payment_order", $sessionOrders);
            }
        }

        // get order
        $this->order = $orderService->getUserOrder(
            $orderProcessorService,
            self::ORDER_TYPE
        );

        // build order for existing member, renewal
        $orderDetails = $this->order->getCustomerDetails();
        if (
            $this->member &&
            !$this->isEzUser() &&
            $this->getJoinStep() == JoinRenew::JOIN_INTRO
        ) {
            $this->memberApi = $this->member->getApiData();
            $this->order->setCustomerDetails(
                $this->memberApi->get("general[first_name]", null, true),
                $this->memberApi->get("general[last_name]", null, true),
                $this->memberApi->get("general[email]", null, true),
                array(
                    "joinStep" => self::JOIN_MEMBERSHIP_CATEGORY
                )
            );
            $this->persistOrder();
        }

    }

    /**
     * Check if current user is an eZ Publish user
     * which is not linked to MLA
     * @return boolean
     */
    public function isEzUser()
    {
        $this->member = $this->memberService->getCurrentMember();
        if (!$this->member) {
            return false;
        }
        if ($this->member->getMemberStatus() == MemberObject::MEMBER_STATUS_EZUSER) {           
            return true;
        }
        return false;
    }

    /**
     * Check if current user is a full member
     * @return boolean
     */
    public function isFullMember()
    {
        $this->member = $this->memberService->getCurrentMember();
        if (!$this->member) {
            return false;
        }
        if ($this->member->getMemberStatus() == MemberObject::MEMBER_STATUS_FULL) {
            return true;
        }
        return false;
    }

    /**
     * Check is user is eligible to go through the
     * join/renew process
     * @return boolean
     */
    public function isValidUser()
    {
        $this->member = $this->memberService->getCurrentMember();
        if (!$this->member) { return true; }
        return !$this->isEzUser() && !$this->isFullMember();
    }

    /**
     * Current user is a member (full or join in progress)
     * @return boolean
     */
    public function isMember()
    {
        return $this->memberService->getCurrentMember() ? true : false;
    }

    /**
     * Return false if a joint member id is registered but is
     * not longer valid
     * @return boolean
     */
    public function hasValidJointMember()
    {
        $jointMemberId = $this->getJointMemberId();

        // no joint member is considered a valid state
        if (!$jointMemberId) {
            return true;
        }

        try {
            $jointMember = $this->memberService->getMemberFromApiId($jointMemberId);
        } catch(ApiResponseException $e) {
            return false;
        }

        if ($jointMember->getMemberStatus() == MemberObject::MEMBER_STATUS_JOIN) {
            return true;
        }

        return false;
    }

    /**
     * Persist order to session / member profile
     */
    private function persistOrder()
    {
        // persist to session
        $this->orderService->storeUserOrder($this->order);

        // persist order to member if available
        $this->member = $this->memberService->getCurrentMember();
        if ($this->member) {
            $sessionOrders = $this->session->get("thinkcreative_payment_order");
            $this->memberService->persistDataToMember($this->member, "order", $sessionOrders['membership']);
        }
    }

    /**
     * Get order detail value
     * @param string $key
     * @return (any)
     */
    public function getOrderDetail($key)
    {
        $orderDetails = $this->order->getCustomerDetails();
        if (
            in_array(
                $key, 
                array(
                    "firstName",
                    "lastName",
                    "email"
                )
            )
        ) {
            return isset($orderDetails[$key]) ? $orderDetails[$key] : null;
        }
        return isset($orderDetails['misc'][$key]) ? $orderDetails['misc'][$key] : null;
    }

    /**
     * Set order details as key/value
     * @param string $key
     * @param (any) $value
     */
    private function setOrderDetail($key, $value = null)
    {
        $orderDetails = $this->order->getCustomerDetails();
        if (
            in_array(
                $key, 
                array(
                    "firstName",
                    "lastName",
                    "email"
                )
            )
        ) {
            $orderDetails[$key] = $value;
        } else {
            $orderDetails['misc'][$key] = $value;
        }
        $this->order->setCustomerDetails(
            $orderDetails['firstName'],
            $orderDetails['lastName'],
            $orderDetails['email'],
            $orderDetails['misc']
        );
    }

    /**
     * Get current step in join/renew process
     * @return integer
     */
    public function getJoinStep()
    {
        $orderDetails = $this->order->getCustomerDetails();
        return isset($orderDetails['misc']['joinStep']) ? $orderDetails['misc']['joinStep'] : self::JOIN_INTRO;
    }

    /**
     * Ensure join step amount is valid
     * @param integer $joinStep
     * @throws InvalidArgumentException
     */
    private function validateJoinStep($joinStep = 0)
    {
        if (!is_integer($joinStep)) {
            throw new \InvalidArgumentException("Join step must be an integer.");
        }
        if ($joinStep < 0) {
            throw new \InvalidArgumentException("Join step must be greater than zero.");   
        } 
    }

    /**
     * Set current step in join/renew process
     * @param integer $joinStatus
     * @throws InvalidArgumentException
     */
    public function setJoinStep($joinStep = 0)
    {
        $this->validateJoinStep($joinStep);
        $orderDetails['misc']['joinStep'] = intval($joinStep);
        $this->setOrderDetail("joinStep", intval($joinStep));
        $this->persistOrder();
    }

    /**
     * Use form results to find other members with the
     * same 'basic information'
     * @param Form $form
     * @return array
     * @throws ApiResponseException
     */
    public function findExistingMembers($form)
    {
        // perform member search api request
        $this->memberSearch = array();

        try {
            $this->memberSearchName = $this->apiService->request(
                "members",
                "GET",
                array(
                    "first_name" => trim($form->get("first_name")->getData()),
                    "last_name" => trim($form->get("last_name")->getData()),
                    "membership_status" => "all"
                )
            );
            $this->memberSearch = $this->memberSearchName['data'];

        } catch (ApiResponseException $e) {
            if ($e->getStatusCode() != "API-2102") {
                throw $e;
            }
        }

        try {
            $this->memberSearchEmail = $this->apiService->request(
                "members",
                "GET",
                array(
                    "email" => trim($form->get("email")->getData())
                )
            );
            $this->memberSearch = array_replace($this->memberSearch, $this->memberSearchEmail['data']);
        } catch (ApiResponseException $e) {
            if ($e->getStatusCode() != "API-2102") {
                throw $e;
            }
        }
        return $this->memberSearch;
    }

    /**
     * Get basic information about user
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     */
    public function storeBasicInformation($firstName, $lastName, $email)
    {
        $this->order->setCustomerDetails(
            trim($firstName),
            trim($lastName),
            trim($email),
            array(
                'joinStep' => self::JOIN_MEMBERSHIP_CATEGORY
            )
        );
        $this->persistOrder();
    }

    /**
     * Check if promo code is valid
     * @param string $promoCode
     * @return boolean
     */
    public function isValidPromoCode($promoCode = "")
    {
        if (!isset($this->configuration['promo_codes'])) {
            return false;
        }
        foreach($this->configuration['promo_codes'] as $promoConf) {
            if (!isset($promoConf['code'])) {
                continue;
            }
            if (trim($promoCode) == $promoConf['code']) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get order promo code
     * @return string
     */
    public function getPromoCode()
    {
        $orderDetails = $this->order->getCustomerDetails();
        return isset($orderDetails['misc']['promoCode']) ? $orderDetails['misc']['promoCode'] : "";
    }

    /**
     * Return list of available promo codes
     * @return array
     */
    public function getAvailablePromoCodes()
    {
        if (isset($this->configuration['promo_codes'])) {
            return $this->configuration['promo_codes'];
        }
        return array();
    }

    /**
     * Set promo code for order
     * @param string $promoCode
     */
    public function setPromoCode($promoCode = "")
    {
        if (trim($promoCode) && !$this->isValidPromoCode($promoCode)) {
            return;
        }
        $orderDetails = $this->order->getCustomerDetails();
        $orderDetails['misc']['promoCode'] = trim($promoCode);
        $this->persistOrder();
    }

    /**
     * Get selected class code for order
     * @return string|null
     */
    public function getMemberClassCode()
    {
        // look for membership product in order
        foreach ($this->order->getProductList() as $productId=>$quantity) {
            $productId = explode(OrderProcessorService::PRODUCT_VARIANT_SEPERATOR, $productId);
            if ($productId[0] == ProductService::MEMBERSHIP_OBJECT_ID) {
                return substr($productId[1], 1);
            }
        }
        return null;
    }

    /**
     * Add given class to order
     * @param string $classCode
     * @param string|null $memberShipCategory
     * @param integer|null $jointMemberId
     */
    public function addMemberClassToOrder($classCode = "", $memberShipCategory = null, $jointMemberId = null)
    {

        if (!$classCode) {
            return;
        }

        // remove previous membership products
        foreach ($this->order->getProductList() as $productId=>$quantity) {
            $productId = explode(OrderProcessorService::PRODUCT_VARIANT_SEPERATOR, $productId);
            if ($productId[0] == ProductService::MEMBERSHIP_OBJECT_ID) {
                $this->order->removeProduct($productId[0] . OrderProcessorService::PRODUCT_VARIANT_SEPERATOR . $productId[1]);
            }
        }

        // add membership product to order
        $memberShipProductId = ProductService::MEMBERSHIP_OBJECT_ID;
        $this->order->setQuantity("{$memberShipProductId}". OrderProcessorService::PRODUCT_VARIANT_SEPERATOR ."_{$classCode}", 1);

        // other values
        $orderDetails = $this->order->getCustomerDetails();
        $this->setOrderDetail("memberShipCategory", $memberShipCategory);
        $this->setOrderDetail("jointMemberId", $jointMemberId);

        if ($jointMemberId) {
            $this->order->setExtraFee("joint-membership", $this->configuration['joint_membership_fee']);
        } else {
            $this->order->setExtraFee("joint-membership", 0);
        }

        $this->persistOrder();

    }

    /**
     * Get membership category
     * @return string
     */
    public function getMembershipCategory()
    {
        $orderDetails = $this->order->getCustomerDetails();
        return isset($orderDetails['misc']['membershipCategory']) ? $orderDetails['misc']['membershipCategory'] : "";
    }

    /**
     * Get joint member id
     * @return integer|null
     */
    public function getJointMemberId()
    {
        $orderDetails = $this->order->getCustomerDetails();
        return isset($orderDetails['misc']['jointMemberId']) ? $orderDetails['misc']['jointMemberId'] : null;
    }

    /**
     * Return last year member paid for membership
     * @return integer|null
     */
    public function getMemberLastPaidYear()
    {
        if (!$this->member) {
            return null;
        }
        return intval($this->member->getApiData()->get("membership[latest_paid_year]", null, true)) ?: null;
    }

    /**
     * Get current membership signup year
     * @return integer
     */
    public function getCurrentSignupYear()
    {
        return $this->memberService->getCurrentSignupYear();
    }

    /**
     * Return current user ORCID
     * @return string|null
     */
    public function getOrcid()
    {
        if (
            !$this->member ||
            $this->member->getMemberStatus() == MemberObject::MEMBER_STATUS_JOIN
        ) {
            $orderDetails = $this->order->getCustomerDetails();
            if (!isset($orderDetails['misc']['orcid'])) {
                return null;
            }
            return $orderDetails['misc']['orcid'];
        }
        return $this->memberService->getMemberOrcid($this->member);
    }

    /**
     * Set ORCID for current order and member
     * @param string $orcid
     */
    public function setOrcid($orcid = null)
    {
        if ($this->member) {
            $this->memberService->setMemberOrcid(
                $this->member,
                $orcid
            );
        }
        $this->setOrderDetail("orcid", $orcid);
        $this->persistOrder();
    }

    /**
     * Get available benefits variants
     * @return array
     */
    public function getAvailableBenefitVariants()
    {
        if ($this->isMember()) {
            $availableBenefitVariants = $this->apiService->request(
                "members/" . $this->member->getMemberApiId() . "/benefits"
            );
            return isset($availableBenefitVariants['data'][0]['publication_codes']) ? $availableBenefitVariants['data'][0]['publication_codes'] : array();
        }
        return array();
    }

    /**
     * Check if given content object is a benefit
     * @param integer $contentObjectId
     * @param boolean $throwException  If true exception will be thrown instead of returning false
     * @return boolean
     */
    public function isBenefit($contentObjectId, $throwException = false)
    {

        // get a content id from field value
        if (!$contentObjectId || !is_numeric($contentObjectId)) {
            if ($throwException) {
                throw new \InvalidArgumentException("Content object ID must be a numeric value.");
            }
            return false;
        }
        
        // try to load content object
        $content = $this->contentService->loadContent($contentObjectId);
            
        // ensure content is a product
        if ($content->contentInfo->contentTypeId != ProductService::PRODUCT_CLASS_ID) {
            if ($throwException) {
                throw new \InvalidArgumentException("Content object must be a 'product' content type.");
            }
            return false;
        }

        // ensure product has at least one variant
        $variants = $content->getFieldValue("variants")->values;
        if (count($variants) == 0) {
            if ($throwException) {
                throw new \InvalidArgumentException("Product must contain at least one variant.");
            }
            return false;
        }

        // ensure product variant is an available benefit from api
        if (
            in_array(
                $variants[0]['id'],
                $this->getAvailableBenefitVariants()
            )
        ) {
            return true;
        }
        return false;

    }

    /**
     * Add a benefit to order
     * @param integer $contentObjectId
     */
    public function addBenefit($contentObjectId)
    {
        if (!$this->isBenefit($contentObjectId)) {
            return;
        }
        $content = $this->contentService->loadContent($contentObjectId);
        $variants = $content->getFieldValue("variants")->values;
        $this->order->addProduct(
            $contentObjectId . OrderProcessorService::PRODUCT_VARIANT_SEPERATOR . $variants[0]['id'],
            1
        );
    }

    /**
     * Remove a benefit from order
     * @param integer $contentObjectId
     */
    public function removeBenefit($contentObjectId)
    {
        if (!$this->isBenefit($contentObjectId)) {
            return;
        }
        foreach ($this->order->getProductList() as $productId=>$quantity) {
            $productId = explode(OrderProcessorService::PRODUCT_VARIANT_SEPERATOR, $productId);
            if ($productId[0] == $contentObjectId) {
                $this->order->removeProduct($productId[0] . OrderProcessorService::PRODUCT_VARIANT_SEPERATOR . $productId[1]);   
            }
        }
    }

    /**
     * Remove all donation products in order
     */
    public function removeDonations()
    {
        foreach ($this->order->getProductList() as $productId=>$quantity) {
            $productId = explode(OrderProcessorService::PRODUCT_VARIANT_SEPERATOR, $productId);
            if ($productId[0] == $this->configuration['donation_product_object_id']) {
                $this->order->removeProduct($productId[0] . OrderProcessorService::PRODUCT_VARIANT_SEPERATOR . $productId[1]);
            }
        }
    }

    /**
     * Add donation to order
     * @param string $variantId
     * @param integer $amount
     */
    public function addDonation($variantId, $amount)
    {
        // ensure variant is valid
        $content = $this->contentService->loadContent($this->configuration['donation_product_object_id']);
        $variants = $content->getFieldValue("variants")->values;
        $hasVariant = false;
        foreach($variants as $variant) {
            if ($variant['id'] == $variantId) {
                $hasVariant = true;
                break;
            }
        }
        if (!$hasVariant) {
            return;
        }

        // add
        $this->order->removeProduct($this->configuration['donation_product_object_id'] . OrderProcessorService::PRODUCT_VARIANT_SEPERATOR . trim($variantId));
        $this->order->addProduct(
            $this->configuration['donation_product_object_id'] . OrderProcessorService::PRODUCT_VARIANT_SEPERATOR . trim($variantId),
            floor($amount)
        );
    }

    /**
     * Return true is given content object id exists as
     * product in cart
     * @param integer $contentObjectId
     * @param string $variantId  If provided checks that given variant exists with product too
     * @return boolean
     */
    public function hasProductInOrder($contentObjectId, $variantId = "")
    {
        foreach ($this->order->getProductList() as $productId=>$quantity) {
            $productId = explode(OrderProcessorService::PRODUCT_VARIANT_SEPERATOR, $productId);
            if (intval($productId[0]) == intval($contentObjectId)) {
                if (!$variantId) {
                    return true;
                }
                if (trim($productId[1]) == trim($variantId)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns amount donated for given variant or
     * null if not in order
     * @param string $variantId
     * @return integer|null
     */
    public function getDonationAmount($variantId)
    {
        foreach ($this->order->getProductList() as $productId=>$quantity) {
            $productId = explode(OrderProcessorService::PRODUCT_VARIANT_SEPERATOR, $productId);
            if (
                $productId[0] == $this->configuration['donation_product_object_id'] &&
                $productId[1] == trim($variantId)
            ) {
                return $quantity;
            }
        }
        return null;
    }

    /**
     * Return member object
     * @return Member|null
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * Return order object
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set payment data for Omnipay
     * @param string $paymentGateway
     * @param array $paymentData
     * @throws Omnipay\Common\Exception\InvalidCreditCardException
     */
    public function setPaymentData($paymentGateway, $paymentData = array())
    {
        $creditCard = new CreditCard($paymentData);
        $this->setOrderDetail("creditCard", $paymentData);
        $this->setOrderDetail("paymentGateway", $paymentGateway);
        $this->persistOrder();
    }

    /**
     * Authorize payment
     * @param string $returnUrl
     * @param string $cancelUrl
     */
    public function authorizePayment($returnUrl = "", $cancelUrl = "")
    {
        $orderDetails = $this->order->getCustomerDetails();
        if (
            !isset($orderDetails['misc']['paymentGateway']) ||
            !isset($orderDetails['misc']['creditCard'])
        ) {
            return;
        }

        // create omnipay credit card 
        $creditCard = new CreditCard($orderDetails['misc']['creditCard']);

        // authorize payment
        $transactionReference = $this->orderService->authorizePayment(
            $this->order,
            $orderDetails['misc']['paymentGateway'],
            $creditCard,
            $returnUrl,
            $cancelUrl
        );

        return $this->capturePayment($transactionReference);

    }

    /**
     * Capture payment after authorization
     * @param string $transactionReference
     * @throws ShopifyApiResponseException
     * @throws PaymentGatewayErrorException
     */
    private function capturePayment($transactionReference)
    {

        $orderDetails = $this->order->getCustomerDetails();
        if (
            !isset($orderDetails['misc']['paymentGateway']) ||
            !isset($orderDetails['misc']['creditCard'])
        ) {
            return;
        }

        // create omnipay credit card 
        $creditCard = new CreditCard($orderDetails['misc']['creditCard']);

        // create order on Shopify
        try {
            $this->order->getProcessorService()->createShopifyOrder($this->order);
        
        // shopify api exception
        } catch (ShopifyApiResponseException $e) {

            // cancel payment
            if ($orderDetails['misc']['paymentGateway'] != self::PAYPAL_GATEWAY) {
                $this->orderService->cancelPayment(
                    $this->order,
                    $orderDetails['misc']['paymentGateway'],
                    $creditCard,
                    $transactionReference
                );

            // paypal will need a new order id
            } else {

                // generate new order id
                $this->order->setOrderIdentifier(
                    strtoupper(
                        substr(
                            hash("sha256", Order::TRANSACTION_SECRET.time().$_SERVER['HTTP_USER_AGENT'].$_SERVER['REMOTE_ADDR']),
                            0,
                            10
                        )
                    )
                );

                // persist order
                $this->persistOrder();
            }

            // rethrow
            throw $e;
        }

        // capture payment
        try {
            $this->orderService->capturePayment(
                $this->order,
                $orderDetails['misc']['paymentGateway'],
                $creditCard,
                $transactionReference
            );

        // payment error
        } catch(PaymentGatewayErrorException $e) {

            // cancel shopify order
            $this->order->getProcessorService()->cancelShopifyOrder($this->order);

            // cancel payment
            if ($orderDetails['misc']['paymentGateway'] != self::PAYPAL_GATEWAY) {
                $this->orderService->cancelPayment(
                    $this->order,
                    $orderDetails['misc']['paymentGateway'],
                    $creditCard,
                    $transactionReference
                );
            }

            // rethrow
            throw $e;

        }

        // clear order
        $this->orderService->clearUserOrder($this->order);
    }

    /**
     * Capture payment after redirect
     * @param string $token
     */
    public function completeRedirectPayment($token)
    {

        $orderDetails = $this->order->getCustomerDetails();
        if (
            !isset($orderDetails['misc']['paymentGateway']) ||
            !isset($orderDetails['misc']['creditCard'])
        ) {
            return;
        }

        // create omnipay credit card 
        $creditCard = new CreditCard($orderDetails['misc']['creditCard']);

        // complete authorization
        $transactionReference = $this->orderService->completeAuthorize(
            $this->order, 
            $orderDetails['misc']['paymentGateway'],
            $creditCard,
            $token
        );

        return $this->capturePayment($transactionReference);

    }

    /**
     * Get order info from shopify
     */
    public function getShopifyOrder($orderId)
    {
        // request order data
        $orderResponse = $this->shopifyService->getShopifyOrder($orderId);

        // ensure member associated with order is current member
        $orderMember = null;
        if ($this->member && isset($orderResponse['order']['note_attributes'])) {
            foreach ($orderResponse['order']['note_attributes'] as $noteAttr) {
                if ($noteAttr['name'] == "member_id") {
                    $orderMember = $this->memberService->getMemberFromApiId(
                        $noteAttr['value']
                    );
                    break;
                }
            }
        } 
        if (!$this->member || !$orderMember || $orderMember->getMemberApiId() != $this->member->getMemberApiId()) {
            throw new AccessDeniedException("Cannot access order data for another user.");
        }

        // request transaction data
        $transactionResponse = $this->shopifyService->getShopifyTransactions($orderId);

        return array_merge($orderResponse, $transactionResponse);

    }

    /**
     * Clear joint member from order
     */
    public function clearJointMember()
    {
        $this->setOrderDetail("jointMemberId", null);
        $this->order->setExtraFee("joint-membership", 0);
        $this->persistOrder();
    }

}