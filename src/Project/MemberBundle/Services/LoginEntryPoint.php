<?php

namespace Project\MemberBundle\Services;

use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Csrf\CsrfProvider\CsrfProviderInterface;
use Twig_Environment;

class LoginEntryPoint implements AuthenticationEntryPointInterface
{

    protected $twigEnvironment;
    protected $csrfProvider;

    public function __construct(Twig_Environment $twigEnvironment, CsrfProviderInterface $csrfProvider)
    {
        $this->twigEnvironment = $twigEnvironment;
        $this->csrfProvider = $csrfProvider;
    }

    /**
     * Called when authentication exception occurs. Handles
     * exception and returns response.
     */
    public function start(Request $request, AuthenticationException $authException = null, $navigationLoginAttempt = false)
    {

        // create response
        $response = new Response();
        $response->setContent(
            $this->twigEnvironment->render(
                "ProjectMemberBundle:full:misc/login.html.twig",
                array(
                    "error" => array(
                        "message" => $authException->getMessage() ?: $authException->getMessageKey(),
                        "type" => $authException ? "authentication" : ""
                    ),
                    "last_username" => $request->request->get("_username"),
                    "csrf_token" => $this->csrfProvider->generateCsrfToken( 'authenticate' ),
                    "target_path" => $request->request->get("_target_path") ?: $request->getUri(),
                    "nav_login_attempt" => $navigationLoginAttempt
                )
            )
        );
        return $response;

    }

}