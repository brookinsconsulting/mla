<?php

namespace Project\MemberBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Yaml\Yaml;

class ProjectMemberExtension extends Extension
{

    public function load(array $configs, ContainerBuilder $container) {
        $YamlFileLoader = new Loader\YamlFileLoader(
            $container, new FileLocator(__DIR__ . '/../Resources/config')
        );
        $YamlFileLoader->load('services.yml');

        // process configuration
        $processor = new Processor();
        $configuration = new Configuration();
        $config = $processor->processConfiguration($configuration, $configs);

        // setup member search configuration
        $container->setParameter("ProjectMember_MemberSearch", $config['member_search']);

        // setup join/renew configuration
        $container->setParameter("ProjectMember_JoinRenew", $config['join_renew']);

    }

    public function getAlias() {
        return 'project_member';
    }

}