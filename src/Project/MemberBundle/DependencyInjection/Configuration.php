<?php

namespace Project\MemberBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {

        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('project_member');

        $rootNode
            ->children()
                ->arrayNode('member_search')
                    ->children()
                        ->integerNode('limit')
                            ->defaultValue(300)
                        ->end()
                        ->integerNode('reset_interval')
                            ->defaultValue(86400)
                        ->end()
                        ->arrayNode('limit_reached_notification_emails')        
                            ->prototype('scalar')->end()
                            ->defaultValue(array('admin@mla.org'))
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('join_renew')
                    ->children()
                        ->integerNode('donation_product_object_id')
                            ->defaultValue(0)
                        ->end()
                        ->floatNode("joint_membership_fee")
                            ->defaultValue(44.00)
                        ->end()
                        ->arrayNode('promo_codes')
                            ->prototype('array')
                                ->children()
                                    ->scalarNode('code')->end()
                                    ->scalarNode('value')->end()
                                    ->scalarNode('label')->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;

    }
}