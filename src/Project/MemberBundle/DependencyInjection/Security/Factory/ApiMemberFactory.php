<?php

namespace Project\MemberBundle\DependencyInjection\Security\Factory;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;
use eZ\Publish\Core\Repository\UserService;

class ApiMemberFactory implements SecurityFactoryInterface
{

    public function create(ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
        $providerId = 'project.member.authentication.provider.'.$id;
        $container
            ->setDefinition($providerId, new DefinitionDecorator('project.member.authentication.provider'))
            ->replaceArgument(0, new Reference($userProvider))
        ;

        $listenerId = 'project.member.authentication.listener.'.$id;
        $listener = $container->setDefinition($listenerId, new DefinitionDecorator('project.member.authentication.listener'));

        return array($providerId, $listenerId, $defaultEntryPoint);
    }

    public function getPosition()
    {
        return 'pre_auth';
    }

    public function getKey()
    {
        return 'project_member';
    }

    public function addConfiguration(NodeDefinition $node)
    {
    } 

}