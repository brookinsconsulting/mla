<?php

namespace Project\MemberBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Project\MemberBundle\DependencyInjection\Security\Factory\ApiMemberFactory;

class ProjectMemberBundle extends Bundle
{

    protected $name = "ProjectMemberBundle";

    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new ApiMemberFactory());
    }    

}