<?php

namespace Project\MemberBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * @ORM\Entity
 * @ORM\Table(name="project_member_searchlookups")
 */
class MemberSearchLookups
{

    /**
     * Member search configuration
     * @var array
     */
    protected $configuration;

    /**
     * Unique id for database storage.
     * @var integer
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Member API id
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $memberId;

    /**
     * Usage counter
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $usageCount;

    /**
     * Time of first request
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $firstRequestTime;

    /**
     * Time of last request
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $lastRequestTime;

    /**
     * Limit notification email sent flag
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $limitNotificationSent;

    /**
     * Set member search configuration
     * @param array $config
     */
    public function setMemberSearchConfiguration(array $config)
    {
        $this->configuration = $config;
    }

    /**
     * Set member api id
     * @param integer $member_id
     */
    public function setMemberId($member_id)
    {
        $this->memberId = intval($member_id);
    }

    /**
     * Get usage count
     * @return integer
     */
    public function getUsageCount()
    {
        return $this->usageCount;
    }

    /**
     * Return true if member usage count is 
     * within usage parameters
     * @return boolean
     */
    public function canUse()
    {
        return $this->getUsageCount() < (isset($this->configuration['limit']) ? $this->configuration['limit'] : 300);
    }

    /**
     * Add one to usage count
     */
    public function incrementUsageCount()
    {
        // increase usage count by one
        if (!$this->usageCount) {
            $this->usageCount = 1;
        } else {
            $this->usageCount = intval($this->usageCount) + 1;
        }
    }

    /**
     * Reset usage count to 0
     */
    public function resetUsageCount()
    {
        $this->usageCount = 0;
    }

    /**
     * Get last request time
     * @return integer
     */
    public function getLastRequestTime()
    {
        return $this->lastRequestTime;
    }

    /**
     * Set last request time to current time
     */
    public function updateLastRequestTime()
    {
        $this->lastRequestTime = time();
    }

    /**
     * Get first request time
     * @return integer
     */
    public function getFirstRequestTime()
    {
        return $this->firstRequestTime;
    }


    /**
     * Set first request time to current time
     */
    public function updateFirstRequestTime()
    {
        $this->firstRequestTime = time();
    }

    /**
     * Return true if usage count can be reset
     * based on time since last lookup
     * @return boolean
     */
    public function canReset()
    {
        return time() > $this->getFirstRequestTime() + (isset($this->configuration['reset_interval']) ? $this->configuration['reset_interval'] : 86400);
    }

    /**
     * Returns true if email notification has been flagged as sent
     * @return boolean
     */
    public function hasSentEmailNotification()
    {
        return $this->limitNotificationSent;
    }

    /**
     * Flags that email notification has been sent
     */
    public function setSentEmailNotification()
    {
        $this->limitNotificationSent = true;
    }

    /**
     * Resets/unflags email notification as beening sent
     */
    public function resetSentEmailNotification()
    {
        $this->limitNotificationSent = false;
    }

}