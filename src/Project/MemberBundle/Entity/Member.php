<?php

namespace Project\MemberBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * @ORM\Entity
 * @ORM\Table(name="memberrelation")
 */
class Member
{

    /**
     * Unique id for database storage.
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * eZPublish User Id
     * @var int
     * @ORM\Column(type="integer")
     */    
    protected $ezUserId;

    /**
     * Member API id
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $memberApiId;

    /**
     * Member data
     * @var array
     * @ORM\Column(type="array", nullable=true)
     */
    protected $memberData;

    /**
     * ORCID user id
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $orcidUserId;

    /**
     * Member source id
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $memberSourceId;

    /**
     * Member access level
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $memberAccessLevel;

    /**
     * Get member mapping ID
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get eZPublish User Id
     * @return integer
     */
    public function getEzUserId()
    {
        return $this->ezUserId;
    }

    /**
     * Set eZPublish User Id
     * @param integer $value
     */
    public function setEzUserId($value)
    {
        $this->ezUserId = intval($value);
    }

    /**
     * Get Member API Id
     * @return integer
     */
    public function getMemberApiId()
    {
        return $this->memberApiId;
    }

    /**
     * Set Member API Id
     * @param integer $value
     */
    public function setMemberApiId($value)
    {
        $this->memberApiId = intval($value);
    }

    /**
     * Get member data
     * @return ParameterBag
     */
    public function getMemberData()
    {
        if (!$this->memberData) {
            $this->memberData = array();
        }
        return new ParameterBag($this->memberData);
    }

    /**
     * Set member data
     * @param ParameterBag $value
     */
    public function setMemberData(ParameterBag $value) {
        $this->memberData = $value->all();
    }

    /**
     * Get ORCID User id
     * @return string
     */
    public function getOrcidUserId()
    {
        return $this->orcidUserId;
    }

    /**
     * Set ORCID User id
     * @param string $value
     */
    public function setOrcidUserId($value)
    {
        $this->orcidUserId = $value;
    }

    /**
     * Set member source id
     * @param integer $value
     */
    public function setMemberSourceId($value)
    {
        $this->memberSourceId = intval($value);
    }

    /**
     * Get member source id
     * @return integer
     */
    public function getMemberSourceId()
    {
        return $this->memberSourceId;
    }

    /**
     * Set member access level
     * @param integer $value
     */
    public function setMemberAccessLevel($value)
    {
        $this->memberAccessLevel = intval($value);
    }

    /**
     * Get member access level
     * @return integer
     */
    public function getMemberAccessLevel()
    {
        return $this->memberAccessLevel;
    }

}
