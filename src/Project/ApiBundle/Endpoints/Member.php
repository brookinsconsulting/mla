<?php

namespace Project\ApiBundle\Endpoints;
use Symfony\Component\HttpFoundation\ParameterBag;
use Project\ApiBundle\Services\Api;

/**
 * MLA Member API Class
 */
class Member extends ParameterBag
{
    /**
     * Defines available endpoints for member api
     * @var array
     */
    static public $apiEndpoints = array("authentication", "general", "addresses", "languages", "forums", "dues", "contributions");

    /**
     * Cache member responses
     * @var array
     */
    static protected $memberResponseCache = array();

    /**
     * Member source identifiers
     * @var integer
     */
    const MEMBER_SOURCE_MLA = 1;
    const MEMBER_SOURCE_DEPARTMENTS = 2;

    /**
     * Member ID number.
     * @var integer
     */
    protected $memberId;

    /**
     * Member source id
     * @var integer
     */
    protected $memberSourceId;

    /**
     * CoreApi object
     * @var Api
     */
    protected $api;

    /**
     * Array containing params associated with member
     * @var array
     */
    protected $parameters;

    /**
     * Constructor
     * @param Api $api
     * @param string|integer $member_identifier  MLA Member identifier
     * @param array $member_response  Response from API (optional).
     * @param integer $cache_expiration  Time before cache should expire
     */
    public function __construct(Api $api, $member_source_id = 1, $member_identifier, $member_response = array(), $cache_expiration = -1)
    {

        $this->api = $api;
        $this->memberSourceId = $member_source_id;
        
        // get member response from API
        if (!$member_response) {

            // check for response cache
            if (array_key_exists(
                    $member_source_id . ":" . $member_identifier,
                    self::$memberResponseCache
                )
            ) {
                $member_response = self::$memberResponseCache[$member_source_id . ":" . $member_identifier];

            // get new response
            } else {

                $member_response = $api->request(
                    self::getMemberSourcePath($this->memberSourceId) . $member_identifier,
                    "GET",
                    array(),
                    array(),
                    $cache_expiration
                );

            }
        }

        // cache response
        self::$memberResponseCache[$member_source_id . ":" . $member_identifier] = $member_response;

        // set member id
        $this->memberId = $member_response['data'][0]['id'];

        // make all member_response array keys lowercase and set parameters
        $this->parameters = array_change_key_case($member_response['data'][0], CASE_LOWER);
        foreach ($this->parameters as $key=>$parameter) {
            if (gettype($parameter) == "array") {
                $this->parameters[$key] = array_change_key_case($parameter, CASE_LOWER);
            }
        }
    }

    /**
     * Update given endpoint with values present.
     * @param string $endpoint
     * @param boolean $use_shell_request  If true a shell request will be peformed
     */
    public function update($endpoint, $use_shell_request = true)
    {
        if (!in_array($endpoint, self::$apiEndpoints)) {
            return false;
        }

        // update cache
        $cachePath = $this->api->getRequestCacheFilepath("members/" . $this->memberId);
        if (file_exists($cachePath)) {
            $cache = json_decode(file_get_contents($cachePath), True);
            $cache['data'][0] = $this->parameters;
            file_put_contents($cachePath, json_encode($cache));
        }

        // update API
        $site_root = getcwd() . "../";
        $args = array();
        switch($endpoint) {

            case "forums":
                break;

            case "dues":
                break;

            case "contributions":
                break;

            case "addresses":
                $args = array(
                    self::getMemberSourcePath($this->memberSourceId) . $this->memberId . "/{$endpoint}", 
                    "PUT", 
                    array(), 
                    array_values($this->get($endpoint))
                );
                break;

            default:
                $args = array(
                    self::getMemberSourcePath($this->memberSourceId) . $this->memberId . "/{$endpoint}",
                    "PUT",
                    array(),
                    $this->get($endpoint)
                );
                break;
        }

        // clear response cache
        if (array_key_exists(
                $this->memberSourceId . ":" . $this->memberId,
                self::$memberResponseCache
            )
        ) {
            unset(self::$memberResponseCache[$this->memberSourceId . ":" . $this->memberId]);
        }
        if (array_key_exists(
                $this->memberSourceId . ":" . $this->get("general[username]", null, true),
                self::$memberResponseCache
            )
        ) {
            unset(self::$memberResponseCache[$this->memberSourceId . ":" . $this->memberId]);
        }

        // if args were generated make api update request
        if ($args) {
            if ($use_shell_request) {
                call_user_func_array(
                    array($this->api, "shellRequest"),
                    $args
                );
            } else {
                call_user_func_array(
                    array($this->api, "request"),
                    $args
                );
            }
        }
    }

    /**
     * Create a new 'mla' member.
     * @param Api $api
     * @param array $parameters
     * @return Member|boolean
     */
    static public function create(Api $api, $parameters = array())
    {
        $results = $api->request(
            self::getMemberSourcePath(self::MEMBER_SOURCE_MLA),
            "POST",
            array(),
            $parameters
        );
        if ($results && array_key_exists("data", $results)) {
            if (count($results["data"]) > 0) {
                $data = array_change_key_case($results["data"][0]);
                if (array_key_exists("id", $data)) {
                    return new Member($api, self::MEMBER_SOURCE_MLA, abs($data["id"]));
                }
            }
        }

        if (isset($parameters['authentication']['username'])) {
            $memberSearch = $api->request(
                self::getMemberSourcePath(self::MEMBER_SOURCE_MLA),
                "GET",
                array("username" => $parameters['authentication']['username']),
                array()
            );
            if (
                $memberSearch && 
                isset($memberSearch['data']) && 
                count($memberSearch['data']) > 0 && 
                isset($memberSearch['data'][0]['id'])
            ) {
                return new Member($api, self::MEMBER_SOURCE_MLA, abs($memberSearch['data'][0]['id']));
            }
        }

        return false;
    }

    /**
     * Return a list of 'mla' source members.
     * @param Api $api
     * @param array $parameters,
     * @param interger $cache_expiration
     * @return array
     */
    static public function find(Api $api, $parameters = array(), $cache_expiration = -1)
    {
        $results = $api->request(
            self::getMemberSourcePath(self::MEMBER_SOURCE_MLA),
            "GET", 
            $parameters,
            array(),
            $cache_expiration
        );
        return $results;
    }

    /**
     * Verify password for current member source.
     * Will either return access level (1 or 2) or
     * boolean false for invalid password.
     * @param string $password
     * @return integer|boolean
     */
    public function verifyPassword($password)
    {

        switch ($this->memberSourceId)
        {

            // mla source has no access level, any level can be considered
            // to be valid (returns 1)
            case self::MEMBER_SOURCE_MLA:
                // compare password bcrypt hash
                return (
                    crypt(
                        trim($password),
                        $this->get("authentication[password]", null, true)
                    ) === $this->get("authentication[password]", null, true)
                ) ? 1 : false;
                break;

            // department source has two possible passwords, attempt
            // to match one, the matching password determines access level
            case self::MEMBER_SOURCE_DEPARTMENTS:

                // member (level 2)
                if (
                    crypt(
                        trim($password),
                        $this->get("authentication[member_password]", null, true)
                    ) === $this->get("authentication[member_password]", null, true)
                ) {
                    return 2;

                // chair (level 1)
                } else if (
                    crypt(
                        trim($password),
                        $this->get("authentication[chair_password]", null, true)
                    ) === $this->get("authentication[chair_password]", null, true)
                ) {
                    return 1;
                }

                // password incorrect
                return false;
                break;

        }
        return false;       

    }

    /**
     * Retrieve survey results for this member
     * @param integer year
     * @param interger $cache_expiration
     * @return Survey
     * @throws DomainException
     */
    public function getSurvey($year = null, $cache_expiration = -1)
    {

        // must be mla member
        if ($this->memberSourceId != self::MEMBER_SOURCE_MLA) {
            throw new \DomainException("Survey endpoint not available for this member source.");
        }

        return new Survey(
            $this->api,
            $this->memberId,
            $year ?: date("Y"),
            $cache_expiration
        );
    }

    /**
     * Get API endpoint path for given member source id
     * @param integer $memberSourceId
     * @return string
     * @throws InvalidArgumentException
     */
    static protected function getMemberSourcePath($member_source_id = 1)
    {

        switch ($member_source_id)
        {
            case self::MEMBER_SOURCE_MLA:
                return "members/";
                break;

            case self::MEMBER_SOURCE_DEPARTMENTS:
                return "association_departments/";
                break;
        }
        throw new \InvalidArgumentException("Invalid member source id provided.");

    }

    /**
     * Get member source id
     */
    public function getMemberSourceId() 
    {
        return $this->memberSourceId;
    }

}
