<?php

namespace Project\ApiBundle\Endpoints;
use Symfony\Component\HttpFoundation\ParameterBag;
use Project\ApiBundle\Services\Api;

/**
 * MLA Product Variant API Class
 */
class ProductVariant extends ParameterBag
{

    /**
     * Debug mode, will use debug endpoint to test
     * updating of product variants.
     * @var boolean
     */
    const DEBUG = false;

    /**
     * Cached variant list
     * @var array
     */
    static protected $variantList = array();

    /**
     * Product parameters
     * @var array
     */
    protected $parameters = array();

    /**
     * Constructor
     * @param array $product_params
     */
    public function __construct(array $product_params)
    {
        $this->parameters = $product_params;
    }

    /**
     * Retrieve a product variant from the API
     * @param Api $api
     * @param string $product_id
     * @param integer $cache_expiration
     * @return Product
     */
    public static function getVariantById(Api $api, $variant_id, $cache_expiration = -1)
    {

        // get variants
        $variantList = self::getAllVariants($api, $cache_expiration);

        // find variant
        foreach ($variantList as $variant) {
            if ($variant->get("id") == $variant_id) {
                return $variant;
            }
        }
        return false;
    }

    /**
     * Retrieve a product variant from the API
     * with its ISBN
     * @param Api $api
     * @param string $isbn
     * @param integer $cache_expiration
     * @return Product
     */
    public static function getVariantByIsbn(Api $api, $isbn, $cache_expiration = -1)
    {
        // get variants
        $variantList = self::getAllVariants($api, $cache_expiration);

        // find variant
        foreach ($variantList as $variant) {
            if ($variant->get("isbn") == $isbn) {
                return $variant;
            }
        }
    }

    /**
     * Retrieve all products variants from the API
     * @param Api $api
     * @param integer $cache_expiration
     * @return array
     */
    public static function getAllVariants(Api $api, $cache_expiration = -1)
    {
     
        // use cached response
        if (self::$variantList) {
            return self::$variantList;
        }

        // make api call
        $apiResponse = $api->request(
            "products" . (self::DEBUG ? "/debug" : ""),
            "GET",
            array(),
            array(),
            $cache_expiration
        );

        // format the list
        $variantList = array();
        if ($apiResponse && array_key_exists("data", $apiResponse)) {
            foreach ($apiResponse['data'] as $variant) {
                $variantList[] = new ProductVariant($variant);
            }
        }

        // store variant list to cache
        self::$variantList = $variantList;

        return $variantList;
    }

}