<?php

namespace Project\ApiBundle\Endpoints;
use Symfony\Component\HttpFoundation\ParameterBag;
use Project\ApiBundle\Services\Api;

class Organizations
{

    /**
     * Store API response for reuse if called
     * more then once in same request
     * @var array
     */
    static protected $cache = array();

    /**
     * API resposne data
     * @var array
     */
    protected $data;

    /**
     * Constructor
     */
    public function __construct($api_response = array())
    {
        $this->data = $api_response;
    }

    /**
     * Get list of organizations and return Organizations
     * object
     * @param Project\ApiBundle\Services\Api $api
     * @param integer $cache_expiration
     * @return Organization
     */
    public static function getList(Api $api, $cache_expiration = -1)
    {

        // get cached lookup
        if (self::$cache) {
            return new Organizations(self::$cache);
        }

        $apiResponse = $api->request(
            "organizations",
            "GET",
            array(),
            array(),
            $cache_expiration
        );
        if (!$apiResponse) {
            return false;
        }

        // cache
        self::$cache = $apiResponse;

        return new Organizations($apiResponse);
    }

    /**
     * Check if given organization matches given filters
     * @param array $organization
     * @param string|null $category
     * @param string|null $type
     * @param boolean|null $exclude_from_commons
     * @return boolean
     */
    private function checkFilters($organization, $category = null, $type = null, $exclude_from_commons = null)
    {
        if ($category) {
            if (
                !array_key_exists("category", $organization) &&
                !array_key_exists("category_name", $organization)
            ) {
                return false;
            }
            if (
                strtolower(trim($organization["category"])) != strtolower(trim($category)) &&
                strtolower(trim($organization["category_name"])) != strtolower(trim($category))
            ) { 
                return false;
            }
        }
        if ($type) {
            if (
                !array_key_exists("type", $organization) ||
                strtolower(trim($organization["type"])) != strtolower(trim($type))
            ) {
                return false;
            }
        }
        if ($exclude_from_commons !== null) {
            if (
                !array_key_exists("exclude_from_commons", $organization) ||
                (bool) $exclude_from_commons != in_array(strtolower(trim($organization["exclude_from_commons"])), array("n", "")) ? false  : true
            ) {
                return false;
            }
        }
        return true;
    }

    /**
     * Return array containing organization categories
     * including sub categories
     * @param string|null $type
     * @param boolean|null $exclude_from_commons
     * @return array
     */
    public function getCategories($type = null, $exclude_from_commons = null)
    {

        if (!array_key_exists("data", $this->data)) {
            return array();
        }

        $categories = array();
        foreach ($this->data["data"] as $organization) {

            if (!$this->checkFilters($organization, null, $type, $exclude_from_commons)) {
                continue;
            }

            $hasCategory = false;
            foreach ($categories as $key => $category) {
                if ($category["id"] == trim($organization["category"])) {
                    $hasCategory = true;
                    $categories[$key]["count"]++;
                    if (trim($organization["subcategory"])) {
                        $hasSubCat = false;
                        foreach ($category["subcategories"] as $skey => $subcategory) {
                            if ($subcategory["id"] == trim($organization["subcategory"])) {
                                $hasSubCat = true;
                                $categories[$key]["subcategories"][$skey]["count"]++;
                                break;
                            }
                        }
                        if (!$hasSubCat) {
                            $categories[$key]["subcategories"][] = array(
                                "id" => trim($organization["subcategory"]),
                                "name" => trim($organization["subcategory_name"]),
                                "count" => 1
                            );
                        }
                    }
                    break;
                }
            }
            if ($hasCategory) {
                continue;
            }
            $subcategory = array();
            if (trim($organization["subcategory"])) {
                $subcategory[] = array(
                    "id" => trim($organization["subcategory"]),
                    "name" => trim($organization["subcategory_name"]),
                    "count" => 1
                );
            }

            $categories[] = array(
                "id" => trim($organization["category"]),
                "name" => trim($organization["category_name"]) ?: "Uncategorized",
                "count" => 1,
                "subcategories" => $subcategory
            );
        }
        return $categories;
    }

    /**
     * Get list of organizations
     * @param string|null $category
     * @param string|null $type
     * @param boolean|null $exclude_from_commons
     * @return array
     */
    public function getOrganizations($category = null, $type = null, $exclude_from_commons = null)
    {
        if (!array_key_exists("data", $this->data)) {
            return array();
        }

        $organizations = array();
        foreach ($this->data["data"] as $organization) {
            if (!$this->checkFilters($organization, $category, $type, $exclude_from_commons)) {
                continue;
            }
            $organizations[] = $organization;
        }
        return $organizations;
    }

}