<?php

namespace Project\ApiBundle\Endpoints;
use Symfony\Component\HttpFoundation\ParameterBag;
use Project\ApiBundle\Services\Api;

/**
 * MLA Member Survey API Class
 */
class Survey extends ParameterBag
{

    /**
     * CoreApi object
     * @var CoreApi $api
     */
    protected $api;

    /**
     * Member identifier
     * @var integer
     */
    protected $memberId;

    /**
     * Survey Year
     * @var integer
     */
    protected $year;

    /**
     * Array containing params associated with member
     * @var array $parameters
     */
    protected $parameters;

    /**
     * Constructor
     * @param Api $api
     * @param integer $member_identifier  MLA Member identifier
     * @param integer $cache_expiration  Time before cache should expire
     */
    public function __construct(Api $api, $member_identifier, $year = null, $cache_expiration = -1)
    {

        // api
        $this->api = $api;

        // member indentifier
        $this->memberId = $member_identifier;

        // set year
        if (!$year) {
            $year = date("Y");
        }
        $this->year = $year;
        
        // get survey response
        $response = $this->api->request(
            "members/{$member_identifier}/survey",
            "GET",
            array("year" => $year),
            array(),
            $cache_expiration
        );

        // make all array keys lowercase and set parameters
        $this->parameters = array_change_key_case($response['data'][0], CASE_LOWER);
        foreach ($this->parameters as $key=>$parameter) {
            if (gettype($parameter) == "array") {
                $this->parameters[$key] = array_change_key_case($parameter, CASE_LOWER);
            }
        }

    }

    /**
     * Update survey with values present.
     * @param boolean $use_shell_request  If true a shell request will be peformed
     */
    public function update($use_shell_request = true)
    {

        // update cache
        $cachePath = $this->api->getRequestCacheFilepath(
            "members/" . $this->memberId . "/survey",
            array("year" => $this->year)
        );
        if (file_exists($cachePath)) {
            $cache = json_decode(file_get_contents($cachePath), True);
            $cache['data'][0] = $this->parameters;
            file_put_contents($cachePath, json_encode($cache));
        }

        // update API
        $site_root = getcwd() . "../";
        
        $args = array(
            "members/" . $this->memberId . "/survey", 
            "PUT", 
            array("year" => $this->year), 
            array($this->all())
        );

        // if args were generated make api update request
        if ($args) {
            if ($use_shell_request) {
                call_user_func_array(
                    array($this->api, "shellRequest"),
                    $args
                );
            } else {
                call_user_func_array(
                    array($this->api, "request"),
                    $args
                );
            }
        }
    }


}
