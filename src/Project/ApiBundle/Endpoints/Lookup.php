<?php

namespace Project\ApiBundle\Endpoints;
use Symfony\Component\HttpFoundation\ParameterBag;
use Project\ApiBundle\Services\Api;

/**
 * MLA Lookups API class
 */
class Lookup extends ParameterBag
{

    /**
     * Storage for lookups made to be
     * reused if the same request is made twice.
     * @var array
     */
    static protected $lookupCache = array();

    /**
     * Data returned from API
     * @var array
     */
    protected $parameters;

    /**
     * Constructor
     * @param array $api_response  Response from API
     */
    public function __construct($api_response)
    {
        $this->parameters = $api_response['data']; 
    }

    /**
     * Retrive lookup data from API
     * @param Api $api
     * @param string $endpoint
     * @param array $params
     * @param integer $cache_expiration
     * @return Lookup
     */
    public static function getLookup(Api $api, $endpoint, $params = array(), $cache_expiration = -1)
    {

        // get cached lookup
        if (
            array_key_exists(
                base64_encode($endpoint . json_encode($params)),
                self::$lookupCache
            )
        ) {
            return new Lookup(
                self::$lookupCache[base64_encode($endpoint . json_encode($params))]
            );
        }

        $apiResponse = $api->request(
            "lookups/{$endpoint}",
            "GET",
            $params,
            array(),
            $cache_expiration
        );
        if ($apiResponse) {

            // cache
            self::$lookupCache[base64_encode($endpoint . json_encode($params))] = $apiResponse;

            // return new Lookup object
            return new Lookup($apiResponse);
        }
        return false;
    }

    /**
     * Retrieve all items whose values match
     * @param string $key  Key to lookup
     * @param string $value  Value to match
     * @return array
     */
    public function getItemsWithKey($key, $value)
    {
        $output = array();
        foreach ($this->parameters as $item) {
            if (!array_key_exists($key, $item)) {
                continue;
            }
            if ($item[$key] == $value) {
                $output[] = $item;
            }
        }
        return $output;
    }
}