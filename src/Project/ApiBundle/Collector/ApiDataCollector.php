<?php

namespace Project\ApiBundle\Collector;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Project\ApiBundle\Services\Api;

/**
 * Api Data Collector
 * Used to debug the API
 */
class ApiDataCollector extends DataCollector
{

    /**
     * API Service
     * @var Project\ApiBundle\Services\Api
     */
    protected $api;

    /**
     * Constructor
     */
    public function __construct(Api $api)
    {
        $this->api = $api;
    }

    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
        $this->data = array(
            'api_total_calls' => $this->api->getTotalCalls(),
            'api_call_log' => $this->api->getResults()
        );
    }   

    public function getTotalCalls()
    {
        return $this->data['api_total_calls'];
    }

    public function getCallLog()
    {
        return $this->data['api_call_log'];
    }

    public function getName()
    {
        return 'api_data_collector';
    }
}