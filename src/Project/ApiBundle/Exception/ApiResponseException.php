<?php

namespace Project\ApiBundle\Exception;

class ApiResponseException extends \Exception {

    protected $apiResponse = array();
    protected $httpResponseCode;

    public function __construct($http_response_code, $http_body)
    {
        $this->httpResponseCode = $http_response_code;

        // decode api response
        $this->apiResponse = json_decode($http_body, true);

        // generate message
        if (!$http_body) {
            $this->message = "API returned no content body.";
        }

        // api returned error message
        else if ($this->apiResponse) {
            $this->message = 
                ucfirst($this->getStatus()) . 
                " " . 
                $this->getStatusCode() . 
                ": ". 
                $this->apiResponse['meta']['message']
            ;
        }
        else {
            $this->message = $http_body;
        }
    }

    public function getHttpResponseCode()
    {
        return $this->httpResponseCode;
    }

    public function getStatus()
    {
        if (
            array_key_exists("meta", $this->apiResponse) &&
            array_key_exists("status", $this->apiResponse['meta'])
        ) {
            return $this->apiResponse['meta']['status'];
        }
        return "";
    }

    public function getStatusCode()
    {
        if (
            array_key_exists("meta", $this->apiResponse) &&
            array_key_exists("code", $this->apiResponse['meta'])
        ) {
            return $this->apiResponse['meta']['code'];
        }
        return "";
    }

}