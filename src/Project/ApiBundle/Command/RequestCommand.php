<?php

namespace Project\ApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Project\ApiBundle\Services\Api;

class RequestCommand extends ContainerAwareCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('project_api:request')
            ->setDescription('Make a request to the MLA API')
            ->addOption(
                'endpoint',
                null,
                InputOption::VALUE_REQUIRED,
                'API endpoint',
                null
            )
            ->addOption(
                'method',
                null,
                InputOption::VALUE_OPTIONAL,
                'Request method',
                "GET"
            )
            ->addOption(
                'params',
                null,
                InputOption::VALUE_OPTIONAL,
                'URL parameters',
                ""
            )
            ->addOption(
                'post',
                null,
                InputOption::VALUE_OPTIONAL,
                'POST parameters',
                ""
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        // get api service
        $api = $this->getContainer()->get("project.api.api");

        $urlParams = array();
        parse_str($input->getOption('params'), $urlParams);
        $postParams = array();
        parse_str($input->getOption('post'), $postParams);

        $response = $api->request(
            $input->getOption('endpoint'),
            $input->getOption('method') ?: "GET",
            $urlParams,
            $postParams
        );

        $output->writeln("<info>Request to " . $input->getOption("endpoint") . "...</info>");
        $output->writeln(json_encode($response));

    }

}