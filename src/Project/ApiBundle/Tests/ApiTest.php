<?php

namespace Project\ApiBundle\Tests;
use Project\ApiBundle\Services\Api;
use Project\ApiBundle\Classes\Member;

class ApiTest extends \PHPUnit_Framework_TestCase
{

    const TEST_MEMBER_ID = 182340;
    const TEST_API_KEY = "thinkcreative";
    const TEST_API_SECRET = "a7ba2096719aece556de08598209d37258e00a61d7619842c0ba9318de498792";
    const TEST_API_URL = "https://apidev.mla.org/1";

    /**
     * Get test API object.
     */ 
    public function getApi()
    {
        return new Api(
            self::TEST_API_KEY, 
            self::TEST_API_SECRET,
            self::TEST_API_URL
        );
    }

    /**
     * Test to ensure that valid
     * response is returned.
     */
    public function testRequest()
    {
        $api = $this->getApi();
        $response = $api->request(
            "members/" . self::TEST_MEMBER_ID,
            "GET",
            array(),
            array(),
            0
        );

        // make sure a non error status was returned
        $this->assertTrue(array_key_exists("meta", $response));
        $this->assertTrue(array_key_exists("status", $response['meta']));
        $this->assertTrue($response['meta']['status'] == "success");
    }

    /**
     * Test to ensure that Lookups return a response
     */
    public function testLookups()
    {
        $api = $this->getApi();
        foreach (array("forums", "departments", "languages") as $endpoint) {
            $lookup = $api->getLookup($endpoint, 0);
            $this->assertGreaterThan(0, count($lookup->all()));
        }
    }

    /**
     * Test to ensure Member returns a response
     */
    public function testMemberRetrieve()
    {
        $api = $this->getApi();
        $member = $api->getMember(self::TEST_MEMBER_ID, 0);

        foreach ($member::$apiEndpoints as $endpoint) {
            $this->assertTrue($member->has($endpoint));
        }

    }

    /**
     * Test to ensure a Member can be updated on all endpoints
     */
    public function testMemberUpdate()
    {

        $api = $this->getApi();

        $member = $api->getMember(self::TEST_MEMBER_ID, 0);
        
        // use current time as a way to verify that
        // value was set
        $time = time();

        // values to use when testing the various endpoints
        $testValues = array(
            /*"authentication" => array(
                "username" => "testuser"
            ),*/
            "general" => array(
                "title" => "Mr",
                "first_name" => "Think{$time}",
                "last_name" => "Creative{$time}",
                "suffix" => "",
                "email" => "info{$time}@thinkcreative.com",
                "email_visible" => "Y",
                "email_shareable" => "",
                "phone" => "8881119999",
                "website" => "www.thinkcreative.com"
            ),
            "addresses" => array(
                array(
                    "action" => "U",
                    "type" => "primary",
                    "hidden" => "",
                    "send_mail" => "N",
                    "affiliation" => "Think Creative",
                    "department" => rand(1000,9999),
                    "rank" => "Mr",
                    "line1" => substr((string) $time, -4) . " Test St",
                    "line2" => "Box " . substr((string) $time, -4),
                    "line3" => "",
                    "city" => "Tallahassee",
                    "state" => "FL",
                    "zip" => substr((string) $time, -5),
                    "country_code" => "US"
                ),
                array(
                    "type" => "secondary",
                    "action" => "D"
                ),
                array(
                    "type" => "home",
                    "action" => "D"
                )
            ),
            "languages" => array(
                array(
                    "lang_code" => rand(300, 400),
                    "primary" => "Y"
                )
            )
        );

        foreach ($testValues as $endpoint => $values) {
            $member->set($endpoint, $values);

            $member->update($endpoint, false);
            $member = $api->getMember(self::TEST_MEMBER_ID, 0);

            $newValues = $member->get($endpoint);

            if ($endpoint == "addresses" || $endpoint == "languages") {
                $newValues = $newValues[0];
                $values = $values[0];
            }
            if ($endpoint == "languages") {
                $values['code'] = $values['lang_code'];
            }

            foreach ($newValues as $key=>$value) {
                if (array_key_exists($key, $values)) {
                    $this->assertTrue($values[$key] == $value);
                }
            }
            
        }

    }

    /**
     * Test to ensure Member creation works
     */

    
    /**public function testMemberCreation()
    {
        $time = time();
        $member = Member::create(
            $this->getApi(),
            array(
                "authentication" => array(
                    "username" => "thinkcreative{$time}",
                    "password" => "thinkpass"
                ),
                "general" => array(
                    "title" => "Mr",
                    "first_name" => "Think{$time}",
                    "last_name" => "Creative{$time}",
                    "suffix" => "",
                    "email" => "info{$time}@thinkcreative.com",
                    "email_visible" => "Y",
                    "email_shareable" => "",
                    "phone" => "8881119999",
                    "website" => "www.thinkcreative.com",
                    "status" => ""
                ),
                "addresses" => array(
                    array(
                        'action' => "U",
                        "type" => "primary",
                        "hidden" => "",
                        "send_mail" => "N",
                        "affiliation" => "Think Creative",
                        "department" => 1001,
                        "department_other" => "Development",
                        "rank" => "Mr",
                        "line1" => substr((string) $time, -4) . " Test St",
                        "line2" => "Box " . substr((string) $time, -4),
                        "line3" => "",
                        "city" => "Tallahassee",
                        "state" => "FL",
                        "zip" => substr((string) $time, -5),
                        "country_code" => "US"
                    )
                )
            )
        );

        $this->assertTrue(is_a($member, 'Member'));
    }**/

    /*public function testMemberSearch()
    {

        $search = Member::find(
            $this->getApi(),
            array(
                "latest_paid_year" => date("Y")
            )
        );
        $this->assertTrue(True);

    }*/


}