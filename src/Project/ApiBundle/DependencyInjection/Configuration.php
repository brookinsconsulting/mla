<?php

namespace Project\ApiBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('project_api');
        $rootNode
            ->children()
                ->scalarNode('api_key')
                    ->defaultValue("")
                    ->end()
                ->scalarNode('api_secret')
                    ->defaultValue("")
                    ->end()
                ->scalarNode('api_url')
                    ->defaultValue("https://apidev.mla.org/1")
                    ->end()
            ->end();
        return $treeBuilder;
    }
}
