<?php

namespace Project\ApiBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Config\Definition\Processor;

class ProjectApiExtension extends Extension
{

    public function load(array $configs, ContainerBuilder $container) {
        

        $YamlFileLoader = new Loader\YamlFileLoader(
            $container, new FileLocator(__DIR__ . '/../Resources/config')
        );

        // process api config
        $processor = new Processor();
        $configuration = new Configuration();
        $config = $processor->processConfiguration($configuration, $configs);

        // api parameters
        $container->setParameter("project.api.config.key", $config['api_key']);
        $container->setParameter("project.api.config.secret", $config['api_secret']);
        $container->setParameter("project.api.config.url", "");
        $container->setParameter("project.api.config.url", $config['api_url']);

        // load services
        $YamlFileLoader->load('services.yml');
    }

    public function getAlias() {
        return 'project_api';
    }

}