<?php

namespace Project\ApiBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ProjectApiBundle extends Bundle
{

    protected $name = "ProjectApiBundle";

}