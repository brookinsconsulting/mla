<?php

namespace Project\ApiBundle\Services;
use Project\ApiBundle\Exception\ApiCurlException;
use Project\ApiBundle\Exception\ApiResponseException;
use Project\ApiBundle\Endpoints\Member;
use Project\ApiBundle\Endpoints\Lookup;
use Project\ApiBundle\Endpoints\Organizations;

/**
 * Service for interfacing with the MLA API
 */
class Api
{

    /**
     * If true api cache will not be used and
     * shell_request method will be routed to
     * request method.
     * @var boolean
     */
    const DEBUG = true;

    /**
     * API Key.
     * @var string
     */
    protected $apiKey;

    /**
     * API Secret.
     * @var string
     */
    protected $apiSecret;

    /**
     * API URL.
     * @var string
     */
    protected $apiUrl = "https://apidev.mla.org/1";

    /**
     * Array containing result strings from all
     * API calls made during this request
     * @var string
     */
    protected $apiRawResults = array();

    /**
     * Time in seconds before cache should expire.
     * @var integer
     */
    protected $cacheExpiration = 3600;

    /**
     * Constructor.
     * @param string $api_key
     * @param string $api_secret
     * @param string $api_url
     */
    public function __construct($api_key, $api_secret, $api_url = "")
    {
        $this->apiKey = $api_key;
        $this->apiSecret = $api_secret;
        if ($api_url) {
            $this->apiUrl = rtrim($api_url, '/');
        }
    }

    /**
     * Make a request to remote server.
     * @param string $endpoint
     * @param string $request_method
     * @param array $url_params
     * @param array $post_params
     */
    public function request($endpoint, $request_method = "GET", $url_params = array(), $post_params = array(), $cache_expiration = -1)
    {

        // create static array of GET request so mutliple request aren't
        // made in the same page request
        static $apiGetRequests = array();

        // load request from cache if available and not expired
        if ($cache_expiration < 0) {
            $cache_expiration = $this->cacheExpiration;
        }
        if (strtoupper($request_method) == "GET" && !self::DEBUG) {

            // create cache dir if not exists
            if (!file_exists(sys_get_temp_dir() . "/mla_api_cache")) {
                mkdir(sys_get_temp_dir() . "/mla_api_cache");
            }

            // get cache file path
            $cacheFilepath = $this->getRequestCacheFilepath($endpoint, $url_params);

            // use cache file if it exists and is not expired
            if ($this->hasCache($endpoint, $url_params, $cache_expiration)) {
                return json_decode(file_get_contents($cacheFilepath), True);
            }

        }

        // generate urld
        $url_params['key'] = $this->apiKey;
        $url_params['timestamp'] = time();
        $uri = $this->apiUrl . "/" . $endpoint . "?" . http_build_query($url_params);

        // generate signature;
        $signature = hash_hmac(
            'sha256',
            strtoupper($request_method) . '&' . rawurlencode($uri),
            $this->apiSecret
        );

        $uri = $uri . '&signature=' . $signature;

        // setup curl
        $curl = curl_init();
        $curl_opts = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $uri,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            //CURLOPT_VERBOSE => true
        );

        // set request method
        if (strtoupper($request_method) != "GET") {
            $curl_opts[CURLOPT_CUSTOMREQUEST] = strtoupper($request_method);
            if ($post_params) {
                $curl_opts[CURLOPT_POST] = true;
                $curl_opts[CURLOPT_HTTPHEADER] = array('Content-Type: application/json');
                $curl_opts[CURLOPT_POSTFIELDS] = json_encode($post_params);
            }
        }

        // set curl options
        curl_setopt_array($curl, $curl_opts);

        // perform request
        $result = curl_exec($curl);
        $this->apiRawResults[] = array(
            "time" => time(),
            "endpoint" => $endpoint,
            "method" => $request_method,
            "url_params" => json_encode($url_params),
            "post_params" => json_encode($post_params),
            "results" => $result
        );
        
        $httpResponseNo = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        // error
        if (curl_errno($curl)) {
            throw new ApiCurlException("Curl Err No " . curl_errno($curl) . ": " . curl_error($curl));
        }

        // close curl
        curl_close($curl);

        // http response not 2xx
        if ($httpResponseNo < 200 || $httpResponseNo > 299) {
            throw new ApiResponseException($httpResponseNo, $result);
        }

        // no results
        if (!trim($result)) {
            throw new ApiResponseException($httpResponseNo, "");
        }

        // determine if error was returned
        $json_result = json_decode($result, true);

        // returned error
        if (isset($json_result['meta']['status']) && strtolower($json_result['meta']['status']) != "success") {
            throw new ApiResponseException($httpResponseNo, $result);
        }

        // cache GET request
        if (strtoupper($request_method) == "GET" && !self::DEBUG) {
            file_put_contents($cacheFilepath, $result);
        }

        return $json_result;

    }

    /**
     * Get a member.
     * @param string|integer $member_identifier
     * @param integer $cache_expiration
     * @return Member
     */
    public function getMember($member_identifier, $member_source_id = 1, $cache_expiration = -1)
    {
        return new Member(
            $this,
            $member_source_id,
            $member_identifier,
            array(),
            $cache_expiration
        );
    }

    /**
     * Get a lookup
     * @param string $endpoint
     * @param array $params
     * @param integer $cache_expiration
     * @return Lookup
     */
    public function getLookup($endpoint, $params = array(), $cache_expiration = -1)
    {
        return Lookup::getLookup(
            $this, 
            $endpoint,
            $params,
            $cache_expiration
        );
    }

    /**
     * Get organizations
     * @param integer $cache_expiration
     * @return Organiziations
     */
    public function getOrganizations($cache_expiration = -1)
    {
        return Organizations::getList($this, $cache_expiration);
    }

    /**
     * Returns last result string
     * @return string
     */
    public function getLastResult()
    {
        return $this->apiRawResults[count($this->apiRawResults) - 1];
    }

    /**
     * Get array of all results
     * @return array
     */
    public function getResults()
    {
        return $this->apiRawResults;
    }

    /**
     * Get total number of API calls made
     * @return interger
     */
    public function getTotalCalls()
    {
        return count($this->apiRawResults);
    }

    /**
     * Get filepath to request cache
     * @param string $endpoint
     * @param array $url_params
     * @return string
     */
    public function getRequestCacheFilepath($endpoint, $url_params = array()) {

        // get cache file name
        $cacheFilename = md5( $endpoint . json_encode($url_params) );

        // get cache file path
        return sys_get_temp_dir() . "/mla_api_cache/{$cacheFilename}";
    }

    /**
     * Returns true if given request is currently cached
     * @param string $endpoint
     * @param array $url_params
     * @param integer $cache_expiration
     * @return string
     */
    public function hasCache($endpoint, $url_params = array(), $cache_expiration = -1)
    {

        // use default cache expiration value if not provided
        if ($cache_expiration < 0) {
            $cache_expiration = $this->cacheExpiration;
        }

        // make sure cache file exists and that it hasn't expired
        $cachePath = $this->getRequestCacheFilepath($endpoint, $url_params);
        return (
            file_exists($cachePath) &&
            filemtime($cachePath) + $cache_expiration > time()
        );
    }

    /**
     * Perform an API request via a new process.
     * This does not return results...ideal for 
     * posting data to API. Can also be used to
     * pull information for retrivial later via cache.
     * @param string $endpoint
     * @param string $request_method
     * @param array $url_params
     * @param array $post_params
     * @return boolean  Returns true if shell executed
     */
    public function shellRequest($endpoint, $request_method = "GET", $url_params = array(), $post_params = array())
    {

        // don't use shell request if debugging
        if (self::DEBUG) {
            return $this->request(
                $endpoint,
                $request_method,
                $url_params,
                $post_params,
                0
            );
        }

        $rootDir = getcwd() . "/..";
        if (!file_exists($rootDir)) {
            return false;
        }

        if (file_exists("{$rootDir}/ezpublish")) {
            $exeDir = "{$rootDir}/ezpublish";
        } elseif (file_exists("{$rootDir}/app")) {
            $exeDir = "{$rootDir}/app";
        } else {
            return false;
        }

        // make command
        $shell_cmd = 
            "php {$exeDir}/console project_api:request" .
            " --endpoint='" . $endpoint . 
            "' --method='" . $request_method .
            "' --params='" . http_build_query($url_params) .
            "' --post='" . http_build_query($post_params) . "' &"
        ;

        // log
        $this->apiRawResults[] = array(
            "time" => time(),
            "endpoint" => $endpoint,
            "request_method" => $request_method,
            "url_params" => $url_params,
            "post_params" => $post_params,
            "shell" => true,
            "results" => "Executed as seperate process...
            {$shell_cmd}
            "
        );

        shell_exec($shell_cmd);
        return True;

    }


}