<?php

class BulletinSearch
{

    protected $SearchService;

    public function __construct( $search_service ) {
        $this->SearchService = $search_service;
    }

    public function search( $query, $limit = 10, $offset = 0 ) {
        $QueryString = $this->buildQueryString( $query );

        $SearchResults = $this->SearchService->search(
            'bulletin', $QueryString, $limit, $offset
        );

        return $SearchResults;
    }

    protected function buildQueryString( $query ) {
        
    }

    protected function buildCategoryQuery( $query ) {
        
    }

}
